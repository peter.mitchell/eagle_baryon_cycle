import numpy as np
import h5py
import os
import warnings
import match_searchsorted as ms

test_mode = False # Write a single subvolume with a non-default nsub_1d

sub_sample = False
n_per_bin = 1000
np.random.seed(13426743)

# 100 Mpc REF with 50 snips
'''sim_name = "L0100N1504_REF_50_snip"
h=0.6777 
box_size = 100 * h
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,
                98, 106, 114, 122, 130, 138, 146, 154, 164, 172, 180, 188,
                196, 204, 212, 220, 228, 236, 244, 252, 260, 268, 276, 284,
                292, 300, 308, 318, 326, 334, 342, 350, 358, 366, 374, 382,
                390, 398, 405][::-1]
nsub_1d = 4'''

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
h=0.6777 
box_size = 100 * h
snap_final = 200
snipshots, snapshots = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 2'''

# 50 Mpc reference run, 28 snapshots
'''sim_name = "L0050N0752_REF_snap"
h=0.6777
box_size = 50 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
nsub_1d = 1'''

# 25 Mpc reference run, 28 snapshots
'''sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_snap_par_test"
#sim_name = "L0025N0376_REF_snap_par_test2"
h=0.6777
box_size = 25 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
#nsub_1d = 1'''

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
h=0.6777
box_size = 25 * h
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
h=0.6777
box_size = 25 * h
snap_final = 100
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
                    73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
                        145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
                        217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
                        289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
                        361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]
nsub_1d = 1'''

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip_smthr_abshi"
#sim_name = "L0025N0376_REF_200_snip_smthr_frac"
#sim_name = "L0025N0376_REF_200_snip_m200_smthr_abshi"
#sim_name = "L0025N0376_REF_200_snip_not_bound_only"
sim_name = "L0025N0376_REF_200_snip_track_star" # Note this is only a separate analysis to avoid needing to redo the extra_accretion/wind measurements with new output format
h=0.6777
box_size = 25 * h
snap_final = 200
snapshots = np.arange(1,snap_final+1)[::-1] # Note this one starts at one for some reason
snipshots = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub_1d = 1

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
h=0.6777
box_size = 25 * h
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1'''

# 25 Mpc ref 400 snapshots
'''sim_name = "L0025N0376_REF_400_snap"
h=0.6777
box_size = 25 * h
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1'''

# 25 Mpc ref 500 snapshots
'''sim_name = "L0025N0376_REF_500_snap"
h=0.6777
box_size = 25 * h
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1'''

# 25 Mpc ref 1000 snapshots
'''sim_name = "L0025N0376_REF_1000_snap"
h=0.6777
box_size = 25 * h
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1'''

# 50 Mpc CFB with 28 snaps
'''sim_name = "L0050N0752_CFB_snap"
h=0.6777
box_size = 50 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 50mpc box"
nsub_1d = 2'''

# 50 Mpc model with no AGN, 28 snapshots
'''sim_name = "L0050N0752_NOAGN_snap"
h=0.6777
box_size = 50 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
nsub_1d = 1'''

# 25 Mpc - recal (high-res), 202 snipshots
'''#sim_name = "L0025N0752_Recal"
#sim_name = "L0025N0752_Recal_m200_smthr_abslo"
sim_name = "L0025N0752_Recal_m200_smthr_abshi"
h=0.6777
box_size = 25*h
snap_final = 202
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1'''

if len(snapshots)>len(snipshots):
    snapshots = snapshots[0:-1]
if len(snapshots)>len(snipshots):
    print "Error, len(snapshots) != len(snipshots)"
    quit()

# Mass scale for which to separate massive haloes (and their subhaloes) from the overthise equal volume subvolumes at z=0
mhhalo_sep = 10**18 # Dummy value

# Choose which catalogue file to read
#input_path =       "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
#output_path_base = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub_1d)+"/"
input_path =       "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
output_path_base = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub_1d)+"/"

if sub_sample:
    #output_path_base = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub_1d)+"_subsample_"+str(n_per_bin)+"/"
    output_path_base = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub_1d)+"_subsample_"+str(n_per_bin)+"/"

if test_mode:
    print ""
    print ""
    print "running in test mode"
    print ""
    print ""

    test_mode_1_sub = True
    if "L0025N0376" in sim_name:
        subvol_choose = 36
        #subvol_choose = 500 # m200~10^10.5 at z=0
        #subvol_choose = 501
        #subvol_choose = 525
        #subvol_choose = 1574 # 1e10 halo at z=0, for investigating the ISM changes 
    elif "L0100N1504" in sim_name:
        subvol_choose = 2000
    else:
        print "errrr"
    print "only choosing one sub of subvolume", subvol_choose
    print ""

    mhhalo_sep = 10**11.5
    if test_mode_1_sub:
        mhhalo_sep = 1e9
    mchalo_choose = 10**14 # used to only select a single halo for a separate subvolume
    nsub_1d = 1
    #output_path_base = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub_1d)+"_test_mode/"
    output_path_base = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub_1d)+"_test_mode/"

# Choose r200 fractions to compute subvol sizes
r200_frac = 1.5



# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name

if "L0025N0376_REF_200_snip" in sim_name:
    filename = "subhalo_catalogue_L0025N0376_REF_200_snip"
if "L0025N0752_Recal" in sim_name:
    filename = "subhalo_catalogue_L0025N0752_Recal"

filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

############### Select subvolumes for z0 subhaloes #####################################

# First, read the z0 snapshot information
snapnum_group = File["Snap_"+str(snapshots.max())]
subhalo_group = snapnum_group["subhalo_properties"]

g2Msun = 1./(1.989e33)
try:
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h
    mhhalo_subhalo = subhalo_group["mhhalo"][:] * g2Msun * 1.989e43 /h # Msun
except:
    print "Error: need to run copy_mchalo.py first"
    quit()

subgroup_number = subhalo_group["SubGroupNumber_hydro"][:]
group_number = subhalo_group["GroupNumber_hydro"][:]

if sub_sample:
    print "Sub-sampling z=0 halo catalogue"
    
    # Spacing of bins
    log_dmh = 0.2
    bin_mh = np.arange(8.0, 15.1,0.2)
    bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])
    
    ok = np.log10(mchalo_subhalo) > 4
    indicies = np.arange(0,len(mchalo_subhalo))
    keep = []

    for j_bin in range(len(bin_mh_mid)):
        ok_j = np.where((np.log10(mchalo_subhalo) > bin_mh[j_bin]) & (np.log10(mchalo_subhalo) < bin_mh[j_bin+1]) & (subgroup_number==0))[0]
        if len(ok_j) > n_per_bin:
            complete = False
            while not complete:
                rand_ind = np.random.randint(0,len(ok_j),n_per_bin*5)
                temp = np.unique(rand_ind)
                np.random.shuffle(temp)
                if len(temp) >= n_per_bin:
                    rand_ind = temp[0:n_per_bin]
                    complete = True
            keep.append(ok_j[rand_ind])
        else:
            keep.append(ok_j)
    keep = np.concatenate(keep)

    ptr = ms.match(group_number, group_number[keep])
    keep = ptr>=0

    keep = keep[np.argsort(group_number[keep])]

else:
    keep = np.arange(len(mchalo_subhalo))[np.argsort(group_number)] # Sorting is for test mode so that we select the desired halo grn

group_number = group_number[keep]
subgroup_number = subgroup_number[keep]
mchalo_subhalo = mchalo_subhalo[keep]
mhhalo_subhalo = mhhalo_subhalo[keep]

node_index = subhalo_group["nodeIndex"][:][keep]
halo_x_subhalo = subhalo_group["CentreOfPotential_x"][:][keep] # Mpc h^-1
halo_y_subhalo = subhalo_group["CentreOfPotential_y"][:][keep]
halo_z_subhalo = subhalo_group["CentreOfPotential_z"][:][keep]

subhalo_noprogen_group = snapnum_group["subhalo_no_progenitor"]
node_index_noprogen = subhalo_noprogen_group["nodeIndex"][:]
halo_x_noprogen = subhalo_noprogen_group["CentreOfPotential_x"][:] # Mpc h^-1
halo_y_noprogen = subhalo_noprogen_group["CentreOfPotential_y"][:]
halo_z_noprogen = subhalo_noprogen_group["CentreOfPotential_z"][:]
subgroup_number_noprogen = subhalo_noprogen_group["SubGroupNumber_hydro"][:]
group_number_noprogen = subhalo_noprogen_group["GroupNumber_hydro"][:]

# Separate out haloes (and their subhaloes) which are above a given mass point at z=0 (these will get their own separate subvolumes)
sep = mhhalo_subhalo > mhhalo_sep

# For the remaining haloes, subdivide them into equal volume subvolumes at z=0 (otherwise you need a space-filling curve in python)

# Compute subvolume vertices
xyz_subvol = np.zeros((nsub_1d**3,3))
ind_sub = 0
for i in range(nsub_1d):
    x_sub = i / float(nsub_1d)
    for j in range(nsub_1d):
        y_sub = j / float(nsub_1d)
        for k in range(nsub_1d):
            z_sub = k / float(nsub_1d)
            xyz_subvol[ind_sub] = np.array([x_sub, y_sub, z_sub])
            ind_sub += 1

print "Doing initial subhalo domain decomposition at final output"
            
# Build node index list
node_index_list = []
node_index_noprogen_list = []
for n in range(nsub_1d**3):

    xmin,ymin,zmin = xyz_subvol[n]

    dx = 1/float(nsub_1d)
    xmax = xmin + dx; ymax = ymin + dx; zmax = zmin + dx

    # Look for satellites that have no central
    ptr = ms.match(group_number[subgroup_number>0], group_number[subgroup_number==0])
    no_central = np.zeros_like(group_number)<0
    no_central[subgroup_number>0] = ptr < 0

    ok = (halo_x_subhalo > xmin*box_size) & (halo_x_subhalo < xmax*box_size) & (halo_y_subhalo > ymin*box_size) & (halo_y_subhalo < ymax*box_size) & (halo_z_subhalo > zmin*box_size) & (halo_z_subhalo < zmax*box_size) & (sep == False) & ((subgroup_number==0)|(no_central))

    ptr = ms.match( group_number[subgroup_number>0], group_number[ok])
    ok[subgroup_number>0] = ptr >= 0

    node_index_list.append(node_index[ok])
    
    ok2 = (halo_x_noprogen > xmin*box_size) & (halo_x_noprogen < xmax*box_size) & (halo_y_noprogen > ymin*box_size) & (halo_y_noprogen < ymax*box_size) & (halo_z_noprogen > zmin*box_size) & (halo_z_noprogen < zmax*box_size) & (subgroup_number_noprogen==0)

    if len(group_number_noprogen[subgroup_number_noprogen>0]) > 0 and len(group_number_noprogen[ok2]) > 0:
        ptr = ms.match( group_number_noprogen[subgroup_number_noprogen>0], group_number_noprogen[ok2])
        ok2[subgroup_number_noprogen>0] = ptr >= 0

    node_index_noprogen_list.append(node_index_noprogen[ok2]) 

sep_cen = sep & (subgroup_number == 0)

nsep = len(subgroup_number[sep_cen])
for n in range(nsep):
    ok = group_number[sep] == group_number[sep_cen][n]
    node_index_list.append(node_index[sep][ok])

    node_index_noprogen_list.append([])

if test_mode:
    ind_choose = np.argmin(abs(mchalo_subhalo[sep_cen]-mchalo_choose))

# Total number of subvolumes
nsubvol = nsub_1d**3 + nsep

################# Now loop through snipshots, read in the full catalogue at each point and then loop through the subvolumes

def z2snip(z):
    snip = snipnum_list[ np.argmin(abs(ref_z-z)) ]
    return snip

for snap, snip in zip(snapshots,snipshots):

    print "building subvolumes for tree snapshot", snap
    
    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue

    #if snap < 162:
    #    print "Stopping prematurely for subvol 0 regen"
    #    quit()

    # Read the data
    snapnum_group = File["Snap_"+str(snap)]
       
    subhalo_group = snapnum_group["subhalo_properties"]
    subhalo_noprogen_group = snapnum_group["subhalo_no_progenitor"]
    progenitor_group = snapnum_group["main_progenitors"]
    all_subhalo_ns_group = snapnum_group["all_subhalo_ns"]

    group_number_subhalo = subhalo_group["GroupNumber_hydro"][:]
    subgroup_number_subhalo = subhalo_group["SubGroupNumber_hydro"][:]

    redshift_ts = np.array(subhalo_group["redshift"])
    redshift_ps = np.array(progenitor_group["redshift"])

    try:
        nodeIndex_subhalo = subhalo_group["nodeIndex"][:]
    except:
        output_path = output_path_base + "Snip_"+str(snip)+"/"
        try:
            os.makedirs(output_path)
        except OSError:
            pass
        continue

    descendantIndex_subhalo = subhalo_group["descendantIndex"][:]
    isInterpolated_subhalo = subhalo_group["isInterpolated"][:]
    halo_x_subhalo = subhalo_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_subhalo = subhalo_group["CentreOfPotential_y"][:]
    halo_z_subhalo = subhalo_group["CentreOfPotential_z"][:]
    halo_vx_subhalo = subhalo_group["Velocity_x"][:] # kms^-1
    halo_vy_subhalo = subhalo_group["Velocity_y"][:]
    halo_vz_subhalo = subhalo_group["Velocity_z"][:]
    # rvir will be zero for satellites, but this should have no adverse effects (will also be zero for 25Mpc ref, but again no problem provided nsub_1d = 1)
    rvir_subhalo = subhalo_group["r200"][:] # kpc
    rvir_subhalo *= h/1e3*(1+redshift_ts) # cMpc h^-1
    vmax_subhalo = subhalo_group["Vmax"][:]

    halo_m200_host_subhalo = subhalo_group["Group_M_Crit200_host"][:]  * g2Msun * 1.989e43 /h # Msun
    halo_r200_host_subhalo = subhalo_group["Group_R_Crit200_host"][:] # cMpc h^-1

    g2Msun = 1./(1.989e33)
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h # Msun
    mhhalo_subhalo = subhalo_group["mhhalo"][:] * g2Msun * 1.989e43 /h # Msun

    group_number_noprogen = subhalo_noprogen_group["GroupNumber_hydro"][:]
    subgroup_number_noprogen = subhalo_noprogen_group["SubGroupNumber_hydro"][:]
    nodeIndex_noprogen = subhalo_noprogen_group["nodeIndex"][:]
    descendantIndex_noprogen = subhalo_noprogen_group["descendantIndex"][:]
    isInterpolated_noprogen = subhalo_noprogen_group["isInterpolated"][:]
    halo_x_noprogen = subhalo_noprogen_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_noprogen = subhalo_noprogen_group["CentreOfPotential_y"][:]
    halo_z_noprogen = subhalo_noprogen_group["CentreOfPotential_z"][:]
    halo_vx_noprogen = subhalo_noprogen_group["Velocity_x"][:] # kms^-1
    halo_vz_noprogen = subhalo_noprogen_group["Velocity_z"][:]
    rvir_noprogen = subhalo_noprogen_group["r200"][:] # kpc
    rvir_noprogen *= h/1e3*(1+redshift_ts) # cMpc h^-1
    halo_m200_host_noprogen = subhalo_noprogen_group["Group_M_Crit200_host"][:] * g2Msun * 1.989e43 /h # Msun
    halo_r200_host_noprogen = subhalo_noprogen_group["Group_R_Crit200_host"][:]


    group_number_progenitor = progenitor_group["GroupNumber_hydro"][:]
    subgroup_number_progenitor = progenitor_group["SubGroupNumber_hydro"][:]
    node_index_progenitor = progenitor_group["nodeIndex"][:]
    isInterpolated_progenitor = progenitor_group["isInterpolated"][:]
    halo_x_progenitor = progenitor_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_progenitor = progenitor_group["CentreOfPotential_y"][:]
    halo_z_progenitor = progenitor_group["CentreOfPotential_z"][:]
    halo_vx_progenitor = progenitor_group["Velocity_x"][:] # kms^-1
    halo_vy_progenitor = progenitor_group["Velocity_y"][:]
    halo_vz_progenitor = progenitor_group["Velocity_z"][:]
    vmax_progenitor = progenitor_group["Vmax"][:]
    # rvir will be zero for satellites, but this should have no adverse effects
    rvir_progenitor = progenitor_group["r200"][:] # kpc
    rvir_progenitor *= h/1e3*(1+redshift_ps) # cMpc h^-1
    halo_m200_host_progenitor = progenitor_group["Group_M_Crit200_host"][:] * g2Msun * 1.989e43 /h # Msun
    halo_r200_host_progenitor = progenitor_group["Group_R_Crit200_host"][:]

    group_number_all_progenitor = all_subhalo_ns_group["GroupNumber"][:]
    subgroup_number_all_progenitor = all_subhalo_ns_group["SubGroupNumber"][:]
    halo_x_all_progenitor = all_subhalo_ns_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_all_progenitor = all_subhalo_ns_group["CentreOfPotential_y"][:]
    halo_z_all_progenitor = all_subhalo_ns_group["CentreOfPotential_z"][:]
    descendant_index_all_progenitor = all_subhalo_ns_group["descendantIndex"][:]
    node_index_all_progenitor = all_subhalo_ns_group["nodeIndex"][:]
    isInterpolated_all_progenitor = all_subhalo_ns_group["isInterpolated"][:]
    halo_m200_host_all_progenitor = all_subhalo_ns_group["Group_M_Crit200_host"][:] * g2Msun * 1.989e43 /h # Msun
    halo_r200_host_all_progenitor = all_subhalo_ns_group["Group_R_Crit200_host"][:]
    mtot_all_progenitor = all_subhalo_ns_group["Mtotal"][:] *g2Msun * 1.989e43 /h

    # Choose where to output subvol files and write directory if needed
    output_path = output_path_base + "Snip_"+str(snip)+"/"
    try:
        os.makedirs(output_path)
    except OSError:
        pass



    if nsub_1d > 1:

        # For all progenitor list, just take crude step of cloning the sample if they are within a given distance to the box boundary
        clone_dist = 200.0 /1e3 *h # 200 kpc in units of Mpc h^-1

        near_large_x = (halo_x_all_progenitor > box_size - clone_dist) & (isInterpolated_all_progenitor==0)
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_large_x]-box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_large_x]-box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_large_x]-box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_large_x])
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_large_x])
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_large_x])
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_large_x])
        isInterpolated_all_progenitor = np.append(isInterpolated_all_progenitor, isInterpolated_all_progenitor[near_large_x])
        halo_m200_host_all_progenitor = np.append(halo_m200_host_all_progenitor, halo_m200_host_all_progenitor[near_large_x])
        halo_r200_host_all_progenitor = np.append(halo_r200_host_all_progenitor, halo_r200_host_all_progenitor[near_large_x])
        mtot_all_progenitor = np.append(mtot_all_progenitor, mtot_all_progenitor[near_large_x])

        near_small_x = (halo_x_all_progenitor < clone_dist) & (isInterpolated_all_progenitor==0)
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_small_x]+box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_small_x]+box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_small_x]+box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_small_x])
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_small_x])
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_small_x])
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_small_x])
        isInterpolated_all_progenitor = np.append(isInterpolated_all_progenitor, isInterpolated_all_progenitor[near_small_x])
        halo_m200_host_all_progenitor = np.append(halo_m200_host_all_progenitor, halo_m200_host_all_progenitor[near_small_x])
        halo_r200_host_all_progenitor = np.append(halo_r200_host_all_progenitor, halo_r200_host_all_progenitor[near_small_x])
        mtot_all_progenitor = np.append(mtot_all_progenitor, mtot_all_progenitor[near_small_x])


        near_large_y = (halo_y_all_progenitor > box_size - clone_dist) & (isInterpolated_all_progenitor==0)
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_large_y]-box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_large_y]-box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_large_y]-box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_large_y])
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_large_y])
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_large_y])
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_large_y])
        isInterpolated_all_progenitor = np.append(isInterpolated_all_progenitor, isInterpolated_all_progenitor[near_large_y])
        halo_m200_host_all_progenitor = np.append(halo_m200_host_all_progenitor, halo_m200_host_all_progenitor[near_large_y])
        halo_r200_host_all_progenitor = np.append(halo_r200_host_all_progenitor, halo_r200_host_all_progenitor[near_large_y])
        mtot_all_progenitor = np.append(mtot_all_progenitor, mtot_all_progenitor[near_large_y])

        
        near_small_y = (halo_y_all_progenitor < clone_dist) & (isInterpolated_all_progenitor==0)
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_small_y]+box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_small_y]+box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_small_y]+box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_small_y])
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_small_y])
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_small_y])
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_small_y])
        isInterpolated_all_progenitor = np.append(isInterpolated_all_progenitor, isInterpolated_all_progenitor[near_small_y])
        halo_m200_host_all_progenitor = np.append(halo_m200_host_all_progenitor, halo_m200_host_all_progenitor[near_small_y])
        halo_r200_host_all_progenitor = np.append(halo_r200_host_all_progenitor, halo_r200_host_all_progenitor[near_small_y])
        mtot_all_progenitor = np.append(mtot_all_progenitor, mtot_all_progenitor[near_small_y])

        
        near_large_z = (halo_z_all_progenitor > box_size - clone_dist) & (isInterpolated_all_progenitor==0)
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_large_z]-box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_large_z]-box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_large_z]-box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_large_z])
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_large_z])
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_large_z])
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_large_z])
        isInterpolated_all_progenitor = np.append(isInterpolated_all_progenitor, isInterpolated_all_progenitor[near_large_z])
        halo_m200_host_all_progenitor = np.append(halo_m200_host_all_progenitor, halo_m200_host_all_progenitor[near_large_z])
        halo_r200_host_all_progenitor = np.append(halo_r200_host_all_progenitor, halo_r200_host_all_progenitor[near_large_z])
        mtot_all_progenitor = np.append(mtot_all_progenitor, mtot_all_progenitor[near_large_z])


        near_small_z = (halo_z_all_progenitor < clone_dist) & (isInterpolated_all_progenitor==0)
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_small_z]+box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_small_z]+box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_small_z]+box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_small_z])
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_small_z])
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_small_z])
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_small_z])
        isInterpolated_all_progenitor = np.append(isInterpolated_all_progenitor, isInterpolated_all_progenitor[near_small_z])
        halo_m200_host_all_progenitor = np.append(halo_m200_host_all_progenitor, halo_m200_host_all_progenitor[near_small_z])
        halo_r200_host_all_progenitor = np.append(halo_r200_host_all_progenitor, halo_r200_host_all_progenitor[near_small_z])
        mtot_all_progenitor = np.append(mtot_all_progenitor, mtot_all_progenitor[near_small_z])


    # Select subhalo samples within each subvolume, compute required comoving subvolume boundaries and write to subvolume files
    xyz_subvol_part_min = np.zeros((nsubvol,3))
    xyz_subvol_part_max = np.zeros((nsubvol,3))

    node_index_list_ps = list(node_index_list)

    # Work out which subhaloes are in which subvolume on this snapshot
    if snap != snapshots.max():
        node_index_list = []

        for n in range(nsubvol):
            
            if test_mode:

                if not test_mode_1_sub:
                    if n != ind_choose + nsub_1d**3:
                        node_index_list.append([])
                        continue
                else:

                    if n < nsub_1d**3 or n != subvol_choose:
                        node_index_list.append([])
                        continue

            if len(descendantIndex_subhalo) > 0 and len(node_index_list_ps[n]) >0:
                ptr = ms.match(descendantIndex_subhalo, node_index_list_ps[n])
                ok = ptr >= 0
                node_index_list.append(nodeIndex_subhalo[ok])
            else:
                node_index_list.append([])

    for n in range(nsubvol):

        if test_mode:
            if not test_mode_1_sub:
                if n != ind_choose + nsub_1d**3:
                    continue
            else:
                if n < nsub_1d**3 or n != subvol_choose:
                    continue

        #if n > 0:
        #   print "hack skip for subvol 0 regen"
        #   continue

        # Check for satellites without a central
        ptr_nocentral = ms.match(group_number_subhalo[subgroup_number_subhalo>0], group_number_subhalo[subgroup_number_subhalo==0])
        no_central = np.zeros_like(group_number_subhalo)<0
        no_central[subgroup_number_subhalo>0] = (ptr_nocentral < 0) & (isInterpolated_subhalo[subgroup_number_subhalo>0]==0)

        if snap == snapshots.max():
            ptr = ms.match(nodeIndex_subhalo, node_index_list[n])
            ok = ((ptr >= 0) & (subgroup_number_subhalo==0)) | ((ptr>=0) & no_central)
            ok_nI = ok

        else:
            if len(descendantIndex_subhalo) > 0 and len(node_index_list_ps[n]) > 0:
                ptr = ms.match(descendantIndex_subhalo, node_index_list_ps[n])
            else:
                continue

            # Interpolated subhaloes always have a subgroup_number of ~1e8
            # so ok_nI here is selected non_interpolated subhalos that have a progenitor, and are:
            # a) central, b) satellite with no central
            ok_nI = ((ptr >= 0) & (subgroup_number_subhalo==0)) | ((ptr>=0) & no_central)

            # Same but also selects Interpolated subhaloes (that have a progenitor)
            ok = ok_nI | ((ptr>=0) & (isInterpolated_subhalo==1))

        if n < nsub_1d**3:
            xmin,ymin,zmin = xyz_subvol[n]

            dx = 1/float(2*nsub_1d)
            xmid = box_size * (xmin + dx); ymid = box_size * (ymin + dx); zmid = box_size * (zmin + dx)

            if nsub_1d == 1:
                disp = box_size * 2
            else:
                disp = box_size * 0.5
            #else:
            #    disp = box_size * 1/3.0

        else:
            try:
                main = np.argmax(mchalo_subhalo[ok_nI][subgroup_number_subhalo[ok_nI]==0])
            except:
                continue
            xmid = halo_x_subhalo[ok_nI][subgroup_number_subhalo[ok_nI]==0][main]
            ymid = halo_y_subhalo[ok_nI][subgroup_number_subhalo[ok_nI]==0][main]
            zmid = halo_z_subhalo[ok_nI][subgroup_number_subhalo[ok_nI]==0][main]

            disp = box_size * 0.5

        # First rearrange central positions that cross the box boundaries (as well as satellites without a central)
        halo_x_cen = halo_x_subhalo[ok_nI]
        halo_y_cen = halo_y_subhalo[ok_nI]
        halo_z_cen = halo_z_subhalo[ok_nI]

        too_large_x = halo_x_cen - xmid > disp
        halo_x_cen[too_large_x] -= box_size
        too_small_x = xmid - halo_x_cen > disp
        halo_x_cen[too_small_x] += box_size
        
        too_large_y = halo_y_cen - ymid > disp
        halo_y_cen[too_large_y] -= box_size
        too_small_y = ymid - halo_y_cen > disp
        halo_y_cen[too_small_y] += box_size
        
        too_large_z = halo_z_cen - zmid > disp
        halo_z_cen[too_large_z] -= box_size
        too_small_z = zmid - halo_z_cen > disp
        halo_z_cen[too_small_z] += box_size

        temp = halo_x_subhalo[ok_nI]; temp = halo_x_cen; halo_x_subhalo[ok_nI] = temp
        temp = halo_y_subhalo[ok_nI]; temp = halo_y_cen; halo_y_subhalo[ok_nI] = temp
        temp = halo_z_subhalo[ok_nI]; temp = halo_z_cen; halo_z_subhalo[ok_nI] = temp

        # Now rearrange satellites that cross the box boundary
        sat = (subgroup_number_subhalo>0) & (no_central==False) & (isInterpolated_subhalo==0)
        if len(group_number_subhalo[subgroup_number_subhalo>0])>0 and len(group_number_subhalo[ok_nI])>0: 
            ptr = ms.match( group_number_subhalo[(subgroup_number_subhalo>0)&(no_central==False)&(isInterpolated_subhalo==0)], group_number_subhalo[ok_nI])

            halo_x_host = halo_x_subhalo[ok_nI][ptr][ptr>=0]
            halo_y_host = halo_y_subhalo[ok_nI][ptr][ptr>=0]
            halo_z_host = halo_z_subhalo[ok_nI][ptr][ptr>=0]       

            ok[(subgroup_number_subhalo>0)&(no_central==False)&(isInterpolated_subhalo==0)] = ptr >= 0

            halo_x_sat = halo_x_subhalo[sat][ptr>=0]
            halo_y_sat = halo_y_subhalo[sat][ptr>=0]
            halo_z_sat = halo_z_subhalo[sat][ptr>=0]

            too_large_x = halo_x_sat - halo_x_host > 0.25 * box_size
            halo_x_sat[too_large_x] -= box_size
            too_small_x = halo_x_host - halo_x_sat > 0.25 * box_size
            halo_x_sat[too_small_x] += box_size
        
            too_large_y = halo_y_sat - halo_y_host > 0.25 * box_size
            halo_y_sat[too_large_y] -= box_size
            too_small_y = halo_y_host - halo_y_sat > 0.25 * box_size
            halo_y_sat[too_small_y] += box_size
        
            too_large_z = halo_z_sat - halo_z_host > 0.25 * box_size
            halo_z_sat[too_large_z] -= box_size
            too_small_z = halo_z_host - halo_z_sat > 0.25 * box_size
            halo_z_sat[too_small_z] += box_size

            temp = halo_x_subhalo[sat]; temp[ptr>=0] = halo_x_sat; halo_x_subhalo[sat] = temp
            temp = halo_y_subhalo[sat]; temp[ptr>=0] = halo_y_sat; halo_y_subhalo[sat] = temp
            temp = halo_z_subhalo[sat]; temp[ptr>=0] = halo_z_sat; halo_z_subhalo[sat] = temp
            
        if len(node_index_noprogen_list[n]) > 0:
            if snap != snapshots.max():
                ptr2 = ms.match(descendantIndex_noprogen, node_index_list_ps[n])
            else:
                ptr2 = ms.match(nodeIndex_noprogen, node_index_noprogen_list[n])

            ok2 = (ptr2 >= 0) & (isInterpolated_noprogen==0)

            # First rearrange central positions that cross the box boundaries 
            halo_x_cen = halo_x_noprogen[ok2][subgroup_number_noprogen[ok2]==0]
            halo_y_cen = halo_y_noprogen[ok2][subgroup_number_noprogen[ok2]==0]
            halo_z_cen = halo_z_noprogen[ok2][subgroup_number_noprogen[ok2]==0]

            too_large_x = halo_x_cen - xmid > disp
            halo_x_cen[too_large_x] -= box_size
            too_small_x = xmid - halo_x_cen > disp
            halo_x_cen[too_small_x] += box_size
        
            too_large_y = halo_y_cen - ymid > disp
            halo_y_cen[too_large_y] -= box_size
            too_small_y = ymid - halo_y_cen > disp
            halo_y_cen[too_small_y] += box_size
        
            too_large_z = halo_z_cen - zmid > disp
            halo_z_cen[too_large_z] -= box_size
            too_small_z = zmid - halo_z_cen > disp
            halo_z_cen[too_small_z] += box_size

            temp = halo_x_noprogen[ok2]; temp[subgroup_number_noprogen[ok2]==0] = halo_x_cen; halo_x_noprogen[ok2] = temp
            temp = halo_y_noprogen[ok2]; temp[subgroup_number_noprogen[ok2]==0] = halo_y_cen; halo_y_noprogen[ok2] = temp
            temp = halo_z_noprogen[ok2]; temp[subgroup_number_noprogen[ok2]==0] = halo_z_cen; halo_z_noprogen[ok2] = temp

            # Now rearrange satellite positions that cross the box boundaries
            sat = (subgroup_number_noprogen>0) & (isInterpolated_noprogen==0)
            if len(group_number_noprogen[sat])>0 and len(group_number_noprogen[ok2&(sat==False)])>0:
                ptr = ms.match(group_number_noprogen[sat], group_number_noprogen[ok2&(sat==False)])

                halo_x_host = halo_x_noprogen[ok2&(sat==False)][ptr][ptr>=0]
                halo_y_host = halo_y_noprogen[ok2&(sat==False)][ptr][ptr>=0]
                halo_z_host = halo_z_noprogen[ok2&(sat==False)][ptr][ptr>=0]
            
                ok2[(subgroup_number_noprogen>0)&(isInterpolated_noprogen==0)] = ptr >= 0

                halo_x_sat = halo_x_noprogen[sat][ptr>=0]
                halo_y_sat = halo_y_noprogen[sat][ptr>=0]
                halo_z_sat = halo_z_noprogen[sat][ptr>=0]

                too_large_x = halo_x_sat - halo_x_host > 0.25 * box_size
                halo_x_sat[too_large_x] -= box_size
                too_small_x = halo_x_host - halo_x_sat > 0.25 * box_size
                halo_x_sat[too_small_x] += box_size

                too_large_y = halo_y_sat - halo_y_host > 0.25 * box_size
                halo_y_sat[too_large_y] -= box_size
                too_small_y = halo_y_host - halo_y_sat > 0.25 * box_size
                halo_y_sat[too_small_y] += box_size

                too_large_z = halo_z_sat - halo_z_host > 0.25 * box_size
                halo_z_sat[too_large_z] -= box_size
                too_small_z = halo_z_host - halo_z_sat > 0.25 * box_size
                halo_z_sat[too_small_z] += box_size

                temp = halo_x_noprogen[sat]; temp[ptr>=0] = halo_x_sat; halo_x_noprogen[sat] = temp
                temp = halo_y_noprogen[sat]; temp[ptr>=0] = halo_y_sat; halo_y_noprogen[sat] = temp
                temp = halo_z_noprogen[sat]; temp[ptr>=0] = halo_z_sat; halo_z_noprogen[sat] = temp
            
        else:
            ok2 = np.zeros_like(halo_x_noprogen) > 0


        halo_xmax_subhalo = halo_x_subhalo + rvir_subhalo * r200_frac
        halo_xmin_subhalo = halo_x_subhalo - rvir_subhalo * r200_frac
        halo_ymax_subhalo = halo_y_subhalo + rvir_subhalo * r200_frac
        halo_ymin_subhalo = halo_y_subhalo - rvir_subhalo * r200_frac
        halo_zmax_subhalo = halo_z_subhalo + rvir_subhalo * r200_frac
        halo_zmin_subhalo = halo_z_subhalo - rvir_subhalo * r200_frac

        halo_xmax_noprogen = halo_x_noprogen + rvir_noprogen * r200_frac
        halo_xmin_noprogen = halo_x_noprogen - rvir_noprogen * r200_frac
        halo_ymax_noprogen = halo_y_noprogen + rvir_noprogen * r200_frac
        halo_ymin_noprogen = halo_y_noprogen - rvir_noprogen * r200_frac
        halo_zmax_noprogen = halo_z_noprogen + rvir_noprogen * r200_frac
        halo_zmin_noprogen = halo_z_noprogen - rvir_noprogen * r200_frac

        # For progenitor haloes, we need to reposition those that cross the simulation boundary w.r.t their descendants in order to compute the required particle data vertices
        nI_progen = ok & (isInterpolated_progenitor == 0) & (isInterpolated_subhalo == 0)
        too_large_x = (halo_x_progenitor - halo_x_subhalo > 0.25 * box_size) & nI_progen  
        halo_x_progenitor[too_large_x] -= box_size
        too_small_x = (halo_x_subhalo - halo_x_progenitor > 0.25 * box_size) & nI_progen  
        halo_x_progenitor[too_small_x] += box_size

        too_large_y = (halo_y_progenitor - halo_y_subhalo > 0.25 * box_size) & nI_progen  
        halo_y_progenitor[too_large_y] -= box_size
        too_small_y = (halo_y_subhalo - halo_y_progenitor > 0.25 * box_size) & nI_progen  
        halo_y_progenitor[too_small_y] += box_size
        
        too_large_z = (halo_z_progenitor - halo_z_subhalo > 0.25 * box_size) & nI_progen  
        halo_z_progenitor[too_large_z] -= box_size
        too_small_z = (halo_z_subhalo - halo_z_progenitor > 0.25 * box_size) & nI_progen  
        halo_z_progenitor[too_small_z] += box_size
        
        halo_xmax_progenitor = halo_x_progenitor + rvir_progenitor * r200_frac
        halo_xmin_progenitor = halo_x_progenitor - rvir_progenitor * r200_frac
        halo_ymax_progenitor = halo_y_progenitor + rvir_progenitor * r200_frac
        halo_ymin_progenitor = halo_y_progenitor - rvir_progenitor * r200_frac
        halo_zmax_progenitor = halo_z_progenitor + rvir_progenitor * r200_frac
        halo_zmin_progenitor = halo_z_progenitor - rvir_progenitor * r200_frac

        if len(descendant_index_all_progenitor)>0 and len(nodeIndex_subhalo[ok])>0:
            ptr3 = ms.match(descendant_index_all_progenitor, nodeIndex_subhalo[ok])
            ok3 = ptr3 >= 0
        else:
            ok3 = np.ones_like(descendant_index_all_progenitor)<0

        # Work out cube size to read particles for in this subvolume
        if len(halo_x_subhalo[ok_nI]) > 0:
                
            if len(halo_xmax_noprogen[ok2]) == 0:
                halo_xmax_noprogen_ok2 = [0]
                halo_xmin_noprogen_ok2 = [1e9]
                halo_ymax_noprogen_ok2 = [0]
                halo_ymin_noprogen_ok2 = [1e9]
                halo_zmax_noprogen_ok2 = [0]
                halo_zmin_noprogen_ok2 = [1e9]
            else:
                halo_xmax_noprogen_ok2 = halo_xmax_noprogen[ok2]
                halo_xmin_noprogen_ok2 = halo_xmin_noprogen[ok2]
                halo_ymax_noprogen_ok2 = halo_ymax_noprogen[ok2]
                halo_ymin_noprogen_ok2 = halo_ymin_noprogen[ok2]
                halo_zmax_noprogen_ok2 = halo_zmax_noprogen[ok2]
                halo_zmin_noprogen_ok2 = halo_zmin_noprogen[ok2]

            # vertices of the cube for which we are going to read particle data for later
            xmax_part = max(np.max(halo_xmax_subhalo[ok_nI]),np.max(halo_xmax_progenitor[nI_progen]),np.max(halo_xmax_noprogen_ok2))
            ymax_part = max(np.max(halo_ymax_subhalo[ok_nI]),np.max(halo_ymax_progenitor[nI_progen]),np.max(halo_ymax_noprogen_ok2))
            zmax_part = max(np.max(halo_zmax_subhalo[ok_nI]),np.max(halo_zmax_progenitor[nI_progen]),np.max(halo_zmax_noprogen_ok2))
    
            xmin_part = min(np.min(halo_xmin_subhalo[ok_nI]),np.min(halo_xmin_progenitor[nI_progen]),np.min(halo_xmin_noprogen_ok2))
            ymin_part = min(np.min(halo_ymin_subhalo[ok_nI]),np.min(halo_ymin_progenitor[nI_progen]),np.min(halo_ymin_noprogen_ok2))
            zmin_part = min(np.min(halo_zmin_subhalo[ok_nI]),np.min(halo_zmin_progenitor[nI_progen]),np.min(halo_zmin_noprogen_ok2))

            if xmin_part < 0 and xmax_part > box_size:
                xmin_part = 0.0; xmax_part = box_size
            if ymin_part < 0 and ymax_part > box_size:
                ymin_part = 0.0; ymax_part = box_size
            if zmin_part < 0 and zmax_part > box_size:
                zmin_part = 0.0; zmax_part = box_size

            # Check selection boundary has a reasonable size
            disp_check = 0.75 * box_size
            if xmax_part - xmin_part > disp_check or ymax_part -ymin_part > disp_check or zmax_part -zmin_part > disp_check:
                if nsub_1d > 1:
                    print "Warning: selection boundary is too large, maybe to debug"

                    print ""
                    print "subvol", n
                    print xmin_part, xmax_part, ymin_part, ymax_part, zmin_part, zmax_part
                    print "xmin", np.min(halo_xmin_subhalo[ok_nI]), np.min(halo_xmin_progenitor[nI_progen]), np.min(halo_xmin_noprogen_ok2)
                    print "ymin", np.min(halo_ymin_subhalo[ok_nI]), np.min(halo_ymin_progenitor[nI_progen]), np.min(halo_ymin_noprogen_ok2)
                    print "zmin", np.min(halo_zmin_subhalo[ok_nI]), np.min(halo_zmin_progenitor[nI_progen]), np.min(halo_zmin_noprogen_ok2)
                    print ""
                    print "xmax", np.max(halo_xmax_subhalo[ok_nI]), np.max(halo_xmax_progenitor[nI_progen]), np.max(halo_xmax_noprogen_ok2)
                    print "ymax", np.max(halo_ymax_subhalo[ok_nI]), np.max(halo_ymax_progenitor[nI_progen]), np.max(halo_ymax_noprogen_ok2)
                    print "zmax", np.max(halo_zmax_subhalo[ok_nI]), np.max(halo_zmax_progenitor[nI_progen]), np.max(halo_zmax_noprogen_ok2)
                    print ""


        else:
                
            xmax_part = np.nan
            ymax_part = np.nan
            zmax_part = np.nan
                
            xmin_part = np.nan
            ymin_part = np.nan
            zmin_part = np.nan

        #print xmax_part, xmin_part, ymax_part, ymin_part, zmax_part, zmin_part
        #print "debugging so not writing anything to disk"
        #continue

        # Repair some corrupted subvols block
        #if snip < 306:
        #    print "stopping here"
        #    quit()
        #ok_temp = ((snip==306)&(n==4)) | ((snip==344)&(n==3))
        #if not ok_temp:
        #    print "Repairing 100 Mpc 3,4 subvols, skipping"
        #    continue
        #if snip != 932:
        #    print "skip"
        #    continue
        #else:
        #    print "Repair"

        # Write to disk
        filename = "subhalo_catalogue_subvol_"+sim_name+"_snip_"+str(snip)+"_"+str(n)+".hdf5"
        print "writing", output_path+filename

        # Delete existing hdf5 file if one exists before writing a new one
        if os.path.isfile(output_path+filename):
            os.remove(output_path+filename)
            #print "deleted previous hdf5 file at ", output_path+filename

        File_subvol = h5py.File(output_path+filename,"a")
        
        # Write tree snapshot info
        tree_snapshot_info = File_subvol.create_group("tree_snapshot_info")
        tree_snapshot_info.create_dataset("snapshots_tree", data= snapshot_numbers)
        tree_snapshot_info.create_dataset("redshifts_tree", data = z_snapshots)
        tree_snapshot_info.create_dataset("cosmic_times_tree", data = t_snapshots)
        tree_snapshot_info.create_dataset("this_tree_snapshot", data = snap)
        tree_snapshot_info.create_dataset("sn_i_a_pshots", data=sn_i_a_pshots_simulation)

        select_group = File_subvol.create_group("select_region")
        select_group.create_dataset("xyz_min",data=np.array([xmin_part,ymin_part,zmin_part]))
        select_group.create_dataset("xyz_max",data=np.array([xmax_part,ymax_part,zmax_part]))

        halo_data_group = File_subvol.create_group("subhalo_data")
        halo_data_group.create_dataset("group_number",data=group_number_subhalo[ok])
        halo_data_group.create_dataset("subgroup_number",data=subgroup_number_subhalo[ok])
        halo_data_group.create_dataset("rc200",data=rvir_subhalo[ok])
        halo_data_group.create_dataset("x_halo",data=halo_x_subhalo[ok])
        halo_data_group.create_dataset("y_halo",data=halo_y_subhalo[ok])
        halo_data_group.create_dataset("z_halo",data=halo_z_subhalo[ok])
        halo_data_group.create_dataset("vx_halo",data=halo_vx_subhalo[ok])
        halo_data_group.create_dataset("vy_halo",data=halo_vy_subhalo[ok])
        halo_data_group.create_dataset("vz_halo",data=halo_vz_subhalo[ok])
        halo_data_group.create_dataset("node_index",data=nodeIndex_subhalo[ok])
        halo_data_group.create_dataset("isInterpolated",data=isInterpolated_subhalo[ok])
        halo_data_group.create_dataset("descendant_index",data=descendantIndex_subhalo[ok])
        halo_data_group.create_dataset("mchalo",data=mchalo_subhalo[ok])
        halo_data_group.create_dataset("mhhalo",data=mhhalo_subhalo[ok])
        halo_data_group.create_dataset("vmax",data=vmax_subhalo[ok])
        halo_data_group.create_dataset("m200_host",data=halo_m200_host_subhalo[ok])
        halo_data_group.create_dataset("r200_host",data=halo_r200_host_subhalo[ok])

        no_progen_data_group = File_subvol.create_group("subhalo_noprogen")
        no_progen_data_group.create_dataset("group_number",data=group_number_noprogen[ok2])
        no_progen_data_group.create_dataset("subgroup_number",data=subgroup_number_noprogen[ok2])
        no_progen_data_group.create_dataset("rc200",data=rvir_noprogen[ok2])
        no_progen_data_group.create_dataset("x_halo",data=halo_x_noprogen[ok2])
        no_progen_data_group.create_dataset("y_halo",data=halo_y_noprogen[ok2])
        no_progen_data_group.create_dataset("z_halo",data=halo_z_noprogen[ok2])
        no_progen_data_group.create_dataset("node_index",data=nodeIndex_noprogen[ok2])
        no_progen_data_group.create_dataset("descendant_index",data=descendantIndex_noprogen[ok2])
        no_progen_data_group.create_dataset("m200_host",data=halo_m200_host_noprogen[ok2])
        no_progen_data_group.create_dataset("r200_host",data=halo_r200_host_noprogen[ok2])

        progenitor_data_group = File_subvol.create_group("progenitor_data")
        progenitor_data_group.create_dataset("group_number",data=group_number_progenitor[ok])
        progenitor_data_group.create_dataset("subgroup_number",data=subgroup_number_progenitor[ok])
        progenitor_data_group.create_dataset("rc200",data=rvir_progenitor[ok])
        progenitor_data_group.create_dataset("x_halo",data=halo_x_progenitor[ok])
        progenitor_data_group.create_dataset("y_halo",data=halo_y_progenitor[ok])
        progenitor_data_group.create_dataset("z_halo",data=halo_z_progenitor[ok])
        progenitor_data_group.create_dataset("vx_halo",data=halo_vx_progenitor[ok])
        progenitor_data_group.create_dataset("vy_halo",data=halo_vy_progenitor[ok])
        progenitor_data_group.create_dataset("vz_halo",data=halo_vz_progenitor[ok])
        progenitor_data_group.create_dataset("vmax",data=vmax_progenitor[ok])
        progenitor_data_group.create_dataset("node_index",data=node_index_progenitor[ok])
        progenitor_data_group.create_dataset("isInterpolated",data=isInterpolated_progenitor[ok])
        progenitor_data_group.create_dataset("m200_host",data=halo_m200_host_progenitor[ok])
        progenitor_data_group.create_dataset("r200_host",data=halo_r200_host_progenitor[ok])

        all_progenitor_group = File_subvol.create_group("all_progenitor_data")
        all_progenitor_group.create_dataset("group_number",data=group_number_all_progenitor[ok3])
        all_progenitor_group.create_dataset("subgroup_number",data=subgroup_number_all_progenitor[ok3])
        all_progenitor_group.create_dataset("descendant_index",data=descendant_index_all_progenitor[ok3])
        all_progenitor_group.create_dataset("isInterpolated",data=isInterpolated_all_progenitor[ok3])
        all_progenitor_group.create_dataset("m200_host",data=halo_m200_host_all_progenitor[ok3])
        all_progenitor_group.create_dataset("r200_host",data=halo_r200_host_all_progenitor[ok3])
        all_progenitor_group.create_dataset("x_halo",data=halo_x_all_progenitor[ok3])
        all_progenitor_group.create_dataset("y_halo",data=halo_y_all_progenitor[ok3])
        all_progenitor_group.create_dataset("z_halo",data=halo_z_all_progenitor[ok3])
        all_progenitor_group.create_dataset("mtot",data=mtot_all_progenitor[ok3])
        
        File_subvol.close()

    # Also delete any unwanted subvolume files with higher indicies
    finished = False
    while not finished:
        n += 1
        filename = "subhalo_catalogue_subvol_"+sim_name+"_snip_"+str(snip)+"_"+str(n)+".hdf5"
        if os.path.isfile(output_path+filename):
            os.remove(output_path+filename)
        else:
            finished = True

File.close()
