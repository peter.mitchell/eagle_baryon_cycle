import glob
import os
import read_eagle as re
import h5py
import numpy as np
import match_searchsorted as ms
import utilities_cosmology as uc
import measure_particles_functions as mpf
import measure_particles_classes as mpc

def Get_Particle_Data_Directories(DATDIR,sim_name,snip):

    # get file direcories and order by redshift
    if not "snap" in sim_name:
        SNIPS = []
        SNIPS_all = []
        for index_temp in range(10):
            SNIPS += glob.glob(DATDIR+'particledata_snip_'+str(index_temp)+'*')
            SNIPS_all += glob.glob(DATDIR+'snipshot_'+str(index_temp)+'*')
    else:

        SNIPS = []
        SNIPS_all = []
        for index_temp in range(10):
            SNIPS += glob.glob(DATDIR+'particledata_'+str(index_temp)+'*')
            SNIPS_all += glob.glob(DATDIR+'snapshot_'+str(index_temp)+'*')

    SNIPS.sort(key=os.path.getmtime)
    SNIPS_all.sort(key=os.path.getmtime)

    for SNIP in SNIPS:

        no_problem = False
        for SNIP_all in SNIPS_all:
            if SNIP[-13:] == SNIP_all[-13:]:
                no_problem = True
                break
        if not no_problem:
            if SNIP == "/cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/REFERENCE/data/particledata_001_z015p132": # Special case
                continue
            else:
                print "Error: mpio.Get_Particle_Data_Directories(): snip,",SNIP,"could not be found in the SNIPs_all list"
                quit()

        snip_str = str(snip)

        if len(snip_str) == 1:
            snip_str = "00"+snip_str
        elif len(snip_str) ==2:
            snip_str = "0" + snip_str
        if not "snap" in sim_name:
            if "snip_"+snip_str in SNIP:
                path = SNIP
                path_all = SNIP_all
        else:
            if "data_"+snip_str in SNIP:
                path = SNIP
                path_all = SNIP_all

    try:
        return path, path_all
    except:
        print "Error in mpio.Get_Particle_Data_Directories(): couldn't find the file paths"
        quit()

def Get_Box_Size(sim_name, DATDIR,snip_use):
    path, path_all = Get_Particle_Data_Directories(DATDIR,sim_name,snip_use)


    if not "snap" in sim_name:
        egfile  = path+"/eagle_subfind_snip_particles"+path.split('particledata_snip')[-1]+'.0.hdf5'
    else:
        egfile  = path+"/eagle_subfind_particles"+path.split('particledata')[-1]+'.0.hdf5'
    sobj        = re.EagleSnapshot(egfile)
    boxsize = sobj.boxsize # Mpc h^-1
    return boxsize

def Read_Particle_Data(snip, xyz_max, xyz_min, sim_name, DATDIR):

    path, path_all = Get_Particle_Data_Directories(DATDIR,sim_name,snip)

    if not "snap" in sim_name:
        egfile  = path+"/eagle_subfind_snip_particles"+path.split('particledata_snip')[-1]+'.0.hdf5'
    else:
        egfile  = path+"/eagle_subfind_particles"+path.split('particledata')[-1]+'.0.hdf5'

    sobj        = re.EagleSnapshot(egfile)
    h5py_file = h5py.File(egfile)

    h = h5py_file["Header"].attrs["HubbleParam"]

    if not "snap" in sim_name:
        redshift = float(path.split('particledata_snip_')[-1][5:].replace('p','.'))
    else:
        redshift = float(path.split('particledata_')[-1][5:].replace('p','.'))
    expansion_factor = 1. / (1.+redshift)
    boxsize = sobj.boxsize # Mpc h^-1

    # Select everything inside the desired subvolume that lies within the simulation boundaries
    vertices    = (
        xyz_min[0] , xyz_max[0],
        xyz_min[1] , xyz_max[1],
        xyz_min[2] , xyz_max[2]
        )

    sobj.select_region(*vertices)

    # 0 => gas particles (PartType0)
    group_number_gas_list = sobj.read_dataset(0, 'GroupNumber')
    subgroup_number_gas_list = sobj.read_dataset(0, 'SubGroupNumber')
    ok = (group_number_gas_list >= 0) & (subgroup_number_gas_list < 1e8) # Remove fof particles that are not bound to subhalos
    group_number_gas_list = group_number_gas_list[ok]
    subgroup_number_gas_list = subgroup_number_gas_list[ok]
    mass_gas_list = sobj.read_dataset(0, 'Mass')[ok]
    density_gas_list = sobj.read_dataset(0,"Density")[ok]
    temperature_gas_list = sobj.read_dataset(0,"Temperature")[ok]
    metallicity_gas_list = sobj.read_dataset(0,"Metallicity")[ok]
    id_gas_list = sobj.read_dataset(0, "ParticleIDs")[ok]

    # 4 => star particles (PartType4)
    try:
        group_number_star_list = sobj.read_dataset(4, 'GroupNumber')
        subgroup_number_star_list = sobj.read_dataset(4, 'SubGroupNumber')
        ok = (group_number_star_list >= 0) & (subgroup_number_star_list < 1e8)
        group_number_star_list = group_number_star_list[ok]
        subgroup_number_star_list = subgroup_number_star_list[ok]
        mass_star_list = sobj.read_dataset(4, 'Mass')[ok]
        mass_star_init_list = sobj.read_dataset(4, 'InitialMass')[ok]
        aform_star_list = sobj.read_dataset(4,"StellarFormationTime")[ok]
        id_star_list = sobj.read_dataset(4, 'ParticleIDs')[ok]
    except:
        group_number_star_list = np.zeros(0); subgroup_number_star_list = np.zeros(0); mass_star_list = np.zeros(0); id_star_list = np.zeros(0); mass_star_init_list = np.zeros(0); aform_star_list = np.zeros(0)

    # 5 => bh particles (PartType5)
    try:
        group_number_bh_list = sobj.read_dataset(5, 'GroupNumber')
        subgroup_number_bh_list = sobj.read_dataset(5, 'SubGroupNumber')
        ok = (group_number_bh_list >= 0) & (subgroup_number_bh_list < 1e8)
        group_number_bh_list = group_number_bh_list[ok]
        subgroup_number_bh_list = subgroup_number_bh_list[ok]
        mass_bh_list = sobj.read_dataset(5, 'BH_Mass')[ok]
        nseed_bh_list = sobj.read_dataset(5, 'BH_CumlNumSeeds')[ok]
        id_bh_list = sobj.read_dataset(5, 'ParticleIDs')[ok]
    except:
        group_number_bh_list = np.zeros(0); subgroup_number_bh_list = np.zeros(0); mass_bh_list = np.zeros(0); nseed_bh_list = np.zeros(0); id_bh_list = np.zeros(0)

    sobj.close()

    gas_data = [group_number_gas_list, subgroup_number_gas_list, mass_gas_list, density_gas_list, metallicity_gas_list, id_gas_list, temperature_gas_list]
    star_data = [group_number_star_list, subgroup_number_star_list, mass_star_list, mass_star_init_list, aform_star_list, id_star_list]
    bh_data = [group_number_bh_list, subgroup_number_bh_list, mass_bh_list, nseed_bh_list, id_bh_list]
    sim_props = [h, redshift, expansion_factor, boxsize]

    return gas_data, star_data, bh_data, sim_props



def Read_All_Particle_Data(snip, xyz_max, xyz_min, sim_name, DATDIR, data_names_in, data_names_out, read_all="", verbose=True):

    # Unpack requested data name strings
    gas_names_in, dm_names_in, star_names_in, bh_names_in, all_gas_names_in, all_dm_names_in, all_star_names_in, all_bh_names_in = data_names_in
    gas_names, dm_names, star_names, bh_names, all_gas_names, all_dm_names, all_star_names, all_bh_names = data_names_out

    # Set up the class instances to store data
    gas = mpc.DataSet()
    star = mpc.DataSet()
    dm = mpc.DataSet()
    bh = mpc.DataSet()

    # read_all is a string which dictate which of the particle types you want to read in all particles (not just those that are bound)
    # e.g. read_all = "g" reads in all gas particles, "gs" reads in all gas and all stars particles - total would be "gsdb"

    path, path_all = Get_Particle_Data_Directories(DATDIR,sim_name,snip)

    if not "snap" in sim_name:
        egfile  = path+"/eagle_subfind_snip_particles"+path.split('particledata_snip')[-1]+'.0.hdf5'
    else:
        egfile  = path+"/eagle_subfind_particles"+path.split('particledata')[-1]+'.0.hdf5'

    # This object is for the bound particle files
    sobj        = re.EagleSnapshot(egfile)
    h5py_file = h5py.File(egfile)
    # This one is for all particles
    if not "snap" in sim_name:
        egfile_all = path_all+"/snip"+path_all.split('snipshot')[-1]+'.0.hdf5'
    else:
        egfile_all  = path_all+"/snap"+path_all.split('snapshot')[-1]+'.0.hdf5'
    sobj_all = re.EagleSnapshot(egfile_all)

    h = h5py_file["Header"].attrs["HubbleParam"]
    omm = h5py_file["Header"].attrs["Omega0"]

    if not "snap" in sim_name:
        redshift = float(path.split('particledata_snip_')[-1][5:].replace('p','.'))
    else:
        redshift = float(path.split('particledata_')[-1][5:].replace('p','.'))
    expansion_factor = 1. / (1.+redshift)
    boxsize = sobj.boxsize # Mpc h^-1

    # Select everything inside the desired subvolume that lies within the simulation boundaries
    vertices    = (
        xyz_min[0] , xyz_max[0],
        xyz_min[1] , xyz_max[1],
        xyz_min[2] , xyz_max[2]
        )

    sobj.select_region(*vertices)
    sobj_all.select_region(*vertices)

    # 0 => gas particles (PartType0)
    # First read bound particles
    if verbose:
        print "reading bound gas particles"

    gas.add_data(np.rint(sobj.read_dataset(0, 'GroupNumber')).astype("int"), "group_number")
    gas.add_data(np.rint(sobj.read_dataset(0, 'SubGroupNumber')).astype("int"), "subgroup_number")
    gas.add_data(np.rint(sobj.read_dataset(0, "ParticleIDs")).astype("int"), "id")

    # Read other fields that are not requested for the all particle arrays
    for name_in, name in zip(gas_names_in, gas_names):
        if name not in all_gas_names or not "g" in read_all:
            gas.add_data(sobj.read_dataset(0, name_in),name)

            if name == "coordinates":
                gas.data["coordinates"] = mpf.Periodic_Box_Check(gas.data["coordinates"], xyz_min, xyz_max, boxsize)
        #else:
        #    if not "g" in read_all:
        #        print "Error in mpio: Inconsistency between read_all and all_gas_names_in for gas"
        #        quit()

    # List of all gas data to read in (besides particle ids, group_number and subgroup_number)
    if "g" in read_all:
        if verbose:
            print "Reading all gas particles"

        # Init new instance for all gas data
        gas_all = mpc.DataSet()

        # Read all particles
        gas_all.add_data(np.rint(sobj_all.read_dataset(0, "ParticleIDs")).astype("int"), "id")

        for name_in, name in zip(all_gas_names_in,all_gas_names):
            gas_all.add_data(sobj_all.read_dataset(0, name_in), name)

            if name == "coordinates":
                gas_all.data["coordinates"] = mpf.Periodic_Box_Check(gas_all.data["coordinates"], xyz_min, xyz_max, boxsize)
            
        # Cross-match particle lists to get a master list
        gas_all.add_data(np.zeros_like(gas_all.data["id"]) - 1e9, "group_number")
        gas_all.add_data(np.zeros_like(gas_all.data["id"]) - 1e9, "subgroup_number")

        if len(gas.data["id"])>0 and len(gas_all.data["id"]) > 0:
            ptr = ms.match(gas_all.data["id"], gas.data["id"])
            gas_all.data["group_number"][ptr>=0] = gas.data["group_number"][ptr][ptr>=0]
            gas_all.data["subgroup_number"][ptr>=0] = gas.data["subgroup_number"][ptr][ptr>=0]

        # For any arrays that were read for all particles, but not bound particles, reconstruct the bound array
        rematch_gas = False
        for name in all_gas_names_in:
            if name in gas_names_in:
                rematch_gas = True
        if rematch_gas:
            if len(gas.data["group_number"]) and len(gas_all.data["group_number"])>0:
                ptr = ms.match(gas.data["id"], gas_all.data["id"])
                ok_match = ptr>= 0
                if len(ok_match) != len(ok_match[ok_match]):
                    print "Error: there are bound particles that were not read in with the all particle list"
                    quit()
                for name in all_gas_names:
                    if name in gas_names:
                        if verbose:
                            print "rematching ", name
                        gas.data[name] = gas_all.data[name][ptr]
                        gas.names.append(name)

    else:
        gas_all = None

    # 1 => dm particles (PartType1)
    # List of gas data to read in (besides particle ids, group_number and subgroup_number)

    mass_dm = h5py_file["Header"].attrs["MassTable"][1]
    dm.mass_dm = mass_dm

    # First read bound particles
    if len(dm_names_in)>0:

        if verbose:
            print "Reading bound dm particles"
        dm.add_data(np.rint(sobj.read_dataset(1, 'GroupNumber')).astype("int"), "group_number")
        dm.add_data(np.rint(sobj.read_dataset(1, 'SubGroupNumber')).astype("int"), "subgroup_number")
        dm.add_data(np.rint(sobj.read_dataset(1, "ParticleIDs")).astype("int"), "id")

        # Read other fields that are not requested for the all particle arrays
        for name_in, name in zip(dm_names_in, dm_names):
            if name not in all_dm_names:
                dm.add_data(sobj.read_dataset(1, name_in),name)

                if name == "coordinates":
                    dm.data["coordinates"] = mpf.Periodic_Box_Check(dm.data["coordinates"], xyz_min, xyz_max, boxsize)
            else:
                if not "d" in read_all:
                    print "Error in mpio: Inconsistency between read_all and all_dm_names_in for dark matter"
                    quit()

        if "d" in read_all:
            if verbose:
                print "Reading all dm particles"

            # Init new instance for all dm data
            dm_all = mpc.DataSet()
            dm_all.mass_dm = mass_dm

            # Read all particles
            dm_all.add_data(np.rint(sobj_all.read_dataset(1, "ParticleIDs")).astype("int"), "id")

            for name_in, name in zip(all_dm_names_in,all_dm_names):
                dm_all.add_data(sobj_all.read_dataset(1, name_in), name)

                if name == "coordinates":
                    dm_all.data["coordinates"] = mpf.Periodic_Box_Check(dm_all.data["coordinates"], xyz_min, xyz_max, boxsize)

            # Cross-match particle lists to get a master list
            dm_all.add_data(np.zeros_like(dm_all.data["id"]) - 1e9, "group_number")
            dm_all.add_data(np.zeros_like(dm_all.data["id"]) - 1e9, "subgroup_number")

            if len(dm.data["id"]) > 0 and len(dm_all.data["id"]) > 0:
                ptr = ms.match(dm_all.data["id"], dm.data["id"])
                dm_all.data["group_number"][ptr>=0] = dm.data["group_number"][ptr][ptr>=0]
                dm_all.data["subgroup_number"][ptr>=0] = dm.data["subgroup_number"][ptr][ptr>=0]

            # For any arrays that were read for all particles, but not bound particles, reconstruct the bound array
            rematch_dm = False
            for name in all_dm_names_in:
                if name in dm_names_in:
                    rematch_dm = True
            if rematch_dm:
                if len(dm.data["group_number"]) and len(dm_all.data["group_number"])>0:
                    ptr = ms.match(dm.data["id"], dm_all.data["id"])
                    ok_match = ptr>= 0
                    if len(ok_match) != len(ok_match[ok_match]):
                        print "Error: there are bound particles that were not read in with the all particle list"
                        quit()
                    for name in all_dm_names:
                        if name in dm_names:
                            if verbose:
                                print "rematching ", name
                            dm.data[name] = dm_all.data[name][ptr]
                            dm.names.append(name)
        else:
            dm_all = None
    else:
        dm = None; dm_all = None

    # 4 => star particles (PartType4)
    if len(star_names_in) > 0:
        try:
            if verbose:
                print "Reading bound star particles"

            # First read bound particles
            star.add_data(np.rint(sobj.read_dataset(4, 'GroupNumber')).astype("int"), "group_number")
            star.add_data(np.rint(sobj.read_dataset(4, 'SubGroupNumber')).astype("int"), "subgroup_number")
            star.add_data(np.rint(sobj.read_dataset(4, "ParticleIDs")).astype("int"), "id")

            # Read other fields that are not requested for the all particle arrays
            for name_in, name in zip(star_names_in, star_names):
                if name not in all_star_names:
                    star.add_data(sobj.read_dataset(4, name_in),name)

                    if name == "coordinates":
                        star.data["coordinates"] = mpf.Periodic_Box_Check(star.data["coordinates"], xyz_min, xyz_max, boxsize)
                else:
                    if not "s" in read_all:
                        print "Error in mpio: Inconsistency between read_all and all_star_names_in for stars"
                        quit()

            if "s" in read_all:
                if verbose:
                    print "Reading all star particles"

                # Init new instance for all star data
                star_all = mpc.DataSet()

                # Read all particles
                star_all.add_data(np.rint(sobj_all.read_dataset(4, "ParticleIDs")).astype("int"), "id")

                for star_name_in, star_name in zip(star_names_in,star_names):
                    star_all.add_data(sobj_all.read_dataset(4, star_name_in), star_name)

                    if star_name == "coordinates":
                        star_all.data["coordinates"] = mpf.Periodic_Box_Check(star_all.data["coordinates"], xyz_min, xyz_max, boxsize)

                # Cross-match particle lists to get a master list
                star_all.add_data(np.zeros_like(star_all.data["id"]) - 1e9, "group_number")
                star_all.add_data(np.zeros_like(star_all.data["id"]) - 1e9, "subgroup_number")

                if len(star.data["id"])>0 and len(star_all.data["id"])>0:
                    ptr = ms.match(star_all.data["id"], star.data["id"])
                    star_all.data["group_number"][ptr>=0] = star.data["group_number"][ptr][ptr>=0]
                    star_all.data["subgroup_number"][ptr>=0] = star.data["subgroup_number"][ptr][ptr>=0]

                # For any arrays that were read for all particles, but not bound particles, reconstruct the bound array
                rematch_star = False
                for name in all_star_names_in:
                    if name in star_names_in:
                        rematch_star = True
                if rematch_star:
                    if len(star.data["group_number"]) and len(star_all.data["group_number"])>0:
                        ptr = ms.match(star.data["id"], star_all.data["id"])
                        ok_match = ptr>= 0
                        if len(ok_match) != len(ok_match[ok_match]):
                            print "Error: there are bound particles that were not read in with the all particle list"
                            quit()
                        for name in all_star_names:
                            if name in star_names:
                                if verbose:
                                    print "rematching ", name
                                star.data[name] = star_all.data[name][ptr]
                                star.names.append(name)
            else:
                star_all = None

        except:
            star.add_data(np.zeros(0).astype("int"),"group_number")
            star.add_data(np.zeros(0).astype("int"),"subgroup_number")
            star.add_data(np.zeros(0).astype("int"),"id")
            for star_name in star_names:
                if star_name =="coordinates":
                    star.add_data(np.zeros((0,3)),star_name)
                else:
                    star.add_data(np.zeros(0),star_name)

            if "s" in read_all:
                star_all = star
            else:
                star_all = None
    else:
        star = None; star_all = None
        

    # 5 => bh particles (PartType5)

    if len(bh_names_in)>0:
        
        try:
            if verbose:
                print "Reading bound bh data"
            # First read bound particles
            bh.add_data(np.rint(sobj.read_dataset(5, 'GroupNumber')).astype("int"), "group_number")
            bh.add_data(np.rint(sobj.read_dataset(5, 'SubGroupNumber')).astype("int"), "subgroup_number")
            bh.add_data(np.rint(sobj.read_dataset(5, "ParticleIDs")).astype("int"), "id")

            # Read other fields that are not requested for the all particle arrays
            for name_in, name in zip(bh_names_in, bh_names):
                if name not in all_bh_names:
                    bh.add_data(sobj.read_dataset(5, name_in),name)

                    if name == "coordinates":
                        bh.data["coordinates"] = mpf.Periodic_Box_Check(bh.data["coordinates"], xyz_min, xyz_max, boxsize)
                else:
                    if not "s" in read_all:
                        print "Error in mpio: Inconsistency between read_all and all_bh_names_in for bhs"
                        quit()

            if "b" in read_all:
                if verbose:
                    print "Reading all bh particles"

                # Init new instance for all bh data
                bh_all = mpc.DataSet()

                # Read all particles
                bh_all.add_data(np.rint(sobj_all.read_dataset(5, "ParticleIDs")).astype("int"), "id")

                for bh_name_in, bh_name in zip(bh_names_in,bh_names):
                    bh_all.add_data(sobj_all.read_dataset(5, bh_name_in), bh_name)

                    if bh_name == "coordinates":
                        bh_all.data["coordinates"] = mpf.Periodic_Box_Check(bh_all.data["coordinates"], xyz_min, xyz_max, boxsize)

                # Cross-match particle lists to get a master list
                bh_all.add_data(np.zeros_like(bh_all.data["id"]) - 1e9, "group_number")
                bh_all.add_data(np.zeros_like(bh_all.data["id"]) - 1e9, "subgroup_number")

                if len(bh.data["id"]) > 0 and len(bh_all.data["id"])>0:
                    ptr = ms.match(bh_all.data["id"], bh.data["id"])
                    bh_all.data["group_number"][ptr>=0] = bh.data["group_number"][ptr][ptr>=0]
                    bh_all.data["subgroup_number"][ptr>=0] = bh.data["subgroup_number"][ptr][ptr>=0]

                # For any arrays that were read for all particles, but not bound particles, reconstruct the bound array
                rematch_bh = False
                for name in all_bh_names_in:
                    if name in bh_names_in:
                        rematch_bh = True
                if rematch_bh:
                    if len(bh.data["group_number"]) and len(bh_all.data["group_number"])>0:
                        ptr = ms.match(bh.data["id"], bh_all.data["id"])
                        ok_match = ptr>= 0
                        if len(ok_match) != len(ok_match[ok_match]):
                            print "Error: there are bound particles that were not read in with the all particle list"
                            quit()
                        for name in all_bh_names:
                            if name in bh_names:
                                if verbose:
                                    print "rematching ", name
                                bh.data[name] = bh_all.data[name][ptr]
                                bh.names.append(name)
            else:
                bh_all = None
        except:
            bh.add_data(np.zeros(0).astype("int"),"group_number")
            bh.add_data(np.zeros(0).astype("int"),"subgroup_number")
            bh.add_data(np.zeros(0).astype("int"),"id")
            for bh_name in bh_names:
                if bh_name =="coordinates":
                    bh.add_data(np.zeros((0,3)),bh_name)
                else:
                    bh.add_data(np.zeros(0),bh_name)

            if "b" in read_all:
                bh_all = bh
            else:
                bh_all = None
    else:
        bh = None; bh_all = None

    sobj.close()
    sobj_all.close()

    out_list = [gas, dm, star, bh]

    if "g" in read_all:
        out_list.append(gas_all)

    if "d" in read_all:
        out_list.append(dm_all)

    if "s" in read_all:
        out_list.append(star_all)
 
    if "b" in read_all:
        out_list.append(bh_all)

    sim_props = {"h":h, "redshift":redshift, "expansion_factor":expansion_factor, "boxsize":boxsize, "mass_dm":mass_dm, "omm":omm}
    out_list.append(sim_props)

    return out_list

def JobLib_Read(file_i_w_ind, names, part_types, verbose, nfiles):
    file_i, i_file = file_i_w_ind
    if verbose:
        print "Reading",i_file,"of",nfiles,file_i
    data = []

    if not os.path.isfile(file_i):
        if verbose:
            print "Couldn't find file", file_i, "moving on"
        for name in names:
            data.append([])
        return data

    File = h5py.File(file_i)

    for name, part_type in zip(names,part_types):
        #if verbose:
        #    print name, part_type
        try:
            data.append(File["PartType"+str(part_type)+"/"+name][:])
        except:
            if name == "Coordinates" or name == "Velocity":
                data.append(np.zeros((0,3)))
            else:
                data.append(np.zeros(0))

    File.close()

    return data

from joblib import Parallel, delayed
def Read_Particle_Data_JobLib(datapath, snip, sim_name, data_names_in, data_names_out, part_types, njobs=16, verbose=False):

    path, path_all = Get_Particle_Data_Directories(datapath,sim_name,snip)

    file_list = glob.glob(path+"/*")

    # Read basic simulation properties
    egfile = file_list[0]
    File = h5py.File(egfile)
    header = File["Header"]
    omm = header.attrs["Omega0"]
    h = header.attrs["HubbleParam"]
    redshift = header.attrs["Redshift"]
    expansion_factor = header.attrs["ExpansionFactor"]
    boxsize = header.attrs["BoxSize"]
    mass_dm = header.attrs["MassTable"][1]
    File.close()

    # Follow convention from earlier mpio functions that ids, grn and sgrn are not in the input names list
    temp = [0,1,4,5]
    for n_temp in temp:

        # Only read below arrays if we want to read something else also
        ok = part_types == n_temp
        if len(ok[ok]) > 0:

            data_names_in += ["GroupNumber","SubGroupNumber","ParticleIDs"]
            data_names_out += ["group_number", "subgroup_number", "id"]
            part_types = np.append(part_types, [n_temp, n_temp, n_temp])

    file_list_w_ind = []
    for i_file, file_i in enumerate(file_list):
        file_list_w_ind.append([file_i, i_file])

    # Call JobLib function to do parallel IO
    output = Parallel(n_jobs=njobs)(delayed(JobLib_Read)(file_i, data_names_in, part_types, verbose, len(file_list)) for file_i in file_list_w_ind )

    if verbose:
        print "Finished IO, now unpacking and repacking data into desired form"

    output = np.array(output)

    # Unpack the data into desired format
    nfiles = len(file_list)
    narrays = len(part_types)

    gas = mpc.DataSet()
    dm = mpc.DataSet()
    star = mpc.DataSet()
    bh = mpc.DataSet()

    # Note I effectively double the memory cost of the data here, could consider descoping as you go through to be more efficient
    for iarray in range(narrays):
        part_type = part_types[iarray]
        name = data_names_out[iarray]

        if verbose:
            print "Unpacking", name, part_type
        
        if part_type == 0:
            gas.add_data( np.concatenate(output[:,iarray]) , name)
        elif part_type == 1:
            dm.add_data( np.concatenate(output[:,iarray]) , name)
        elif part_type == 4:
            star.add_data( np.concatenate(output[:,iarray]) , name)
        elif part_type == 5:
            bh.add_data( np.concatenate(output[:,iarray]) , name)
        else:
            print "part type not recognised", part_type

    # Pack data for output
    out_list = [gas, dm, star, bh]
    sim_props = {"h":h, "redshift":redshift, "expansion_factor":expansion_factor, "boxsize":boxsize, "mass_dm":mass_dm, "omm":omm}
    out_list.append(sim_props)

    return out_list






def Read_Particle_Data_JobLib_Stars_Only(datapath, snip, sim_name, data_names_in, data_names_out, part_types, njobs=16, verbose=False):

    for part_type in part_types:
        if part_type != 4:
            print "Error: Read_Particle_Data_JobLib_Stars_Only() - can only read in part type 4, you asked for ", part_types
            quit()

    path, path_all = Get_Particle_Data_Directories(datapath,sim_name,snip)

    file_list = glob.glob(path+"/*")

    # Read basic simulation properties
    egfile = file_list[0]
    File = h5py.File(egfile)
    header = File["Header"]
    omm = header.attrs["Omega0"]
    h = header.attrs["HubbleParam"]
    redshift = header.attrs["Redshift"]
    expansion_factor = header.attrs["ExpansionFactor"]
    boxsize = header.attrs["BoxSize"]
    mass_dm = header.attrs["MassTable"][1]
    File.close()

    # Follow convention from earlier mpio functions that ids, grn and sgrn are not in the input names list
    temp = [4]
    for n_temp in temp:
        data_names_in += ["GroupNumber","SubGroupNumber"]
        data_names_out += ["group_number", "subgroup_number"]
        part_types = np.append(part_types, [n_temp, n_temp])

    file_list_w_ind = []
    for i_file, file_i in enumerate(file_list):
        file_list_w_ind.append([file_i, i_file])

    # Call JobLib function to do parallel IO
    output = Parallel(n_jobs=njobs)(delayed(JobLib_Read)(file_i, data_names_in, part_types, verbose, len(file_list)) for file_i in file_list_w_ind )

    if verbose:
        print "Finished IO, now unpacking and repacking data into desired form"

    output = np.array(output)

    # Unpack the data into desired format
    nfiles = len(file_list)
    narrays = len(part_types)

    gas = mpc.DataSet()
    dm = mpc.DataSet()
    star = mpc.DataSet()
    bh = mpc.DataSet()

    # Note I effectively double the memory cost of the data here, could consider descoping as you go through to be more efficient
    for iarray in range(narrays):
        part_type = part_types[iarray]
        name = data_names_out[iarray]

        if verbose:
            print "Unpacking", name, part_type
        
        if part_type == 0:
            gas.add_data( np.concatenate(output[:,iarray]) , name)
        elif part_type == 1:
            dm.add_data( np.concatenate(output[:,iarray]) , name)
        elif part_type == 4:
            star.add_data( np.concatenate(output[:,iarray]) , name)
        elif part_type == 5:
            bh.add_data( np.concatenate(output[:,iarray]) , name)
        else:
            print "part type not recognised", part_type

    # Pack data for output
    out_list = [gas, dm, star, bh]
    sim_props = {"h":h, "redshift":redshift, "expansion_factor":expansion_factor, "boxsize":boxsize, "mass_dm":mass_dm, "omm":omm}
    out_list.append(sim_props)

    return out_list

def Read_Star_Particle_Data_JCH(snip, xyz_max, xyz_min, sim_name, DATDIR, data_names_in, data_names_out, verbose=True):

    # Unpack requested data name strings
    star_names_in = data_names_in
    star_names = data_names_out

    # Set up the class instances to store data
    star = mpc.DataSet()

    path, path_all = Get_Particle_Data_Directories(DATDIR,sim_name,snip)

    if not "snap" in sim_name:
        egfile  = path+"/eagle_subfind_snip_particles"+path.split('particledata_snip')[-1]+'.0.hdf5'
    else:
        egfile  = path+"/eagle_subfind_particles"+path.split('particledata')[-1]+'.0.hdf5'

    # This object is for the bound particle files
    sobj        = re.EagleSnapshot(egfile)
    h5py_file = h5py.File(egfile)

    h = h5py_file["Header"].attrs["HubbleParam"]
    omm = h5py_file["Header"].attrs["Omega0"]
    mass_dm = h5py_file["Header"].attrs["MassTable"][1]

    if not "snap" in sim_name:
        redshift = float(path.split('particledata_snip_')[-1][5:].replace('p','.'))
    else:
        redshift = float(path.split('particledata_')[-1][5:].replace('p','.'))
    expansion_factor = 1. / (1.+redshift)
    boxsize = sobj.boxsize # Mpc h^-1

    # Select everything inside the desired subvolume that lies within the simulation boundaries
    vertices    = (
        xyz_min[0] , xyz_max[0],
        xyz_min[1] , xyz_max[1],
        xyz_min[2] , xyz_max[2]
        )

    sobj.select_region(*vertices)

    try:
        if verbose:
            print "Reading bound star particles"

        # First read bound particles
        star.add_data(np.rint(sobj.read_dataset(4, 'GroupNumber')).astype("int"), "group_number")
        star.add_data(np.rint(sobj.read_dataset(4, 'SubGroupNumber')).astype("int"), "subgroup_number")
        star.add_data(np.rint(sobj.read_dataset(4, "ParticleIDs")).astype("int"), "id")

        # Read other fields that are not requested for the all particle arrays
        for name_in, name in zip(star_names_in, star_names):
            star.add_data(sobj.read_dataset(4, name_in),name)

            if name == "coordinates":
                star.data["coordinates"] = mpf.Periodic_Box_Check(star.data["coordinates"], xyz_min, xyz_max, boxsize)
                
    except:
        star.add_data(np.zeros(0).astype("int"),"group_number")
        star.add_data(np.zeros(0).astype("int"),"subgroup_number")
        star.add_data(np.zeros(0).astype("int"),"id")
        for star_name in star_names:
            if star_name =="coordinates":
                star.add_data(np.zeros((0,3)),star_name)
            else:
                star.add_data(np.zeros(0),star_name)

    sobj.close()

    out_list = [star]

    sim_props = {"h":h, "redshift":redshift, "expansion_factor":expansion_factor, "boxsize":boxsize, "mass_dm":mass_dm, "omm":omm}
    out_list.append(sim_props)

    return out_list
