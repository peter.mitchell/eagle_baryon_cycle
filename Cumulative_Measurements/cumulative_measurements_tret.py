# The purpose of this script is as a post-processing step after the main analysis, walk up the merger the tree of each z=0 galaxy and calculate the integrated outflow/sfr activity
# This variant just does for the tret/rmax histograms for returning particles
# Question is do I want to do the averaging here? This makes it easier to combine the varyin tret bins by rebinning onto a much finer grid
# (It would not be sensible to store that fine grid to disk for each galaxy)
# Let's go with the pre-binning strategy for now

import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf

z_stop = 3.0# Christensen16 and AnglesAlcazar17 both start tracking at z=0, so we can copy that

test_mode = False # Write a single subvolume with a non-default nsub_1d
#catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc REF with 200 snips
sim_name = "L0025N0376_REF_200_snip"

# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(catalogue_path+filename):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename
    quit()

File = h5py.File(catalogue_path+filename)
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename)

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    redshift = z_snapshots[i_snap]
    a = 1/(1.+redshift)
    if redshift > z_stop:
        print "Stopping here"
        break

    print "building subvolumes for tree snapshot", snap

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    try:
        snapnum_group = File["Snap_"+str(snap)]
        subhalo_group = snapnum_group["subhalo_properties"]
        node_index_ts = subhalo_group["node_index"][:]
    except:
        print "No data found for",snap,snip,"skipping"
        continue

    tret_bins_ts = subhalo_group["tret_bins"][:]
    tret_mid_ts = 0.5*(tret_bins_ts[1:] + tret_bins_ts[0:-1])

    if i_snap == 1:
        sgrn_z0 = subhalo_group["subgroup_number"][:]

        central_z0 = sgrn_z0 == 0
        node_index_ts = node_index_ts[central_z0]

        m200_z0 = subhalo_group["m200_host"][:][central_z0]
        # Started implementing this but did not finish
        #r200_z0 = subhalo_group["r200_host"][:][central_z0] * a * 1e3 # kpc

        # Set the halo mass bins
        bin_mh = np.arange(10.0, 15.0,0.2)
        bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])
        n_bin_mh = len(bin_mh_mid)

        # Set the time bins
        bin_t = np.arange(0.0, 12.0, 0.1)
        n_bin_t = len(bin_t)-1
        
        ok = (np.log10(m200_z0) > bin_mh[0]) & (np.log10(m200_z0) < bin_mh[-1])
        node_index_ts = node_index_ts[ok]
        m200_z0 = m200_z0[ok]

        node_index_ns = node_index_ts
        z0_id_ns = np.arange(0, len(node_index_ns))
        z0_id_z0 = np.copy(z0_id_ns)
        z0_m200_ns = np.copy(m200_z0)

        mret_cumul = np.zeros((n_bin_mh, n_bin_t))
        mret_cumul2 = np.zeros((len(z0_id_z0), n_bin_t))

        #mret_r = subhalo_group["n_r_wind_ret"][:][central_z0][ok]
        #mret_r_cumul = np.zeros_like(mret_r)

        mret = subhalo_group["mass_t_wind_ret"][:][central_z0][ok]
        mret_flat = np.ravel(mret)

        n_halo,  n_t = mret.shape

        logm200_flat = np.repeat(np.log10(m200_z0), n_t)
        tret_mid_ts_flat = np.tile(tret_mid_ts, n_halo)

        out = np.histogram2d(logm200_flat, tret_mid_ts_flat, bins = [bin_mh,bin_t], weights=mret_flat)[0]
        mret_cumul += out

        for i_gal in range(len(z0_id_z0)):
            mret_cumul2[i_gal] = np.histogram(tret_mid_ts,bins=bin_t,weights=mret[i_gal])[0]

        continue

    descendant_index_ts = subhalo_group["descendant_index"][:]

    ptr = ms.match(descendant_index_ts,node_index_ns)
    ok_match = ptr >= 0

    z0_id_ts = np.zeros_like(descendant_index_ts)+np.nan
    z0_id_ts[ok_match] = z0_id_ns[ptr][ok_match]

    z0_m200_ts = np.zeros(len(descendant_index_ts))+np.nan
    z0_m200_ts[ok_match] = z0_m200_ns[ptr][ok_match]

    mass_new_stars_init = subhalo_group["mass_new_stars_init"][:]
    ok = (np.isnan(z0_id_ts)==False) & (np.isnan(mass_new_stars_init)==False)

    #mret_r = subhalo_group["n_r_wind_ret"][:][ok]

    mret = subhalo_group["mass_t_wind_ret"][:][ok]
    z0_m200_ts_use = z0_m200_ts[ok]
    mret_flat = np.ravel(mret)

    n_halo,  n_t = mret.shape
    
    logm200_flat = np.repeat(np.log10(z0_m200_ts_use), n_t)
    tret_mid_ts_flat = np.tile(tret_mid_ts, n_halo)

    out = np.histogram2d(logm200_flat, tret_mid_ts_flat, bins = [bin_mh,bin_t], weights=mret_flat)[0]
    mret_cumul += out

    ptr2 = ms.match(z0_id_ts[ok], z0_id_z0)
    ok_match2 = ptr2 >= 0

    for i_gal in range(len(z0_id_ts[ok][ok_match2])):
        ind = ptr2[ok_match2][i_gal]
        mret_cumul2[ind] += np.histogram(tret_mid_ts, bins=bin_t, weights=mret[ok_match2][i_gal])[0]
        #mret_r_cumul[ind] += mret_r[ok_match2][i_gal]

    node_index_ns = node_index_ts
    z0_id_ns = z0_id_ts
    z0_m200_ns = z0_m200_ts

#quit()

# Write the data to disk
print "Writing data to disk"

snapnum_group = File["Snap_"+str(snapshots[1:].max())]

group_name = "all_progen_tracked_properties"
if group_name in snapnum_group:
    subhalo_group = snapnum_group[group_name]
else:
    subhalo_group = snapnum_group.create_group(group_name)

name_list = ["mass_recool_m200_t_binned", "m200_bins", "t_bins","mass_recool_t_binned","m200"]
for name in name_list:
    for thing in subhalo_group:
        if thing == name:
            del subhalo_group[name]

subhalo_group.create_dataset("m200_bins",data=bin_mh)
subhalo_group.create_dataset("m200",data=m200_z0)
subhalo_group.create_dataset("t_bins",data=bin_t)
subhalo_group.create_dataset("mass_recool_m200_t_binned",data=mret_cumul)
subhalo_group.create_dataset("mass_recool_t_binned",data=mret_cumul2)

File.close()
