# The purpose of this script is as a post-processing step after the main analysis, walk up the merger the tree of each z=0 galaxy and calculate the integrated outflow/sfr activity
# This script is an offshoot of cumulative_measurements in that the integration is done over finite zbins (and then written to catalogue of th elowest redshift in each z bin)
# This allows (for example) median mass loading factors to be defined over a finite redshift bin, irrespective of the snapshot resolution employed
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf

fVmax_cut = 0.25
h=0.6777

test_mode = False # Write a single subvolume with a non-default nsub_1d
#catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc Recal with 200 snips
#sim_name = "L0025N0752_Recal"

# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# 50 Mpc REF model, 28 snapshots
#sim_name = "L0050N0752_REF_snap"

# Set values of ln(expansion factor) that will define the centre of each redshift bin
# Note this should match what I am using in build grid
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])
nbins = len(lna_mid_choose)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(catalogue_path+filename):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename
    quit()

File = h5py.File(catalogue_path+filename,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Work out which redshift bin each snapshot is in
a_snapshots = 1./(1.+z_snapshots)
lna_snapshots = np.log(a_snapshots)

izbins = np.zeros_like(a_snapshots)
for i, lna in enumerate(lna_snapshots):
    ind = np.argmin(abs(lna-lna_mid_choose))
    izbins[i] = ind

zlo_bins = np.zeros_like(lna_mid_choose); zhi_bins = np.zeros_like(lna_mid_choose)
for i in range(nbins):
    zlo_bins[i] = z_snapshots[i == izbins].min()
    zhi_bins[i] = z_snapshots[i == izbins].max()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename)

i_bin = 0
mass_stars_init_cumul_list = []
mass_ism_wind_cumul_list = []
mZ_ism_wind_cumul_list = []
mass_ism_cumul_list = []
mZ_ism_cumul_list = []
mchalo_cumul_list = []
sgrn_cumul_list = []
sfr_cumul_list = []
mstar_cumul_list = []
vcirc_cumul_list = []
vmax_cumul_list = []
        
################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]
    a = a_snapshots[i_snap]

    print "building subvolumes for tree snapshot", snap

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    # Read the data
    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    try:
        data_ok = True
        node_index_ts = np.rint(subhalo_group["node_index"][:]).astype("int")
    except:
        data_ok = False
        print "No data for this snap"

    if data_ok:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
        mass_join_ism_wind = subhalo_group["mass_sf_ism_join_wind"+fV_str][:] + subhalo_group["mass_ism_reheated_join_wind"+fV_str][:] + subhalo_group["mass_nsf_ism_join_wind"+fV_str][:]
        mZ_join_ism_wind = subhalo_group["mZ_sf_ism_join_wind"+fV_str][:] + subhalo_group["mZ_ism_reheated_join_wind"+fV_str][:] + subhalo_group["mZ_nsf_ism_join_wind"+fV_str][:]

        mass_new_stars_init = subhalo_group["mass_new_stars_init"][:]
        mass_ism = subhalo_group["mass_gas_SF"][:] + subhalo_group["mass_gas_NSF_ISM"][:]
        mZ_ism = subhalo_group["mZ_gas_SF"][:] + subhalo_group["mZ_gas_NSF_ISM"][:]

        mchalo = subhalo_group["m200_host"][:]
        subgroup_number = subhalo_group["subgroup_number"][:]

        sfr = subhalo_group["mass_new_stars_init_100_30kpc"][:] / 100 / 1e6 # Msun yr^-1                                                                                                                                                          
        mstars = subhalo_group["mass_stars_30kpc"][:] # Msun

        g2Msun = 1./(1.989e33)
        vmax = subhalo_group["vmax"][:]
        G = 6.674e-11 # m^3 kg^-1 s-2                                                                                                                                                                                                     
        Msun = 1.989e30 # kg                                                                                                                                                                                                              
        Mpc = 3.0857e22
        vcirc = np.sqrt(G * subhalo_group["m200_host"][:]*Msun / (subhalo_group[ "r200_host"][:]*Mpc/h*a)) / 1e3

        if i_snap == 1 or izbins[i_snap] != izbins[i_snap-1]:
            print "Starting a new redshift bin"
            node_index_ns = node_index_ts
            z0_id_ns = np.arange(0, len(node_index_ns))
            z0_id_z0 = np.copy(z0_id_ns)
            mass_stars_init_cumul = np.copy(mass_new_stars_init)
            mass_ism_wind_cumul = np.copy(mass_join_ism_wind)
            mZ_ism_wind_cumul = np.copy(mZ_join_ism_wind)
            mass_ism_cumul = np.copy(mass_ism)
            mZ_ism_cumul = np.copy(mZ_ism)

            mchalo_cumul_list.append(mchalo)
            sgrn_cumul_list.append(subgroup_number)
            sfr_cumul_list.append(sfr)
            mstar_cumul_list.append(mstars)
            vcirc_cumul_list.append(vcirc)
            vmax_cumul_list.append(vmax)

            continue

        descendant_index_ts = np.rint(subhalo_group["descendant_index"][:]).astype("int")

        ptr = ms.match(descendant_index_ts,node_index_ns)
        ok_match = ptr >= 0

        z0_id_ts = np.zeros_like(descendant_index_ts)+np.nan
        z0_id_ts[ok_match] = z0_id_ns[ptr][ok_match]

        ok = (np.isnan(z0_id_ts)==False) & (np.isnan(mass_join_ism_wind)==False)

        mass_stars_init_cumul_ts, mass_ism_wind_cumul_ts, mZ_ism_wind_cumul_ts, mass_ism_cumul_ts, mZ_ism_cumul_ts = mpf.Sum_Common_ID([mass_new_stars_init[ok], mass_join_ism_wind[ok], mZ_join_ism_wind[ok], mass_ism[ok], mZ_ism[ok]], z0_id_ts[ok], z0_id_z0)
        mass_stars_init_cumul += mass_stars_init_cumul_ts
        mass_ism_wind_cumul += mass_ism_wind_cumul_ts
        mZ_ism_wind_cumul += mZ_ism_wind_cumul_ts
        mass_ism_cumul += mass_ism_cumul_ts
        mZ_ism_cumul += mZ_ism_cumul_ts

        node_index_ns = node_index_ts
        z0_id_ns = z0_id_ts

    if snap == snapshots.min()+1 or izbins[i_snap+1] != izbins[i_snap]:
        print "Consolidating this redshift bin"

        mass_stars_init_cumul_list.append(mass_stars_init_cumul)
        mass_ism_wind_cumul_list.append(mass_ism_wind_cumul)
        mZ_ism_wind_cumul_list.append(mZ_ism_wind_cumul)
        mass_ism_cumul_list.append(mass_ism_cumul)
        mZ_ism_cumul_list.append(mZ_ism_cumul)
    
        i_bin += 1

# Write the data to disk
print "Writing data to disk"

snapnum_group = File["Snap_"+str(snapshots[1:].max())]
subhalo_group = snapnum_group["subhalo_properties"]

for i_bin in range(nbins):

    zlo_str = ("%3.1f" % zlo_bins[i_bin]).replace(".","p")
    zhi_str = ("%3.1f" % zhi_bins[i_bin]).replace(".","p")
    z_str = "_z"+zlo_str+"_"+zhi_str

    name_list = ["mass_ism_wind_cumul_pp"+fV_str+z_str, "mass_stars_init_cumul_pp"+z_str, "mchalo_cumul_pp"+z_str, "sgrn_cumul_pp"+z_str, "vcirc_cumul_pp"+z_str, "vmax_cumul_pp"+z_str, "sfr_cumul_pp"+z_str, "mstar_cumul_pp"+z_str,"mZ_ism_wind_cumul_pp"+fV_str+z_str, "mass_ism_cumul_pp"+fV_str+z_str, "mZ_ism_cumul_pp"+fV_str+z_str]
    for name in name_list:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]

    mass_ism_wind_cumul = mass_ism_wind_cumul_list[i_bin]
    mZ_ism_wind_cumul = mZ_ism_wind_cumul_list[i_bin]
    mass_ism_cumul = mass_ism_cumul_list[i_bin]
    mZ_ism_cumul = mZ_ism_cumul_list[i_bin]
    mass_stars_init_cumul = mass_stars_init_cumul_list[i_bin]
    mchalo = mchalo_cumul_list[i_bin]
    sgrn = sgrn_cumul_list[i_bin]
    mstar = mstar_cumul_list[i_bin]
    sfr = sfr_cumul_list[i_bin]
    vcirc = vcirc_cumul_list[i_bin]
    vmax = vmax_cumul_list[i_bin]

    subhalo_group.create_dataset("mass_ism_wind_cumul_pp"+fV_str+z_str,data=mass_ism_wind_cumul)
    subhalo_group.create_dataset("mZ_ism_wind_cumul_pp"+fV_str+z_str,data=mZ_ism_wind_cumul)
    subhalo_group.create_dataset("mass_ism_cumul_pp"+fV_str+z_str,data=mass_ism_cumul)
    subhalo_group.create_dataset("mZ_ism_cumul_pp"+fV_str+z_str,data=mZ_ism_cumul)
    subhalo_group.create_dataset("mass_stars_init_cumul_pp"+z_str,data=mass_stars_init_cumul)

    subhalo_group.create_dataset("mchalo_cumul_pp"+z_str,data=mchalo)
    subhalo_group.create_dataset("sgrn_cumul_pp"+z_str,data=sgrn)
    subhalo_group.create_dataset("mstar_cumul_pp"+z_str,data=mstar)
    subhalo_group.create_dataset("sfr_cumul_pp"+z_str,data=sfr)
    subhalo_group.create_dataset("vcirc_cumul_pp"+z_str,data=vcirc)
    subhalo_group.create_dataset("vmax_cumul_pp"+z_str,data=vmax)

File.close()
