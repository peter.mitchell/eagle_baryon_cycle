import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

test_mode = False
use_aperture = True

star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

halo_mass = "m200"

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0050N0752_REF_snap"
#sim_name = "L0025N0376_REF_snap"
sim_name = "L0025N0376_REF_200_snip"


omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units (AT FIDUCIAL EAGLE RESOLUTION ONLY!)
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])
nbins = len(lna_mid_choose)

filename = "subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File_cat = h5py.File(catalogue_path+filename,"r")
tree_snapshot_info = File_cat["tree_snapshot_info"]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
File_cat.close()

snapshots = snapshot_numbers

a = 1./(1.+z_snapshots)
lna_snapshots = np.log(a)

izbins = np.zeros_like(a)
for i, lna in enumerate(lna_snapshots):
    ind = np.argmin(abs(lna-lna_mid_choose))
    izbins[i] = ind

zlo_bins = np.zeros_like(lna_mid_choose); zhi_bins = np.zeros_like(lna_mid_choose)
for i in range(nbins):
    zlo_bins[i] = z_snapshots[i == izbins].min()
    zhi_bins[i] = z_snapshots[i == izbins].max()

t, tlb = uc.t_Universe(a, omm, h)


filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

fVmax_cut = 0.25
fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

f_name_list = ["ism_wind_Zratio_cum_pp_med"+fV_str, "ism_wind_Zratio_cum_pp_lo"+fV_str, "ism_wind_Zratio_cum_pp_hi"+fV_str, "ism_wind_Zratio_cum_pp_complete"+fV_str, "ism_wind_Zratio_cum_pp_complete2"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = np.zeros((nbins,len(bin_mh)-1))

subhalo_group = File["Snap_"+str(snapshots[1:].max())+"/subhalo_properties"] # _pp data is written for the last snapshot where there are measurements (which due to _ns is not the final snapshot)

for i in range(nbins):
    
    zlo_str = ("%3.1f" % zlo_bins[i]).replace(".","p")
    zhi_str = ("%3.1f" % zhi_bins[i]).replace(".","p")
    z_str = "_z"+zlo_str+"_"+zhi_str

    Z_wind = subhalo_group["mZ_ism_wind_cumul_pp"+fV_str+z_str][:] /subhalo_group["mass_ism_wind_cumul_pp"+fV_str+z_str][:]
    Z_ism = subhalo_group["mZ_ism_cumul_pp"+fV_str+z_str][:] /subhalo_group["mass_ism_cumul_pp"+fV_str+z_str][:]

    # For the metallicity ratio, we are allowing Z_wind = 0 into the median; so to be fair I should allow Z_ism = 0 also
    correct = (Z_ism==0) & ((np.isnan(Z_wind)==False)&(Z_wind>0))
    Z_ism[correct] += 1e9

    mchalo = subhalo_group["mchalo_cumul_pp"+z_str][:]
    subgroup_number = subhalo_group["sgrn_cumul_pp"+z_str][:]

    cent = subgroup_number == 0

    f_med_dict["ism_wind_Zratio_cum_pp_med"+fV_str][i], f_med_dict["ism_wind_Zratio_cum_pp_lo"+fV_str][i], f_med_dict["ism_wind_Zratio_cum_pp_hi"+fV_str][i],junk,f_med_dict["ism_wind_Zratio_cum_pp_complete"+fV_str][i] = \
        us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),Z_wind[cent]/Z_ism[cent],np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)
    # This "complete2" measurement is here so I can distinguish complete = 0 cases between - no galaxies in bin versus <80% galaxies with +ve sfr
    junk, junk, junk, junk, f_med_dict["ism_wind_Zratio_cum_pp_complete2"+fV_str][i] = \
        us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),np.ones_like(mchalo[cent]),np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_cumul_zbins_metal_flows"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)

for f_name in f_name_list:

    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

File_grid.close()
