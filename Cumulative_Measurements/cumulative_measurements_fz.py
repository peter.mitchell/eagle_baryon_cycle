# The purpose of this script is as a post-processing step after the main analysis
#, walk up the merger the tree of each z=0 galaxy and calculate the integrated outflow/sfr activity
# This version saves the redshift evolution in 2d arrays, integrating along all progenitors of the final halo

import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf

fVmax_cut = 0.25
do_dm = True

test_mode = False # Write a single subvolume with a non-default nsub_1d
#catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"

# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(catalogue_path+filename):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename
    quit()

File = h5py.File(catalogue_path+filename)
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename)

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "building subvolumes for tree snapshot", snap

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    try:
        # Read the data
        snapnum_group = File["Snap_"+str(snap)]
        subhalo_group = snapnum_group["subhalo_properties"]
        node_index_ts = subhalo_group["node_index"][:]
    except:
        print "No data for this snap, stopping here"
        break

    track_names = ["mass_praccreted"]
    if do_dm:
        track_names += ["mass_diffuse_dm_pristine"]

    if i_snap == 1:
        node_index_ns = node_index_ts
        z0_id_ns = np.arange(0, len(node_index_ns))
        z0_id_z0 = np.copy(z0_id_ns)

        mass_evo_dict = {}
        for name in track_names:
            mass_evo_dict[name]  = np.zeros((len(snapshots), len(z0_id_ns)))

            mass_ts = subhalo_group[name][:]
            mass_evo_dict[name][:2] = mass_ts

        continue

    descendant_index_ts = subhalo_group["descendant_index"][:]

    ptr = ms.match(descendant_index_ts,node_index_ns)
    ok_match = ptr >= 0

    z0_id_ts = np.zeros_like(descendant_index_ts)+np.nan
    z0_id_ts[ok_match] = z0_id_ns[ptr][ok_match]

    mass_star = subhalo_group["mass_star"][:]
    ok = (np.isnan(z0_id_ts)==False) & (np.isnan(mass_star)==False)

    mass_ts_dict = {}
    for name in track_names:
        mass_ts_dict[name] = subhalo_group[name][:][ok]

    delta_mass_dict = mpf.Sum_Common_ID(mass_ts_dict, z0_id_ts[ok], z0_id_z0,use_dict=True,dict_names=track_names)

    for name in track_names:
        mass_evo_dict[name][:i_snap+1] += delta_mass_dict[name]

    node_index_ns = node_index_ts
    z0_id_ns = z0_id_ts

for name in track_names:
    mass_evo_dict[name] = np.swapaxes(mass_evo_dict[name],0,1)

from utilities_plotting import *
y1 = mass_evo_dict["mass_praccreted"]
y2 = mass_evo_dict["mass_diffuse_dm_pristine"]*0.17

# Write the data to disk
print "Writing data to disk"

snapnum_group = File["Snap_"+str(snapshots[1:].max())]

group_name = "all_progen_tracked_properties"
if group_name in snapnum_group:
    subhalo_group = snapnum_group[group_name]
else:
    subhalo_group = snapnum_group.create_group(group_name)

for name in track_names:
    for thing in subhalo_group:
        if thing == name:
            del subhalo_group[name]

    subhalo_group.create_dataset(name, data=mass_evo_dict[name])

File.close()
