import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

test_mode = False
use_aperture = True

star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

halo_mass = "m200"

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")

# 50 Mpc REF with 28 snaps
#sim_name = "L0050N0752_REF_snap"
#tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/tree_028.0.hdf5"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]
#snip_list = np.arange(0,snap_final+1)[::-1]

# 25 Mpc REF with 28 snaps
'''sim_name = "L0025N0376_REF_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 28 snaps par test
'''sim_name = "L0025N0376_REF_snap_par_test"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
snap_final = 100
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/tree_100.0.hdf5"
snip_list = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
               73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
               145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
               217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
               289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
               361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_final = 200
snap_list = np.arange(1,snap_final+1)[::-1] # These trees start at 1
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]'''

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/tree_299.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 400 snapshots
'''sim_name = "L0025N0376_REF_400_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/tree_399.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 500 snapshots
#sim_name = "L0025N0376_REF_500_snap"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version"
'''sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/tree_499.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 1000 snapshots
'''sim_name = "L0025N0376_REF_1000_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/tree_999.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 50 Mpc no-AGN 28 snaps
#sim_name = "L0050N0752_NOAGN_snap"
#tree_file = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/tree_028.0.hdf5"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]
#snip_list = np.copy(snap_list)

# 25 Mpc Recal 200 snips
'''sim_name = "L0025N0752_Recal"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/tree_202.0.hdf5"
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snip_list = np.rint(snipshots[::-1]).astype("int")
snap_list = np.rint(snapshots[::-1]).astype("int")'''

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()


omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units (AT FIDUCIAL EAGLE RESOLUTION ONLY!)
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1))
bin_mstar_mid = 0.5*(bin_mstar[1:]+bin_mstar[0:-1])
bin_vmax = np.arange(1.4, 3.0,(3.0-1.4)/(n_bins+1))
bin_vmax_mid = 0.5*(bin_vmax[1:]+bin_vmax[0:-1])
bin_vcirc = np.arange(1.4, 2.8,(2.8-1.4)/(n_bins+1))
bin_vcirc_mid = 0.5*(bin_vcirc[1:]+bin_vcirc[0:-1])
bin_sfr = np.arange(-3.0, 2.0,(5.0)/(n_bins+1))
bin_sfr_mid = 0.5*(bin_sfr[1:]+bin_sfr[0:-1])
bin_ssfr = np.arange(-3.0, 2.0,(5.0)/(n_bins+1))
bin_ssfr_mid = 0.5*(bin_ssfr[1:]+bin_ssfr[0:-1])

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])
nbins = len(lna_mid_choose)

File_tree = h5py.File(tree_file,"r")
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

lna_snapshots = np.log(a)
z_snapshots = 1./a-1.

izbins = np.zeros_like(a)
for i, lna in enumerate(lna_snapshots):
    ind = np.argmin(abs(lna-lna_mid_choose))
    izbins[i] = ind

zlo_bins = np.zeros_like(lna_mid_choose); zhi_bins = np.zeros_like(lna_mid_choose)
for i in range(nbins):
    zlo_bins[i] = z_snapshots[i == izbins].min()
    zhi_bins[i] = z_snapshots[i == izbins].max()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()


filename = "processed_subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

fVmax_cut = 0.25
fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

f_name_list = ["ism_wind_cum_ml_pp"+fV_str, "ism_wind_cum_ml_pp_med"+fV_str, "ism_wind_cum_ml_pp_lo"+fV_str, "ism_wind_cum_ml_pp_hi"+fV_str, "ism_wind_cum_ml_pp_complete"+fV_str]
f_name_list += ["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar","ism_wind_cum_ml_pp_complete"+fV_str+"_sfr","ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc","ism_wind_cum_ml_pp_complete"+fV_str+"_vmax"]
f_name_list += ["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar","ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr","ism_wind_cum_ml_pp_complete2"+fV_str+"_vcirc","ism_wind_cum_ml_pp_complete2"+fV_str+"_vmax","ism_wind_cum_ml_pp_complete2"+fV_str]
f_med_dict = {}

for f_name in f_name_list:
    f_med_dict[f_name] = np.zeros((nbins,len(bin_mh)-1))

subhalo_group = File["Snap_"+str(snap_list[1:].max())+"/subhalo_properties"] # _pp data is written for the last snapshot where there are measurements (which due to _ns is not the final snapshot)

for i in range(nbins):
    
    zlo_str = ("%3.1f" % zlo_bins[i]).replace(".","p")
    zhi_str = ("%3.1f" % zhi_bins[i]).replace(".","p")
    z_str = "_z"+zlo_str+"_"+zhi_str

    mass_out = subhalo_group["mass_ism_wind_cumul_pp"+fV_str+z_str][:]
    sfr_cumul = subhalo_group["mass_stars_init_cumul_pp"+z_str][:]

    mchalo = subhalo_group["mchalo_cumul_pp"+z_str][:]
    subgroup_number = subhalo_group["sgrn_cumul_pp"+z_str][:]
    sfr = subhalo_group["sfr_cumul_pp"+z_str][:]
    mstars = subhalo_group["mstar_cumul_pp"+z_str][:]
    vcirc = subhalo_group["vcirc_cumul_pp"+z_str][:]
    vmax = subhalo_group["vmax_cumul_pp"+z_str][:]

    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)

        f_med_dict["ism_wind_cum_ml_pp"+fV_str][i,j_bin] = np.sum(mass_out[ok_cent]) / np.sum(sfr_cumul[ok_cent]) # Dimensionless

    cent = subgroup_number == 0
    f_med_dict["ism_wind_cum_ml_pp_med"+fV_str][i], f_med_dict["ism_wind_cum_ml_pp_lo"+fV_str][i], f_med_dict["ism_wind_cum_ml_pp_hi"+fV_str][i],junk,f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][i] = \
        us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/sfr_cumul[cent],np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)
    # This "complete2" measurement is here so I can distinguish complete = 0 cases between - no galaxies in bin versus <80% galaxies with +ve sfr
    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][i] = \
        us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),np.ones_like(mass_out[cent]),np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)

    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][i] = \
        us.Calculate_Percentiles(bin_mstar,np.log10(mstars[cent]),mass_out[cent]/sfr_cumul[cent],np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)
    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][i] = \
        us.Calculate_Percentiles(bin_mstar,np.log10(mstars[cent]),np.ones_like(mass_out[cent]),np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)

    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_sfr"][i] = \
        us.Calculate_Percentiles(bin_sfr,np.log10(sfr[cent]),mass_out[cent]/sfr_cumul[cent],np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)
    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr"][i] = \
        us.Calculate_Percentiles(bin_sfr,np.log10(sfr[cent]),np.ones_like(mass_out[cent]),np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)

    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc"][i] = \
        us.Calculate_Percentiles(bin_vcirc,np.log10(vcirc[cent]),mass_out[cent]/sfr_cumul[cent],np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)
    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_vcirc"][i] = \
        us.Calculate_Percentiles(bin_vcirc,np.log10(vcirc[cent]),np.ones_like(mass_out[cent]),np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)

    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_vmax"][i] = \
        us.Calculate_Percentiles(bin_vmax,np.log10(vmax[cent]),mass_out[cent]/sfr_cumul[cent],np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)
    junk, junk, junk, junk, f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_vmax"][i] = \
        us.Calculate_Percentiles(bin_vmax,np.log10(vmax[cent]),np.ones_like(mass_out[cent]),np.ones_like(cent[cent]),1,lohi=(0.16,0.84),complete=0.8)

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_cumul_zbins"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_mstar_grid", data=bin_mstar_mid)
File_grid.create_dataset("log_vmax_grid", data=bin_vmax_mid)
File_grid.create_dataset("log_vcirc_grid", data=bin_vcirc_mid)
File_grid.create_dataset("log_sfr_grid", data=bin_sfr_mid)
File_grid.create_dataset("log_ssfr_grid", data=bin_ssfr_mid)

for f_name in f_name_list:

    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

File_grid.close()
