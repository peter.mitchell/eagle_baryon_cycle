# The purpose of this script is as a post-processing step after the main analysis, walk up the merger the tree of each z=0 galaxy and calculate the integrated outflow/sfr activity

import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf

fVmax_cut = 0.25
do_dm = True
track_star = True

test_mode = False # Write a single subvolume with a non-default nsub_1d
#catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip_track_star"

# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

if not os.path.exists(catalogue_path+filename_subcat):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename_subcat
    quit()

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename)

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "building subvolumes for tree snapshot", snap

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    try:
    #if True:
        # Read the data
        snapnum_group = File["Snap_"+str(snap)]

        subhalo_group = snapnum_group["subhalo_properties"]

        node_index_ts = np.rint(subhalo_group["node_index"][:]).astype("int")

        if i_snap == 1:
            node_index_ns = node_index_ts
            z0_id_ns = np.arange(0, len(node_index_ns))
            z0_id_z0 = np.copy(z0_id_ns)
            mass_stars_init_cumul = np.zeros(len(z0_id_ns))
            mass_ism_wind_cumul = np.zeros(len(z0_id_ns))
            mass_prcooled_cumul = np.zeros(len(z0_id_ns))
            mass_recooled_cumul = np.zeros(len(z0_id_ns))
            mass_recooled_ireheat_cumul = np.zeros(len(z0_id_ns))
            mass_recooled_from_mp_cumul = np.zeros(len(z0_id_ns))
            mass_galtran_cumul = np.zeros(len(z0_id_ns))
            mass_merge_ism_cumul = np.zeros(len(z0_id_ns))
            mass_merge_cgm_cumul = np.zeros(len(z0_id_ns))
            mass_pracc_cumul = np.zeros(len(z0_id_ns))
            if do_dm:
                mass_pracc_dm_cumul = np.zeros(len(z0_id_ns))
            if track_star:
                sfr_prcooled_cumul = np.zeros(len(z0_id_ns))
                sfr_recooled_cumul = np.zeros(len(z0_id_ns))
                sfr_recooled_from_mp_cumul = np.zeros(len(z0_id_ns))
                sfr_galtran_cumul = np.zeros(len(z0_id_ns))
                sfr_merge_cumul = np.zeros(len(z0_id_ns))
            continue

        descendant_index_ts = np.rint(subhalo_group["descendant_index"][:]).astype("int")

        ptr = ms.match(descendant_index_ts,node_index_ns)
        ok_match = ptr >= 0

        z0_id_ts = np.zeros_like(descendant_index_ts)+np.nan
        z0_id_ts[ok_match] = z0_id_ns[ptr][ok_match]

        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
        mass_join_ism_wind = subhalo_group["mass_sf_ism_join_wind"+fV_str][:] + subhalo_group["mass_ism_reheated_join_wind"+fV_str][:] + subhalo_group["mass_nsf_ism_join_wind"+fV_str][:]
        mass_new_stars_init = subhalo_group["mass_new_stars_init"][:]

        mass_pracc = subhalo_group["mass_praccreted"][:]
        mass_prcooled = subhalo_group["mass_prcooled"][:]
        mass_recooled = subhalo_group["mass_recooled"+fV_str][:]
        mass_recooled_ireheat = subhalo_group["mass_recooled_ireheat"][:]
        mass_recooled_ireheat += subhalo_group["mass_recooled_0p5vmax"][:]

        mass_recooled_from_mp = subhalo_group["mass_recooled_from_mp"+fV_str][:]
        mass_galtran = subhalo_group["mass_galtran"][:]
        mass_merge_ism = subhalo_group["mass_gmerged_ISM"][:]
        mass_merge_cgm = subhalo_group["mass_gmerged_CGM"][:]

        if track_star:
            sfr_prcooled = subhalo_group["mass_new_stars_init_prcooled"][:]
            sfr_recooled = subhalo_group["mass_new_stars_init_recooled"][:]
            sfr_recooled_from_mp = subhalo_group["mass_new_stars_init_recooled_from_mp"][:]
            sfr_galtran = subhalo_group["mass_new_stars_init_galtran"][:]
            sfr_merge = subhalo_group["mass_new_stars_init_merge"][:]

        if do_dm:
            mass_pracc_dm = subhalo_group["mass_diffuse_dm_pristine"][:]

        ok = (np.isnan(z0_id_ts)==False) & (np.isnan(mass_join_ism_wind)==False)

        if do_dm:
            mass_stars_init_cumul_ts, mass_ism_wind_cumul_ts, mass_pracc_ts, mass_prcooled_ts, mass_recooled_ts, mass_recooled_ireheat_ts, mass_recooled_from_mp_ts, mass_galtran_ts, mass_merge_ism_ts, mass_merge_cgm_ts, mass_pracc_dm_ts = mpf.Sum_Common_ID([mass_new_stars_init[ok], mass_join_ism_wind[ok], mass_pracc[ok], mass_prcooled[ok], mass_recooled[ok], mass_recooled_ireheat[ok], mass_recooled_from_mp[ok], mass_galtran[ok], mass_merge_ism[ok], mass_merge_cgm[ok], mass_pracc_dm[ok]], z0_id_ts[ok], z0_id_z0)
        elif track_star:
            mass_stars_init_cumul_ts, mass_ism_wind_cumul_ts, mass_pracc_ts, mass_prcooled_ts, mass_recooled_ts, mass_recooled_ireheat_ts, mass_recooled_from_mp_ts, mass_galtran_ts, mass_merge_ism_ts, mass_merge_cgm_ts, sfr_prcooled_ts, sfr_recooled_ts, sfr_recooled_from_mp_ts, sfr_galtran_ts, sfr_merge_ts = mpf.Sum_Common_ID([mass_new_stars_init[ok], mass_join_ism_wind[ok], mass_pracc[ok], mass_prcooled[ok], mass_recooled[ok], mass_recooled_ireheat[ok], mass_recooled_from_mp[ok], mass_galtran[ok], mass_merge_ism[ok], mass_merge_cgm[ok], sfr_prcooled[ok], sfr_recooled[ok], sfr_recooled_from_mp[ok], sfr_galtran[ok], sfr_merge[ok]], z0_id_ts[ok], z0_id_z0)
        else:
            mass_stars_init_cumul_ts, mass_ism_wind_cumul_ts, mass_pracc_ts, mass_prcooled_ts, mass_recooled_ts, mass_recooled_ireheat_ts, mass_galtran_ts = mpf.Sum_Common_ID([mass_new_stars_init[ok], mass_join_ism_wind[ok], mass_pracc[ok], mass_prcooled[ok], mass_recooled[ok], mass_recooled_ireheat[ok], mass_recooled_from_mp[ok], mass_galtran[ok], mass_merge_ism[ok], mass_merge_cgm[ok]], z0_id_ts[ok], z0_id_z0)
        mass_stars_init_cumul += mass_stars_init_cumul_ts
        mass_ism_wind_cumul += mass_ism_wind_cumul_ts

        mass_pracc_cumul += mass_pracc_ts
        mass_prcooled_cumul += mass_prcooled_ts
        mass_recooled_cumul += mass_recooled_ts
        mass_recooled_ireheat_cumul += mass_recooled_ireheat_ts
        mass_recooled_from_mp_cumul += mass_recooled_from_mp_ts
        mass_galtran_cumul += mass_galtran_ts
        mass_merge_ism_cumul += mass_merge_ism_ts
        mass_merge_cgm_cumul += mass_merge_cgm_ts
        if do_dm:
            mass_pracc_dm_cumul += mass_pracc_dm_ts
        if track_star:
            sfr_prcooled_cumul += sfr_prcooled_ts
            sfr_recooled_cumul += sfr_recooled_ts
            sfr_recooled_from_mp_cumul += sfr_recooled_ts
            sfr_galtran_cumul += sfr_galtran_ts
            sfr_merge_cumul += sfr_merge_ts
         
        node_index_ns = node_index_ts
        z0_id_ns = z0_id_ts
        
    except:
        print "No data found for",snap,snip,"skipping"

# Write the data to disk
print "Writing data to disk"

snapnum_group = File["Snap_"+str(snapshots[1:].max())]
subhalo_group = snapnum_group["subhalo_properties"]

name_list = ["mass_ism_wind_cumul_pp"+fV_str, "mass_stars_init_cumul_pp"]
name_list += ["mass_prcooled_cumul_pp", "mass_recooled_cumul_pp"+fV_str,"mass_recooled_from_mp_cumul_pp"+fV_str, "mass_galtran_cumul_pp", "mass_merge_ism_cumul_pp", "mass_merge_cgm_cumul_pp", "mass_pracc_cumul_pp", "mass_pracc_dm_cumul_pp", "sfr_prcooled_cumul_pp", "sfr_recooled_cumul_pp", "sfr_recooled_from_mp_cumul_pp", "sfr_galtran_cumul_pp", "sfr_merge_cumul_pp"]
name_list += ["mass_recooled_ireheat_cumul_pp"]
for name in name_list:
    for thing in subhalo_group:
        if thing == name:
            del subhalo_group[name]

subhalo_group.create_dataset("mass_ism_wind_cumul_pp"+fV_str,data=mass_ism_wind_cumul)
subhalo_group.create_dataset("mass_stars_init_cumul_pp",data=mass_stars_init_cumul)
subhalo_group.create_dataset("mass_pracc_cumul_pp",data=mass_pracc_cumul)
subhalo_group.create_dataset("mass_prcooled_cumul_pp",data=mass_prcooled_cumul)
subhalo_group.create_dataset("mass_recooled_cumul_pp"+fV_str,data=mass_recooled_cumul)
subhalo_group.create_dataset("mass_recooled_ireheat_cumul_pp",data=mass_recooled_ireheat_cumul)
subhalo_group.create_dataset("mass_recooled_from_mp_cumul_pp"+fV_str,data=mass_recooled_from_mp_cumul)
subhalo_group.create_dataset("mass_galtran_cumul_pp",data=mass_galtran_cumul)
subhalo_group.create_dataset("mass_merge_ism_cumul_pp",data=mass_merge_ism_cumul)
subhalo_group.create_dataset("mass_merge_cgm_cumul_pp",data=mass_merge_cgm_cumul)
if do_dm:
    subhalo_group.create_dataset("mass_pracc_dm_cumul_pp",data=mass_pracc_dm_cumul)
if track_star:
    subhalo_group.create_dataset("sfr_prcooled_cumul_pp",data=sfr_prcooled_cumul)
    subhalo_group.create_dataset("sfr_recooled_cumul_pp",data=sfr_recooled_cumul)
    subhalo_group.create_dataset("sfr_recooled_from_mp_cumul_pp",data=sfr_recooled_from_mp_cumul)
    subhalo_group.create_dataset("sfr_galtran_cumul_pp",data=sfr_galtran_cumul)
    subhalo_group.create_dataset("sfr_merge_cumul_pp",data=sfr_merge_cumul)

File.close()
