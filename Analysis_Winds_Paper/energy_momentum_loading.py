import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25
r_bins = [0.15, 0.95]

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

test_mode = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 50 Mpc ref with 28 tree snapshots
sim_name = "L0050N0752_REF_snap"

# 50 Mpc no-AGN with 28 tree snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"


part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str, "halo_wind_tot_subnorm"+fV_str]

vmin_i = 0.0
f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]

f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"]
f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"]

f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]

f_name_list += ["AGN_energy_injection_rate", "SNe_energy_injection_rate"]
f_name_list += ["AGN_energy_injection_rate_vcnorm", "SNe_energy_injection_rate_vcnorm"]

f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"]
f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"]
f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]

f_name_list += ["AGN_momentum_injection_rate", "SNe_momentum_injection_rate"]
f_name_list += ["AGN_momentum_injection_rate_vcnorm", "SNe_momentum_injection_rate_vcnorm"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
    
file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]

    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':8,
                    'legend.fontsize':8,
                    'axes.labelsize':9})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","blah","m","blah","g"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 10.; xhi = 15.0

if show_incomplete and complete_cut == "100star":
    xlo = 10.5

for i,ax in enumerate(subplots):
    py.axes(ax)


    r_bins_t = np.arange(0,1.1,0.1)
    r_mid_t = 0.5 * (r_bins_t[1:] + r_bins_t[:-1])
    i_r = np.argmin(abs(r_bins[0]-r_mid_t))
    i_r2 = np.argmin(abs(r_bins[1]-r_mid_t))

    show = [0,2,4]

    if i == 0:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            #quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            #quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1

            #quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            #quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][n,:,i_r]

            quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"][n,:,i_r]
            quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"][n,:,i_r]
            quant -= f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"][n,:,i_r]
            quant -= f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"][n,:,i_r]

            g2Msun = 1.989e33
            Msun = g2Msun *1e-3
            erg = 1e-7
            to_erg = Msun * 1e6 / erg

            #quant2 = (f_med_dict["AGN_energy_injection_rate"][n] ) / to_erg # Msun km^2 s^-2 yr^-1
            #quant3 = f_med_dict["SNe_energy_injection_rate"][n] / to_erg

            quant2 = (f_med_dict["AGN_energy_injection_rate_vcnorm"][n] ) # Gyr^-1
            quant3 = f_med_dict["SNe_energy_injection_rate_vcnorm"][n] 

            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                else:
                    ok = bin_mh_mid >= 10.8
            else:
                ok = bin_mh_mid > 0

            if n == 0:
                py.plot(bin_mh_mid[ok],np.log10(quant)[ok],label=r"$0.1 < r/R_{\mathrm{vir}} < 0.2$",c=c_list[n], linewidth=1)
                py.plot(bin_mh_mid[ok],np.log10(quant3)[ok],label=r"SNe injected",c=c_list[n],linestyle='--', linewidth=1)
                py.plot(bin_mh_mid[ok],np.log10(quant2)[ok],label=r"AGN injected",c=c_list[n],linestyle=':', linewidth=1)
            else:
                py.plot(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n], linewidth=1)
                py.plot(bin_mh_mid[ok],np.log10(quant3)[ok],c=c_list[n],linestyle='--', linewidth=1)
                py.plot(bin_mh_mid[ok],np.log10(quant2)[ok],c=c_list[n],linestyle=':', linewidth=1)

        py.legend(loc='upper right',frameon=False,numpoints=1,handlelength =3)
        #py.ylabel(r"$\log_{10}(\langle \dot{E} \rangle \, / \mathrm{M_\odot \, km^2 s^{-2} \, yr^{-1}})$")
        py.ylabel(r"$\log_{10}(\langle \dot{E} \rangle \, / 0.5 f_{\mathrm{B}} M_{200} V_{\mathrm{c}}^2 \, / \mathrm{ Gyr^{-1}})$")
        ylo = -1.9; yhi = 2.1

    if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            #quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            #quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant -= f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant -= f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            g2Msun = 1.989e33
            Msun = g2Msun *1e-3
            erg = 1e-7
            to_erg = Msun * 1e6 / erg
            quant *= to_erg # erg yr^-1

            quant *= 1./(f_med_dict["AGN_energy_injection_rate"][n] + f_med_dict["SNe_energy_injection_rate"][n])

            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                else:
                    ok = bin_mh_mid >= 10.8
            else:
                ok = bin_mh_mid > 0

            if n == show[0]:
                py.plot(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n], linewidth=1,label = "$0.1 < r/R_{\mathrm{vir}} < 0.2$")
            else:
                py.plot(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n], linewidth=1)
            py.scatter(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n],edgecolors="none",s=5)

            quant2 = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r2] # Msun km^2 s^-2 yr^-1
            quant2 += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r2] # Msun km^2 s^-2 yr^-1
            quant2 -= f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][n,:,i_r2] # Msun km^2 s^-2 yr^-1
            quant2 -= f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][n,:,i_r2] # Msun km^2 s^-2 yr^-1
            quant2 *= to_erg # erg yr^-1

            quant2 *= 1./(f_med_dict["AGN_energy_injection_rate"][n] + f_med_dict["SNe_energy_injection_rate"][n])

            if n == show[0]:
                py.plot(bin_mh_mid[ok],np.log10(quant2)[ok],c=c_list[n], linewidth=1,linestyle='--',label = "$0.9 < r/R_{\mathrm{vir}} < 1$")
            else:
                py.plot(bin_mh_mid[ok],np.log10(quant2)[ok],c=c_list[n], linewidth=1,linestyle='--')

        py.legend(loc='upper left',frameon=False,numpoints=1,handlelength =3)
        py.ylabel(r"$\log_{10}(\langle \dot{E}_{\mathrm{out}} \rangle \, / \langle \dot{E}_{\mathrm{injected}} \rangle)$")
        ylo = -1.5; yhi = 0.75
        py.axhline(0.0, c="k", alpha = 0.5)

    if i == 2:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            #quant = f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1

            #quant = f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant = f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant -= f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"][n,:,i_r] # Msun km^2 s^-2 yr^-1

            #quant2 = (f_med_dict["AGN_momentum_injection_rate"][n] )  # Msun km s^-1 yr^-1
            #quant3 = f_med_dict["SNe_momentum_injection_rate"][n] # Msun km s^-1 yr^-1

            quant2 = (f_med_dict["AGN_momentum_injection_rate_vcnorm"][n] ) # Gyr^-1
            quant3 = f_med_dict["SNe_momentum_injection_rate_vcnorm"][n] 

            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                else:
                    ok = bin_mh_mid >= 10.8
            else:
                ok = bin_mh_mid > 0


            py.plot(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n], linewidth=1,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
            py.plot(bin_mh_mid[ok],np.log10(quant3)[ok],c=c_list[n],linestyle='--', linewidth=1)
            py.plot(bin_mh_mid[ok],np.log10(quant2)[ok],c=c_list[n],linestyle=':', linewidth=1)

        #py.ylabel(r"$\log_{10}(\langle \dot{p} \rangle \, / \mathrm{M_\odot \, km^2 s^{-2} \, yr^{-1}})$")
        py.ylabel(r"$\log_{10}(\langle \dot{p} \rangle \, / f_{\mathrm{B}} M_{200} V_{\mathrm{c}} \, / \mathrm{ Gyr^{-1}})$")
        ylo = -3.0; yhi = 1.0
        py.legend(loc='upper right',frameon=False,numpoints=1)

    if i == 3:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            #quant = f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km s^-1 yr^-1
            #quant *= 1./(f_med_dict["AGN_momentum_injection_rate"][n] + f_med_dict["SNe_momentum_injection_rate"][n])
            quant = f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r] # Msun km s^-1 yr^-1
            quant -= f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][n,:,i_r] # Msun km s^-1 yr^-1
            quant *= 1./(f_med_dict["AGN_momentum_injection_rate"][n] + f_med_dict["SNe_momentum_injection_rate"][n])

            py.plot(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n], linewidth=1)
            py.scatter(bin_mh_mid[ok],np.log10(quant)[ok],c=c_list[n],edgecolors="none",s=5)


            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                else:
                    ok = bin_mh_mid >= 10.8
            else:
                ok = bin_mh_mid > 0

            quant2 = f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r2] # Msun km s^-1 yr^-1
            quant2 -= f_med_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][n,:,i_r2] # Msun km s^-1 yr^-1
            quant2 *= 1./(f_med_dict["AGN_momentum_injection_rate"][n] + f_med_dict["SNe_momentum_injection_rate"][n])
            py.plot(bin_mh_mid[ok],np.log10(quant2)[ok],c=c_list[n], linewidth=1,linestyle='--')

        py.ylabel(r"$\log_{10}(\langle \dot{p}_{\mathrm{out}} \rangle \, / \langle \dot{p}_{\mathrm{injected}} \rangle)$")
        ylo = -1.5; yhi = 0.75
        py.axhline(0.0, c="k", alpha = 0.5)


    if i == 2 or i == 3:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "energy_momentum_loading_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "energy_momentum_loading_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
