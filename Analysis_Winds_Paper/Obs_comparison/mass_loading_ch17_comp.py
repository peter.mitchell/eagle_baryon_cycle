import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str+"_mstar"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]


# Chisholm 17 observations
logmstar_ch17, logml_ch17, lo_ch17, hi_ch17 = np.loadtxt("Chisholm17.txt",unpack=True)

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.96,
                    'font.size':10,
                    'axes.labelsize':9,
                    'legend.fontsize':9})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 6.8; xhi = 11.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    errors = [-lo_ch17, hi_ch17]
    py.errorbar(logmstar_ch17, logml_ch17, errors, fmt="k^",label="Chisholm et al. (2017)",ms=3)

    for n in range(len(a_grid)):
        if n > 0:
            break

        if complete_cut == "1newstar":
            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
        else:
            ok = bin_mstar_mid >= np.log10(1.81e6)+2

        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_mstar"][n][ok]),label=(r"EAGLE, $%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c="b",linewidth=1)
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_mstar"][n]),c="b",linewidth=1,linestyle='--')
        

    py.legend(loc='upper right',frameon=False,numpoints=1)
    py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, /\dot{M}_\star)$")
    ylo = -1.0; yhi = 2.0

    py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_c17_comp_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_c17_comp_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
