import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap

# 50 Mpc REF with 28 snapshots
sim_name = "L0050N0752_REF_snap"

# 25 Mpc REF with 300 snapshots
#sim_name = "L0025N0376_REF_300_snap"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["f_injected_energy_SNe","f_heated_mass_SNe"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    fVmax_cut = 0.25
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]

    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.97,
                    'font.size':8,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    show = [0,2,3,6]
    for n in range(len(a_grid)):
        if n not in show:
            continue

        quant = f_med_dict["f_injected_energy_SNe"][n]
        if show_incomplete:
            py.plot(bin_mh_mid,quant,c=c_list[n],linestyle='--')

            if complete_cut == "1newstar":
                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
            else:
                ok = bin_mh_mid >= 10.8
            quant[ok==False] = np.nan
        else:
            ok = bin_mh_mid > 0

        py.plot(bin_mh_mid,quant,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
        py.scatter(bin_mh_mid[ok],f_med_dict["f_injected_energy_SNe"][n][ok],c=c_list[n],edgecolors="none",s=5)

    py.legend(loc='upper right',frameon=False,numpoints=1)
    py.ylabel(r"$f_{\mathrm{E,stellar}}$")

    ylo = 0.0; yhi = 1.0



    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "fraction_injected_energy_SNe_AGN_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "fraction_injected_energy_SNe_AGN_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()


