import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 stellar particles corresponds to logSFR of
sfr_lim = [-1.4, -1.2, -0.96, -0.67, -0.54, -0.39, -0.13, 0.16]
# 100 stellar particles corresponds to logVmax of
vmax_lim = [1.77, 1.82, 1.86, 1.92, 1.98, 2.02, 2.07, 2.15  ]


#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"

# 100 Mpc ref with 50 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 50 Mpc ref with 28 tree snapshots
#sim_name = "L0050N0752_REF_snap"

# 50 Mpc no-AGN with 28 tree snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# 25 Mpc ref with 50 tree snapshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 100 tree snapshots
#sim_name = "L0025N0376_REF_100_snip"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
bin_sfr_mid = file_grid["log_sfr_grid"][:]
bin_vcirc_mid = file_grid["log_vcirc_grid"][:]
bin_vmax_mid = file_grid["log_vmax_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str+"_sfr","ism_wind_tot_ml"+fV_str+"_vmax","ism_wind_tot"+fV_str+"_mstar","ism_wind_tot"+fV_str+"_sfr","ism_wind_tot"+fV_str+"_vmax","ism_wind_tot_ml"+fV_str+"_ssfr","ism_wind_tot"+fV_str+"_ssfr", "ism_wind_tot_ml_med"+fV_str+"_sfr"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar", "ism_wind_cum_ml_pp_complete"+fV_str+"_sfr", "ism_wind_cum_ml_pp_complete"+fV_str+"_vmax"] 
f_name_list2 += ["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar", "ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr", "ism_wind_cum_ml_pp_complete2"+fV_str+"_vmax"]

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.20,
                    'figure.figsize':[6.64,4.98*1.5],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.06,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 3; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    show = [0,1,2,3,4,5,6]

    if i == 0:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            quant = np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_mstar"][n])
            if show_incomplete:
                py.plot(bin_mstar_mid,quant,c=c_list[n],linestyle='--')

                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][n] == 1)  | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][n] == 0)
                else:
                    ok = bin_mstar_mid >= np.log10(1.81e6)+2 # 100 stellar particles
                quant[ok==False] = np.nan
            else:
                ok = bin_mh_mid > 0.0

            py.plot(bin_mstar_mid,quant,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
            py.scatter(bin_mstar_mid[ok],np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_mstar"][n])[ok],c=c_list[n],edgecolors="none",s=5)

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        ylo = -0.2; yhi = 1.5
        xlo = 7.0; xhi = 11.75

    if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot"+fV_str+"_mstar"][n])
            if show_incomplete:
                py.plot(bin_mstar_mid,quant,c=c_list[n],linestyle='--')

                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][n] == 1)  | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][n] == 0)
                else:
                    ok = bin_mstar_mid >= np.log10(1.81e6)+2 # 100 stellar particles
                quant[ok==False] = np.nan
            else:
                ok = bin_mh_mid > 0.0

            py.plot(bin_mstar_mid,quant,c=c_list[n])
            py.scatter(bin_mstar_mid[ok],np.log10(f_med_dict["ism_wind_tot"+fV_str+"_mstar"][n])[ok],c=c_list[n],edgecolors="none",s=5)


        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \mathrm{M_\odot yr}^{-1})$")
        ylo = -2.5; yhi = 2.5

    if i == 2:
        for n in range(len(a_grid)):
            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_sfr"][n])
            if show_incomplete:
                py.plot(bin_sfr_mid,quant,c=c_list[n],linestyle='--')

                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_sfr"][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr"][n] == 0)
                else:
                    ok = bin_sfr_mid >= sfr_lim[n]

                quant[ok==False] = np.nan
            else:
                ok = bin_mh_mid > 0

            py.plot(bin_sfr_mid,quant,c=c_list[n])
            py.scatter(bin_sfr_mid[ok],np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_sfr"][n])[ok],c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        ylo = -0.6; yhi = 1.5
        xlo = -1.4; xhi = 2.0


    if i == 3:
        for n in range(len(a_grid)):
            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot"+fV_str+"_sfr"][n])
            if show_incomplete:
                py.plot(bin_sfr_mid,quant,c=c_list[n],linestyle='--')

                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_sfr"][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr"][n] == 0)
                else:
                    ok = bin_sfr_mid >= sfr_lim[n]

                quant[ok==False] = np.nan
            else:
                ok = bin_mh_mid > 0

            py.plot(bin_sfr_mid,quant,c=c_list[n])
            py.scatter(bin_sfr_mid[ok],np.log10(f_med_dict["ism_wind_tot"+fV_str+"_sfr"][n])[ok],c=c_list[n],edgecolors="none",s=5)
        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \mathrm{M_\odot yr}^{-1})$")
        ylo = -1.1; yhi = 2.0

    if i == 4:
        for n in range(len(a_grid)):
            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_vmax"][n])
            if show_incomplete:
                py.plot(bin_vmax_mid,quant,c=c_list[n],linestyle='--')

                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_sfr"][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr"][n] == 0)
                else:
                    ok = bin_vmax_mid >= vmax_lim[n]

                quant[ok==False] = np.nan
            else:
                ok = bin_mh_mid > 0

            py.plot(bin_vmax_mid,quant,c=c_list[n])
            py.scatter(bin_vmax_mid[ok],np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_vmax"][n])[ok],c=c_list[n],edgecolors="none",s=5)

        xlo = 1.6; xhi = 3.0
        ylo = -0.3; yhi = 1.3

        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")

    if i == 5:
        for n in range(len(a_grid)):
            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot"+fV_str+"_vmax"][n])
            if show_incomplete:
                py.plot(bin_vmax_mid,quant,c=c_list[n],linestyle='--')

                if complete_cut == "1newstar":
                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_sfr"][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str+"_sfr"][n] == 0)
                else:
                    ok = bin_vmax_mid >= vmax_lim[n]

                quant[ok==False] = np.nan
            else:
                ok = bin_mh_mid > 0

            py.plot(bin_vmax_mid,quant,c=c_list[n])
            py.scatter(bin_vmax_mid[ok],np.log10(f_med_dict["ism_wind_tot"+fV_str+"_vmax"][n])[ok],c=c_list[n],edgecolors="none",s=5)
        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \mathrm{M_\odot yr}^{-1})$")
        ylo = -2.; yhi = 2.5

    if i == 0 or i == 1:
        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot})$")
    if i == 2 or i == 3:
        py.xlabel(r"$\log_{10}(\dot{M}_\star \, / \mathrm{M_\odot yr^{-1}})$")
    if i == 4 or i == 5:
        py.xlabel(r"$\log_{10}(V_{\mathrm{max}} \, / \mathrm{km s^{-1}})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "outflow_rate_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "outflow_rate_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
