import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

# Indicate bins where 20% of the galaxies have zero star formation across the entire redshift bin (for all progenitors)
show_incomplete = True

input_path_base = "/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"

# snipshot version doesn't work because redshift intervals don't line up
# 25 Mpc ref with 50 tree snapshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 50 Mpc REF with 28 snapshots
sim_name = "L0050N0752_REF_snap"
disk = "7"

# 50 Mpc no-AGN with 28 snapshots
sim_name2 = "L0050N0752_NOAGN_snap"
disk2 = "6"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
file_grid = h5py.File("/cosma"+disk+input_path_base+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")
file_grid2 = h5py.File("/cosma"+disk2+input_path_base+"efficiencies_grid_"+sim_name2+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

bin_mh_mid2 = file_grid2["log_msub_grid"][:]
a_grid2 = file_grid2["a_grid"][:]
z_grid2 = 1./a_grid2 - 1.0
a_min2 = file_grid2["a_min"]
a_max2 = file_grid2["a_max"]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["mstar_med"]

f_med_dict = {}
f_med_dict2 = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
    f_med_dict2[f_name] = file_grid2[f_name][:]

file_grid.close()
file_grid2.close()

if show_incomplete:
    file_grid = h5py.File("/cosma"+disk+input_path_base+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list = [ "ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]
    file_grid.close()

    file_grid2 = h5py.File("/cosma"+disk2+input_path_base+"efficiencies_grid_"+sim_name2+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list:
        f_med_dict2[f_name] = file_grid2[f_name][:]
    file_grid2.close()

    

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,4.98],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':8,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    n_show = [0, 2, 3, 5]

    if i == 0:
        for n in range(len(a_grid)):

            if n in n_show:

                if show_incomplete:

                    ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] ==0)

                    #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]),c=c_list[n],linestyle='--')

                    temp = np.log10(f_med_dict["mstar_med"][n])
                    temp[ok==False] = np.nan
                    py.plot(bin_mh_mid,temp,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
                    py.scatter(bin_mh_mid[ok],np.log10(f_med_dict["mstar_med"][n][ok]),c=c_list[n],edgecolors="none",s=5)

                    ok2 = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1 ) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str][n]== 0 )

                    temp = np.log10(f_med_dict2["mstar_med"][n])
                    temp[ok==False] = np.nan
                    py.plot(bin_mh_mid,temp,c=c_list[n],linestyle='--')
                    py.scatter(bin_mh_mid[ok2],np.log10(f_med_dict2["mstar_med"][n][ok2]),c=c_list[n],edgecolors="none",s=5)
                    
                else:

                    py.plot(bin_mh_mid,np.log10(f_med_dict["mstar_med"][n]),c=c_list[n],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                    py.scatter(bin_mh_mid,np.log10(f_med_dict["mstar_med"][n]),c=c_list[n],edgecolors="none",s=5)
                    
                    py.plot(bin_mh_mid2,np.log10(f_med_dict2["mstar_med"][n]),c=c_list[n],linestyle='--')

        py.legend(loc='upper right',frameon=False,numpoints=1)
        ylo = -1.49; yhi = 1.0
        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")


    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


py.show()
