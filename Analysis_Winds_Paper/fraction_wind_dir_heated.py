import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

print ""
print "Note that this will give bogus results if you plot a simulation analysed in bound_only mode"
print ""

fVmax_cut = 0.25

test_mode = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 28 tree snapshots
#sim_name = "L0050N0752_REF_snap" # Bugged - bound-only-mode screws up this measurement

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_snap_par_test" # Bugged - bound-only-mode screws up this measurment

# 25 Mpc REF with 300 snapshots
#sim_name = "L0025N0376_REF_300_snap"

# 50 Mpc no-AGN with 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# 25 Mpc REF with 500 snapshots
sim_name = "L0025N0376_REF_500_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["f_mass_halo_wind_dir_heated"+fV_str,"f_mass_ism_wind_dir_heated"+fV_str,"f_dir_heated_mass_in_ISM"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.97,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 13.5

for i,ax in enumerate(subplots):
    py.axes(ax)

    
    if i == 0:

        show = [0,2,3,5]

        for n in range(len(a_grid)):
            if n in show:
                py.plot(bin_mh_mid,f_med_dict["f_dir_heated_mass_in_ISM"][n],c=c_list[n],label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                py.scatter(bin_mh_mid,f_med_dict["f_dir_heated_mass_in_ISM"][n],c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$f_{\mathrm{dh,ISM}}$")
        ylo = 0.; yhi =1.0
        py.legend(loc='lower right',frameon=False,numpoints=1)

        py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "fraction_wind_dir_heated_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "fraction_wind_dir_heated_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()



























# Older version with 3 panels
quit()


py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*3],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.07,
                    'figure.subplot.top':0.97,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 3; ncol = 1
subplots = panelplot(nrow,ncol)

"f_mass_halo_wind_dir_heated","f_mass_ism_wind_dir_heated","f_dir_heated_mass_in_ISM"

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:

        for n in range(len(a_grid)):

            py.plot(bin_mh_mid,f_med_dict["f_mass_ism_wind_dir_heated"][n],label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
            py.scatter(bin_mh_mid,f_med_dict["f_mass_ism_wind_dir_heated"][n],c=c_list[n],edgecolors="none",s=5)

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$f_{\mathrm{wind-ISM,dh}}$")
        ylo = 0.; yhi =0.99

    elif i == 1:

        for n in range(len(a_grid)):
            py.plot(bin_mh_mid,f_med_dict["f_mass_halo_wind_dir_heated"][n],c=c_list[n])
            py.scatter(bin_mh_mid,f_med_dict["f_mass_halo_wind_dir_heated"][n],c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$f_{\mathrm{wind-Halo,dh}}$")
        ylo = 0.; yhi =0.99

    elif i == 2:

        for n in range(len(a_grid)):
            py.plot(bin_mh_mid,f_med_dict["f_dir_heated_mass_in_ISM"][n],c=c_list[n])
            py.scatter(bin_mh_mid,f_med_dict["f_dir_heated_mass_in_ISM"][n],c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$f_{\mathrm{dh,ISM}}$")
        ylo = 0.; yhi =0.99

        py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "fraction_wind_dir_heated_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "fraction_wind_dir_heated_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
