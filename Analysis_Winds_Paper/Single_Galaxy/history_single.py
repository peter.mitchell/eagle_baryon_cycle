# The purpose of this script is as a post-processing step after the main analysis, walk up the merger the tree of each z=0 galaxy and calculate the integrated outflow/sfr activity

import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms

fVmax_cut = 0.25

test_mode = True # Write a single subvolume with a non-default nsub_1d
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

h=0.6777

grn_choose = 36
#grn_choose = 506
#grn_choose = 507
#grn_choose = 535

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc reference run, 50 snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 500 snaps
sim_name = "L0025N0376_REF_500_snap"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if not os.path.exists(catalogue_path+filename):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename
    quit()

File = h5py.File(catalogue_path+filename,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()




mass_join_ism_wind = np.zeros(len(snapshots))+np.nan
mass_join_halo_wind = np.zeros(len(snapshots))+np.nan
mass_join_halo_wind_direct = np.zeros(len(snapshots))+np.nan
mass_new_stars_init= np.zeros(len(snapshots))+np.nan

dmdt_out_0p25vmax = np.zeros((len(snapshots), 10))+np.nan
P50mw = np.zeros((len(snapshots), 10))+np.nan
P50mw_out = np.zeros((len(snapshots), 10))+np.nan
P50fw_out = np.zeros((len(snapshots), 10))+np.nan
Pmean = np.zeros((len(snapshots), 10))+np.nan
a_grav = np.zeros((len(snapshots), 10))+np.nan
m_shell = np.zeros((len(snapshots), 10))+np.nan

r200 = np.zeros(len(snapshots))+np.nan
m200_track = np.zeros(len(snapshots))+np.nan

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename,"r")

first_select = True
################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "reading tree snapshot", snap

    try:
        # Read the data
        snapnum_group = File["Snap_"+str(snap)]

        subhalo_group = snapnum_group["subhalo_properties"]
        node_index_ts = np.rint(subhalo_group["node_index"][:]).astype("int") # Seems I didn't process_subvols correctly
    except:
        print "No data found for",snap,snip,"skipping"
        continue

    grn_ts = subhalo_group["group_number"][:]
    sgrn_ts = subhalo_group["subgroup_number"][:]
    m200 = subhalo_group["m200_host"][:]
    if first_select:

        ok = (sgrn_ts == 0)
        select = np.argmin(abs(grn_ts[sgrn_ts==0]-grn_choose))

        ni_choose_ps = node_index_ts[ok][select]
        first_select = False
    else:
        descendant_index_ts = np.rint(subhalo_group["descendant_index"][:]).astype("int")
        
        ok = (ni_choose_ps == descendant_index_ts) & (sgrn_ts==0)
        if len(ok[ok])==0:
            print "lost track of main progenitor"
            continue
        select = np.argmax(m200[ok])

    redshift_ts = z_snapshots[i_snap]
    r200[i_snap] = subhalo_group["r200_host"][:][select]*1e3/(1+redshift_ts)/h
    
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    mass_join_ism_wind[i_snap] = (subhalo_group["mass_sf_ism_join_wind"+fV_str][:] + subhalo_group["mass_ism_reheated_join_wind"+fV_str][:] + subhalo_group["mass_nsf_ism_join_wind"+fV_str][:])[ok][select]
    mass_join_halo_wind[i_snap] = (subhalo_group["mass_join_halo_wind"+fV_str][:] + subhalo_group["mass_halo_reheated_join_wind"+fV_str][:])[ok][select]
    mass_join_halo_wind_direct[i_snap] = (subhalo_group["mass_join_halo_wind"+fV_str][:] )[ok][select]
    mass_new_stars_init[i_snap] = subhalo_group["mass_new_stars_init"][:][ok][select]
    dmdt_out_0p25vmax[i_snap] = subhalo_group["dmdt_out_0p25vmax"][:][ok][select]
    P50mw[i_snap] = subhalo_group["P50mw"][:][ok][select]
    P50mw_out[i_snap] = subhalo_group["P50mw_out_0p25vmax"][:][ok][select]
    P50fw_out[i_snap] = subhalo_group["P50fw_out_0p25vmax"][:][ok][select]
    Pmean[i_snap] = subhalo_group["Pmean"][:][ok][select]
    a_grav[i_snap] = subhalo_group["a_grav"][:][ok][select]
    m_shell[i_snap] = subhalo_group["m_shell"][:][ok][select]
    m200_track[i_snap] = m200[ok][select]
    
    ni_choose_ps = node_index_ts[ok][select]
    ni_ps = node_index_ts
    m200_ps = m200
    sgrn_ps = sgrn_ts
    grn_ps = grn_ts

    print np.log10(m200[ok][select]), grn_ts[ok][select], sgrn_ts[ok][select]

dt = t_snapshots[:-1] - t_snapshots[1:]
rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]

ok = (z_snapshots >2) & (np.isnan(mass_join_ism_wind)==False)
print "(log) Sum of Lagrangan ISM outflow for z>2 =", np.log10(np.sum(mass_join_ism_wind[ok]))
print "(log) Sum of Lagrangan halo outflow for z>2 =", np.log10(np.sum(mass_join_halo_wind[ok]))

ok = (z_snapshots <1) & (np.isnan(mass_join_ism_wind)==False)
print "(log) Sum of Lagrangan ISM outflow for z<1 =", np.log10(np.sum(mass_join_ism_wind[ok]))
print "(log) Sum of Lagrangan halo outflow for z<1 =", np.log10(np.sum(mass_join_halo_wind[ok]))


from utilities_plotting import *
py.figure()
py.subplot(211)

logy = False

if logy:
    py.plot(t_snapshots[:-1], np.log10(mass_new_stars_init[:-1]*1e-9/dt),c="g")
    py.plot(t_snapshots[:-1], np.log10(mass_join_ism_wind[:-1]*1e-9/dt),c="r")

    #py.plot(t_snapshots[:-1], np.log10(dmdt_out_0p25vmax[:-1][:,0]),c="k",alpha=0.5)
    py.plot(t_snapshots[:-1], np.log10(dmdt_out_0p25vmax[:-1][:,1]),c="k",alpha=0.7,linestyle='--')
    py.plot(t_snapshots[:-1], np.log10(dmdt_out_0p25vmax[:-1][:,4]),c="k",alpha=0.7,linestyle='-.')
    py.plot(t_snapshots[:-1], np.log10(dmdt_out_0p25vmax[:-1][:,7]),c="k",alpha=0.7,linestyle=':')
    py.plot(t_snapshots[:-1], np.log10(dmdt_out_0p25vmax[:-1][:,9]),c="k",alpha=0.7,linestyle=(0, (1,1)))

    ylo = 0.0; yhi = 1.25
    dyn_range = 0.5*(yhi-ylo)
    py.ylim((-0.0,1.25))
    py.xlim((7,12))

else:
    py.plot(t_snapshots[:-1], mass_new_stars_init[:-1]*1e-9/dt,c="g")
    py.plot(t_snapshots[:-1], mass_join_ism_wind[:-1]*1e-9/dt,c="r")
    py.plot(t_snapshots[:-1], mass_join_halo_wind[:-1]*1e-9/dt,c="m",linestyle=(0, (1,1)))
    py.plot(t_snapshots[:-1], mass_join_halo_wind_direct[:-1]*1e-9/dt,c="m",linestyle=(0, (1,1)),alpha=0.5)

    #py.plot(t_snapshots[:-1], dmdt_out_0p25vmax[:-1][:,0],c="k",alpha=0.5)
    py.plot(t_snapshots[:-1], dmdt_out_0p25vmax[:-1][:,1],c="k",alpha=0.7,linestyle='--')
    py.plot(t_snapshots[:-1], dmdt_out_0p25vmax[:-1][:,4],c="k",alpha=0.7,linestyle='-.')
    py.plot(t_snapshots[:-1], dmdt_out_0p25vmax[:-1][:,7],c="k",alpha=0.7,linestyle=':')
    py.plot(t_snapshots[:-1], dmdt_out_0p25vmax[:-1][:,9],c="k",alpha=0.7,linestyle=(0, (1,1)))


    ylo = 0.0; yhi = 1.25
    dyn_range = 0.5*(yhi-ylo)
    #py.ylim((-0.0,1.25))
    #py.xlim((7,12))

py.subplot(212)

py.plot(t_snapshots, np.log10(m200_track), c="k")

'''py.subplot(312)

use = "median"
#use = "mean"
if use == "median":
    print ""
    print "Using median pressure"
    print ""
    P50mw = P50mw
elif use == "mean":
    print ""
    print "Using mean pressure"
    print ""
    P50mw = Pmean
else:
    print "no", use
    quit()

dP = P50mw[:,1:] - P50mw[:,0:-1]
dP *= -1

ilo = np.argmin(abs(7-t_snapshots))

P50mw = np.log10(P50mw)-np.log10(P50mw[ilo])
#dP = np.log10(dP)-np.log10(dP[ilo])

dr = 0.1
rlo = np.arange(0.0,1.0,dr)
rhi = rlo + dr
rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]

kpc =  3.0857e19 # m
Msun = 1.989e30
dPdr = np.zeros_like(dP)
a_pressure = np.zeros_like(dP)
for n in range(len(rmid)-1):
    dPdr[:,n] = dP[:,n] / (dr*r200*kpc) # kg m^-2 s^-2
    a_pressure[:,n] = (dPdr[:,n] * (dr*r200*kpc) * 4*np.pi*(rmid[n]*r200*kpc)**2)/(m_shell[:,n]*Msun) # m s^-2

py.plot(t_snapshots, P50mw[:,1],c="k",linestyle='--',alpha=0.7)
py.plot(t_snapshots, P50mw[:,4],c="k",linestyle='-.',alpha=0.7)
py.plot(t_snapshots, P50mw[:,7],c="k",linestyle=':',alpha=0.7)
py.plot(t_snapshots, P50mw[:,9],c="k",linestyle=(0, (1,1)),alpha=0.7)
py.xlim((7,12))
#py.ylim((-15.75,-13.5))
py.ylim((-dyn_range,dyn_range))

py.subplot(313)

a_pressure = np.log10(a_pressure)
a_grav = np.log10(a_grav)

#a_pressure -= a_grav[ilo,1:]
#a_grav -= a_grav[ilo]

py.plot(t_snapshots, a_pressure[:,0],c="r",linestyle='--',alpha=0.7)
py.plot(t_snapshots, a_pressure[:,3],c="r",linestyle='-.',alpha=0.7)
py.plot(t_snapshots, a_pressure[:,6],c="r",linestyle=':',alpha=0.7)
py.plot(t_snapshots, a_pressure[:,8],c="r",linestyle=(0, (1,1)),alpha=0.7)

py.plot(t_snapshots, a_grav[:,1],c="k",linestyle='--',alpha=0.7)
py.plot(t_snapshots, a_grav[:,4],c="k",linestyle='-.',alpha=0.7)
py.plot(t_snapshots, a_grav[:,7],c="k",linestyle=':',alpha=0.7)
py.plot(t_snapshots, a_grav[:,9],c="k",linestyle=(0,(1,1)),alpha=0.7)

py.xlim((7,12))
py.ylim((-11.6,-9))
#py.ylim((-dyn_range,dyn_range))'''


py.show()
