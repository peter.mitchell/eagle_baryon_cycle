import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"
#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/"
#nsnap =50
#tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"

# 25 Mpc ref with 50 tree snapshots
sim_name = "L0025N0376_REF_50_snip"
sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/"
snap =50
tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"
#tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
#nsnap = 28
#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["aform_bh"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.97,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    for n in range(len(a_grid)):

        py.plot(bin_mh_mid,1/f_med_dict["aform_bh"][n]-1,label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
        py.scatter(bin_mh_mid,1/f_med_dict["aform_bh"][n]-1,c=c_list[n],edgecolors="none",s=5)

    py.legend(loc='upper right',frameon=False,numpoints=1)
    py.ylabel(r"$z_{\mathrm{form,bh}}$")
    ylo = 0.0; yhi = 12.0

    py.xlabel(r"$\log(M_{\mathrm{sub}} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "zform_bh_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "zform_bh_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()


