import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

test_mode = True

star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

mhalo_choose = 1e12

# 25 Mpc REF with 50 snips
sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]

omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"
if test_mode:
    output_base = input_path + sim_name+"_subvols_test_mode/"
else:
    output_base = input_path + sim_name+"_subvols/"

if star_mass == "particle_mass":
    name_list = ["mchalo","SubGroupNumber_hydro","Vmax","nodeIndex","descendantIndex","mass_star","Mbh","mass_bh"]
    name_list += ["mass_new_stars_init","mass_acc_bh"]
    name_list += ["mass_sf_ism_join_wind","mass_nsf_ism_join_wind","mass_ism_reheated_join_wind"]
    name_list += ["mass_join_halo_wind","mass_halo_reheated_join_wind"]
    name_list += ["mass_ism_wind_cumul","mass_ism_to_halo_wind_cumul", "mass_join_halo_wind_from_ism"]

data_list = {}
dt_list = np.zeros_like(snip_list[:-1]).astype("float")

for name in name_list:
    data_list[name] = np.zeros_like(snip_list[:-1]).astype("float")
        
filename = "subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename)

print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    print i_snap,"of",len(snap_list[:-1])

    subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    progen_group = File["Snap_"+str(snap)+"/main_progenitors"]

    dt_list[i_snap] = t[i_snap] - t[i_snap+1]

    g2Msun = 1./(1.989e33)
    mchalo = subhalo_group["mchalo"][:]* g2Msun * 1.989e43 /h
    node_index_ts = subhalo_group["nodeIndex"][:]
    node_index_progen_ts = progen_group["nodeIndex"][:]
    
    if i_snap == 0:
        ind_choose = [np.argmin(abs(mchalo-mhalo_choose))]
    else:
        ind_choose = np.where(node_index_ts == node_index_progen_choose_ps)[0]
    
    if len(ind_choose) == 0:
        print "no data for this snap"
        continue

    node_index_progen_choose_ts = node_index_progen_ts[ind_choose]
    node_index_progen_choose_ps = node_index_progen_choose_ts

    if name_list[-1] in subhalo_group:

        for i_name, name in enumerate(name_list):
            data = subhalo_group[name][:]
            #print data[ind_choose]
            data_list[name][i_snap] = data[ind_choose]
    else:
        print "no data for this snap"

    if i_snap == 1:
        print "Final stellar mass"
        print data_list["mass_star"][i_snap], np.log10(data_list["mass_star"][i_snap])

# SN feedback parameters
e_sn = 8.73e15 # ergs per gram - energy injected per unit mass
g2Msun = 1.989e33
e_sn *= g2Msun # erg per Msun

# AGN feedback parameters
eps_r = 0.1 # Radiative efficiency (fraction of accreted rest-mass energy which is radiated)
eps_f = 0.15 # Coupling efficiency between radiated energy and the ISM
c = 299792458 # ms-1
c2 = c**2 * 1e7 * g2Msun *1e-3 # erg per Msun
e_bh = eps_r *eps_f / (1-eps_r)*c2 # energy injected per unit mass growth of black holes (erg per Msun)

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,2.49],
                    'figure.subplot.left':0.07,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.right':0.99,
                    'figure.subplot.top':0.97,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = t.min(); xhi = t[1:].max()


dt_list *= 1e9 # yr

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        py.plot(t[:-1], data_list["mass_new_stars_init"]/dt_list,label="SFR",c="k") # Msun yr^-1
        py.scatter(t[:-1], data_list["mass_new_stars_init"]/dt_list,c="k",s=5,edgecolors="none")

        py.plot(t[:-1], data_list["mass_acc_bh"]/dt_list*e_bh/e_sn,label="BHAR $(\epsilon_{\mathrm{bh}}/\epsilon_{\mathrm{sn}})$",c="g")
        py.scatter(t[:-1], data_list["mass_acc_bh"]/dt_list*e_bh/e_sn,c="g",s=5,edgecolors="none")
        
        py.plot(t[:-1], (data_list["mass_sf_ism_join_wind"]+data_list["mass_nsf_ism_join_wind"]+data_list["mass_ism_reheated_join_wind"])/dt_list,label="ISM wind",c="b")
        py.scatter(t[:-1], (data_list["mass_sf_ism_join_wind"]+data_list["mass_nsf_ism_join_wind"]+data_list["mass_ism_reheated_join_wind"])/dt_list,c="b",s=5,edgecolors="none")
        
        py.plot(t[:-1], (data_list["mass_join_halo_wind"]+data_list["mass_halo_reheated_join_wind"])/dt_list,label="Halo wind",c="r")
        py.scatter(t[:-1], (data_list["mass_join_halo_wind"]+data_list["mass_halo_reheated_join_wind"])/dt_list,s=5,edgecolors="none",c="r")
    else:
        '''g2Msun = 1./(1.989e33)
        py.plot(t[:-1], data_list["Mbh"]* g2Msun * 1.989e43 /h)
        py.scatter(t[:-1], data_list["Mbh"]* g2Msun * 1.989e43 /h)

        py.plot(t[:-1], data_list["mass_bh"])
        py.scatter(t[:-1], data_list["mass_bh"])'''

        py.plot(t[:-1], data_list["mass_ism_to_halo_wind_cumul"]/data_list["mass_ism_wind_cumul"],c="b",label="ISM to halo (cumulative)")
        py.scatter(t[:-1], data_list["mass_ism_to_halo_wind_cumul"]/data_list["mass_ism_wind_cumul"])

        halo_wind = data_list["mass_join_halo_wind"]+data_list["mass_halo_reheated_join_wind"]
        py.plot(t[:-1], data_list["mass_join_halo_wind_from_ism"]/halo_wind,c="r",label="halo from ISM")
        py.scatter(t[:-1], data_list["mass_join_halo_wind_from_ism"]/halo_wind,c="r")

    py.legend(loc='upper right',frameon=False,numpoints=1)
    py.ylabel(r"$\log_{10}(\dot{M} \, / \mathrm{M_\odot yr^{-1}})$")
    ylo = -1.; yhi =25

    py.xlabel(r"$t \, / {\mathrm{Gyr}}$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "sfh_illustration_figure.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
