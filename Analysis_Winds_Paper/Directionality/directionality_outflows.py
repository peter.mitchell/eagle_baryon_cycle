import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

test_mode = False
subsample = False
n_per_bin = 1000

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 2
snap = 175

# 50 Mpc REF with 28 snaps
#sim_name = "L0050N0752_REF_snap"
#nsub1d = 1
#snip_list = np.arange(0,29)[::-1]
#snap_list = np.arange(0,29)[::-1]

# 25 Mpc Ref with 28 snaps
#sim_name = "L0025N0376_REF_snap"
#snip_list = np.arange(0,29)[::-1]
#snap_list = np.arange(0,29)[::-1]
#nsub1d = 1
#snap = 25

snip = snip_list[np.argmin(abs(snap_list-snap))]

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if "L0025" in sim_name:
        subvol_choose = [36]
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = [2000]
    else:
        print "err"
        quit()


output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)
    if subsample:
        output_base += "_subsample_"+str(n_per_bin)
    output_base += "_test_mode/"

subhalo_prop_names = ["m200_host"]
subhalo_prop_types =  ["float"]

name_list = ["mass_stars_30kpc"]

vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
for vmin_i in vmin:

    name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["frac_omega_grt_"+str(vmin_i).replace(".","p")+"vmax"]

print "plotting for snapshot", snap


############# Read data ###################################

output_path = output_base + "Snip_"+str(snip)+"/"

sub_input_list = []
input_list = []
for name in name_list:
    input_list.append([])   
for name in subhalo_prop_names:
    sub_input_list.append([])

# Find all the subvolume files (they won't be sorted in order)
subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

if len(subvols) == 0:
    print "There are no subvolume files. Run the pipeline first"
    File.close()
    quit()

# Gather together measurements
for n, subvol in enumerate(subvols):

    if test_mode:
        skip = True
        for subvol_i in subvol_choose:
            if "_"+str(subvol_i)+".hdf5" in subvol:
                skip = False
        if skip:
            continue

    try:
        File_subvol = h5py.File(subvol,"r")
    except:
        print "Couldn't read subvol file", subvol, "moving on"
        continue

    print "Reading", subvol

    try:
        halo_group = File_subvol["directionality_group"]
    except:
        print "Read failed, skipping this subvol"
        continue

    if name_list[-1] in halo_group and name_list[0] in halo_group:
        for i_name, name in enumerate(name_list):

            # Deal with 2d arrays
            if input_list[i_name] == [] and len(halo_group[name][:].shape) ==2:
                input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1]))
            elif input_list[i_name] == [] and len(halo_group[name][:].shape) ==3:
                 input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1],halo_group[name][:].shape[2]))

            input_list[i_name] = np.append(input_list[i_name], halo_group[name][:],axis=0)

        for i_name, name in enumerate(subhalo_prop_names):

            prop_type = subhalo_prop_types[i_name]
            if prop_type == "int":
                if n == 0:
                    sub_input_list[i_name] = np.array([]).astype("int")
                temp = halo_group[name][:]

            elif prop_type == "float":
                temp = halo_group[name][:]
            else:
                "Error: prop type",prop_type,"not understood"
                quit()

            sub_input_list[i_name] = np.append(sub_input_list[i_name], temp)

    else:
        print "subvol",n,"didn't contain any haloes"

    File_subvol.close()

# Do mean stacking for v>0.25Vmax for now, over some finite halo mass interval
mhalo_lo = 11.75
mhalo_hi = 12.25

mhalo = sub_input_list[0]
ok = (np.log10(mhalo) > mhalo_lo) & (np.log10(mhalo) < mhalo_hi)

for i_name, name in enumerate(name_list):
    if name == "frac_omega_grt_0p25vmax":
        covering_frac = np.mean(input_list[i_name][ok],axis=0)

for i_name, name in enumerate(name_list):
    if name == "dmdt_out_0p25vmax":
        galangle = np.mean(input_list[i_name][ok],axis=0)

n_theta_bins = 72
theta_bins = np.pi * np.arange(-1, 1+2./n_theta_bins, 2./n_theta_bins)
theta_mid = 0.5*(theta_bins[1:] + theta_bins[0:-1])

dr = 0.1
rbins = np.arange(0,1.1,dr)
rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
n_rbins = len(rmid)

rmid_grid = np.repeat(rmid, n_theta_bins)
theta_mid_grid = np.repeat(theta_mid, n_rbins)
theta_mid_grid = np.zeros_like(rmid_grid)
n = -1
for i in range(len(rmid_grid)):
    n += 1
    theta_mid_grid[i] = theta_mid[n]
    if n == len(theta_mid)-1:
        n = -1

galangle_grid = np.ravel(galangle)

from utilities_plotting import *

nrow = 2; ncol = 1

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.1,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*nrow*1.15],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'font.size':10,
                    'legend.fontsize':10,
                    'axes.labelsize':11})
 
py.figure()
np.seterr(all='ignore')
fig, axes = py.subplots(nrow,ncol)

for i,ax in enumerate(axes.flat):
    py.axes(ax)

    if i == 0:
        image = py.hist2d(rmid_grid, theta_mid_grid, bins = [rbins,theta_bins], weights=np.log10(galangle_grid),cmap="viridis")
        
        for tick in ax.xaxis.get_major_ticks():
            tick.label1On = False

        #weights = galangle_grid
        #image.norm.vmin = np.log10(weights[weights>0].min())
        #image.norm.vmax = np.log10(weights.max())

        py.ylabel(r"$\mathrm{Galactocentric \, Angle}$")

        y_ticks = np.array([-np.pi, -np.pi*0.5, 0.0, np.pi*0.5, np.pi])
        tick_labels = [r"$-\pi$", r"$-\pi /2$", r"$0$", r"$\pi /2$", r"$\pi$"]

        ax.set_yticks(y_ticks)
        ax.set_yticklabels(tick_labels)

        cbar_label = r'$\log_{10}(\dot{M}_{\mathrm{out}}(v_{\mathrm{rad}} > 0.25 V_{\mathrm{max}}) \, / \mathrm{M_\odot yr^{-1}})$'
        fig.subplots_adjust(top=0.86)
        cbar_ax = fig.add_axes([0.15, 0.95, 0.77, 0.03])
        cbar = fig.colorbar(mappable=image[3], cax=cbar_ax, label=cbar_label,orientation = "horizontal")
        cbar.ax.tick_params(labelsize=6)

        #py.colorbar(label="test",orientation="horizontal")
        
    if i == 1:
        py.plot(rmid, covering_frac,c="k")
        py.scatter(rmid, covering_frac,edgecolors="none",c="k",s=5)
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

        py.ylabel(r"$\Omega(\bar{v}_{\mathrm{rad}} > 0.25 \, V_{\mathrm{max}}) \, / 4 \pi$")

        py.annotate(r"$10^{11.75} < M_{200} \, / \mathrm{M_\odot} < 10^{12.25}$",(0.1,0.06))
        py.annotate(r"$z=0.25$",(0.1,0.12))

        py.ylim((0.0, 0.49))

fig_name = "directionality_outflows_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
