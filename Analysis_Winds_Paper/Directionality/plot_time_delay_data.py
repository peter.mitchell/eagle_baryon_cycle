import numpy as np

sim_name = "L0025N0376_REF_200_snip"

z, logmh_lo, tcross, sfr_end, sfr_start = np.loadtxt("time_delay_data.dat",unpack=True)

from utilities_plotting import *

nrow = 2; ncol = 1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

py.figure()

subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:

        ok1 = z < 1
        py.plot(logmh_lo[ok1]+0.1, tcross[ok1], linewidth=1, c="k", label = r"$z=0.25$")
        py.scatter(logmh_lo[ok1]+0.1, tcross[ok1], edgecolors="none",s=5, c="k")

        ok2 = z > 1
        py.plot(logmh_lo[ok2]+0.1, tcross[ok2], linewidth=1, c="c", label = r"$z=2$")
        py.scatter(logmh_lo[ok2]+0.1, tcross[ok2], edgecolors="none",s=5, c="c")

        py.legend(loc='upper left',frameon=False,numpoints=1)

        py.ylabel(r"$\Delta t_{\mathrm{cross}} \, / \mathrm{Gyr}$")

    if i == 1:

        ok1 = z < 1
        y1 = np.log10(sfr_end / sfr_start)[ok1]
        py.plot(logmh_lo[ok1]+0.1, y1, linewidth=1, c="k", label = r"$z=0.25$")
        py.scatter(logmh_lo[ok1]+0.1, y1, edgecolors="none",s=5, c="k")

        ok2 = z > 1
        y2 = np.log10(sfr_end / sfr_start)[ok2]
        py.plot(logmh_lo[ok2]+0.1, y2, linewidth=1, c="c", label = r"$z=2$")
        py.scatter(logmh_lo[ok2]+0.1, y2, edgecolors="none",s=5, c="c")

        py.axhline(0.0,c="k",alpha=0.5,linestyle='--')

        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log_{10}(\dot{M}_\star(t) \, / \dot{M}_\star(t- \Delta t_{\mathrm{cross}}) \, )$")

        py.ylim((-1.2,0.29))

fig_name = "time_delay_sfr_change_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
