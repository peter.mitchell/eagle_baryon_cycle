import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

test_mode = False
subsample = False
n_per_bin = 1000

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
#sim_name = "L0100N1504_REF_200_snip"
#snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
#snip_list = np.rint(snip_list[::-1]).astype("int")
#snap_list = np.rint(snap_list[::-1]).astype("int")
#nsub1d = 2

# 50 Mpc REF with 28 snaps
sim_name = "L0050N0752_REF_snap"
nsub1d = 1
snip_list = np.arange(0,29)[::-1]
snap_list = np.arange(0,29)[::-1]
snap = 25

# 25 Mpc Ref with 28 snaps
#sim_name = "L0025N0376_REF_snap"
#snip_list = np.arange(0,29)[::-1]
#snap_list = np.arange(0,29)[::-1]
#nsub1d = 1
#snap = 25

snip = snip_list[np.argmin(abs(snap_list-snap))]

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if "L0025" in sim_name:
        subvol_choose = [36]
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = [2000]
    else:
        print "err"
        quit()


output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)
    if subsample:
        output_base += "_subsample_"+str(n_per_bin)
    output_base += "_test_mode/"

subhalo_prop_names = ["m200_host","r200_host"]
subhalo_prop_types =  ["float","float"]

name_list = ["mass_stars_30kpc"]
name_list += ["dmdr","dpdr","dEkdr","dEtdr"]
name_list += ["Pmean", "P50mw","P50Vw", "mshell", "agrav"]

vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
for vmin_i in vmin:

    name_list += ["dmdt_out_standard_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["dpdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["dEkdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["dEtdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["frac_omega_grt_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["mass_grt_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["P50fw_grt_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

print "plotting for snapshot", snap


############# Read data ###################################

output_path = output_base + "Snip_"+str(snip)+"/"

sub_input_list = []
input_list = []
for name in name_list:
    input_list.append([])   
for name in subhalo_prop_names:
    sub_input_list.append([])

# Find all the subvolume files (they won't be sorted in order)
subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

if len(subvols) == 0:
    print "There are no subvolume files. Run the pipeline first"
    File.close()
    quit()

# Gather together measurements
for n, subvol in enumerate(subvols):

    if test_mode:
        skip = True
        for subvol_i in subvol_choose:
            if "_"+str(subvol_i)+".hdf5" in subvol:
                skip = False
        if skip:
            continue

    try:
        File_subvol = h5py.File(subvol,"r")
    except:
        print "Couldn't read subvol file", subvol, "moving on"
        continue

    print "Reading", subvol

    try:
        halo_group = File_subvol["directionality_group"]
    except:
        print "Read failed, skipping this subvol"
        continue

    redshifts = File_subvol["tree_snapshot_info/redshifts_tree"][:]
    snapshots_tree = File_subvol["tree_snapshot_info/snapshots_tree"][:]
    redshift = redshifts[np.argmin(abs(snapshots_tree-snap))]

    if name_list[-1] in halo_group and name_list[0] in halo_group:
        for i_name, name in enumerate(name_list):

            # Deal with 2d arrays
            if input_list[i_name] == [] and len(halo_group[name][:].shape) ==2:
                input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1]))
            elif input_list[i_name] == [] and len(halo_group[name][:].shape) ==3:
                 input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1],halo_group[name][:].shape[2]))

            input_list[i_name] = np.append(input_list[i_name], halo_group[name][:],axis=0)

        for i_name, name in enumerate(subhalo_prop_names):

            prop_type = subhalo_prop_types[i_name]
            if prop_type == "int":
                if n == 0:
                    sub_input_list[i_name] = np.array([]).astype("int")
                temp = halo_group[name][:]

            elif prop_type == "float":
                temp = halo_group[name][:]
            else:
                "Error: prop type",prop_type,"not understood"
                quit()

            sub_input_list[i_name] = np.append(sub_input_list[i_name], temp)

    else:
        print "subvol",n,"didn't contain any haloes"

    File_subvol.close()

# Do mean stacking for v>0.25Vmax for now, over some finite halo mass interval
mhalo_lo = 12.0
mhalo_hi = 12.2
mhalo_lo = 13.0
mhalo_hi = 13.5
#mhalo_lo = 14.0
#mhalo_hi = 14.5
mhalo = sub_input_list[0]
ok = (np.log10(mhalo) > mhalo_lo) & (np.log10(mhalo) < mhalo_hi)

h=0.6777 
r200 = sub_input_list[1]*1e3/(1+redshift)/h # pkpc

for i_name, name in enumerate(name_list):
    if name == "dmdr":
        dmdr = np.nanmedian(input_list[i_name][ok],axis=0)

    if name == "dmdr_out_0p25vmax":
        dmdr_0p25vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dmdr_out_0p0vmax":
        dmdr_0vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dmdt":
        dmdt = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dmdt_out_standard_0p25vmax":
        dmdt_0p25vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dmdt_out_standard_0p0vmax":
        dmdt_0vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dpdr":
        dpdr = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dpdr_out_0p25vmax":
        dpdr_0p25vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dpdr_out_0p0vmax":
        dpdr_0vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dEkdr":
        dEkdr = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dEkdr_out_0p25vmax":
        dEkdr_0p25vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dEkdr_out_0p0vmax":
        dEkdr_0vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dEtdr":
        dEtdr = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dEtdr_out_0p25vmax":
        dEtdr_0p25vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "dEtdr_out_0p0vmax":
        dEtdr_0vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "agrav":
        agrav = np.nanmedian(input_list[i_name][ok],axis=0)

    if name == "mshell":
        mshell = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50mw":
        P50mw = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50Vw":
        P50Vw = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "Pmean":
        Pmean = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50fw_out_0p25vmax":
        P50fw_0p25_vmax = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "frac_omega_grt_0p25vmax":
        covering_frac = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "mass_grt_0p25vmax":
        mshell_wind = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50fw_grt_0p25vmax":
        P50fw_wind = np.nanmedian(input_list[i_name][ok],axis=0)

    if name == "P50fw_grt_0p0vmax":
        P50fw_check0 = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50fw_grt_0p125vmax":
        P50fw_check0p125 = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50fw_grt_0p5vmax":
        P50fw_check0p5 = np.nanmedian(input_list[i_name][ok],axis=0)
    if name == "P50fw_grt_1p0vmax":
        P50fw_check1 = np.nanmedian(input_list[i_name][ok],axis=0)


'''print Pmean
print P50fw_wind
print P50fw_0p25_vmax
print P50mw
print agrav
quit()'''

r200 = np.mean(r200[ok],axis=0)

dr = 0.1
rbins = np.arange(0,1.1,dr)
rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
n_rbins = len(rmid)

dP50 = P50mw[1:] - P50mw[0:-1]
dP50fw = P50fw_0p25_vmax[1:] - P50fw_0p25_vmax[0:-1]
dP50fw_wind = P50fw_wind[1:] - P50fw_wind[0:-1]
dPmean = Pmean[1:] - Pmean[0:-1]
kpc =  3.0857e19 # m
Msun = 1.989e30
dPmeandr = np.zeros_like(dP50)
dP50dr = np.zeros_like(dP50)
dP50fwdr = np.zeros_like(dP50)
dP50fw_wind_dr = np.zeros_like(dP50)
a_pressure_mean = np.zeros_like(dP50)
a_pressure_med = np.zeros_like(dP50)
a_pressure_fwmed = np.zeros_like(dP50)

a_pressure_fwmed2 = np.zeros_like(dP50)

rmid_mid = 0.5*(np.array(rmid)[1:]+np.array(rmid)[0:-1])
for n in range(len(rmid)-1):
    dPmeandr[n] = dPmean[n] / (dr*r200*kpc) # kg m^-2 s^-2
    dP50dr[n] = dP50[n] / (dr*r200*kpc) # kg m^-2 s^-2
    dP50fwdr[n] = dP50fw_wind[n] / (dr*r200*kpc) # kg m^-2 s^-2
    dP50fw_wind_dr[n] = dP50fw[n] / (dr*r200*kpc) # kg m^-2 s^-2

    mshell_n = 0.5*(mshell[n]+mshell[n+1])

    a_pressure_med[n] = -(dP50dr[n] * (dr*r200*kpc) * 4*np.pi*(rmid_mid[n]*r200*kpc)**2)/(mshell_n*Msun) # m s^-2
    a_pressure_fwmed[n] = -(dP50fwdr[n] * (dr*r200*kpc) * 4*np.pi*(rmid_mid[n]*r200*kpc)**2)/(mshell_n*Msun) # m s^-2
    a_pressure_mean[n] = -(dPmeandr[n] * (dr*r200*kpc) * 4*np.pi*(rmid_mid[n]*r200*kpc)**2)/(mshell_n*Msun) # m s^-2

    covering_frac_n = 0.5*(covering_frac[n] + covering_frac[n+1]) 
    mshell_wind_n = 0.5*(mshell_wind[n] + mshell_wind[n+1]) 

    a_pressure_fwmed2[n] = -(dP50fw_wind_dr[n] * (dr*r200*kpc) * covering_frac_n*4*np.pi*(rmid_mid[n]*r200*kpc)**2)/(mshell_wind_n*Msun) # m s^-2

'''from utilities_plotting import *

py.figure()

py.subplot(321)
py.plot(rmid, np.log10(dmdr), c="k")
py.plot(rmid, np.log10(dmdr_0vmax), c="r")
py.plot(rmid, np.log10(dmdr-dmdr_0vmax), c="b")
py.plot(rmid, np.log10(dmdr_0p25vmax), c="r",linestyle='--')

py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

py.subplot(322)

py.plot(rmid, np.log10(dpdr), c="k")
py.plot(rmid, np.log10(dpdr_0vmax), c="r")
py.plot(rmid, np.log10(dpdr-dpdr_0vmax), c="b")
py.plot(rmid, np.log10(dpdr_0p25vmax), c="r",linestyle='--')

py.subplot(323)

py.plot(rmid, np.log10(dEkdr+dEtdr), c="k")
py.plot(rmid, np.log10(dEkdr_0vmax+dEtdr_0vmax), c="r")
py.plot(rmid, np.log10(dEkdr-dEkdr_0vmax+dEtdr-dEtdr_0vmax), c="b")
py.plot(rmid, np.log10(dEkdr_0p25vmax+dEtdr_0p25vmax), c="r",linestyle='--')

py.subplot(324)
#py.plot(rmid, np.log10(dmdt), c="k")
py.plot(rmid, np.log10(dmdt_0vmax), c="r")
#py.plot(rmid, np.log10(dmdt-dmdr_0vmax), c="b")
py.plot(rmid, np.log10(dmdt_0p25vmax), c="r",linestyle='--')

py.subplot(325)
py.plot(rmid, np.log10(P50mw), c="k")
py.plot(rmid, np.log10(P50Vw), c="k",linestyle=':')
py.plot(rmid, np.log10(Pmean), c="k",linestyle='--')
py.plot(rmid, np.log10( P50fw_0p25_vmax),c="r")
py.ylabel("P")

py.subplot(326)
py.plot(rmid, np.log10(agrav), c="k")
py.plot(rmid_mid, np.log10(a_pressure_med), c="r")
py.plot(rmid_mid, np.log10(a_pressure_mean), c="r",linestyle='--')
py.plot(rmid_mid, np.log10(a_pressure_fwmed), c="r",linestyle=':')
py.plot(rmid_mid, np.log10(a_pressure_fwmed2), c="r",linestyle='-.')
py.ylabel("a")

py.show()'''

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64*0.5,4.98*1.5],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.06,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

py.figure()
np.seterr(all='ignore')
nrow = 3; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 2:
        py.plot(rmid, np.log10(dmdr), c= "k", label = r"Total gas",linewidth=1)
        py.plot(rmid, np.log10(dmdr_0vmax), c= "k", label = r"Outflowing gas",linewidth=1,linestyle=':')
        py.plot(rmid, np.log10(dmdr_0p25vmax), c= "k", label = r"Wind $(v_{\mathrm{rad}} > 0.25 V_{\mathrm{max}})$",linestyle='--',linewidth=1)

        py.ylabel(r"$\log_{10}\left(\frac{\mathrm{d}m}{\mathrm{d}(r/R_{\mathrm{vir}})} \, / \mathrm{M_\odot}\right)$")

        py.legend(loc='lower right',frameon=False,numpoints=1)
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
        py.ylim((9.6,10.99))

    '''if i == 1:
        py.plot(rmid, np.log10(dmdt_0vmax), c= "k", label = r"Outflowing gas",linewidth=1,linestyle='--')
        py.plot(rmid, np.log10(dmdt_0p25vmax), c= "k", label = r"Wind $(v_{\mathrm{rad}} > 0.25 V_{\mathrm{max}})$",linestyle=':',linewidth=1)

        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \mathrm{M_\odot yr^{-1}})$")

        py.legend(loc='lower right',frameon=False,numpoints=1)'''

    if i == 0:

        # Joop commented that pressure is usually plotted as P/kb in units of cm^-3 K
        # My P50 is in kg m^-1 s^-2
        kb = 1.38064852 *10**(-23) # m^2 kg s-2 K-1
        # P50 / kb > m^-3 K
        norm = 1e-6 / kb # Transforms to P / kb / (cm^-3 K)

        py.plot(rmid, np.log10(P50mw*norm), c="k",linewidth=1,label=r"Total gas (mass-weighted)")
        py.plot(rmid, np.log10(P50Vw*norm), c="k",linewidth=1,linestyle=':',label=r"Total gas (volume-weighted)")
        py.plot(rmid, np.log10(P50fw_wind*norm), c="k",linestyle='--',linewidth=1,label=r"Wind (flux-weighted, $v_{\mathrm{rad}} > 0.25 V_{\mathrm{max}}$)")

        #py.plot(rmid, np.log10(P50fw_check0), c="k",linestyle='-.',linewidth=1,label=r"Wind $(v_{\mathrm{rad}} > 0)$")
        #py.plot(rmid, np.log10(P50fw_check0p125), c="k",linestyle=':',linewidth=1,label=r"Wind $(v_{\mathrm{rad}} > 0.125 V_{\mathrm{max}})$")
        #py.plot(rmid, np.log10(P50fw_check0p5), c="k",linestyle='--',linewidth=1,label=r"Wind $(v_{\mathrm{rad}} > 0.5 V_{\mathrm{max}})$")
        #py.plot(rmid, np.log10(P50fw_check1), c="k",linestyle='-.',linewidth=1,label=r"Wind $(v_{\mathrm{rad}} > V_{\mathrm{max}})$")
        
        py.ylabel(r"$\log_{10}(P \, / k_{\mathrm{B}} \, / \mathrm{cm^{-3} K})$")
        py.legend(loc='lower left',frameon=False,numpoints=1,handlelength=2)

        #py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

    if i == 1:
        # Convert into km^-1 yr^-1
        s2yr = 60**2 * 24 * 365.25
        norm = 1e-3*s2yr
        py.plot(rmid, np.log10(norm*agrav), c= "k", label = r"Gravity",linewidth=1)
        py.plot(rmid_mid, np.log10(norm*a_pressure_med), c="r", label = r"Pressure (Total gas)",linewidth=1)
        py.plot(rmid_mid, np.log10(norm*a_pressure_fwmed2), c="r",linestyle='--', label=r"Pressure (Wind)",linewidth=1)

        py.ylabel(r"$\log_{10}(\dot{v}_{\mathrm{rad}} \, / \mathrm{km s^{-1} yr^{-1}})$")

        py.legend(loc='upper right',frameon=False,numpoints=1)


        ylo = -8; yhi = -5.4
        py.ylim((ylo,yhi))

fig_name = "pressure_gradients_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
