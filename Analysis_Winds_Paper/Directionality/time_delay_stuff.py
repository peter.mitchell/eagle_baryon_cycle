import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

test_mode = False
subsample = False
n_per_bin = 1000

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 50 Mpc REF with 28 snaps
#sim_name = "L0050N0752_REF_snap"
#nsub1d = 1
#snip_list = np.arange(0,29)[::-1]
#snap_list = np.arange(0,29)[::-1]
#snap = 25

# 25 Mpc Ref with 28 snaps
#sim_name = "L0025N0376_REF_snap"
#snip_list = np.arange(0,29)[::-1]
#snap_list = np.arange(0,29)[::-1]
#nsub1d = 1
#snap = 25

# 25 Mpc REF with 200 snips 
sim_name = "L0025N0376_REF_200_snip"
snap_final = 200
snap_list = np.arange(1,snap_final+1)[::-1] # Note this one starts at one for some reason
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
snap = 175 # z=0.25
#snap = 100 # z=2
nsub1d = 1



snip = snip_list[np.argmin(abs(snap_list-snap))]

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if "L0025" in sim_name:
        subvol_choose = [36]
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = [2000]
    else:
        print "err"
        quit()


output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)
    if subsample:
        output_base += "_subsample_"+str(n_per_bin)
    output_base += "_test_mode/"

subhalo_prop_names = ["m200_host","r200_host","subgroup_number"]
subhalo_prop_types =  ["float","float","int"]

name_list = ["mass_stars_30kpc","mass_new_stars_init"]
name_list += ["time_join_halo_wind_from_ism"]

print "plotting for snapshot", snap


############# Read data ###################################

output_path = output_base + "Snip_"+str(snip)+"/"

sub_input_list = []
input_list = []
for name in name_list:
    input_list.append([])   
for name in subhalo_prop_names:
    sub_input_list.append([])

# Find all the subvolume files (they won't be sorted in order)
subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

if len(subvols) == 0:
    print "There are no subvolume files. Run the pipeline first"
    File.close()
    quit()

# Gather together measurements
for n, subvol in enumerate(subvols):

    if test_mode:
        skip = True
        for subvol_i in subvol_choose:
            if "_"+str(subvol_i)+".hdf5" in subvol:
                skip = False
        if skip:
            continue

    try:
        File_subvol = h5py.File(subvol,"r")
    except:
        print "Couldn't read subvol file", subvol, "moving on"
        continue

    print "Reading", subvol

    try:
        halo_group = File_subvol["subhalo_data"]
    except:
        print "Read failed, skipping this subvol"
        continue

    redshifts = File_subvol["tree_snapshot_info/redshifts_tree"][:]
    snapshots_tree = File_subvol["tree_snapshot_info/snapshots_tree"][:]
    redshift = redshifts[np.argmin(abs(snapshots_tree-snap))]

    if name_list[-1] in halo_group and name_list[0] in halo_group:
        for i_name, name in enumerate(name_list):

            # Deal with 2d arrays
            if input_list[i_name] == [] and len(halo_group[name][:].shape) ==2:
                input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1]))
            elif input_list[i_name] == [] and len(halo_group[name][:].shape) ==3:
                 input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1],halo_group[name][:].shape[2]))

            input_list[i_name] = np.append(input_list[i_name], halo_group[name][:],axis=0)

        for i_name, name in enumerate(subhalo_prop_names):

            prop_type = subhalo_prop_types[i_name]
            if prop_type == "int":
                if n == 0:
                    sub_input_list[i_name] = np.array([]).astype("int")
                temp = halo_group[name][:]

            elif prop_type == "float":
                temp = halo_group[name][:]
            else:
                "Error: prop type",prop_type,"not understood"
                quit()

            sub_input_list[i_name] = np.append(sub_input_list[i_name], temp)

    else:
        print "subvol",n,"didn't contain any haloes"

    File_subvol.close()

# Do mean stacking for v>0.25Vmax for now, over some finite halo mass interval
#mhalo_lo = 12.0
#mhalo_hi = 12.2

mhalo_lo = 13.0
mhalo_hi = 13.2

mhalo = sub_input_list[0]
subgroup_number = sub_input_list[2]
ok = (np.log10(mhalo) > mhalo_lo) & (np.log10(mhalo) < mhalo_hi) & (subgroup_number==0)

for i_name, name in enumerate(name_list):

    #name_list = ["mass_stars_30kpc","mass_new_stars_init"]
    #name_list += ["time_join_halo_wind_from_ism"]

    if name == "time_join_halo_wind_from_ism":
        tcross = np.nanmedian(input_list[i_name][ok],axis=0)

    if name == "mass_stars_30kpc":
        mstars = np.nanmedian(input_list[i_name][ok],axis=0)

print ""
print "median crossing time for haloes selected at z=",redshift,"and with mhalo",mhalo_lo,mhalo_hi, "is", tcross, "Gyr"
print ""

n_track = 50
mstardot_track = np.zeros(n_track)
time_track = np.zeros(n_track)
redshift_track = np.zeros(n_track)

for i_snap in range(n_track):
    snap_i = snap - i_snap 
    snip_i = snip_list[np.argmin(abs(snap_list-snap_i))]

    output_path = output_base + "Snip_"+str(snip_i)+"/"
    subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip_i)+'_*')
    # Only works for nsub1d =1 
    subvol = subvols[0]
    File_subvol = h5py.File(subvol,"r")
    snapshots_tree = File_subvol["tree_snapshot_info/snapshots_tree"][:]
    redshifts = File_subvol["tree_snapshot_info/redshifts_tree"][:]
    redshift = redshifts[np.argmin(abs(snapshots_tree-snap_i))]
    times = File_subvol["tree_snapshot_info/cosmic_times_tree"][:]
    time = times[np.argmin(abs(snapshots_tree-snap_i))]

    time_track[i_snap] = time
    redshift_track[i_snap] = redshift

    time_ps = times[np.argmin(abs(snapshots_tree-snap_i+1))]
    dt = time - time_ps

    halo_group = File_subvol["subhalo_data"]
    progen_group = File_subvol["progenitor_data"]
    node_index_ts = halo_group["node_index"][:]
    node_index_ps = progen_group["node_index"][:]
    
    if i_snap == 0:
        track = node_index_ps[ok]
        mstardot_track[i_snap] = np.nanmedian(halo_group["mass_new_stars_init"][:][ok] / (dt*1e9)) # Msun yr^-1

    else:
        ptr = ms.match(node_index_ts, track)
        ok = ptr >= 0


        ptr2 = ms.match(track, node_index_ts)
        ok2 = ptr2 >= 0

        track = np.zeros_like(track)-1
        track[ok2] = node_index_ps[ptr2][ok2]
        #print "Kept track of ", len(track[track>=0] ) / float(len(track))

        mstardot_ts = np.zeros(len(track))+np.nan
        mstardot_ts[ok2] = halo_group["mass_new_stars_init"][:][ptr2][ok2] / (dt*1e9)

        mstardot_track[i_snap] = np.nanmedian(mstardot_ts) # Msun yr^-1
        
    File_subvol.close()

#print mstardot_track
#print time_track

from utilities_plotting import *
py.plot(time_track, mstardot_track)
py.axvline(time_track[0]-tcross)
py.show()
