import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"
#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/"
#nsnap =50
#tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"

# 25 Mpc ref with 50 tree snapshots
sim_name = "L0025N0376_REF_50_snip"
sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/"
snap =50
tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"
#tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
#nsnap = 28
#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["ism_wind_tot_ml_med","ism_wind_tot_ml_lo","ism_wind_tot_ml_hi","halo_wind_tot_ml_med","halo_wind_tot_ml_lo","halo_wind_tot_ml_hi"]
f_name_list += ["ism_wind_tot_ml","halo_wind_tot_ml", "ism_wind_complete", "halo_wind_complete"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

for f_name in f_name_list:
    if "_lo" in f_name:
        #f_med_dict[f_name][np.isnan(f_med_dict[f_name])] = 1e-9
        f_med_dict[f_name][f_med_dict[f_name]==0] = 1e-9
        f_med_dict[f_name][f_med_dict[f_name]==np.inf] = np.nan

    elif "_hi" in f_name:
        #f_med_dict[f_name][np.isnan(f_med_dict[f_name])] = 1e9
        f_med_dict[f_name][f_med_dict[f_name]==0] = 1e-9
        f_med_dict[f_name][f_med_dict[f_name]==np.inf] = np.nan

    elif "_med" in f_name:
        f_med_dict[f_name][f_med_dict[f_name]==np.inf] = np.nan

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':7})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

n_list = [0,3,0,3]

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0 or i == 1:
        quant = "ism_wind_tot_ml"
        complete = np.where(f_med_dict["ism_wind_complete"][n_list[i]]==1)[0][0]
    else:
        quant = "halo_wind_tot_ml"
        complete = np.where(f_med_dict["halo_wind_complete"][n_list[i]]==1)[0][0]
    py.axvline(bin_mh_mid[complete],c="k",linestyle='--')

    #print f_med_dict["ism_wind_complete"][n_list[i]]
    #print bin_mh_mid[complete]
    #quit()

    py.fill_between(bin_mh_mid, np.log10(f_med_dict[quant+"_lo"][n_list[i]]), np.log10(f_med_dict[quant+"_hi"][n_list[i]]),color=c_list[n_list[i]],alpha=0.1)

    py.plot(bin_mh_mid,np.log10(f_med_dict[quant][n_list[i]]),c=c_list[n_list[i]],label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n_list[i]]-1,1./a_min[n_list[i]]-1)))
    py.scatter(bin_mh_mid,np.log10(f_med_dict[quant][n_list[i]]),c=c_list[n_list[i]],edgecolors="none",s=5)
        
    py.plot(bin_mh_mid,np.log10(f_med_dict[quant+"_med"][n_list[i]]),c=c_list[n_list[i]],linestyle='--')
    py.plot(bin_mh_mid,np.log10(f_med_dict[quant+"_lo"][n_list[i]]),c=c_list[n_list[i]],linestyle=':')
    py.plot(bin_mh_mid,np.log10(f_med_dict[quant+"_hi"][n_list[i]]),c=c_list[n_list[i]],linestyle=':')

    py.legend(loc='upper right',frameon=False,numpoints=1)


    if i == 2 or i == 3:
        py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

    
    if i == 0:
        py.ylabel(r"$\log(\dot{M}_{\mathrm{wind,ism}} \, / \dot{M}_\star )$")
        ylo = -0.49; yhi = 2.0
    if i == 2:
        ylo = 0.01; yhi = 2.5
        py.ylabel(r"$\log(\dot{M}_{\mathrm{wind,halo}} \, / \dot{M}_\star )$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_distns_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_distns_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()


