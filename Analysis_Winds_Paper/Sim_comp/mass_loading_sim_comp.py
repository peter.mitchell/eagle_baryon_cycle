import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

plot_ewind = False

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

sim_name_list = ["L0025N0376_REF_200_snip", "L0025N0752_Recal"]
label_list = ["Ref (25)", "Recal (25)"]

# M200 limit for 100 star particles (taken from Joop's paper)
m200_lim = [10.8, 10.4]

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
output = []

for sim_name in sim_name_list:
    if test_mode:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
    else:
        print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str]

    vmin_i = 0.0
    f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
    f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
    f_name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
    
    f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
    f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
    
    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    if show_incomplete and complete_cut == "1newstar":
        file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
        f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
        for f_name in f_name_list2:
            f_med_dict[f_name] = file_grid2[f_name][:]
        file_grid2.close()

    output_i = bin_mh_mid, a_max, a_min, f_med_dict
    output.append(output_i)

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 9.75; xhi = 13.5

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1
ls_list = ["-", "--","-.",":"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    #show_z = [0,1,2,3,4,5,6]
    show_z = [0,2,4]

    if i == 0:
        for n in range(len(a_grid)):
            if n in show_z:
                for i_sim, sim_name in enumerate(sim_name_list):
                    
                    bin_mh_mid, a_max, a_min, f_med_dict = output[i_sim]
                    ml_ism = f_med_dict["ism_wind_tot_ml"+fV_str]
                    
                    if i_sim == 0:
                        label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                    else:
                        label = ""

                    if show_incomplete:

                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        else:
                            ok = bin_mh_mid >= m200_lim[i_sim]

                        ind = np.max(np.where(ok==False)[0])+2

                        temp = np.log10(ml_ism[n])
                        py.plot(bin_mh_mid[0:ind], temp[0:ind], c=c_list[n],linewidth=lw*0.8, linestyle=ls_list[i_sim], alpha=0.3)
                        temp[ok==False] = np.nan
                        py.plot(bin_mh_mid,temp,label=label,c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

                    else:
                        py.plot(bin_mh_mid,np.log10(ml_ism[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                    
        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")

        py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -0.5; yhi = 2.5

    if i == 1:

        if plot_ewind:
            py.annotate(r"$r = 0.15 \, R_{\mathrm{vir}}$",(10.0,5.75))
        else:
            py.annotate(r"$r = 0.15 \, R_{\mathrm{vir}}$",(12.5,0.75))

        for n in range(len(a_grid)):
            if n in show_z:
                for i_sim, sim_name in enumerate(sim_name_list):
                    
                    bin_mh_mid, a_max, a_min, f_med_dict = output[i_sim]
                    i_r = 1
                    dmdt = f_med_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"][:,:,i_r]
                    dEdt = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][:,:,i_r]
                    dEdt += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][:,:,i_r]
                    ewind = dEdt / dmdt

                    dEdt_vcnorm = f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][:,:,i_r]
                    dEdt_vcnorm += f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][:,:,i_r]

                    if i_sim == 0:
                        label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                    else:
                        label = ""

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        else:
                            ok = bin_mh_mid >= m200_lim[i_sim]
                        ind = np.max(np.where(ok==False)[0])+2

                        if plot_ewind:
                            temp = np.log10(ewind[n])
                        else:
                            temp = np.log10(dEdt_vcnorm[n])
                        py.plot(bin_mh_mid[0:ind], temp[0:ind], c=c_list[n],linewidth=lw*0.8, linestyle=ls_list[i_sim], alpha=0.3)
                        temp[ok==False] = np.nan
                        py.plot(bin_mh_mid,temp,label=label,c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

                    else:
                        if plot_ewind:
                            py.plot(bin_mh_mid,np.log10(ewind[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                        else:
                            py.plot(bin_mh_mid,np.log10(dEdt_vcnorm[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))

        if plot_ewind:
            py.ylabel(r"$\log_{10}(\langle \dot{E}_{\mathrm{wind}} \rangle \, / \langle \dot{M}_{\mathrm{wind}} \rangle \, / \mathrm{km^2 s^{-2}})$")
        else:
            py.ylabel(r"$\log_{10}(\langle\dot{E}_{\mathrm{out}}\rangle \, / 0.5 f_{\mathrm{b}} M_{200} V_{\mathrm{c}}^2 \, / \mathrm{Gyr^{-1}})$")

        #py.legend(loc='upper right',frameon=False,numpoints=1)
        
        if plot_ewind:
            ylo = 4.01; yhi = 6.0
        else:
            ylo = -1.99; yhi = 1.0
        

    if i == 2:
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):
                    
                    bin_mh_mid, a_max, a_min, f_med_dict = output[i_sim]
                    ml_halo = f_med_dict["halo_wind_tot_ml"+fV_str]

                    if n == show_z[0]:
                        label = label_list[i_sim]
                    else:
                        label = ""

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        else:
                            ok = bin_mh_mid >= m200_lim[i_sim]
                        ind = np.max(np.where(ok==False)[0])+2

                        temp = np.log10(ml_halo[n])
                        py.plot(bin_mh_mid[0:ind], temp[0:ind], c=c_list[n],linewidth=lw*0.8, linestyle=ls_list[i_sim], alpha=0.3)
                        temp[ok==False] = np.nan
                        py.plot(bin_mh_mid,temp,c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=label)

                    else:
                        py.plot(bin_mh_mid,np.log10(ml_halo[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=label)


        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        py.legend(loc='upper right',frameon=False,numpoints=1)
      
        ylo = -0.5; yhi = 2.49

    if i == 3:
        if plot_ewind:
            py.annotate(r"$r = 0.95 \, R_{\mathrm{vir}}$",(10.0,5.75))
        else:
            py.annotate(r"$r = 0.95 \, R_{\mathrm{vir}}$",(12.5,0.75))
        for n in range(len(a_grid)):
            if n in show_z:
                for i_sim, sim_name in enumerate(sim_name_list):
                    
                    bin_mh_mid, a_max, a_min, f_med_dict = output[i_sim]
                    i_r = 9
                    dmdt = f_med_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"][:,:,i_r]
                    dEdt = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][:,:,i_r]
                    dEdt += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][:,:,i_r]                    
                    ewind = dEdt / dmdt

                    dEdt_vcnorm = f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][:,:,i_r]
                    dEdt_vcnorm += f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][:,:,i_r]

                    if i_sim == 0:
                        label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                    else:
                        label = ""

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        else:
                            ok = bin_mh_mid >= m200_lim[i_sim]
                        ind = np.max(np.where(ok==False)[0])+2
                        
                        if plot_ewind:
                            temp = np.log10(ewind[n])
                        else:
                            temp = np.log10(dEdt_vcnorm[n])
                        py.plot(bin_mh_mid[0:ind], temp[0:ind], c=c_list[n],linewidth=lw*0.8, linestyle=ls_list[i_sim], alpha=0.3)
                        temp[ok==False] = np.nan
                        py.plot(bin_mh_mid,temp,label=label,c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

                    else:
                        if plot_ewind:
                            py.plot(bin_mh_mid,np.log10(ewind[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                        else:
                            py.plot(bin_mh_mid,np.log10(dEdt_vcnorm[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))

        if plot_ewind:
            py.ylabel(r"$\log_{10}(\langle \dot{E}_{\mathrm{wind}} \rangle \, / \langle \dot{M}_{\mathrm{wind}} \rangle \, / \mathrm{km^2 s^{-2}})$")
        else:
            py.ylabel(r"$\log_{10}(\langle\dot{E}_{\mathrm{out}}\rangle \, / 0.5 f_{\mathrm{b}} M_{200} V_{\mathrm{c}}^2 \, / \mathrm{Gyr^{-1}})$")

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -2.0; yhi = 1.

        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "mass_loading_sim_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
