import sys
sys.path.append("../Hydro_comparison/")

import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as us

plot = "TNG_vcut"
plot = "TNG_rshell"
plot = "FIRE"
plot = "FIRE_mhalo"


fVmax_cut = 0.25

test_mode = False

show_incomplete = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path2 = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 50 tree snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################


############ Read shell data ##################
if test_mode:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

#for n in range(len(rvir_med[3])):
#    print rvir_med[3][n], np.log10(mstar_med[3][n])
#quit()
# From this, the stellar mass corresponding to a virial radius of 50kpc is about 10^9
# 20kpc corresponds to about 10^7 - so no issue

f_name_list = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_50"]
for i in range(len(f_name_list)):
    f_name_list[i] += "_med_mstar"
f_name_list2 = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_0", "ml_rvir_cvmin_50", "ml_rvir_cvmin_150","ml_rvir_cvmin_250","ml_rvir_cvmin_250","ml_0p25rvir_cvmin_0"]
for i in range(len(f_name_list2)):
    f_name_list2[i] += "_mstar"
f_name_list+=f_name_list2
f_name_list += ["ml_0p25rvir_cvmin_0"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

logmstar_tng, ml_tng, type_tng = np.loadtxt("../Hydro_comparison/tng_outflow_data.dat",unpack=True)

 
############ Read shell data matched to Horizon-AGN ####################
# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"
# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

if test_mode:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_hagn_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_hagn.hdf5","r")

f_name_list4 = ["dmdt_out_r0p95rvir_subnorm_med_mstar","dmdt_out_r0p2rvir_subnorm_med_mstar"]
f_med_dict4 = {}
for f_name in f_name_list4:
    f_med_dict4[f_name] = file_grid[f_name][:]

file_grid.close()

############## Read standard particle track data ##############

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"

if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
bin_vcirc_mid = file_grid["log_vcirc_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]
rvir_med = file_grid["R200_med"][:]
mstar_med = file_grid["mstar_med"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str+"_vcirc","ism_wind_cum_ml_pp"+fV_str+"_mstar","ism_wind_cum_ml_pp"+fV_str,"halo_wind_tot_ml"+fV_str+"_vcirc"]
#f_name_list = ["ism_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str+"_vcirc","halo_wind_tot_ml"+fV_str+"_vcirc"]

f_med_dict2 = {}
for f_name in f_name_list:
    f_med_dict2[f_name] = file_grid[f_name][:]

file_grid.close()

file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str,"ism_wind_cum_ml_pp_complete"+fV_str+"_mstar","ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc"]
f_name_list2 += ["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar", "ism_wind_cum_ml_pp_complete2"+fV_str+"_vcirc"]
for f_name in f_name_list2:
    f_med_dict2[f_name] = file_grid2[f_name][:]

file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.21,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.99,
                    'font.size':8,
                    'axes.labelsize':11,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

linewidth = 1.0

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)


for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    ylo = -0.5; yhi = 2.0

    # Tng comparison 1
    if plot == "TNG_vcut":

        if show_incomplete:
            ok = bin_mstar_mid > 0
        else:
            ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)

        #label=(r"$z=%3.1f$" % (1./a_max[3]-1))
        #l1, = py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_0_med_mstar"][3][ok]),label=r"EAGLE",c="b",linewidth=linewidth)
        
        l3 = py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3][ok]),c="b",linestyle='-',linewidth=linewidth,label=r"EAGLE")
        
        l4, = py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3][ok]),c="b",linestyle='--',linewidth=linewidth)

        #ok = type_tng == 1
        #py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='-',label="TNG",linewidth=linewidth)

        ok = type_tng == 2
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='-',linewidth=linewidth,label="TNG")

        ok = type_tng == 3
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='--',linewidth=linewidth)

        ok = type_tng == 0
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle=':',alpha=0.7,linewidth=linewidth)
        
    
        #py.plot(bin_mh_mid, -1/3. * bin_mh_mid + 4.3333,linestyle='--',c="k")
        #py.plot(bin_mh_mid, -2/3. * bin_mh_mid + 7.6666,linestyle='--',c="b")
        #py.plot(bin_mh_mid, -0.5 * bin_mh_mid + 6,linestyle='--',c="b")

        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1,handlelength=1.5,bbox_to_anchor=(0.1,1.0))
        py.gca().add_artist(legend1)

        #l5, = py.plot([-20,-20],[-20,20],label=r"$v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",c="k",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],c="k",linestyle='-',label=r"$v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$",linewidth=linewidth)
        l7, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=r"$v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$",linewidth=linewidth)
        l8, = py.plot([-20,-20],[-20,20],c="k",linestyle=':',label="Injection (TNG)",linewidth=linewidth)
        py.legend(handles=[l6,l7,l8],labels=[r"$v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$",r"$v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$","Injection (TNG)"], loc="center right",frameon=False,numpoints=1,handlelength=3,bbox_to_anchor=(0.92,0.8))


        #plot_lines= [l1, l2]
        #py.legend([l[0] for l in plot_lines], ["Eagle", "TNG"], loc=4)
        xlo = 7.5; xhi = 11.5
        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot}) \, [r < 30 \, \mathrm{kpc}]$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}}(r = 10 \, \mathrm{kpc}) \, / \dot{M}_\star )$")


    # Tng radial comparison
    elif plot == "TNG_rshell":
        xlo = 7.5; xhi = 11.5

        if show_incomplete:
            ok = bin_mstar_mid > 0
        else:
            ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)

        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_50_med_mstar"][3][ok]),c="b",linewidth=linewidth)
        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r20_cvmin_50_med_mstar"][3][ok]),c="b",linestyle='--',linewidth=linewidth)
        ok2 = ok & (bin_mstar_mid > 9)
        py.plot(bin_mstar_mid[ok2],np.log10(f_med_dict["ml_r50_cvmin_50_med_mstar"][3][ok2]),c="b",linestyle=':',linewidth=linewidth)
        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_rvir_cvmin_50_med_mstar"][3][ok]),c="b",linestyle='-.',linewidth=linewidth)

        ok = type_tng == 4
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linewidth=linewidth)
        ok = type_tng == 5
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='--',linewidth=linewidth)
        ok = type_tng == 6
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle=':',linewidth=linewidth)

        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",label=r"$r = 10 \, \mathrm{kpc}$",linewidth=linewidth)
        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",linestyle='--',label="$r = 20 \, \mathrm{kpc}$",linewidth=linewidth)
        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",linestyle=':',label="$r = 50 \, \mathrm{kpc}$",linewidth=linewidth)
        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",linestyle='-.',label="$r = r_{\mathrm{vir}}$",linewidth=linewidth)
        

        '''py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3]),c="orange")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_150_med_mstar"][3]),c="orange",linestyle='--')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r50_cvmin_150_med_mstar"][3]),c="orange",linestyle=':')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_150_mstar"][3]),c="orange",linestyle='-.')
        
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3]),c="g")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_250_med_mstar"][3]),c="g",linestyle='--')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r50_cvmin_250_med_mstar"][3]),c="g",linestyle=':')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_250_mstar"][3]),c="g",linestyle='-.')'''

        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3,bbox_to_anchor=(0.92,1.0))
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label="EAGLE",c="b",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],label="TNG",c="r",linewidth=linewidth)
        py.legend(handles=[l5,l6],labels=["EAGLE","TNG"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)
        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot}) \, [r < 30 \, \mathrm{kpc}]$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")

    # Particle tracking and 0.25 Rvir Eulerian comparison as a function of mstar - Anglez-Alcazar (FIRE), Muratov (FIRE)
    elif plot == "FIRE":

        stellar_mass_comp = True

        if stellar_mass_comp:

            if show_incomplete:
                ok = bin_mstar_mid > 0
            else:
                ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][0] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][0] == 0) # Assuming these measurements correspond to lowest redshift bin

            #py.plot(bin_mstar_mid[ok],np.log10(f_med_dict2["ism_wind_cum_ml_pp"+fV_str+"_mstar"][ok]),c="b",label="EAGLE",linewidth=linewidth)
            py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_0p25rvir_cvmin_0_mstar"][0][ok]),c="b",linestyle='-',linewidth=linewidth,label="EAGLE")
            #py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_rvir_cvmin_0_mstar"][0][ok]),c="b",linestyle='-.',linewidth=linewidth)
        
            # Muratov Shell measurements at 0.25 Rvir, relation independent of redshift
            log_eta_m15 = np.log10(3.6 * (10**bin_mstar_mid / 1e10)**-0.35)
            ok = bin_mstar_mid < np.log10(2e10)
            py.plot(bin_mstar_mid[ok],log_eta_m15[ok],c="m",linestyle='-',linewidth=linewidth,label="FIRE")

            logmstar_m15, logeta_m15, xtype = np.loadtxt("../Hydro_comparison/mass_loading_muratov_15.dat",unpack=True)
            ok = xtype == 1
            logmstar_m15 = logmstar_m15[ok]; logeta_m15 = logeta_m15[ok]
            #py.scatter(logmstar_m15,logeta_m15,c="m",edgecolors="None",marker="v",s=5)

            logmstar_m17, logeta_m17, xtype = np.loadtxt("../Hydro_comparison/mass_loading_muratov_17.dat",unpack=True)
            ok = xtype == 1
            logmstar_m17 = logmstar_m17[ok]; logeta_m17 = logeta_m17[ok]
            #py.scatter(logmstar_m17,logeta_m17,c="m",edgecolors="None",marker="x",s=5)

            # Angles-Alcazar particle tracking measurements, taken from the fit in Dave 2019, cumulative measurements integrated over entire history of a galaxy
            log_eta_aa17 = np.log10(9 * (10**bin_mstar_mid / 5.2e9)**-0.317 )
            ok = bin_mstar_mid > np.log10(5.2e9)
            log_eta_aa17[ok] = np.log10(9 * (10**bin_mstar_mid[ok] / 5.2e9)**-0.761 )
            ok = bin_mstar_mid < 11
            #py.plot(bin_mstar_mid[ok],log_eta_aa17[ok],c="m",linestyle='-',linewidth=linewidth,label="FIRE")

            xlo = 7.5; xhi = 11.5
            ylo = -0.5; yhi = 2.2
    
            py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot})$")

            legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3.0,bbox_to_anchor=(0.92,1.0))
            py.gca().add_artist(legend1)

            shell1 = r"Shell, $0.25 R_{\mathrm{vir}}$"
            shell2 = r"Shell, $0.25 R_{\mathrm{vir}}$"
            track1 = r"Tracking, ISM"

            l5, = py.plot([-20,-20],[-20,20],label=track1,c="k",linewidth=linewidth)

            l6, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=shell1,linewidth=linewidth)
            #l7 = py.scatter([-20,-20],[-20,20],label=shell2,c="k",marker="v",s=5,edgecolors="none")

            #py.legend(handles=[l5,l6],labels=[track1, shell1], loc="lower left",frameon=False,numpoints=1,handlelength=3,fontsize=7)
            py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")

        
    else:
        py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.21,
                            'legend.fontsize':7})

        #py.plot(bin_mh_mid,np.log10(f_med_dict2["ism_wind_cum_ml_pp"+fV_str]),c="b",label="EAGLE, track",linewidth=linewidth)
        
        py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][0]),c="b",linestyle='-',label="EAGLE, $r = 0.25 R_{\mathrm{vir}}, z=0.25$",linewidth=linewidth)        

        # Edit: the Muratov Shell data was only measured for the z=0.25 snapshot
        #py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][0]),c="b",linestyle='-',label="EAGLE, $r = 0.25 R_{\mathrm{vir}}, %3.1f < z < %3.1f$" % (1./a_max[0]-1,1./a_min[2]-1),linewidth=linewidth)
        #py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][2]),c="b",linestyle='--',label="EAGLE, $r = 0.25 R_{\mathrm{vir}}, %3.1f < z < %3.1f$" % (1./a_max[2]-1,1./a_min[2]-1),linewidth=linewidth)
        #py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][4]),c="b",linestyle=':',label="EAGLE, $r = 0.25 R_{\mathrm{vir}}, %3.1f < z < %3.1f$" % (1./a_max[4]-1,1./a_min[4]-1),linewidth=linewidth)

        # z=0.25
        Mh60 = 8.3*10**10 # valid at z=0.25
        log_eta_m15 = np.log10( (1+0.25)**1.3 * 2.9*(10**bin_mh_mid / Mh60)**-1.1)
        ok = 10**bin_mh_mid > Mh60
        log_eta_m15[ok] = np.log10( (1+0.25)**1.3 * 2.9*(10**bin_mh_mid[ok] / Mh60)**-0.33)
        ok = bin_mh_mid < 12
        py.plot(bin_mh_mid[ok],log_eta_m15[ok],c="m",linestyle='-',label="FIRE, $r = 0.25 R_{\mathrm{vir}}, z=0.25$",linewidth=linewidth)

        '''# z=1.25
        Mh60 = 4.0*10**10
        log_eta_m15 = np.log10( (1+1.25)**1.3 * 2.9*(10**bin_mh_mid / Mh60)**-1.1)
        ok = 10**bin_mh_mid > Mh60
        log_eta_m15[ok] = np.log10( (1+1.25)**1.3 * 2.9*(10**bin_mh_mid[ok] / Mh60)**-0.33)
        ok = bin_mh_mid < 12
        py.plot(bin_mh_mid[ok],log_eta_m15[ok],c="m",linestyle=':',label="FIRE, $r = 0.25 R_{\mathrm{vir}}, z=1.25$",linewidth=linewidth)

        # z=3
        Mh60 = 1.8*10**10
        log_eta_m15 = np.log10( (1+3)**1.3 * 2.9*(10**bin_mh_mid / Mh60)**-1.1)
        ok = 10**bin_mh_mid > Mh60
        log_eta_m15[ok] = np.log10( (1+3)**1.3 * 2.9*(10**bin_mh_mid[ok] / Mh60)**-0.33)
        ok = bin_mh_mid < 12
        py.plot(bin_mh_mid[ok],log_eta_m15[ok],c="m",linestyle='--',label="FIRE, $r = 0.25 R_{\mathrm{vir}}, z=1.25$",linewidth=linewidth)'''

        # For halo mass, we take the Angles-Alcazar data points from figure 8, no fit is provided.
        #logmh_aa, logeta_aa = np.loadtxt("../Hydro_comparison/mass_loading_angles_alcazar17.dat",unpack=True)
        #py.scatter(logmh_aa,logeta_aa,c="m",linestyle='-',label="FIRE, track",s=3,edgecolors="None")

        xlo = 9.5; xhi = 13.5

        py.xlabel(r"$\log_{10}(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")
        ylo = -0.5; yhi = 2.2

        py.legend(loc='lower left',frameon=False,numpoints=1,handlelength=3)
    
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")


    

    
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

if plot == "TNG_vcut":
    fig_name = "mass_loading_"+sim_name+"_hydro_sim_comp_tng_vcut.pdf"
elif plot == "TNG_rshell":
    fig_name = "mass_loading_"+sim_name+"_hydro_sim_comp_tng_rshell.pdf"
elif plot == "FIRE":
    fig_name = "mass_loading_"+sim_name+"_hydro_sim_comp_FIRE.pdf"
else:
    fig_name = "mass_loading_"+sim_name+"_hydro_sim_comp_FIRE_mhalo.pdf"

py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
