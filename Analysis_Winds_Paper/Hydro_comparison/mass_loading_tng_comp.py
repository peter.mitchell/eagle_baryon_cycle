import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 50 tree snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 tree snipshots
sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_50"]
for i in range(len(f_name_list)):
    f_name_list[i] += "_med_mstar"
f_name_list2 = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_0", "ml_rvir_cvmin_50", "ml_rvir_cvmin_150","ml_rvir_cvmin_250"]
for i in range(len(f_name_list2)):
    f_name_list2[i] += "_mstar"
f_name_list+=f_name_list2

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

logmstar_tng, ml_tng, type_tng = np.loadtxt("tng_outflow_data.dat",unpack=True)

sim_name = "L0100N1504_REF_200_snip"

if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]
rvir_med = file_grid["R200_med"][:]
mstar_med = file_grid["mstar_med"][:]

#for n in range(len(rvir_med[3])):
#    print rvir_med[3][n], np.log10(mstar_med[3][n])
#quit()
# From this, the stellar mass corresponding to a virial radius of 50kpc is about 10^9
# 20kpc corresponds to about 10^7 - so no issue

f_name_list = ["ism_wind_tot_ml_mstar","halo_wind_tot_ml_mstar"]

f_med_dict2 = {}
for f_name in f_name_list:
    f_med_dict2[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 7.5; xhi = 11.5

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        #py.plot(bin_mstar_mid,np.log10(f_med_dict2["ism_wind_tot_ml_mstar"][3]),c="k")
        #py.scatter(bin_mstar_mid,np.log10(f_med_dict2["ism_wind_tot_ml_mstar"][3]),c="k",edgecolors="None")

        #py.plot(bin_mstar_mid,np.log10(f_med_dict2["halo_wind_tot_ml_mstar"][3]),c="r")
        #py.scatter(bin_mstar_mid,np.log10(f_med_dict2["halo_wind_tot_ml_mstar"][3]),c="r",edgecolors="None")

        #label=(r"$z=%3.1f$" % (1./a_max[3]-1))
        l1, = py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_0_med_mstar"][3]),label=r"$v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",c="b")

        ok = type_tng == 1
        py.plot(logmstar_tng[ok],ml_tng[ok],c="b",linestyle='--')
        
        l3 = py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3]),c="orange",label="$v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$")
        ok = type_tng == 2
        py.plot(logmstar_tng[ok],ml_tng[ok],c="orange",linestyle='--')
        
        l4, = py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3]),c="g",label="$v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$")
        ok = type_tng == 3
        py.plot(logmstar_tng[ok],ml_tng[ok],c="g",linestyle='--')
        
    
        #py.plot(bin_mh_mid, -1/3. * bin_mh_mid + 4.3333,linestyle='--',c="k")
        #py.plot(bin_mh_mid, -2/3. * bin_mh_mid + 7.6666,linestyle='--',c="b")
        #py.plot(bin_mh_mid, -0.5 * bin_mh_mid + 6,linestyle='--',c="b")

        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label="Eagle",c="k")
        l6, = py.plot([-20,-20],[-20,20],label="TNG",c="k",linestyle='--')
        py.legend(handles=[l5,l6],labels=["Eagle","TNG"], loc="upper center",frameon=False,numpoints=1,handlelength=3)


        #plot_lines= [l1, l2]
        #py.legend([l[0] for l in plot_lines], ["Eagle", "TNG"], loc=4)

    elif i == 1:
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_50_med_mstar"][3]),c="b",label=r"$r = 10 \, \mathrm{kpc}$")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_50_med_mstar"][3]),c="b",linestyle='--',label="$r = 20 \, \mathrm{kpc}$")
        ok = bin_mstar_mid > 9
        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r50_cvmin_50_med_mstar"][3][ok]),c="b",linestyle=':',label="$r = 50 \, \mathrm{kpc}$")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_50_med_mstar"][3]),c="b",linestyle='-.',label="$r = r_{\mathrm{vir}}$")

        ok = type_tng == 4
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r")
        ok = type_tng == 5
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='--')
        ok = type_tng == 6
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle=':')


        '''py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3]),c="orange")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_150_med_mstar"][3]),c="orange",linestyle='--')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r50_cvmin_150_med_mstar"][3]),c="orange",linestyle=':')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_150_mstar"][3]),c="orange",linestyle='-.')
        
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3]),c="g")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_250_med_mstar"][3]),c="g",linestyle='--')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r50_cvmin_250_med_mstar"][3]),c="g",linestyle=':')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_250_mstar"][3]),c="g",linestyle='-.')'''

        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label="Eagle",c="b")
        l6, = py.plot([-20,-20],[-20,20],label="TNG",c="r")
        py.legend(handles=[l5,l6],labels=["Eagle","TNG"], loc="upper left",frameon=False,numpoints=1,handlelength=3)


    py.ylabel(r"$\log(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")
    ylo = -0.5; yhi = 1.99

    
    py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_"+sim_name+"_tng_comp_test_mode.pdf"
else:
    fig_name = "mass_loading_"+sim_name+"_tng_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
