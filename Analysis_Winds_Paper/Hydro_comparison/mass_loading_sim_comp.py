import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as us

fVmax_cut = 0.25

test_mode = False

show_incomplete =True
complete_cut = "100star"
#complete_cut = "1newstar"

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path2 = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 50 tree snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################


############ Read shell data ##################
if test_mode:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

#for n in range(len(rvir_med[3])):
#    print rvir_med[3][n], np.log10(mstar_med[3][n])
#quit()
# From this, the stellar mass corresponding to a virial radius of 50kpc is about 10^9
# 20kpc corresponds to about 10^7 - so no issue

f_name_list = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_50"]
for i in range(len(f_name_list)):
    f_name_list[i] += "_med_mstar"
f_name_list2 = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_0", "ml_rvir_cvmin_50", "ml_rvir_cvmin_150","ml_rvir_cvmin_250","ml_rvir_cvmin_250","ml_0p25rvir_cvmin_0"]
for i in range(len(f_name_list2)):
    f_name_list2[i] += "_mstar"
f_name_list+=f_name_list2
f_name_list += ["ml_0p25rvir_cvmin_0"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

logmstar_tng, ml_tng, type_tng = np.loadtxt("tng_outflow_data.dat",unpack=True)

 
############ Read shell data matched to Horizon-AGN ####################
# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"
# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

if test_mode:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_hagn_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path2+"efficiencies_grid_"+sim_name+"_"+part_mass+"_hagn.hdf5","r")

f_name_list4 = ["dmdt_out_r0p95rvir_subnorm_med_mstar","dmdt_out_r0p2rvir_subnorm_med_mstar"]
f_med_dict4 = {}
for f_name in f_name_list4:
    f_med_dict4[f_name] = file_grid[f_name][:]

file_grid.close()

############## Read standard particle track data ##############

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"

if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
bin_vcirc_mid = file_grid["log_vcirc_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]
rvir_med = file_grid["R200_med"][:]
mstar_med = file_grid["mstar_med"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str+"_vcirc","ism_wind_cum_ml_pp"+fV_str+"_mstar","ism_wind_cum_ml_pp"+fV_str,"halo_wind_tot_ml"+fV_str+"_vcirc"]
#f_name_list = ["ism_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str+"_mstar","ism_wind_tot_ml"+fV_str+"_vcirc","halo_wind_tot_ml"+fV_str+"_vcirc"]

f_med_dict2 = {}
for f_name in f_name_list:
    f_med_dict2[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str,"ism_wind_cum_ml_pp_complete"+fV_str+"_mstar","ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc"]
    f_name_list2 += ["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar", "ism_wind_cum_ml_pp_complete2"+fV_str+"_vcirc"]
    for f_name in f_name_list2:
        f_med_dict2[f_name] = file_grid2[f_name][:]

    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.21,
                    'figure.figsize':[6.64,4.98*1.5],
                    'figure.subplot.left':0.1,
                    'figure.subplot.bottom':0.06,
                    'figure.subplot.top':0.99,
                    'font.size':8,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

linewidth = 1.0

py.figure()
np.seterr(all='ignore')
nrow = 3; ncol = 2
subplots = panelplot(nrow,ncol)


for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    ylo = -0.5; yhi = 2.0

    # Tng comparison 1
    if i == 0:

        if show_incomplete:
            if complete_cut == "1newstar":
                ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)
            else:
                ok = bin_mstar_mid >= np.log10(1.81e6)+2
        else:
            ok = bin_mstar_mid > 0

        #label=(r"$z=%3.1f$" % (1./a_max[3]-1))
        l1, = py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_0_med_mstar"][3][ok]),label=r"EAGLE",c="b",linewidth=linewidth)
        
        l3 = py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3][ok]),c="b",linestyle='--',linewidth=linewidth)
        
        l4, = py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3][ok]),c="b",linestyle='-.',linewidth=linewidth)

        ok = type_tng == 1
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='-',label="TNG",linewidth=linewidth)

        ok = type_tng == 2
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='--',linewidth=linewidth)

        ok = type_tng == 3
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='-.',linewidth=linewidth)

        ok = type_tng == 0
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle=(0, (1,1)),alpha=0.7,linewidth=linewidth)
        
    
        #py.plot(bin_mh_mid, -1/3. * bin_mh_mid + 4.3333,linestyle='--',c="k")
        #py.plot(bin_mh_mid, -2/3. * bin_mh_mid + 7.6666,linestyle='--',c="b")
        #py.plot(bin_mh_mid, -0.5 * bin_mh_mid + 6,linestyle='--',c="b")

        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1,handlelength=1.5)#,bbox_to_anchor=(0.1,1.0))
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label=r"$v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",c="k",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=r"$v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$",linewidth=linewidth)
        l7, = py.plot([-20,-20],[-20,20],c="k",linestyle='-.',label=r"$v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$",linewidth=linewidth)
        l8, = py.plot([-20,-20],[-20,20],c="k",linestyle=(0, (1,1)),label="Injection (TNG)",linewidth=linewidth)
        #py.legend(handles=[l5,l6,l7,l8],labels=[r"$v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",r"$v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$",r"$v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$","Injection (TNG)"], loc="center right",frameon=False,numpoints=1,handlelength=3,bbox_to_anchor=(0.92,0.8))
        py.legend(handles=[l5,l6,l7,l8],labels=[r"$v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",r"$v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$",r"$v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$","Injection (TNG)"], loc="upper right",frameon=False,numpoints=1,handlelength=3)


        #plot_lines= [l1, l2]
        #py.legend([l[0] for l in plot_lines], ["Eagle", "TNG"], loc=4)
        xlo = 7.5; xhi = 11.5
        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot}) \, [r < 30 \, \mathrm{kpc}]$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}}(r = 10 \, \mathrm{kpc}) \, / \dot{M}_\star )$")

        py.annotate(r"$z=2$",(8.85,1.75),fontsize=9)
        py.annotate(r"$r = 10 \, \mathrm{kpc}$",(8.58,1.48),fontsize=9)

    # Tng radial comparison
    elif i == 1:
        xlo = 7.5; xhi = 11.5

        if show_incomplete:
            if complete_cut == "1newstar":
                ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)
            else:
                ok = bin_mstar_mid >= np.log10(1.81e6)+2
        else:
            ok = bin_mstar_mid > 0

        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r10_cvmin_50_med_mstar"][3][ok]),c="b",linewidth=linewidth)
        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r20_cvmin_50_med_mstar"][3][ok]),c="b",linestyle='--',linewidth=linewidth)
        ok2 = ok & (bin_mstar_mid > 9)
        py.plot(bin_mstar_mid[ok2],np.log10(f_med_dict["ml_r50_cvmin_50_med_mstar"][3][ok2]),c="b",linestyle=':',linewidth=linewidth)
        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_rvir_cvmin_50_med_mstar"][3][ok]),c="b",linestyle='-.',linewidth=linewidth)

        ok = type_tng == 4
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linewidth=linewidth)
        ok = type_tng == 5
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle='--',linewidth=linewidth)
        ok = type_tng == 6
        py.plot(logmstar_tng[ok],ml_tng[ok],c="r",linestyle=':',linewidth=linewidth)

        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",label=r"$r = 10 \, \mathrm{kpc}$",linewidth=linewidth)
        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",linestyle='--',label="$r = 20 \, \mathrm{kpc}$",linewidth=linewidth)
        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",linestyle=':',label="$r = 50 \, \mathrm{kpc}$",linewidth=linewidth)
        py.plot(bin_mstar_mid,bin_mstar_mid-99,c="k",linestyle='-.',label="$r = r_{\mathrm{vir}}$",linewidth=linewidth)
        

        '''py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3]),c="orange")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_150_med_mstar"][3]),c="orange",linestyle='--')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r50_cvmin_150_med_mstar"][3]),c="orange",linestyle=':')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_150_mstar"][3]),c="orange",linestyle='-.')
        
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3]),c="g")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_250_med_mstar"][3]),c="g",linestyle='--')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r50_cvmin_250_med_mstar"][3]),c="g",linestyle=':')
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_250_mstar"][3]),c="g",linestyle='-.')'''

        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3,bbox_to_anchor=(0.95,1.0))
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label="EAGLE",c="b",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],label="TNG",c="r",linewidth=linewidth)
        py.legend(handles=[l5,l6],labels=["EAGLE","TNG"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)
        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot}) \, [r < 30 \, \mathrm{kpc}]$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}}(v_{\mathrm{r}} > 50 \, \mathrm{km s^{-1}}) \, / \dot{M}_\star )$")

        py.annotate(r"$z=2$",(8.9,1.75),fontsize=9)
        py.annotate(r"$v_{\mathrm{r}} > 50 \mathrm{km s^{-1}}$",(8.68,1.48),fontsize=9)

    # Particle tracking and 0.25 Rvir Eulerian comparison as a function of mstar - Anglez-Alcazar (FIRE), Muratov (FIRE)
    elif i == 2:

        stellar_mass_comp = True

        if stellar_mass_comp:

            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)
                else:
                    ok = bin_mstar_mid >= np.log10(1.81e6)+2
            else:
                ok = bin_mstar_mid > 0

            py.plot(bin_mstar_mid[ok],np.log10(f_med_dict2["ism_wind_cum_ml_pp"+fV_str+"_mstar"][ok]),c="b",label="EAGLE",linewidth=linewidth)
            py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_0p25rvir_cvmin_0_mstar"][0][ok]),c="b",linestyle='--',linewidth=linewidth)
            py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_rvir_cvmin_0_mstar"][0][ok]),c="b",linestyle='-.',linewidth=linewidth)
        
            # Muratov Shell measurements at 0.25 Rvir, relation independent of redshift
            log_eta_m15 = np.log10(3.6 * (10**bin_mstar_mid / 1e10)**-0.35)
            ok = bin_mstar_mid < np.log10(2e10)
            py.plot(bin_mstar_mid[ok],log_eta_m15[ok],c="m",linestyle='--',linewidth=linewidth)

            logmstar_m15, logeta_m15, xtype = np.loadtxt("mass_loading_muratov_15.dat",unpack=True)
            ok = xtype == 1
            logmstar_m15 = logmstar_m15[ok]; logeta_m15 = logeta_m15[ok]
            py.scatter(logmstar_m15,logeta_m15,c="m",edgecolors="None",marker="v",s=5)

            logmstar_m17, logeta_m17, xtype = np.loadtxt("mass_loading_muratov_17.dat",unpack=True)
            ok = xtype == 1
            logmstar_m17 = logmstar_m17[ok]; logeta_m17 = logeta_m17[ok]
            py.scatter(logmstar_m17,logeta_m17,c="m",edgecolors="None",marker="x",s=5)

            # Angles-Alcazar particle tracking measurements, taken from the fit in Dave 2019, cumulative measurements integrated over entire history of a galaxy
            log_eta_aa17 = np.log10(9 * (10**bin_mstar_mid / 5.2e9)**-0.317 )
            ok = bin_mstar_mid > np.log10(5.2e9)
            log_eta_aa17[ok] = np.log10(9 * (10**bin_mstar_mid[ok] / 5.2e9)**-0.761 )
            ok = bin_mstar_mid < 11
            py.plot(bin_mstar_mid[ok],log_eta_aa17[ok],c="m",linestyle='-',linewidth=linewidth,label="FIRE")

            xlo = 7.5; xhi = 11.5
            ylo = -0.65; yhi = 1.85
    
            py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot})$")

            legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3.0,bbox_to_anchor=(0.92,1.0))
            py.gca().add_artist(legend1)

            shell1 = r"Shell, $0.25 R_{\mathrm{vir}}$, $z=0.25$"
            shell2 = r"Shell, $0.25 R_{\mathrm{vir}}$, $z<0.5$"
            shell3 = r"Shell, $R_{\mathrm{vir}}$, $z=0.25$"
            shell4 = r"Shell, $R_{\mathrm{vir}}$, $z<0.5$"
            track1 = r"Tracking, galaxy scale"

            l5, = py.plot([-20,-20],[-20,20],label=track1,c="k",linewidth=linewidth)

            l6, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=shell1,linewidth=linewidth)
            l7 = py.scatter([-20,-20],[-20,20],label=shell2,c="k",marker="v",s=5,edgecolors="none")
            l8, = py.plot([-20,-20],[-20,20],label=shell3,c="k",linestyle='-.',linewidth=linewidth)
            l9 = py.scatter([-20,-20],[-20,20],label=shell4,c="k",marker="x",s=5,edgecolors="none")

            py.legend(handles=[l6,l7,l8,l9,l5],labels=[ shell1, shell2,shell3, shell4,track1], loc="lower left",frameon=False,numpoints=1,handlelength=3,fontsize=7)

        
        else:
            py.plot(bin_mh_mid,np.log10(f_med_dict2["ism_wind_cum_ml_pp"+fV_str]),c="b",label="EAGLE, track",linewidth=linewidth)
            py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][0]),c="b",linestyle='--',label="EAGLE, shell",linewidth=linewidth)

            Mh60 = 8.3*10**10 # valid at z=0.25
            log_eta_m15 = np.log10( 2.9*(10**bin_mh_mid / Mh60)**-1.1)
            ok = 10**bin_mh_mid > Mh60
            log_eta_m15[ok] = np.log10( 2.9*(10**bin_mh_mid[ok] / Mh60)**-0.33)
            py.plot(bin_mh_mid,log_eta_m15,c="m",linestyle='--',label="FIRE, shell",linewidth=linewidth)

            # For halo mass, we take the Angles-Alcazar data points from figure 8, no fit is provided.
            logmh_aa, logeta_aa = np.loadtxt("mass_loading_angles_alcazar17.dat",unpack=True)
            py.scatter(logmh_aa,logeta_aa,c="m",linestyle='-',label="FIRE, track, Angles-Alcazar et al. (2017)",s=3,edgecolors="None")

            xlo = 9.5; xhi = 13.5
    
            py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
            ylo = -0.5; yhi = 2.2
            
            py.legend(loc='lower left',frameon=False,numpoints=1,handlelength=3)
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")


    # Particle tracking comparison as a function of vcirc 1 - Tollet (Nihao), Cristensen (Gasoline)
    elif i == 3:

        if show_incomplete:
            if complete_cut == "1newstar":
                ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)
            else:
                vcirc_lim = 1.73 # Corresponds to 100 star particles at low-z bin
                ok = bin_vcirc_mid >= vcirc_lim
        else:
            ok = bin_mstar_mid > 0

        py.plot(bin_vcirc_mid[ok],np.log10(f_med_dict2["ism_wind_tot_ml"+fV_str+"_vcirc"][0][ok]),c="b",label="EAGLE",linewidth=linewidth)
        #py.scatter(bin_vcirc_mid,np.log10(f_med_dict2["ism_wind_tot_ml_vcirc"][0]),c="k",edgecolors="None")

        py.plot(bin_vcirc_mid[ok],np.log10(f_med_dict2["halo_wind_tot_ml"+fV_str+"_vcirc"][0][ok]),c="b",linestyle='--',linewidth=linewidth)
        #py.scatter(bin_mstar_mid,np.log10(f_med_dict2["halo_wind_tot_ml_mstar"][0]),c="r",edgecolors="None")

        # Taken from table 2 and eqn 7 of Tollet 2019 - their eta_gal quantity (mass ejected from galaxy) - note Table 2 is confusing between eta_gal_cold and eta_gal - be careful if cross-checking later!
        log_eta_f19 = -4.6 * (bin_vcirc_mid-2) + np.log10(4)
        ok = (10**bin_vcirc_mid > 60)&(10**bin_vcirc_mid < 200)
        py.plot(bin_vcirc_mid[ok], log_eta_f19[ok], c="tab:orange", label="NIHAO",linewidth=linewidth)

        log_eta_f19_halo = -3.5 * (bin_vcirc_mid-2)+np.log10(2)
        ok = (10**bin_vcirc_mid > 50)&(10**bin_vcirc_mid < 200)
        py.plot(bin_vcirc_mid[ok], log_eta_f19_halo[ok], c="tab:orange",linestyle='--',linewidth=linewidth)

        # Taken from Cristensen 2016, figure 11, their quoted fit
        # These measurements were made to particle track, leave ISM data, over 1Gyr intervals, found no sign of redshift evolution.
        logeta_at_vc10 = 1.81
        logeta_c16 = -2.2*(bin_vcirc_mid - np.log10(10)) + logeta_at_vc10
        ok = (bin_vcirc_mid < np.log10(150))
        py.plot(bin_vcirc_mid[ok], logeta_c16[ok], c="g", label="C16",linewidth=linewidth)
        

        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=1.5,bbox_to_anchor=(0.92,1.0))
        py.gca().add_artist(legend1)

        xlo = 1.4; xhi = 2.5
        py.xlabel(r"$\log_{10}(V_{\mathrm{c}} \, / \mathrm{km s^{-1}})$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")

        l5, = py.plot([-20,-20],[-20,20],label=r"ISM",c="k",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=r"Halo",linewidth=linewidth)
        py.legend(handles=[l5,l6],labels=[r"ISM", r"Halo"], loc="lower left",frameon=False,numpoints=1,handlelength=3)

        # Christensen for is for 0<z<2 - but it doesn't seem to evolve - so should be fine
        # Tollet is at z=0
        py.annotate(r"$z=0$",(1.45,1.7),fontsize=9)

    # Horizon AGN comparison at R20
    elif i == 4:

        ylo = -1.0
        yhi = 2.5
        # Mapping between symlog scale and actual scale from fig13, beckmann17
        '''x = np.array([0.0, 52.0, 325.0, 662.0, 1000.])
        y = np.array([0.0, 1.0, 10.0, 100.0, 1000.])
        deg_c = 10
        coeff = np.polynomial.polynomial.polyfit(x,y,deg=deg_c)
        x2 = np.arange(x.min(),x.max(),1)       
        y2 = np.zeros(len(x2))
        for n in range(deg_c+1):
            y2 += x2**n * coeff[n]
            
        logmstar_b17, y_b17, ind = np.loadtxt("beckmann17.dat",unpack=True)
        mout_b17 = us.Linear_Interpolation(x2,y2,y_b17)

        ls_list = [":","-.","--","-",":","-.","--","-"]

        for n in [4,5,6,7]:
            ok = ind == n
            py.plot(logmstar_b17[ok],np.log10(mout_b17[ok]),linestyle=ls_list[n],c="y")'''

        # I now have access to the actual beckmann 17 data
        mstar_b17_z5, dmdt_out_r20_z5, dmdt_out_r95_z5 = np.loadtxt("Beckmann17_data/horizon_outflows_070.csv",unpack=True,delimiter=",")
        mstar_b17_z3, dmdt_out_r20_z3, dmdt_out_r95_z3 = np.loadtxt("Beckmann17_data/horizon_outflows_125.csv",unpack=True,delimiter=",")
        mstar_b17_z1, dmdt_out_r20_z1, dmdt_out_r95_z1 = np.loadtxt("Beckmann17_data/horizon_outflows_343.csv",unpack=True,delimiter=",")
        mstar_b17_z0p01, dmdt_out_r20_z0p01, dmdt_out_r95_z0p01 = np.loadtxt("Beckmann17_data/horizon_outflows_752.csv",unpack=True,delimiter=",")

        bin_mh = np.arange(8.0, 15.1,0.2)
        n_bins = len(bin_mh)
        bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1)) # This should match build_grid_winds_version.py
        y_r20_z5 = np.zeros_like(bin_mstar_mid)
        y_r95_z5 = np.zeros_like(bin_mstar_mid)
        y_r20_z3 = np.zeros_like(bin_mstar_mid)
        y_r95_z3 = np.zeros_like(bin_mstar_mid)
        y_r20_z1 = np.zeros_like(bin_mstar_mid)
        y_r95_z1 = np.zeros_like(bin_mstar_mid)
        y_r20_z0p01 = np.zeros_like(bin_mstar_mid)
        y_r95_z0p01 = np.zeros_like(bin_mstar_mid)

        print "Computing Beckmann17 stats"
        for j_bin in range(len(bin_mstar_mid)):
            ok = (np.log10(mstar_b17_z5) > bin_mstar[j_bin]) & (np.log10(mstar_b17_z5) < bin_mstar[j_bin+1])
            y_r20_z5[j_bin] = np.median(dmdt_out_r20_z5[ok])
            y_r95_z5[j_bin] = np.median(dmdt_out_r95_z5[ok])

            ok = (np.log10(mstar_b17_z3) > bin_mstar[j_bin]) & (np.log10(mstar_b17_z3) < bin_mstar[j_bin+1])
            y_r20_z3[j_bin] = np.median(dmdt_out_r20_z3[ok])
            y_r95_z3[j_bin] = np.median(dmdt_out_r95_z3[ok])

            ok = (np.log10(mstar_b17_z1) > bin_mstar[j_bin]) & (np.log10(mstar_b17_z1) < bin_mstar[j_bin+1])
            y_r20_z1[j_bin] = np.median(dmdt_out_r20_z1[ok])
            y_r95_z1[j_bin] = np.median(dmdt_out_r95_z1[ok])

            ok = (np.log10(mstar_b17_z0p01) > bin_mstar[j_bin]) & (np.log10(mstar_b17_z0p01) < bin_mstar[j_bin+1])
            y_r20_z0p01[j_bin] = np.median(dmdt_out_r20_z0p01[ok])
            y_r95_z0p01[j_bin] = np.median(dmdt_out_r95_z0p01[ok])

        print "Done"

        py.plot(bin_mstar_mid, np.log10(y_r20_z5), linestyle=':', c="y",linewidth=linewidth)
        py.plot(bin_mstar_mid, np.log10(y_r20_z3), linestyle='-.', c="y",linewidth=linewidth)
        py.plot(bin_mstar_mid, np.log10(y_r20_z1), linestyle='--', c="y",linewidth=linewidth)
        py.plot(bin_mstar_mid, np.log10(y_r20_z0p01), linestyle='-', c="y",linewidth=linewidth)
        
        xlo = 8.0; xhi = 12.0
        linestyle_list = ["-","--","-.",":"]
        show = [0,2,4,6]
        z_list_hagn = [0.1, 1.0, 3.0, 4.9]
        complete_list = [0, 2, 4, 6] # Redshift bins from the rest of the paper that coincide with the above redshifts
        # Ah this was redundant - nvm

        ind_temp = 0
        for n, n2, linestyle in zip(show,complete_list,linestyle_list):

            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)
                else:
                    ok = bin_mstar_mid >= np.log10(1.81e6)+2
            else:
                ok = bin_mstar_mid > 0

            py.plot(bin_mstar_mid[ok],np.log10(f_med_dict4["dmdt_out_r0p2rvir_subnorm_med_mstar"][n][ok]),c="b",linestyle=linestyle,linewidth=linewidth)

            py.plot(bin_mstar_mid[ok]-99, f_med_dict4["dmdt_out_r0p2rvir_subnorm_med_mstar"][n][ok]-99,c="k",label=(r"$z = %3.1f$" % (z_list_hagn[ind_temp])),linestyle=linestyle,linewidth=linewidth)
            ind_temp += 1

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3.0)
        py.gca().add_artist(legend1)

        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}}(r = 0.2 \, R_{\mathrm{vir}}) \, / \mathrm{M_\odot yr^{-1}} )$")

        l5, = py.plot([-20,-20],[-20,20],c="b",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],c="y",linewidth=linewidth)
        py.legend(handles=[l6,l5],labels=["Horizon-AGN","EAGLE"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)

        py.annotate(r"$r=0.2 \, R_{\mathrm{vir}}$",(8.15,1.6))

    # Horizon AGN comparison at R95
    elif i == 5:
        yhi = 2.5
        ylo = -1.0

        #for n in [0,1,2,3]:
        #    ok = ind == n
        #    py.plot(logmstar_b17[ok],np.log10(mout_b17[ok]),linestyle=ls_list[n],c="y")

        py.plot(bin_mstar_mid, np.log10(y_r95_z5), linestyle=':', c="y",linewidth=linewidth)
        py.plot(bin_mstar_mid, np.log10(y_r95_z3), linestyle='-.', c="y",linewidth=linewidth)
        py.plot(bin_mstar_mid, np.log10(y_r95_z1), linestyle='--', c="y",linewidth=linewidth)
        py.plot(bin_mstar_mid, np.log10(y_r95_z0p01), linestyle='-', c="y",linewidth=linewidth)


        xlo = 8.0; xhi = 12.0
        show = [0,2,4,6]
        linestyle_list = ["-","--","-.",":"]
        ind_temp = 0
        for n, linestyle in zip(show,linestyle_list):

            if show_incomplete:
                if complete_cut == "1newstar":
                    ok = (f_med_dict2["ism_wind_cum_ml_pp_complete"+fV_str+"_mstar"][3] == 1) | (f_med_dict2["ism_wind_cum_ml_pp_complete2"+fV_str+"_mstar"][3] == 0)
                else:
                    ok = bin_mstar_mid >= np.log10(1.81e6)+2
            else:
                ok = bin_mstar_mid > 0

            py.plot(bin_mstar_mid[ok],np.log10(f_med_dict4["dmdt_out_r0p95rvir_subnorm_med_mstar"][n][ok]),c="b",linestyle=linestyle,linewidth=linewidth)

            py.plot(bin_mstar_mid[ok]-99,f_med_dict4["dmdt_out_r0p95rvir_subnorm_med_mstar"][n][ok],c="k",label=(r"$z = %3.1f$" % (z_list_hagn[ind_temp])),linestyle=linestyle,linewidth=linewidth)
            ind_temp += 1

        py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3)

        py.xlabel(r"$\log_{10}(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}}(r = 0.95 \, R_{\mathrm{vir}}) \, / \mathrm{M_\odot yr^{-1}} )$")

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        py.annotate(r"$r=0.95 \, R_{\mathrm{vir}}$",(8.15,1.6))

        l5, = py.plot([-20,-20],[-20,20],c="b",linewidth=linewidth)
        l6, = py.plot([-20,-20],[-20,20],c="y",linewidth=linewidth)
        py.legend(handles=[l6,l5],labels=["Horizon-AGN","EAGLE"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)

    

    
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_"+sim_name+"_hydro_sim_comp_test_mode.pdf"
else:
    fig_name = "mass_loading_"+sim_name+"_hydro_sim_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
