import matplotlib.pyplot as py
import numpy as np
from scipy.interpolate import interp1d

def Linear_Interpolation(x1_in, y1_in, x2_in):

    f = interp1d(x1_in, y1_in, kind="linear",axis=0)

    y2_out = f(x2_in)

    return y2_out

x = np.array([0.0, 52.0, 325.0, 662.0, 1000.])
y = np.array([0.0, 1.0, 10.0, 100.0, 1000.])

deg_c = 10
coeff = np.polynomial.polynomial.polyfit(x,y,deg=deg_c)
x2 = np.arange(x.min(),x.max(),1)

y2 = np.zeros(len(x2))
for n in range(deg_c+1):
    y2 += x2**n * coeff[n]

logmstar_b17, y_b17, ind = np.loadtxt("beckmann17.dat",unpack=True)

mout_b17 = Linear_Interpolation(x2,y2,y_b17)

c_list = ["k","b","c","g","k","b","c","g"]
ls_list = ['-','-','-','-','--','--','--','--']

py.subplot(211)

for n in range(8):
    ok = ind == n
    py.plot(logmstar_b17[ok],y_b17[ok],c=c_list[n],linestyle=ls_list[n])

py.subplot(212)

for n in range(8):
    ok = ind == n
    py.plot(logmstar_b17[ok],np.log10(mout_b17[ok]),c=c_list[n],linestyle=ls_list[n])

py.show()

# This gives is our best-fit relationship between the symlog scale from Ricarda's plot and the actual linear value

'''py.subplot(211)
py.plot(x,y)
py.plot(x2,y2)
py.xlim((0,1000)), py.ylim((0,1000.0))
py.subplot(212)
py.plot(np.log(x),np.log(y))
py.plot(np.log(x2),np.log(y2))
py.show()'''
