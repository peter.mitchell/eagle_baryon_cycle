import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"

# 25 Mpc ref with 50 tree snapshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mstar_mid = file_grid["log_mstar_grid"][:]
bin_vmax_mid = file_grid["log_vmax_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["ism_wind_tot_ml_mstar","halo_wind_tot_ml_mstar","ism_wind_tot_ml_vmax","halo_wind_tot_ml_vmax"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':7})

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        for n in range(len(a_grid)):

            py.plot(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_ml_mstar"][n]),label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
            py.scatter(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_ml_mstar"][n]),c=c_list[n],edgecolors="none",s=5)
        
        py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle )$")    
        py.legend(loc='upper right',frameon=False,numpoints=1)
        xlo = 7.0; xhi = 12.5
    
    if i == 1:
        for n in range(len(a_grid)):

            py.plot(bin_vmax_mid,np.log10(f_med_dict["ism_wind_tot_ml_vmax"][n]),c=c_list[n])
            py.scatter(bin_vmax_mid,np.log10(f_med_dict["ism_wind_tot_ml_vmax"][n]),c=c_list[n],edgecolors="none",s=5)
        
        xlo = 1.4; xhi = 3.0

    if i == 2:
        for n in range(len(a_grid)):
            py.plot(bin_mstar_mid,np.log10(f_med_dict["halo_wind_tot_ml_mstar"][n]),c=c_list[n])
            py.scatter(bin_mstar_mid,np.log10(f_med_dict["halo_wind_tot_ml_mstar"][n]),c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle \dot{M}_\star \rangle )$")
        xlo = 7.0; xhi = 12.5
        py.xlabel(r"$\log(M_{\star} \, / \mathrm{M_\odot})$")

    if i == 3:
        for n in range(len(a_grid)):
            py.plot(bin_vmax_mid,np.log10(f_med_dict["halo_wind_tot_ml_vmax"][n]),c=c_list[n])
            py.scatter(bin_vmax_mid,np.log10(f_med_dict["halo_wind_tot_ml_vmax"][n]),c=c_list[n],edgecolors="none",s=5)

        xlo = 1.4; xhi = 3.0
        py.xlabel(r"$\log(V_{\mathrm{max}} \, / \mathrm{kms^{-1}})$")

        
    ylo = 0.0; yhi = 2.49
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_mstar_vmax_dep_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_mstar_vmax_dep_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
