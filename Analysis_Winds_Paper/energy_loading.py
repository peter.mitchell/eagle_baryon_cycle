import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25
r_bins = [0.25, 0.95]

test_mode = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 50 Mpc ref with 28 tree snapshots
#sim_name = "L0050N0752_REF_snap"

# 50 Mpc no-AGN with 28 tree snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 500 tree snapshots
sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"


part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str, "halo_wind_tot_subnorm"+fV_str]

vmin_i = 0.0
f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]

f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]

f_name_list += ["AGN_energy_injection_rate", "SNe_energy_injection_rate"]
f_name_list += ["AGN_energy_injection_rate_vcnorm", "SNe_energy_injection_rate_vcnorm"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
    
file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)


    r_bins = np.arange(0,1.1,0.1)
    r_mid = 0.5 * (r_bins[1:] + r_bins[:-1])
    i_r = np.argmin(abs(r_bins-r_mid[0]))


    show = [0,2,4]

    if i == 0:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            #quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            #quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1

            #quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            #quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"][n,:,i_r] # Gyr^-1
            quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"][n,:,i_r] # Gyr^-1

            g2Msun = 1.989e33
            Msun = g2Msun *1e-3
            erg = 1e-7
            to_erg = Msun * 1e6 / erg

            #quant2 = (f_med_dict["AGN_energy_injection_rate"][n] ) / to_erg # Msun km^2 s^-2 yr^-1
            #quant3 = f_med_dict["SNe_energy_injection_rate"][n] / to_erg

            quant2 = (f_med_dict["AGN_energy_injection_rate_vcnorm"][n] ) # Gyr^-1
            quant3 = f_med_dict["SNe_energy_injection_rate_vcnorm"][n] 

            if n == 0:
                py.plot(bin_mh_mid,np.log10(quant),label=r"ISM wind, $r=0.25 R_{\mathrm{vir}}$",c=c_list[n])
                py.plot(bin_mh_mid,np.log10(quant3),label=r"SNe injected",c=c_list[n],linestyle='--')
                py.plot(bin_mh_mid,np.log10(quant2),label=r"AGN injected",c=c_list[n],linestyle=':')
            else:
                py.plot(bin_mh_mid,np.log10(quant),label=r"ISM wind, $r=0.25 R_{\mathrm{vir}}$",c=c_list[n])
                py.plot(bin_mh_mid,np.log10(quant3),label=r"SNe injected",c=c_list[n],linestyle='--')
                py.plot(bin_mh_mid,np.log10(quant2),label=r"AGN injected",c=c_list[n],linestyle=':')

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\langle \dot{E} \rangle \, / \mathrm{M_\odot \, km^2 s^{-2} \, yr^{-1}})$")
        ylo = -0.5; yhi = 2.0

    if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            #quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            #quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant = f_med_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            quant += f_med_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][n,:,i_r] # Msun km^2 s^-2 yr^-1
            g2Msun = 1.989e33
            Msun = g2Msun *1e-3
            erg = 1e-7
            to_erg = Msun * 1e6 / erg
            quant *= to_erg # erg yr^-1

            quant *= 1./(f_med_dict["AGN_energy_injection_rate"][n] + f_med_dict["SNe_energy_injection_rate"][n])

            py.plot(bin_mh_mid,np.log10(quant),label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
            py.scatter(bin_mh_mid,np.log10(quant),c=c_list[n],edgecolors="none",s=5)

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\langle \dot{E}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{E}_{\mathrm{injected}} \rangle)$")
        ylo = -0.5; yhi = 2.0

    '''if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]),c=c_list[n])
            py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]),c=c_list[n],edgecolors="none",s=5)


        py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -3.0; yhi = -0.25'''

    if i == 1:
        py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "energy_loading_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "energy_loading_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
