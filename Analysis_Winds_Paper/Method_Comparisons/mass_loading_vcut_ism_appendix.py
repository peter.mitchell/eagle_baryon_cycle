import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as us


test_mode = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 tree snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################


############ Read vcut data ##################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

cv_min_list2 = ["0p25","0p125","0p5"]


f_name_list_j = ["ism_wind_tot_ml","halo_wind_tot_ml","sf_wind_ml","nsfism_wind_ml","halo_wind_ml","ism_reheat_wind_ml","halo_reheat_wind_ml","ism_wind_tot_from_SF_ml"]
f_name_list = []
for cv_min in cv_min_list2:
    for name in f_name_list_j:
        name2 = name + "_" + cv_min +"vmax"
        f_name_list.append(name2 )

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()
 
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98*1.5],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.06,
                    'figure.subplot.top':0.99,
                    'font.size':8,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow =3; ncol = 1
subplots = panelplot(nrow,ncol)


for i,ax in enumerate(subplots):
    py.axes(ax)

    ylo = -0.6; yhi = 2.49
    xlo = 9.5; xhi = 15.0

    if i == 0:

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p125vmax"])[0],c="k",label=r"$v_{\mathrm{r}}>0.125 \, V_{\mathrm{max}}$",linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p25vmax"][0]),c="k",linestyle='--',label="$v_{\mathrm{r}}>0.25 \, V_{\mathrm{max}}$ (fiducial)",linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p5vmax"][0]),c="k",linestyle=':',label="$v_{\mathrm{r}}>0.5 \, V_{\mathrm{max}}$",linewidth=1)

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p125vmax"])[3],c="c",linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p25vmax"][3]),c="c",linestyle='--',linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p5vmax"][3]),c="c",linestyle=':',linewidth=1)

        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        label=(r"$%3.1f < z < %3.1f$" % (1./a_max[0]-1,1./a_min[0]-1))
        label2=(r"$%3.1f < z < %3.1f$" % (1./a_max[3]-1,1./a_min[3]-1))

        l1, = py.plot([-20,-20],[-20,20],label=label,c="k",linewidth=1)
        l2, = py.plot([-20,-20],[-20,20],c="c",linestyle='-',label=label2,linewidth=1)
        py.legend(handles=[l1,l2],labels=[label,label2], loc="lower left",frameon=False,numpoints=1,handlelength=1.5)


        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")

    if i == 1:


        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml_0p125vmax"][0]),c="k",label=r"$v_{\mathrm{r}}>0.125 \, V_{\mathrm{max}}$",linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml_0p25vmax"][0]),c="k",linestyle='--',label="$v_{\mathrm{r}}>0.25 \, V_{\mathrm{max}}$ (fiducial)",linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml_0p5vmax"][0]),c="k",linestyle=':',label="$v_{\mathrm{r}}>0.5 \, V_{\mathrm{max}}$",linewidth=1)

        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml_0p125vmax"][3]),c="c",linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml_0p25vmax"][3]),c="c",linestyle='--',linewidth=1)
        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml_0p5vmax"][3]),c="c",linestyle=':',linewidth=1)

        legend1 = py.legend(loc='lower left',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        label=(r"$%3.1f < z < %3.1f$" % (1./a_max[0]-1,1./a_min[0]-1))
        label2=(r"$%3.1f < z < %3.1f$" % (1./a_max[3]-1,1./a_min[3]-1))

        #l1, = py.plot([-20,-20],[-20,20],label=label,c="k",linewidth=1)
        #l2, = py.plot([-20,-20],[-20,20],c="c",linestyle='-',label=label2,linewidth=1)
        #py.legend(handles=[l1,l2],labels=[label,label2], loc="upper right",frameon=False,numpoints=1,handlelength=1.5)

        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle \dot{M}_\star \rangle)$")

    if i == 2:
        fVmax_cut = 0.25
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    
        label=(r"$%3.1f < z < %3.1f$" % (1./a_max[0]-1,1./a_min[0]-1))

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][0]),c="k",linestyle='-',linewidth=1,label=label)
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_from_SF_ml"+fV_str][0]),c="k",linestyle='--',linewidth=1)

        label=(r"$%3.1f < z < %3.1f$" % (1./a_max[3]-1,1./a_min[3]-1))

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][3]),c="c",linestyle='-',linewidth=1,label=label)
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_from_SF_ml"+fV_str][3]),c="c",linestyle='--',linewidth=1)

        #legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=1.5)
        #py.gca().add_artist(legend1)

        l1, = py.plot([-20,-20],[-20,20],label=r"Fiducial ISM definition",c="k",linewidth=1)
        l2, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=r"Only star-forming gas",linewidth=1)
        py.legend(handles=[l1,l2],labels=[r"Fiducial ISM definition",r"Only star-forming gas"], loc="upper left",frameon=False,numpoints=1,handlelength=3)

        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "mass_loading_"+sim_name+"_vcut_ism_appendix.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
