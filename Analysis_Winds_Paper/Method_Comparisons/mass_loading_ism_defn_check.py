import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

fVmax_cut = 0.25

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 tree snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 tree snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"ism_wind_tot_from_SF_ml"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

nrow = 2; ncol = 1

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.97,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:

        label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[0]-1,1./a_min[0]-1))

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][0]),c="k",linestyle='-')
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_from_SF_ml"+fV_str][0]),c="k",linestyle='--')

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][3]),c="c",linestyle='-')
        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_from_SF_ml"+fV_str][3]),c="c",linestyle='--')

        #py.plot(bin_mh_mid,np.log10(f_med_dict["sf_wind_ml"][0]),c="r",linestyle='-')
        #py.plot(bin_mh_mid,np.log10(f_med_dict["sf_reheat_ml"][0]),c="r",linestyle='--')

        #py.plot(bin_mh_mid,np.log10(f_med_dict["nsfism_wind_ml"][0]),c="c",linestyle='-')
        #py.plot(bin_mh_mid,np.log10(f_med_dict["nsfism_reheat_ml"][0]),c="c",linestyle='--')


    py.legend(loc='upper right',frameon=False,numpoints=1)
    py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{out}} \rangle \, / \langle \dot{M}_\star \rangle)$")
    ylo = -0.3; yhi = 1.5

    py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_ism_defn_check_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_ism_defn_check_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()

