import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as us


test_mode = False

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 50 tree snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################


############ Read shell data ##################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_tng.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_50"]
for i in range(len(f_name_list)):
    f_name_list[i] += "_med_mstar"
f_name_list2 = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_0", "ml_rvir_cvmin_50", "ml_rvir_cvmin_150","ml_rvir_cvmin_250","ml_rvir_cvmin_250","ml_0p25rvir_cvmin_0"]
for i in range(len(f_name_list2)):
    f_name_list2[i] += "_mstar"
f_name_list+=f_name_list2
f_name_list += ["ml_0p25rvir_cvmin_0"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()
 
############ Read shell data matched to Horizon-AGN ####################
# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"
# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_hagn_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_hagn.hdf5")

f_name_list4 = ["dmdt_out_r0p95rvir_subnorm_med_mstar","dmdt_out_r0p2rvir_subnorm_med_mstar"]
f_med_dict4 = {}
for f_name in f_name_list4:
    f_med_dict4[f_name] = file_grid[f_name][:]

file_grid.close()

############## Read standard particle track data ##############

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"

if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
bin_vcirc_mid = file_grid["log_vcirc_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]
rvir_med = file_grid["R200_med"][:]
mstar_med = file_grid["mstar_med"][:]


f_name_list = ["ism_wind_tot_ml_mstar","ism_wind_tot_ml","halo_wind_tot_ml_mstar","ism_wind_tot_ml_vcirc","ism_wind_cum_ml_pp_mstar","ism_wind_cum_ml_pp","halo_wind_tot_ml_vcirc","halo_wind_tot_ml"]

f_med_dict2 = {}
for f_name in f_name_list:
    f_med_dict2[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.21,
                    'figure.figsize':[6.64,4.98*1.5],
                    'figure.subplot.left':0.1,
                    'figure.subplot.bottom':0.06,
                    'figure.subplot.top':0.99,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 3; ncol = 2
subplots = panelplot(nrow,ncol)


for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    ylo = -0.5; yhi = 2.0

    # Tng comparison 1
    if i == 0:

        #label=(r"$z=%3.1f$" % (1./a_max[3]-1))
        l1, = py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_0_med_mstar"][3]),c="b")
        
        l3 = py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_150_med_mstar"][3]),c="b",linestyle='--')
        
        l4, = py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_250_med_mstar"][3]),c="b",linestyle='-.')

        py.plot(bin_mstar_mid,np.log10(f_med_dict2["ism_wind_tot_ml_mstar"][3]),c="r",label="Tracking, ISM")
        py.plot(bin_mstar_mid,np.log10(f_med_dict2["halo_wind_tot_ml_mstar"][3]),c="r",label='Tracking, halo',linestyle='--')
    
        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1,handlelength=1.5)
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label=r"$Shell, v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",c="k")
        l6, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',label=r"$Shell, v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$")
        l7, = py.plot([-20,-20],[-20,20],c="k",linestyle='-.',label=r"$Shell, v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$")
        py.legend(handles=[l5,l6,l7],labels=[r"Shell, $r = 10 \, \mathrm{kpc}, v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$",r"Shell, $r = 10 \, \mathrm{kpc}, v_{\mathrm{r}}>150 \, \mathrm{km s^{-1}}$",r"Shell, $r = 10 \, \mathrm{kpc}, v_{\mathrm{r}}>250 \, \mathrm{km s^{-1}}$"], loc="upper right",frameon=False,numpoints=1,handlelength=3)

        xlo = 7.5; xhi = 11.5
        py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log(\dot{M}_{\mathrm{out}}(r = 10 \, \mathrm{kpc}) \, / \dot{M}_\star )$")

    # Tng radial comparison
    elif i == 1:
        xlo = 7.5; xhi = 11.5


        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r10_cvmin_50_med_mstar"][3]),c="b",label=r"Shell, $r = 10 \, \mathrm{kpc}, v_{\mathrm{r}}>50 \, \mathrm{km s^{-1}}$")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_r20_cvmin_50_med_mstar"][3]),c="b",linestyle='--',label="Shell, $r = 20 \, \mathrm{kpc}, v_{\mathrm{r}}>50 \, \mathrm{km s^{-1}}$")
        ok = bin_mstar_mid > 9
        py.plot(bin_mstar_mid[ok],np.log10(f_med_dict["ml_r50_cvmin_50_med_mstar"][3][ok]),c="b",linestyle=':',label="Shell, $r = 50 \, \mathrm{kpc}, v_{\mathrm{r}}>50 \, \mathrm{km s^{-1}}$")
        py.plot(bin_mstar_mid,np.log10(f_med_dict["ml_rvir_cvmin_50_med_mstar"][3]),c="b",linestyle='-.',label="Shell, $r = r_{\mathrm{vir}}, v_{\mathrm{r}}>50 \, \mathrm{km s^{-1}}$")

        py.plot(bin_mstar_mid,np.log10(f_med_dict2["ism_wind_tot_ml_mstar"][3]),c="r")
        py.plot(bin_mstar_mid,np.log10(f_med_dict2["halo_wind_tot_ml_mstar"][3]),c="r",linestyle='--')

        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label="Track, ISM",c="r")
        l6, = py.plot([-20,-20],[-20,20],label="Track, halo",c="r")
        py.legend(handles=[l5,l6],labels=["Track, ISM","Track, halo"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)
        py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log(\dot{M}_{\mathrm{out}}(v_{\mathrm{r}} > 50 \, \mathrm{km s^{-1}}) \, / \dot{M}_\star )$")

    # Particle tracking and 0.25 Rvir Eulerian comparison as a function of mstar - Anglez-Alcazar (FIRE), Muratov (FIRE)
    elif i == 2:

        py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][0]),c="b",linestyle='--',label="Shell, $r = 0.25 R_{\mathrm{vir}}, v_{\mathrm{r}}>0 \, \mathrm{km s^{-1}}$")

        py.plot(bin_mh_mid,np.log10(f_med_dict2["ism_wind_tot_ml"][0]),c="r",label="Track, ISM")
        py.plot(bin_mh_mid,np.log10(f_med_dict2["halo_wind_tot_ml"][0]),c="r",label="Track, halo",linestyle='--')

        xlo = 9.5; xhi = 13.5

        py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")
        ylo = -0.5; yhi = 2.2


        py.legend(loc='lower left',frameon=False,numpoints=1,handlelength=3)
        py.ylabel(r"$\log(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")



    # Horizon AGN comparison at R20
    elif i == 4:
        break

        ylo = -1.0
        yhi = 2.5

        xlo = 8.0; xhi = 12.0
        linestyle_list = ["-","--","-.",":"]
        show = [0,2,4,6]
        z_list_hagn = [0.1, 1.0, 3.0, 4.9]
        ind_temp = 0
        for n, linestyle in zip(show,linestyle_list):
            py.plot(bin_mstar_mid,np.log10(f_med_dict4["dmdt_out_r0p2rvir_subnorm_med_mstar"][n]),c="b",label=(r"$z = %3.1f$" % (z_list_hagn[ind_temp])),linestyle=linestyle)
            ind_temp += 1

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3.0)
        py.gca().add_artist(legend1)

        py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log(\dot{M}_{\mathrm{out}}(r = 0.2 \, R_{\mathrm{vir}}) \, / \mathrm{M_\odot yr^{-1}} )$")

        l5, = py.plot([-20,-20],[-20,20],c="b")
        l6, = py.plot([-20,-20],[-20,20],c="y")
        py.legend(handles=[l6,l5],labels=["Horizon-AGN, Beckmann et al. (2017)","EAGLE"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)

        py.annotate(r"$r=0.2 \, R_{\mathrm{vir}}$",(11.1,0.1))

    # Horizon AGN comparison at R95
    elif i == 5:
        yhi = 2.5
        ylo = -1.0

        #for n in [0,1,2,3]:
        #    ok = ind == n
        #    py.plot(logmstar_b17[ok],np.log10(mout_b17[ok]),linestyle=ls_list[n],c="y")

        py.plot(bin_mstar_mid, np.log10(y_r95_z5), linestyle=':', c="y")
        py.plot(bin_mstar_mid, np.log10(y_r95_z3), linestyle='-.', c="y")
        py.plot(bin_mstar_mid, np.log10(y_r95_z1), linestyle='--', c="y")
        py.plot(bin_mstar_mid, np.log10(y_r95_z0p01), linestyle='-', c="y")


        xlo = 8.0; xhi = 12.0
        show = [0,2,4,6]
        linestyle_list = ["-","--","-.",":"]
        ind_temp = 0
        for n, linestyle in zip(show,linestyle_list):
            py.plot(bin_mstar_mid,np.log10(f_med_dict4["dmdt_out_r0p95rvir_subnorm_med_mstar"][n]),c="b",label=(r"$z = %3.1f$" % (z_list_hagn[ind_temp])),linestyle=linestyle)
            ind_temp += 1

        py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3)

        py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log(\dot{M}_{\mathrm{out}}(r = 0.95 \, R_{\mathrm{vir}}) \, / \mathrm{M_\odot yr^{-1}} )$")

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        py.annotate(r"$r=0.95 \, R_{\mathrm{vir}}$",(11.1,0.1))

        l5, = py.plot([-20,-20],[-20,20],c="b")
        l6, = py.plot([-20,-20],[-20,20],c="y")
        py.legend(handles=[l6,l5],labels=["Horizon-AGN, Beckmann et al. (2017)","EAGLE"], loc="upper left",frameon=False,numpoints=1,handlelength=1.5)

    

    
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "mass_loading_"+sim_name+"_track_shell_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
