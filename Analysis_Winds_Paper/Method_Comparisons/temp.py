import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as us


test_mode = False

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 50 tree snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################


############ Read shell data ##################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_shell_appendix_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_shell_appendix.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]


cv_min_list = ["0","50","150","250"]
cv_min_list2 = ["","0p25","0p125","0p5"]
r_shell_list = ["10","30","50"]
r_shell_list2 = ["0p25","0p5","0p75",""]

f_name_list = ["ism_wind_tot_ml","halo_wind_tot_ml"]

for cv_min in cv_min_list:
    for r_shell in r_shell_list:
        name = "ml_r"+r_shell+"_cvmin_"+cv_min
        f_name_list.append(name)

    for r_shell in r_shell_list2:
        name = "ml_"+r_shell+"rvir_cvmin_"+cv_min
        f_name_list.append(name )

for cv_min in cv_min_list2:

    for r_shell in r_shell_list2:
        name = "ml_"+r_shell+"rvir_cvmin_"+cv_min+"vmax"
        f_name_list.append(name )


f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

 
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.99,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)


for i,ax in enumerate(subplots):
    py.axes(ax)

    #for tick in ax.xaxis.get_major_ticks():
    #    tick.label1On=True

    ylo = -0.5; yhi = 2.49
    xlo = 9.5; xhi = 15.0




    if i ==0:
        #label=(r"$z=%3.1f$" % (1./a_max[0]-1))
        l1, = py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0"][0]),c="b")
        
        l3 = py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0p125vmax"][0]),c="b",linestyle='--')
        
        l4, = py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0p25vmax"][0]),c="b",linestyle='-.')

        l4, = py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_0p5vmax"][0]),c="b",linestyle=':')

        l4, = py.plot(bin_mh_mid,np.log10(f_med_dict["ml_0p25rvir_cvmin_vmax"][0]),c="b",linestyle='-')

        py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][0]),c="r",label="Tracking, ISM")
        py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml"][0]),c="r",label='Tracking, halo',linestyle='--')
    
        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1,handlelength=3.0)
        py.gca().add_artist(legend1)

        l5, = py.plot([-20,-20],[-20,20],label=r"$Shell, v_{\mathrm{r}}>0$",c="b")
        l6, = py.plot([-20,-20],[-20,20],c="b",linestyle='--',label=r"$Shell, v_{\mathrm{r}}> 0.125 V_{\mathrm{max}}$")
        l7, = py.plot([-20,-20],[-20,20],c="b",linestyle='-.',label=r"$Shell, v_{\mathrm{r}}> 0.25 V_{\mathrm{max}}$")
        l8, = py.plot([-20,-20],[-20,20],c="b",linestyle=':',label=r"$Shell, v_{\mathrm{r}}> 0.5 V_{\mathrm{max}}$")
        l9, = py.plot([-20,-20],[-20,20],c="b",linestyle='-',label=r"$Shell, v_{\mathrm{r}}> V_{\mathrm{max}}$")
        py.legend(handles=[l5,l6,l7,l8,l9],labels=[r"Shell, $r = 0.25 R_{\mathrm{vir}}, v_{\mathrm{r}}>0$",r"Shell, $r = 0.25 R_{\mathrm{vir}}, v_{\mathrm{r}}>0.125 \, V_{\mathrm{max}}$",r"Shell, $r = 0.25 R_{\mathrm{vir}}, v_{\mathrm{r}}>0.25 \, V_{\mathrm{max}}$",r"Shell, $r = 0.25 R_{\mathrm{vir}}, v_{\mathrm{r}}>0.5 \, V_{\mathrm{max}}$",r"Shell, $r = 0.25 R_{\mathrm{vir}}, v_{\mathrm{r}}> V_{\mathrm{max}}$"], loc="upper right",frameon=False,numpoints=1,handlelength=3)

        py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")
        py.ylabel(r"$\log(\dot{M}_{\mathrm{out}} \, / \dot{M}_\star )$")

        py.annotate(r"$z=0.25$",(9.9,-0.25))





    
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "gm1_outflow_shells.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
