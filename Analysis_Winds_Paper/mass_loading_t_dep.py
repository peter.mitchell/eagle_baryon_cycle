import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False
show_fakhouri_formula = False # Note Correa formula has the complication that you need to know the final mass of the halo - which is a bit awkward

# For checking purposes only, compare to the median values
show_median = False

# Indicate bins where 20% of the galaxies have zero star formation across the entire redshift bin (for all progenitors)
show_incomplete = True

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 50 Mpc ref with 28 tree snapshots
#sim_name = "L0050N0752_REF_snap"

# 50 Mpc no-AGN with 28 tree snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 50 tree snapshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 100 tree snapshots
#sim_name = "L0025N0376_REF_100_snip"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"


part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!



if show_fakhouri_formula:
    dmdt = lambda mhalo,z: 46.1 * (mhalo/1e12)**1.1 * (1+1.11*z) * np.sqrt(omm * (1+z)**3 + oml) # Msun yr^-1

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str, "halo_wind_tot_subnorm"+fV_str]

if show_median:
    f_name_list += ["ism_wind_tot_ml_med"+fV_str, "halo_wind_tot_ml_med"+fV_str, "halo_wind_complete"+fV_str, "ism_wind_complete"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()


if show_incomplete:
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp"+fV_str, "ism_wind_cum_ml_pp_med"+fV_str, "ism_wind_cum_ml_pp_lo"+fV_str, "ism_wind_cum_ml_pp_hi"+fV_str, "ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    show = [0,1,2,3,4,5,6]

    if i == 0:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            if show_incomplete:
                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)

                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]*t_grid[n]),c=c_list[n],linestyle='--')

                temp = np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]*t_grid[n])
                temp[ok==False] = np.nan
                py.plot(bin_mh_mid,temp,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
                py.scatter(bin_mh_mid[ok],np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n][ok]*t_grid[n]),c=c_list[n],edgecolors="none",s=5)
                                               
            else:
                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]*t_grid[n]),label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
                #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]),label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=py.cm.tab10(c_index))
                py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]*t_grid[n]),c=c_list[n],edgecolors="none",s=5)
                

            if show_median:
                
                ok = f_med_dict["ism_wind_complete"+fV_str][n] > 0.8
                py.plot(bin_mh_mid[ok], np.log10(f_med_dict["ism_wind_tot_ml_med"+fV_str][n][ok]*t_grid[n]),c=c_list[n],linestyle='--')

        #py.plot(bin_mh_mid, -0.5 * bin_mh_mid + 5,linestyle='--',c="k")
        #py.annotate(r"$\propto M_{200}^{-0.5} \propto V_{\mathrm{c}}^{-1.5}$",(10.8,-0.35))
        
        py.legend(loc='upper right',frameon=False,numpoints=1)
        #py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        ylo = -0.5; yhi = 2.0

    if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]*t_grid[n])
            if show_incomplete:
                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]*t_grid[n]),c=c_list[n],linestyle='--')

                quant[ok==False] = np.nan

            else:
                ok = bin_mh_mid > 0

            py.plot(bin_mh_mid,quant,c=c_list[n])
            py.scatter(bin_mh_mid[ok],np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n][ok]*t_grid[n]),c=c_list[n],edgecolors="none",s=5)


        #py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle f_{\mathrm{B}} M_{200} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -2.5; yhi = 0.5

    if i == 2:
        for n in range(len(a_grid)):
            if n not in show:
                continue

            if show_incomplete:
                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] ==1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)

                temp = np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n]*t_grid[n])
                temp[ok==False] = np.nan

                py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n]*t_grid[n]),c=c_list[n],linestyle='--')
                py.plot(bin_mh_mid,temp,c=c_list[n])
                py.scatter(bin_mh_mid[ok],np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n][ok]*t_grid[n]),c=c_list[n],edgecolors="none",s=5)
                
            else:
                py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n]*t_grid[n]),c=c_list[n])
                py.scatter(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n]*t_grid[n]),c=c_list[n],edgecolors="none",s=5)
                

            if show_median:
                ok = f_med_dict["halo_wind_complete"+fV_str][n] > 0.8
                py.plot(bin_mh_mid[ok], np.log10(f_med_dict["halo_wind_tot_ml_med"+fV_str][n][ok]*t_grid[n]),c=c_list[n],linestyle='--')

        #py.legend(loc='upper right',frameon=False,numpoints=1)
        #py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        ylo = 0.0; yhi = 2.49
        
        #py.plot(bin_mh_mid, -2/3. * bin_mh_mid + 7.2, linestyle='--',c="k",alpha=0.7)
        #py.annotate(r"$\propto M_{200}^{-2/3} \propto V_{\mathrm{c}}^{-2}$",(9.5,0.02),alpha=0.7)

        py.plot(bin_mh_mid, -1 * bin_mh_mid + 10.6,linestyle='--',c="k")
        py.annotate(r"$\propto M_{200}^{-1}$",(9.5,0.15),color="k")
        

    if i == 3:
        for n in range(len(a_grid)):
            if n not in show:
                continue

            quant = np.log10(f_med_dict["halo_wind_tot_subnorm"+fV_str][n]*t_grid[n])
            if show_incomplete:
                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"+fV_str][n]*t_grid[n]),c=c_list[n],linestyle='--')

                quant[ok==False] = np.nan

            else:
                ok = bin_mh_mid > 0

            py.plot(bin_mh_mid,quant,c=c_list[n])
            py.scatter(bin_mh_mid[ok],np.log10(f_med_dict["halo_wind_tot_subnorm"+fV_str][n][ok]*t_grid[n]),c=c_list[n],edgecolors="none",s=5)

        #py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle f_{\mathrm{B}} M_{200} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -2.5; yhi = 0.5

        if show_fakhouri_formula:
            for n in range(len(a_grid)):
                #if n == 0 or n == 3 or n == 6:
                    #print dmdt(10**bin_mh_mid,z_grid[n]), dmdt(10**bin_mh_mid,z_grid[n])*1e9, dmdt(10**bin_mh_mid,z_grid[n])*1e9 / (10**bin_mh_mid)
                py.plot(bin_mh_mid,np.log10(dmdt(10**bin_mh_mid,z_grid[n])*1e9 / (10**bin_mh_mid)),c=c_list[n],linestyle='--')


    if i == 2 or i == 3:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
