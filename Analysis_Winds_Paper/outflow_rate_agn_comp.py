import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 50 tree snapshots
#sim_name = "L0100N1504_REF_50_snip"

# snipshot version doesn't work because redshift intervals don't line up
# 25 Mpc ref with 50 tree snapshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc REF with 28 snapshots
sim_name = "L0025N0376_REF_snap"

# 50 Mpc no-AGN with 28 snapshots
sim_name2 = "L0050N0752_NOAGN_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")
file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name2+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

bin_mh_mid2 = file_grid2["log_msub_grid"][:]
a_grid2 = file_grid2["a_grid"][:]
z_grid2 = 1./a_grid2 - 1.0
a_min2 = file_grid2["a_min"]
a_max2 = file_grid2["a_max"]

f_name_list = ["ism_wind_tot_subnorm", "halo_wind_tot_subnorm"]

f_med_dict = {}
f_med_dict2 = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
    f_med_dict2[f_name] = file_grid2[f_name][:]

file_grid.close()
file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    n_show = [0, 2, 3, 5]

    if i == 0:
        for n in range(len(a_grid)):

            if n in n_show:

                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"][n]),c=c_list[n],label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"][n]),c=c_list[n],edgecolors="none",s=5)

                py.plot(bin_mh_mid2,np.log10(f_med_dict2["ism_wind_tot_subnorm"][n]),c=c_list[n],linestyle='--')

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{sub}} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -3.25; yhi = -0.5

    if i == 1:
        for n in range(len(a_grid)):
            
            if n in n_show:
                py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"][n]),c=c_list[n])
                py.scatter(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"][n]),c=c_list[n],edgecolors="none",s=5)

                py.plot(bin_mh_mid2,np.log10(f_med_dict2["halo_wind_tot_subnorm"][n]),c=c_list[n],linestyle='--')

        py.plot(np.arange(0,10),np.arange(0,10)-1e9,c="k",label="Reference, with AGN")
        py.plot(np.arange(0,10),np.arange(0,10)-1e9,c="k",label="no AGN",linestyle='--')

        py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)

        py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{sub}} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -2.5; yhi = 0.0
        py.xlabel(r"$\log(M_{\mathrm{sub}} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "outflow_rate_agn_comp_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
