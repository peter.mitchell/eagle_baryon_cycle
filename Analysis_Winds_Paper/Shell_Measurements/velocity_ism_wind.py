import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25 # ISM wind definition

vmin = 0.0 # radial velocity for (wind) gas in the shell
#vmin = 0.25

#i_r = 2 # This is the 0.2-0.3 Rvir radius bin
i_r = 1 # This is the 0.1-0.2 Rvir radius bin
#i_r = 5

xquant_s = "mhalo"
#xquant_s = "vmax"
#xquant_s = "mstar"

test_mode = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 50 Mpc REF with 28 snapshots
#sim_name = "L0050N0752_REF_snap"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!



########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_v50_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]
bin_vmax_mid = file_grid["log_vmax_grid"][:]

t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

if xquant_s == "mhalo":
    xquant = bin_mh_mid
    xlo = 10.0; xhi = 15.0
elif xquant_s == "mstar":
    xquant = bin_mstar_mid
    xlo = 8.0; xhi = 11.5
else:
    xquant = np.log10(10**(bin_vmax_mid)/200.0)
    xlo = 0.0; xhi = 500.0

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["vmax_med"]

f_name_list += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
f_name_list += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
f_name_list += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
f_name_list += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]

f_name_list += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
f_name_list += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
f_name_list += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]
f_name_list += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")
f_med_dict["R200_med"] = file_grid["R200_med"][:]
file_grid.close()

# Only show results for bins where 80% of the galaxies actually had outflowing wind gas in the shell 
v50fw = f_med_dict["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
v90fw = f_med_dict["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]

v50fw_tot = f_med_dict["v50fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
v90fw_tot = f_med_dict["v90fw_out_"+str(vmin).replace(".","p")+"vmax_med"]

# Only show results for bins where 80% of the galaxies actually had outflowing wind gas in the shell 
complete_50fw = f_med_dict["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
complete_90fw = f_med_dict["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]

complete_50fw_tot = f_med_dict["v50fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]
complete_90fw_tot = f_med_dict["v90fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]

v50fw[complete_50fw == 0] = np.nan
v90fw[complete_90fw == 0] = np.nan

v50fw_tot[complete_50fw_tot == 0] = np.nan
v90fw_tot[complete_90fw_tot == 0] = np.nan

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':8,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]


py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    #show = [0,2,3,5]
    show = [0,3]

    if i == 0:
        for n in range(len(a_grid)):
            
            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            py.plot(xquant,v50fw[n][:,i_r],c=c_list[n],linestyle='--',linewidth=1)
            py.scatter(xquant,v50fw[n][:,i_r],c=c_list[n],edgecolors="none",s=5)

            py.plot(xquant,v50fw_tot[n][:,i_r],c=c_list[n],linestyle='-',label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),linewidth=1)
            py.scatter(xquant,v50fw_tot[n][:,i_r],c=c_list[n],edgecolors="none",s=5)

            py.plot(xquant, f_med_dict["vmax_med"][n],c=c_list[n],linestyle=':',alpha=0.6)
            
            #G = 6.674e-11 # m^3 kg^-1 s-2
            #Msun = 1.989e30 # kg
            #kpc = 3.0857e19
            #vcirc = np.sqrt(G * 10**bin_mh_mid*Msun / (f_med_dict["R200_med"][n]*kpc)) / 1e3
            #py.plot(xquant, vcirc,c=c_list[n],linestyle='-.',alpha=0.6)
            

        py.legend(loc='upper left',frameon=False,numpoints=1)

        r_bins = np.arange(0,1.1,0.1)
        py.annotate(r"$%3.1f < r \, / R_{\mathrm{vir}} < %3.1f$" % (r_bins[i_r],r_bins[i_r+1]) , (10.25,300))

        py.ylabel(r"$\langle v_{\mathrm{50}} \rangle \, / \mathrm{km s^{-1}}$")
        ylo = 0.0; yhi = 400.0

    if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            if n == 0:

                py.plot(xquant,v90fw[n][:,i_r],c=c_list[n],linestyle='--',label="ISM-wind",linewidth=1)
                py.scatter(xquant,v90fw[n][:,i_r],c=c_list[n],edgecolors="none",s=5)

                py.plot(xquant,v90fw_tot[n][:,i_r],c=c_list[n],linestyle='-',label="All outflowing gas",linewidth=1)
                py.scatter(xquant,v90fw_tot[n][:,i_r],c=c_list[n],edgecolors="none",s=5)
                
                py.plot(xquant, f_med_dict["vmax_med"][n],c=c_list[n],linestyle=':',alpha=0.6,label=r"$V_{\mathrm{max}}$")

            else:

                py.plot(xquant,v90fw[n][:,i_r],c=c_list[n],linestyle='--',linewidth=1)
                py.scatter(xquant,v90fw[n][:,i_r],c=c_list[n],edgecolors="none",s=5)

                py.plot(xquant,v90fw_tot[n][:,i_r],c=c_list[n],linestyle='-',linewidth=1)
                py.scatter(xquant,v90fw_tot[n][:,i_r],c=c_list[n],edgecolors="none",s=5)
                
                py.plot(xquant, f_med_dict["vmax_med"][n],c=c_list[n],linestyle=':',alpha=0.6)

            #G = 6.674e-11 # m^3 kg^-1 s-2
            #Msun = 1.989e30 # kg
            #kpc = 3.0857e19
            #vcirc = np.sqrt(G * 10**bin_mh_mid*Msun / (f_med_dict["R200_med"][n]*kpc)) / 1e3
            #py.plot(xquant, vcirc,c=c_list[n],linestyle='-.',alpha=0.6)


        py.legend(loc='upper left',frameon=False,numpoints=1,handlelength=3)

        py.ylabel(r"$\langle v_{\mathrm{90}} \rangle \, / \mathrm{km s^{-1}}$")
        ylo = 0.0; yhi = 699.


        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    #if xquant_s == "vmax":
    #    py.plot([0.0,0.0],[1000.,1000.],c="k",alpha=0.5)

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "velocity_ism_wind_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "velocity_ism_wind_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
