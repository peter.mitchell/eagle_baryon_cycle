import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as us

fVmax_cut = 0.25

vmin = 0.0
#vmin = 0.25

logmhalo_bin = 12.0
#logmhalo_bin = 13.0

#redshift = 2.0
redshift = 0.0

test_mode = False

median = False

exclude_ISM = True
if median:
    exclude_ISM = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 tree snipshots
sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 50 tree snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 50 Mpc ref with 28 tree snapshots
#sim_name = "L0050N0752_REF_snap"


part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################


############ Read shell data ##################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

#file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_v50_only.hdf5","r")
file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
dm = bin_mh_mid[1] - bin_mh_mid[0]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

iz = np.argmin(abs(z_grid - redshift))
imass = np.argmin(abs(logmhalo_bin - bin_mh_mid))

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

f_name_list = []
f_name_list2 = []

if median:
    f_name_list += ["dmdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
    f_name_list += ["dpdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
    f_name_list += ["dEkdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
    f_name_list += ["dEtdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]

    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
    
    f_name_list += ["dmdt_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list += ["dpdt_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list += ["dEkdt_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list += ["dEtdt_out_"+str(vmin).replace(".","p")+"vmax_med"]

    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]
else:
    f_name_list += ["dmdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind"]
    f_name_list += ["dpdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind"]
    f_name_list += ["dEkdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind"]
    f_name_list += ["dEtdt_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind"]

    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"] # Should be fine to look at median velocities (otherwise will need a NaN correction in build_grid for mean)
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"]
    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
    
    f_name_list += ["dmdt_out_"+str(vmin).replace(".","p")+"vmax"]
    f_name_list += ["dpdt_out_"+str(vmin).replace(".","p")+"vmax"]
    f_name_list += ["dEkdt_out_"+str(vmin).replace(".","p")+"vmax"]
    f_name_list += ["dEtdt_out_"+str(vmin).replace(".","p")+"vmax"]

    f_name_list += ["dmdt_out_"+str(vmin).replace(".","p")+"vmax_ISM"]
    f_name_list += ["dpdt_out_"+str(vmin).replace(".","p")+"vmax_ISM"]
    f_name_list += ["dEkdt_out_"+str(vmin).replace(".","p")+"vmax_ISM"]
    f_name_list += ["dEtdt_out_"+str(vmin).replace(".","p")+"vmax_ISM"]

    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v50fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]
    f_name_list2 += ["v90fw_out_"+str(vmin).replace(".","p")+"vmax_complete"]

    if "snap" in sim_name:
        vmin2 = 0.25
        vmin3 = 0.5
        f_name_list += ["dEdt_SNe_shell","dEdt_AGN_shell","dEdt_SNe_sat_shell", "dEdt_AGN_sat_shell"]
        f_name_list += ["dmdr_shell", "P50grad_shell", "Ggrad_shell"]
        f_name_list += ["P50fwgrad_"+str(vmin2).replace(".","p")+"vmax_shell"]
        f_name_list += ["P50fwgrad_"+str(vmin3).replace(".","p")+"vmax_shell"]


f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
f_med_dict2 = {}
for f_name in f_name_list2:
    f_med_dict2[f_name] = file_grid2[f_name][:]

# Set velocities of shells with >20% of galaxies without a measurement to NaN (note median is only for the non-NaN sample)

print ""
print "Add below block back in once I have the corrected completeness values"
print ""
'''nok = f_med_dict2["v50fw_out_"+str(vmin).replace(".","p")+"vmax_complete"] == 0
f_med_dict2["v50fw_out_"+str(vmin).replace(".","p")+"vmax_med"][nok] = np.nan
nok = f_med_dict2["v90fw_out_"+str(vmin).replace(".","p")+"vmax_complete"] == 0
f_med_dict2["v90fw_out_"+str(vmin).replace(".","p")+"vmax_med"][nok] = np.nan

nok = f_med_dict2["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"] == 0
f_med_dict2["v50fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"][nok] = np.nan
nok = f_med_dict2["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_complete"] == 0
f_med_dict2["v90fw_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"][nok] = np.nan'''


file_grid.close()
file_grid2.close()
 
from utilities_plotting import *

nrow = 3; ncol = 2

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.1,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,2.49*nrow],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'font.size':8,
                    'legend.fontsize':8,
                    'axes.labelsize':9})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
subplots = panelplot(nrow,ncol)


for i,ax in enumerate(subplots):
    py.axes(ax)

    #for tick in ax.xaxis.get_major_ticks():
    #    tick.label1On=True

    ylo = -0.5; yhi = 2.49
    xlo = 0.0; xhi = 1.0

    rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]

    if median:
        str1 = "_out_"+str(vmin).replace(".","p")+"vmax_med"
        str2 = "_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind_med"
    else:
        str1 = "_out_"+str(vmin).replace(".","p")+"vmax"
        str2 = "_out_"+str(vmin).replace(".","p")+"vmax"+fV_str+"_wind"

    if i == 0:

        if exclude_ISM:
            py.plot(rmid,np.log10(f_med_dict["dmdt"+str1][iz][imass] - f_med_dict["dmdt"+str1+"_ISM"][iz][imass]),c="k",label=r"Total gas",linewidth=1)
        else:
            py.plot(rmid,np.log10(f_med_dict["dmdt"+str1][iz][imass]),c="k",label=r"Total gas",linewidth=1)

        '''py.plot(rmid,np.log10(f_med_dict["dmdt"+str2][iz][imass]),c="k",label=r"ISM-wind",linestyle='--',linewidth=1)
        
        if exclude_ISM:
            py.plot(rmid,np.log10((f_med_dict["dmdt"+str1]-f_med_dict["dmdt"+str2] - f_med_dict["dmdt"+str1+"_ISM"])[iz][imass]),c="k",label=r"Not ISM-wind",linestyle=':',linewidth=1)
        else:
            py.plot(rmid,np.log10((f_med_dict["dmdt"+str1]-f_med_dict["dmdt"+str2])[iz][imass]),c="k",label=r"Not ISM-wind",linestyle=':',linewidth=1)'''

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        py.ylabel(r"$\log_{10}(\dot{M}_{\mathrm{out}} \, / \mathrm{M_\odot yr^{-1}} )$")

        py.annotate(r"$%3.1f < z < %3.1f$" % (1./a_max[iz]-1,1./a_min[iz]-1) , (0.05,0.9))
        py.annotate(r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[imass]-dm*0.5,bin_mh_mid[imass]+dm*0.5) , (0.05,1.05))
        ylo = -0.09; yhi = 1.1

    elif i == 1:

        if exclude_ISM:
            py.plot(rmid,np.log10(f_med_dict["dpdt"+str1][iz][imass]-f_med_dict["dpdt"+str1+"_ISM"][iz][imass]),c="k",linewidth=1)
            #py.plot(rmid,np.log10((f_med_dict["dpdt"+str1]-f_med_dict["dpdt"+str2]-f_med_dict["dpdt"+str1+"_ISM"])[iz][imass]),c="k",linestyle=':',linewidth=1)
        else:
            py.plot(rmid,np.log10(f_med_dict["dpdt"+str1][iz][imass]),c="k",linewidth=1)
            #py.plot(rmid,np.log10((f_med_dict["dpdt"+str1]-f_med_dict["dpdt"+str2])[iz][imass]),c="k",linestyle=':',linewidth=1)
 
        #py.plot(rmid,np.log10(f_med_dict["dpdt"+str2][iz][imass]),c="k",linestyle='--',linewidth=1)
        
        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        py.ylabel(r"$\log_{10}(\dot{p}_{\mathrm{out}} \, / \mathrm{M_\odot \, km s^{-1} \, yr^{-1}} )$")

        #py.annotate(r"$z=2$",(0.05,1.3))
        ylo = 2.11; yhi = 3.3

    elif i == 2:

        if exclude_ISM:
            py.plot(rmid,np.log10(f_med_dict["dEkdt"+str1][iz][imass]-f_med_dict["dEkdt"+str1+"_ISM"][iz][imass]),c="b",label=r"Kinetic",linewidth=1)
            py.plot(rmid,np.log10(f_med_dict["dEtdt"+str1][iz][imass]-f_med_dict["dEtdt"+str1+"_ISM"][iz][imass]),c="r",label=r"Thermal",linewidth=1)

            #py.plot(rmid,np.log10((f_med_dict["dEkdt"+str1]-f_med_dict["dEkdt"+str2]-f_med_dict["dEkdt"+str1+"_ISM"])[iz][imass]),c="b",linestyle=':',linewidth=1)
            #py.plot(rmid,np.log10((f_med_dict["dEtdt"+str1]-f_med_dict["dEtdt"+str2]-f_med_dict["dEtdt"+str1+"_ISM"])[iz][imass]),c="r",linestyle=':',linewidth=1)

            py.plot(rmid, np.log10(f_med_dict["dEdt_SNe_shell"][iz][imass]),c="k",linestyle='--',linewidth=1,label="SNe injected cent")
            py.plot(rmid, np.log10(f_med_dict["dEdt_SNe_sat_shell"][iz][imass]),c="k",linestyle=':',linewidth=1,label="SNe injected sat")
            py.plot(rmid, np.log10(f_med_dict["dEdt_AGN_shell"][iz][imass]),c="c",linestyle='--',linewidth=1,label="AGN injected cent")
            py.plot(rmid, np.log10(f_med_dict["dEdt_AGN_sat_shell"][iz][imass]),c="c",linestyle=':',linewidth=1,label="AGN injected sat")

        
        else:
            py.plot(rmid,np.log10(f_med_dict["dEkdt"+str1][iz][imass]),c="b",label=r"Kinetic",linewidth=1)
            py.plot(rmid,np.log10(f_med_dict["dEtdt"+str1][iz][imass]),c="r",label=r"Thermal",linewidth=1)

            #py.plot(rmid,np.log10((f_med_dict["dEkdt"+str1]-f_med_dict["dEkdt"+str2])[iz][imass]),c="b",linestyle=':',linewidth=1)
            #py.plot(rmid,np.log10((f_med_dict["dEtdt"+str1]-f_med_dict["dEtdt"+str2])[iz][imass]),c="r",linestyle=':',linewidth=1)

        #py.plot(rmid,np.log10(f_med_dict["dEtdt"+str2][iz][imass]),c="r",linestyle='--',linewidth=1)
        #py.plot(rmid,np.log10(f_med_dict["dEkdt"+str2][iz][imass]),c="b",linestyle='--',linewidth=1)


        jim = f_med_dict["dEtdt"+str1][iz][imass] +f_med_dict["dEkdt"+str1][iz][imass]
        tim = f_med_dict["dEtdt"+str2][iz][imass] +f_med_dict["dEkdt"+str2][iz][imass]

        sim = jim - tim

        py.plot(rmid, np.log10(jim), c="k", label=r"Total energy",linewidth=1)
        #py.plot(rmid, np.log10(tim), c="k", linestyle='--',linewidth=1)
        #py.plot(rmid, np.log10(sim), c="k", linestyle=':',linewidth=1)
        
        legend1 = py.legend(loc='lower left',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        py.ylabel(r"$\log_{10}(\dot{E}_{\mathrm{out}} \, / \mathrm{M_\odot \, km^2 s^{-2} \, yr^{-1}} )$")

        #py.annotate(r"$z=2$",(0.05,1.3))
        ylo = 4.6; yhi = 5.8

    elif i == 3:

        py.plot(rmid,np.log10(f_med_dict2["v50fw"+str1+"_med"][iz][imass]),c="b",label=r"$V_{\mathrm{50}}$",linewidth=1)
        #py.plot(rmid,np.log10(f_med_dict2["v50fw"+str2+"_med"][iz][imass]),c="b",linestyle='--',linewidth=1)

        py.plot(rmid,np.log10(f_med_dict2["v90fw"+str1+"_med"][iz][imass]),c="r",label=r"$V_{\mathrm{90}}$",linewidth=1)
        #py.plot(rmid,np.log10(f_med_dict2["v90fw"+str2+"_med"][iz][imass]),c="r",linestyle='--',linewidth=1)
        
        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)
        py.gca().add_artist(legend1)

        py.ylabel(r"$\log_{10}(V_{\mathrm{out}} \, / \mathrm{km s^{-1}} )$")

        #py.annotate(r"$z=2$",(0.05,1.3))
        ylo = 1.7; yhi = 2.9

    elif i == 4:

        py.plot(rmid, np.log10(f_med_dict["dmdr_shell"][iz][imass]),c="k",linewidth=1)


    elif i == 5:

        rmid_mid = 0.5*(np.array(rmid[1:])+np.array(rmid[0:-1]))

        py.plot(rmid_mid, np.log10(f_med_dict["Ggrad_shell"][iz][imass]),c="k",linewidth=1)
        py.plot(rmid_mid, np.log10(f_med_dict["P50fwgrad_0p25vmax_shell"][iz][imass]),c="r",linewidth=1,linestyle='--')
        py.plot(rmid_mid, np.log10(f_med_dict["P50fwgrad_0p5vmax_shell"][iz][imass]),c="r",linewidth=1,linestyle=':')
        py.plot(rmid_mid, np.log10(f_med_dict["P50grad_shell"][iz][imass]),c="r",linewidth=1)
        
    if i == 4 or i == 5:
        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

    
    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "outflow_shells_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
