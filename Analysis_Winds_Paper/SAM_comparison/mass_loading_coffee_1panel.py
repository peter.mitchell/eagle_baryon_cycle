import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time


test_mode = False

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"
output_path = input_path

# 100 Mpc ref with 50 tree snapshots
sim_name = "L0100N1504_REF_50_snip"
sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/"
nsnap =50
tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"

# 25 Mpc ref with 50 tree snapshots
#sim_name = "L0025N0376_REF_50_snip"
#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/"
#snap =50
#tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["sf_reheat_ml","sf_wind_ml","nsfism_reheat_ml","nsfism_wind_ml","ism_reheat_wind_ml","halo_reheat_ml","halo_wind_ml","halo_reheat_wind_ml","ism_reheat_tot_ml","ism_wind_tot_ml","halo_wind_tot_ml","ism_wind_tot_subnorm", "halo_wind_tot_subnorm", "vmax_med","mstar_med"]

if "snap" in sim_name:
    f_name_list += ["ism_tot_eml","halo_tot_eml"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

vmax = f_med_dict["vmax_med"]
gamma_SN = 3.2
V_SN = 320
ml_galform = (vmax / V_SN)**(-gamma_SN)

# Henriques 15 outflow rates
epsilon_0 = 2.6
beta_1 = 0.72
V_reheat = 480
epsilon_disk = epsilon_0 * (0.5 + (vmax/V_reheat)**(-beta_1))
eta_0 = 0.62
V_eject = 100
beta_2 = 0.8
epsilon_halo = eta_0 * (0.5 + (vmax/V_eject)**(-beta_2))
V_SN = 630
ml_lgals_halo = epsilon_halo * (V_SN/vmax)**2 - epsilon_disk # halo mass loading

saturate = epsilon_halo * 0.5 * V_SN**2 < 0.5 * epsilon_disk * vmax**2
ml_lgals_ism = epsilon_disk # ISM mass loading
ml_lgals_ism[saturate] = (epsilon_halo * (V_SN/vmax)**2)[saturate]

ml_lgals_halo[ml_lgals_halo<0.0] = 1e-9

mstar_med = f_med_dict["mstar_med"]
logmstar_ch17, logml_ch17, lo_ch17, hi_ch17 = np.loadtxt("Chisholm17.txt",unpack=True)
import utilities_interpolation as ui
logmhalo_ch17 = ui.Linear_Interpolation(np.log10(mstar_med[0]), bin_mh_mid, logmstar_ch17)

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.96,
                    'font.size':10,
                    'legend.fontsize':5})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 14.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    #py.plot(bin_mh_mid,np.log10(ml_galform[0]),label=(r"GALFORM, Lacey et al. (2016)"),c="g",linewidth=2)
    
    #py.plot(bin_mh_mid,np.log10(ml_lgals_ism[0]),label=(r"L-Galaxies, ISM-scale"),c="b",linewidth=2)
    #py.plot(bin_mh_mid,np.log10(ml_lgals_halo[0]),label=(r"L-Galaxies, Halo-scale"),c="b",linewidth=2,linestyle='--')

    errors = [-lo_ch17, hi_ch17]
    py.errorbar(logmhalo_ch17, logml_ch17, errors, fmt="k^",label="Chisholm et al. (2017)",ms=3)

    show_list = [0]
    show_eagle = True
    show_ism = False
    label_base = "EAGLE, ISM-scale, "
    yval = np.log10(f_med_dict["ism_wind_tot_ml"])
    yval2 = np.log10(f_med_dict["halo_wind_tot_ml"])
    label_base2 = "EAGLE, Halo-scale"

    if show_eagle:
        for n in range(len(a_grid)):
            n_i = len(a_grid)-1-n
            if n_i in show_list:
                py.plot(bin_mh_mid,yval[n_i],label=(label_base+r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n_i]-1,1./a_min[n_i]-1)),c=c_list[n_i],linewidth = 2)
                py.scatter(bin_mh_mid,yval[n_i],c=c_list[n_i],edgecolors="none",s=10)

                if not show_ism:
                    py.plot(bin_mh_mid,yval2[n_i],label=label_base2,c=c_list[n_i],linewidth = 2,linestyle='--')
                    py.scatter(bin_mh_mid,yval[n_i],c=c_list[n_i],edgecolors="none",s=10)

    py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=3)
    py.ylabel(r"$\log(\eta = \dot{M}_{\mathrm{out}} \, /\dot{M}_\star)$")
    ylo = -0.5; yhi = 3.0


    py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_coffee_1panel_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_coffee_1panel_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
