import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

show_incomplete = False

test_mode = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str, "halo_wind_tot_subnorm"+fV_str,"vmax_med","mstar_med"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

# For Galform, we need the relationship between v_galaxy and m_halo - which I'm getting from graph click of a figure I made to understand the mstar-mhalo scatter in the Eagle-Galform comparison paper:
# As such, this is valid for the reference model presented in that paper
logmh_gal   = np.array([10.178, 10.525, 10.880, 11.228, 11.575, 11.923, 12.270, 12.625, 12.973, 13.320, 13.676])
logvgal_gal = np.array([1.596,  1.710,  1.829,  1.966,  2.067,  2.172,  2.332,  2.584,  2.757,  2.835,  2.831])
vgal_gal = 10**logvgal_gal

gamma_SN = 3.2
#V_SN = 320 # Lacey 18
V_SN = 380 # Consistent with the model used in Mitchell 18 - which is where we taking v_gal from
ml_galform = (vgal_gal / V_SN)**(-gamma_SN)

# Henriques 15 outflow rates
vmax = f_med_dict["vmax_med"]
epsilon_0 = 2.6
beta_1 = 0.72
V_reheat = 480
epsilon_disk = epsilon_0 * (0.5 + (vmax/V_reheat)**(-beta_1))
eta_0 = 0.62
V_eject = 100
beta_2 = 0.8
epsilon_halo = eta_0 * (0.5 + (vmax/V_eject)**(-beta_2))
V_SN = 630
print "the vmax here shoiuld be a V_200 I think"
ml_lgals_halo = epsilon_halo * (V_SN/vmax)**2 - epsilon_disk # halo mass loading

saturate = epsilon_halo * 0.5 * V_SN**2 < 0.5 * epsilon_disk * vmax**2
ml_lgals_ism = epsilon_disk # ISM mass loading
print "the vmax here shoiuld be a V_200 I think"
ml_lgals_ism[saturate] = (epsilon_halo * (V_SN/vmax)**2)[saturate]

ml_lgals_halo[ml_lgals_halo<0.0] = 1e-9


# Somerville 15 model (similar to 08) - vmax dependent variable
eta_sn = 1.5
v0 = 200
alpha_rh = 2.2
ml_s15_ism = eta_sn * (v0 / vmax)**(alpha_rh)
#ml_s15_halo = ??# Not the same - need to see s08 for details...

Veject = 120.0
alpha_eject = 6
print "hack - v200 here should be v200 not vmax"
#frac_ej = (1 + (V200 / Veject)**alpha_eject)**-1 # Eqn 13 from S08
frac_ej = (1 + (vmax / Veject)**alpha_eject)**-1 # Eqn 13 from S08
ml_s15_halo = ml_s15_ism * frac_ej

# GAEA model (Note they are now using the "FIRE" model as their fiducial model moving forwards, complete with explicit redshift dependence...)

alpha = np.zeros_like(vmax[0])-3.2
ok = vmax[0] > 60
alpha[ok] = -1
eps_reheat = 0.3
ml_h16_ism = eps_reheat * (1+z_grid[0])**1.25 * (vmax[0]/60)**alpha

V_SN = 630
epsilon_eject = 0.1
epsilon_halo = epsilon_eject * (1+z_grid[0])**1.25 * (vmax[0]/60)**alpha
print "should be a v200 here"
ml_h16_halo = epsilon_halo * (V_SN/vmax[0])**2 - ml_h16_ism

# Chisholm 17 observations
mstar_med = f_med_dict["mstar_med"]
logmstar_ch17, logml_ch17, lo_ch17, hi_ch17 = np.loadtxt("Chisholm17.txt",unpack=True)
import utilities_interpolation as ui
logmhalo_ch17 = ui.Linear_Interpolation(np.log10(mstar_med[0]), bin_mh_mid, logmstar_ch17)

# Shark default model from Lagos 18 paper
# To see which SNe scheme and parameters were chosen, see her Table 3 (she uses the "Lagos13" parametrisation)
# Claudia's paper states that the dependent variable is V_circ (implying at R_vir) as the dependent variable.
# But then she states "Vcirc is the maximum circular velocity of the galaxy" - so I'll just assume she meant V_max
v_sn_L18 = 110 # From table in paper this is also V_hot
beta_disk_L18 = 4.5
redshift_power_L18 = 0.12
eps_halo_L18 = 2
eps_disk_L18 = 1
vhot_prime = v_sn_L18 * (1+z_grid[0]) **redshift_power_L18
f_L18 = eps_disk_L18 * (vmax[0]/v_sn_L18)**(-1*beta_disk_L18) # There must be a typo in Claudia's equation 
V_w_L18 = 1.9 * (vmax[0])**1.1
E_excess_L18_over_SFR = eps_halo_L18 * V_w_L18**2 /2. * f_L18 
mdot_outflow_L18_over_SFR = f_L18
ml_L18_ism = mdot_outflow_L18_over_SFR
ml_L18_halo = E_excess_L18_over_SFR / (vmax[0]**2 /2) - mdot_outflow_L18_over_SFR
problem = ml_L18_halo < 0.0
ml_L18_ism[problem] = E_excess_L18_over_SFR[problem] / (vmax[0][problem]**2 /2)
ml_L18_halo[problem] *= 0.0

file_grid.close()

file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str,"ism_wind_cum_ml_pp_complete"+fV_str+"_mstar","ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc"]
for f_name in f_name_list2:
    f_med_dict[f_name] = file_grid2[f_name][:]

file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.96,
                    'font.size':10,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 13.5

for i,ax in enumerate(subplots):
    py.axes(ax)

    #print "Hack"
    #bin_mh_mid = np.log10(vmax[0])

    for n in range(len(a_grid)):
        if n > 0:
            break

        if show_incomplete:
            ok = bin_mh_mid > 0
        else:
            ok = f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1

            py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]),c="b",linewidth=1,alpha=0.3)
            py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n]),c="b",linestyle='--',linewidth=1,alpha=0.3)

        py.plot(bin_mh_mid[ok],np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n][ok]),label=(r"EAGLE"),c="b",linewidth=1)
        py.plot(bin_mh_mid[ok],np.log10(f_med_dict["halo_wind_tot_ml"+fV_str][n][ok]),c="b",linestyle='--',linewidth=1)


    py.plot(logmh_gal,np.log10(ml_galform),label=(r"GALFORM"),c="g",linewidth=1)
    
    py.plot(bin_mh_mid,np.log10(ml_lgals_ism[0]),label=(r"L-Galaxies"),c="k",linewidth=1)
    py.plot(bin_mh_mid,np.log10(ml_lgals_halo[0]),c="k",linestyle='--',linewidth=1)
    
    py.plot(bin_mh_mid, np.log10(ml_s15_ism[0]),label=(r"Santa Cruz"),c="y",linewidth=1)
    py.plot(bin_mh_mid, np.log10(ml_s15_halo[0]),c="y",linestyle='--',linewidth=1)

    py.plot(bin_mh_mid, np.log10(ml_h16_ism),label=(r"GAEA"),c="r",linewidth=1)
    py.plot(bin_mh_mid, np.log10(ml_h16_halo),c="r",linestyle='--',linewidth=1)

    py.plot(bin_mh_mid, np.log10(ml_L18_ism),label=(r"SHARK"),c="c",linewidth=1)
    py.plot(bin_mh_mid, np.log10(ml_L18_halo),c="c",linewidth=1,linestyle='--')

    #errors = [-lo_ch17, hi_ch17]
    #py.errorbar(logmhalo_ch17, logml_ch17, errors, fmt="k^",label="Chisholm et al. (2017)",ms=3)


        
    legend1 = py.legend(loc='upper right',frameon=False,numpoints=1,handlelength=1.5)
    py.gca().add_artist(legend1)

    l1, = py.plot([-20,-20],[-20,20],c="k",linewidth=1)
    l2, = py.plot([-20,-20],[-20,20],c="k",linestyle='--',linewidth=1)
    py.legend(handles=[l1,l2],labels=[r"Galaxy scale",r"Halo scale"], loc="lower left",frameon=False,numpoints=1,handlelength=3)

    py.ylabel(r"$\log_{10}(\eta = \dot{M}_{\mathrm{out}} \, /\dot{M}_\star)$")
    ylo = -1.5; yhi = 3.0

    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_sam_comp_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "mass_loading_sam_comp_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
