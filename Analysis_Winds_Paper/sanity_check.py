import numpy as np
import h5py
from utilities_plotting import *

case = 2

if case == 1:
    test_mode = False
    sim_name = "L0100N1504_REF_50_snip"
    snap_list = np.arange(0,51)[::-1]
elif case == 2:
    test_mode = False
    sim_name = "L0025N0376_REF_50_snip"
    snap_final = 50
    snap_list = np.arange(0,snap_final+1)[::-1]
elif case == 3:
    test_mode = True
    sim_name = "L0025N0376_REF_50_snip"
    snap_final = 50
    snap_list = np.arange(0,snap_final+1)[::-1]
elif case == 4:
    test_mode = True
    sim_name = "L0100N1504_REF_50_snip"
    snap_list = np.arange(0,51)[::-1]

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"
filename = "subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename)

subhalo_group = File["Snap_"+str(23)+"/subhalo_properties"]

vmax = subhalo_group["Vmax"][:]
r200 = subhalo_group["Group_R_Crit200_host"][:]
m200 = subhalo_group["Group_M_Crit200_host"][:]
sgrn = subhalo_group["SubGroupNumber_hydro"][:]


mstar = subhalo_group["mass_star"][:]
mstars_new = subhalo_group["mass_new_stars_init"][:]

mass_SF = subhalo_group["mass_gas_SF"][:]
mass_NSF = subhalo_group["mass_gas_NSF"][:]

mass_sf_ism_reheated = subhalo_group["mass_sf_ism_reheated"][:]
mhalo_reheat = subhalo_group["mass_halo_reheated"][:]

mism_wind = subhalo_group["mass_sf_ism_join_wind"][:]

mass_dm_acc = subhalo_group["mass_diffuse_dm_accretion"][:]

h=0.6777
g2Msun = 1./(1.989e33)
m200 *= 1.989e43 * g2Msun / h
mass_dm_acc *= 1.989e43 * g2Msun / h

ok = sgrn == 0

jim = mism_wind[ok] == 0
mism_wind = mism_wind[ok]
mism_wind[jim] = 10**5.5

#py.scatter(np.log10(m200[ok]), np.log10(mass_sf_ism_reheated[ok]))
py.scatter(np.log10(m200[ok]), np.log10(mass_SF[ok]))
py.xlim((9,14))
py.ylim((5.5,10))
#py.ylim((0,4))
py.show()
