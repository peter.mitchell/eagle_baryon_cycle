import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False
show_fakhouri_formula = False # Note Correa formula has the complication that you need to know the final mass of the halo - which is a bit awkward

input_path_base = "/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

#sim_name_list = ["L0025N0376_REF_snap","L0025N0376_REF_snap_par_test"]
sim_name_list = ["L0025N0376_REF_snap","L0025N0376_REF_snap_retain_mem_false_fidcuts","L0025N0376_REF_snap_standardsubcuts"]
sim_path_list = ["/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/","/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/","/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"]
snap_list = [28,28,28]
tree_file_list = ["/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5","/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5","/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"]
disks = ["/cosma7", "/cosma7","/cosma6"]

lo = 0; hi = 6
sim_name_list = sim_name_list[lo:hi]
sim_path_list = sim_path_list[lo:hi]
snap_list = snap_list[lo:hi]
tree_file_list = tree_file_list[lo:hi]

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


if show_fakhouri_formula:
    dmdt = lambda mhalo,z: 46.1 * (mhalo/1e12)**1.1 * (1+1.11*z) * np.sqrt(omm * (1+z)**3 + oml) # Msun yr^-1

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,2.49],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0


########### Load tabulated efficiencies #########################

i_sim = -1
for sim_name, sim_path, snap, tree_file, disk in zip(sim_name_list, sim_path_list, snap_list, tree_file_list, disks):
    i_sim += 1

    input_path = disk + input_path_base

    # Neistein uses dark matter subhalo mass as "halo mass"
    if test_mode:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5")
    else:
        print "reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str, "halo_wind_tot_subnorm"+fV_str]


    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    ls_list = [':','--','-','-.',':','--','-']
    #index_a_grid_show = [0,2,4,6]
    index_a_grid_show = [0,1,2,3,4,5,6]
    #index_a_grid_show = [5,6,7,8]

    for i,ax in enumerate(subplots):
        py.axes(ax)

        if i == 0:
            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
                #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][n]),label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=py.cm.tab10(c_index))
                py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]),c=c_list[n],edgecolors="none",s=5)

            py.legend(loc='upper right',frameon=False,numpoints=1)
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            ylo = -2.75; yhi = 0.0

        if i == 1:
            for n in index_a_grid_show:

                if n == 0:
                    label = sim_name
                else:
                    label = ""

                #py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"][n]),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                #py.scatter(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"][n]),c=c_list[n],edgecolors="none",s=5)

                #quant = f_med_dict["halo_wind_ml"][n]
                quant = f_med_dict["halo_wind_tot_subnorm"+fV_str][n]
                #quant = f_med_dict["halo_wind_tot_ml"][n]
                py.plot(bin_mh_mid,np.log10(quant),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                py.scatter(bin_mh_mid,np.log10(quant),c=c_list[n],edgecolors="none",s=5)

            ylo = -2.25; yhi = 0.5
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            py.legend(loc='upper right',frameon=False,numpoints=1)


        if i == 2 or i == 3:
            py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_consistency_test_mode.pdf"
else:
    fig_name = "mass_loading_consistency.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
