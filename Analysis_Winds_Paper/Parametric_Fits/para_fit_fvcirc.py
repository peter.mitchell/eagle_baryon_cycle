import matplotlib.pyplot as py
import numpy as np
import scipy.optimize as so
import h5py

x = 10**np.arange(10,15,0.1)

def ISM_Wind_Func(x, N,n1,n2,M1,Mcut,n3):

    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2)) * np.exp(-x/Mcut))
    
    #y = N * ( (x/M1) **n1 + (x/M1)**n2)   
    #logy = np.log10(y)  * np.exp(-x/Mcut)
    
    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2) * np.exp(-x/Mcut)**2)

    #logy = np.log10( N * ( (x/M1) **n1 + (x/M1)**n2) * 0.1*(10-x/Mcut)**0.2)

    #acut = 0.3
    #logy = np.log10(y)  * (10-x/Mcut)**acut

    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2 * np.exp(-x/Mcut)))

    # Behroozi 13 inspired (The idea from him is similar to my own idea, which is fit three power laws and use an expontial decay function to kill the contribution of the 3rd power law in the low-mass range
    # The Behroozi part is the exponential damp term at the end - I'm not using a superlogarithm here  
    logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2)) + n3 * np.log10( x/M1)  * np.exp(-Mcut/x)

    # Hacky wat to apply hard priors to certain parameters
    if n3 > 0:
        logy *= 1e9

    if Mcut < M1:
        logy *= 1e9

    if np.log10(Mcut) <2.2:
        logy *= 1e9
    
    return logy

N = 1
n1 = -2.0
n2 = 1.0
M1 = 150
Mcut = 250
n3 = -3.0

# Fit ISM or halo mass loading
ism_wind = True

sim_name = "L0100N1504_REF_200_snip"
part_mass = "particle_mass"
#file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5","r")
file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_vc_mid = file_grid["log_vcirc_grid"][:]
t_grid = file_grid["t_grid"][:]
#a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

print "temp hack, using median expansion factor for 100 Mpc, 200 snipshot"
a_grid = np.array([0.89337879, 0.67392123, 0.47913527, 0.34346414, 0.2583262, 0.19983937, 0.14593461, 0.0845002 ])
z_grid = 1./a_grid -1.0

fVmax_cut = 0.25
fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str+"_vcirc"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

f_name_list = ["ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc"]
file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
file_grid.close()

if ism_wind:
    logy_grid = np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_vcirc"])
else:
    print "No"
    quit()
    logy_grid = np.log10(f_med_dict["halo_wind_tot_ml"])

# highest-z bins are not well fit
if ism_wind:
    cut = 5
    z_bins = np.arange(0,cut)
    z_grid = z_grid[0:cut]
    a_grid = a_grid[0:cut]

    scnd_order = False

else:
    z_bins = np.arange(0,7)
    z_grid = z_grid[0:7]
    a_grid = a_grid[0:7]

N_f = np.zeros(len(z_bins))
n1_f = np.zeros(len(z_bins))
n2_f = np.zeros(len(z_bins))
M1_f = np.zeros(len(z_bins))
Mcut_f = np.zeros(len(z_bins))
n3_f = np.zeros(len(z_bins))

for z_bin in z_bins:

    params_guess = (N, n1, n2, M1, Mcut, n3)

    x = 10**bin_vc_mid

    logy = logy_grid[z_bin]

    ok = (np.isnan(logy)==False) & (x>10**1.4) & (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str+"_vcirc"][z_bin] == 1)
    x = x[ok]; logy = logy[ok]

    out = so.curve_fit(ISM_Wind_Func, x, logy, params_guess)
    
    N_f[z_bin] = out[0][0]
    n1_f[z_bin] = out[0][1]
    n2_f[z_bin] = out[0][2]
    M1_f[z_bin] = out[0][3]
    Mcut_f[z_bin] = out[0][4]
    n3_f[z_bin] = out[0][5]
   
    #print out[0]

    show_each_fit = False
    show_all_fits = False

    if show_each_fit:
        logy_fit = ISM_Wind_Func(10**bin_vc_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
        py.plot(bin_vc_mid,logy_grid[z_bin],c="b")
        py.plot(bin_vc_mid,logy_fit,c="y")
        if ism_wind:
            py.ylim((-0.5,1.5))
        else:
            py.ylim((0.,2.5))
        py.show()

    elif show_all_fits:
        c_list = ["k","m","b","c","g","y","orange"]
        logy_fit = ISM_Wind_Func(10**bin_vc_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
        py.plot(bin_mh_mid,logy_grid[z_bin],c=c_list[z_bin])
        py.plot(bin_mh_mid,logy_fit,c=c_list[z_bin],linestyle='--')
        if ism_wind:
            py.ylim((-0.5,1.5))
        else:
            py.ylim((0.,2.5))

if show_all_fits:
    py.show()
    quit()

if show_each_fit:
    quit()

def Poly_Fit2(a,p0,p1,p2):
    
    y = p0 + p1*a + p2*a**2
    
    return y

def Poly_Fit1(a,p0,p1):
    
    y = p0 + p1*a
    
    return y

if ism_wind:

    if scnd_order:
        p_guess = [0.2,0.,0.0]
        out_N = so.curve_fit(Poly_Fit2, z_grid, np.log10(N_f), p_guess)
        p0_N, p1_N, p2_N = out_N[0]
        print "logN fit p0,p1,p2 = ", p0_N,p1_N,p2_N, "only valid for z>", z_grid[0]
    
    else:
        p_guess = [0.2,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, np.log10(N_f), p_guess)
        p0_N, p1_N = out_N[0]
        print "logN fit p0,p1 = ", p0_N,p1_N, "only valid for z>", z_grid[0]

        p_guess = [2.2,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, np.log10(M1_f), p_guess)
        p0_M1, p1_M1 = out_N[0]
        print "logM1 fit p0,p1 = ", p0_M1,p1_M1, "only valid for z>", z_grid[0]

        p_guess = [0.2,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, n1_f, p_guess)
        p0_n1, p1_n1 = out_N[0]
        print "n1 fit p0,p1 = ", p0_n1,p1_n1, "only valid for z>", z_grid[0]

        
show_param_evo = False

if show_param_evo:
    py.subplot(321)
    py.plot(z_grid, np.log10(N_f))
    if ism_wind:
        z_check = np.arange(0,3,0.01)
        a_check = 1./(1+z_check)
        if scnd_order:
            py.plot(z_check, Poly_Fit2(z_check,p0_N,p1_N,p2_N),c="y")
        else:
            py.plot(z_check, Poly_Fit1(z_check,p0_N,p1_N),c="y")
    else:
        z_check = np.arange(0,7)
        a_check = 1./(1+z_check)
        py.plot(z_check, Poly_Fit2(a_check,p0_N,p1_N,p2_N),c="y")

    py.ylabel(r"$\log(N)$")

    py.subplot(322)
    py.plot(z_grid, n1_f)
    py.ylabel(r"$n_1$")

    if not scnd_order:
        py.plot(z_grid, Poly_Fit1(z_grid,p0_n1,p1_n1),c="y")


    py.subplot(323)
    py.plot(z_grid, n2_f)
    py.ylabel(r"$n_2$")

    py.subplot(324)
    py.plot(z_grid, np.log10(M1_f))
    py.ylabel(r"$\log(M_1)$")

    if not ism_wind:
        py.plot(z_grid, Poly_Fit2(a_grid,p0_M1,p1_M1,p2_M1),c="y")

    if not scnd_order:
        py.plot(z_grid, Poly_Fit1(z_grid,p0_M1,p1_M1),c="y")

    py.subplot(325)
    py.plot(z_grid, np.log10(Mcut_f))
    py.ylabel(r"$\log(M_{cut}$")

    py.subplot(326)
    py.plot(z_grid, n3_f)
    py.ylabel(r"$n_3$")

    py.show()
    quit()

# For parameters which we won't evolve, choose which bin to take the best-fit value from
anchor_bin = 1

if ism_wind:
    if scnd_order:
        N_f_g = 10**(Poly_Fit2(z_grid, p0_N,p1_N,p2_N))
        n1_g = n1_f[anchor_bin]
        M1_g = M1_f[anchor_bin]
    else:
        N_f_g = 10**(Poly_Fit1(z_grid, p0_N,p1_N))
        M1_g = 10**(Poly_Fit1(z_grid, p0_M1,p1_M1))
        n1_g = Poly_Fit1(z_grid, p0_n1,p1_n1)
        

n2_g = n2_f[anchor_bin]
Mcut_g = Mcut_f[anchor_bin]
n3_g = n3_f[anchor_bin]

if scnd_order:
    print "n1, n2,M1,logM1,n3,Mcut,logMcut", n1_g, n2_g, M1_g, np.log10(M1_g), n3_g, Mcut_g, np.log10(Mcut_g)
else:
    print "n2,n3,Mcut,logMcut", n2_g, n3_g, Mcut_g, np.log10(Mcut_g)

show_z_fits = True
if show_z_fits:
    for z_bin in z_bins:

        logy_fit_z = ISM_Wind_Func(10**bin_vc_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
        if ism_wind:
            if scnd_order:
                logy_fit_evo = ISM_Wind_Func(10**bin_vc_mid, N_f_g[z_bin], n1_g, n2_g, M1_g, Mcut_g,n3_g)
            else:
                logy_fit_evo = ISM_Wind_Func(10**bin_vc_mid, N_f_g[z_bin], n1_g[z_bin], n2_g, M1_g[z_bin], Mcut_g,n3_g)

        py.plot(bin_vc_mid,logy_grid[z_bin],c="b")
        py.plot(bin_vc_mid,logy_fit_z,c="c")
        py.plot(bin_vc_mid,logy_fit_evo,c="y")
        py.ylim((-0.5,2.5))
        py.show()

print "Fit is valid for range", z_grid.min(), z_grid.max()

c_list = ["k","m","b","c","g","y","orange"]

for z_bin in z_bins:

    #logy_fit_z = ISM_Wind_Func(10**bin_vc_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
    if ism_wind:
        if scnd_order:
            logy_fit_evo = ISM_Wind_Func(10**bin_vc_mid, N_f_g[z_bin], n1_g, n2_g, M1_g, Mcut_g,n3_g)
        else:
            logy_fit_evo = ISM_Wind_Func(10**bin_vc_mid, N_f_g[z_bin], n1_g[z_bin], n2_g, M1_g[z_bin], Mcut_g,n3_g)

    py.plot(bin_vc_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str+"_vcirc"][z_bin]),c=c_list[z_bin])
    py.plot(bin_vc_mid,logy_fit_evo,c=c_list[z_bin],linestyle='--')
    
py.show()
