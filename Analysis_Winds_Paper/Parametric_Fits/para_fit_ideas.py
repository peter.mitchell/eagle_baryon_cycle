import matplotlib.pyplot as py
import numpy as np
import scipy.optimize as so
import h5py

x = 10**np.arange(10,15,0.1)

def ISM_Wind_Func(x, N,n1,n2,M1,Mcut,n3,delta):

    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2)) * np.exp(-x/Mcut))
    
    #y = N * ( (x/M1) **n1 + (x/M1)**n2)   
    #logy = np.log10(y)  * np.exp(-x/Mcut)
    
    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2) * np.exp(-x/Mcut)**2)

    #logy = np.log10( N * ( (x/M1) **n1 + (x/M1)**n2) * 0.1*(10-x/Mcut)**0.2)

    #acut = 0.3
    #logy = np.log10(y)  * (10-x/Mcut)**acut

    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2 * np.exp(-x/Mcut)))

    # Behroozi 13 inspired (The idea from him is similar to my own idea, which is fit three power laws and use an expontial decay function to kill the contribution of the 3rd power law in the low-mass range
    # The Behroozi part is the exponential damp term at the end - I'm not using a superlogarithm here  
    logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2)) + delta * np.log10( (x/M1)**n3)  * np.exp(-Mcut/x)
    
    return logy

N = 1
n1 = -0.5
n2 = 0.5
M1 = 1e12
Mcut = 2e13
n3 = 1.0
delta = -1.0

sim_name = "L0100N1504_REF_200_snip"
part_mass = "particle_mass"
file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["sf_reheat_ml","sf_wind_ml","nsfism_reheat_ml","nsfism_wind_ml","ism_reheat_wind_ml","halo_reheat_ml","halo_wind_ml","halo_reheat_wind_ml","ism_reheat_tot_ml","ism_wind_tot_ml","halo_wind_tot_ml","ism_wind_tot_subnorm", "halo_wind_tot_subnorm"]

if "snap" in sim_name:
    f_name_list += ["ism_tot_eml","halo_tot_eml"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()



# highest-z bins are not well fit
z_bins = np.arange(0,5)
z_grid = z_grid[0:5]

N_f = np.zeros(len(z_bins))
n1_f = np.zeros(len(z_bins))
n2_f = np.zeros(len(z_bins))
M1_f = np.zeros(len(z_bins))
Mcut_f = np.zeros(len(z_bins))
n3_f = np.zeros(len(z_bins))
delta_f = np.zeros(len(z_bins))

for z_bin in z_bins:

    params_guess = (N, n1, n2, M1, Mcut, n3, delta)

    x = 10**bin_mh_mid
    logy = np.log10(f_med_dict["ism_wind_tot_ml"][z_bin])

    ok = (np.isnan(logy)==False) & (x>10**9.5)
    x = x[ok]; logy = logy[ok]
    
    out = so.curve_fit(ISM_Wind_Func, x, logy, params_guess)
    
    N_f[z_bin] = out[0][0]
    n1_f[z_bin] = out[0][1]
    n2_f[z_bin] = out[0][2]
    M1_f[z_bin] = out[0][3]
    Mcut_f[z_bin] = out[0][4]
    n3_f[z_bin] = out[0][5]
    delta_f[z_bin] = out[0][6]

    #print out[0]

    #logy_fit = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin],delta_f[z_bin])
    #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][z_bin]),c="b")
    #py.plot(bin_mh_mid,logy_fit,c="y")
    #py.ylim((-0.5,1.5))
    #py.show()


'''py.subplot(331)
py.plot(z_grid, np.log10(N_f))
py.plot(z_grid, -0.35 + 0.1*z_grid,c="y")
py.subplot(332)
py.plot(z_grid, n1_f)
py.subplot(333)
py.plot(z_grid, n2_f)
py.subplot(334)
py.plot(z_grid, np.log10(M1_f))
py.subplot(335)
py.plot(z_grid, np.log10(Mcut_f))
py.subplot(336)
py.plot(z_grid, n3_f)
py.subplot(337)
py.plot(z_grid, delta_f)
py.ylim((-2,0.0))

py.show()'''


N_f_g = 10**(-0.35 + 0.1 * z_grid)
n1_g = n1_f[0]
n2_g = n2_f[0]
M1_g = M1_f[0]
Mcut_g = Mcut_f[0]
n3_g = n3_f[0]
delta_g = delta_f[0]


'''for z_bin in z_bins:

    logy_fit_z = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin],delta_f[z_bin])
    logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g, n2_g, M1_g, Mcut_g,n3_g,delta_g)
    py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][z_bin]),c="b")
    py.plot(bin_mh_mid,logy_fit_z,c="c")
    py.plot(bin_mh_mid,logy_fit_evo,c="y")
    #py.ylim((-0.5,1.5))
    py.show()'''


for z_bin in z_bins:

    #logy_fit_z = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin],delta_f[z_bin])
    logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g, n2_g, M1_g, Mcut_g,n3_g,delta_g)
    #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][z_bin]),c="b")
    #py.plot(bin_mh_mid,logy_fit_z,c="c")
    py.plot(bin_mh_mid,logy_fit_evo)
    
py.show()
