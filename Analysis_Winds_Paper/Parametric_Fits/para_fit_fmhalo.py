import matplotlib.pyplot as py
import numpy as np
import scipy.optimize as so
import h5py

x = 10**np.arange(10,15,0.1)

def ISM_Wind_Func(x, N,n1,n2,M1,Mcut,n3):

    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2)) * np.exp(-x/Mcut))
    
    #y = N * ( (x/M1) **n1 + (x/M1)**n2)   
    #logy = np.log10(y)  * np.exp(-x/Mcut)
    
    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2) * np.exp(-x/Mcut)**2)

    #logy = np.log10( N * ( (x/M1) **n1 + (x/M1)**n2) * 0.1*(10-x/Mcut)**0.2)

    #acut = 0.3
    #logy = np.log10(y)  * (10-x/Mcut)**acut

    #logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2 * np.exp(-x/Mcut)))

    # Behroozi 13 inspired (The idea from him is similar to my own idea, which is fit three power laws and use an expontial decay function to kill the contribution of the 3rd power law in the low-mass range
    # The Behroozi part is the exponential damp term at the end - I'm not using a superlogarithm here  
    logy = np.log10(N * ( (x/M1) **n1 + (x/M1)**n2)) + n3 * np.log10( (x/M1))  * np.exp(-Mcut/x)

    # Hacky wat to apply hard priors to certain parameters
    if n3 > 0:
        logy *= 1e9

    if Mcut < M1:
        logy *= 1e9
    
    return logy

N = 1.0
n1 = -0.5
n2 = 0.5
M1 = 1e12
Mcut = 2e13
n3 = -1.0

# Fit ISM or halo mass loading
ism_wind = False

if not ism_wind:
    n2 = 2.0
    n1 = -1.0

if ism_wind:
    scnd_order = False
else:
    scnd_order = False

sim_name = "L0100N1504_REF_200_snip"
part_mass = "particle_mass"
#file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5)"
file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
#a_grid = file_grid["a_grid"][:]
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

print "temp hack, using median expansion factor for 100 Mpc, 200 snipshot"
a_grid = np.array([0.89337879, 0.67392123, 0.47913527, 0.34346414, 0.2583262, 0.19983937, 0.14593461, 0.0845002 ])

z_grid = 1./a_grid -1.0

fVmax_cut = 0.25
fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

f_name_list = ["ism_wind_cum_ml_pp_complete"+fV_str]
file_grid = h5py.File("efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]
file_grid.close()

if ism_wind:
    logy_grid = np.log10(f_med_dict["ism_wind_tot_ml"+fV_str])
else:
    logy_grid = np.log10(f_med_dict["halo_wind_tot_ml"+fV_str])

# highest-z bins are not well fit
if ism_wind:
    cut = 5
    z_bins = np.arange(0,cut)
    z_grid = z_grid[0:cut]
    a_grid = a_grid[0:cut]
else:
    cut = 5
    z_bins = np.arange(0,cut)
    z_grid = z_grid[0:cut]
    a_grid = a_grid[0:cut]

N_f = np.zeros(len(z_bins))
n1_f = np.zeros(len(z_bins))
n2_f = np.zeros(len(z_bins))
M1_f = np.zeros(len(z_bins))
Mcut_f = np.zeros(len(z_bins))
n3_f = np.zeros(len(z_bins))

for z_bin in z_bins:

    params_guess = (N, n1, n2, M1, Mcut, n3)

    x = 10**bin_mh_mid

    logy = logy_grid[z_bin]

    ok = (np.isnan(logy)==False) & (x>10**9.5) & (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][z_bin] == 1)
    x = x[ok]; logy = logy[ok]

    '''print x, logy, ISM_Wind_Func( x, N, n1, n2, M1, Mcut, n3)
    py.plot(np.log10(x),logy)
    py.plot(np.log10(x),ISM_Wind_Func( x, N, n1, n2, M1, Mcut, n3))
    py.show()
    quit()'''

    out = so.curve_fit(ISM_Wind_Func, x, logy, params_guess)
    
    N_f[z_bin] = out[0][0]
    n1_f[z_bin] = out[0][1]
    n2_f[z_bin] = out[0][2]
    M1_f[z_bin] = out[0][3]
    Mcut_f[z_bin] = out[0][4]
    n3_f[z_bin] = out[0][5]
    
    #print out[0]

    show_ind_fits = False
    show_all_fits = False

    if show_ind_fits:
        logy_fit = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
        py.plot(bin_mh_mid,logy_grid[z_bin],c="b")
        py.plot(bin_mh_mid,logy_fit,c="y")
        if ism_wind:
            py.ylim((-0.5,1.5))
        else:
            py.ylim((0.,2.5))
        py.show()

    elif show_all_fits:
        c_list = ["k","m","b","c","g","y","o"]
        logy_fit = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
        py.plot(bin_mh_mid,logy_grid[z_bin],c=c_list[z_bin])
        py.plot(bin_mh_mid,logy_fit,c=c_list[z_bin],linestyle='--')
        if ism_wind:
            py.ylim((-0.5,1.5))
        else:
            py.ylim((0.,2.5))

if show_all_fits:
    py.show()
    quit()

if show_ind_fits:
    quit()

def Poly_Fit2(a,p0,p1,p2):
    
    y = p0 + p1*a + p2*a**2
    
    return y

def Poly_Fit1(a,p0,p1):

    y = p0 + p1*a

    return y

if ism_wind:

    if scnd_order:
        # This one leads to an M1 that is too low at z=0
        p_guess = [0.2,0.,0.]
        out_N = so.curve_fit(Poly_Fit2, z_grid, np.log10(N_f), p_guess)
        p0_N, p1_N, p2_N = out_N[0]
        print "logN fit p0,p1,p2 = ", p0_N,p1_N,p2_N, "only valid for z>", z_grid[0]

        p_guess = [12.0,0.,0.]
        out_N = so.curve_fit(Poly_Fit2, a_grid, np.log10(M1_f), p_guess)
        p0_M1, p1_M1,p2_M1 = out_N[0]
        print "logM1 fit p0,p1,p2 = ", p0_M1,p1_M1,p2_M1, "only valid for z>", z_grid[0]


    else:
        p_guess = [0.2,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, np.log10(N_f), p_guess)
        p0_N, p1_N = out_N[0]
        print "logN fit p0,p1 = ", p0_N,p1_N, "only valid for z>", z_grid[0]

        # Edit - after excluding 1st bin it no longer needs to be fit
        ## M1 is not well fit by the 1st redshift bin for the current reduction
        #p_guess = [12.0,0.]
        #out_N = so.curve_fit(Poly_Fit1, z_grid[1:], np.log10(M1_f[1:]), p_guess)
        #p0_M1, p1_M1 = out_N[0]
        #print "logM1 fit p0,p1 = ", p0_M1,p1_M1, "only valid for z>", z_grid[0]

        # n1 is not well fit by the 1st redshift bin for the current reduction
        p_guess = [-0.3,0.0]
        out_N = so.curve_fit(Poly_Fit1, z_grid[1:], n1_f[1:], p_guess)
        p0_n1, p1_n1 = out_N[0]
        print "n1 fit p0,p1 = ", p0_n1,p1_n1, "only valid for z>", z_grid[0]

    z_check = np.arange(0,5,0.01)
    
else:
    if scnd_order:

        p_guess = [0.2,0.,0.]
        out_N = so.curve_fit(Poly_Fit2, a_grid, np.log10(N_f), p_guess)
        p0_N, p1_N, p2_N = out_N[0]
        print "logN fit p0,p1,p2 = ", p0_N,p1_N,p2_N, "only valid for z>", z_grid[0]

        p_guess = [0.5,0.,0.]
        out_N = so.curve_fit(Poly_Fit2, a_grid, n1_f, p_guess)
        p0_n1, p1_n1, p2_n1 = out_N[0]
        print "n1 fit p0,p1,p2 = ", p0_n1,p1_n1,p2_n1, "only valid for z>", z_grid[0]

        p_guess = [12.0,0.,0.]
        out_N = so.curve_fit(Poly_Fit2, a_grid, np.log10(M1_f), p_guess)
        p0_M1, p1_M1, p2_M1 = out_N[0]
        print "logM1 fit p0,p1,p2 = ", p0_M1,p1_M1,p2_M1, "only valid for z>", z_grid[0]

    else:

        p_guess = [0.2,0.]
        out_N = so.curve_fit(Poly_Fit1, a_grid, np.log10(N_f), p_guess)
        p0_N, p1_N = out_N[0]
        print "logN fit p0,p1 = ", p0_N,p1_N, "only valid for z>", z_grid[0]


        p_guess = [0.5,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, n1_f, p_guess)
        p0_n1, p1_n1 = out_N[0]
        print "n1 fit p0,p1 = ", p0_n1,p1_n1, "only valid for z>", z_grid[0]

        p_guess = [0.5,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, n2_f, p_guess)
        p0_n2, p1_n2 = out_N[0]
        print "n2 fit p0,p1 = ", p0_n2,p1_n2, "only valid for z>", z_grid[0]

        p_guess = [12.0,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, np.log10(M1_f), p_guess)
        p0_M1, p1_M1 = out_N[0]
        print "logM1 fit p0,p1 = ", p0_M1,p1_M1, "only valid for z>", z_grid[0]

        p_guess = [-1.0, 0.0]
        out_N = so.curve_fit(Poly_Fit1, z_grid, n3_f, p_guess)
        p0_n3, p1_n3 = out_N[0]
        print "n3 fit p0,p1 = ", p0_n3,p1_n3, "only valid for z>", z_grid[0]

        p_guess = [13.0,0.]
        out_N = so.curve_fit(Poly_Fit1, z_grid, np.log10(Mcut_f), p_guess)
        p0_Mcut, p1_Mcut = out_N[0]
        print "logMcut fit p0,p1 = ", p0_Mcut,p1_Mcut, "only valid for z>", z_grid[0]

    z_check = np.arange(0,7,0.01)

a_check = 1./(1+z_check)

show_param_evo = False

if show_param_evo:
    py.subplot(321)
    py.plot(z_grid, np.log10(N_f))
    if ism_wind:
        if scnd_order:
            py.plot(z_check, Poly_Fit2(z_check,p0_N,p1_N,p2_N),c="y")
        else:
            py.plot(z_check, Poly_Fit1(z_check,p0_N,p1_N),c="y")
    else:
        if scnd_order:
            py.plot(z_check, Poly_Fit2(a_check,p0_N,p1_N,p2_N),c="y")
        else:
            py.plot(z_check, Poly_Fit1(a_check,p0_N,p1_N),c="y")

    py.ylabel(r"$\log(N)$")

    ##################################

    py.subplot(322)
    py.plot(z_grid, n1_f)

    if ism_wind and not scnd_order:
        py.plot(z_check, Poly_Fit1(z_check, p0_n1,p1_n1),c="y")
    
    py.ylabel(r"$n_1$")

    if not ism_wind:
        if scnd_order:
            py.plot(z_check, Poly_Fit2(a_check,p0_n1,p1_n1,p2_n1),c="y")
        else:
           py.plot(z_check, Poly_Fit1(z_check,p0_n1,p1_n1),c="y")

    #####################################

    py.subplot(323)
    py.plot(z_grid, n2_f)
    py.ylabel(r"$n_2$")

    if not ism_wind:
        if not scnd_order:
            py.plot(z_check, Poly_Fit1(z_check, p0_n2,p1_n2),c="y")    

    #####################

    py.subplot(324)
    py.plot(z_grid, np.log10(M1_f))
    py.ylabel(r"$\log(M_1)$")

    if ism_wind:
        if scnd_order:
            py.plot(z_check, Poly_Fit2(a_check,p0_M1,p1_M1,p2_M1),c="y")
        #else:
        #    py.plot(z_check, Poly_Fit1(z_check,p0_M1,p1_M1),c="y")
    else:
        if scnd_order:
            py.plot(z_check, Poly_Fit2(a_check,p0_M1,p1_M1,p2_M1),c="y")
        else:
            py.plot(z_check, Poly_Fit1(z_check,p0_M1,p1_M1),c="y")

    ############
            
    py.subplot(325)
    py.plot(z_grid, np.log10(Mcut_f))
    py.ylabel(r"$\log(M_{cut}$")

    if not ism_wind:
        if not scnd_order:
            py.plot(z_check, Poly_Fit1(z_check, p0_Mcut,p1_Mcut),c="y")    

    ###############

    py.subplot(326)
    py.plot(z_grid, n3_f)
    py.ylabel(r"$n_3$")

    if not ism_wind:
        if not scnd_order:
            py.plot(z_check, Poly_Fit1(z_check, p0_n3,p1_n3),c="y")

    py.show()
    quit()

# For parameters which we won't evolve, choose which bin to take the best-fit value from
anchor_bin = 1

if ism_wind:
    if scnd_order:
        N_f_g = 10**(Poly_Fit2(z_grid, p0_N,p1_N,p2_N))
    else:
        N_f_g = 10**(Poly_Fit1(z_grid, p0_N,p1_N))

    if scnd_order:
        n1_g = n1_f[anchor_bin]

        print "n1", n1_g
    else:
        n1_g = Poly_Fit1(z_grid, p0_n1, p1_n1)

    if scnd_order:
        M1_g = 10**(Poly_Fit2(a_grid, p0_M1,p1_M1,p2_M1))
    else:
        #M1_g = 10**(Poly_Fit1(z_grid, p0_M1,p1_M1))
        M1_g = M1_f[anchor_bin]
        print "M1, log10(M1)", M1_g, np.log10(M1_g)
else:
    if scnd_order:
        N_f_g = 10**(Poly_Fit2(a_grid, p0_N,p1_N,p2_N))
        n1_g = Poly_Fit2(a_grid, p0_n1,p1_n1,p2_n1)
        M1_g = 10**(Poly_Fit2(a_grid, p0_M1,p1_M1,p2_M1))
    else:
        N_f_g = 10**(Poly_Fit1(a_grid, p0_N,p1_N))
        n1_g = Poly_Fit1(z_grid, p0_n1,p1_n1)
        n2_g = Poly_Fit1(z_grid, p0_n2,p1_n2)
        M1_g = 10**(Poly_Fit1(z_grid, p0_M1,p1_M1))
        Mcut_g = 10**(Poly_Fit1(z_grid, p0_Mcut,p1_Mcut))
        n3_g = Poly_Fit1(z_grid, p0_n3, p1_n3)

if not ism_wind and not scnd_order:
    pass
else:
    n2_g = n2_f[anchor_bin]
    n3_g = n3_f[anchor_bin]
    Mcut_g = Mcut_f[anchor_bin]
    print "n2, Mcut, n3, logMcut", n2_g, Mcut_g, n3_g, np.log10(Mcut_g)

show_z_fits = True
if show_z_fits:
    for z_bin in z_bins:

        logy_fit_z = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
        if ism_wind:
            if scnd_order:
                logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g, n2_g, M1_g[z_bin], Mcut_g,n3_g)
            else:
                logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g[z_bin], n2_g, M1_g, Mcut_g,n3_g)

        else:
            if scnd_order:
                logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g[z_bin], n2_g, M1_g[z_bin], Mcut_g,n3_g)
            else:
                logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g[z_bin], n2_g[z_bin], M1_g[z_bin], Mcut_g[z_bin],n3_g[z_bin])    

        py.plot(bin_mh_mid,logy_grid[z_bin],c="b")
        py.plot(bin_mh_mid,logy_fit_z,c="c")
        py.plot(bin_mh_mid,logy_fit_evo,c="y")
        py.ylim((-0.5,2.5))
        py.show()

print "Fit is valid for range", z_grid.min(), z_grid.max()

for z_bin in z_bins:

    #logy_fit_z = ISM_Wind_Func(10**bin_mh_mid, N_f[z_bin], n1_f[z_bin], n2_f[z_bin], M1_f[z_bin], Mcut_f[z_bin],n3_f[z_bin])
    if ism_wind:
        if scnd_order:
            logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g, n2_g, M1_g[z_bin], Mcut_g,n3_g)
        else:
            logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g[z_bin], n2_g, M1_g, Mcut_g,n3_g)

    else:
        if scnd_order:
            logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g[z_bin], n2_g, M1_g[z_bin], Mcut_g,n3_g)
        else:
            logy_fit_evo = ISM_Wind_Func(10**bin_mh_mid, N_f_g[z_bin], n1_g[z_bin], n2_g[z_bin], M1_g[z_bin], Mcut_g[z_bin],n3_g[z_bin])

    #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][z_bin]),c="b")
    #py.plot(bin_mh_mid,logy_fit_z,c="c")
    py.plot(bin_mh_mid,logy_fit_evo)
    
py.show()
