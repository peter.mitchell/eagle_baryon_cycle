import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
show_fakhouri_formula = False # Note Correa formula has the complication that you need to know the final mass of the halo - which is a bit awkward

input_path_base = "/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

sim_name_list = ["L0025N0376_REF_snap", "L0025N0376_REF_50_snip", "L0025N0376_REF_100_snip", "L0025N0376_REF_200_snip","L0025N0376_REF_300_snap","L0025N0376_REF_400_snap","L0025N0376_REF_500_snap_bound_only_version"]
snap_list = [28, 50, 100, 200, 300, 400,500]
disks = ["6","6","6","6","6","6","6"]
show = [0,1,2,3,6]

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


if show_fakhouri_formula:
    dmdt = lambda mhalo,z: 46.1 * (mhalo/1e12)**1.1 * (1+1.11*z) * np.sqrt(omm * (1+z)**3 + oml) # Msun yr^-1

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,4.98],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0


########### Load tabulated efficiencies #########################

i_sim = -1
for sim_name, snap, disk in zip(sim_name_list, snap_list, disks):
    i_sim += 1

    if i_sim not in show:
        continue

    input_path = "/cosma"+disk+input_path_base
    
    if test_mode:
        filename = input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5"
    else:
        filename = input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"

    print "reading", filename

    file_grid = h5py.File(filename)
 
    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    f_name_list = [  "sfr_subnorm", "ism_wind_tot_subnorm_0p25vmax", "ism_wind_tot_ml_0p25vmax"]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    ls_list = [':','--','-','-.',':','--','-']
    index_a_grid_show = [0,2,4,6]
    #index_a_grid_show = [2,3,4]

    for i,ax in enumerate(subplots):
        py.axes(ax)

        for n in index_a_grid_show:

            c_index = float(n)/(len(a_grid)-1)

            if n == 0:
                label = sim_name
            else:
                label = ""

            py.plot(bin_mh_mid,np.log10(f_med_dict["sfr_subnorm"][n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
            #py.scatter(bin_mh_mid,np.log10(f_med_dict["sfr_subnorm"][n]),c=c_list[n],edgecolors="none",s=5)

            #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm_0p25vmax"][n]),label=label,c=c_list[n],linestyle=ls_list[i_sim)]
            #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml_0p25vmax"][n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\langle \dot{M}_{\star} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -2.75; yhi = 0.0

        py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

        #py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


    print sim_name, 1./a_max-1, 1./a_min-1

if test_mode:
    fig_name = "sfr_convergence_test_mode.pdf"
else:
    fig_name = "sfr_convergence.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
