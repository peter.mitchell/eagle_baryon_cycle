This script has been essentially replaced by the outflow version





import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
show_fakhouri_formula = False # Note Correa formula has the complication that you need to know the final mass of the halo - which is a bit awkward

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = input_path

sim_name_list = ["L0025N0376_REF_snap", "L0025N0376_REF_50_snip", "L0025N0376_REF_100_snip", "L0025N0376_REF_200_snip","L0025N0376_REF_300_snap","L0025N0376_REF_400_snap","L0025N0376_REF_500_snap"]
sim_path_list = ["/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/","/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/", "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/", "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/","/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/"]
snap_list = [28, 50, 100, 200, 300, 400,500,1000]
tree_file_list = ["/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5","/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5", "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/tree_100.0.hdf5", "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/tree_299.0.hdf5", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/tree_399.0.hdf5", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/tree_499.0.hdf5", "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_999/tree_999.0.hdf5"]

lo = 0; hi = 6
sim_name_list = sim_name_list[lo:hi]
sim_path_list = sim_path_list[lo:hi]
snap_list = snap_list[lo:hi]
tree_file_list = tree_file_list[lo:hi]

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


if show_fakhouri_formula:
    dmdt = lambda mhalo,z: 46.1 * (mhalo/1e12)**1.1 * (1+1.11*z) * np.sqrt(omm * (1+z)**3 + oml) # Msun yr^-1

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64,2.49],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0


########### Load tabulated efficiencies #########################

i_sim = -1
for sim_name, sim_path, snap, tree_file in zip(sim_name_list, sim_path_list, snap_list, tree_file_list):
    i_sim += 1

    # Neistein uses dark matter subhalo mass as "halo mass"
    if test_mode:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version_test_mode.hdf5")
    else:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_winds_version.hdf5")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    f_name_list = ["sf_reheat_ml","sf_wind_ml","nsfism_reheat_ml","nsfism_wind_ml","ism_reheat_wind_ml","halo_reheat_ml","halo_wind_ml","halo_reheat_wind_ml","ism_reheat_tot_ml","ism_wind_tot_ml","halo_wind_tot_ml","ism_wind_tot_subnorm", "halo_wind_tot_subnorm"]

    if "snap" in sim_name:
        f_name_list += ["ism_tot_eml","halo_tot_eml"]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    ls_list = [':','--','-','-.',':','--','-']
    index_a_grid_show = [0,2,4,6]
    #index_a_grid_show = [5,6,7,8]

    for i,ax in enumerate(subplots):
        py.axes(ax)

        if i == 0:
            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"][n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
                #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][n]),label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=py.cm.tab10(c_index))
                py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"][n]),c=c_list[n],edgecolors="none",s=5)

            py.legend(loc='upper right',frameon=False,numpoints=1)
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            ylo = -2.75; yhi = 0.0

        if i == 1:
            for n in index_a_grid_show:

                if n == 0:
                    label = sim_name
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"][n]),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                py.scatter(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_subnorm"][n]),c=c_list[n],edgecolors="none",s=5)

            ylo = -2.25; yhi = 0.5
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{wind,halo}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            py.legend(loc='upper right',frameon=False,numpoints=1)


        if i == 2 or i == 3:
            py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


if test_mode:
    fig_name = "mass_loading_convergence_test_mode.pdf"
else:
    fig_name = "mass_loading_convergence.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
