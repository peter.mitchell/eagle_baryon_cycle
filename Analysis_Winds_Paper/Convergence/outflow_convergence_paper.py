import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

input_path_base = "/data/dp004/d72fqv/Baryon_Cycle/"

sim_name_list = ["L0025N0376_REF_snap", "L0025N0376_REF_50_snip", "L0025N0376_REF_100_snip", "L0025N0376_REF_200_snip","L0025N0376_REF_300_snap","L0025N0376_REF_400_snap","L0025N0376_REF_500_snap_bound_only_version","L0025N0376_REF_1000_snap"]
snap_list = [28, 50, 100, 200, 300, 400,500,1000]
disks = ["6","6","6","6","6","6","6","7"]

'''lo = 0; hi = 8
sim_name_list = sim_name_list[lo:hi]
sim_path_list = sim_path_list[lo:hi]
snap_list = snap_list[lo:hi]
tree_file_list = tree_file_list[lo:hi]'''

show = [0,1,2,3,6,7] # aagh list indexing
#show = [0,1,2,3,6]
sim_name_list2 = []
sim_path_list2 = []
snap_list2 = []
tree_file_list2 = []
disks2 = []
for i in range(len(snap_list)):
    if i in show:
        sim_name_list2.append(sim_name_list[i])
        snap_list2.append(snap_list[i])
        disks2.append(disks[i])

sim_name_list = sim_name_list2
snap_list = snap_list2
disks = disks2

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.2,'figure.subplot.hspace':0.2,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':8,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0


########### Load tabulated efficiencies #########################

for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    if i == 0:

        print len(sim_name_list), len(snap_list)

        i_sim = -1
        for sim_name, snap, disk in zip(sim_name_list, snap_list, disks):
            i_sim += 1

            print i_sim, sim_name

            input_path = "/cosma"+disk+input_path_base
            if test_mode:
                full_path = input_path+"Processed_Catalogues/efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5"
            else:
                full_path = input_path+"Processed_Catalogues/efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"

            print "Reading", full_path

            file_grid = h5py.File(full_path)

            bin_mh_mid = file_grid["log_msub_grid"][:]
            t_grid = file_grid["t_grid"][:]
            a_grid = file_grid["a_grid"][:]
            z_grid = 1./a_grid -1.0
            t_min = file_grid["t_min"][:]
            t_max = file_grid["t_max"][:]
            a_min = file_grid["a_min"][:]
            a_max = file_grid["a_max"][:]

            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
            f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str, "halo_wind_tot_subnorm"+fV_str]

            f_med_dict = {}
            for f_name in f_name_list:
                f_med_dict[f_name] = file_grid[f_name][:]

            file_grid.close()

            #ls_list = [':','--','-','-.',':','--','-']
            ls_list = ['-',':','--','-.',':','--','-']
            index_a_grid_show = [0,2,4]#,6]
            #index_a_grid_show = [5,6,7,8]

            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]),label=label,c=c_list[i_sim],linestyle=ls_list[n])
                #py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"][n]),label=(r"$%3.1f \leq z \leq %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=py.cm.tab10(c_index))
                py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_subnorm"+fV_str][n]),c=c_list[i_sim],edgecolors="none",s=5)

        py.ylabel(r"$\log_{10}(\langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle f_{\mathrm{B}} M_{200} \rangle \, / \mathrm{Gyr}^{-1})$")
        ylo = -3.25; yhi = -0.4
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=2.0)
        py.gca().add_artist(legend1)

        lines_j = []
        labels_j = []

        for i_sim2 in range(len(sim_name_list)):
            l_i, = py.plot([-20,-20],[-20,20],c=c_list[i_sim2])
            lines_j.append(l_i )
            labels_j.append( r"$N_{\mathrm{snap}} = %3i$" % snap_list[i_sim2] )
        py.legend(handles=lines_j,labels=labels_j,loc="upper right",frameon=False,numpoints=1,handlelength=1)


    if i == 1:
        sim_name = "L0025N0376_REF_200_snip"
        filename = "subhalo_catalogue_" + sim_name
        if test_mode:
            filename += "_test_mode"
        filename += ".hdf5"
        File = h5py.File(input_path+filename)

        tree_snapshot_info = File["tree_snapshot_info"]
        snapshot_numbers = tree_snapshot_info["snapshots_tree"][:][::-1]
        z_snapshots = tree_snapshot_info["redshifts_tree"][:][::-1]
        t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
        sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:][::-1]
        File.close()

        dt = t_snapshots[1:] - t_snapshots[0:-1]

        z_med = 0.5*(z_snapshots[0:-1] + z_snapshots[1:])
        t_med = 0.5*(t_snapshots[0:-1] + t_snapshots[1:])
        tdyn = t_med * 0.1
        
        d_snap_ns_list = np.zeros_like(t_snapshots)+np.nan
        for n in range(len(t_snapshots[:-1])):
            cosmic_t_ts = t_snapshots[n] # Cosmic time at current
            tdyn_ts = cosmic_t_ts * 0.1 # Approximate halo dynamical time in Gyr at this timestep
            snap_ts = snapshot_numbers[n] # Current tree snapshot
            
            ok = np.where(snapshot_numbers == snap_ts)[0][0]
            t_remain = t_snapshots[ok:]
            dt_temp = t_remain[1:] - t_remain[0]

            tdyn_frac_ns = 0.25
            d_snap_ns_list[n] = np.argmin( abs(dt_temp - tdyn_ts * tdyn_frac_ns)) +1

        py.plot(np.log10(1+z_med), np.log10(dt / t_med), c="c")



        '''# Also show the 1000 snap simulation
        sim_name = "L0025N0376_REF_1000_snap"
        filename = "subhalo_catalogue_" + sim_name
        if test_mode:
            filename += "_test_mode"
        filename += ".hdf5"
        File = h5py.File(input_path+filename)

        tree_snapshot_info = File["tree_snapshot_info"]
        snapshot_numbers = tree_snapshot_info["snapshots_tree"][:][::-1]
        z_snapshots = tree_snapshot_info["redshifts_tree"][:][::-1]
        t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
        sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:][::-1]
        File.close()

        dt = t_snapshots[1:] - t_snapshots[0:-1]

        z_med = 0.5*(z_snapshots[0:-1] + z_snapshots[1:])
        t_med = 0.5*(t_snapshots[0:-1] + t_snapshots[1:])
        tdyn = t_med * 0.1
        
        d_snap_ns_list = np.zeros_like(t_snapshots)+np.nan
        for n in range(len(t_snapshots[:-1])):
            cosmic_t_ts = t_snapshots[n] # Cosmic time at current
            tdyn_ts = cosmic_t_ts * 0.1 # Approximate halo dynamical time in Gyr at this timestep
            snap_ts = snapshot_numbers[n] # Current tree snapshot
            
            ok = np.where(snapshot_numbers == snap_ts)[0][0]
            t_remain = t_snapshots[ok:]
            dt_temp = t_remain[1:] - t_remain[0]

            tdyn_frac_ns = 0.25
            d_snap_ns_list[n] = np.argmin( abs(dt_temp - tdyn_ts * tdyn_frac_ns)) +1

        py.plot(np.log10(1+z_med), np.log10(dt / t_med), c="y")'''

        py.ylabel(r"$\log_{10}(\Delta t / t)$")
        ylo = -3.25; yhi = -0.5
        py.xlabel(r"$\log_{10}(1+z)$")

        #py.ylim((ylo, yhi))
        #py.xlim((xlo, xhi))

        
if test_mode:
    fig_name = "mass_loading_convergence_test_mode.pdf"
else:
    fig_name = "mass_loading_convergence.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
