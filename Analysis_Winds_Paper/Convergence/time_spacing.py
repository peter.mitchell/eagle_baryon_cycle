import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

input_path = "/gpfs/data/d72fqv/Baryon_Cycle/"

sim_name_list = ["L0025N0376_REF_snap", "L0025N0376_REF_50_snip", "L0025N0376_REF_100_snip", "L0025N0376_REF_200_snip","L0025N0376_REF_300_snap","L0025N0376_REF_400_snap"]#,"L0025N0376_REF_500_snap"]
snap_list = [28, 50, 100, 200, 300, 400]#,500]

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[2*3.32,2.49],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 2
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0


########### Load tabulated efficiencies #########################


for snap, sim_name in zip(snap_list, sim_name_list):

    filename = "subhalo_catalogue_" + sim_name
    if test_mode:
        filename += "_test_mode"
    filename += ".hdf5"
    File = h5py.File(input_path+filename)

    tree_snapshot_info = File["tree_snapshot_info"]
    snapshot_numbers = tree_snapshot_info["snapshots_tree"][:][::-1]
    z_snapshots = tree_snapshot_info["redshifts_tree"][:][::-1]
    t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
    sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:][::-1]
    File.close()

    dt = t_snapshots[1:] - t_snapshots[0:-1]
    
    z_med = 0.5*(z_snapshots[0:-1] + z_snapshots[1:])
    t_med = 0.5*(t_snapshots[0:-1] + t_snapshots[1:])
    tdyn = t_med * 0.1

    d_snap_ns_list = np.zeros_like(t_snapshots)+np.nan
    for n in range(len(t_snapshots[:-1])):
        cosmic_t_ts = t_snapshots[n] # Cosmic time at current
        tdyn_ts = cosmic_t_ts * 0.1 # Approximate halo dynamical time in Gyr at this timestep
        snap_ts = snapshot_numbers[n] # Current tree snapshot

        ok = np.where(snapshot_numbers == snap_ts)[0][0]
        t_remain = t_snapshots[ok:]
        dt_temp = t_remain[1:] - t_remain[0]

        tdyn_frac_ns = 0.25
        d_snap_ns_list[n] = np.argmin( abs(dt_temp - tdyn_ts * tdyn_frac_ns)) +1

        
    for i,ax in enumerate(subplots):
        py.axes(ax)

        if i == 0:
            py.plot(z_med, np.log10(dt / t_med), label=snap)
        else:
            py.plot(z_snapshots, d_snap_ns_list)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        py.axhline(np.log10(0.1), c="k",linestyle='--')
        py.axhline(np.log10(0.025), c="k",linestyle='--')

        py.legend(frameon=False)
        py.ylabel(r"$\log(\Delta t \, / t)$")

    else:
        py.ylabel(r"$n_{skip}$")

    py.xlim((0, 10))
    py.xlabel(r"$z$")

fig_name = "convergence_time_spacing.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Baryon_Cycle/'+fig_name)
py.show()
