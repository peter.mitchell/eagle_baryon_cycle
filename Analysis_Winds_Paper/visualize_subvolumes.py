import numpy as np
import h5py
import glob
import os
from utilities_plotting import *

# 100 Mpc REF with 50 snips
sim_name = "L0100N1504_REF_50_snip"
snap_final = 50
h=0.6777
box_size = 100 * h
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,
                98, 106, 114, 122, 130, 138, 146, 154, 164, 172, 180, 188,
                196, 204, 212, 220, 228, 236, 244, 252, 260, 268, 276, 284,
                292, 300, 308, 318, 326, 334, 342, 350, 358, 366, 374, 382,
                390, 398, 405][::-1]
snip_sample_rate = 2
nsub_1d = 4
test_mode = False

# 50 Mpc model with no AGN, 28 snapshots
'''sim_name = "L0050N0752_NOAGN_snap"
h=0.6777
box_size = 50 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
snip_sample_rate = 1
nsub_1d = 2'''

# 25 Mpc reference run, 28 snapshots
'''sim_name = "L0025N0376_REF_snap"
h=0.6777
box_size = 25 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
nsub_1d = 4
snip_sample_rate = 1
test_mode = True'''

snipshots = snipshots[::snip_sample_rate]

ind_choose = np.arange(0,nsub_1d**3,nsub_1d)
c_list = ["k","b","r","g","y","m"]

z_slice = 0
ind_choose += z_slice

input_path_base = "/gpfs/data/d72fqv/Baryon_Cycle/"+sim_name+"_subvols"
if test_mode:
    input_path_base += "_test_mode"
input_path_base += "/"

fig = py.figure()

nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
for i,ax in enumerate(subplots):
    py.axes(ax)
ax.set_aspect("equal")

ind_choose = []

for i_snipshot, snipshot in enumerate(snipshots):
    print "reading", snipshot

    input_path = input_path_base + "Snip_"+str(snipshot)+"/"

    file_path_base = "subhalo_catalogue_subvol_"+sim_name + "_snip_"+str(snipshot)+"_"

    subvols = glob.glob(input_path+file_path_base+'*')

    i_plot = 0
    i_color = 0

    for i_subvol, subvol in enumerate(subvols):

        if i_snipshot > 0:
            if i_subvol not in ind_choose:
                continue

        subvol_file = h5py.File(subvol)
        xyz_min_ts = subvol_file["select_region/xyz_min"][:]
        xyz_max_ts = subvol_file["select_region/xyz_max"][:]
        subvol_file.close()

        z_slice_max = 10.0
        z_slice_min = -10.0

        if i_snipshot == 0:
            if xyz_min_ts[2] < z_slice_max and xyz_max_ts[2] > z_slice_min:
                ind_choose.append(i_subvol)
            else:
                continue

        py.plot([xyz_min_ts[0],xyz_max_ts[0]], [xyz_min_ts[1],xyz_min_ts[1]], c=c_list[i_color])
        py.plot([xyz_min_ts[0],xyz_max_ts[0]], [xyz_max_ts[1],xyz_max_ts[1]], c=c_list[i_color])
        py.plot([xyz_min_ts[0],xyz_min_ts[0]], [xyz_min_ts[1],xyz_max_ts[1]], c=c_list[i_color])
        py.plot([xyz_max_ts[0],xyz_max_ts[0]], [xyz_min_ts[1],xyz_max_ts[1]], c=c_list[i_color])
    
        i_plot +=1

        i_color += 1
        if i_color == len(c_list):
            i_color = 0

    #break

for n in range(nsub_1d+1):
    
    py.axvline( box_size * n / nsub_1d,c="c",linewidth=1.2,linestyle='--')
    py.axhline( box_size * n / nsub_1d,c="c",linewidth=1.2,linestyle='--')

py.show()
