import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, part_data_fof,  sim_props, modes, output_names = inputs

    time_track_select = 0.0
    time_track_ptr = 0.0
    time_ptr_actual = 0.0

    # Unpack modes
    debug, ih_choose, optimise_sats, do_pristine = modes

    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_mass_dict = {}

    output_mass_names = output_names[0]
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ps, sim_props_ts, sim_props_ns, boxsize = sim_props

    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    expansion_factor_ps = sim_props_ps["expansion_factor"]
    expansion_factor_ns = sim_props_ns["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    redshift_ps = sim_props_ps["redshift"]
    redshift_ns = sim_props_ns["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    # Unpack particle data
    if do_pristine:
        dm_ps, dm_ts, dm_ns = part_data
    else:
        dm_ps, dm_ts, dm_ns = part_data
    dm_grn_sorted, dm_all_progen_sorted = part_data_fof

    # If this is an interpolated halo on _ts, it will not contain any particles and we should not do any further calculations
    # If this is an interpolated halo on _ps, most of the calculations will be wrong - so skip also
    # (will miss legitimate accretion that happens but capturing this would require linking to subhalo before it was interpolated)
    halo_isInterpolated = subhalo_props["isInterpolated"][i_halo]
    halo_isInterpolated_progen = subhalo_props["isInterpolated"][i_halo]
    if halo_isInterpolated == 1 or halo_isInterpolated_progen == 1:
        for name in output_mass_names:
            halo_mass_dict[name] = np.nan
        timing_info = [0.0, 0.0, 0.0, 0.0]
        return halo_mass_dict, timing_info

    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    # If optimize_sat==True, we skip most of the computations for satellite subhaloes, and only do the bare essentials
    if optimise_sats and halo_subgroup_number > 0:
        full_calculation = False
    else:
        full_calculation = True

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = subhalo_props["r200_host"][i_halo]
    halo_m200_host = subhalo_props["m200_host"][i_halo]
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    
    halo_x_progen = subhalo_props["x_halo_progen"][i_halo]
    halo_y_progen = subhalo_props["y_halo_progen"][i_halo]
    halo_z_progen = subhalo_props["z_halo_progen"][i_halo]
    halo_vx_progen = subhalo_props["vx_halo_progen"][i_halo]
    halo_vy_progen = subhalo_props["vy_halo_progen"][i_halo]
    halo_vz_progen = subhalo_props["vz_halo_progen"][i_halo]
    halo_vmax_progen = subhalo_props["vmax_progen"][i_halo]
    halo_r200_host_progen = subhalo_props["r200_host_progen"][i_halo]
    halo_group_number_progen = subhalo_props["group_number_progen"][i_halo]
    halo_subgroup_number_progen = subhalo_props["subgroup_number_progen"][i_halo]
    halo_subhalo_index_progen = subhalo_props["subhalo_index_progen"][i_halo]
    
    halo_x_ns = subhalo_props["x_halo_ns"][i_halo]
    halo_y_ns = subhalo_props["y_halo_ns"][i_halo]
    halo_z_ns = subhalo_props["z_halo_ns"][i_halo]
    halo_vx_ns = subhalo_props["vx_halo_ns"][i_halo]
    halo_vy_ns = subhalo_props["vy_halo_ns"][i_halo]
    halo_vz_ns = subhalo_props["vz_halo_ns"][i_halo]
    halo_vmax_ns = subhalo_props["vmax_ns"][i_halo]
    halo_r200_host_ns = subhalo_props["r200_host_ns"][i_halo]
    halo_group_number_ns = subhalo_props["group_number_ns"][i_halo]
    halo_subgroup_number_ns = subhalo_props["subgroup_number_ns"][i_halo]
    halo_subhalo_index_ns = subhalo_props["subhalo_index_ns"][i_halo]

    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ##### Select dm particles that belong to desired group, subgroup (note only used for some ISM defns)
    select_dm = mpf.Match_Index(halo_subhalo_index, dm_ts.subhalo_index_unique, dm_ts.particle_subhalo_index)

    id_dm = mpf.Index_Array(dm_ts.data["id"],select_dm,None)
    order_dm = np.argsort(id_dm)
    id_dm = id_dm[order_dm]
    if do_pristine:
        was_bound_dm = mpf.Index_Array(dm_ts.data["was_bound"],select_dm,order_dm)

    if full_calculation:

        select_fof_all_progen_dm = mpf.Match_Index(halo_group_number, dm_all_progen_sorted.grn_unique, dm_all_progen_sorted.grn_index)

        id_dm_all_progen = mpf.Index_Array(dm_all_progen_sorted.data["id"],select_fof_all_progen_dm, None)
        order_dm_all_progen = np.argsort(id_dm_all_progen)
        id_dm_all_progen = id_dm_all_progen[order_dm_all_progen]
        group_number_dm_all_progen = mpf.Index_Array(dm_all_progen_sorted.data["group_number"],select_fof_all_progen_dm,order_dm_all_progen)

        select_fof_as = mpf.Match_Index(halo_group_number, dm_grn_sorted.grn_unique, dm_grn_sorted.grn_index)
        id_dm_as = mpf.Index_Array(dm_grn_sorted.data["id"],select_fof_as,None)
        order_dm_as = np.argsort(id_dm_as)
        id_dm_as = id_dm_as[order_dm_as]

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ########################## Do the rate measurements #####################################

    g2Msun = 1./(1.989e33)

    ###### Identify dm accretion  #################
    if full_calculation:
        if len(id_dm_all_progen) > 0 and len(id_dm) > 0:

            ptr_accrete_dm = ms.match(id_dm, id_dm_all_progen, arr2_sorted=True)
            accreted_dm = ptr_accrete_dm < 0

            halo_mass_dict["mass_diffuse_dm_accretion"] = len(id_dm[accreted_dm]) * mass_dm * 1.989e43 * g2Msun / h

            if do_pristine:
                accreted_pristine = accreted_dm & (was_bound_dm == False)

                halo_mass_dict["mass_diffuse_dm_pristine"] = len(id_dm[accreted_pristine]) * mass_dm * 1.989e43 * g2Msun / h
                
        elif len(id_dm_all_progen) == 0:
            halo_mass_dict["mass_diffuse_dm_accretion"] = len(id_dm) * mass_dm * 1.989e43 * g2Msun / h

            if do_pristine:
                pristine = was_bound_dm == False
                halo_mass_dict["mass_diffuse_dm_pristine"] = len(id_dm[pristine]) * mass_dm * 1.989e43 * g2Msun / h

    # For satellites, we can compute first-time dm accretion using was bound variable
    elif do_pristine and len(id_dm) > 0:
        halo_mass_dict["mass_diffuse_dm_pristine"] = len(id_dm[was_bound_dm==False]) * mass_dm * 1.989e43 * g2Msun / h

    # For centrals, compute the dm accretion through mergers
    if halo_subgroup_number == 0:

        # Find particles that have merged with the main progenitor FoF group of this subhalo (that were bound to other FoF groups on the previous step)
        ok_mp = np.where(group_number_dm_all_progen != halo_group_number_progen)[0]

        # Note the all_progen list here has already had "smooth accretion" low-mass progenitors removed, and so do not count towards the merger rate.
        if len(id_dm_all_progen[ok_mp]) > 0 and len(id_dm_as) > 0:

            ptr = ms.match(id_dm_as, id_dm_all_progen[ok_mp],arr2_sorted=True)
            ok_match = ptr >= 0
            
            merged = ok_match

            halo_mass_dict["mass_hmerged_dm"] = len(id_dm_as[ merged ])*mass_dm* 1.989e43 * g2Msun / h

    if debug and i_halo == ih_choose:
        print ""
        print np.unique(group_number_dm_all_progen)
        print "log smooth/merger/m200:", np.log10(halo_mass_dict["mass_diffuse_dm_accretion"]), np.log10(halo_mass_dict["mass_hmerged_dm"]), np.log10(halo_m200_host)
        print ""

    time_track_ptr += time.time() - time_ptr

    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop, time_ptr_actual]

    return halo_mass_dict, timing_info
