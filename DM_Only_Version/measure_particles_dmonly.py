import time
t1 = time.time()
t0 = t1

# Where to read/write cat files
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

verbose = False
debug = False

subsample =False
n_per_bin = 1000

use_restart = True # Only strictly needed if we use do_pristine, but convenient in any case

do_pristine = True # If activated, this option tracks which particles have ever been in a halo - to determine first-infall DM accretion rates

if do_pristine:
    use_restart = True

bound_only_mode = True
retain_memory = True

# If true, find redefine the central subhalo as being all particles within r200_crit
# (that are not subfind bound to other satellites - and I think also not part of other FoF groups)
central_r200 = True

# Smooth acc thr (Msun) - mass threshold below which merging subhaloes are considered as smooth accretion for inflows
# This corresponds to 100 dm particles at standard eagle resolution
smooth_acc_thr = 969504631.0
print "Using smooth accretion threshold mass", smooth_acc_thr

# If true, cut all halos below this halo mass in the analysis (note gas/dm halo accretion rates still include haloes down to smooth acc thr)
# But these haloes will not be included for wind/ejecta type analysis - so this is also the minimum mass to be included in galactic transfer and recycling measurements
# This is desirable because this way we control the definition of those quantities, rather than it just being set implicitly by the simulation resolution.
cut_m200 = True
m200_cut = smooth_acc_thr # Msun
if cut_m200:
    print "Running with m200 cut of", m200_cut
else:
    print "Running without an m200 cut"

#m200_cut = 0.0
#print ""
#print "Trying with no subhalo mass cut"
#print ""

# Low-mass satellites are (when added together) very expensive inside the main loop if they have to cross-match against the (often much larger) FOF particle lists
# To mitigate this, the below option approximates the solution for wind/ejecta etc, and does not perform any measurements for the satellites themselves
optimise_sats = True
if optimise_sats:
    print "Running with satellite optimisations"

ns_tdyn = True # Set to true to use a fixed fraction of the halo dynamical time to use for _ns
# tdyn_frac_ns is a parameter that controls the fraction of a halo dynamical time used for finding _ns for wind slection
tdyn_frac_ns = 0.25

# Maximum subvolume size (at a given snapshot) before splitting the subvolume into 8 smaller subvolumes when performing IO
# and doing a first round of particle selection - This is only used if we are using Peano-Hilbert IO
volmax = (0.6777 * 25 * 1e5)**3 # Mpc h^-1

import sys
import argparse
sys.path.append("../.")
import measure_particles_functions as mpf

parser = argparse.ArgumentParser()
parser.add_argument("-subvol", help = "subvolume",type=int)
parser.add_argument("-nsub1d", help = "number of subvolumes",type=int)
parser.add_argument("-datapath", help = "directory containting simulation data")
parser.add_argument("-simname", help = "sim/tree name")
parser.add_argument("-snap", help = "final snapshot to measure for",type=int)
parser.add_argument("-test_mode", type=mpf.str2bool)
parser.add_argument("-n_jobs", help = "number of parallel joblib subprocesses",type=int)

if len(sys.argv)!=15:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()
 
subvol = args.subvol
nsub1d = args.nsub1d
DATDIR = args.datapath
sim_name = args.simname
snap_end = args.snap
test_mode = args.test_mode
n_jobs_parallel=args.n_jobs # Number of sub-processes to spawn for executing the joblib loops

# If there is one subvolume, it's always better to do custom IO if possible
if nsub1d == 1:
    volmax = 1e5**3

if test_mode:
    print ""
    print "Running in test mode"
    print ""
    output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_test_mode/"

    skip_start_hack = False
    if skip_start_hack:
        print ""
        print "Warning: hacking test mode to skip starting snapshots"
        print ""

    use_restart = False

else:
    output_path = catalogue_path + sim_name+"_subvols_nsub1d_"+str(nsub1d)+"/"
    if subsample:
        output_path = catalogue_path+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
    skip_start_hack = False

import read_eagle as re
import numpy as np
import warnings
np.seterr(all='ignore')
import os
import h5py
from joblib import Parallel, delayed
from os import listdir
import match_searchsorted as ms
import utilities_cosmology as uc
import measure_particles_io as mpio
import measure_particles_classes as mpc
import main_loop_dmonly as main

# First let's work out the relationship between tree snapshots and simulation snapshots/snipshots
cat_path = catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5"

n_tries = 0
success = True
# This can fail if two jobs try to read the catalogue at the same time - get round this by waiting 
while n_tries < 50:
    try:
        cat_file = h5py.File(cat_path,"r")
        tree_snapshot_info = cat_file["tree_snapshot_info"]
        snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
        redshifts = tree_snapshot_info["redshifts_tree"][:]
        sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
        cosmic_time = tree_snapshot_info["cosmic_times_tree"][:]
        cat_file.close()
        success = True
        break
    except:
        print "Failed to read base catalogue, trying again in 10 seconds", cat_path
        n_tries += 1
        time.sleep(10)

if not success:
    print "Error: failed to read base catalogue, stopping here"
    quit()

# Bad convention to refer to simulation snipshots/snapshots as "snipshots" from here on in
# And to refer to tree outputs as "snapshots" from here on in
ok = snapshot_numbers <= snap_end
snap_list = snapshot_numbers[ok][::-1]
snip_list = sn_i_a_pshots_simulation[ok][::-1]
cosmic_t_list = cosmic_time[ok][::-1]
redshifts = redshifts[ok][::-1]

def UnConcatenate(arrays,nparts):
    
    list_out = []
    nparts_cumul = np.cumsum(nparts)
    for i_sub in range(len(nparts)):
        if i_sub==0:
            ind_prev = 0
        else:
            ind_prev = nparts_cumul[i_sub-1]
        list_out.append(arrays[ind_prev:nparts_cumul[i_sub]])

    return list_out

retain_ts = False
retain_ns = False

####### Look for a restart file and use it if use_restart set to True #########
restart_found = False
if use_restart:
    for snip in snip_list[:-1]:
        output_path_ts = output_path + "Snip_"+str(snip)+"/"
        restart_path = output_path_ts + "restart_file_"+sim_name+"_snip_"+str(snip)+"_"+str(subvol)+"_dmonly.hdf5"

        if os.path.isfile(restart_path):
            print "Found restart file at snip", snip,"restarting at this point"

            restart_file = h5py.File(restart_path,"r")

            if do_pristine:
                id_was_bound = restart_file["id_was_bound"][:]

            ok_snip = np.where(snip_list >= snip)[0]

            snip_list_tot = np.copy(snip_list)

            snip_list = snip_list[ok_snip]
            snap_list = snap_list[ok_snip]
            cosmic_t_list = cosmic_t_list[ok_snip]
            redshifts = redshifts[ok_snip]

            restart_file.close()
            restart_found = True
            break

print "measuring rates for snipshots/snapshots", snip_list[0:-1]

t2 = time.time()
print "finished preliminaries, time elapsed", t2-t1

if do_pristine and not restart_found:
    # List of IDs of particles that have been in a halo (above the m200 cuts) before
    id_was_bound = np.array([]).astype("int")

for i_snip, snip_ts in enumerate(snip_list[:-1]):

    #if snip_ts < 318:
    #    print "TEMP HACK"
    #    continue

    # For certain applications, we want to know where we are in the snapshot loop without being affected by the restart resets
    try:
        i_snip_glob = np.argmin(abs(snip_ts - snip_list_tot))
    except:
        i_snip_glob = i_snip

    # No _ps on first snapshots/snipshot
    # For restart, this skip also serves us to move us to the correct starting point
    if i_snip == 0:
        continue
    
    # Special case for broken first snapshots in 50 Mpc ref run
    if sim_name == "L0050N0752_REF_snap" and i_snip <= 2:
        print "skipping first output because broken"
        continue

    snip_ps = snip_list[i_snip-1]

    t1 = time.time()
    
    print ""
    print "measuring for snip (simulation), snap (tree), time (Gyr), redshift = ", snip_ts, snap_list[i_snip], cosmic_t_list[i_snip], redshifts[i_snip]

    # This is supposed to help with getting python to actually communicate something to the log file
    sys.stdout.flush()

    ######## Work out which snapshot to read as the _ns for wind selection ###############
    cosmic_t_ts = cosmic_t_list[i_snip] # Cosmic time at current 
    tdyn_ts = cosmic_t_ts * 0.1 # Approximate halo dynamical time in Gyr at this timestep
    snap_ts = snap_list[i_snip] # Current tree snapshot

    ok = np.where(snap_list == snap_ts)[0][0]
    t_remain = cosmic_t_list[ok:]
    dt = t_remain[1:] - t_remain[0]

    # tdyn_frac_ns is a parameter set at the top
    d_snap_ns = np.argmin( abs(dt - tdyn_ts * tdyn_frac_ns)) +1 # Number of snapshots into the future that should be read for _ns

    if not ns_tdyn:
        d_snap_ns = 1

    if d_snap_ns > 1:
        print "_ns skip is", d_snap_ns

    snip_ns = snip_list[i_snip + d_snap_ns]
    snap_ns = snap_ts + d_snap_ns

    # Read subvol file to load halo catalogue and selection boundaries
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    if not os.path.exists(output_path_ts):
        print "Couldn't find directory",output_path_ts,"moving on"
        continue

    print "Attempting to read", output_path_ts+filename_ts
    File = h5py.File(output_path_ts+filename_ts,"a")

    # Read selection boundary
    try:
        xyz_min_ts = File["select_region/xyz_min"][:]
        xyz_max_ts = File["select_region/xyz_max"][:]
    except:
        print output_path_ts+filename_ts, "doesn't exist, moving to next snipshot"
        File.close()
        continue

    # Quit if there are no subhaloes in this subvolume (this assumes that future subvols will also not have subhaloes)
    if np.isnan(xyz_min_ts[0]):
        print "No haloes inside this subvolume, moving to next snipshot"
        File.close()
        continue
    try:
        # Read in selection boundary for next snipshot
        output_path_ns = output_path + "Snip_"+str(snip_ns)+"/"
        filename_ns = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ns)+"_"+str(subvol)+".hdf5"
        File_ns = h5py.File(output_path_ns+filename_ns,"r")
        
        xyz_max_ns = File_ns["select_region/xyz_max"][:]
        xyz_min_ns = File_ns["select_region/xyz_min"][:]
    except:
        if not i_snip+1 == len(snip_list): # There is no next snipshot once we reach z=0
            print "Error: failed to read selection boundary for the next snipshot, quitting"
            exit()

    # Want to read in a big enough volume to accomodate both the subhalo & progenitor samples for this snipshot and the subhalo/progenitor samples for the next snipshot
    boxsize = mpio.Get_Box_Size(sim_name, DATDIR,snip_ts)
 
    for i_dim in range(3):
        if xyz_max_ts[i_dim] > xyz_max_ns[i_dim] + 0.5*boxsize:
            xyz_max_ns[i_dim]+= boxsize; xyz_min_ns[i_dim] += boxsize
        if xyz_max_ns[i_dim] > xyz_max_ts[i_dim] + 0.5*boxsize:
            xyz_max_ts[i_dim] += boxsize; xyz_min_ts[i_dim] += boxsize

    if retain_memory and d_snap_ns == 1:
        # In this case, we can carry over the particle data from _ns > _ts > _ps
        for i_dim in range(3):
            xyz_max_ns[i_dim] = max(xyz_max_ns[i_dim], xyz_max_ts[i_dim])
            xyz_min_ns[i_dim] = min(xyz_min_ns[i_dim], xyz_min_ts[i_dim])

    if nsub1d >1 or test_mode:
        print "for _ts, reading particles from volume with vertices: x=", xyz_min_ts[0],xyz_max_ts[0], " y=", xyz_min_ts[1], xyz_max_ts[1], " z=",xyz_min_ts[2],xyz_max_ts[2] 
        print "for _ns, reading particles from volume with vertices: x=", xyz_min_ns[0],xyz_max_ns[0], " y=", xyz_min_ns[1], xyz_max_ns[1], " z=",xyz_min_ns[2],xyz_max_ns[2] 

        problem_z = (xyz_max_ns[2] > boxsize) & (xyz_min_ns[2] < 0)
        problem_x = (xyz_max_ns[0] > boxsize) & (xyz_min_ns[0] < 0)
        problem_y = (xyz_max_ns[1] > boxsize) & (xyz_min_ns[1] < 0)
        if (problem_z | problem_x | problem_y) & (nsub1d > 1):
            print "Error: Something has gone wrong with the IO region size calculation"
            quit()

    # Read halo catalogue
    subhalo_group = File["subhalo_data"]
    
    subhalo_props = {}

    try:
        mchalo_list = subhalo_group["mchalo"][:]
    except:
        print "Error: first run copy_mchalo.py, then rerun select_subvolumes.py"
        quit()

    order_sub = np.argsort(mchalo_list)[::-1]
    mchalo_list = mchalo_list[order_sub]

    subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host", "isInterpolated"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]

    for name in subhalo_prop_names:
        subhalo_props[name] = subhalo_group[name][:][order_sub]

    # Select halos above the mass cut
    if cut_m200:
        ok = (subhalo_props["m200_host"] > m200_cut) & (subhalo_props["subgroup_number"] == 0)
        
        n_for_limit = min(100,len(subhalo_props["m200_host"]))
        mchalo_lim = np.median(np.sort(subhalo_props["mchalo"][ok])[0:n_for_limit])
        if test_mode:
            mchalo_lim = m200_cut # Not enough subhaloes in test mode to reliably evaluate the offset between m200 and mchalo (subfind mass) for centrals
            
        n_sub_before_mask = len(mchalo_list)
        ok = ok | ((subhalo_props["subgroup_number"]>0) & (subhalo_props["mchalo"] > mchalo_lim)) | ((subhalo_props["isInterpolated"]==1)&(subhalo_props["mchalo"] > mchalo_lim))
        
        # Keep track of non-selected satellite haloes, so we can control the smooth/merger halo accretion later
        removed_mask = np.where((ok==False) & (subhalo_props["subgroup_number"]>0) & (subhalo_props["m200_host"] > m200_cut))[0]
        removed_props_ts = {}
        for name in subhalo_prop_names:
            removed_props_ts[name] = subhalo_props[name][removed_mask]

        subhalo_mask = np.where(ok)[0]

        for name in subhalo_prop_names:
            subhalo_props[name] = subhalo_props[name][subhalo_mask]
        mchalo_list = mchalo_list[subhalo_mask]
        
        if len(mchalo_list)==0:
            print "After applying subhalo mass cut, there are no remaining subhalos, moving on"
            File.close()
            File_ns.close()
            continue
    else:
        removed_props_ts = {}
        for name in subhalo_prop_names:
            removed_props_ts[name] = np.zeros(0)

    if debug:
        ih_choose = np.argmax(mchalo_list)
    else:
        ih_choose = 0 # Dummy value

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ts = max(len(str(int(subhalo_props["subgroup_number"].max()))) +1 ,6)
    subhalo_props["subhalo_index"] = subhalo_props["group_number"].astype("int64") * 10**nsep_ts + subhalo_props["subgroup_number"].astype("int64")

    # Find satellites that have no central (to be used later)
    ptr_no_central = ms.match(subhalo_props["group_number"], subhalo_props["group_number"][subhalo_props["subgroup_number"]==0])
    no_central = (subhalo_props["subgroup_number"] > 0) & (ptr_no_central < 0)

    progenitor_group = File["progenitor_data"]
    
    subhalo_prop_names = ["group_number","subgroup_number","x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host","isInterpolated"]

    for name in subhalo_prop_names:
        name_progen = name+"_progen"
        subhalo_props[name_progen] = progenitor_group[name][:][order_sub]
        
    if cut_m200:
        for name in subhalo_prop_names:
            subhalo_props[name+"_progen"] = subhalo_props[name+"_progen"][subhalo_mask]

    all_progenitor_group = File["all_progenitor_data"]

    subhalo_prop_names =  ["group_number","subgroup_number","descendant_index","m200_host","mtot","isInterpolated"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","r200_host"]

    for name in subhalo_prop_names:
        name_all_progen = name+"_all_progen"
        subhalo_props[name_all_progen] = all_progenitor_group[name][:]
        
    if cut_m200:
        ptr = ms.match(subhalo_props["descendant_index_all_progen"], np.append(subhalo_props["node_index"],removed_props_ts["node_index"]))
        ok_match = ptr >= 0
        for name in subhalo_prop_names:
            subhalo_props[name+"_all_progen"] = subhalo_props[name+"_all_progen"][ok_match]

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ps = max(len(str(int(np.append(subhalo_props["subgroup_number_progen"],subhalo_props["subgroup_number_all_progen"]).max()))) +1 ,6)
    subhalo_props["subhalo_index_progen"] = subhalo_props["group_number_progen"].astype("int64") * 10**nsep_ps + subhalo_props["subgroup_number_progen"].astype("int64")
    subhalo_props["subhalo_index_all_progen"] = subhalo_props["group_number_all_progen"]* 10**nsep_ps + subhalo_props["subgroup_number_all_progen"]

    # Error checking
    ptr = ms.match(subhalo_props["group_number_progen"], subhalo_props["group_number_all_progen"])
    if len(ptr[ptr<0])>0:
        print "Error, halos in the progen list are not present in the all progen list"
        print np.unique(subhalo_props["group_number_progen"])
        print np.unique(subhalo_props["group_number_all_progen"])
        quit()

    # Read halo properties for _ns output (in order to identify the positions of the descendants of haloes from this output) - if _ns is not the next output in the tree we need to walk down the tree to find the correct descendant
    n_tree_walk = snap_ns - snap_ts
    for i_walk in range(n_tree_walk):

        snip_i_walk = snip_list[i_snip+i_walk+1]

        print "Reading", output_path + "Snip_"+str(snip_i_walk)+"/"

        filename_i_walk = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_i_walk)+"_"+str(subvol)+".hdf5"
        output_path_i_walk = output_path + "Snip_"+str(snip_i_walk)+"/"
        File_ns = h5py.File(output_path_i_walk+filename_i_walk,"r")

        subhalo_group_ns = File_ns["subhalo_data"]
        node_index_list_ns = subhalo_group_ns["node_index"][:]

        if i_walk == 0:
            ptr = ms.match(subhalo_props["descendant_index"], node_index_list_ns)
        else:
            ptr = ms.match(subhalo_props["descendant_index_ns"], node_index_list_ns) # Note descendant left over from prev iteration through the loop                            
        ok_match = ptr>=0

        subhalo_props["descendant_index_ns"] = np.zeros_like(mchalo_list).astype("int")-1e7 # Note it's important this is an ints array - not float                              
        subhalo_props["descendant_index_ns"][ok_match] = subhalo_group_ns["descendant_index"][:][ptr][ok_match]

        if i_walk != n_tree_walk-1:
            File_ns.close()

    if len(subhalo_props["descendant_index"][ok_match]) != len(subhalo_props["descendant_index"]):
        print "Warning: " + str(len(subhalo_props["descendant_index"][ok_match==False])) +" of "+str(len(subhalo_props["descendant_index"]))+" haloes could not find a descendant on the next simulation output"

    subhalo_prop_names = ["group_number","subgroup_number"]
    subhalo_prop_types =  ["int",         "int",          ]

    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host","isInterpolated"]
    subhalo_prop_types +=  ["float", "float", "float", "float", "float", "float", "float", "float","int"]

    for name, prop_type in zip(subhalo_prop_names,subhalo_prop_types):
        name_ns = name+"_ns"
        if prop_type == "int":
            subhalo_props[name_ns] = np.zeros_like(mchalo_list).astype("int")-1000000
        elif prop_type == "float":
            subhalo_props[name_ns] = np.zeros_like(mchalo_list) + np.nan
        else:
            "Error: prop type",prop_type,"not understood"
            quit()
        subhalo_props[name_ns][ok_match] = subhalo_group_ns[name][:][ptr][ok_match]

    File_ns.close()

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    if len(mchalo_list[ok_match])>0:
        nsep_ns = max(len(str(int(subhalo_props["subgroup_number_ns"][ok_match].max()))) +1 ,6)
    else:
        nsep_ns = 6
    subhalo_props["subhalo_index_ns"] = subhalo_props["group_number_ns"].astype("int64") * 10**nsep_ns + subhalo_props["subgroup_number_ns"].astype("int64")

    ############### Initialize output dicts ###########################

    output_mass_names = ["mass_diffuse_dm_accretion","mass_hmerged_dm"]
    if do_pristine:
        output_mass_names += ["mass_diffuse_dm_pristine"]
    output_mass_dict = {}

    for name in output_mass_names:
        output_mass_dict[name] = np.zeros(len(mchalo_list))
    
    ############## Read Eagle particle data #######################
    t2 = time.time()

    # 4/4/18 Structure here is that we read in the data for previous snipshot _ps, current snipshot _ts, next snipshot _ns - this triple read is done for every snipshot
    # Note this means that we triple the IO cost - the reason for doing this is that it allows us to discard unwanted data to save memory as we go through
    # If the code becomes IO limited at some point in the future - and memory isn't a big problem then you should change this approach!

    gas_names_in_ps = []
    gas_names_out_ps = []

    all_gas_names_in_ps = []; all_gas_names_out_ps = []

    dm_names_in_ps = []; dm_names_out_ps = []
    dm_names_in_ps += ["Coordinates"]
    dm_names_out_ps += ["coordinates"]

    star_names_in_ps = []
    star_names_out_ps = []

    bh_names_in_ps = []
    bh_names_out_ps = []

    data_names_in_ps = [gas_names_in_ps, dm_names_in_ps, star_names_in_ps, bh_names_in_ps, all_gas_names_in_ps,[],[],[]]
    data_names_out_ps = [gas_names_out_ps, dm_names_out_ps, star_names_out_ps, bh_names_out_ps, all_gas_names_out_ps,[],[],[]]

    ####### _ps #############
    print "Reading particle data for _ps (previous snipshot)"

    # Decide how many sub-subvolumes we want for doing IO (and some intial particle selection)
    # The answer is ideally 1, but if memory is limited we can reduce memory cost by splitting the IO into smaller steps, discarding un-needed particles as we go (this is much less important for bound_only_mode)
    vol_ps = np.product(xyz_max_ts-xyz_min_ts) # Mpc h^-1

    if vol_ps > volmax:
        nsubvol_pts = 8
        
        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ts
        
        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ts[0]-xyz_min_ts[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ts[1]-xyz_min_ts[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ts[2]-xyz_min_ts[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1
        
        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ts[0]-xyz_min_ts[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ts[1]-xyz_min_ts[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ts[2]-xyz_min_ts[2])*0.5
    else:
        nsubvol_pts = 1

    if retain_ts: # Do we carry over particle data from previous step?
        print "Using previous _ts data on _ps"
        nsubvol_pts = 1  # In this case we are not doing any _ps IO on this step, so the below loop is redundant
        gas_ps_in = gas_ts; dm_ps_in = dm_ts; star_ps_in = star_ts; bh_ps_in = bh_ts; sim_props_ps = sim_props_ts
        all_gas_ps_in = gas_ps_in

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_temp = time.time()

        t_core_io_i = time.time()

        # Read in particle data for previous snapshot
        if not retain_ts:
            if nsub1d > 1 or test_mode or not bound_only_mode:
                # Use JCH Hash-Table IO (for efficiently reading in sub-regions of the box)
                if bound_only_mode:
                    read_all = ""
                    gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, sim_props_ps = mpio.Read_All_Particle_Data(snip_ps, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ps,data_names_out_ps,read_all=read_all,verbose=verbose)
                    all_gas_ps_in = gas_ps_in

                else:
                    read_all = "g"
                    gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, all_gas_ps_in, sim_props_ps = mpio.Read_All_Particle_Data(snip_ps, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ps,data_names_out_ps,read_all=read_all,verbose=verbose)

            else:

                if not bound_only_mode:
                    print "Custom IO not implemented for non-bound particles"
                    quit()
                # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
                data_names_in_ps = gas_names_in_ps+ dm_names_in_ps+ star_names_in_ps+ bh_names_in_ps
                data_names_out_ps = gas_names_out_ps+ dm_names_out_ps+ star_names_out_ps+ bh_names_out_ps
                part_types = np.concatenate(( np.zeros(len(gas_names_in_ps)), np.ones(len(dm_names_in_ps)), np.zeros(len(star_names_in_ps))+4, np.zeros(len(bh_names_in_ps))+5 )).astype("int")

                gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, sim_props_ps = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ps, sim_name, data_names_in_ps,data_names_out_ps,part_types,njobs=n_jobs_parallel,verbose=verbose)
                all_gas_ps_in = gas_ps_in

        t_core_io = time.time() - t_core_io_i

        # Create arrays of halo properties for _ps
        # For _ps, we need to be careful to select particles that are only in subhalo progenitors (and not complete FoF groups which tend to include massive halos)
        # Create a unique id for each subhalo out of group_number and subgroup_number (this is because we don't have node_index for particles)                    
        nsep = max(len(str(int(subhalo_props["subgroup_number_all_progen"].max()))) +1 ,6)

        cat_list_all_progen = subhalo_props["group_number_all_progen"].astype("int64") * 10**nsep + subhalo_props["subgroup_number_all_progen"].astype("int64")
        cat_list_progen = subhalo_props["group_number_progen"].astype("int64") * 10**nsep + subhalo_props["subgroup_number_progen"].astype("int64")

        cat_ps_master_list = np.concatenate((cat_list_progen, cat_list_all_progen)) # (Note to self - isn't this redundant - I thought the code crashes if there are progen halos not in the all_progen list?) 
        cat_ps_master_list, ind_unique = np.unique(cat_ps_master_list,return_index=True)

        t_rebind_i = time.time()
        if central_r200 and not retain_ts:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            # Note if retain_ts is true, we already did the required rebinding on an earlier step
            if verbose:
                print "rebinding _ps"

            halo_coordinates_ps = [np.append(subhalo_props["x_halo_progen"],subhalo_props["x_halo_all_progen"])[ind_unique],\
                                       np.append(subhalo_props["y_halo_progen"],subhalo_props["y_halo_all_progen"])[ind_unique],\
                                       np.append(subhalo_props["z_halo_progen"],subhalo_props["z_halo_all_progen"])[ind_unique]]
            grn_temp = np.append(subhalo_props["group_number_progen"], subhalo_props["group_number_all_progen"])[ind_unique]
            sgrn_temp = np.append(subhalo_props["subgroup_number_progen"], subhalo_props["subgroup_number_all_progen"])[ind_unique]
            r200_temp = np.append(subhalo_props["r200_host_progen"], subhalo_props["r200_host_all_progen"])[ind_unique]
            iI_temp = np.append(subhalo_props["isInterpolated_progen"], subhalo_props["isInterpolated_all_progen"])[ind_unique]

            # Note - this horrible block is outside a function (and repeated many times) because of the JobLib pecularities (pickle-related)
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_ps, r200_temp, boxsize)

            # Dark matter rebinding
            if len(dm_names_in_ps) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ps_in)

        t_rebind = time.time() - t_rebind_i
        
        cat_dm_list_ps = dm_ps_in.data["group_number"].astype("int64") * 10**nsep + dm_ps_in.data["subgroup_number"].astype("int64")
        if len(cat_ps_master_list) > 0 and len(cat_dm_list_ps) > 0:
            ptr = ms.match(cat_dm_list_ps, cat_ps_master_list,arr2_sorted=True)
            ok_match = ptr >= 0
            dm_ps_in.select_bool(ok_match)

        # Init datasets
        if i_subvol == 0:
            gas_ps = mpc.DataSet(); dm_ps = mpc.DataSet(); star_ps = mpc.DataSet(); bh_ps = mpc.DataSet()

        # Merge datasets
        if nsubvol_pts > 1:
            gas_ps.merge_dataset(gas_ps_in)
            star_ps.merge_dataset(star_ps_in)
            bh_ps.merge_dataset(bh_ps_in)
            if not skip_dark_matter:
                dm_ps.merge_dataset(dm_ps_in)

            # Put the input datasets out of scope
            gas_ps_in = None; dm_ps_in = None; star_ps_in = None; bh_ps_in = None
        else:
            gas_ps = gas_ps_in; dm_ps = dm_ps_in; star_ps = star_ps_in; bh_ps = bh_ps_in
    
        # Put the input datasets out of scope
        cat_gas_list_ps = None; cat_dm_list_ps = None; cat_star_list_ps = None; cat_bh_list_ps = None


    ########## _ts ##############
    print "Reading particle data for _ts (current snipshot)"

    gas_names_in_ts = []
    gas_names_out_ts = []

    all_gas_names_in_ts = []
    all_gas_names_out_ts = []

    dm_names_in_ts = ["Coordinates"]
    dm_names_out_ts = ["coordinates"]

    star_names_in_ts = []
    star_names_out_ts = []

    bh_names_in_ts = []
    bh_names_out_ts = []

    data_names_in_ts = [gas_names_in_ts, dm_names_in_ts, star_names_in_ts, bh_names_in_ts, all_gas_names_in_ts,[],[],[]]
    data_names_out_ts = [gas_names_out_ts, dm_names_out_ts, star_names_out_ts, bh_names_out_ts, all_gas_names_out_ts,[],[],[]]

    if retain_ns: # Do we carry over particle data from previous step?
        print "Using previous _ns data on _ts"
        nsubvol_pts = 1  # In this case we are not doing any _ts IO on this step, so the below loop is redundant
        gas_ts_in = gas_ns; dm_ts_in = dm_ns; star_ts_in = star_ns; bh_ts_in = bh_ns; sim_props_ts = sim_props_ns
        all_gas_ts_in = gas_ts_in

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_core_io_i = time.time()
            
        if not retain_ns:
            if nsub1d > 1 or test_mode or not bound_only_mode:
                if bound_only_mode:
                    read_all = ""
                    gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)
                    all_gas_ts_in = gas_ts_in
                else:
                    read_all = "g"
                    gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, all_gas_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)

            else:
                # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
                data_names_in_ts = gas_names_in_ts+ dm_names_in_ts+ star_names_in_ts+ bh_names_in_ts
                data_names_out_ts = gas_names_out_ts+ dm_names_out_ts+ star_names_out_ts+ bh_names_out_ts
                part_types = np.concatenate(( np.zeros(len(gas_names_in_ts)), np.ones(len(dm_names_in_ts)), np.zeros(len(star_names_in_ts))+4, np.zeros(len(bh_names_in_ts))+5 )).astype("int")
                
                gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ts, sim_name, data_names_in_ts,data_names_out_ts,part_types,njobs=n_jobs_parallel,verbose=verbose)
                all_gas_ts_in = gas_ts_in

        t_core_io += time.time() - t_core_io_i

        t_rebind_i = time.time()
        if central_r200 and not retain_ns:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            if verbose:
                print "rebinding _ts"

            # In this case we need to rebind for haloes from _ts on this step, and _ps on the next step
            if retain_memory and i_snip+1 < len(snip_list[0:-1]):
                
                snip_next_step = snip_list[i_snip+1]
                filename_next_step = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_next_step)+"_"+str(subvol)+".hdf5"
                output_path_next_step = output_path + "Snip_"+str(snip_next_step)+"/"
                print "For _ts rebinding, reading", output_path_next_step+filename_next_step
                File_next_step = h5py.File(output_path_next_step+filename_next_step,"r")
                
                all_progenitor_group = File_next_step["all_progenitor_data"]

                x_next_step = all_progenitor_group["x_halo"][:]
                y_next_step = all_progenitor_group["y_halo"][:]
                z_next_step = all_progenitor_group["z_halo"][:]
                grn_next_step = all_progenitor_group["group_number"][:]
                sgrn_next_step = all_progenitor_group["subgroup_number"][:]
                r200_next_step = all_progenitor_group["r200_host"][:]
                iI_next_step = all_progenitor_group["isInterpolated"][:]

                File_next_step.close()

                nsep = max(len(str(int(sgrn_next_step.max()))) +1 ,6)

                cat_list_next_step = grn_next_step.astype("int64") * 10**nsep + sgrn_next_step.astype("int64")
                cat_list = subhalo_props["group_number"].astype("int64") * 10**nsep + subhalo_props["subgroup_number"].astype("int64")

                cat_master_list = np.concatenate((cat_list_next_step,cat_list))
                cat_master_list, ind_unique = np.unique(cat_master_list,return_index=True)

                halo_coordinates_temp = [np.append(x_next_step,subhalo_props["x_halo"])[ind_unique],\
                                       np.append(y_next_step,subhalo_props["y_halo"])[ind_unique],\
                                       np.append(z_next_step,subhalo_props["z_halo"])[ind_unique]]
                grn_temp = np.append(grn_next_step, subhalo_props["group_number"])[ind_unique]
                sgrn_temp = np.append(sgrn_next_step, subhalo_props["subgroup_number"])[ind_unique]
                r200_temp = np.append(r200_next_step, subhalo_props["r200_host"])[ind_unique]
                iI_temp = np.append(iI_next_step, subhalo_props["isInterpolated"])[ind_unique]
            else:
                # Otherwise we are rebinding for _ts haloes on this step only
                halo_coordinates_temp = [subhalo_props["x_halo"], subhalo_props["y_halo"], subhalo_props["z_halo"]]
                grn_temp = subhalo_props["group_number"]; sgrn_temp = subhalo_props["subgroup_number"]; r200_temp = subhalo_props["r200_host"]; iI_temp = subhalo_props["isInterpolated"]

            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_temp, r200_temp, boxsize)

            if len(dm_names_in_ps) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ts_in)

        else:
            grn_temp = subhalo_props["group_number"] # Used to decide which particles to keep at the end of this sub-loop


        t_rebind += time.time() - t_rebind_i

        # Remove bound particles from other particle types
        if not retain_memory:
            star_ts_in.select_bool(star_ts_in.is_bound())
            bh_ts_in.select_bool(bh_ts_in.is_bound())
            if len(dm_names_in_ts) > 0:
                dm_ts_in.select_bool(dm_ts_in.is_bound())
        
        # Keep only particles bound to subhalos from this subvolume (as opposed to bound to any halo in the simulation)
        if not retain_memory: # As before, we will want the other particles for halo_reheat on the next step if we pass the data on
            gas_ts_in.ptr_prune(grn_temp,"group_number")
            star_ts_in.ptr_prune(grn_temp,"group_number")
            bh_ts_in.ptr_prune(grn_temp,"group_number")
            if len(dm_names_in_ts) > 0:
                dm_ts_in.ptr_prune(grn_temp,"group_number")


        if nsubvol_pts > 1:

            # Init datasets
            if i_subvol == 0:
                gas_ts = mpc.DataSet(); dm_ts = mpc.DataSet(); star_ts = mpc.DataSet(); bh_ts = mpc.DataSet()

            # Merge datasets
            gas_ts.merge_dataset(gas_ts_in)
            star_ts.merge_dataset(star_ts_in)
            bh_ts.merge_dataset(bh_ts_in)
            if not skip_dark_matter:
                dm_ts.merge_dataset(dm_ts_in)

            # Put the input datasets out of scope
            gas_ts_in = None; dm_ts_in = None; star_ts_in = None; bh_ts_in = None
        else:
            gas_ts = gas_ts_in; dm_ts = dm_ts_in; star_ts = star_ts_in; bh_ts = bh_ts_in



    ####### _ns ###########
    print "reading particle data for _ns (next snipshot)"

    gas_names_in_ns = []
    gas_names_out_ns = []

    all_gas_names_in_ns = []; all_gas_names_out_ns = []

    dm_names_in_ns = []
    dm_names_out_ns = []

    star_names_in_ns = []
    star_names_out_ns = []
    
    bh_names_in_ns = []
    bh_names_out_ns = []

    if d_snap_ns == 1 and retain_memory: # If we are going to carry this data over to later snapshots, we need to ensure we read all variables that will be needed later
# Note that I'm assuming here that any _ps variables will also be in _ts
        for name_in, name_out in zip(dm_names_in_ts,dm_names_out_ts):
            if name_in not in dm_names_in_ns:
                dm_names_in_ns.append(name_in); dm_names_out_ns.append(name_out)
        
    data_names_in_ns = [gas_names_in_ns, dm_names_in_ns, star_names_in_ns, bh_names_in_ns, all_gas_names_in_ns,[],[],[]]
    data_names_out_ns = [gas_names_out_ns, dm_names_out_ns, star_names_out_ns, bh_names_out_ns, all_gas_names_out_ns,[],[],[]]

    if vol_ps > volmax:
        nsubvol_ns = 8
        
        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ns
        
        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ns[0]-xyz_min_ns[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ns[1]-xyz_min_ns[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ns[2]-xyz_min_ns[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1
        
        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ns[0]-xyz_min_ns[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ns[1]-xyz_min_ns[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ns[2]-xyz_min_ns[2])*0.5
    else:
        nsubvol_ns = 1

    for i_subvol in range(nsubvol_ns):
        
        if nsubvol_ns > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_ns
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ns
            xyz_subvol_max_i = xyz_max_ns

        t_core_io_i = time.time()

        if nsub1d > 1 or test_mode or not bound_only_mode:
            if bound_only_mode:
                read_all = ""
                gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, sim_props_ns = mpio.Read_All_Particle_Data(snip_ns, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ns,data_names_out_ns, read_all=read_all,verbose=verbose)
                all_gas_ns_in = gas_ns_in
            else:
                read_all = "g"
                gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, all_gas_ns_in, sim_props_ns = mpio.Read_All_Particle_Data(snip_ns, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ns,data_names_out_ns, read_all=read_all,verbose=verbose)

        else:
            # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
            data_names_in_ns = gas_names_in_ns+ dm_names_in_ns+ star_names_in_ns+ bh_names_in_ns
            data_names_out_ns = gas_names_out_ns+ dm_names_out_ns+ star_names_out_ns+ bh_names_out_ns
            part_types = np.concatenate(( np.zeros(len(gas_names_in_ns)), np.ones(len(dm_names_in_ns)), np.zeros(len(star_names_in_ns))+4, np.zeros(len(bh_names_in_ns))+5 )).astype("int")

            gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, sim_props_ns = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ns, sim_name, data_names_in_ns,data_names_out_ns,part_types,njobs=n_jobs_parallel,verbose=verbose)
            all_gas_ns_in = gas_ns_in

        t_core_io += time.time() - t_core_io_i
        
        t_rebind_i = time.time()
        if central_r200:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo

            # For _ns, we could in principle have duplicate entries in the subhalo lists, so need to make a unique list here (just for re-binding)
            nsep = max(len(str(int(subhalo_props["subgroup_number_ns"][np.isnan(subhalo_props["subgroup_number_ns"])==False].max()))) +1 ,6)
            sub_ind_ns = subhalo_props["group_number_ns"]* 10**nsep + subhalo_props["subgroup_number_ns"]
            junk, ind_unique = np.unique(sub_ind_ns, return_index=True)
            grn_temp = subhalo_props["group_number_ns"][ind_unique]
            sgrn_temp = subhalo_props["subgroup_number_ns"][ind_unique]
            r200_temp = subhalo_props["r200_host_ns"][ind_unique]
            iI_temp = subhalo_props["isInterpolated_ns"][ind_unique]

            halo_coordinates_temp = [subhalo_props["x_halo_ns"][ind_unique],subhalo_props["y_halo_ns"][ind_unique],subhalo_props["z_halo_ns"][ind_unique]]

            # If we are carrying this data over to future steps, we need to ensure rebinding is performed for all relevant halos (this is going to be a bit of a painful block of code)
            if d_snap_ns == 1 and retain_memory:
                
                for i_temp in range(2):
                    i_next_step = i_snip + i_temp + 1

                    if i_next_step < len(snip_list[0:-1]):

                        snip_next_step = snip_list[i_next_step]
                        filename_next_step = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_next_step)+"_"+str(subvol)+".hdf5"
                        output_path_next_step = output_path + "Snip_"+str(snip_next_step)+"/"
                        print "For _ns rebinding, reading", output_path_next_step+filename_next_step
                        File_next_step = h5py.File(output_path_next_step+filename_next_step,"r")

                        if i_temp == 0:
                            subhalo_group_temp = File_next_step["subhalo_data"]
                        else:
                            subhalo_group_temp = File_next_step["all_progenitor_data"]

                        x_next_step = subhalo_group_temp["x_halo"][:]
                        y_next_step = subhalo_group_temp["y_halo"][:]
                        z_next_step = subhalo_group_temp["z_halo"][:]
                        grn_next_step = subhalo_group_temp["group_number"][:]
                        sgrn_next_step = subhalo_group_temp["subgroup_number"][:]
                        r200_next_step = subhalo_group_temp["r200_host"][:]
                        iI_next_step = subhalo_group_temp["isInterpolated"][:]

                        File_next_step.close()

                        nsep = max(len(str(int(sgrn_next_step.max()))) +1 ,6)

                        cat_list_next_step = grn_next_step.astype("int64") * 10**nsep + sgrn_next_step.astype("int64")
                        cat_list = grn_temp.astype("int64") * 10**nsep + sgrn_temp.astype("int64")

                        cat_master_list = np.concatenate((cat_list_next_step,cat_list))
                        cat_master_list, ind_unique = np.unique(cat_master_list,return_index=True)

                        halo_coordinates_temp = [np.append(x_next_step,halo_coordinates_temp[0])[ind_unique],\
                                                 np.append(y_next_step,halo_coordinates_temp[1])[ind_unique],\
                                                 np.append(z_next_step,halo_coordinates_temp[2])[ind_unique]]
                        grn_temp = np.append(grn_next_step, grn_temp)[ind_unique]
                        sgrn_temp = np.append(sgrn_next_step, sgrn_temp)[ind_unique]
                        r200_temp = np.append(r200_next_step, r200_temp)[ind_unique]
                        iI_temp = np.append(iI_next_step, iI_temp)[ind_unique]

            if verbose:
                print "rebinding _ns"

            if len(dm_names_in_ns) > 0:
                # dark matter rebinding
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ns_in)

        t_rebind += time.time() - t_rebind_i

        if len(dm_names_in_ns) > 0:
            if d_snap_ns != 1 or retain_memory == False:
                dm_ns_in.select_bool(dm_ns_in.is_bound())
                dm_ns_in.ptr_prune(grn_temp,"group_number")

        # Init datasets
        if i_subvol == 0:
            gas_ns = mpc.DataSet(); dm_ns = mpc.DataSet(); star_ns = mpc.DataSet(); bh_ns = mpc.DataSet()

        if nsubvol_ns > 1:

            # Merge datasets
            gas_ns.merge_dataset(gas_ns_in)
            if ism_definition == "Mitchell18_adjusted":
                dm_ns.merge_dataset(dm_ns_in)
                bh_ns.merge_dataset(bh_ns_in)
                star_ns.merge_dataset(star_ns_in)

            # Put the input datasets out of scope
            gas_ns_in = None; dm_ns_in = None; star_ns_in = None; bh_ns_in = None
        else:
            gas_ns = gas_ns_in; dm_ns = dm_ns_in; star_ns = star_ns_in; bh_ns = bh_ns_in

        bound_parts_ns = None; not_bound_parts_ns = None

    print "finished io, time elapsed", time.time() - t2, "time elapsed for core IO", t_core_io, "time for rebinding", t_rebind

    t4 = time.time()

    ####### Unpack and get simulation characteristics for this point of the snipshot loop ########
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    expansion_factor_ps = sim_props_ps["expansion_factor"]
    expansion_factor_ns = sim_props_ns["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    redshift_ps = sim_props_ps["redshift"]
    redshift_ns = sim_props_ns["redshift"]
    mass_dm = sim_props_ts["mass_dm"]

    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    # Work out 10 time bins from now to the first snapshot, logarithmically spaced in lookback time
    # These are used to compute the binned ejected/return time distributions for wind/ejecta particles
    t_ls = uc.t_Universe(1.0, omm, h)[0]
    tlb_fs = t_ls - cosmic_t_list[0]
    tlb_ts = t_ls - cosmic_t_list[i_snip]


    ##################### Create arrays for each FoF group (this is an optimization step to avoid repeating this step for satellites within the main loop) ##############
    print "Computing FoF lists"
    time_fof = time.time()
    
    # Create tools to efficiently index the particle arrays within the main loop

    ####### Start with the list of particles bound to each _ts FoF group #######
    dm_grn_sorted = mpc.DataSet()

    dm_grn_sorted.add_data( dm_ts.data["group_number"] , "group_number")
    dm_grn_sorted.add_data( dm_ts.data["subgroup_number"] , "subgroup_number")
    dm_grn_sorted.add_data( dm_ts.data["id"] , "id")

    dm_grn_sorted.sort("group_number")

    dm_grn_sorted.grn_unique, dm_grn_sorted.grn_index = np.unique(dm_grn_sorted.data["group_number"], return_index=True)
    dm_grn_sorted.grn_index = np.append(dm_grn_sorted.grn_index,len(dm_grn_sorted.data["group_number"]))

    ##################### Sort the main particle arrays and create indexes for each subhalo (for efficient indexing within the main loop) ##############
    
    print "Sorting particle lists and creating indexes"
    time_sort = time.time()

    dm_ps_subhalo_index = dm_ps.set_index(nsep_ps,return_subhalo_index=True)
    dm_ts.set_index(nsep_ts)

    print "Sorting/index creation completed, took", time.time() - time_sort, "seconds"


    ###### Now do the list of particles bound to all progenitors of each _ts FoF group #########
    ptr = ms.match(subhalo_props["descendant_index_all_progen"], np.append(subhalo_props["node_index"],removed_props_ts["node_index"]))
    ok_match = ptr>=0
    if len(ok_match[ok_match]) < len(ok_match):
        print "Error: There are _ps halos all the all progenitor list who do not have a descendant in the _ts halos list"
        print subhalo_props["descendant_index_all_progen"]
        print subhalo_props["node_index"]
        print removed_props_ts["node_index"]
        quit()

    ind_temp = subhalo_props["group_number_all_progen"]*1e6 + subhalo_props["subgroup_number_all_progen"]
    ind_temp2 = subhalo_props["group_number_progen"]*1e6 + subhalo_props["subgroup_number_progen"]
    ptr_temp2 = ms.match(ind_temp, ind_temp2)
    main_progens = ptr_temp2>=0

    # This is where we define the distinction between smooth accretion and merger accretion of gas/dm
    # Note this is going to exclude central progenitors below the mass limit, but not low-mass satellites of progenitors of this halo (inc the main progenitor)
    smooth_check = (subhalo_props["m200_host_all_progen"] > smooth_acc_thr) | main_progens

    dm_all_progen_sorted = mpc.DataSet()
    # Create the sorted (and so indexable) all progen list for dark matter (to compute smooth dm accretion rate)
    ptr2 = ms.match(dm_ps_subhalo_index, subhalo_props["subhalo_index_all_progen"])

    ok_match2 = ptr2>=0
    if len(ok_match2[ok_match2]) < len(ok_match2) and retain_ts ==False:
        print "Error: there are particles in dm_ps that are not bound on _ts to a subhalo in the halos list"
        quit()

    dm_all_progen_sorted.add_data(np.append(subhalo_props["group_number"],removed_props_ts["group_number"])[ptr][ptr2][ok_match2],"group_number_descendant")

    dm_all_progen_sorted.add_data(np.append(subhalo_props["mchalo"],removed_props_ts["mchalo"])[ptr][ptr2][ok_match2],"mchalo_descendant")
    dm_all_progen_sorted.add_data(dm_ps.data["id"][ok_match2], "id")
    dm_all_progen_sorted.add_data(dm_ps.data["group_number"][ok_match2], "group_number")

    dm_all_progen_sorted.select_bool(smooth_check[ptr2][ok_match2])

    dm_all_progen_sorted.sort("group_number_descendant")
    dm_all_progen_sorted.grn_unique, dm_all_progen_sorted.grn_index = np.unique(dm_all_progen_sorted.data["group_number_descendant"], return_index=True)
    dm_all_progen_sorted.grn_index = np.append(dm_all_progen_sorted.grn_index,len(dm_all_progen_sorted.data["id"]))

    print "FoF lists computed, took", time.time() - time_fof, "seconds"

    ########### Flag which particles on _ts are in the master list of was_bound ###########
    # Doing this inside the main loop is very slow! - would be good to port this into the main version if possible later
    if do_pristine:
        # We want to find particles that have already been counted (in the main loop) as having been accreted
        if len(id_was_bound) > 0:
            ptr = ms.match(dm_ts.data["id"], id_was_bound)
            ok_match = ptr >= 0

            dm_ts.add_data(ok_match, "was_bound")
        else:
            dm_ts.add_data(np.zeros_like(dm_ts.data["id"]) < 0, "was_bound")

    # Pack particle data to pass into function
    part_data = [dm_ps, dm_ts, dm_ns]
    part_data_fof = [dm_grn_sorted, dm_all_progen_sorted]

    # Pack simulation info
    sim_props = [sim_props_ps, sim_props_ts, sim_props_ns, boxsize]

    # Pack boolean options
    modes = [debug, ih_choose, optimise_sats, do_pristine]

    # Pack up output variable names information
    output_names = [output_mass_names]

    #################### Main loop ####################################################
    time_track_select = 0
    time_track_ptr = 0
    time_ptr_actual = 0

    time_pack = time.time()

    print "time spent packing the data", time.time()-time_pack
    time_main = time.time()

    def Dummy_function(i_halo):

        inputs = [ i_halo, i_snip_glob, subhalo_props, part_data, part_data_fof, sim_props, modes, output_names]

        return main.Subhalo_Loop(inputs)

    output = Parallel(n_jobs=n_jobs_parallel)(delayed(Dummy_function)(i_halo) for i_halo in range(len(subhalo_props["group_number"])))

    print "time spent on main loop", time.time() -time_main
    time_unpack = time.time()

    # Unpack the data
    time_track_subloop = 0.0
    for i_halo in range(len(subhalo_props["group_number"])):

        halo_mass_dict, timing_info = output[i_halo]
        
        time_track_select += timing_info[0]
        time_track_ptr += timing_info[1]
        time_track_subloop += timing_info[2]
        time_ptr_actual += timing_info[3]

        for name in output_mass_names:
            output_mass_dict[name][i_halo] += halo_mass_dict[name]

    print "time spent unpacking the data", time.time() - time_unpack

    ########### Update the master list of particles that were counted as accreted for this step ###########
    if do_pristine:
        # We want to find particles that have alread been counted (in the main loop) as having been accreted
        ptr_check1 = ms.match(dm_ts.data["group_number"], subhalo_props["group_number"]) # Find parts bound to selected subhalo list on _ts
        ok_check1 = ptr_check1 >= 0

        ## Find parts that will be counted as accretion events
        id_was_bound = np.unique(np.concatenate((id_was_bound,dm_ts.data["id"][ok_check1]))) # Note this is already sorted

    ih_choose = np.argmax(mchalo_list)

    ############### Write restarts to disk if requested ############################

    if use_restart:
        restart_path = output_path_ts + "restart_file_"+sim_name+"_snip_"+str(snip_ts)+"_"+str(subvol)+"_dmonly.hdf5"

        # Check if the restart file already exists, and delete if that is the case
        if os.path.isfile(restart_path):
            os.remove(restart_path)
            print "Overwriting existing restart file at", restart_path
        else:
            print "Writing restart file to", restart_path

        restart_file = h5py.File(restart_path)

        if do_pristine:
            restart_file.create_dataset("id_was_bound", data=id_was_bound)

        restart_file.close()

        # Delete previous restart file after writing this one (or delete restart at the end)
        if snip_ts == snip_list[:-1][-1]:
            snips_prev = snip_list[snip_list <= snip_ts][::-1]
        else:
            snips_prev = snip_list[snip_list < snip_ts][::-1]

        for snip_prev in snips_prev:
            output_path_prev = output_path + "Snip_"+str(snip_prev)+"/"
            restart_path_prev = output_path_prev + "restart_file_"+sim_name+"_snip_"+str(snip_prev)+"_"+str(subvol)+"_dmonly.hdf5"

            if os.path.isfile(restart_path_prev):
                print "removing previous restart at", restart_path_prev
                os.remove(restart_path_prev)


    ################### Write output data to disk #############################################

    output_group_str = "Measurements"
    if output_group_str not in File:
        output_group = File.create_group(output_group_str)
    else:
        output_group = File[output_group_str]

    for dset in output_mass_names:
        for thing in output_group:
            if thing == dset:
                del output_group[dset]

    for name in output_mass_names:
        output_group.create_dataset(name, data=output_mass_dict[name])

    File.close()

    print "computing / write time", time.time()-t4
    print "select,matching block,ptr (actual), subloop time", time_track_select, time_track_ptr, time_ptr_actual, time_track_subloop
    print "total time for this snapshot", time.time() - t1

    # If _ns will be _ts on the next step, we have the option to retain the particle data
    if d_snap_ns == 1 and retain_memory:
        retain_ns = True
    else:
        retain_ns = False

    if retain_memory:
        retain_ts = True

print "Total time for the program in minutes", (time.time() - t0)/60.0
