# This routine loads the tree/subhalo files and computes diffuse dark matter accretion rates
# I separate this from the main integrator routine because in general I expect you want to run this just once for a given simulation but the integrator several times

import numpy as np
from os import listdir
import os
import h5py
import load_trees as lt
import utilities_cosmology as uc
import match_searchsorted as ms

#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/"
#nsnap = 50
#sim_name = "L0100N1504_REF_50_snip"

#sub_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/"
#tree_path = sub_path
#nsnap = 200
#sim_name = "L0100N1504_REF_200_snip"

#tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/"
#sub_path = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/subfind/"
#nsnap = 28
#sim_name = "L0050N0752_REF_snap"

#tree_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"
#sub_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"
#nsnap = 28
#sim_name = "L0025N0376_REF_snap"

#sub_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/"
#tree_path = sub_path
#nsnap = 50
#sim_name = "L0025N0376_REF_50_snip"

#sub_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/"
#tree_path = sub_path
#nsnap = 100
#sim_name = "L0025N0376_REF_100_snip"

#sim_name = "L0025N0376_REF_200_snip"
#sub_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/"
#tree_path = sub_path
#nsnap =200

#sim_name = "L0025N0376_REF_300_snap"
#sub_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/"
#tree_path = sub_path
#nsnap =299

#sim_name = "L0025N0376_REF_400_snap"
#sub_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/"
#tree_path = sub_path
#nsnap =399

#sim_name = "L0025N0376_REF_500_snap"
#sub_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/"
#tree_path = sub_path
#nsnap =499

sim_name = "L0025N0376_REF_1000_snap"
sub_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/"
tree_path = sub_path
nsnap =999

#sim_name = "L0050N0752_NOAGN_snap"
#sub_path = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/subfind/"
#tree_path = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/"
#nsnap = 28

#sim_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0050N0752/EagleVariation_fE1p00_ALPHA1p0e3/trees/treedir_028/"
#nsnap = 28
#sim_name = "L0050N0752_CFB_snap"

# 25 Mpc - recal (high-res), 202 snipshots
#sim_name = "L0025N0752_Recal"
#sub_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/"
#tree_path = sub_path
#nsnap = 202

g2Msun = 1./(1.989e33)
h=0.6777
fb = 0.0482519 / 0.307
fdm = 1-fb

output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
test_mode = False # This is a relic of Neistein version I think - do not use it!

def Sum_Common_ID(quantities_list1, id_list1, id_list2_in, verbose=False):

    if verbose:
        print "Sum_Common_ID(): Building bins for histogram and mapping back onto unbinned array"
    
    # sort id list 2 and save the order
    order = np.argsort(id_list2_in)
    inverse = np.argsort(order)
    id_list2 = np.sort(id_list2_in)

    id_bin2 = np.repeat(id_list2,2).astype('float64')
    id_bin2[1::2] += 0.5
    id_bin2[0:-1:2] += -0.5
    id_bin2 = np.unique(id_bin2)
    
    id_bin2_mid = 0.5*(id_bin2[0:-1]+id_bin2[1:])
    ok = ms.match(id_bin2_mid, id_list2) >= 0

    if verbose:
        print "Sum_Common_ID(): Done, now computing histograms for each quantity"
    quantities_list2 = []

    for i_quant, quantity1 in enumerate(quantities_list1):

        quantity2 = np.histogram( id_list1, bins=id_bin2, weights=quantity1 )[0][ok]
        if verbose:
            print "Sum_Common_ID(): Histogram computed for quantity", i_quant,"of",len(quantities_list1)

        quantities_list2.append( quantity2[inverse] )

    return quantities_list2

def Compute_Diffuse_Accretion(msub_progen, msub_descend, node_index, descendant_index, group_number, subgroup_number, is_main_progenitor):
    # Assumes descendant lists (msub_descend, group_number, node_index, subgroup_number) are sorted by group number
    # Assumes each progenitor has a descendant

    # Group numbers of descendant fof groups
    central = subgroup_number == 0
    group_number_fof = np.unique(group_number)

    # Flag satellites that have no central as centrals (1 per fof group, if there are >1, set central as the one with largest mass)
    isolated_fof = ms.match(group_number_fof, group_number[central]) < 0
    for i_fof in range(len(group_number_fof[isolated_fof])):
        sat = group_number == group_number_fof[isolated_fof][i_fof]
        big_sat = np.argmax(msub_descend[sat])
        temp = central[sat]
        temp[big_sat] = 1
        central[sat] = temp

    # Compute fof group masses of descendants
    mfof_descend = Sum_Common_ID([msub_descend], group_number, group_number_fof)[0]

    # Match progenitors to descendants
    ptr1 = ms.match(descendant_index, node_index)

    if -1 in ptr1:
        print "Error: Compute_Diffuse_Accretion(): there are progenitors without a descendant"
        quit()

    # Group number list of descendants aligned onto progenitor list
    group_number_matched = group_number[ptr1]

    # Now compute sum of progenitor fof masses for each descendant fof group
    mfof_progen = Sum_Common_ID([msub_progen], group_number_matched, group_number_fof)[0]

    # Compute diffuse accretion mass
    dm_diffuse = mfof_descend - mfof_progen

    nve_acc = dm_diffuse<0
    
    dm_diffuse_net = np.copy(dm_diffuse) # allow negative diffuse accretion rates
    dm_diffuse[nve_acc] = 0.0 # Don't allow negative diffuse accretion rates

    # Match fof descendant list to their main progenitors
    node_index_fof = node_index[central]

    if len(node_index_fof) != len(dm_diffuse):
        print "Error: Compute_Diffuse_Accretion(): FoF diffuse accretion array doesn't have the same length as the FoF node index array"
        quit()

    main_progenitors = is_main_progenitor == 1

    ptr2 = ms.match( descendant_index[main_progenitors] , node_index_fof)
    ok_ptr2 = ptr2 >= 0

    dm_diffuse_progen = np.zeros_like(msub_progen)
    dm_diffuse_progen_net = np.zeros_like(msub_progen)

    temp = dm_diffuse_progen[main_progenitors]
    temp[ok_ptr2] = dm_diffuse[ptr2][ok_ptr2]
    dm_diffuse_progen[main_progenitors] = temp

    temp = dm_diffuse_progen_net[main_progenitors]
    temp[ok_ptr2] = dm_diffuse_net[ptr2][ok_ptr2]
    dm_diffuse_progen_net[main_progenitors] = temp

    return dm_diffuse_progen, dm_diffuse_progen_net

'''msub_progen = np.array([2,4,3,5,4])
msub_descend = np.array([1., 20, 3])
node_index = np.array([0, 1, 2])
descendant_index = np.array([0, 1, 1, 1, 2])
group_number = np.array([0, 0, 1])
subgroup_number = np.array([1, 0, 0])
is_main_progenitor = np.array([1, 0, 0, 1, 1])

print "was expecting", [0, 0, 0, 7, 0], [1, 20, 4]
print Compute_Diffuse_Accretion(msub_progen, msub_descend, node_index, descendant_index, group_number, subgroup_number, is_main_progenitor)
exit()'''

'''a = [np.random.randint(0, high=1000000000000,  size=2000000).astype('float64')]
id1 = np.random.randint(0, high=1000000000000, size=2000000)
id2 = np.random.randint(0, high=1000000000000, size=2500000)
print Sum_Common_ID(a, id1, id2)
exit()'''

# Needed subhalo/tree data
subhalo_quantity_list = ["GroupNumber", "SubGroupNumber", "MassType","MassType","MassType","MassType"]
part_index =            [None,          None            , 1         ,0         ,4         ,5]
subhalo_type_list =     ["int",        "int",             "float",    "float",  "float",   "float"]

tree_quantity_list = ["descendantIndex", "nodeIndex","snapshotNumber","isMainProgenitor","isInterpolated"]
tree_type_list =     ["int",             "int",       "int",          "int",             "int"]

# Load subhalo/tree information
output_list, expansion_factor, snapshots = lt.Load_Trees(sub_path, tree_path, nsnap, subhalo_quantity_list, subhalo_type_list, part_index, tree_quantity_list,tree_type_list,test_mode)

# Unpack the data
(group_number, subgroup_number, mdm, mgas, mstar, mbh, descendant_index, node_index, snap_num, isMainProgenitor, isInterpolated) = output_list

# Identify star clumps, and find the correct halo mass if they contain a lot of stars
g2Msun = 1./(1.989e33)
mbar = mstar+mbh+mgas
clump = (mdm/fdm*fb < (mbar)*0.5) & (mstar * 1.989e43 * g2Msun / h > 1e9) & (isInterpolated==0)

if len(clump[clump])>0:
    ptr = ms.match(group_number[clump], group_number[subgroup_number==0])
    temp = mdm[clump]
    temp[ptr>=0] = mdm[subgroup_number==0][ptr][ptr>=0]
    mdm[clump] = temp

mchalo = np.copy(mdm) # halo mass variable that takes max past value for satellites

node_index_lite = np.arange(0,len(node_index))
ptr = ms.match(descendant_index, node_index)
ok = ptr >= 0
descendant_index_lite = node_index_lite[ptr]
descendant_index_lite[ok==False] = -1

# Output arrays
dm_diffuse_progen = np.zeros_like(mdm)
dm_diffuse_progen_net = np.zeros_like(mdm)
msub_int = np.zeros_like(mdm)

# Set -ve mass halos (interpolated haloes) as having 0 mass
nve_mass = mdm < 0
mdm[nve_mass] = 0.0

# In test mode, where we only load one tree file, we need to remove galaxies that don't have descendants
if test_mode:
    print "test mode: computing subhalos with no descendant"
    ptr = ms.match(descendant_index, node_index)
    has_descendant = ptr >= 0
    print "done"

# Snapshot loop
for i_snap, snapshot in enumerate(snapshots[:-1]):

    print "computing accretion rates for", i_snap, "of",len(snapshots[:-1])
    
    ok_ts = (snap_num == snapshot) & (descendant_index >= 0)
    ok_ns = (snap_num == snapshot+1)

    if test_mode:
        ok_ts = ok_ts & has_descendant

    # Sort ns subhalos by group_number
    order_ns = np.argsort(group_number[ok_ns])

    #dm_diffuse_progen[ok_ts], dm_diffuse_progen_net[ok_ts] = Compute_Diffuse_Accretion(mdm[ok_ts], mdm[ok_ns][order_ns], node_index[ok_ns][order_ns], descendant_index[ok_ts], group_number[ok_ns][order_ns], subgroup_number[ok_ns][order_ns], isMainProgenitor[ok_ts])
    dm_diffuse_progen[ok_ts], dm_diffuse_progen_net[ok_ts] = Compute_Diffuse_Accretion(msub_int[ok_ts], mdm[ok_ns][order_ns], node_index[ok_ns][order_ns], descendant_index[ok_ts], group_number[ok_ns][order_ns], subgroup_number[ok_ns][order_ns], isMainProgenitor[ok_ts])

    # Integrate dm masses
    msub_int_ts = msub_int[ok_ts]
    msub_int_ts += dm_diffuse_progen_net[ok_ts]

    msub_int[ok_ns] = Sum_Common_ID([msub_int_ts], descendant_index_lite[ok_ts], node_index_lite[ok_ns])[0]

    # Accrete mass onto central haloes on the next step that have no progenitor                                                                                                                                                                                                  
    ok_ns = ok_ns & (mdm > 0)
    no_progen = (msub_int[ok_ns] == 0.0) & (subgroup_number[ok_ns]==0)
    msub_int_ns = msub_int[ok_ns]
    msub_int_ns[no_progen] += mdm[ok_ns][no_progen]
    msub_int[ok_ns] = msub_int_ns

    ok_ns = (snap_num == snapshot+1)
    # compute max past mass for satellite subhalos (also deal with interpolated halos)
    sat_ns = (subgroup_number[ok_ns] > 0) | (isInterpolated[ok_ns]==1)
    if len(mdm[ok_ns][sat_ns]>0) and len(mdm[ok_ts]>0):
        ok_ts_mp = ok_ts & (isMainProgenitor==1)
        ptr = ms.match(node_index[ok_ns][sat_ns], descendant_index[ok_ts_mp])
        ok_match = ptr >= 0
        msat_progen = np.zeros_like(mdm[ok_ns][sat_ns])
        msat_progen[ok_match] = mchalo[ok_ts_mp][ptr][ok_match]
        temp = mchalo[ok_ns]
        temp[sat_ns] = np.maximum(temp[sat_ns], msat_progen)
        mchalo[ok_ns] = temp

# Delete existing hdf5 file if one exists before writing a new one                                                                   
if test_mode:
    filename = "test_trees_preprocess_"+sim_name+".hdf5"
else:
    filename = "trees_preprocess_"+sim_name+".hdf5"
if os.path.isfile(output_path+filename):
    os.remove(output_path+filename)

File_pre = h5py.File(output_path+filename,"a")
File_pre.create_dataset("dm_diffuse",data=dm_diffuse_progen)
File_pre.create_dataset("dm_diffuse_net",data=dm_diffuse_progen_net)
File_pre.create_dataset("msub_int",data=msub_int)
File_pre.create_dataset("mchalo", data=mchalo)
