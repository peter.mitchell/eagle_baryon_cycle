import time
t1 = time.time()
t0 = t1

# Where to read/write cat files
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

verbose = False
debug = False

subsample =False
n_per_bin = 1000

# If we true, we make the approximation that all particles that have left the FoF group are definitely in a halo wind
# This significantly reduces the IO cost, but will break dir_heated measurements (and anything else that needs properties (besides coordinates) outside of haloes)
bound_only_mode = True
if bound_only_mode:
    print "Running in bound-only mode"

    # When possible, retain memory between steps (this increases memory usage but reduces IO cost)
    retain_memory = True
    if retain_memory:
        print "Running in retain memory mode"
else:
    retain_memory = False
    print "Not runing in bound-only mode"

# If true, find redefine the central subhalo as being all particles within r200_crit
# (that are not subfind bound to other satellites - and I think also not part of other FoF groups)
central_r200 = True

ism_definition = "Joop_version"
print "Running with ism definition", ism_definition

# Depending on the ISM defn, we may be able to skip reading the DM particles. This means the DM accretion/merger rates won't be computed.
# But these can be run in a separate script later if needed.
if ism_definition == "Joop_version":
    skip_dark_matter = True
    print "Skipping dark matter"
else:
    skip_dark_matter = False

ns_tdyn = True # Set to true to use a fixed fraction of the halo dynamical time to use for _ns                                                                              
# tdyn_frac_ns is a parameter that controls the fraction of a halo dynamical time used for finding _ns for wind slection                                                    
tdyn_frac_ns = 0.25

# Smooth acc thr (Msun) - mass threshold below which merging subhaloes are considered as smooth accretion for inflows
# Decide whether to use fixed mass cuts, or cuts that vary as a fixed fraction of m200
mass_cuts = "absolute"
#mass_cuts = "fraction"
if mass_cuts != "absolute" and mass_cuts != "fraction":
    print "Error: mass_cuts=",mass_cuts,"must be 'absolute' or 'fraction'"
    quit()

mfrac_cut = 1e-3 # For now lets use this for both m200 and smooth acr for now
if mass_cuts == "fraction":
    print "Using a fractional mass cut for smooth acr thr of", mfrac_cut

# This corresponds to 100 dm particles at standard eagle resolution
smooth_acc_thr = 969504631.0
# This corresponds to 1000 dm particles at standard eagle resolution
#smooth_acc_thr = 9695046310.0
# This corresponds to 10 dm particles at standard eagle resolution
#smooth_acc_thr = 96950463.1
if mass_cuts == "absolute":
    print "Using smooth accretion threshold mass", smooth_acc_thr

# If true, cut all halos below this halo mass in the analysis (note gas/dm halo accretion rates still include haloes down to smooth acc thr)
# But these haloes will not be included for wind/ejecta type analysis - so this is also the minimum mass to be included in galactic transfer and recycling measurements
# This is desirable because this way we control the definition of those quantities, rather than it just being set implicitly by the simulation resolution.
cut_m200 = True
m200_cut = 969504631.0 # Msun - 100 dm particles at standard Eagle resolution
#m200_cut = 9695046310.0 # Msun - 1000 dm particles at standard Eagle resolution
#m200_cut = 96950463.1 # Msun - 10 dm particles at standard Eagle resolution
if cut_m200:
    print "Running with m200 cut of", m200_cut
else:
    print "Running without an m200 cut"


# Low-mass satellites are (when added together) very expensive inside the main loop if they have to cross-match against the (often much larger) FOF particle lists
# To mitigate this, the below option approximates the solution for satellite wind/ejecta etc, and does not perform any measurements for the satellites themselves
optimise_sats = True
if optimise_sats:
    print "Running with satellite optimisations"

# Maximum subvolume size (at a given snapshot) before splitting the subvolume into 8 smaller subvolumes when performing IO
# and doing a first round of particle selection - This is only used if we are using Peano-Hilbert IO
volmax = (0.6777 * 25 * 1e5)**3 # Mpc h^-1

import sys
import argparse
import measure_particles_functions as mpf

parser = argparse.ArgumentParser()
parser.add_argument("-subvol", help = "subvolume",type=int)
parser.add_argument("-nsub1d", help = "number of subvolumes",type=int)
parser.add_argument("-datapath", help = "directory containting simulation data")
parser.add_argument("-simname", help = "sim/tree name")
parser.add_argument("-snap", help = "final snapshot to measure for",type=int)
parser.add_argument("-test_mode", type=mpf.str2bool)
parser.add_argument("-n_jobs", help = "number of parallel joblib subprocesses",type=int)

if len(sys.argv)!=15:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()
 
subvol = args.subvol
nsub1d = args.nsub1d
DATDIR = args.datapath
sim_name = args.simname
snap_end = args.snap
test_mode = args.test_mode
n_jobs_parallel=args.n_jobs # Number of sub-processes to spawn for executing the joblib loops

# If there is one subvolume, it's always better to do custom IO if possible
if nsub1d == 1:
    volmax = 1e5**3

if test_mode:
    print ""
    print "Running in test mode"
    print ""
    output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_test_mode/"

    skip_start_hack = False
    if skip_start_hack:
        print ""
        print "Warning: hacking test mode to skip starting snapshots"
        print ""

    use_restart = False

else:
    output_path = catalogue_path + sim_name+"_subvols_nsub1d_"+str(nsub1d)+"/"
    if subsample:
        output_path = catalogue_path+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
    skip_start_hack = False

# For snapshot data, star particles have a variable that enables the calculation of the injected supernova energy
# I'm hijacking this to also ask if we want to compute rate of particles being directly heated in the ISM wind and halo wind
if "snap" in sim_name:
    write_SNe_energy_fraction = True
else:
    write_SNe_energy_fraction = False

import read_eagle as re
import numpy as np
import warnings
np.seterr(all='ignore')
import os
import h5py
from joblib import Parallel, delayed
from os import listdir
import shutil
import match_searchsorted as ms
import utilities_cosmology as uc
import measure_particles_io as mpio
import measure_particles_classes as mpc
import main_loop_temp_R_SFR as main

# First let's work out the relationship between tree snapshots and simulation snapshots/snipshots
cat_path = catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5"

# Not-ideal hacky solution to not wanting duplicate catalogue files for variant analysis runs
if "L0025N0752_Recal" in sim_name:
    cat_path = catalogue_path + "subhalo_catalogue_L0025N0752_Recal.hdf5"
if "L0025N0376_REF_200_snip" in sim_name:
    cat_path = catalogue_path + "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"

n_tries = 0
success = True
# This can fail if two jobs try to read the catalogue at the same time - get round this by waiting 
while n_tries < 50:
    try:
        cat_file = h5py.File(cat_path,"r")
        tree_snapshot_info = cat_file["tree_snapshot_info"]
        snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
        redshifts = tree_snapshot_info["redshifts_tree"][:]
        sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
        cosmic_time = tree_snapshot_info["cosmic_times_tree"][:]
        cat_file.close()
        success = True
        break
    except:
        print "Failed to read base catalogue, trying again in 10 seconds", cat_path
        n_tries += 1
        time.sleep(10)

if not success:
    print "Error: failed to read base catalogue, stopping here"
    quit()

# Bad convention to refer to simulation snipshots/snapshots as "snipshots" from here on in
# And to refer to tree outputs as "snapshots" from here on in
ok = snapshot_numbers <= snap_end
snap_list = snapshot_numbers[ok][::-1]
snip_list = sn_i_a_pshots_simulation[ok][::-1]
cosmic_t_list = cosmic_time[ok][::-1]
redshifts = redshifts[ok][::-1]


# Added on 18/2/2020 - prevent tret bins from depending on restart redshift.
cosmic_t_list_initial = np.copy(cosmic_t_list)

def UnConcatenate(arrays,nparts):
    
    list_out = []
    nparts_cumul = np.cumsum(nparts)
    for i_sub in range(len(nparts)):
        if i_sub==0:
            ind_prev = 0
        else:
            ind_prev = nparts_cumul[i_sub-1]
        list_out.append(arrays[ind_prev:nparts_cumul[i_sub]])

    return list_out


retain_ts = False
retain_ns = False

print "measuring rates for snipshots/snapshots", snip_list[0:-1]

t2 = time.time()
print "finished preliminaries, time elapsed", t2-t1

for i_snip, snip_ts in enumerate(snip_list[:-1]):

    # For certain applications, we want to know where we are in the snapshot loop without being affected by the restart resets
    try:
        i_snip_glob = np.argmin(abs(snip_ts - snip_list_tot))
    except:
        i_snip_glob = i_snip

    # No _ps on first snapshots/snipshot
    # For restart, this skip also serves us to move us to the correct starting point
    if i_snip == 0:
        continue
    
    # Special case for broken first snapshots in 50 Mpc ref run
    if sim_name == "L0050N0752_REF_snap" and i_snip <= 2:
        print "skipping first output because broken"
        continue

    snip_ps = snip_list[i_snip-1]

    t1 = time.time()
    
    print ""
    print "measuring for snip (simulation), snap (tree), time (Gyr), redshift = ", snip_ts, snap_list[i_snip], cosmic_t_list[i_snip], redshifts[i_snip]

    # This is supposed to help with getting python to actually communicate something to the log file
    sys.stdout.flush()

    ######## Work out which snapshot to read as the _ns for wind selection ###############
    cosmic_t_ts = cosmic_t_list[i_snip] # Cosmic time at current 
    tdyn_ts = cosmic_t_ts * 0.1 # Approximate halo dynamical time in Gyr at this timestep
    snap_ts = snap_list[i_snip] # Current tree snapshot

    ok = np.where(snap_list == snap_ts)[0][0]
    t_remain = cosmic_t_list[ok:]
    dt = t_remain[1:] - t_remain[0]

    # tdyn_frac_ns is a parameter set at the top
    d_snap_ns = np.argmin( abs(dt - tdyn_ts * tdyn_frac_ns)) +1 # Number of snapshots into the future that should be read for _ns

    if not ns_tdyn:
        d_snap_ns = 1

    if d_snap_ns > 1:
        print "_ns skip is", d_snap_ns

    snip_ns = snip_list[i_snip + d_snap_ns]
    snap_ns = snap_ts + d_snap_ns

    # Read subvol file to load halo catalogue and selection boundaries
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    if not os.path.exists(output_path_ts):
        print "Couldn't find directory",output_path_ts,"moving on"
        continue

    print "Attempting to read", output_path_ts+filename_ts
    File = h5py.File(output_path_ts+filename_ts,"a")

    # Read selection boundary
    try:
        xyz_min_ts = File["select_region/xyz_min"][:]
        xyz_max_ts = File["select_region/xyz_max"][:]
    except:
        print output_path_ts+filename_ts, "doesn't exist, moving to next snipshot"
        File.close()
        continue

    # Quit if there are no subhaloes in this subvolume (this assumes that future subvols will also not have subhaloes)
    if np.isnan(xyz_min_ts[0]):
        print "No haloes inside this subvolume, moving to next snipshot"
        File.close()
        continue
    try:
        # Read in selection boundary for next snipshot
        output_path_ns = output_path + "Snip_"+str(snip_ns)+"/"
        filename_ns = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ns)+"_"+str(subvol)+".hdf5"
        File_ns = h5py.File(output_path_ns+filename_ns,"r")
        
        xyz_max_ns = File_ns["select_region/xyz_max"][:]
        xyz_min_ns = File_ns["select_region/xyz_min"][:]
    except:
        if not i_snip+1 == len(snip_list): # There is no next snipshot once we reach z=0
            print "Error: failed to read selection boundary for the next snipshot, skipping this snapshots"
            continue

    # Want to read in a big enough volume to accomodate both the subhalo & progenitor samples for this snipshot and the subhalo/progenitor samples for the next snipshot
    boxsize = mpio.Get_Box_Size(sim_name, DATDIR,snip_ts)
 
    for i_dim in range(3):
        if xyz_max_ts[i_dim] > xyz_max_ns[i_dim] + 0.5*boxsize:
            xyz_max_ns[i_dim]+= boxsize; xyz_min_ns[i_dim] += boxsize
        if xyz_max_ns[i_dim] > xyz_max_ts[i_dim] + 0.5*boxsize:
            xyz_max_ts[i_dim] += boxsize; xyz_min_ts[i_dim] += boxsize

    if retain_memory and d_snap_ns == 1:
        # In this case, we can carry over the particle data from _ns > _ts > _ps
        for i_dim in range(3):
            xyz_max_ns[i_dim] = max(xyz_max_ns[i_dim], xyz_max_ts[i_dim])
            xyz_min_ns[i_dim] = min(xyz_min_ns[i_dim], xyz_min_ts[i_dim])

    if nsub1d >1 or test_mode:
        print "for _ts, reading particles from volume with vertices: x=", xyz_min_ts[0],xyz_max_ts[0], " y=", xyz_min_ts[1], xyz_max_ts[1], " z=",xyz_min_ts[2],xyz_max_ts[2] 
        print "for _ns, reading particles from volume with vertices: x=", xyz_min_ns[0],xyz_max_ns[0], " y=", xyz_min_ns[1], xyz_max_ns[1], " z=",xyz_min_ns[2],xyz_max_ns[2] 

    # Read halo catalogue
    subhalo_group = File["subhalo_data"]
    
    subhalo_props = {}

    try:
        mchalo_list = subhalo_group["mchalo"][:]
    except:
        print "Error: first run copy_mchalo.py, then rerun select_subvolumes.py"
        quit()

    order_sub = np.argsort(mchalo_list)[::-1]
    mchalo_list = mchalo_list[order_sub]

    subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host", "isInterpolated"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]

    for name in subhalo_prop_names:
        subhalo_props[name] = subhalo_group[name][:][order_sub]

    # Select halos above the mass cut
    if cut_m200:
        ok = (subhalo_props["m200_host"] > m200_cut) & (subhalo_props["subgroup_number"] == 0)
        
        n_for_limit = min(100,len(subhalo_props["m200_host"]))
        mchalo_lim = np.median(np.sort(subhalo_props["mchalo"][ok])[0:n_for_limit])
        if test_mode:
            mchalo_lim = m200_cut # Not enough subhaloes in test mode to reliably evaluate the offset between m200 and mchalo (subfind mass) for centrals
            
        n_sub_before_mask = len(mchalo_list)
        ok = ok | ((subhalo_props["subgroup_number"]>0) & (subhalo_props["mchalo"] > mchalo_lim)) | ((subhalo_props["isInterpolated"]==1)&(subhalo_props["mchalo"] > mchalo_lim))
        
        # Keep track of non-selected satellite haloes, so we can control the smooth/merger halo accretion later
        removed_mask = np.where((ok==False) & (subhalo_props["subgroup_number"]>0) & (subhalo_props["m200_host"] > m200_cut))[0]
        removed_props_ts = {}
        for name in subhalo_prop_names:
            removed_props_ts[name] = subhalo_props[name][removed_mask]

        subhalo_mask = np.where(ok)[0]

        for name in subhalo_prop_names:
            subhalo_props[name] = subhalo_props[name][subhalo_mask]
        mchalo_list = mchalo_list[subhalo_mask]
        
        if len(mchalo_list)==0:
            print "After applying subhalo mass cut, there are no remaining subhalos, moving on"
            File.close()
            File_ns.close()
            continue
    else:
        removed_props_ts = {}
        for name in subhalo_prop_names:
            removed_props_ts[name] = np.zeros(0)

    if debug:
        ih_choose = np.argmax(mchalo_list)
    else:
        ih_choose = 0 # Dummy value

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ts = max(len(str(int(subhalo_props["subgroup_number"].max()))) +1 ,6)
    subhalo_props["subhalo_index"] = subhalo_props["group_number"].astype("int64") * 10**nsep_ts + subhalo_props["subgroup_number"].astype("int64")

    # Find satellites that have no central (to be used later)
    ptr_no_central = ms.match(subhalo_props["group_number"], subhalo_props["group_number"][subhalo_props["subgroup_number"]==0])
    no_central = (subhalo_props["subgroup_number"] > 0) & (ptr_no_central < 0)

    progenitor_group = File["progenitor_data"]
    
    subhalo_prop_names = ["group_number","subgroup_number","x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host","isInterpolated","node_index"]

    for name in subhalo_prop_names:
        name_progen = name+"_progen"
        subhalo_props[name_progen] = progenitor_group[name][:][order_sub]
        
    if cut_m200:
        for name in subhalo_prop_names:
            subhalo_props[name+"_progen"] = subhalo_props[name+"_progen"][subhalo_mask]

    all_progenitor_group = File["all_progenitor_data"]

    subhalo_prop_names =  ["group_number","subgroup_number","descendant_index","m200_host","mtot","isInterpolated"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","r200_host"]

    for name in subhalo_prop_names:
        name_all_progen = name+"_all_progen"
        subhalo_props[name_all_progen] = all_progenitor_group[name][:]
        
    if cut_m200:
        ptr = ms.match(subhalo_props["descendant_index_all_progen"], np.append(subhalo_props["node_index"],removed_props_ts["node_index"]))
        ok_match = ptr >= 0
        for name in subhalo_prop_names:
            subhalo_props[name+"_all_progen"] = subhalo_props[name+"_all_progen"][ok_match]

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ps = max(len(str(int(np.append(subhalo_props["subgroup_number_progen"],subhalo_props["subgroup_number_all_progen"]).max()))) +1 ,6)
    subhalo_props["subhalo_index_progen"] = subhalo_props["group_number_progen"].astype("int64") * 10**nsep_ps + subhalo_props["subgroup_number_progen"].astype("int64")
    subhalo_props["subhalo_index_all_progen"] = subhalo_props["group_number_all_progen"]* 10**nsep_ps + subhalo_props["subgroup_number_all_progen"]
    
    # Error checking
    ptr = ms.match(subhalo_props["group_number_progen"], subhalo_props["group_number_all_progen"])
    if len(ptr[ptr<0])>0:
        print "Error, halos in the progen list are not present in the all progen list"
        print np.unique(subhalo_props["group_number_progen"])
        print np.unique(subhalo_props["group_number_all_progen"])
        quit()

    # Read halo properties for _ns output (in order to identify the positions of the descendants of haloes from this output) - if _ns is not the next output in the tree we need to walk down the tree to find the correct descendant
    n_tree_walk = snap_ns - snap_ts
    for i_walk in range(n_tree_walk):

        snip_i_walk = snip_list[i_snip+i_walk+1]

        print "Reading", output_path + "Snip_"+str(snip_i_walk)+"/"
        
        filename_i_walk = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_i_walk)+"_"+str(subvol)+".hdf5"
        output_path_i_walk = output_path + "Snip_"+str(snip_i_walk)+"/"
        File_ns = h5py.File(output_path_i_walk+filename_i_walk,"r")

        subhalo_group_ns = File_ns["subhalo_data"]
        node_index_list_ns = subhalo_group_ns["node_index"][:]

        if i_walk == 0:
            ptr = ms.match(subhalo_props["descendant_index"], node_index_list_ns)
        else:
            ptr = ms.match(subhalo_props["descendant_index_ns"], node_index_list_ns) # Note descendant left over from prev iteration through the loop
        ok_match = ptr>=0

        subhalo_props["descendant_index_ns"] = np.zeros_like(mchalo_list).astype("int")-1e7 # Note it's important this is an ints array - not float
        subhalo_props["descendant_index_ns"][ok_match] = subhalo_group_ns["descendant_index"][:][ptr][ok_match]

        if i_walk != n_tree_walk-1:
            File_ns.close()

    if len(subhalo_props["descendant_index"][ok_match]) != len(subhalo_props["descendant_index"]):
        print "Warning: " + str(len(subhalo_props["descendant_index"][ok_match==False])) +" of "+str(len(subhalo_props["descendant_index"]))+" haloes could not find a descendant on the next simulation output"
        
    subhalo_prop_names = ["group_number","subgroup_number"]
    subhalo_prop_types =  ["int",         "int",          ]

    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host","isInterpolated"]
    subhalo_prop_types +=  ["float", "float", "float", "float", "float", "float", "float", "float","int"]

    for name, prop_type in zip(subhalo_prop_names,subhalo_prop_types):
        name_ns = name+"_ns"
        if prop_type == "int":
            subhalo_props[name_ns] = np.zeros_like(mchalo_list).astype("int")-1000000
        elif prop_type == "float":
            subhalo_props[name_ns] = np.zeros_like(mchalo_list) + np.nan
        else:
            "Error: prop type",prop_type,"not understood"
            quit()
        subhalo_props[name_ns][ok_match] = subhalo_group_ns[name][:][ptr][ok_match]

    File_ns.close()

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    if len(mchalo_list[ok_match])>0:
        nsep_ns = max(len(str(int(subhalo_props["subgroup_number_ns"][ok_match].max()))) +1 ,6)
    else:
        nsep_ns = 6
    subhalo_props["subhalo_index_ns"] = subhalo_props["group_number_ns"].astype("int64") * 10**nsep_ns + subhalo_props["subgroup_number_ns"].astype("int64")

    ############### Initialize output dicts ###########################

    output_mass_names = ["mass_new_stars_init_sat", "mass_stellar_rec"]


    output_mass_dict = {}

    for name in output_mass_names:
        output_mass_dict[name] = np.zeros(len(mchalo_list))
    
  

    ############## Read Eagle particle data #######################
    t2 = time.time()

    gas_names_in_ps = ['Coordinates']
    gas_names_out_ps = ['coordinates']

    all_gas_names_in_ps = []; all_gas_names_out_ps = []
    if not bound_only_mode:
        all_gas_names_in_ps += ['Coordinates']
        all_gas_names_out_ps += ['coordinates']

    dm_names_in_ps = []; dm_names_out_ps = []
    if not skip_dark_matter:
        dm_names_in_ps += ["Coordinates"]
        dm_names_out_ps += ["coordinates"]

    star_names_in_ps = ['Mass', "InitialMass", 'Coordinates']
    star_names_out_ps = ['mass', "mass_init",'coordinates']
    
    bh_names_in_ps = []
    bh_names_out_ps = []

    data_names_in_ps = [gas_names_in_ps, dm_names_in_ps, star_names_in_ps, bh_names_in_ps, all_gas_names_in_ps,[],[],[]]
    data_names_out_ps = [gas_names_out_ps, dm_names_out_ps, star_names_out_ps, bh_names_out_ps, all_gas_names_out_ps,[],[],[]]

    ####### _ps #############
    print "Reading particle data for _ps (previous snipshot)"

    # Decide how many sub-subvolumes we want for doing IO (and some intial particle selection)
    # The answer is ideally 1, but if memory is limited we can reduce memory cost by splitting the IO into smaller steps, discarding un-needed particles as we go (this is much less important for bound_only_mode)
    vol_ps = np.product(xyz_max_ts-xyz_min_ts) # Mpc h^-1

    if vol_ps > volmax:
        nsubvol_pts = 8
        
        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ts
        
        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ts[0]-xyz_min_ts[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ts[1]-xyz_min_ts[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ts[2]-xyz_min_ts[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1
        
        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ts[0]-xyz_min_ts[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ts[1]-xyz_min_ts[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ts[2]-xyz_min_ts[2])*0.5
    else:
        nsubvol_pts = 1

    if retain_ts: # Do we carry over particle data from previous step?
        if verbose:
            print "Using previous _ts data on _ps"
        nsubvol_pts = 1  # In this case we are not doing any _ps IO on this step, so the below loop is redundant
        gas_ps_in = gas_ts; dm_ps_in = dm_ts; star_ps_in = star_ts; bh_ps_in = bh_ts; sim_props_ps = sim_props_ts
        all_gas_ps_in = gas_ps_in

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_temp = time.time()

        t_core_io_i = time.time()

        # Read in particle data for previous snapshot
        if not retain_ts:
            if nsub1d > 1 or test_mode or not bound_only_mode:
                # Use JCH Hash-Table IO (for efficiently reading in sub-regions of the box)
                if bound_only_mode:
                    read_all = ""
                    gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, sim_props_ps = mpio.Read_All_Particle_Data(snip_ps, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ps,data_names_out_ps,read_all=read_all,verbose=verbose)
                    all_gas_ps_in = gas_ps_in

                else:
                    read_all = "g"
                    gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, all_gas_ps_in, sim_props_ps = mpio.Read_All_Particle_Data(snip_ps, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ps,data_names_out_ps,read_all=read_all,verbose=verbose)


            else:

                if not bound_only_mode:
                    print "Custom IO not implemented for non-bound particles"
                    quit()
                # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
                data_names_in_ps = gas_names_in_ps+ dm_names_in_ps+ star_names_in_ps+ bh_names_in_ps
                data_names_out_ps = gas_names_out_ps+ dm_names_out_ps+ star_names_out_ps+ bh_names_out_ps
                part_types = np.concatenate(( np.zeros(len(gas_names_in_ps)), np.ones(len(dm_names_in_ps)), np.zeros(len(star_names_in_ps))+4, np.zeros(len(bh_names_in_ps))+5 )).astype("int")

                gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, sim_props_ps = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ps, sim_name, data_names_in_ps,data_names_out_ps,part_types,njobs=n_jobs_parallel,verbose=verbose)
                all_gas_ps_in = gas_ps_in

        t_core_io = time.time() - t_core_io_i

        # Create arrays of halo properties for _ps
        # For _ps, we need to be careful to select particles that are only in subhalo progenitors (and not complete FoF groups which tend to include massive halos)
        # Create a unique id for each subhalo out of group_number and subgroup_number (this is because we don't have node_index for particles)                    
        nsep = max(len(str(int(subhalo_props["subgroup_number_all_progen"].max()))) +1 ,6)

        cat_list_all_progen = subhalo_props["group_number_all_progen"].astype("int64") * 10**nsep + subhalo_props["subgroup_number_all_progen"].astype("int64")
        cat_list_progen = subhalo_props["group_number_progen"].astype("int64") * 10**nsep + subhalo_props["subgroup_number_progen"].astype("int64")

        cat_ps_master_list = np.concatenate((cat_list_progen, cat_list_all_progen)) # (Note to self - isn't this redundant - I thought the code crashes if there are progen halos not in the all_progen list?) 
        cat_ps_master_list, ind_unique = np.unique(cat_ps_master_list,return_index=True)

        t_rebind_i = time.time()
        if central_r200 and not retain_ts:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            # Note if retain_ts is true, we already did the required rebinding on an earlier step
            if verbose:
                print "rebinding _ps"

            halo_coordinates_ps = [np.append(subhalo_props["x_halo_progen"],subhalo_props["x_halo_all_progen"])[ind_unique],\
                                       np.append(subhalo_props["y_halo_progen"],subhalo_props["y_halo_all_progen"])[ind_unique],\
                                       np.append(subhalo_props["z_halo_progen"],subhalo_props["z_halo_all_progen"])[ind_unique]]
            grn_temp = np.append(subhalo_props["group_number_progen"], subhalo_props["group_number_all_progen"])[ind_unique]
            sgrn_temp = np.append(subhalo_props["subgroup_number_progen"], subhalo_props["subgroup_number_all_progen"])[ind_unique]
            r200_temp = np.append(subhalo_props["r200_host_progen"], subhalo_props["r200_host_all_progen"])[ind_unique]
            iI_temp = np.append(subhalo_props["isInterpolated_progen"], subhalo_props["isInterpolated_all_progen"])[ind_unique]

            # Gas rebinding
            # Note - this horrible block is outside a function (and repeated many times) because of the JobLib pecularities (pickle-related)
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_ps, r200_temp, boxsize)
            temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)

            gas_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ps_in)

            if not bound_only_mode:
                mpf.Rebind_All_Particles_From_Bound(all_gas_ps_in, gas_ps_in)

            # Dark matter rebinding
            if len(dm_names_in_ps) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ps_in)

            # Star rebinding
            if len(star_ps_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                star_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ps_in)


        t_rebind = time.time() - t_rebind_i
        
        # Having read in positions of all gas particles (including those not bound) - we want to extract anything from the non-bound particles now before discarding the list
        bound_parts_ps = all_gas_ps_in.is_bound()
        not_bound_parts_ps = bound_parts_ps == False
        
        if central_r200:
            ok_unbound_ps = not_bound_parts_ps|all_gas_ps_in.data["subfind_unbound"] # Subfind unbound = True means particles that have been bound to central by Rebind, but were considered unbound by subfind before
        else:
            ok_unbound_ps = not_bound_parts_ps

        
        # Put boolean array out of scope (in hopes garbage collector will do its job) - this probably does nothing
        not_bound_parts_ps = None; bound_parts_ps = None

        # Put all gas dataset out of scope
        all_gas_ps_in = None

        # Remove non-bound particles and bound particles from unwanted halos/subhalo
        cat_gas_list_ps = gas_ps_in.data["group_number"].astype("int64") * 10**nsep + gas_ps_in.data["subgroup_number"].astype("int64")
        if len(cat_ps_master_list) > 0 and len(cat_gas_list_ps) > 0:
            ptr = ms.match(cat_gas_list_ps, cat_ps_master_list,arr2_sorted=True)
            ok_match = ptr >= 0
            gas_ps_in.select_bool(ok_match)

        if not skip_dark_matter:
            cat_dm_list_ps = dm_ps_in.data["group_number"].astype("int64") * 10**nsep + dm_ps_in.data["subgroup_number"].astype("int64")
            if len(cat_ps_master_list) > 0 and len(cat_dm_list_ps) > 0:
                ptr = ms.match(cat_dm_list_ps, cat_ps_master_list,arr2_sorted=True)
                ok_match = ptr >= 0
                dm_ps_in.select_bool(ok_match)

        cat_star_list_ps = star_ps_in.data["group_number"].astype("int64") * 10**nsep + star_ps_in.data["subgroup_number"].astype("int64")
        if len(cat_ps_master_list) > 0 and len(cat_star_list_ps) > 0:
            ptr = ms.match(cat_star_list_ps, cat_ps_master_list,arr2_sorted=True)
            ok_match = ptr >= 0
            star_ps_in.select_bool(ok_match)


        # Init datasets
        if i_subvol == 0:
            gas_ps = mpc.DataSet(); dm_ps = mpc.DataSet(); star_ps = mpc.DataSet(); bh_ps = mpc.DataSet()

        # Merge datasets
        if nsubvol_pts > 1:
            gas_ps.merge_dataset(gas_ps_in)
            star_ps.merge_dataset(star_ps_in)
            bh_ps.merge_dataset(bh_ps_in)
            if not skip_dark_matter:
                dm_ps.merge_dataset(dm_ps_in)

            # Put the input datasets out of scope
            gas_ps_in = None; dm_ps_in = None; star_ps_in = None; bh_ps_in = None
        else:
            gas_ps = gas_ps_in; dm_ps = dm_ps_in; star_ps = star_ps_in; bh_ps = bh_ps_in
    
        # Put the input datasets out of scope
        cat_gas_list_ps = None; cat_dm_list_ps = None; cat_star_list_ps = None; cat_bh_list_ps = None


    ########## _ts ##############
    print "Reading particle data for _ts (current snipshot)"

    gas_names_in_ts = ['Coordinates']
    gas_names_out_ts = ['coordinates']


    all_gas_names_in_ts = []
    all_gas_names_out_ts = []
    if not bound_only_mode:
        all_gas_names_in_ts += ['Coordinates']
        all_gas_names_out_ts += ['coordinates']

    dm_names_in_ts = []; dm_names_out_ts = []
    if not skip_dark_matter:
        dm_names_in_ts = ["Coordinates"]
        dm_names_out_ts = ["coordinates"]

    star_names_in_ts = ['Mass', "InitialMass", 'Coordinates',"StellarFormationTime"]
    star_names_out_ts = ['mass', "mass_init",'coordinates',"aform"]

    bh_names_in_ts = []
    bh_names_out_ts = []

    data_names_in_ts = [gas_names_in_ts, dm_names_in_ts, star_names_in_ts, bh_names_in_ts, all_gas_names_in_ts,[],[],[]]
    data_names_out_ts = [gas_names_out_ts, dm_names_out_ts, star_names_out_ts, bh_names_out_ts, all_gas_names_out_ts,[],[],[]]

    if retain_ns: # Do we carry over particle data from previous step?
        if verbose:
            print "Using previous _ns data on _ts"
        nsubvol_pts = 1  # In this case we are not doing any _ts IO on this step, so the below loop is redundant
        gas_ts_in = gas_ns; dm_ts_in = dm_ns; star_ts_in = star_ns; bh_ts_in = bh_ns; sim_props_ts = sim_props_ns
        all_gas_ts_in = gas_ts_in

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_core_io_i = time.time()
            
        if not retain_ns:
            if nsub1d > 1 or test_mode or not bound_only_mode:
                if bound_only_mode:
                    read_all = ""
                    gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)
                    all_gas_ts_in = gas_ts_in
                else:
                    read_all = "g"
                    gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, all_gas_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)

            else:
                # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
                data_names_in_ts = gas_names_in_ts+ dm_names_in_ts+ star_names_in_ts+ bh_names_in_ts
                data_names_out_ts = gas_names_out_ts+ dm_names_out_ts+ star_names_out_ts+ bh_names_out_ts
                part_types = np.concatenate(( np.zeros(len(gas_names_in_ts)), np.ones(len(dm_names_in_ts)), np.zeros(len(star_names_in_ts))+4, np.zeros(len(bh_names_in_ts))+5 )).astype("int")
                
                gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ts, sim_name, data_names_in_ts,data_names_out_ts,part_types,njobs=n_jobs_parallel,verbose=verbose)
                all_gas_ts_in = gas_ts_in

        t_core_io += time.time() - t_core_io_i

        t_rebind_i = time.time()
        if central_r200 and not retain_ns:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            if verbose:
                print "rebinding _ts"

            # In this case we need to rebind for haloes from _ts on this step, and _ps on the next step
            if retain_memory and i_snip+1 < len(snip_list[0:-1]):
                
                snip_next_step = snip_list[i_snip+1]
                filename_next_step = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_next_step)+"_"+str(subvol)+".hdf5"
                output_path_next_step = output_path + "Snip_"+str(snip_next_step)+"/"
                print "For _ts rebinding, reading", output_path_next_step+filename_next_step
                File_next_step = h5py.File(output_path_next_step+filename_next_step,"r")
                
                all_progenitor_group = File_next_step["all_progenitor_data"]

                x_next_step = all_progenitor_group["x_halo"][:]
                y_next_step = all_progenitor_group["y_halo"][:]
                z_next_step = all_progenitor_group["z_halo"][:]
                grn_next_step = all_progenitor_group["group_number"][:]
                sgrn_next_step = all_progenitor_group["subgroup_number"][:]
                r200_next_step = all_progenitor_group["r200_host"][:]
                iI_next_step = all_progenitor_group["isInterpolated"][:]

                File_next_step.close()

                nsep = max(len(str(int(sgrn_next_step.max()))) +1 ,6)

                cat_list_next_step = grn_next_step.astype("int64") * 10**nsep + sgrn_next_step.astype("int64")
                cat_list = subhalo_props["group_number"].astype("int64") * 10**nsep + subhalo_props["subgroup_number"].astype("int64")

                cat_master_list = np.concatenate((cat_list_next_step,cat_list))
                cat_master_list, ind_unique = np.unique(cat_master_list,return_index=True)

                halo_coordinates_temp = [np.append(x_next_step,subhalo_props["x_halo"])[ind_unique],\
                                       np.append(y_next_step,subhalo_props["y_halo"])[ind_unique],\
                                       np.append(z_next_step,subhalo_props["z_halo"])[ind_unique]]
                grn_temp = np.append(grn_next_step, subhalo_props["group_number"])[ind_unique]
                sgrn_temp = np.append(sgrn_next_step, subhalo_props["subgroup_number"])[ind_unique]
                r200_temp = np.append(r200_next_step, subhalo_props["r200_host"])[ind_unique]
                iI_temp = np.append(iI_next_step, subhalo_props["isInterpolated"])[ind_unique]
            else:
                # Otherwise we are rebinding for _ts haloes on this step only
                halo_coordinates_temp = [subhalo_props["x_halo"], subhalo_props["y_halo"], subhalo_props["z_halo"]]
                grn_temp = subhalo_props["group_number"]; sgrn_temp = subhalo_props["subgroup_number"]; r200_temp = subhalo_props["r200_host"]; iI_temp = subhalo_props["isInterpolated"]

            # gas rebinding
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
            temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_temp, r200_temp, boxsize)
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
            gas_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ts_in)

            if not bound_only_mode:
                mpf.Rebind_All_Particles_From_Bound(all_gas_ts_in, gas_ts_in)

            if len(dm_names_in_ps) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ts_in)

            if len(star_ts_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                star_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ts_in)

        
        else:
            grn_temp = subhalo_props["group_number"] # Used to decide which particles to keep at the end of this sub-loop


        t_rebind += time.time() - t_rebind_i

        bound_parts_ts = all_gas_ts_in.is_bound()
        not_bound_parts_ts = bound_parts_ts == False

        if central_r200:
            # In this case include subfind unbound particles as being unbound for the unbound particle list (this is used to identify "flythrough" wind particles later on)
            ok_unbound_ts = not_bound_parts_ts | all_gas_ts_in.data["subfind_unbound"]
        else:
            ok_unbound_ts = not_bound_parts_ts


        # Make a list of all gas particles which are bound on _ps but unbound on _ts #
        # This is going to be used to more efficiently identify the coordinates of these gas particles later when identifying halo winds
        # Edit: 31/10/2019 - this is now also going to be used to identify FOF ejecta for satellites and low-mass centrals
        ptr = ms.match(gas_ps.data["id"], all_gas_ts_in.data["id"][ok_unbound_ts])
        ok_match = ptr>=0

        if "unbound_ts_ns" not in gas_ps.names:
            gas_ps.add_append_data(ok_match,"unbound_ts_ns") # Boolean that is True if particle is going to be unbound on _ts (will check _ns later)
        else:
            print "Hmm unexpected (unless using sub-subvolume IO, which is no longer allowed), stopping here"
            quit()

        
        # Find any (formerly bound) particles that left the box (or left FoF+SO groups for bound_only_mode) - given them a grn of zero, and put at infinity
        ptr_box = ms.match(gas_ps.data["id"], np.append(star_ts_in.data["id"],all_gas_ts_in.data["id"]))
        ok_unbound_ps = ptr_box < 0

        if nsubvol_pts > 1:
            print "Stopping here. The sub-subvolume IO option no longer works with the above lines r.e. finding unbound particles that have left the 'box'"
            print "We will end up adding every single particle to gas_unbound_ts if we do this...."
            quit()

        gas_ps.data["unbound_ts_ns"][ok_unbound_ps] = True

      
        if not bound_only_mode:
            bound_parts_ts = gas_ts_in.is_bound()
            not_bound_parts_ts = bound_parts_ts == False

            if central_r200:
                # In this case include subfind unbound particles as being unbound for the unbound particle list (this is used to identify "flythrough" wind particles later on)
                ok_unbound_ts = not_bound_parts_ts | gas_ts_in.data["subfind_unbound"]
            else:
                ok_unbound_ts = not_bound_parts_ts
        
        
        # Put all gas dataset out of scope
        all_gas_ts_in = None


        if not retain_memory: # If we are passing on the particle data, we will want unbound particles to find their properties for halo reheat
            bound_parts_ts = gas_ts_in.is_bound()
            gas_ts_in.select_bool(bound_parts_ts)

        # De-scope boolean arrays
        bounds_parts_ts = None; not_bound_parts = None; ok_unbound_ps = None; ok_unbound_ts = None

        # Remove bound particles from other particle types
        if not retain_memory:
            star_ts_in.select_bool(star_ts_in.is_bound())
            bh_ts_in.select_bool(bh_ts_in.is_bound())
            if len(dm_names_in_ts) > 0:
                dm_ts_in.select_bool(dm_ts_in.is_bound())
        
        # Keep only particles bound to subhalos from this subvolume (as opposed to bound to any halo in the simulation)
        if not retain_memory: # As before, we will want the other particles for halo_reheat on the next step if we pass the data on
            gas_ts_in.ptr_prune(grn_temp,"group_number")
            star_ts_in.ptr_prune(grn_temp,"group_number")
            bh_ts_in.ptr_prune(grn_temp,"group_number")
            if len(dm_names_in_ts) > 0:
                dm_ts_in.ptr_prune(grn_temp,"group_number")


        if nsubvol_pts > 1:

            # Init datasets
            if i_subvol == 0:
                gas_ts = mpc.DataSet(); dm_ts = mpc.DataSet(); star_ts = mpc.DataSet(); bh_ts = mpc.DataSet()

            # Merge datasets
            gas_ts.merge_dataset(gas_ts_in)
            star_ts.merge_dataset(star_ts_in)
            bh_ts.merge_dataset(bh_ts_in)
            if not skip_dark_matter:
                dm_ts.merge_dataset(dm_ts_in)

            # Put the input datasets out of scope
            gas_ts_in = None; dm_ts_in = None; star_ts_in = None; bh_ts_in = None
        else:
            gas_ts = gas_ts_in; dm_ts = dm_ts_in; star_ts = star_ts_in; bh_ts = bh_ts_in


    ####### _ns ###########
    print "reading particle data for _ns (next snipshot)"

    gas_names_in_ns = ['Coordinates']
    gas_names_out_ns = ['coordinates']

    all_gas_names_in_ns = []; all_gas_names_out_ns = []
    if not bound_only_mode:
        all_gas_names_in_ns += ['Coordinates']
        all_gas_names_out_ns += ['coordinates']

    dm_names_in_ns = []
    dm_names_out_ns = []

    star_names_in_ns = ['Mass','Coordinates']
    star_names_out_ns = ['mass','coordinates']
    
    bh_names_in_ns = []
    bh_names_out_ns = []

    if d_snap_ns == 1 and retain_memory: # If we are going to carry this data over to later snapshots, we need to ensure we read all variables that will be needed later
        # Note that I'm assuming here that any _ps variables will also be in _ts
        for name_in, name_out in zip(gas_names_in_ts,gas_names_out_ts):
            if name_in not in gas_names_in_ns:
                gas_names_in_ns.append(name_in); gas_names_out_ns.append(name_out)
        for name_in, name_out in zip(all_gas_names_in_ts,all_gas_names_out_ts):
            if name_in not in all_gas_names_in_ns:
                all_gas_names_in_ns.append(name_in); all_gas_names_out_ns.append(name_out)
        for name_in, name_out in zip(dm_names_in_ts,dm_names_out_ts):
            if name_in not in dm_names_in_ns:
                dm_names_in_ns.append(name_in); dm_names_out_ns.append(name_out)
        for name_in, name_out in zip(star_names_in_ts,star_names_out_ts):
            if name_in not in star_names_in_ns:
                star_names_in_ns.append(name_in); star_names_out_ns.append(name_out)
        for name_in, name_out in zip(bh_names_in_ts,bh_names_out_ts):
            if name_in not in bh_names_in_ns:
                bh_names_in_ns.append(name_in); bh_names_out_ns.append(name_out)
        
    data_names_in_ns = [gas_names_in_ns, dm_names_in_ns, star_names_in_ns, bh_names_in_ns, all_gas_names_in_ns,[],[],[]]
    data_names_out_ns = [gas_names_out_ns, dm_names_out_ns, star_names_out_ns, bh_names_out_ns, all_gas_names_out_ns,[],[],[]]

    if vol_ps > volmax:
        nsubvol_ns = 8
        
        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ns
        
        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ns[0]-xyz_min_ns[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ns[1]-xyz_min_ns[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ns[2]-xyz_min_ns[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1
        
        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ns[0]-xyz_min_ns[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ns[1]-xyz_min_ns[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ns[2]-xyz_min_ns[2])*0.5
    else:
        nsubvol_ns = 1

    for i_subvol in range(nsubvol_ns):
        
        if nsubvol_ns > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_ns
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ns
            xyz_subvol_max_i = xyz_max_ns


        t_core_io_i = time.time()

        if nsub1d > 1 or test_mode or not bound_only_mode:
            if bound_only_mode:
                read_all = ""
                gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, sim_props_ns = mpio.Read_All_Particle_Data(snip_ns, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ns,data_names_out_ns, read_all=read_all,verbose=verbose)
                all_gas_ns_in = gas_ns_in
            else:
                read_all = "g"
                gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, all_gas_ns_in, sim_props_ns = mpio.Read_All_Particle_Data(snip_ns, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ns,data_names_out_ns, read_all=read_all,verbose=verbose)

        else:
            # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
            data_names_in_ns = gas_names_in_ns+ dm_names_in_ns+ star_names_in_ns+ bh_names_in_ns
            data_names_out_ns = gas_names_out_ns+ dm_names_out_ns+ star_names_out_ns+ bh_names_out_ns
            part_types = np.concatenate(( np.zeros(len(gas_names_in_ns)), np.ones(len(dm_names_in_ns)), np.zeros(len(star_names_in_ns))+4, np.zeros(len(bh_names_in_ns))+5 )).astype("int")

            gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, sim_props_ns = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ns, sim_name, data_names_in_ns,data_names_out_ns,part_types,njobs=n_jobs_parallel,verbose=verbose)
            all_gas_ns_in = gas_ns_in

        t_core_io += time.time() - t_core_io_i
        
        t_rebind_i = time.time()
        if central_r200:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo

            # For _ns, we could in principle have duplicate entries in the subhalo lists, so need to make a unique list here (just for re-binding)
            nsep = max(len(str(int(subhalo_props["subgroup_number_ns"][np.isnan(subhalo_props["subgroup_number_ns"])==False].max()))) +1 ,6)
            sub_ind_ns = subhalo_props["group_number_ns"]* 10**nsep + subhalo_props["subgroup_number_ns"]
            junk, ind_unique = np.unique(sub_ind_ns, return_index=True)
            grn_temp = subhalo_props["group_number_ns"][ind_unique]
            sgrn_temp = subhalo_props["subgroup_number_ns"][ind_unique]
            r200_temp = subhalo_props["r200_host_ns"][ind_unique]
            iI_temp = subhalo_props["isInterpolated_ns"][ind_unique]

            halo_coordinates_temp = [subhalo_props["x_halo_ns"][ind_unique],subhalo_props["y_halo_ns"][ind_unique],subhalo_props["z_halo_ns"][ind_unique]]

            # If we are carrying this data over to future steps, we need to ensure rebinding is performed for all relevant halos (this is going to be a bit of a painful block of code)
            if d_snap_ns == 1 and retain_memory:
                
                for i_temp in range(2):
                    i_next_step = i_snip + i_temp + 1

                    if i_next_step < len(snip_list[0:-1]):

                        snip_next_step = snip_list[i_next_step]
                        filename_next_step = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_next_step)+"_"+str(subvol)+".hdf5"
                        output_path_next_step = output_path + "Snip_"+str(snip_next_step)+"/"
                        if verbose:
                            print "For _ns rebinding, reading", output_path_next_step+filename_next_step
                        File_next_step = h5py.File(output_path_next_step+filename_next_step,"r")

                        if i_temp == 0:
                            subhalo_group_temp = File_next_step["subhalo_data"]
                        else:
                            subhalo_group_temp = File_next_step["all_progenitor_data"]

                        x_next_step = subhalo_group_temp["x_halo"][:]
                        y_next_step = subhalo_group_temp["y_halo"][:]
                        z_next_step = subhalo_group_temp["z_halo"][:]
                        grn_next_step = subhalo_group_temp["group_number"][:]
                        sgrn_next_step = subhalo_group_temp["subgroup_number"][:]
                        r200_next_step = subhalo_group_temp["r200_host"][:]
                        iI_next_step = subhalo_group_temp["isInterpolated"][:]

                        File_next_step.close()

                        nsep = max(len(str(int(sgrn_next_step.max()))) +1 ,6)

                        cat_list_next_step = grn_next_step.astype("int64") * 10**nsep + sgrn_next_step.astype("int64")
                        cat_list = grn_temp.astype("int64") * 10**nsep + sgrn_temp.astype("int64")

                        cat_master_list = np.concatenate((cat_list_next_step,cat_list))
                        cat_master_list, ind_unique = np.unique(cat_master_list,return_index=True)

                        halo_coordinates_temp = [np.append(x_next_step,halo_coordinates_temp[0])[ind_unique],\
                                                 np.append(y_next_step,halo_coordinates_temp[1])[ind_unique],\
                                                 np.append(z_next_step,halo_coordinates_temp[2])[ind_unique]]
                        grn_temp = np.append(grn_next_step, grn_temp)[ind_unique]
                        sgrn_temp = np.append(sgrn_next_step, sgrn_temp)[ind_unique]
                        r200_temp = np.append(r200_next_step, r200_temp)[ind_unique]
                        iI_temp = np.append(iI_next_step, iI_temp)[ind_unique]

            if verbose:
                print "rebinding _ns"

            # gas rebinding
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
            temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_temp, r200_temp, boxsize)
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
            gas_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ns_in)

            if not bound_only_mode:
                mpf.Rebind_All_Particles_From_Bound(all_gas_ns_in, gas_ns_in)

            if len(dm_names_in_ns) > 0:
                # dark matter rebinding
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ns_in)

            if len(star_names_in_ns) > 0:
                # star rebinding
                if len(star_ns_in.data["id"])>0:
                    particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                    temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                    output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                    star_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ns_in)

            if len(bh_names_in_ns) > 0:
                # bh rebinding
                if len(bh_ns_in.data["id"])>0:
                    particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(bh_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                    temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                    output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                    bh_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, bh_ns_in)

        t_rebind += time.time() - t_rebind_i

        bound_parts_ns = all_gas_ns_in.is_bound()
        not_bound_parts_ns = bound_parts_ns == False

        if i_subvol == 0:
            gas_halo_reheat_ns = mpc.DataSet()

        if central_r200:
            ok_unbound_ns = not_bound_parts_ns|all_gas_ns_in.data["subfind_unbound"]
        else:
            ok_unbound_ns = not_bound_parts_ns

            

        # Put all gas dataset out of scope
        all_gas_ns_in = None

        if d_snap_ns != 1 or retain_memory == False:
            bound_parts_ns = gas_ns_in.is_bound()

            gas_ns_in.select_bool(bound_parts_ns)
            star_ns_in.select_bool(star_ns_in.is_bound())

            gas_ns_in.ptr_prune(grn_temp,"group_number")
            star_ns_in.ptr_prune(grn_temp,"group_number")

        if len(dm_names_in_ns) > 0:
            if d_snap_ns != 1 or retain_memory == False:
                dm_ns_in.select_bool(dm_ns_in.is_bound())
                dm_ns_in.ptr_prune(grn_temp,"group_number")

        if len(bh_names_in_ns) > 0:
            if d_snap_ns != 1 or retain_memory == False:
                bh_ns_in.select_bool(bh_ns_in.is_bound())
                bh_ns_in.ptr_prune(grn_temp,"group_number")
            

        # Init datasets
        if i_subvol == 0:
            gas_ns = mpc.DataSet(); dm_ns = mpc.DataSet(); star_ns = mpc.DataSet(); bh_ns = mpc.DataSet()

        if nsubvol_ns > 1:

            # Merge datasets
            gas_ns.merge_dataset(gas_ns_in)
            if ism_definition == "Mitchell18_adjusted":
                dm_ns.merge_dataset(dm_ns_in)
                bh_ns.merge_dataset(bh_ns_in)
                star_ns.merge_dataset(star_ns_in)

            # Put the input datasets out of scope
            gas_ns_in = None; dm_ns_in = None; star_ns_in = None; bh_ns_in = None
        else:
            gas_ns = gas_ns_in; dm_ns = dm_ns_in; star_ns = star_ns_in; bh_ns = bh_ns_in

        bound_parts_ns = None; not_bound_parts_ns = None

    print "finished io, time elapsed", time.time() - t2, "time elapsed for core IO", t_core_io, "time for rebinding", t_rebind

    t4 = time.time()

    ####### Unpack and get simulation characteristics for this point of the snipshot loop ########
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    expansion_factor_ps = sim_props_ps["expansion_factor"]
    expansion_factor_ns = sim_props_ns["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    redshift_ps = sim_props_ps["redshift"]
    redshift_ns = sim_props_ns["redshift"]
    mass_dm = sim_props_ts["mass_dm"]

    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    # Work out 10 time bins from now to the first snapshot, logarithmically spaced in lookback time
    # These are used to compute the binned ejected/return time distributions for wind/ejecta particles
    t_ls = uc.t_Universe(1.0, omm, h)[0]
    tlb_fs = t_ls - cosmic_t_list_initial[0] # 18/2/2020 - fixed this so that restarts don't change the bin definitions!
    tlb_ts = t_ls - cosmic_t_list[i_snip]



    ##################### Sort the main particle arrays and create indexes for each subhalo (for efficient indexing within the main loop) ##############
    
    print "Sorting particle lists and creating indexes"
    time_sort = time.time()

    gas_ps_subhalo_index = gas_ps.set_index(nsep_ps, return_subhalo_index=True)
    gas_ts.set_index(nsep_ts)
    gas_ns.set_index(nsep_ns)

    star_ps_subhalo_index = star_ps.set_index(nsep_ps, return_subhalo_index=True)
    star_ts.set_index(nsep_ts)
    star_ns.set_index(nsep_ns)





    print "Sorting/index creation completed, took", time.time() - time_sort, "seconds"


    # Pack particle data to pass into function
    part_data = [gas_ps, gas_ts, gas_ns, star_ps, star_ts, star_ns]

    # Pack simulation info
    sim_props = [sim_props_ps, sim_props_ts, sim_props_ns, boxsize]

    # Pack boolean options
    modes = [debug, ih_choose, write_SNe_energy_fraction, ism_definition, skip_dark_matter, optimise_sats, test_mode, bound_only_mode]

    # Pack up output variable names information
    output_names = [output_mass_names]

    #################### Main loop ####################################################
    time_track_select = 0
    time_track_ptr = 0
    time_ptr_actual = 0

    time_pack = time.time()

    if verbose:
        print "time spent packing the data", time.time()-time_pack
    time_main = time.time()

    def Dummy_function(i_halo):

        inputs = [ i_halo, i_snip_glob, subhalo_props, part_data, sim_props, modes, output_names]

        return main.Subhalo_Loop(inputs)

    output = Parallel(n_jobs=n_jobs_parallel)(delayed(Dummy_function)(i_halo) for i_halo in range(len(subhalo_props["group_number"])))
    
    if verbose:
        print "time spent on main loop", time.time() -time_main
    time_unpack = time.time()

    # Unpack the data
    time_track_subloop = 0.0
    for i_halo in range(len(subhalo_props["group_number"])):

        halo_mass_dict, timing_info = output[i_halo]
        
        time_track_select += timing_info[0]
        time_track_ptr += timing_info[1]
        time_track_subloop += timing_info[2]
        time_ptr_actual += timing_info[3]

        for name in output_mass_names:
            output_mass_dict[name][i_halo] += halo_mass_dict[name]

    print "time spent unpacking the data", time.time() - time_unpack

    ih_choose = np.argmax(mchalo_list)



    ################### Write output data to disk #############################################
    

    output_group_str = "Measurements"
    if output_group_str not in File:
        output_group = File.create_group(output_group_str)
    else:
        output_group = File[output_group_str]

    for dset in output_mass_names:
        for thing in output_group:
            if thing == dset:
                del output_group[dset]

    for name in output_mass_names:
        output_group.create_dataset(name, data=output_mass_dict[name])


    File.close()

    print "computing / write time", time.time()-t4
    print "select,matching block,ptr (actual), subloop time", time_track_select, time_track_ptr, time_ptr_actual, time_track_subloop
    print "total time for this snapshot", time.time() - t1

    # If _ns will be _ts on the next step, we have the option to retain the particle data
    if d_snap_ns == 1 and retain_memory:
        retain_ns = True
    else:
        retain_ns = False

    if retain_memory:
        retain_ts = True

print "Total time for the program in minutes", (time.time() - t0)/60.0
