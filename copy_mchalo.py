import numpy as np
from os import listdir
import h5py
import load_trees as lt
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os

#sim_name = "L0100N1504_REF_200_snip"
#subfind_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/"
#tree_path = subfind_path
#nsnap =200

# 50 Mpc reference run, 28 snapshots
#sim_name = "L0050N0752_REF_snap"
#nsnap = 28
#tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/"
#subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/subfind/"

#sim_name = "L0025N0376_REF_snap"
#subfind_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"
#tree_path = subfind_path
#nsnap =28

#sim_name = "L0025N0376_REF_50_snip"
#subfind_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/"
#tree_path = subfind_path
#nsnap =50

#sim_name = "L0025N0376_REF_100_snip"
#subfind_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/"
#tree_path = subfind_path
#nsnap =100

#sim_name = "L0025N0376_REF_200_snip"
#subfind_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/"
#tree_path = subfind_path
#nsnap =200

#sim_name = "L0025N0376_REF_300_snap"
#subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/"
#tree_path = subfind_path
#nsnap =299

#sim_name = "L0025N0376_REF_400_snap"
#subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapsho#ts/trees/treedir_399/"
#tree_path = subfind_path
#nsnap =399

#sim_name = "L0025N0376_REF_500_snap"
#subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/"
#tree_path = subfind_path
#nsnap =499

sim_name = "L0025N0376_REF_1000_snap"
subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/"
tree_path = subfind_path
nsnap =999

#sim_name = "L0050N0752_CFB_snap"
#subfind_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0050N0752/EagleVariation_fE1p00_ALPHA1p0e3/trees/treedir_028/"
#tree_path = subfind_path
#nsnap =28

#subfind_path = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/subfind/"
#tree_path = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/"
#nsnap = 28
#sim_name = "L0050N0752_NOAGN_snap"

#sim_name = "L0025N0752_Recal"
#subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/"
#tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/"
#nsnap = 202

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
#output_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

# Needed subhalo/tree data
subhalo_quantity_list = []
part_index =            []
subhalo_type_list =     []
tree_quantity_list = ["nodeIndex","snapshotNumber"]
tree_type_list = ["int","int"]

# Load subhalo/tree information
output_list, expansion_factor, snapshots = lt.Load_Trees(subfind_path, tree_path, nsnap, subhalo_quantity_list, subhalo_type_list, part_index, tree_quantity_list, tree_type_list)

# Unpack the data
(node_index, snap_num) = output_list

print "reading mchalo"

# Load mchalo
preprocess_File = h5py.File(input_path+"trees_preprocess_"+sim_name+".hdf5","r")

mchalo = preprocess_File["mchalo"][:] # mdm but with max past mass set for satellites
preprocess_File.close()

print "done"

# Load subhalo catalogue we want to write mchalo to
filename = "subhalo_catalogue_" + sim_name 
filename += ".hdf5"
File = h5py.File(output_path+filename,"a")

snap_list = np.arange(0,nsnap+1)

print "reading",output_path+filename

for snap in snap_list:
    try:
        snap_group = File["Snap_"+str(snap)]
    except:
        print "no subhalo catalogue data for snapshot",snap
        continue

    print "copying mchalo for snap", snap

    subhalo_group = snap_group["subhalo_properties"]

    try:
        node_index_sub = subhalo_group["nodeIndex"][:]
        group_number_sub = subhalo_group["GroupNumber_hydro"][:]
        subgroup_number_sub = subhalo_group["SubGroupNumber_hydro"][:]
    except:
        print "skipping this snapshot"
        continue

    mchalo_sub = np.zeros_like(node_index_sub).astype("float64")

    ok_ts = snap_num == snap
    if len(snap_num[ok_ts]) > 0:
        ptr = ms.match(node_index_sub, node_index[ok_ts])
        ok_match = ptr >= 0
        
        if -1 in ptr:
            print "Warning",len(ptr[ptr==-1]),"mchalo couldn't be matched"

        mchalo_sub[ok_match] += mchalo[ok_ts][ptr][ok_match]

    sat = (subgroup_number_sub > 0)
    cent = (subgroup_number_sub == 0)

    mhhalo_sub = np.copy(mchalo_sub)
    ptr = ms.match(group_number_sub[sat], group_number_sub[cent])
    ok_match = ptr >= 0

    temp = mhhalo_sub[sat]
    temp[ok_match] = mchalo_sub[cent][ptr][ok_match]
    mhhalo_sub[sat] = temp

    if "mchalo" in subhalo_group:
        del subhalo_group["mchalo"]

    if "mhhalo" in subhalo_group:
        del subhalo_group["mhhalo"]

    dset = subhalo_group.create_dataset("mchalo", data=mchalo_sub)
    dset = subhalo_group.create_dataset("mhhalo", data=mhhalo_sub)

File.close()
