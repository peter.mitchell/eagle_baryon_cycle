import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

test_mode = False
cumulative_pp = False
use_aperture = True
do_t_ret = False
compute_medians = False # Compute a few medians, as well as means
do_dm = True
extra_accretion_measurements = False
rmax_measurements = False

# Only include particles with a stellar mass above some value (if use apertue this stellar mass will be the 30kpc aperture mass)
# If you want no selection on stellar mass then set to a negative number
mstar_cut = -1e9
#mstar_cut = 0.0
#mstar_cut = 1.81e7 # 10 particles at standard Eagle resolution (assuming stars have starting gas mass, which they won't)

#star_mass = "n_part" # in this mode, we just measure particle numbers, not particle masses
star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

if star_mass=="n_part":
    print "In this case, I need to remove hard-coded particle masses (for recal)"
    quit()

halo_mass = "m200"

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")

# 50 Mpc REF with 28 snaps
'''sim_name = "L0050N0752_REF_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 28 snaps
'''sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_snap_par_test"
#sim_name = "L0025N0376_REF_snap_par_test2"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''


# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
snap_final = 100
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/tree_100.0.hdf5"
snip_list = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
               73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
               145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
               217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
               289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
               361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip_smthr_abshi"
#sim_name = "L0025N0376_REF_200_snip_smthr_frac"
#sim_name = "L0025N0376_REF_200_snip_m200_smthr_abshi"
#sim_name = "L0025N0376_REF_200_snip_not_bound_only"
#sim_name = "L0025N0376_REF_200_snip_track_star"
snap_final = 200
snap_list = np.arange(1,snap_final+1)[::-1] # These trees start at 1
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]'''

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/tree_299.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 400 snapshots
'''sim_name = "L0025N0376_REF_400_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/tree_399.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 500 snapshots
'''sim_name = "L0025N0376_REF_500_snap"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/tree_499.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 1000 snapshots
'''sim_name = "L0025N0376_REF_1000_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/tree_999.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 50 Mpc no-AGN 28 snaps
'''sim_name = "L0050N0752_NOAGN_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.copy(snap_list)'''

# 25 Mpc - recal (high-res), 202 snipshots
'''#sim_name = "L0025N0752_Recal"
#sim_name = "L0025N0752_Recal_m200_smthr_abslo"
sim_name = "L0025N0752_Recal_m200_smthr_abshi"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/tree_202.0.hdf5"
snap_final = 202
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snip_list = np.rint(snipshots[::-1]).astype("int")
snap_list = np.rint(snapshots[::-1]).astype("int")'''

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()

omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units (AT FIDUCIAL EAGLE RESOLUTION ONLY!) - this is only used if we are using particles numbers (ala Neistein) instead of masses to circumvent stellar recycling
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()

lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

# Compute the median expansion factor for each bin
jimbo = [[]]
counter = 0
for i_snap, snap in enumerate(snap_list[:-1]):

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    if ind_bin_snap > len(jimbo)-1:
        jimbo.append([])
        
    jimbo[ind_bin_snap].append(a[i_snap])

bin_a_median = np.zeros(len(lna_mid_choose))
for n in range(len(lna_mid_choose)):
    bin_a_median[n] = np.median(jimbo[n])
print "median expansion factor for each redshift bin", bin_a_median

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

bin_mh_r200_med = np.zeros((len(a_max), len(bin_mh_mid)))
bin_mh_vmax_med = np.zeros((len(a_max), len(bin_mh_mid)))

name_list = ["mchalo","subgroup_number","m200_host","r200_host","vmax"]
name_list += ["mass_gas_SF", "mass_gas_NSF", "mass_star", "mass_new_stars_init", "mass_star_init"]
name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]

name_list += ["mass_gas_NSF_ISM"]

name_list += ["mass_prcooled", "mass_galtran", "mass_praccreted"]
name_list += ["mass_fof_transfer","mass_reaccreted_hreheat","mass_recooled_ireheat"]
name_list += ["mass_galtran_from_SF", "mass_prcooled_SF"]
name_list += ["mass_hmerged","mass_gmerged_ISM","mass_gmerged_CGM"]

if do_dm:
    name_list += ["mass_diffuse_dm_accretion", "mass_hmerged_dm"]
    name_list += ["mass_diffuse_dm_pristine"]
name_list += ["mass_cgm_pristine"]
name_list += ["mass_praccreted_TLo", "mass_fof_transfer_TLo"]

#name_list += ["mass_new_stars_init_prcooled", "mass_new_stars_init_galtran", "mass_new_stars_init_recooled", "mass_new_stars_init_recooled_from_mp", "mass_new_stars_init_merge"]
#name_list += ["mass_smacc_sat"]

if extra_accretion_measurements:
    name_list += ["mass_galtran_mchalo_ratio", "mass_fof_transfer_mchalo_ratio"]
    if rmax_measurements:
        name_list += ["n_r_wind", "n_r_ejecta","n_r_wind_ret","n_r_ejecta_ret"]

if do_t_ret:
    name_list += ["mass_t_ejecta","mass_t_wind","mass_t_ejecta_ret","mass_t_wind_ret"]

fVmax_cuts = [0.25]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    name_list += ["mass_inside"+fV_str,"mass_outside_galaxy"+fV_str,"mass_outside"+fV_str]
    
    name_list += [ "mass_recooled_from_SF"+fV_str] # Note a non shell measurment has to go last!
    name_list += [ "mass_recooled_from_mp"+fV_str, "mass_reaccreted_from_mp"+fV_str]
    name_list += ["mass_reaccreted_TLo"+fV_str, "mass_reaccreted_TLo_from_mp"+fV_str]

fVmax_cuts_full = [0.125, 0.25, 0.5]
for fVmax_cut in fVmax_cuts_full:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    
    name_list += [ "mass_recooled"+fV_str, "mass_reaccreted"+fV_str]

data_list = []
dt_list = []
t_list = []
tret_bins_list = []

for lna in lna_mid_choose:
    dt_list.append([])
    t_list.append([])
    tret_bins_list.append(np.zeros(11)) # This assumes there are 10 tret bins!
    data_list.append({})
    for name in name_list:
        data_list[-1][name] = []

f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","facc_pristine_subnorm","facc_fof_subnorm","facc_merge_subnorm"]
f_name_list += ["fcool_pristine_mcgm_norm","fcool_pristine_mpristine_norm","fcool_pristine_mpristine_norm_tscaled","fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm"]

f_name_list += ["fcool_galtran_mcgm_norm"]

f_name_list += ["fcool_pristine_subnorm_tscaled", "fcool_galtran_subnorm_tscaled", "facc_pristine_subnorm_tscaled"]
f_name_list += ["facc_fof_subnorm_tscaled", "facc_merge_subnorm_tscaled", "fcool_merge_ism_subnorm_tscaled", "fcool_merge_cgm_subnorm_tscaled"]

#f_name_list += ["sfr_prcooled_subnorm", "sfr_recooled_subnorm", "sfr_galtran_subnorm", "sfr_merge_subnorm", "sfr_recooled_from_mp_subnorm"]
#f_name_list += ["facc_sat_subnorm"]

if do_dm:
    f_name_list += ["facc_dm_merge_dmnorm","facc_dm_dmnorm", "facc_pristine_prevent","fcool_pristine_prevent"]
    f_name_list += ["facc_dm_pristine_dmnorm", "facc_pristine_prevent_prdm","fcool_pristine_prevent_prdm"]
    f_name_list += ["facc_dm_merge_dmnorm_tscaled","facc_dm_dmnorm_tscaled"]

f_name_list += ["facc_pristine_cool_subnorm", "facc_fof_cool_subnorm", "fcool_pristine_cool_subnorm", "fcool_galtran_cool_subnorm"]

if extra_accretion_measurements:
    f_name_list += ["fret_galtran_mchalo_ratio", "fret_fof_transfer_mchalo_ratio"]
    if rmax_measurements:
        f_name_list += ["f_r_wind", "f_r_ejecta","f_r_wind_ret","f_r_ejecta_ret"]

if do_t_ret:
    f_name_list += ["fret_ism_t_dist", "fret_halo_t_dist", "fej_ism_t_dist", "fej_halo_t_dist"]

for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list += ["facc_reaccreted_return"+fV_str,"fcool_recooled_return"+fV_str, "facc_reaccreted_return_tscaled_"+fV_str,"fcool_recooled_return_tscaled_"+fV_str]

    f_name_list += ["fcool_recooled_from_mp_subnorm"+fV_str, "facc_reaccreted_from_mp_subnorm"+fV_str, "fcool_recooled_from_mp_return_tscaled_"+fV_str, "facc_reaccreted_from_mp_return_tscaled_"+fV_str]

    f_name_list += ["facc_reaccreted_cool_subnorm"+fV_str, "fcool_recooled_cool_subnorm"+fV_str]

    f_name_list += ["fcool_recooled_subnorm_tscaled"+fV_str, "facc_reaccreted_subnorm_tscaled"+fV_str]
    f_name_list += ["fcool_recooled_mcgm_norm"+fV_str, "fcool_recooled_minside_norm"+fV_str]
    f_name_list += ["fcool_recooled_minside_norm_tscaled"+fV_str]
    
for fVmax_cut in fVmax_cuts_full:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    
    f_name_list += ["fcool_recooled_subnorm"+fV_str, "facc_reaccreted_subnorm"+fV_str]

f_med_dict = {}

for f_name in f_name_list:
    
    if "_out" in f_name:
        n_rbins = 10
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
    elif "t_dist" in f_name or "fret" in f_name:
        n_tbins = 10
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_tbins))
    elif "f_r" in f_name:
        n_rbins = 13
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
    else:
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

f_med_dict2 = {}
f_name_list2 = ["bin_mh_med", "mstar_init_med"]
if compute_medians:
    f_name_list2 += ["facc_tot_med"]
    if do_dm:
        f_name_list2 += ["facc_dm_tot_med"]
for f_name in f_name_list2:
    f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))


filename = "processed_subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

print "reading data"

# For return time distributions, the bins change from snapshot to snapshot, so we need to define an average set of bins across for each redshift bin
if do_t_ret:

    count = 1
    prev = -1

    count_list = []
    for i_snap, snap in enumerate(snap_list[:-1]):
        if i_snap == 0:
            continue

        ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))
        
        try:
            subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
        except:
            continue

        if name_list[-1] in subhalo_group:
            tret_bins = subhalo_group["tret_bins"][:]

        tret_bins_list[ind_bin_snap] += tret_bins

        if ind_bin_snap == prev:
            count += 1
        else:
            tret_bins_list[ind_bin_snap-1] *= 1./count
            count = 1
            
        if snap == snap_list[-2]:
            tret_bins_list[ind_bin_snap] *= 1./count
        prev = ind_bin_snap

for i_snap, snap in enumerate(snap_list[:-1]):

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    if name_list[-1] in subhalo_group:

        if do_t_ret:
            tret_bins_ts = subhalo_group["tret_bins"][:]
            tret_bins_av = tret_bins_list[ind_bin_snap]

            tret_ts_mid = 0.5*(tret_bins_ts[1:] + tret_bins_ts[0:-1])
            av_ind = []
            tret_ok = np.zeros_like(tret_bins_ts[1:])<0
            for i_ts, tret in enumerate(tret_ts_mid):
                for i_av in range(len(tret_bins_av[1:])):
                    if tret > tret_bins_av[i_av] and tret < tret_bins_av[i_av+1]:
                        av_ind.append(i_av)
                        tret_ok[i_ts] = True

        #All galaxies on this time bin have the same value of ts - I just need to know which bin that fills in within av

        data = subhalo_group[name_list[-1]][:]

        if use_aperture:
            mstars = subhalo_group["mass_stars_30kpc"] # Msun
        else:
            mstars = subhalo_group["mass_star"] # Msun

        ok = (np.isnan(data)==False) & (mstars>mstar_cut)

        for i_name, name in enumerate(name_list):

            data = subhalo_group[name][:]
            if name == "Group_R_Crit200_host" or name == "r200_host":
                data *= a[i_snap] # pMpc h^-1
            ngal_tot = len(data)

            if len(data.shape) == 1:
                data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])
            elif len(data.shape) == 2:

                # Rebin tret histograms to a common time grid across the redshift bin
                if name == "mass_t_ejecta" or name == "mass_t_wind" or name == "mass_t_ejecta_ret" or name == "mass_t_wind_ret":
                    temp = np.zeros_like(data)
                    temp[:,av_ind] = data[:,tret_ok]
                    data = temp

                if data_list[ind_bin_snap][name] == []:
                    data_list[ind_bin_snap][name] = np.zeros((0, data.shape[1] ))
                data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok],axis=0)
            else:
                print "yikeso"
                quit()
            
        ngal = len(data[ok])
        print ngal, ngal_tot

        dt = t[i_snap] - t[i_snap+1]
        dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)
        t_list[ind_bin_snap] = np.append(t_list[ind_bin_snap], np.zeros(ngal)+t[i_snap])

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])
    else:
        print "no data for this snap"


for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]
    t_gal = t_list[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue
               

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = (data["mass_gas_NSF"]+data["mass_gas_SF"]+data["mass_star"]) >= 0
    dt = dt[ok_measure]
    t_gal = t_gal[ok_measure]
    for name in name_list:
        data[name] = data[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22

    if use_aperture:
        sfr = data["mass_new_stars_init_100_30kpc"] / 100 / 1e6 # Msun yr^-1
        mstars = data["mass_stars_30kpc"] # Msun
    else:
        sfr = data["mass_new_stars_init"] / dt * 1e-9 # Msun yr^-1
        mstars = data["mass_star"]

    ssfr = sfr / mstars * 1e9 # Gyr^-1

    R200 = data["r200_host"] /h * 1e3

    vmax = data["vmax"] # kms
    
    mchalo = data["mchalo"]
    if halo_mass == "m200":
        print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["m200_host"]
    else:
        mchalo *= dm2tot_factor

    mstar_init = data["mass_star_init"]

    # It's probably a better solution to output mass rather than particle numbers 
    if star_mass == "n_part":
        for name in name_list:
            if "n_" in name:
                data[name] *= mgas_part * 1.989e43 * g2Msun / h

    subgroup_number = data["subgroup_number"]

    #### Compute some percentile efficiencies ########
    cent = subgroup_number == 0
    weight_cent = np.zeros_like(dt[cent])
    dt_u = np.unique(dt)
    for dt_u_i in dt_u:
        ok2_cent = dt[cent] == dt_u_i
        weight_cent[ok2_cent] = dt[cent][ok2_cent] / len(dt[cent][ok2_cent])

    y = np.log10(mchalo[cent])
    f_med_dict2["bin_mh_med"][i_bin], jk,jk,jk,jk = us.Calculate_Percentiles(bin_mh, y, y, np.ones_like(weight_cent),5,lohi=(0.16,0.84),complete=0.8)

    y = mstar_init[cent]
    f_med_dict2["mstar_init_med"][i_bin], jk,jk,jk,jk = us.Calculate_Percentiles(bin_mh, np.log10(mchalo[cent]), y, np.ones_like(weight_cent),5,lohi=(0.16,0.84),complete=0.8)

    if compute_medians:
        if do_dm:
            y = data["mass_hmerged_dm"] + data["mass_diffuse_dm_accretion"]
            f_med_dict2["facc_dm_tot_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh, np.log10(mchalo[cent]), y[cent] / dt[cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

        fV_str = "_"+str(0.25).replace(".","p")+"vmax"
        y = data["mass_reaccreted"+fV_str] + data["mass_praccreted"] + data["mass_fof_transfer"] + data["mass_hmerged"]

        f_med_dict2["facc_tot_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh, np.log10(mchalo[cent]), y[cent] / dt[cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

    bin_mh_r200_med[i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh, np.log10(mchalo[cent]), R200[cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
    bin_mh_vmax_med[i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh, np.log10(mchalo[cent]), vmax[cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)


    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)

        weight_cent = np.zeros_like(dt[ok_cent])
        
        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            
        mass_new_stars = data["mass_new_stars_init"]

        for f_name in f_name_list:
            
            for fVmax_cut in fVmax_cuts_full:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                if fV_str in f_name:
                    break

            if "fcool_pristine" in f_name:
                mass_out = data["mass_prcooled"]
                #if "_cool_" in f_name:
                #    mass_out = data["mass_prcooled_TLo"]
            elif "fcool_recooled" in f_name:
                mass_out = data["mass_recooled"+fV_str]
                if "from_mp" in f_name:
                    mass_out = data["mass_recooled_from_mp"+fV_str]
                #if "_cool_" in f_name:
                #    mass_out = data["mass_recooled_TLo"+fV_str]
            elif "fcool_galtran" in f_name:
                mass_out = data["mass_galtran"]
                #if "_cool_" in f_name:
                #    mass_out = data["mass_galtran_TLo"]
            elif "fcool_merge_ism" in f_name:
                mass_out = data["mass_gmerged_ISM"]
            elif "fcool_merge_cgm" in f_name:
                mass_out = data["mass_gmerged_CGM"]
            elif "facc_pristine" in f_name:
                mass_out = data["mass_praccreted"]
                if "_cool_" in f_name:
                    mass_out = data["mass_praccreted_TLo"]
            elif "facc_reaccreted" in f_name:
                mass_out = data["mass_reaccreted"+fV_str]
                if "from_mp" in f_name:
                    mass_out = data["mass_reaccreted_from_mp"+fV_str]
                if "_cool_" in f_name:
                    mass_out = data["mass_reaccreted_TLo"+fV_str]

            elif "facc_fof" in f_name:
                mass_out = data["mass_fof_transfer"]
                if "_cool_" in f_name:
                    mass_out = data["mass_fof_transfer_TLo"]
            elif "facc_merge" in f_name:
                mass_out = data["mass_hmerged"]
            elif "facc_sat" in f_name:
                mass_out = data["mass_smacc_sat"]
            elif "facc_dm_merge" in f_name:
                mass_out = data["mass_hmerged_dm"]
            elif "facc_dm" in f_name:
                if "pristine" in f_name:
                    mass_out = data["mass_diffuse_dm_pristine"]
                else:
                    mass_out = data["mass_diffuse_dm_accretion"]
            elif "sfr_prcooled" in f_name:
                mass_out = data["mass_new_stars_init_prcooled"]
            elif "sfr_recooled_from_mp" in f_name:
                mass_out = data["mass_new_stars_init_recooled_from_mp"]
            elif "sfr_recooled" in f_name:
                mass_out = data["mass_new_stars_init_recooled"]
            elif "sfr_galtran" in f_name:
                mass_out = data["mass_new_stars_init_galtran"]
            elif "sfr_merge" in f_name:
                mass_out = data["mass_new_stars_init_merge"]
            else:
                if f_name != "aform_bh" and f_name != "ism_wind_cum_ml" and "_injection_rate" not in f_name and f_name != "f_dir_heated_mass_in_ISM" and "_out" not in f_name and "t_dist" not in f_name and "f_r" not in f_name and "fret" not in f_name:
                    print "No", f_name
                    quit()

            ##################################################################

            if "subnorm" in f_name:
                if "tscaled" in f_name:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent*t_gal[ok_cent]) / np.sum(fb*mchalo[ok_cent]*weight_cent) # Gyr^-1
                else:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(fb*mchalo[ok_cent]*weight_cent) # Gyr^-1
            elif "dmnorm" in f_name:
                if "tscaled" in f_name:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent*t_gal[ok_cent]) / np.sum((1-fb)*mchalo[ok_cent]*weight_cent) # Gyr^-1
                else:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum((1-fb)*mchalo[ok_cent]*weight_cent) # Gyr^-1

            elif "mcgm_norm" in f_name:
                mass_cgm = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm[ok_cent]*weight_cent) # Gyr^-1

            elif "minside_norm" in f_name:
                mass_inside = data["mass_inside"+fV_str]
                if "tscaled" in f_name:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent*t_gal[ok_cent]) / np.sum(mass_inside[ok_cent]*weight_cent) # Gyr^-1
                else:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_inside[ok_cent]*weight_cent) # Gyr^-1

            elif "mpristine_norm" in f_name:
                mass_cgm = np.copy(data["mass_cgm_pristine"])
                if "tscaled" in f_name:
                    mass_cgm *= 1./t_gal
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm[ok_cent]*weight_cent) # Dimensionless


            elif "_prevent" in f_name:

                if "prdm" in f_name:
                    norm = data["mass_diffuse_dm_pristine"] * dm2tot_factor*fb
                else:
                    norm = data["mass_diffuse_dm_accretion"] * dm2tot_factor*fb
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent) # dimensionless

            elif "_return" in f_name:
                if "cool" in f_name:
                    norm = np.copy(data["mass_outside_galaxy"+fV_str])
                elif "acc" in f_name:
                    norm = np.copy(data["mass_outside"+fV_str])
                else:
                    print "yikes", f_name
                    quit()

                if "tscaled" in f_name:
                    norm *= 1./t_gal # Msun Gyr^-1
                

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]*weight_cent)

            elif "t_dist" in f_name:
                for i_t in range(len(tret_bins_list[i_bin][1:])):
                    
                    if f_name == "fret_ism_t_dist":
                        quant = data["mass_t_wind_ret"]
                    elif f_name == "fret_halo_t_dist":
                        quant = data["mass_t_ejecta_ret"]
                    elif f_name == "fej_ism_t_dist":
                        quant = data["mass_t_wind"]
                    elif f_name == "fej_halo_t_dist":
                        quant = data["mass_t_ejecta"]
                    else:
                        print "hmm", f_name
                        quit()

                    if "fret" in f_name:
                        f_med_dict[f_name][i_bin][j_bin][i_t] = np.sum(quant[:,i_t][ok_cent]/dt[ok_cent]*weight_cent) / np.sum(weight_cent) # Msun Gyr^-1
                    else:
                        f_med_dict[f_name][i_bin][j_bin][i_t] = np.sum(quant[:,i_t][ok_cent]*weight_cent) / np.sum(weight_cent) # Msun


            elif "fret" in f_name:
                for i_m in range(10):
                    if f_name == "fret_galtran_mchalo_ratio":
                        quant = data["mass_galtran_mchalo_ratio"]
                    elif f_name == "fret_fof_transfer_mchalo_ratio":
                        quant = data["mass_fof_transfer_mchalo_ratio"]
                    else:
                        print "Hmmmm", f_name; quit()

                    f_med_dict[f_name][i_bin][j_bin][i_m] = np.sum(quant[:,i_m][ok_cent]/dt[ok_cent]*weight_cent) / np.sum(weight_cent) # Msun Gyr^-1


            elif "f_r" in f_name:
                for i_r in range(13):
                    
                    if f_name == "f_r_wind":
                        quant = data["n_r_wind"]
                    elif f_name == "f_r_ejecta":
                        quant = data["n_r_ejecta"]
                    elif f_name == "f_r_wind_ret":
                        quant = data["n_r_wind_ret"]
                    elif f_name == "f_r_ejecta_ret":
                        quant = data["n_r_ejecta_ret"]
                    else:
                        print "hmm", f_name
                        quit()

                    if "_ret" in f_name:
                        f_med_dict[f_name][i_bin][j_bin][i_r] = np.sum(quant[:,i_r][ok_cent]/dt[ok_cent]*weight_cent) / np.sum(weight_cent) # Msun Gyr^-1
                    else:
                        f_med_dict[f_name][i_bin][j_bin][i_r] = np.sum(quant[:,i_r][ok_cent]*weight_cent) / np.sum(weight_cent) # Msun

            else:

                print "nope", f_name
                quit()


if cumulative_pp:
    fVmax_cut = 0.25
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    f_name_list_cum = ["fprcooled_cum_pp","frecooled_cum_pp"+fV_str,"frecooled_from_mp_cum_pp"+fV_str,"fgaltran_cum_pp", "fmerge_ism_cum_pp", "fmerge_cgm_cum_pp", "fpracc_cum_pp", "frecooled_ireheat_cum_pp"]

    #f_name_list_cum += ["sfr_prcooled_cum_pp", "sfr_recooled_cum_pp", "sfr_recooled_from_mp_cum_pp", "sfr_galtran_cum_pp", "sfr_merge_cum_pp"]

    if do_dm:
        f_name_list_cum += ["fpracc_dm_cum_pp"]
    for f_name in f_name_list_cum:
        f_med_dict[f_name] = np.zeros(len(bin_mh)-1)

    subhalo_group = File["Snap_"+str(snap_list[1:].max())+"/subhalo_properties"]

    subgroup_number = subhalo_group["subgroup_number"][:]
    mchalo = subhalo_group["m200_host"][:]
    mrecooled = subhalo_group["mass_recooled_cumul_pp"+fV_str][:]
    mrecooled_ireheat = subhalo_group["mass_recooled_ireheat_cumul_pp"][:]
    mrecooled_from_mp = subhalo_group["mass_recooled_from_mp_cumul_pp"+fV_str][:]
    mprcooled = subhalo_group["mass_prcooled_cumul_pp"][:]
    mgaltran = subhalo_group["mass_galtran_cumul_pp"][:]
    mmerge_ism = subhalo_group["mass_merge_ism_cumul_pp"][:]
    mmerge_cgm = subhalo_group["mass_merge_cgm_cumul_pp"][:]

    '''sfr_prcooled = subhalo_group["sfr_prcooled_cumul_pp"][:]
    sfr_recooled = subhalo_group["sfr_recooled_cumul_pp"][:]
    sfr_recooled_from_mp = subhalo_group["sfr_recooled_from_mp_cumul_pp"][:]
    sfr_galtran = subhalo_group["sfr_galtran_cumul_pp"][:]
    sfr_merge = subhalo_group["sfr_merge_cumul_pp"][:]'''

    mpracc = subhalo_group["mass_pracc_cumul_pp"][:]
    if do_dm:
        mpracc_dm = subhalo_group["mass_pracc_dm_cumul_pp"][:]

    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])
        ok_cent = ok & (subgroup_number == 0)

        f_med_dict["fprcooled_cum_pp"][j_bin] = np.sum(mprcooled[ok_cent]) / len(mgaltran[ok_cent]) # Msun
        f_med_dict["frecooled_cum_pp"+fV_str][j_bin] = np.sum(mrecooled[ok_cent]) / len(mgaltran[ok_cent]) # Msun
        f_med_dict["frecooled_ireheat_cum_pp"][j_bin] = np.sum(mrecooled_ireheat[ok_cent]) / len(mgaltran[ok_cent]) # Msun
        f_med_dict["frecooled_from_mp_cum_pp"+fV_str][j_bin] = np.sum(mrecooled_from_mp[ok_cent]) / len(mgaltran[ok_cent]) # Msun
        f_med_dict["fgaltran_cum_pp"][j_bin] = np.sum(mgaltran[ok_cent]) / len(mgaltran[ok_cent]) # Msun
        f_med_dict["fmerge_ism_cum_pp"][j_bin] = np.sum(mmerge_ism[ok_cent]) / len(mgaltran[ok_cent]) # Msun
        f_med_dict["fmerge_cgm_cum_pp"][j_bin] = np.sum(mmerge_cgm[ok_cent]) / len(mgaltran[ok_cent]) # Msun

        '''f_med_dict["sfr_prcooled_cum_pp"][j_bin] = np.sum(sfr_prcooled[ok_cent]) / len(mgaltran[ok_cent])
        f_med_dict["sfr_recooled_cum_pp"][j_bin] = np.sum(sfr_recooled[ok_cent]) / len(mgaltran[ok_cent])
        f_med_dict["sfr_recooled_from_mp_cum_pp"][j_bin] = np.sum(sfr_recooled_from_mp[ok_cent]) / len(mgaltran[ok_cent])
        f_med_dict["sfr_galtran_cum_pp"][j_bin] = np.sum(sfr_galtran[ok_cent]) / len(mgaltran[ok_cent])
        f_med_dict["sfr_merge_cum_pp"][j_bin] = np.sum(sfr_merge[ok_cent]) / len(mgaltran[ok_cent])'''

        f_med_dict["fpracc_cum_pp"][j_bin] = np.sum(mpracc[ok_cent]) / len(mgaltran[ok_cent])
        if do_dm:
            f_med_dict["fpracc_dm_cum_pp"][j_bin] = np.sum(mpracc_dm[ok_cent]) / len(mgaltran[ok_cent])

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_inflows_only"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_msub_bins", data=bin_mh)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("a_median", data=bin_a_median)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

File_grid.create_dataset("R200_med", data = bin_mh_r200_med)
File_grid.create_dataset("vmax_med", data = bin_mh_vmax_med)

if do_t_ret:
    File_grid.create_dataset("tret_bins", data=np.array(tret_bins_list))

for f_name in f_name_list:
    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

if cumulative_pp:
    for f_name in f_name_list_cum:
        File_grid.create_dataset(f_name, data=f_med_dict[f_name])

for f_name in f_name_list2:
    File_grid.create_dataset(f_name, data=f_med_dict2[f_name])

File_grid.close()
