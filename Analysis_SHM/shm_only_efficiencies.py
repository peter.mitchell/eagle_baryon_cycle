# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us


sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

use_30kpc = False

variations =      ["",         "_only_prev_cgm",          "_only_tau_infall", "_only_tau_ret_gal",         "_only_tau_ret_halo",     "_only_eta_gal", "_only_eta_halo"]
variation_names = ["Fiducial", r"Halo-scale accretion ($f_{\mathrm{acc}}$)", r"Galaxy-scale accretion ($f_{\mathrm{infall}}$)", r"Galaxy-scale recycling ($f_{\mathrm{ret,gal}}$)", r"Halo-scale recycling ($f_{\mathrm{ret,halo}}$)", r"Galaxy-scale outflow ($\eta_{\mathrm{gal}}$)", r"Halo-scale outflow ($\eta_{\mathrm{halo}}$)"]
c_list =          ["k",        "b",                        "b",               "g",                         "g",                       "r",              "r"              ]
ls_list =         ["-",        "--",                       ":",               ":",                         "--",                      ":",              "--"             ]

variations += ["_only_tau_ret_both", "_only_eta_both",        "_fixed_everything", "_only_tau_SF", "_only_inflows"]
variation_names += [r"Recycling ($f_{\mathrm{ret,gal}}$,$f_{\mathrm{ret,halo}}$)", r"Outflow ($\eta_{\mathrm{gal}}$,$\eta_{\mathrm{halo}}$)", "All", r"Star formation ($f_{\mathrm{SF}}$)",  r"Accretion ($f_{\mathrm{acc}}$, $f_{\mathrm{infall}}$)"]
c_list +=     ["g",                   "r",                      "y",                 "m",             "b"           ]
ls_list +=    ["-",                    "-",                     "-",                 "-",             "-"           ]

panel1 = ["", "_fixed_everything", "_only_prev_cgm", "_only_tau_infall", "_only_inflows"]
panel2 = ["", "_fixed_everything", "_only_tau_ret_gal", "_only_tau_ret_halo", "_only_tau_ret_both"]
panel3 = ["", "_fixed_everything", "_only_eta_gal", "_only_eta_halo", "_only_eta_both"]
panel4 = ["", "_fixed_everything", "_only_inflows", "_only_tau_ret_both", "_only_eta_both", "_only_tau_SF"]

props = ["mass_stars_30kpc", "subgroup_number", "mass_star"]
props += ["m200_host"]

props_integrator = ["stars"]

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()

# Read integrator output, looping over variations
for variant in variations:

    filename2 = "integrator_output_"+sim_name +variant+".hdf5"
    print "Reading", filename2
    File2 = h5py.File(ode_path+filename2,"r")
    snapnum_group2 = File2["Snap_"+str(snap)]
    
    for prop in props_integrator:

        data[prop+variant] = snapnum_group2[prop][ok]

    File2.close()

print "plotting"

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':9})


py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))



for n,ax in enumerate(subplots):
    py.axes(ax)

    if use_30kpc:
        mstar = data["mass_stars_30kpc"]
    else:
        mstar = data["mass_star"]

    #med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),mstar/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    #py.plot(mid_bin, np.log10(med), c="k", label = sim_name,linewidth=1.2, alpha=0.5)
    #py.plot(mid_bin, np.log10(lo), c="k",linewidth=0.7, alpha=0.5)
    #py.plot(mid_bin, np.log10(hi), c="k",linewidth=0.7, alpha=0.5)

    if n == 0:
        panel_list = panel1
    if n == 1:
        panel_list = panel2
    if n == 2:
        panel_list = panel3
    if n == 3:
        panel_list = panel4

    for i, variant in enumerate(panel_list):
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data["stars"+variant]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        for i2, variant_i in enumerate(variations):
            if variant == variant_i:
                i_var = i2

        py.plot(mid_bin, np.log10(med), c=c_list[i_var], label = variation_names[i_var],linewidth=1.3,linestyle=ls_list[i_var])
    
    py.legend(loc="lower right",frameon=False,handlelength=3)
    py.xlim((10., 14.7))
    py.ylim((-3.,-1.5))

    if n == 0 or n ==2:
        py.ylabel(r"$\log_{10}(M_\star \, / M_{200})$")
    if n == 2 or n==3:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_{\odot}})$")
        

fig_name = "shm_only_efficiencies.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)


py.show()
