import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf
import utilities_statistics as us
import utilities_cosmology as uc

logm200_lo = 11.75
logm200_hi = 12.25

fVmax_cut = 0.25
fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

props_track = ["mass_star", "mass_gas_SF", "mass_gas_NSF", "mass_gas_NSF_ISM"]
props_track += ["mass_inside"+fV_str, "mass_outside"+fV_str, "mass_outside_galaxy"+fV_str]

catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"

h=0.6777
omm = 0.307
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

filename_subcat = filename

if not os.path.exists(catalogue_path+filename_subcat):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename_subcat
    quit()

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename,"r")

output = {}
output_z0 = {}
for prop in props_track:
    output[prop] = np.zeros(len(snapshots))

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "building subvolumes for tree snapshot", snap

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    node_index_ts = subhalo_group["node_index"][:]

    if i_snap == 1:

        sgrn = subhalo_group["subgroup_number"][:]
        m200 = subhalo_group["m200_host"][:]
        cent = sgrn==0
        select = cent & (np.log10(m200) > logm200_lo) & (np.log10(m200) < logm200_hi)

        n_z0 = len(select[select])
        
        node_index_ns = node_index_ts[select]
        for prop in props_track:
            output[prop][i_snap] = np.sum(subhalo_group[prop][select]) / n_z0

            output_z0[prop] = subhalo_group[prop][:][cent]

        continue

    descendant_index_ts = subhalo_group["descendant_index"][:]

    ptr = ms.match(descendant_index_ts, node_index_ns)
    match = ptr >= 0

    if len(match) == 0 or len(match[match]) == 0:
        break

    node_index_ns = node_index_ts[match]

    save = match & (subhalo_group["isInterpolated"][:]==0)
    for prop in props_track:
        output[prop][i_snap] = np.sum(subhalo_group[prop][save]) / n_z0

t_snapshots = t_snapshots[1:]
for prop in output:
    output[prop] = output[prop][1:]

star = output["mass_star"]
ism = output["mass_gas_SF"] + output["mass_gas_NSF_ISM"]
cgm_pr = output["mass_gas_NSF"] - output["mass_gas_NSF_ISM"] - output["mass_inside"+fV_str]
cgm_wind = output["mass_inside"+fV_str]
ej_pr = output["mass_outside"+fV_str] - output["mass_outside_galaxy"+fV_str] + output["mass_inside"+fV_str]
ej_wind = output["mass_outside_galaxy"+fV_str] - output["mass_inside"+fV_str]
tot = star + ism + cgm_pr + cgm_wind + ej_pr + ej_wind
norm = tot.max()
igm = norm - tot

star *= 1./norm
igm *= 1./norm
cgm_pr *= 1./norm
cgm_wind *= 1./norm
ej_pr *= 1./norm
ej_wind *= 1./norm
ism *= 1./norm

######### Plotting ################
print "Plotting"

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.2,
                    'figure.figsize':[3.32,2.49*2*1.1],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.08,
                    'figure.subplot.top':0.93,
                    'figure.subplot.right':0.97,
                    'axes.labelsize':9,
                    'legend.fontsize':9})

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        for tick in ax.xaxis.get_major_ticks():
            tick.label1On=True

        # IGM (before being accreted)
        py.fill_between( t_snapshots, np.zeros_like(igm), igm, color="b", alpha=0.7)

        # CGM_pr
        py.fill_between( t_snapshots, igm, igm+cgm_pr, color="c", alpha=0.7)
        # CGM_wind
        cumul = cgm_pr + igm
        py.fill_between( t_snapshots, cumul, cumul+cgm_wind, color="c", alpha=0.3)

        # ISM
        cumul += cgm_wind
        py.fill_between( t_snapshots, cumul, cumul+ism, color="g", alpha=0.7)

        # Stars
        cumul += ism
        py.fill_between( t_snapshots, cumul, cumul+star, color="k", alpha=0.7)

        # Ej_pr
        cumul += star
        py.fill_between( t_snapshots, cumul, cumul+ej_pr, color="r", alpha=0.7)

        # Ej_wind
        cumul += ej_pr
        py.fill_between( t_snapshots, cumul, cumul+ej_wind, color="r", alpha=0.3)

        py.xlabel(r"$t \, / \mathrm{Gyr}$")
        py.ylabel(r"$\mathrm{Fraction}$")

        xlo = 0.0; xhi = 14.0
        py.xlim((xlo,xhi))

        z_ticks = [0, 0.5, 1, 2, 3, 4, 6]
        n_ticks = len(z_ticks)
        tick_labels = []
        for m in range(n_ticks):
            tick_labels.append(str(z_ticks[m]))
        t_locs, junk = uc.t_Universe(1./(1.+np.array(z_ticks)),omm,h)
        
        ax_z = ax.twiny()
        ax_z.set_xticks(t_locs)
        ax_z.set_xticklabels(tick_labels)
        ax_z.set_xlabel(r'$z$')
        ax_z.set_xlim((xlo,xhi))

        py.annotate(r"Not yet accreted",(0.5,0.1),fontsize=9,color="w")
        py.annotate(r"Ejected $(>R_{200})$",(8.0,0.6),fontsize=9,color="k")
        py.annotate(r"CGM",(7.0,0.3),fontsize=9,color="k")
        py.annotate(r"Stars",(12.0,0.28),fontsize=9,color="w")
        py.annotate(r"ISM",(2.685,0.64),fontsize=9,color="k")
        py.plot([4.0,4.6],[0.66,0.67],c="k",linewidth=1.0)
        
    else:
        
        y = output_z0["mass_gas_NSF"] - output_z0["mass_gas_NSF_ISM"]
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(m200[cent]),y/m200[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        py.plot(mid_bin, np.log10(med), c="c", linewidth=1.2,label="CGM")

        y = output_z0["mass_gas_SF"] + output_z0["mass_gas_NSF_ISM"]
        y[y==0] = 1e-9
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(m200[cent]),y/m200[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        py.plot(mid_bin, np.log10(med), c="g", linewidth=1.2,label="ISM")

        y = output_z0["mass_star"]
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(m200[cent]),y/m200[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        py.plot(mid_bin, np.log10(med), c="k", linewidth=1.2,label="Stars")

        y = output_z0["mass_outside"+fV_str]
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(m200[cent]),y/m200[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        py.plot(mid_bin, np.log10(med), c="r", linewidth=1.2,label="Ejected")

        xlo = 10.0; xhi = 14.7
        py.xlim((xlo,xhi))

        ylo = -4.0; yhi = -0.5
        py.ylim((ylo,yhi))

        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$") 
        py.ylabel(r"$\log_{10}(M \, / M_{200})$")

        py.legend(loc="lower right",frameon=False)

fig_name = "total_evo_paper.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()


for n in range(30):
    if n < 5 or n > 10:
        continue
    print n
