# Note to self. This script contains a beatiful example of how to compute the mass enclosed with r for each particle,
# without using for loops. Be careful however, this trick relies on common precisions for the mass variable and
# any unit conversion variables. Otherwise you get significant roundoff errors. By default floats in python are
# initialised as 64 bit floats, but masses in Eagle are stored as 32 bit floats.

import numpy as np
import h5py
import sys
sys.path.append("../../.")
import measure_particles_io as mpio
import read_eagle as re
import match_searchsorted as ms
import utilities_cosmology as uc

'''sim_name = "L0025N0376_REF_snap"
tree_snap = 27; sim_snipsnap = 27
DATDIR = "/cosma7/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE/data/"'''

'''sim_name = "L0100N1504_REF_200_snip"
tree_snap = 199; sim_snipsnap = 404
DATDIR = "/cosma7/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/"'''

sim_name = "L0025N0376_REF_200_snip"
#tree_snap = 199; sim_snipsnap = 397
#tree_snap = 159; sim_snipsnap = 317 
#tree_snap = 135; sim_snipsnap = 269
#tree_snap = 100; sim_snipsnap = 199
tree_snap = 75; sim_snipsnap = 149
DATDIR = "/cosma7/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"

catalogue_dir = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

######## Read subhalo data ######################
catalogue_path = catalogue_dir + "processed_subhalo_catalogue_" + sim_name + ".hdf5"

cat_file = h5py.File(catalogue_path, "r")
snap_group = cat_file["Snap_"+str(tree_snap)]
subhalo_group = snap_group["subhalo_properties"]

names_read = ["x_halo", "y_halo", "z_halo", "group_number", "m200_host"]
subhalo_data = {}

cent = subhalo_group["subgroup_number"][:] == 0
for name in names_read:
    subhalo_data[name] = subhalo_group[name][:][cent]

cat_file.close()

####### Read particle data ######################
path, path_all = mpio.Get_Particle_Data_Directories(DATDIR,sim_name,sim_snipsnap)
if not "snap" in sim_name:
    egfile  = path+"/eagle_subfind_snip_particles"+path.split('particledata_snip')[-1]+'.0.hdf5'
else:
    egfile  = path+"/eagle_subfind_particles"+path.split('particledata')[-1]+'.0.hdf5'

sobj        = re.EagleSnapshot(egfile)
h5py_file = h5py.File(egfile,"r")
h = h5py_file["Header"].attrs["HubbleParam"]
omm = h5py_file["Header"].attrs["Omega0"]
if not "snap" in sim_name:
    redshift = float(path.split('particledata_snip_')[-1][5:].replace('p','.'))
else:
    redshift = float(path.split('particledata_')[-1][5:].replace('p','.'))
expansion_factor = 1. / (1.+redshift)
mass_dm = h5py_file["Header"].attrs["MassTable"][1]
h5py_file.close()

boxsize = sobj.boxsize # cMpc h^-1
vertices = (0.0 , boxsize, 0., boxsize, 0., boxsize)
sobj.select_region(*vertices)

part_names = ["Mass", "Coordinates", "GroupNumber"]
part_type = ["float64", "float64", "int32"]
# These are stored as 32bit float, coordinates as 64bit float, and group number as 32bit integer
# The masses not being 64bit caused me some trouble with unit conversions later - hence this change
part_data = {}

for name, type_i in zip(part_names, part_type):
    print "Reading", name
    part_data[name] = sobj.read_dataset(0, name).astype(type_i)

    #print "Skip non-gas"
    #continue
    if name == "Mass":
        n_dm = len(sobj.read_dataset(1, "GroupNumber"))
        part_data[name] = np.concatenate((part_data[name],np.zeros(n_dm)+mass_dm))
    else:
        part_data[name] = np.concatenate((part_data[name],sobj.read_dataset(1, name)))
    part_data[name] = np.concatenate((part_data[name],sobj.read_dataset(4, name)))
    part_data[name] = np.concatenate((part_data[name],sobj.read_dataset(5, name)))

part_data["GroupNumber"] = np.abs(part_data["GroupNumber"])

ptr = ms.match(part_data["GroupNumber"], subhalo_data["group_number"])
ok_match = ptr >= 0

for name in part_data:
    part_data[name] = part_data[name][ok_match]

def Periodic_Check(delta_x):
    corr_hi = delta_x > boxsize*0.5
    corr_lo = delta_x < -boxsize*0.5
    delta_x[corr_hi] -= boxsize
    delta_x[corr_lo] += boxsize
    return delta_x

part_dx = part_data["Coordinates"][:,0] - subhalo_data["x_halo"][ptr][ok_match]
part_dx = Periodic_Check(part_dx)

part_dy = part_data["Coordinates"][:,1] - subhalo_data["y_halo"][ptr][ok_match]
part_dy = Periodic_Check(part_dy)

part_dz = part_data["Coordinates"][:,2] - subhalo_data["z_halo"][ptr][ok_match]
part_dz = Periodic_Check(part_dz)

part_r = np.sqrt(np.square(part_dx) + np.square(part_dy) + np.square(part_dz) )

# Hmm to compute r_vir I now need to know the mass enclosed for each particle
# Is this possible without a for loop? (probably not)

index_r = part_data["GroupNumber"]*100 + part_r
order = np.argsort(index_r)

part_r = part_r[order]
for name in part_data:
    part_data[name] = part_data[name][order]

part_grn_u, index, counts = np.unique(part_data["GroupNumber"], return_index=True, return_counts=True)

part_m_cum = np.cumsum(part_data["Mass"])
part_m_cum_halo = part_m_cum[index] # Total mass preceeding a given subhalo

part_m_correct = np.repeat(part_m_cum_halo, counts)
part_m_correct -= np.repeat(part_data["Mass"][index], counts)

# Mass enclosed in each halo - look Mum no for loops! :-)
part_m_enclosed = part_m_cum - part_m_correct

volume_enclosed = 4*np.pi/3. * np.power(expansion_factor*part_r/h, 3) # pMpc^3
mean_density_enclosed = part_m_enclosed*1e10/h  / volume_enclosed # Msun pMpc^-3

# Now we compute the Bryan and Norman overdenisty threshold for this redshift
oml = 1-omm
E_o_z_squared = omm*(1+redshift)**3 + oml
Omega = omm * (1+redshift)**3 / E_o_z_squared
x = Omega-1
Delta_c = 18*np.pi**2 +60*x -32 *x**2 # Their equation 6

# Compute the critical density for this redshift
rho_crit = uc.Critical_Density(redshift,h,omm,oml) # Msun pMpc^-3

mean_density_enclosed *= 1./(rho_crit*Delta_c)



# Ok so for each halo we need to find the particle where this quantity is closest to one
# In cases where this happens more than once, pick the closest
ratio_before = mean_density_enclosed[0:-1]
ratio_after = mean_density_enclosed[1:]

virial_pos = (ratio_before > 1) & (ratio_after<1)

mvir_temp = part_m_enclosed[1:][virial_pos]
grn_temp = part_data["GroupNumber"][1:][virial_pos]

grn_cent, index_u = np.unique(grn_temp, return_index=True)
mvir_cent = mvir_temp[index_u] * 1e10/h

# Now account for if some of the haloes don't have a successful Mvir measurement
mvir = np.zeros(len(cent)) + np.nan
mvir_match = np.copy(mvir[cent])
grn_match = subhalo_data["group_number"] # Note this was already reduced to centrals only
ptr = ms.match(grn_match, grn_cent)
ok_match = ptr >= 0
mvir_match[ok_match] = mvir_cent[ptr][ok_match]
mvir[cent] = mvir_match


cat_file = h5py.File(catalogue_path, "a")
snap_group = cat_file["Snap_"+str(tree_snap)]
subhalo_group = snap_group["subhalo_properties"]

name = "Mvir_BN98"

if name in subhalo_group:
    del subhalo_group[name]


subhalo_group.create_dataset(name, data=mvir)

cat_file.close()


'''import matplotlib.pyplot as py

py.scatter( np.log10(subhalo_group["m200_host"][:]), np.log10(mvir), edgecolors="none", s=10)
py.show()

cat_file.close()'''
