import numpy as np

answer = np.array([1.5, 2.3, 3.3, 0.8, 1.8, 1, 2, 1])
g = np.array([0, 0, 0, 1, 1, 2, 2, 3])
m = np.array([1.5, 0.8, 1, 0.8, 1, 1, 1, 1])

junk, index, counts = np.unique(g, return_index=True, return_counts=True)
#index = [0, 3, 5, 7]
#counts = [3, 2, 2, 1]

mcum = np.cumsum( m )
#mcum = [1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5]

mcum_halo = mcum[index]
#mcum_halo = [1.5, 4.5, 6.5, 8.5]

mcum_correct = np.repeat(mcum_halo,counts)
#mcum_correct = [1.5, 1.5, 1.5, 4.5, 4.5, 6.5, 6.5, 8.5]

mcum_correct -= np.repeat(m[index], counts)

m_enclosed = mcum - mcum_correct 
#m_correct = [1.5, 2.5, 3.5, 1, 2, 1, 2, 1]
print m_enclosed

print "Answer"
print answer
