import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star","mass_star_init"]
props += ["m200_host","isInterpolated"]

props_integrator = ["stars","stars_init","stars_init_cent", "stars_init_cent_actual", "stars_init_actual"]

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()

filename2 = "integrator_output_"+sim_name+".hdf5"
File2 = h5py.File(ode_path+filename2,"r")
snapnum_group2 = File2["Snap_"+str(snap)]

for prop in props_integrator:

    data[prop] = snapnum_group2[prop][ok]

File2.close()

'''order = np.argsort(data["m200_host"])[::-1]
print data["subgroup_number"][order][0:50]
print ""
print data["stars_init_cent"][order][0:50]
print data["stars_init"][order][0:50]
print (data["stars_init"] - data["stars_init_cent"])[order][0:50]
print ""
print data["stars_init_cent_actual"][order][0:50] 
print data["stars_init_actual"][order][0:50]
print (data["stars_init_actual"] - data["stars_init_cent_actual"])[order][0:50]
quit()'''

######### Plotting ################
print "Plotting"

ok = (data["subgroup_number"]==0) & (data["isInterpolated"]==0)
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.7
ylo = 0.0; yhi = 1.1

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    ok = data["mass_star"] > 0
    # mass_star_init will include stripped stars, which we don't want
    #frac_in_situ = data["stars_init_cent_actual"] / data["mass_star_init"]
    frac_in_situ = data["stars_init_cent_actual"] / data["stars_init_actual"]
    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),frac_in_situ[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

    ok = mid_bin >= 10.8
    temp = np.max(np.where(ok==False)[0])+2

    py.plot(mid_bin[:temp], med[:temp], c="k", linewidth=1.2,alpha=0.3)
    py.plot(mid_bin[:temp], lo[:temp], c="k",alpha=0.3)
    py.plot(mid_bin[:temp], hi[:temp], c="k",alpha=0.3)  

    py.plot(mid_bin[ok], med[ok], c="k", label=sim_name,linewidth=1.2)
    py.plot(mid_bin[ok], lo[ok], c="k",alpha=0.8)
    py.plot(mid_bin[ok], hi[ok], c="k",alpha=0.8)

    ok = data["stars"] > 0
    frac_in_situ = data["stars_init_cent"] / data["stars_init"]
    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),frac_in_situ[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

    #py.plot(mid_bin, med, c="r", label="Integrator")
    #py.plot(mid_bin, lo, c="r")
    #py.plot(mid_bin, hi, c="r")

    py.ylabel(r"$M_{\mathrm{\star}}^{\mathrm{init}}[\, \mathrm{Central \,\, SF} \, ] \, / M_{\mathrm{\star}}^{\mathrm{init}}$")
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
    
    py.axhline(1.0,c="k",linestyle='--', alpha=0.4)

    #py.legend(loc="upper left",frameon=False)

fig_name = "central_SF_fraction.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
