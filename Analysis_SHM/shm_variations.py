# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us


sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

'''variations =      ["",         "_no_prev_cgm",                "_fixed_tau_infall", "_fixed_tau_ret_gal",         "_fixed_tau_ret_halo",     "_fixed_eta_gal", "_fixed_eta_halo"]
variation_names = ["Fiducial", "No preventive feedback (Rvir)", "Fixed tau infall", "Fixed tau return (Galaxy)", "Fixed tau return (Halo)", "Fixed eta (Galaxy)", "Fixed eta (Halo)"]
variations += ["_fixed_tau_ret_both", "_fixed_eta_both",        "_fixed_everything", "_fixed_tau_SF", "_fixed_inflows", "_fixed_tau_ret_both"]
variation_names += ["Fixed tau ret (both)", "Fixed eta (both)", "Fixed all",         "Fixed tau SF",  "Fixed inflows",  "_fixed tau return (both)"]'''

variations =      ["",  "_fixed_everything", "_only_tau_prev_cgm", "_only_tau_infall", "_only_tau_SF", "_only_tau_ret_gal", "_only_tau_ret_halo", "_only_eta_gal", "_only_eta_halo"]
variation_names = ["Fiducial", "Fixed all", "Only Prev FB (Rvir)", "Only tau infall", "Only tau SF", "Only tau ret (ISM)", "Only tau ret (halo)", "Only eta (ISM)", "Only eta (halo)"]
variations += ["_only_eta_both", "_only_inflows", "_only_tau_ret_both"]
variation_names+=["Only eta (both)", "Only inflows", "Only tau ret (both)"]

#variations = ["", "_no_ism_rec", "_no_prev_cgm", "_sam_cooling"]
#variation_names = ["Fiducial", "No gal rec", "No preventative fb (halo scale)", "SAM-like infall"]

#variations =      ["",         "_no_ism_out","_no_cgm_out", "_boost_ism_out", "_no_out"]
#variation_names = ["Fiducial", "0 x eta_gal", "0 x eta_halo", "3 x eta_gal", "0 x eta"]

props = ["mass_stars_30kpc", "subgroup_number", "mass_star"]
props += ["m200_host"]

props_integrator = ["stars"]

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()

# Read integrator output, looping over variations
for variant in variations:

    filename2 = "integrator_output_"+sim_name +variant+".hdf5"
    File2 = h5py.File(ode_path+filename2,"r")
    snapnum_group2 = File2["Snap_"+str(snap)]
    
    for prop in props_integrator:

        data[prop+variant] = snapnum_group2[prop][ok]

    File2.close()

print "plotting"

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':6})

py.subplot(111)

bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass                                                                                                                                              
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data["mass_stars_30kpc"]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
#med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data["mass_star"]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
py.plot(mid_bin, np.log10(med), c="k", label = sim_name,linewidth=0.9)
py.plot(mid_bin, np.log10(lo), c="k",linewidth=0.7)
py.plot(mid_bin, np.log10(hi), c="k",linewidth=0.7)

c_list =  ["r","g", "c", "m",  "y", "orange", "b", "0.25", "0.75", "k", "g", "r", "b", "c"]
ls_list = ["-","-",  "-", "-", "-", "-",      "-", "--",   "--",   ":", ":", ":", ":", ":"]
lw_list = [1.2, 1.0, 1.0, 1.0, 1.0, 1.0,      1.0, 1.0,     1.0,   1.0, 1.0, 1., 1.0, 1.0]

for i, variant in enumerate(variations):
    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data["stars"+variant]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, np.log10(med), c=c_list[i], label = "Integrator "+variation_names[i],linewidth=lw_list[i]*1.3,linestyle=ls_list[i])
    #py.plot(mid_bin, np.log10(lo), c=c_list[i])
    #py.plot(mid_bin, np.log10(hi), c=c_list[i])
    
py.legend(loc="lower center",frameon=False)


fig_name = "shm_variations.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)

py.xlim((10., 14.4))
py.ylim((-3.5,-1.5))

py.show()
