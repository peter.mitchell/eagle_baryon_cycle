import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us
import match_searchsorted as ms
import measure_particles_functions as mpf

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = [0.0083]

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star","mass_star_init"]
props += ["m200_host","isInterpolated","node_index"]

props_integrator = ["stars","dm_first_acc"]

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

# Also read the actual Mdm values, which are in the master subhalo catalogue, but not in the processed measurements files 
mass_dm_list = []
nI_list = []

for z in z_plot:
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]
    subhalo_group = snapnum_group["subhalo_properties"]
    g2Msun = 1./(1.989e33)
    mass_dm_list.append( subhalo_group["Mdm"][:] * g2Msun * 1.989e43 /h )
    nI_list.append( subhalo_group["nodeIndex"][:] )

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

data = []

for iz, z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    ok = (np.isnan(subhalo_group["mass_star"][:])==False)
    data.append({})
    for prop in props:
        data[-1][prop] = subhalo_group[prop][ok]

    # Match with base subhalo catalogue in order to get the dark matter subhalo mass of each subhalo                                         
    ptr = ms.match(data[-1]["node_index"], nI_list[iz])
    ok_match = ptr >= 0
    data[-1]["Mdm"] = np.zeros(len(data[-1]["node_index"]))
    data[-1]["Mdm"][ok_match] = mass_dm_list[iz][ptr][ok_match]

filename2 = "integrator_output_"+sim_name+"_fixed_everything.hdf5"
File2 = h5py.File(ode_path+filename2,"r")

for iz,z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]
    subhalo_group = snapnum_group["subhalo_properties"]
    ok = (np.isnan(subhalo_group["mass_star"][:])==False)

    snapnum_group2 = File2["Snap_"+str(snap)]

    for prop in props_integrator:

        data[iz][prop] = snapnum_group2[prop][ok]

File2.close()
File.close()

######### Plotting ################
print "Plotting"

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.1,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.7
ylo = -3.; yhi = -1.3

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 3
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    c_list = ["k","b","c","g","r","m"]

    for iz,z in enumerate(z_plot):
        i_snap = np.argmin(abs(z_snapshots-z))

        cent = data[iz]["subgroup_number"]==0

        if i == 0:
            y = data[iz]["mass_star"]/data[iz]["m200_host"]
        elif i == 1:
            y = data[iz]["stars"] / data[iz]["m200_host"]
        elif i == 2:
            y = 0.1*fb*data[iz]["dm_first_acc"] / data[iz]["m200_host"]
        elif i == 3:
            num = data[iz]["mass_star"]
            num[cent] = mpf.Sum_Common_ID([num], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
            y = num / data[iz]["m200_host"]
        elif i == 4:
            num = data[iz]["stars"]
            num[cent] = mpf.Sum_Common_ID([num], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
            y = num / data[iz]["m200_host"]
        elif i == 5:
            num = 0.1*fb*data[iz]["dm_first_acc"]
            num[cent] = mpf.Sum_Common_ID([num], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
            y = num / data[iz]["m200_host"]

        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"][cent]),y[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        label = r"$z= %3.1f$" % z_snapshots[i_snap]
        py.plot(mid_bin, np.log10(med), c=c_list[iz], label=label,linewidth=1.2)
        if iz == 0:
            py.plot(mid_bin, np.log10(lo), c=c_list[iz],alpha=0.4)
            py.plot(mid_bin, np.log10(hi), c=c_list[iz],alpha=0.4)

            bin_mh_ok = bin_mh[bin_mh >= 10.0]
            mean_median, mean_sigma = us.Mean_Median_Spread(bin_mh_ok,np.log10(data[iz]["m200_host"][cent]),np.log10(y[cent]),np.ones_like(y[cent]),-99.0)
            print "mean sigma", mean_sigma

            label = r"$\bar{\sigma} = %3.2f$" % mean_sigma
            py.annotate(label,(12.1,-3.0))

    '''if i == 0:
        py.ylabel(r"$\log_{10}(M_\star \, / M_{200})$")
    elif i == 1:
        py.ylabel(r"$\log_{10}(M_\star \, / \frac{\Omega_b}{\Omega_m-\Omega_b} M_{\mathrm{DM}}^{\mathrm{First-infall}})  \,\, [\mathrm{Central}]$")

    elif i == 2:
        py.ylabel(r"$\log_{10}(M_\star \, / \frac{\Omega_b}{\Omega_m-\Omega_b} M_{\mathrm{DM}}) \,\, [\mathrm{FOF}]$")

    elif i == 3:
        py.ylabel(r"$\log_{10}(M_\star \, / \frac{\Omega_b}{\Omega_m-\Omega_b} M_{\mathrm{DM}}^{\mathrm{First-infall}})  \,\, [\mathrm{FOF}]$")'''

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

    if i == 2 or i == 3:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
    
    py.axhline(1.0,c="k",linestyle='--', alpha=0.4)

    py.legend(loc="lower middle",frameon=False)

#fig_name = "shm_eagle_dm_first_acc.pdf"
#py.rcParams.update({'savefig.dpi':150})
#py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
