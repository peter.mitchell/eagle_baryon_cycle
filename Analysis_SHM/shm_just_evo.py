# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us


sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = [0.0083, 0.5, 1.0, 2.0, 3.0]

use_30kpc = False

c_list = ["k","b","c","g","r","m"]

variations = [""]
variation_names = ["Fiducial"]
panels = [1]

variations +=      ["_evo_just_eta_gal", "_evo_just_eta_halo", "_evo_just_tau_SF", "_evo_just_tau_infall", "_evo_just_prev_fb", "_no_evo_all" ]
variation_names += [r"Galaxy-scale outflow ($\eta_{\mathrm{gal}}$)", r"Halo-scale outflow ($\eta_{\mathrm{halo}}$)"  ,         r"Star formation ($f_{\mathrm{SF}}$)", r"Galaxy-scale accretion ($f_{\mathrm{infall}}$)", r"Halo-scale accretion ($f_{\mathrm{acc}}$)", "All"]
panels +=             [6,              7,                      5,               4,                         3               ,     2,            ]

props = ["mass_stars_30kpc", "subgroup_number", "mass_star"]
props += ["m200_host"]

props_integrator = ["stars"]

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

data = []
for z in z_plot:
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
    data.append({})
    for prop in props:
        data[-1][prop] = subhalo_group[prop][ok]



# Read integrator output, looping over variations
for variant in variations:

    filename2 = "integrator_output_"+sim_name +variant+".hdf5"
    File2 = h5py.File(ode_path+filename2,"r")

    for iz,z in enumerate(z_plot):
        i_snap = np.argmin(abs(z_snapshots-z))

        snap = snapshots[i_snap]
        snip = snipshots[i_snap]
        
        snapnum_group = File["Snap_"+str(snap)]
        subhalo_group = snapnum_group["subhalo_properties"]
        ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
        
        snapnum_group2 = File2["Snap_"+str(snap)]
        for prop in props_integrator:
            data[iz][prop+variant] = snapnum_group2[prop][ok]

    File2.close()
File.close()


# Compute evolution over a given z range in Fiducial integrator, for chosen halo mass bins (to help guide the eye)
mh_show = [11.0, 12.0]
zlo = 0.0083
zhi = 3.0

bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

izlo = np.argmin(abs(zlo-np.array(z_plot)))
med_fid_lo, junk, junk, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[izlo]["m200_host"]),data[izlo]["stars_no_evo_all"]/data[izlo]["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

izhi = np.argmin(abs(zhi-np.array(z_plot)))
med_fid_hi, junk, junk, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[izhi]["m200_host"]),data[izhi]["stars_no_evo_all"]/data[izhi]["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

med_fid_lo_list = []
med_fid_hi_list = []
mh_list = []

for mh in mh_show:
    im = np.argmin(abs(mh - mid_bin))
    med_fid_lo_list.append( med_fid_lo[im] )
    med_fid_hi_list.append( med_fid_hi[im] )
    mh_list.append( mid_bin[im] )

print "plotting"

from utilities_plotting import *
npanels = len(variations)+1; nrow = 3; ncol = 3
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*3],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.08,
                    'figure.subplot.top':0.99,
                    'axes.labelsize':9,
                    'legend.fontsize':9})


py.figure()
np.seterr(all='ignore')
subplots = panelplot_flexible(npanels,nrow,ncol)

xlo = 10.05; xhi = 14.7
ylo = -3.; yhi = -1.5


for n,ax in enumerate(subplots):
    py.axes(ax)

    if n == 0:
        for iz,z in enumerate(z_plot):
            if use_30kpc:
                mstar = data[iz]["mass_stars_30kpc"]
            else:
                mstar = data[iz]["mass_star"]

            med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"]),mstar/data[iz]["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
            label = r"$z=%3.1f$" % z
            py.plot(mid_bin, np.log10(med), c=c_list[iz], label = label,linewidth=1.2)

        py.annotate("EAGLE",(10.25,-1.62))
 
    for i, i_panel in enumerate(panels):

        if i_panel == n:
            variant = variations[i]
            variation_name = variation_names[i]

            for iz,z in enumerate(z_plot):
                med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"]),data[iz]["stars"+variant]/data[iz]["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

                label = r"$z=%3.1f$" % z
                py.plot(mid_bin, np.log10(med), c=c_list[iz],linewidth=1.4,linestyle="-",label=label)
                


            if variation_name == "Fiducial":
                extra_str = ""
            elif variation_name == "All":
                extra_str = ""
            else:
                extra_str = ""
            py.annotate(extra_str+variation_name,(10.25,-1.62))

            for i_m in range(len(mh_list)):
                py.plot([mh_list[i_m], mh_list[i_m]], [np.log10(med_fid_lo_list[i_m]),np.log10(med_fid_hi_list[i_m])], c="k",alpha=0.5)
                py.plot([mh_list[i_m]-0.05, mh_list[i_m]+0.05], [np.log10(med_fid_lo_list[i_m]),np.log10(med_fid_lo_list[i_m])], c="k",alpha=0.5)
                py.plot([mh_list[i_m]-0.05, mh_list[i_m]+0.05], [np.log10(med_fid_hi_list[i_m]),np.log10(med_fid_hi_list[i_m])], c="k",alpha=0.5)


    legend1 = py.legend(loc='lower right',frameon=False)
    py.gca().add_artist(legend1)

    py.xlim((xlo, xhi))
    py.ylim((ylo,yhi))

    if n == 0 or n ==3 or n == 6:
        py.ylabel(r"$\log_{10}(M_\star \, / M_{200})$")
    if n == 6 or n==7 or n == 5:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_{\odot}})$")
        

fig_name = "shm_just_evo.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)


py.show()
