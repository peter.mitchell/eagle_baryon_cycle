import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_interpolation as ui

use_1pluseta = True

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
# First inflows

file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

f_name_list = ["fcool_recooled_return_tscaled_"+fV_str, "fcool_pristine_prevent_prdm", "mstar_init_med"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

# Now read outflows
file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

f_name_list = ["ism_wind_tot_ml"+fV_str, "mstar_med"]

for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

bin_mh_mid2 = file_grid["log_msub_grid"][:]

file_grid.close()

#### Plotting #########
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*1.8],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.35,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.5

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

rec_eff_grid = f_med_dict["fcool_recooled_return_tscaled_"+fV_str] + 1
out_eff_grid = 1. / ( f_med_dict["ism_wind_tot_ml"+fV_str] + 1)

if not use_1pluseta:
    out_eff_grid = 1. / ( f_med_dict["ism_wind_tot_ml"+fV_str])

prev_eff_grid = f_med_dict["fcool_pristine_prevent_prdm"]

#msmh_grid2 = f_med_dict["mstar_med"] / (10**bin_mh_mid2 * fb)
msmh_grid = f_med_dict["mstar_init_med"] / (10**bin_mh_med * fb)

show_z = [2] # z=1

for i,ax in enumerate(subplots):
    py.axes(ax)

    ax.set_position([0.19, 0.1, 0.79, 0.56])
    #ax.set_position([0.19, 0.44, 0.79, 0.36])

    n = show_z[i]
    rec_eff = rec_eff_grid[n]
    out_eff = out_eff_grid[n]
    prev_eff = prev_eff_grid[n]
    
    msmh = msmh_grid[n]

    out_eff_interp = ui.Linear_Interpolation(bin_mh_mid2, out_eff, bin_mh_med[n])
    tot_eff = rec_eff * out_eff_interp * prev_eff

    if show_incomplete:
        if complete_cut == "1newstar":
            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
        elif complete_cut == "100star":
            ok = (bin_mh_med[n] >= 10.8)  | (np.isnan(bin_mh_med[n]))
            ok2 = (bin_mh_mid2 >= 10.8)
        else:
            print "No"; quit()
    else:
        ok = bin_mh_med[n] > 0

    temp = np.max(np.where(ok==False)[0])+2
    temp2 = np.max(np.where(ok2==False)[0])+2

    # Highest mass point in this bin is an outlier in many of the plots
    if "L0100" in sim_name and n == 4:
        crash
        ok2 = (bin_mh_mid != 13.45)
        y[ok2==False] = np.nan
        y2[ok2==False] = np.nan

    py.plot(bin_mh_med[n][:temp], np.log10(rec_eff[:temp]),c="c",linewidth=lw,alpha=0.3)
    rec_eff[ok==False] = np.nan
    py.plot(bin_mh_mid2[:temp2], np.log10(out_eff[:temp2]),c="r",linewidth=lw,alpha=0.3)
    out_eff[ok2==False] = np.nan
    py.plot(bin_mh_med[n][:temp], np.log10(prev_eff[:temp]),c="b",linewidth=lw,alpha=0.3)
    prev_eff[ok==False] = np.nan

    py.plot(bin_mh_med[n][:temp], np.log10(tot_eff[:temp]),c="k",linewidth=lw,alpha=0.3)
    tot_eff[ok==False] = np.nan

    py.plot(bin_mh_mid[:temp], np.log10(msmh[:temp]),c="k",linewidth=lw,alpha=0.3,linestyle='--')
    msmh[ok==False] = np.nan

    #label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
    #py.annotate(label, (11.7,-1.75))

    label = r"$y = 1 + G_{\mathrm{ret}}^{\mathrm{gal}}$"

    py.plot(bin_mh_med[n][ok],np.log10(rec_eff[ok]),c="c",linewidth=lw, label=label)
    py.scatter(bin_mh_med[n][ok],np.log10(rec_eff[ok]),c="c",edgecolors="none",s=5)

    label = r"$y = \frac{1}{1+\eta_{\mathrm{gal}}}$"

    py.plot(bin_mh_mid2[ok2],np.log10(out_eff[ok2]),c="r",linewidth=lw, label=label)
    py.scatter(bin_mh_mid2[ok2],np.log10(out_eff[ok2]),c="r",edgecolors="none",s=5)

    label = r"$y = f_{\mathrm{prev}}$"

    py.plot(bin_mh_med[n][ok],np.log10(prev_eff[ok]),c="b",linewidth=lw, label=label)
    py.scatter(bin_mh_med[n][ok],np.log10(prev_eff[ok]),c="b",edgecolors="none",s=5)

    label = r"$y = f_{\mathrm{prev}} \, \frac{1}{1+\eta^{\mathrm{gal}}} \, (1+G_{\mathrm{ret}}^{\mathrm{gal}})$"

    py.plot(bin_mh_med[n][ok],np.log10(tot_eff[ok]),c="k",linewidth=lw, label=label)
    py.scatter(bin_mh_med[n][ok],np.log10(tot_eff[ok]),c="k",edgecolors="none",s=5)

    label = r"$y= \frac{M_{\star}^{\mathrm{init}}}{f_{\mathrm{b}} M_{200}}$"

    py.plot(bin_mh_med[n][ok],np.log10(msmh[ok]),c="k",linewidth=lw, linestyle='--',label=label)

    #for n in range(7):
    #    py.plot(bin_mh_med[n], np.log10(msmh_grid[n]), c=c_list[n], linestyle=':', alpha=0.7)

    py.ylabel(r"$\log_{10}(y)$")

    legend1 = py.legend(loc='upper left',frameon=False,numpoints=1,prop={'size': 10},bbox_to_anchor=(-0., 1.63))
    py.gca().add_artist(legend1)

    '''l1, = py.plot(bin_mh_med[n],bin_mh_med[n]-99,c="k",linewidth=lw,label="Gas")
    l2, = py.plot(bin_mh_med[n],bin_mh_med[n]-99,c="k",linewidth=0.8,label="Dark matter",linestyle='--')
    py.legend(handles=[l1,l2],labels=[r"Gas","Dark matter"], loc="lower left",frameon=False,numpoints=1)'''

    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

    ylo = -2.; yhi = 0.5
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

fig_name = "combined_efficiencies_galaxy_scale.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
