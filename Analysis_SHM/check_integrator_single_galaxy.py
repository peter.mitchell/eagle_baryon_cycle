# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
sim_name = "L0025N0376_REF_200_snip"

grn_select = 36
sgrn_select = 0

props_track = ["mass_new_stars_init", "mass_stars_30kpc", "group_number", "subgroup_number", "mass_star","mass_star_init"]
props_track += ["mass_gas_NSF_ISM", "mass_gas_NSF", "mass_gas_SF"]
props_track += ["mass_diffuse_dm_pristine","m200_host"]

props_track += ["mass_sf_ism_join_wind_0p25vmax", "mass_nsf_ism_join_wind_0p25vmax", "mass_ism_reheated_join_wind_0p25vmax"]
props_track += ["mass_galtran", "mass_prcooled", "mass_recooled_0p25vmax"]
props_track += ["mass_sf_ism_reheated", "mass_nsf_ism_reheated","mass_recooled_ireheat","mass_recooled_0p5vmax"]

props_track += ["mass_praccreted", "mass_reaccreted_0p25vmax", "mass_fof_transfer"]
props_track += ["mass_join_halo_wind_0p25vmax", "mass_halo_reheated_join_wind_0p25vmax"]
props_track += ["mass_reaccreted_0p5vmax", "mass_halo_reheated", "mass_reaccreted_hreheat"]
props_track += ["mass_outside_galaxy_0p25vmax", "mass_outside_0p25vmax"]

props_integrator = ["cgm","ism","stars","ej","wind"]
props_integrator += ["dm_star_sfr", "dm_cgm_out"]

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
filename2 = "integrator_output_"+sim_name +".hdf5"
if test_mode:
    filename += "_test_mode"
    filename2 = "test_integrator_output_"+sim_name +".hdf5"
filename += ".hdf5"


File = h5py.File(input_path+filename,"r")
File2 = h5py.File(ode_path+filename2,"r")

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "Looking at tree snapshot", snap

    

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    # Read the data
    if "Snap_"+str(snap) not in File:
        print "No data, skipping"
        continue

    snapnum_group = File["Snap_"+str(snap)]
    snapnum_group2 = File2["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]
    
    if "node_index" not in subhalo_group:
        print "No data, skipping"
        continue

    node_index_ts = np.rint(subhalo_group["node_index"][:]).astype("int")

    # Select the desired galaxy on the final (low-z) snapshot
    if i_snap == 1:
        node_index_ns = node_index_ts

        group_number = subhalo_group["group_number"][:]; subgroup_number = subhalo_group["subgroup_number"][:]

        ok = (group_number == grn_select) & (subgroup_number == sgrn_select)

        if len(ok[ok]) == 0:
            print "Error: couldn't find grn/sgrn", grn_select, sgrn_select, "in", input_path+filename, "snapshot=", snap
            quit()
        elif len(ok[ok]) > 1:
            print "Error: wtf!"
            quit()

        node_index_ns = node_index_ns[ok]

        data_track = {}
        for prop in props_track:
            data_track[prop] = np.zeros((len(snapshots),len(node_index_ns)))
            data_track[prop][i_snap] += subhalo_group[prop][ok]

        for prop in props_integrator:
            data_track[prop] = np.zeros((len(snapshots),len(node_index_ns)))
            data_track[prop][i_snap] += snapnum_group2[prop][ok]
            
        continue

    descendant_index_ts = np.rint(subhalo_group["descendant_index"][:]).astype("int")

    ptr = ms.match(descendant_index_ts,node_index_ns)
    ok_match = ptr >= 0

    ok = np.isnan(subhalo_group["mass_star"][:]) == False
    
    if len(ptr[ok_match&ok])>0:
        # Choose main progenitor (just do largest stellar mass)
        mp = np.argmax(subhalo_group["mass_star"][ok&ok_match])
        nI_select = node_index_ts[ok&ok_match][mp]
        ok = nI_select == node_index_ts

        for prop in props_track:
            data_track[prop][i_snap] = subhalo_group[prop][ok]
        for prop in props_integrator:
            data_track[prop][i_snap] = snapnum_group2[prop][ok]

        node_index_ns = node_index_ts[ok]
    else:
        print "Lost track of main progenitor, stoppping"
        break

t_snapshots = t_snapshots[1:]
for prop in props_track:
    data_track[prop] = data_track[prop][1:]
for prop in props_integrator:
    data_track[prop] = data_track[prop][1:]

print "Sgrn:", np.ravel(np.array(data_track["subgroup_number"]))






#for n in range(150):
#    print snapshots[::-1][n], data_track["group_number"][::-1][n], data_track["subgroup_number"][::-1][n], t_snapshots[::-1][n], (data_track["mass_gas_NSF"]-data_track["mass_gas_NSF_ISM"])[::-1][n], data_track["cgm"][::-1][n]
#quit()



print "plotting"

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*3,2.49*2.25],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':6})

py.subplot(321)

py.plot(t_snapshots, data_track["mass_star"], c="k", label="True stellar mass (no aperture)")

py.plot(t_snapshots, data_track["stars"], c="b", label="Integrator stellar mass")

py.legend(loc="upper right",frameon=False)
py.xlabel(r"$t \, / \mathrm{Gyr}$")
py.ylabel(r"$M_\star \, / \mathrm{M_\odot}$")

sat = data_track["subgroup_number"]>0
for i in range(len(t_snapshots)):
    if sat[i]:
        py.axvline(t_snapshots[i],c="b",linestyle='--',alpha=0.1)

py.subplot(322)

py.plot(t_snapshots, data_track["mass_new_stars_init"], c="k", label="True SFR")
py.plot(t_snapshots, data_track["dm_star_sfr"], c="b", label="Integrator SFR")
py.plot(t_snapshots, data_track["mass_diffuse_dm_pristine"]*fb, c="g", label="fb * DM_growth",alpha=0.5)

py.legend(loc="upper right",frameon=False)
py.xlabel(r"$t \, / \mathrm{Gyr}$")
#py.ylabel(r"$M_\star \, / \mathrm{M_\odot}$")

sat = data_track["subgroup_number"]>0
for i in range(len(t_snapshots)):
    if sat[i]:
        py.axvline(t_snapshots[i],c="b",linestyle='--',alpha=0.1)

py.subplot(323)


py.plot(t_snapshots, data_track["mass_gas_NSF"] - data_track["mass_gas_NSF_ISM"], c="k", label="True CGM")

py.plot(t_snapshots, data_track["cgm"], c="b", label="Integrator CGM")

py.legend(loc="upper right",frameon=False)
#py.ylabel(r"$M_\star \, / \mathrm{M_\odot}$")
py.xlabel(r"$t \, / \mathrm{Gyr}$")

sat = data_track["subgroup_number"]>0
for i in range(len(t_snapshots)):
    if sat[i]:
        py.axvline(t_snapshots[i],c="b",linestyle='--',alpha=0.1)

py.subplot(324)


py.plot(t_snapshots, data_track["mass_gas_SF"] + data_track["mass_gas_NSF_ISM"], c="k", label="True ISM")

py.plot(t_snapshots, data_track["ism"], c="b", label="Integrator ISM")

sat = data_track["subgroup_number"]>0
for i in range(len(t_snapshots)):
    if sat[i]:
        py.axvline(t_snapshots[i],c="b",linestyle='--',alpha=0.1)

py.subplot(325)


py.plot(t_snapshots, data_track["mass_halo_reheated"] , c="k", label="True halo outflow")

py.plot(t_snapshots, data_track["dm_cgm_out"], c="b", label="Integrator halo outflow")

sat = data_track["subgroup_number"]>0
for i in range(len(t_snapshots)):
    if sat[i]:
        py.axvline(t_snapshots[i],c="b",linestyle='--',alpha=0.1)

py.legend(loc="upper right",frameon=False)
py.xlabel(r"$t \, / \mathrm{Gyr}$")
#py.ylabel(r"$M_\star \, / \mathrm{M_\odot}$")

fig_name = "check_integrator_single_galaxy.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)

py.show()
