# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star"]
props += ["mass_gas_NSF_ISM", "mass_gas_NSF", "mass_gas_SF"]
props += ["m200_host","isInterpolated"]
props += ["mass_outside_galaxy_0p25vmax", "mass_outside_0p25vmax", "mass_inside_0p25vmax"]

props_integrator = ["cgm_pr","ism","stars","ej_pr","ej_wind", "n_snap","cgm_wind"]

show_ej = False # Choose whether fourth panel shows ej or wind

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()

filename2 = "integrator_output_"+sim_name+".hdf5"
File2 = h5py.File(ode_path+filename2,"r")
snapnum_group2 = File2["Snap_"+str(snap)]

for prop in props_integrator:

    data[prop] = snapnum_group2[prop][ok]

File2.close()


######### Plotting ################
print "Plotting"

ok = (data["subgroup_number"]==0) & (data["isInterpolated"]==0)
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.2,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 15.
ylo = -1.; yhi = 1.

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        mcgm = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]
        mcgm_wind = data["mass_inside"+fV_str]
        mcgm_pr = mcgm - mcgm_wind

        mcgm2 = data["cgm_pr"] + data["cgm_wind"]
        mcgm_wind2 = data["cgm_wind"]
        mcgm_pr2 = data["cgm_pr"]

        ok_cgm = ok & (mcgm>0)
        ok_cgm2 = ok & (mcgm2>0)
            
        #py.scatter(np.log10(data["m200_host"][ok_cgm]), np.log10(data["cgm"][ok_cgm]/mcgm[ok_cgm]), cmap="autumn", c=data["n_snap"][ok_cgm], edgecolors="none",alpha=0.5)
        #py.axhline(0.0,c="k",alpha=0.3)

        #mean_ratio = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok_cgm]),mcgm[ok_cgm],data["cgm"][ok_cgm])
        #mcgm[mcgm==0] = 1e-9

        med_pr, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_cgm]),mcgm_pr[ok_cgm]/mcgm[ok_cgm],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_wind, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_cgm]),mcgm_wind[ok_cgm]/mcgm[ok_cgm],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_pr2, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_cgm2]),mcgm_pr2[ok_cgm2]/mcgm2[ok_cgm2],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_wind2, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_cgm2]),mcgm_wind2[ok_cgm2]/mcgm2[ok_cgm2],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, med_wind, c="r")
        py.plot(mid_bin, med_wind2, c="r",linestyle='--')

        py.plot(mid_bin, med_pr, c="b")
        py.plot(mid_bin, med_pr2, c="b",linestyle='--')
        
        #py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

        #py.ylabel(r"$\log_{10}(M_{\mathrm{CGM}} \, / M_{\mathrm{CGM,true}})$")
        py.ylabel(r"$\mathrm{Fraction \,\, (CGM)}$")
        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))

    if i == 1:
        mej = data["mass_outside"+fV_str]
        mej_wind = data["mass_outside_galaxy"+fV_str] - data["mass_inside"+fV_str]
        mej_pr = mej - mej_wind

        mej2 = data["ej_pr"] + data["ej_wind"]
        mej_wind2 = data["ej_wind"]
        mej_pr2 = data["ej_pr"]

        ok_ej = ok & (mej>0)
        ok_ej2 = ok & (mej2>0)

        #py.scatter(np.log10(data["m200_host"][ok_ism]), np.log10(data["ism"][ok_ism]/mism[ok_ism]), cmap="autumn", c=data["n_snap"][ok_ism], edgecolors="none",alpha=0.5)
        #py.axhline(0.0,c="k",alpha=0.3)

        #mean_ratio = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok_ism]),mism[ok_ism],data["ism"][ok_ism])
        #mism[mism==0] = 1e-9
        
        med_pr, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_ej]),mej_pr[ok_ej]/mej[ok_ej],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_pr2, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_ej2]),mej_pr2[ok_ej2]/mej2[ok_ej2],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_wind, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_ej]),mej_wind[ok_ej]/mej[ok_ej],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_wind2, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok_ej2]),mej_wind2[ok_ej2]/mej2[ok_ej2],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, med_wind, c="r")
        py.plot(mid_bin, med_wind2, c="r",linestyle='--')

        py.plot(mid_bin, med_pr, c="b")
        py.plot(mid_bin, med_pr2, c="b",linestyle='--')
        
        #py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

        py.ylabel(r"$\mathrm{Fraction \, \, (Ejected)}$")
        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


fig_name = "frac_cgm_ej_wind.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
