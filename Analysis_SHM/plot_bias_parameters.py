import numpy as np
import h5py

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
sim_name = "L0025N0376_REF_200_snip"

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

fVmax_cut = 0.25
fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
file_name = input_path+"/Processed_Catalogues/efficiencies_grid_"+sim_name+"_integrator"
file_name += ".hdf5"

file_grid = h5py.File(file_name,"r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

#eff_names = ["bias_cgm_out"+fV_str, "bias_reacc"+fV_str,"bias_cgm_out2"+fV_str, "bias_reacc2"+fV_str]

# R200 outflow wind bias
eff_names = ["bias_cgm_out2"+fV_str]
labels    = ["Ejection bias"]
ylabel = [r"$\log_{10}(B_{\mathrm{out}})$"] # Note f_prev is not good semantically since higher prev fb implies lower accretion, which is not the case atm!

# Reaccretion wind bias
eff_names += ["bias_reacc2"+fV_str]
labels    += ["Reaccretion bias"]
ylabel    += [r"$\log_{10}(B_{\mathrm{ret}})$"]

eff_dict = {}
for name in eff_names:
    eff_dict[name] = file_grid[name][:]

file_grid.close()


from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.15,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.985,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 13.5

c_list = ["k","m","b","c","g","y","tab:orange","r"]
lw = 1

npanels = len(eff_names); nrow = 2; ncol = 1

py.figure()
np.seterr(all='ignore')
subplots = panelplot_flexible(npanels,nrow,ncol)

for n,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0,1,2,3,4,5,6]
    
    for iz in show_z:
        
        y = np.log10(eff_dict[eff_names[n]][iz])

        ok = (bin_mh_med[iz] >= 10.8)  | (np.isnan(bin_mh_med[iz]))
        temp = np.max(np.where(ok==False)[0])+2

        # Highest mass point in this bin is an outlier in many of the plots                                                                                         
        if "L0100" in sim_name and iz == 4:
            ok2 = (bin_mh_mid != 13.45)
            y[ok2==False] = np.nan
            
        py.plot(bin_mh_med[iz][:temp], y[:temp],c=c_list[iz],linewidth=lw,alpha=0.3,linestyle='--')
        y[ok==False] = np.nan

        py.plot(bin_mh_med[iz][ok],y[ok],c=c_list[iz])

    py.xlim((xlo,xhi))

    if n == 1:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.ylabel(ylabel[n])

    ylo = -1.5
    yhi = 0.9
    py.ylim((ylo,yhi))
    py.xlim((xlo,xhi))

    py.axhline(0.0,c="k",linestyle='--',alpha=0.4)

    py.annotate(labels[n], (12.,0.7))

    if n == 0:
        labels_z = []
        lines = []
        for iz in show_z:
            labels_z += [r"$%3.1f < z < %3.1f$" % (1./a_max[iz]-1 , 1./a_min[iz]-1)]
            line, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c=c_list[iz],linewidth=lw)
            lines.append(line)
        py.legend(handles=lines, labels=labels_z,loc="lower right", frameon=False,numpoints=1)

fig_name = "bias_parameters.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)

py.show()
