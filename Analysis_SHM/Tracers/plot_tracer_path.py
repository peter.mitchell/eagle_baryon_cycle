import numpy as np
import h5py
import match_searchsorted as ms

#sim_name = "L0100N1504_REF_200_snip"
sim_name = "L0025N0376_REF_200_snip"
# Note to self - remember that sometimes I select a different galaxy for tracer pre-selection, but won't redo all the runs

#variation = ""
variation = "_no_nuisance"

#variation = "_boost_tau_ret_gal_10p0"
#variation = "_boost_tau_ret_halo_10p0"

#variation = "_boost_tau_infall_10p0"

#variation = "_boost_tau_infall_tau_ret_halo_10p0"
#variation = "_boost_tau_infall_tau_ret_halo_100p0"

#variation = "_boost_tau_ret_10p0" # Both recycling terms
#variation = "_boost_tau_ret_100p0" # Both recycling terms

#variation = "_boost_inflows_10p0" # Both recycling terms + 1st-time infall
#variation = "_boost_inflows_100p0" # Both recycling terms + 1st-time infall
#variation = "_boost_inflows_1000p0" # BOOM

#variation = "_boost_eta_gal_2p0"
#variation = "_boost_eta_halo_2p0"

######### Load snapshot/snipshot information ###############
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
filename = "subhalo_catalogue_" + sim_name + ".hdf5"
File = h5py.File(input_path+filename,"r")
tree_snapshot_info = File["tree_snapshot_info"]
snapshots = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
File.close()

######### Get tracer data ####################
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"
filename = "integrator_output_" + sim_name + variation + ".hdf5"
File = h5py.File(input_path+filename,"r")

# Init tracer arrays
tracer_names = ["group_number", "subgroup_number", "node_index", "descendant_index", "state"]

for i_snap, snap in enumerate(snapshots):
    if i_snap == 0:
        continue
    group_str = "Tracer_Data_Snap"+str(snap)
    if group_str not in File:
        break

    tracer_data = File[group_str]
    state = tracer_data["state"][:]
    in_wind = tracer_data["state"][:]
    node_index = tracer_data["node_index"][:]

    n_tr = len(state)

    if i_snap == 1:
        n_snap = len(snapshots)
        state_evo = np.zeros(( n_snap, n_tr ))
        
        n_igm = np.zeros(( n_snap ))
        n_cgm_pr = np.zeros(( n_snap ))
        n_cgm_wind = np.zeros(( n_snap ))
        n_ism = np.zeros(( n_snap ))
        n_stars = np.zeros(( n_snap ))
        n_ej_wind = np.zeros(( n_snap ))
        n_ej_pr = np.zeros(( n_snap ))

        n_tr_tot = n_tr

    state_evo[i_snap,:n_tr] = state
    state_evo[i_snap,:n_tr] = in_wind

    n_cgm_pr[i_snap] = len(state[state == 1])
    n_cgm_wind[i_snap] = len(state[state == 2])
    n_ism[i_snap] = len(state[state == 3])
    n_stars[i_snap] = len(state[state == 4])
    n_ej_pr[i_snap] = len(state[state == 5])
    n_ej_wind[i_snap] = len(state[state == 6])

    n_igm[i_snap] = n_tr_tot - n_tr

n_igm *= 1./n_tr_tot
n_cgm_pr *= 1./n_tr_tot
n_cgm_wind *= 1./n_tr_tot
n_stars *= 1./n_tr_tot
n_ism *= 1./n_tr_tot
n_ej_wind *= 1./n_tr_tot
n_ej_pr *= 1./n_tr_tot

from utilities_plotting import *
py.figure()

py.subplot(211)

py.fill_between( t_snapshots, np.zeros_like(n_igm), n_igm, color="b", alpha=0.7)

py.fill_between( t_snapshots, n_igm, n_cgm_pr+n_igm, color="c", alpha=0.7)
py.fill_between( t_snapshots, n_igm+n_cgm_pr, n_cgm_wind+n_igm+n_cgm_pr, color="c", alpha=0.3)

cumul = n_igm + n_cgm_pr + n_cgm_wind
py.fill_between( t_snapshots, cumul, cumul+n_ism, color="g", alpha=0.7)

cumul += n_ism
py.fill_between( t_snapshots, cumul, cumul+n_stars, color="k", alpha=0.7)

cumul += n_stars
py.fill_between( t_snapshots, cumul, cumul+n_ej_pr, color="r", alpha=0.7)

cumul += n_ej_pr
py.fill_between( t_snapshots, cumul, cumul+n_ej_wind, color="r", alpha=0.3)


py.xlabel(r"$t \, / \mathrm{Gyr}$")
py.ylabel(r"$\mathrm{Fraction}$")

py.subplot(212)

n_tr_show = 10
lw = 2.0
delta_t_pos = (t_snapshots[0:-1] - t_snapshots[1:])*0.5
delta_t_neg = (t_snapshots[1:-1] - t_snapshots[2:])*0.5
show = np.random.randint(0, n_tr_tot, n_tr_show)

for n in range(n_tr_show):
    n_show = show[n]
    for m in range(n_snap-2):
        if state_evo[m+1,n_show] == 0:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="b",linewidth=lw)
        if state_evo[m+1,n_show] == 1:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="c",linewidth=lw)
        if state_evo[m+1,n_show] == 2:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="c",linewidth=lw*2,alpha=0.5)
        if state_evo[m+1,n_show] == 3:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="g",linewidth=lw)
        if state_evo[m+1,n_show] == 4:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="k",linewidth=lw)
        if state_evo[m+1,n_show] == 5:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="r",linewidth=lw)
        if state_evo[m+1,n_show] == 6:
            py.plot([t_snapshots[m+1]-delta_t_neg[m],t_snapshots[m+1]+delta_t_pos[m]],[n,n],c="r",linewidth=lw*2,alpha=0.5)
   
py.ylim((-1,n_tr_show))
py.show()
