import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us
import match_searchsorted as ms
import measure_particles_functions as mpf

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = [0.0083]

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star","mass_star_init"]
props += ["m200_host","isInterpolated","node_index"]

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

# Also read the actual Mdm values, which are in the master subhalo catalogue, but not in the processed measurements files 
mass_dm_list = []
nI_list = []

for z in z_plot:
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]
    subhalo_group = snapnum_group["subhalo_properties"]
    g2Msun = 1./(1.989e33)
    mass_dm_list.append( subhalo_group["Mdm"][:] * g2Msun * 1.989e43 /h )
    nI_list.append( subhalo_group["nodeIndex"][:] )

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

data = []

for iz, z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    ok = (np.isnan(subhalo_group["mass_star"][:])==False)
    data.append({})
    for prop in props:
        data[-1][prop] = subhalo_group[prop][ok]

    # Match with base subhalo catalogue in order to get the dark matter subhalo mass of each subhalo                                         
    ptr = ms.match(data[-1]["node_index"], nI_list[iz])
    ok_match = ptr >= 0
    data[-1]["Mdm"] = np.zeros(len(data[-1]["node_index"]))
    data[-1]["Mdm"][ok_match] = mass_dm_list[iz][ptr][ok_match]

File.close()

######### Plotting ################
print "Plotting"

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.18,
                    'figure.subplot.top':0.97,
                    'figure.subplot.right':0.99,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.7
ylo = -3.2; yhi = -1.5

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    c_list = ["k","b","c","g","r","m"]

    for iz,z in enumerate(z_plot):
        i_snap = np.argmin(abs(z_snapshots-z))

        cent = data[iz]["subgroup_number"]==0

        y = data[iz]["mass_star"]/data[iz]["m200_host"]

        num = data[iz]["mass_star"]
        num[cent] = mpf.Sum_Common_ID([num], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
        y2 = num / data[iz]["m200_host"]

        y3 = data[iz]["mass_stars_30kpc"]/data[iz]["m200_host"]

        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"][cent]),y3[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        ok = mid_bin >= 10.8
        temp = np.max(np.where(ok==False)[0])+2

        py.plot(mid_bin[:temp], np.log10(med[:temp]),c="k", alpha=0.3, linewidth=1.2,linestyle='--')
        py.plot(mid_bin[:temp], np.log10(lo[:temp]),c="k", alpha=0.3,linestyle='--')
        py.plot(mid_bin[:temp], np.log10(hi[:temp]),c="k", alpha=0.3,linestyle='--')

        label = r"$M_\star(r<30 \, / \mathrm{kpc})$"
        py.plot(mid_bin[ok], np.log10(med[ok]), c="k", label=label,linewidth=1.2)
        py.plot(mid_bin[ok], np.log10(lo[ok]), c="k",alpha=0.4)
        py.plot(mid_bin[ok], np.log10(hi[ok]), c="k",alpha=0.4)

        med3, lo3, hi3, mid_bin3 = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"][cent]),y[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        label = r"$M_\star \, \mathrm{[Central \,\, Subhalo]}$"

        py.plot(mid_bin[:temp], np.log10(med3[:temp]), c="r", linewidth=1.2, alpha=0.3,linestyle='--')
        py.plot(mid_bin[ok], np.log10(med3[ok]), c="r", linewidth=1.2, label=label)

        med2, lo2, hi2, mid_bin2 = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"][cent]),y2[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        label = r"$M_\star(r<R_{200})$"
        py.plot(mid_bin[:temp], np.log10(med2[:temp]), c="c", linewidth=1.2, alpha=0.3, linestyle='--')
        py.plot(mid_bin[ok], np.log10(med2[ok]), c="c", linewidth=1.2, label=label)
        
    py.ylabel(r"$\log_{10}(M_\star \, / M_{200})$")
    py.xlim((xlo, xhi))

    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
    
    py.legend(loc="lower middle",frameon=False)

fig_name = "shm_eagle_paper.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
