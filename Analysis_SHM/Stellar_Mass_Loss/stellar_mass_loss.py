import h5py
import numpy as np
import utilities_interpolation as ui

# Compute stellar mass loss using Eagle yields, given an initial stellar mass, metallicity, and time
# Reference is Wiersma 09b

agb_path = "/cosma/home/dphlss/d72fqv/Baryon_Cycle/Analysis_SHM/Stellar_Mass_Loss/AGB.hdf5"
snii_path = "/cosma/home/dphlss/d72fqv/Baryon_Cycle/Analysis_SHM/Stellar_Mass_Loss/SNII.hdf5"
lifetime_path = "/cosma/home/dphlss/d72fqv/Baryon_Cycle/Analysis_SHM/Stellar_Mass_Loss/Lifetimes.hdf5"

agb_file = h5py.File(agb_path,"r")
snii_file = h5py.File(snii_path,"r")
lifetime_file = h5py.File(lifetime_path,"r")

# Read liftimes info for all stars
mass_init_lt = lifetime_file["Masses"][:] # Msun
lifetime_lt = lifetime_file["Lifetimes"][:] # yr
Zstar_lt = lifetime_file["Metallicities"][:] # No units (not rel to solar) - see Fig A1 in Wiersma 09b for confirmation

# Read AGB mass loss for stars between 0.85 and 5 Msun
mass_init_agb = agb_file["Masses"][:]
Zstar_agb = agb_file["Metallicities"][:]

mass_loss_agb = np.zeros((len(Zstar_agb), len(mass_init_agb)))

for i_y, y in enumerate(agb_file["Yields"]):
    mass_loss_agb[i_y] = agb_file["Yields/"+y+"/Ejected_mass"][:]

# Read SNII mass loss for stars between 6 and 120 Msun
mass_init_snii = snii_file["Masses"][:]
Zstar_snii = snii_file["Metallicities"][:]

mass_loss_snii = np.zeros((len(Zstar_snii), len(mass_init_snii)))
y = snii_file["Yields/Z_0.004/"]

for i_y, y in enumerate(snii_file["Yields"]):
    mass_loss_snii[i_y] = snii_file["Yields/"+y+"/Ejected_mass"][:]

agb_file.close()
snii_file.close()
lifetime_file.close()

# For a Chabrier IMF, pretabulate the mass fraction associated with each stellar mass in the lifetime grid
mlo = 0.1
mhi = 100.

# Create custom grid for masses
bins_log_mass = np.linspace( np.log10(mlo), np.log10(mhi), 501)
delta_m = 10**bins_log_mass[1:] - 10**bins_log_mass[0:-1]
mass_init = 10**( 0.5*(bins_log_mass[1:] + bins_log_mass[0:-1]))

# See appendix A1 of Wiersma 09b
logMc = np.log10(0.079)
sigma = 0.69

m_dndm = np.exp(- np.square(np.log10( mass_init ) - logMc ) / (2*sigma**2))

# Use A=1
pivot = np.exp(- np.square(np.log10( 1.0 ) - logMc ) / (2*sigma**2))
B = pivot / (1**-1.3)

ok = (mass_init >= 1.0)
m_dndm[ok] = B * mass_init[ok]**-1.3

dndm = m_dndm / mass_init

norm = np.sum(delta_m*dndm*mass_init) # Total mass of the non-normalised IMF
dndm *= 1./norm

n_star = dndm * delta_m # Number of stars (per solar mass) in each mass bin for the lifetime grid

'''print "Number of stars per 100 Msun with initial mass > 6 Msun"
print np.sum(n_star_lt[mass_init_lt >= 6])*100

Msun = 2e33 # g
N_sne_s15 = 8.73*10.0**15 / 10.**51 * 100 * Msun # N SNe above 6 Msun for 100 Msun of stars - according to Schaye15

print "Number of stars per 100 Msun with initial mass > 6 Msun (implied by Schaye15)"
print N_sne_s15

quit()'''

########## Interpolate grids to compute the final stellar mass (for 1 Msun initial mass) as a function of metallicity and age #######

Zstar_grid = np.unique(np.concatenate((Zstar_agb, Zstar_snii)))

###### First interpolate to get the lifetime for each star initial mass and metallicity in the AGB and SNII grids #######

# Interpolate in metallicity
lifetime_agb = ui.Linear_Interpolation(Zstar_lt, lifetime_lt, Zstar_agb)
lifetime_snii = ui.Linear_Interpolation(Zstar_lt, lifetime_lt, Zstar_snii)

# Interpolate in initial star mass
lifetime_agb = ui.Linear_Interpolation( mass_init_lt, np.swapaxes(lifetime_agb,0,1), mass_init_agb)
lifetime_snii = ui.Linear_Interpolation( mass_init_lt, np.swapaxes(lifetime_snii,0,1), mass_init_snii)
lifetime_agb = np.swapaxes(lifetime_agb,0,1)
lifetime_snii = np.swapaxes(lifetime_snii,0,1)

###### Now  interpolate in metallicity to get the mass returned to ISM at end of lifetime, and lifetime, for AGB stars
n_mass_agb = len(mass_init_agb)
mass_loss_agb_interp = np.zeros((len(Zstar_grid), n_mass_agb))
lifetime_agb_interp = np.zeros((len(Zstar_grid), n_mass_agb))

ok = (Zstar_grid >= Zstar_agb.min()) & (Zstar_grid <= Zstar_agb.max())
mass_loss_agb_interp[ok] = ui.Linear_Interpolation( Zstar_agb, mass_loss_agb, Zstar_grid[ok])
lifetime_agb_interp[ok] = ui.Linear_Interpolation( Zstar_agb, lifetime_agb, Zstar_grid[ok])

# Set metallicity outside grid to end point
lo = Zstar_grid < Zstar_agb.min()
mass_loss_agb_interp[lo] += mass_loss_agb[0]
lifetime_agb_interp[lo] += lifetime_agb[0]

hi = Zstar_grid > Zstar_agb.max()
mass_loss_agb_interp[hi] += mass_loss_agb[-1]
lifetime_agb_interp[hi] += lifetime_agb[-1]

######  do the same for SNII
n_mass_snii = len(mass_init_snii)
mass_loss_snii_interp = np.zeros((len(Zstar_grid), n_mass_snii))
lifetime_snii_interp = np.zeros((len(Zstar_grid), n_mass_snii))

ok = (Zstar_grid >= Zstar_snii.min()) & (Zstar_grid <= Zstar_snii.max())
mass_loss_snii_interp = ui.Linear_Interpolation( Zstar_snii, mass_loss_snii, Zstar_grid)
lifetime_snii_interp = ui.Linear_Interpolation( Zstar_snii, lifetime_snii, Zstar_grid)

######### Now interpolate mass loss/lifetime for AGB and SNii onto a single grid (as function of metallicity, initial mass)

mass_loss = np.zeros((len(Zstar_grid), len(mass_init)))
lifetime = np.zeros((len(Zstar_grid), len(mass_init)))

# Add the AGB range
ok = (mass_init >= mass_init_agb.min()) & (mass_init <= mass_init_agb.max())

mass_loss_temp = ui.Linear_Interpolation( mass_init_agb, np.swapaxes(mass_loss_agb_interp,0,1), mass_init[ok])
mass_loss_temp = np.swapaxes(mass_loss_temp,0,1)

mass_loss[:,ok] += mass_loss_temp

lifetime_temp = ui.Linear_Interpolation( mass_init_agb, np.swapaxes(lifetime_agb_interp,0,1), mass_init[ok])
lifetime_temp = np.swapaxes(lifetime_temp,0,1)

lifetime[:,ok] += lifetime_temp

ok = (mass_init >= mass_init_snii.min()) & (mass_init <= mass_init_snii.max())

mass_loss_temp = ui.Linear_Interpolation( mass_init_snii, np.swapaxes(mass_loss_snii_interp,0,1), mass_init[ok])
mass_loss_temp = np.swapaxes(mass_loss_temp,0,1)

mass_loss[:,ok] += mass_loss_temp

lifetime_temp = ui.Linear_Interpolation( mass_init_snii, np.swapaxes(lifetime_snii_interp,0,1), mass_init[ok])
lifetime_temp = np.swapaxes(lifetime_temp,0,1)

lifetime[:,ok] += lifetime_temp

# Interpolate between the AGB/SNii regime
ok = (mass_init > mass_init_agb.max()) & (mass_init < mass_init_snii.min())

mass_loss_temp = ui.Linear_Interpolation( mass_init[ok==False], np.swapaxes(mass_loss[:,ok==False],0,1), mass_init[ok])
mass_loss_temp = np.swapaxes(mass_loss_temp,0,1)

mass_loss[:,ok] += mass_loss_temp

lifetime_temp = ui.Linear_Interpolation( mass_init[ok==False], np.swapaxes(lifetime[:,ok==False],0,1), mass_init[ok])
lifetime_temp = np.swapaxes(lifetime_temp,0,1)

lifetime[:,ok] += lifetime_temp

# Assume that stars with masses below the AGB range never explode

ok = mass_init < mass_init_agb.min()
lifetime[:,ok] += 2e10 # Longer than age of Universe for sensible cosmologies

lifetime *= 1e-9 # Gyr

def compute_stellar_mass_loss( zstar, mass_star_init, age):
    # Compute stellar mass loss for a given set of 1d arrays of zstar (no units), mass_star_init (Msun), and age (Gyr)

    # First, interpolate mass loss grids onto desired metallicity values - fixing values to endpoint values outside tabulated range
    mass_loss_interpZ = np.zeros((len(zstar), len(mass_init)))
    lifetime_interpZ = np.zeros((len(zstar), len(mass_init)))

    ok = (zstar >= Zstar_grid.min()) & (zstar <= Zstar_grid.max())
    mass_loss_interpZ[ok] = ui.Linear_Interpolation( Zstar_grid, mass_loss, zstar[ok])
    lifetime_interpZ[ok] = ui.Linear_Interpolation( Zstar_grid, lifetime, zstar[ok])

    lo = zstar < Zstar_grid.min()
    mass_loss_interpZ[lo] = mass_loss[0]
    lifetime_interpZ[lo] = lifetime[0]

    hi = zstar > Zstar_grid.max()
    mass_loss_interpZ[hi] = mass_loss[-1]
    lifetime_interpZ[hi] = lifetime[-1]

    # Now for each input metallicity, compute which stars have died at the input age
    died = np.swapaxes(lifetime_interpZ,0,1) <= age
    died = np.swapaxes(died,0,1)

    # Set mass loss for stars that haven't died to zero
    mass_loss_interpZ[died==False] = 0.0

    # Finally, compute the total mass across the IMF that was returned to ISM
    mass_loss_out = np.sum(mass_loss_interpZ * n_star, axis=1) * mass_star_init
    return mass_loss_out

if __name__ == "__main__":

    age = np.arange(0, 12.5, 0.03)
    mass_star_init = np.ones_like(age)
    zstar = 0.01 + np.zeros_like(age)
    zstar2 = 0.0 + np.zeros_like(age)

    mass_loss_out = compute_stellar_mass_loss(zstar, mass_star_init, age)
    mass_loss_out2 = compute_stellar_mass_loss(zstar2, mass_star_init, age)

    age_check, mass_check = np.loadtxt("forPeter.txt", unpack=True)
    age_check_z0, mass_check_z0 = np.loadtxt("forPeter_Z0.txt", unpack=True) # Zero metallicity version

    from utilities_plotting import *
    py.plot(age, mass_star_init - mass_loss_out,c="b")
    py.plot(age, mass_star_init - mass_loss_out2, linestyle='--',c="b")

    py.plot(age_check, mass_check,c="r")
    py.plot(age_check_z0, mass_check_z0,c="r",linestyle='--')

    fig_name = "stellar_mass_lost_test.pdf"
    py.rcParams.update({'savefig.dpi':150})
    py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
    py.show()
