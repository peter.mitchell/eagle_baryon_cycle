import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import match_searchsorted as ms
import measure_particles_functions as mpf
sys.path.append("Stellar_Mass_Loss")
import stellar_mass_loss as ml

props_track = ["mass_new_stars_init", "mass_stars_30kpc", "group_number", "subgroup_number", "mZ_new_stars_init", "mass_star"]
props_track += ["mass_gas_NSF_ISM", "mass_gas_NSF", "mass_gas_SF", "mass_star_init"]
props_track += ["mass_sf_ism_join_wind_0p25vmax", "mass_nsf_ism_join_wind_0p25vmax", "mass_ism_reheated_join_wind_0p25vmax"]
props_track += ["mass_galtran", "mass_prcooled", "mass_recooled_0p25vmax"]
props_track += ["mass_sf_ism_reheated", "mass_nsf_ism_reheated","mass_recooled_ireheat","mass_recooled_0p5vmax"]

props_track += ["mass_praccreted", "mass_reaccreted_0p25vmax", "mass_fof_transfer"]
props_track += ["mass_join_halo_wind_0p25vmax", "mass_halo_reheated_join_wind_0p25vmax"]
props_track += ["mass_reaccreted_0p5vmax", "mass_halo_reheated", "mass_reaccreted_hreheat"]

props_not_sat = ["mass_new_stars_init", "mZ_new_stars_init", "mass_sf_ism_join_wind_0p25vmax", "mass_nsf_ism_join_wind_0p25vmax", "mass_ism_reheated_join_wind_0p25vmax", "mass_galtran", "mass_prcooled", "mass_recooled_0p25vmax", "mass_sf_ism_reheated", "mass_nsf_ism_reheated", "mass_recooled_ireheat","mass_recooled_0p5vmax"]
props_not_sat += ["mass_praccreted", "mass_reaccreted_0p25vmax", "mass_fof_transfer", "mass_join_halo_wind_0p25vmax", "mass_halo_reheated_join_wind_0p25vmax", "mass_reaccreted_0p5vmax", "mass_halo_reheated", "mass_reaccreted_hreheat"]

test_mode = True
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc reference run, 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc REF with 200 snips
sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip_track_star"

# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

if not os.path.exists(catalogue_path+filename_subcat):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename_subcat
    quit()

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename,"r")

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "Looking at tree snapshot", snap

    

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    # Read the data
    if "Snap_"+str(snap) not in File:
        print "No data, skipping"
        continue

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]
    
    #for thing in subhalo_group:
    #    print thing
    #quit()

    if "node_index" not in subhalo_group:
        print "No data, skipping"
        continue

    node_index_ts = np.rint(subhalo_group["node_index"][:]).astype("int")

    if i_snap == 1:
        node_index_ns = node_index_ts
        
        z0_id_ns = np.arange(0, len(node_index_ns))
        z0_id_z0 = np.copy(z0_id_ns)

        data_track = {}
        for prop in props_track:
            data_track[prop] = np.zeros((len(snapshots),len(z0_id_ns)))

            data_track[prop][i_snap] += subhalo_group[prop][:]

        mass_ism_sat_track = np.zeros((len(snapshots),len(z0_id_ns)))
        mass_cgm_sat_track = np.zeros((len(snapshots),len(z0_id_ns)))
        mstar_init_sat_track = np.zeros((len(snapshots),len(z0_id_ns)))

        delta_mass_ism_sat_track = np.zeros((len(snapshots),len(z0_id_ns)))
        delta_mass_cgm_sat_track = np.zeros((len(snapshots),len(z0_id_ns)))
        delta_mstar_init_sat_track = np.zeros((len(snapshots),len(z0_id_ns)))

        node_index_ns_sat = []
        z0_id_ns_sat = []
        
        continue

    descendant_index_ts = np.rint(subhalo_group["descendant_index"][:]).astype("int")

    ptr = ms.match(descendant_index_ts,node_index_ns)
    ok_match = ptr >= 0

    z0_id_ts = np.zeros_like(descendant_index_ts)+np.nan
    z0_id_ts[ok_match] = z0_id_ns[ptr][ok_match]

    data_track_ts = {}
    for prop in props_track:
        data_track_ts[prop] = subhalo_group[prop][:]
    
    ok = (np.isnan(z0_id_ts)==False) & (np.isnan(data_track_ts["mass_new_stars_init"])==False)

    for prop in props_track:
        data_track_ts[prop] = data_track_ts[prop][ok]

    # If optimise_sats was turned on, we do not compute inflow/outflow rates/star formation rates for satellites
    # But some parts (outflow) may be turned on - so manually set them to zero here
    sat = data_track_ts["subgroup_number"]>0

    for prop in props_not_sat:
        data_track_ts[prop][sat] *= 0

    output = mpf.Sum_Common_ID(data_track_ts, z0_id_ts[ok], z0_id_z0, use_dict=True, dict_names=props_track)

    for prop in props_track:
        data_track[prop][i_snap] = output[prop]

    if len(node_index_ns_sat) > 0:
        ptr = ms.match( descendant_index_ts[ok], node_index_ns_sat)
        ok_match = ptr >= 0
        z0_id_ts_sat_ns = np.zeros_like(descendant_index_ts[ok])+np.nan
        z0_id_ts_sat_ns[ok_match] = z0_id_ns_sat[ptr][ok_match]
        
        ok2 = (np.isnan(z0_id_ts_sat_ns)==False) & (np.isnan(data_track_ts["mass_new_stars_init"])==False)

        if len(ok2[ok2]) > 0:
            inputs = [data_track_ts["mass_star_init"][ok2], data_track_ts["mass_gas_NSF_ISM"][ok2]+data_track_ts["mass_gas_SF"][ok2],data_track_ts["mass_gas_NSF"][ok2] - data_track_ts["mass_gas_NSF_ISM"][ok2]]
            output = mpf.Sum_Common_ID(inputs, z0_id_ts_sat_ns[ok2], z0_id_z0)
            delta_mstar_init_sat_track[i_snap-1] -= output[0]
            delta_mass_ism_sat_track[i_snap-1] -= output[1]
            delta_mass_cgm_sat_track[i_snap-1] -= output[2]

    if len(z0_id_ts[ok][sat]) > 0:
        mass_ism_sat = data_track_ts["mass_gas_SF"][sat] + data_track_ts["mass_gas_NSF_ISM"][sat]        
        mass_cgm_sat = data_track_ts["mass_gas_NSF"][sat] - data_track_ts["mass_gas_NSF_ISM"][sat]        
        mstar_init_sat = data_track_ts["mass_star_init"][sat]
        output = mpf.Sum_Common_ID([mass_ism_sat, mstar_init_sat, mass_cgm_sat], z0_id_ts[ok][sat], z0_id_z0)
        mass_ism_sat_track[i_snap] = output[0]
        mstar_init_sat_track[i_snap] = output[1]
        mass_cgm_sat_track[i_snap] = output[2]

        delta_mass_ism_sat_track[i_snap] += mass_ism_sat_track[i_snap]
        delta_mass_cgm_sat_track[i_snap] += mass_cgm_sat_track[i_snap]
        delta_mstar_init_sat_track[i_snap] += mstar_init_sat_track[i_snap]

        node_index_ns_sat = node_index_ts[ok][sat]
        z0_id_ns_sat = z0_id_ts[ok][sat]

    node_index_ns = node_index_ts[ok]
    z0_id_ns = z0_id_ts[ok]

    

# First preselect centrals at z=0 ( and remove final snapshot where there is no data)
ok = data_track["subgroup_number"][1] == 0
for prop in props_track:
    data_track[prop] = data_track[prop][1:,ok]
t_snapshots = t_snapshots[1:]

# Choose the halo to look at
ind = np.argmin(data_track["group_number"][0])

mstar = data_track["mass_star"][:,ind]
mstar_init = data_track["mass_star_init"][:,ind]
mstar_30kpc = data_track["mass_stars_30kpc"][:,ind]
mass_new_stars_init = data_track["mass_new_stars_init"][:,ind]
sfh = np.cumsum( mass_new_stars_init[::-1])[::-1]
Z_new_stars_init = data_track["mZ_new_stars_init"][:,ind] / mstar

mism_sat = mass_ism_sat_track[1:,ok][:,ind]
mstar_init_sat = mstar_init_sat_track[1:,ok][:,ind]

mism_sat_gain = delta_mass_ism_sat_track[1:,ok][:,ind]
mism_sat_loss = np.zeros_like(mism_sat_gain)
mism_sat_loss[mism_sat_gain<0] = -mism_sat_gain[mism_sat_gain<0]
mism_sat_gain[mism_sat_gain<0] *= 0

mstar_init_sat_gain = delta_mstar_init_sat_track[1:,ok][:,ind]
mstar_init_sat_loss = np.zeros_like(mstar_init_sat_gain)
mstar_init_sat_loss[mstar_init_sat_gain<0] = -mstar_init_sat_gain[mstar_init_sat_gain<0]
mstar_init_sat_gain[mstar_init_sat_gain<0] *= 0


mcgm_sat_gain = delta_mass_cgm_sat_track[1:,ok][:,ind]
mcgm_sat_loss = np.zeros_like(mcgm_sat_gain)

# Compute stellar mass loss
mass_loss = np.zeros_like(Z_new_stars_init)
for i, t in enumerate(t_snapshots):
    ok = t_snapshots <= t
    mass_loss[i] = np.sum(ml.compute_stellar_mass_loss( Z_new_stars_init[ok], mass_new_stars_init[ok], t))

mism = data_track["mass_gas_NSF_ISM"][:,ind] + data_track["mass_gas_SF"][:,ind]
mism_in = data_track["mass_prcooled"][:,ind] + data_track["mass_recooled_0p25vmax"][:,ind] + data_track["mass_galtran"][:,ind]
mism_out = data_track["mass_sf_ism_join_wind_0p25vmax"][:,ind] + data_track["mass_nsf_ism_join_wind_0p25vmax"][:,ind] + data_track["mass_ism_reheated_join_wind_0p25vmax"][:,ind]

mism_in_re = data_track["mass_recooled_ireheat"][:,ind] - (data_track["mass_recooled_0p25vmax"][:,ind] - data_track["mass_recooled_0p5vmax"][:,ind])

mism_out_re = data_track["mass_sf_ism_reheated"][:,ind] + data_track["mass_nsf_ism_reheated"][:,ind] - mism_out

# Note that these both are zero for satellites... (unlike sfh and mass_loss)
inflow_history = np.cumsum( mism_in[::-1])[::-1]
outflow_history = np.cumsum( mism_out[::-1])[::-1]

in_re_history = np.cumsum( mism_in_re[::-1] )[::-1]
out_re_history = np.cumsum( mism_out_re[::-1] )[::-1]

ism_sat_gain_history = np.cumsum( mism_sat_gain[::-1])[::-1]
ism_sat_loss_history = np.cumsum( mism_sat_loss[::-1])[::-1]
star_init_sat_gain_history = np.cumsum( mstar_init_sat_gain[::-1])[::-1]
star_init_sat_loss_history = np.cumsum( mstar_init_sat_loss[::-1])[::-1]

mcgm = data_track["mass_gas_NSF"][:,ind] - data_track["mass_gas_NSF_ISM"][:,ind]
mcgm_in = data_track["mass_praccreted"][:,ind] + data_track["mass_reaccreted_0p25vmax"][:,ind] + data_track["mass_fof_transfer"][:,ind]
mcgm_out = data_track["mass_join_halo_wind_0p25vmax"][:,ind] + data_track["mass_halo_reheated_join_wind_0p25vmax"][:,ind]
mcgm_in_re = data_track["mass_reaccreted_hreheat"][:,ind] - (data_track["mass_reaccreted_0p25vmax"][:,ind] - data_track["mass_reaccreted_0p5vmax"][:,ind])
mcgm_out_re = data_track["mass_halo_reheated"][:,ind] - mcgm_out

hin_history = np.cumsum( mcgm_in[::-1] )[::-1]
hout_history = np.cumsum( mcgm_out[::-1] )[::-1]
hin_re_history = np.cumsum( mcgm_in_re[::-1] )[::-1]
hout_re_history = np.cumsum( mcgm_out_re[::-1] )[::-1]

mcgm_sat_loss[mcgm_sat_gain<0] = -mcgm_sat_gain[mcgm_sat_gain<0]
mcgm_sat_gain[mcgm_sat_gain<0] *= 0
cgm_sat_gain_history = np.cumsum( mcgm_sat_gain[::-1])[::-1]
cgm_sat_loss_history = np.cumsum( mcgm_sat_loss[::-1])[::-1]


delta_t = t_snapshots[0:-1] - t_snapshots[1:]
delta_t *= 1e9 # yr

mdot_ism_in = mism_in[0:-1] / delta_t
mdot_ism_out = mism_out[0:-1] / delta_t
mdot_halo_in = mcgm_in[0:-1] / delta_t
mdot_halo_out = mcgm_out[0:-1] / delta_t
sfr = mass_new_stars_init[0:-1] / delta_t

mism_in_pr = data_track["mass_prcooled"][:,ind]
mism_in_rec = data_track["mass_recooled_0p25vmax"][:,ind]

mdot_ism_in_pr = mism_in_pr[0:-1] / delta_t
mdot_ism_in_rec = mism_in_rec[0:-1] / delta_t

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':6})

py.subplot(311)
ok = (t_snapshots[0:-1] > 0.0) & (t_snapshots[0:-1] < 3.0)
py.plot(t_snapshots[0:-1][ok], np.log10(sfr[ok]), c="k", label="SFR")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in[ok]), c="c", label="Inflow ISM")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_halo_in[ok]), c="c", label="Inflow Halo",linestyle='--')
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_out[ok]), c="r", label="Outflow ISM")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_halo_out[ok]), c="r", label="Outflow Halo",linestyle='--')

py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in_pr[ok]), c="b", label="First infall ISM",alpha=0.7)
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in_rec[ok]), c="m", label="Recycled ISM",alpha=0.7)

py.ylabel(r"$\dot{M} \, / \mathrm{M_\odot yr^{-1}}$")

py.subplot(312)
ok = (t_snapshots[0:-1] > 3) & (t_snapshots[0:-1] < 7.0)
py.plot(t_snapshots[0:-1][ok], np.log10(sfr[ok]), c="k", label="SFR")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in[ok]), c="c", label="Inflow ISM")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_halo_in[ok]), c="c", label="Inflow Halo",linestyle='--')
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_out[ok]), c="r", label="Outflow ISM")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_halo_out[ok]), c="r", label="Outflow Halo",linestyle='--')

py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in_pr[ok]), c="b", label="First infall ISM",alpha=0.7)
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in_rec[ok]), c="m", label="Recycled ISM",alpha=0.7)

py.ylabel(r"$\dot{M} \, / \mathrm{M_\odot yr^{-1}}$")

py.subplot(313)
ok = (t_snapshots[0:-1] > 7.0) & (t_snapshots[0:-1] < 30.0)

py.plot(t_snapshots[0:-1][ok], np.log10(sfr[ok]), c="k", label="SFR")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in[ok]), c="c", label="Inflow ISM")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_halo_in[ok]), c="c", label="Inflow Halo",linestyle='--')
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_out[ok]), c="r", label="Outflow ISM")
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_halo_out[ok]), c="r", label="Outflow Halo",linestyle='--')

py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in_pr[ok]), c="b", label="First infall ISM",alpha=0.7)
py.plot(t_snapshots[0:-1][ok], np.log10(mdot_ism_in_rec[ok]), c="m", label="Recycled ISM",alpha=0.7)

py.legend(loc="upper left",frameon=False)
py.xlabel(r"$t \, / \mathrm{Gyr}$")
py.ylabel(r"$\dot{M} \, / \mathrm{M_\odot yr^{-1}}$")


fig_name = "for_fun.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)

py.show()
