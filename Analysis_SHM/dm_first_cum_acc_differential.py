import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us
import match_searchsorted as ms
import measure_particles_functions as mpf

sim_name = "L0100N1504_REF_200_snip"
z_plot = [0.0083]

show_recal = True
sim_name2 = "L0025N0752_Recal"

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star","mass_star_init"]
props += ["m200_host","isInterpolated","node_index"]

props_integrator = ["stars","dm_first_acc"]

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"
filename2 = "subhalo_catalogue_" + sim_name2+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

# Also read the actual Mdm values, which are in the master subhalo catalogue, but not in the processed measurements files 
mass_dm_list = []
nI_list = []

for z in z_plot:
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]
    subhalo_group = snapnum_group["subhalo_properties"]
    g2Msun = 1./(1.989e33)
    mass_dm_list.append( subhalo_group["Mdm"][:] * g2Msun * 1.989e43 /h )
    nI_list.append( subhalo_group["nodeIndex"][:] )

File.close()

File2 = h5py.File(catalogue_path+filename2,"r")
#### Get tree time/redshift info ########
tree_snapshot_info2 = File2["tree_snapshot_info"]
snapshot_numbers2 = tree_snapshot_info2["snapshots_tree"][:]
z_snapshots2 = tree_snapshot_info2["redshifts_tree"][:]
t_snapshots2 = tree_snapshot_info2["cosmic_times_tree"][:]
sn_i_a_pshots_simulation2 = tree_snapshot_info2["sn_i_a_pshots_simulation"][:]

snapshots2 = snapshot_numbers2
snipshots2 = sn_i_a_pshots_simulation2

# Also read the actual Mdm values, which are in the master subhalo catalogue, but not in the processed measurements files 
mass_dm_list2 = []
nI_list2 = []

for z in z_plot:
    i_snap2 = np.argmin(abs(z_snapshots2-z))

    snap2 = snapshots2[i_snap2]

    snapnum_group2 = File2["Snap_"+str(snap2)]
    subhalo_group2 = snapnum_group2["subhalo_properties"]
    mass_dm_list2.append( subhalo_group2["Mdm"][:] * g2Msun * 1.989e43 /h )
    nI_list2.append( subhalo_group2["nodeIndex"][:] )

File2.close()


# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

data = []

for iz, z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    ok = (np.isnan(subhalo_group["mass_star"][:])==False)
    data.append({})
    for prop in props:
        data[-1][prop] = subhalo_group[prop][ok]

    # Match with base subhalo catalogue in order to get the dark matter subhalo mass of each subhalo                                         
    ptr = ms.match(data[-1]["node_index"], nI_list[iz])
    ok_match = ptr >= 0
    data[-1]["Mdm"] = np.zeros(len(data[-1]["node_index"]))
    data[-1]["Mdm"][ok_match] = mass_dm_list[iz][ptr][ok_match]

filename2 = "integrator_output_"+sim_name+"_fixed_everything.hdf5"
File2 = h5py.File(ode_path+filename2,"r")

for iz,z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]
    subhalo_group = snapnum_group["subhalo_properties"]
    ok = (np.isnan(subhalo_group["mass_star"][:])==False)

    snapnum_group2 = File2["Snap_"+str(snap)]

    for prop in props_integrator:

        data[iz][prop] = snapnum_group2[prop][ok]

File2.close()
File.close()

filename2a = "processed_subhalo_catalogue_" + sim_name2 + ".hdf5"

File2a = h5py.File(input_path+filename2a,"r")

data2 = []

for iz, z in enumerate(z_plot):
    i_snap2 = np.argmin(abs(z_snapshots2-z))

    snap2 = snapshots2[i_snap2]
    snip2 = snipshots2[i_snap2]

    snapnum_group2 = File2a["Snap_"+str(snap2)]

    subhalo_group2 = snapnum_group2["subhalo_properties"]

    ok = (np.isnan(subhalo_group2["mass_star"][:])==False)
    data2.append({})
    for prop in props:
        data2[-1][prop] = subhalo_group2[prop][ok]

    # Match with base subhalo catalogue in order to get the dark matter subhalo mass of each subhalo                                         
    ptr = ms.match(data2[-1]["node_index"], nI_list2[iz])
    ok_match = ptr >= 0
    data2[-1]["Mdm"] = np.zeros(len(data2[-1]["node_index"]))
    data2[-1]["Mdm"][ok_match] = mass_dm_list2[iz][ptr][ok_match]

filename2b = "integrator_output_"+sim_name2+"_fixed_everything.hdf5"
File2b = h5py.File(ode_path+filename2b,"r")

for iz,z in enumerate(z_plot):
    i_snap2 = np.argmin(abs(z_snapshots2-z))

    snap2 = snapshots2[i_snap]
    snip2 = snipshots2[i_snap]

    snapnum_group2a = File2a["Snap_"+str(snap2)]
    subhalo_group2 = snapnum_group2["subhalo_properties"]
    ok = (np.isnan(subhalo_group2["mass_star"][:])==False)

    snapnum_group2b = File2b["Snap_"+str(snap2)]

    for prop in props_integrator:

        data2[iz][prop] = snapnum_group2b[prop][ok]

File2a.close()
File2b.close()









######### Plotting ################
print "Plotting"

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.25,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.1,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'figure.subplot.right':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.7
ylo = -3.; yhi = -1.3

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.yaxis.get_major_ticks():
        tick.label1On=True
        tick.label2On=False
    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True
    
    # Show curves for different halo mass bins
    c_list = ["k","b","c","g","r","m"]
    bin_lo = [9.75, 10.75, 11.75, 12.75, 13.75]
    bin_hi = [10.25, 11.25, 12.25, 13.25, 14.25]

    if i == 0:
        py.xlabel(r"$\log_{10}(M_{200} \, / M_{\mathrm{\odot}} )$")
        py.ylabel(r"$M_{\mathrm{DM,cent}} \, / M_{\mathrm{DM,tot}}$")

    if i == 2:
        py.xlabel(r"$\log_{10}(M_{200} \, / M_{\mathrm{\odot}} )$")
        py.ylabel(r"$M_{\mathrm{DM,cent}}^{\mathrm{1st}} \, / M_{\mathrm{DM,tot}}^{\mathrm{1st}}$")
        

    if i == 1:
        var = data[iz]["Mdm"]
        py.xlabel(r"$\log_{10}(M_{\mathrm{DM,sat}} \, / M_{\mathrm{DM,tot}} )$")
        py.ylabel(r"$\mathrm{d}N_{\mathrm{sat}} / \mathrm{d}\log_{10}(M_{\mathrm{DM,sat}} \, / M_{\mathrm{DM,tot}})$")
        var2 = data2[iz]["Mdm"]
    
    if i == 3:
        var = data[iz]["dm_first_acc"]
        py.xlabel(r"$\log_{10}(M_{\mathrm{DM,sat}}^{\mathrm{1st}} \, / M_{\mathrm{DM,tot}}^{\mathrm{1st}} )$")
        py.ylabel(r"$\mathrm{d}N_{\mathrm{sat}} / \mathrm{d}\log_{10}(M_{\mathrm{DM,sat}}^{\mathrm{1st}} \, / M_{\mathrm{DM,tot}}^{\mathrm{1st}})$")
        var2 = data2[iz]["dm_first_acc"]

    i_snap = np.argmin(abs(z_snapshots-z_plot[0]))
    iz = 0
    if i == 1 or i == 3:
        for im in range(len(bin_lo)):

            bin_i = (np.log10(data[iz]["m200_host"]) > bin_lo[im]) & (np.log10(data[iz]["m200_host"]) < bin_hi[im]) & (var>0)
            bin_i2 = (np.log10(data2[iz]["m200_host"]) > bin_lo[im]) & (np.log10(data2[iz]["m200_host"]) < bin_hi[im]) & (var2>0)

            if len(bin_i[bin_i]) > 0:

                cent = data[iz]["subgroup_number"][bin_i]==0
                dm_first_acc_fof = np.copy(var[bin_i])
                dm_first_acc_fof[cent] = mpf.Sum_Common_ID([dm_first_acc_fof], data[iz]["group_number"][bin_i], data[iz]["group_number"][bin_i][cent])[0]

                ptr = ms.match(data[iz]["group_number"][bin_i], data[iz]["group_number"][bin_i][cent])
                dm_first_acc_fof = dm_first_acc_fof[cent][ptr]

                y = var[bin_i] / dm_first_acc_fof

                # Bins of DM_first_acc (subhalo) / m200_host
                bin_size = 0.2
                bin_frac = np.arange(-5.0, 0.2, bin_size)
                mid_bin = 0.5*(bin_frac[1:]+bin_frac[0:-1])

                weights = y / float(len(y[cent])) / bin_size

                sat = cent==False

                counts = np.histogram(np.log10(y[sat]),bins=bin_frac,weights=weights[sat])[0]

                #print i, bin_lo[im]+0.25, np.sum(weights), np.sum(counts)

                if i== 1:
                    label = r"$%3.2f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.2f$" % (bin_lo[im], bin_hi[im])
                else:
                    label = ""
                py.plot(mid_bin, counts, c=c_list[im], linewidth=1.2, label = label)

                # For each m200 bin, plot the 100 dm particle limit (for ref eagle only)
                mdm_eagle = 9.7*10**7
                py.axvline(np.log10(100*mdm_eagle/10**(bin_lo[im]+0.25)), c=c_list[im], alpha=0.5, linewidth=1.2,linestyle='--')

            if len(bin_i2[bin_i2]) > 0:

                cent2 = data2[iz]["subgroup_number"][bin_i2]==0
                dm_first_acc_fof2 = np.copy(var2[bin_i2])
                dm_first_acc_fof2[cent2] = mpf.Sum_Common_ID([dm_first_acc_fof2], data2[iz]["group_number"][bin_i2], data2[iz]["group_number"][bin_i2][cent2])[0]

                ptr = ms.match(data2[iz]["group_number"][bin_i2], data2[iz]["group_number"][bin_i2][cent2])
                dm_first_acc_fof2 = dm_first_acc_fof2[cent2][ptr]

                y2 = var2[bin_i2] / dm_first_acc_fof2

                weights2 = y2 / float(len(y2[cent2])) / bin_size

                sat2 = cent2==False

                counts2 = np.histogram(np.log10(y2[sat2]),bins=bin_frac,weights=weights2[sat2])[0]

                py.plot(mid_bin, counts2, c=c_list[im], linewidth=1.2,linestyle='-.')

                # For each m200 bin, plot the 100 dm particle limit
                #mdm_eagle = 9.7*10**7 * 0.125
                #py.axvline(np.log10(100*mdm_eagle/10**(bin_lo[im]+0.25)), c=c_list[im], alpha=0.5, linewidth=1.2,linestyle=':')

            
            xlo = -5.; xhi = 0.1
            ylo = 0.0; yhi = 0.27

    else:
        bin_mh = np.arange(9.0, 13.0,0.2)
        bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
        bin_mh = np.concatenate((bin_mh, bin_mh2))
 
        if i == 0:
            var = data[iz]["Mdm"]
            var2 = data2[iz]["Mdm"]
        if i == 2:
            var = data[iz]["dm_first_acc"]
            var2 = data2[iz]["dm_first_acc"]

        cent = data[iz]["subgroup_number"]==0
        cent2 = data2[iz]["subgroup_number"]==0
        
        var_fof = np.copy(var)
        var_fof[cent] = mpf.Sum_Common_ID([var], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
        y = var / var_fof

        var_fof2 = np.copy(var2)
        var_fof2[cent2] = mpf.Sum_Common_ID([var2], data2[iz]["group_number"], data2[iz]["group_number"][cent2])[0]
        y2 = var2 / var_fof2

        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"][cent]),y[cent],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        # Use bigger bins for recal
        bin_mh = np.arange(9.0, 12.3, 0.3)
        bin_mh2 = np.array([13.0, 14.0, 15.0])
        bin_mh = np.concatenate((bin_mh, bin_mh2))

        med2, lo2, hi2, mid_bin2 = us.Calculate_Percentiles(bin_mh,np.log10(data2[iz]["m200_host"][cent2]),y2[cent2],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, med, c=c_list[iz], linewidth=1.2)
        py.plot(mid_bin, lo, c=c_list[iz],alpha=0.4)
        py.plot(mid_bin, hi, c=c_list[iz],alpha=0.4)
        xlo = 10.0; xhi = 14.7
        ylo = 0.0; yhi = 1.0

        if show_recal:
            py.plot(mid_bin2, med2, c=c_list[iz], linewidth=1.2,linestyle='-.')

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

    py.legend(loc="upper left",frameon=False)

fig_name = "dm_first_acc_frac_differential.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
