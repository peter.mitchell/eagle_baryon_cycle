# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us


sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

variable = "stars"
#variable = "ism"
#variable = "cgm"

# Choose whether to show the actual simulation using the 30kpc aperture for stellar mass
use_aperture = False

variations =      ["",         "_no_prev_cgm",                "_fixed_tau_infall", "_fixed_tau_ret_gal",         "_fixed_tau_ret_halo",     "_fixed_eta_gal", "_fixed_eta_halo"]
variation_names = ["Fiducial", r"Halo-scale accretion ($f_{\mathrm{acc}}^{\mathrm{halo}}$)", r"Galaxy-scale accretion ($G_{\mathrm{acc}}^{\mathrm{gal}}$)", r"Galaxy-scale recycling ($G_{\mathrm{ret}}^{\mathrm{gal}}$)", r"Halo-scale recycling ($G_{\mathrm{ret}}^{\mathrm{halo}}$)", r"Galaxy-scale outflow ($\eta^{\mathrm{gal}}$)", r"Halo-scale outflow ($\eta^{\mathrm{halo}}$)"]
c_list =          ["k",        "b",                           "b",                 "g",                          "g",                       "r",              "r"              ]
ls_list =         ["-",        "--",                          ":",                 ":",                          "--",                      ":",              "--"             ]

variations += ["_fixed_tau_ret_both", "_fixed_eta_both",        "_fixed_everything", "_fixed_tau_SF", "_fixed_inflows"]
variation_names += [r"Recycling ($G_{\mathrm{ret}}^{\mathrm{gal}}$,$G_{\mathrm{ret}}^{\mathrm{halo}}$)", r"Outflow ($\eta^{\mathrm{gal}}$,$\eta^{\mathrm{halo}}$)", r"All",      r"Star formation ($G_{\mathrm{SF}}$)", r"Accretion ($G_{\mathrm{acc}}^{\mathrm{gal}}$, $G_{\mathrm{acc}}^{\mathrm{halo}}$)"]
c_list +=     ["g",                   "r",                      "y",                 "m",             "b"           ]
ls_list +=    ["-",                    "-",                     "-",                 "-",             "-"           ]



panel1 = ["", "_no_prev_cgm", "_fixed_tau_infall", "_fixed_inflows"]
panel2 = ["", "_fixed_eta_gal", "_fixed_eta_halo", "_fixed_eta_both"]
panel3 = ["", "_fixed_tau_ret_gal", "_fixed_tau_ret_halo", "_fixed_tau_ret_both"]
panel4 = ["", "_fixed_everything", "_fixed_inflows", "_fixed_eta_both", "_fixed_tau_ret_both", "_fixed_tau_SF"]

props = ["mass_stars_30kpc", "subgroup_number", "mass_star", "mass_gas_SF", "mass_gas_NSF_ISM", "mass_gas_NSF"]
props += ["m200_host"]

props_integrator = ["stars", "ism", "cgm_pr","cgm_wind"]

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()

# Read integrator output, looping over variations
for variant in variations:

    filename2 = "integrator_output_"+sim_name +variant+".hdf5"
    File2 = h5py.File(ode_path+filename2,"r")
    snapnum_group2 = File2["Snap_"+str(snap)]
    
    for prop in props_integrator:

        data[prop+variant] = snapnum_group2[prop][ok]

    File2.close()

print "plotting"

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':9})


py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)

bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

tweaked_list = []

for n,ax in enumerate(subplots):
    py.axes(ax)

    if variable == "stars":
        if use_aperture:
            mstar = data["mass_stars_30kpc"]
        else:
            mstar = data["mass_star"]
    elif variable == "cgm":
        mstar = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]
    elif variable == "ism":
        mstar = data["mass_gas_SF"] + data["mass_gas_NSF_ISM"]
    else:
        print "no", variable
        quit()

    if variable != "stars":
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),mstar/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        py.plot(mid_bin, np.log10(med), c="k",linewidth=1.2, alpha=0.5)
        #py.plot(mid_bin, np.log10(lo), c="k",linewidth=0.7, alpha=0.5)
        #py.plot(mid_bin, np.log10(hi), c="k",linewidth=0.7, alpha=0.5)

    if n == 0:
        panel_list = panel1
    if n == 1:
        panel_list = panel2
    if n == 2:
        panel_list = panel3
    if n == 3:
        panel_list = panel4

    for i, variant in enumerate(panel_list):
        if variable == "cgm":
            if variant not in tweaked_list:
                data["cgm_pr"+variant] += data["cgm_wind"+variant]
                tweaked_list.append(variant)
            variable_var = "cgm_pr"
        else:
            variable_var = variable

        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data[variable_var+variant]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        for i2, variant_i in enumerate(variations):
            if variant == variant_i:
                i_var = i2

        lw = 1.
        if variant == "":
            lw = 1.6
            py.plot(mid_bin, np.log10(med), c=c_list[i_var],linewidth=lw,linestyle=ls_list[i_var])
        else:
            py.plot(mid_bin, np.log10(med), c=c_list[i_var], label = variation_names[i_var],linewidth=lw,linestyle=ls_list[i_var])

    if variable == "stars":
        fs = 9
    else:
        fs = 8
    legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,handlelength=3,fontsize=fs)#,bbox_to_anchor=(0.1,1.0))
    py.gca().add_artist(legend1)
    
    l1, = py.plot([-20,-20],[-20,20],label=r"Fiducial",c="k",linewidth=1.6)
    if n == 0:
        xyt = (0.6, 0.35)
    elif n == 1:
        xyt = (0.6, 0.35)
    elif n == 2:
        xyt = (0.6, 0.35)
    else:
        xyt = (0.7, 0.55)

    if n == 1:
        py.annotate(r"Dependence on $M_{200}$ removed for:", (11.0, -2.51))
    elif n < 3:
        py.annotate(r"Dependence on $M_{200}$ removed for:", (10.9, -2.48))
    else:
        py.annotate(r"Dependence on $M_{200}$ removed for:", (11.5, -2.21))

    py.legend(handles=[l1],labels=[r"Fiducial"], loc="lower right",frameon=False,numpoints=1,handlelength=3, bbox_to_anchor=xyt)

    py.xlim((10., 14.7))
    if variable == "stars":
        py.ylim((-3.0,-1.5))
    elif variable == "cgm":
        py.ylim((-2.25, -0.75))
    elif variable == "ism":
        py.ylim((-4.25, -1.75))

    if n == 0 or n ==2:
        if variable == "stars":
            py.ylabel(r"$\log_{10}(M_\star \, / M_{200})$")
        elif variable == "ism":
            py.ylabel(r"$\log_{10}(M_{\mathrm{ISM}} \, / M_{200})$")
        elif variable == "cgm":
            py.ylabel(r"$\log_{10}(M_{\mathrm{CGM}} \, / M_{200})$")
    if n == 2 or n==3:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_{\odot}})$")
        
if variable == "stars":
    fig_name = "shm_fixed_efficiencies.pdf"
elif variable == "cgm":
    fig_name = "cgm_fixed_efficiencies.pdf"
elif variable == "ism":
    fig_name = "ism_fixed_efficiencies.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)


py.show()
