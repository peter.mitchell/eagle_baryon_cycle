# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

# Options are stars, ism, cgm
#variable = "stars"
#variable = "cgm"
variable = "ism"

variations = ["_no_nuisance"]
variation_names = ["Fiducial"]
variation_vars = [""]
panels = [20] #Dummy

boost_factors = [0.125, 0.25, 0.5, 1.0, 2.0, 4.0, 8.0]
#boost_factors = [0.1, 0.25, 0.5, 1.0, 2.0, 4.0, 10.0]
c_list = ["b", "c", "g", "k", "orange", "m", "r"]


variations_b = ["_boost_eta_gal_",         "_boost_eta_halo_",     "_boost_tau_ret_gal_", "_boost_tau_ret_halo_"]
variation_names_b = [r"Galaxy-scale outflow", r"Halo-scale outflow", r"Galaxy-scale recycling", r"Halo-scale recycling"]
variation_vars_b = [r"$\eta^{\mathrm{gal}}$", r"$\eta^{\mathrm{halo}}$", r"$G_{\mathrm{ret}}^{\mathrm{gal}}$", r"$G_{\mathrm{ret}}^{\mathrm{halo}}$"]
panels_b = [5,                     1 ,                         6,                    2]

variations_b += ["_boost_prev_fb_",          "_boost_tau_infall_",      "_boost_tau_SF_"]
variation_names_b += [r"Halo-scale accretion" , r"Galaxy-scale accretion", r"Star formation"]
variation_vars_b += [r"$f_{\mathrm{acc}}^{\mathrm{halo}}$", r"$G_{\mathrm{acc}}^{\mathrm{gal}}$", r"$G_{\mathrm{SF}}$"]
panels_b += [0,                       4,                             3]

for i_b in range(len(boost_factors)):
    boost_factor = str(boost_factors[i_b]).replace(".","p")
    if "1p0" in boost_factor:
        continue
    for variation_b, variation_name_b, variation_var_b, panel_b in zip(variations_b, variation_names_b, variation_vars_b, panels_b):
        variations.append(variation_b+boost_factor)
        variation_names.append(variation_name_b)
        variation_vars.append(variation_var_b)
        panels.append(panel_b)

props = ["mass_stars_30kpc", "subgroup_number", "mass_star"]
props += ["m200_host"]

props_integrator = ["stars","ism","cgm_pr","cgm_wind"]

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()


# Read integrator output, looping over variations
for variant in variations:

    print "Reading", variant

    filename2 = "integrator_output_"+sim_name +variant+".hdf5"
    File2 = h5py.File(ode_path+filename2,"r")
    snapnum_group2 = File2["Snap_"+str(snap)]
    
    for prop in props_integrator:

        data[prop+variant] = snapnum_group2[prop][ok]

    File2.close()

print "plotting"


from utilities_plotting import *
npanels = 7; nrow = 2; ncol = 4
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*1.5],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.13,
                    'figure.subplot.top':0.99,
                    'axes.labelsize':9,
                    'legend.fontsize':7})


py.figure()
np.seterr(all='ignore')
subplots = panelplot_flexible(npanels,nrow,ncol)

bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

xlo = 10.05; xhi = 15.

if variable == "stars":
    ylo = -3.5; yhi = -1.2
elif variable == "ism":
    ylo = -4.25; yhi = -1.75
elif variable == "cgm":
    ylo = -3.25; yhi = -0.75

for n,ax in enumerate(subplots):
    py.axes(ax)


    '''med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data["mass_stars_30kpc"]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, np.log10(med), c="k", label = sim_name,linewidth=1.2, alpha=0.5)
    py.plot(mid_bin, np.log10(lo), c="k",linewidth=0.7, alpha=0.5)
    py.plot(mid_bin, np.log10(hi), c="k",linewidth=0.7, alpha=0.5)'''

    if variable == "cgm":
        if n == 0:
            data[variable+"_pr_no_nuisance"] += data[variable+"_wind_no_nuisance"]
            variable_fid = "cgm_pr"
    else:
        variable_fid = variable

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data[variable_fid+"_no_nuisance"]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, np.log10(med), c="k", linewidth=1.3,linestyle='-')

    im = np.argmin(abs(mid_bin - 12))-1
    #py.plot( [mid_bin[im],mid_bin[im]], [np.log10(med[im]*0.1), np.log10(med[im]*10)], c="k")
    py.plot( [mid_bin[im],mid_bin[im]], [np.log10(med[im]*0.125), np.log10(med[im]*8)], c="k")
    #py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*0.1), np.log10(med[im]*0.1)], c="k")
    py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*0.125), np.log10(med[im]*0.125)], c="k")
    py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*0.25), np.log10(med[im]*0.25)], c="k")
    py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*0.5), np.log10(med[im]*0.5)], c="k")
    py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*2), np.log10(med[im]*2)], c="k")
    py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*4), np.log10(med[im]*4)], c="k")
    #py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*10), np.log10(med[im]*10)], c="k")
    py.plot( [mid_bin[im]-0.05,mid_bin[im]+0.05], [np.log10(med[im]*8), np.log10(med[im]*8)], c="k")

    for i, i_panel in enumerate(panels):

        if i_panel == n:
            variant = variations[i]
            
            for i_boost, boost_factor in enumerate(boost_factors):
                if str(boost_factor).replace(".","p") in variant:
                    c = c_list[i_boost]
                
                    for i2, variant_i in enumerate(variations):
                        if variant == variant_i:
                            i_var = i2
                    if i_boost == 0:
                        py.plot([-20,-10],[-20,-10], c="k", linewidth=1.3, linestyle='-')#, label=variation_names[i_var])
                        if variable == "stars":
                            py.annotate(variation_names[i_var], (10.5, -3.35),fontsize=8.0)
                            py.annotate(variation_vars[i_var], (11.7, -3.1),fontsize=9.0)
                        elif variable == "ism":
                            py.annotate(variation_names[i_var], (10.5, -4.1),fontsize=8.0)
                            py.annotate(variation_vars[i_var], (11.4, -3.8),fontsize=9.0)

                        else:
                            py.annotate(variation_names[i_var], (10.5, -3.1),fontsize=8.0)
                            py.annotate(variation_vars[i_var], (11.7, -2.8),fontsize=9.0)
                    break

            if variable == "cgm":
                data["cgm_pr"+variant] += data["cgm_wind"+variant]
                variable_var = "cgm_pr"
            else:
                variable_var = variable

            med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),data[variable_var+variant]/data["m200_host"],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

            py.plot(mid_bin, np.log10(med), c=c,linewidth=1.4,linestyle="-")
    
    #legend1 = py.legend(loc='lower center',frameon=False)
    #py.gca().add_artist(legend1)

    if n == 0:
        labels = []
        lines = []
        for boost_factor,c  in zip(boost_factors,c_list):
            labels += [r"$\times$"+str(boost_factor)]
            line, = py.plot(bin_mh-99,bin_mh-99,c=c,linewidth=1.3)
            lines.append(line)
        py.legend(handles=lines, labels=labels,loc="upper left", frameon=False,numpoints=1,bbox_to_anchor=(3.2, -0.4))


    py.xlim((xlo, xhi))
    py.ylim((ylo,yhi))

    if n == 0 or n == 4:
        if variable == "stars":
            py.ylabel(r"$\log_{10}(M_\star \, / M_{200})$")
        if variable == "ism":
            py.ylabel(r"$\log_{10}(M_{\mathrm{ISM}} \, / M_{200})$")
        if variable == "cgm":
            py.ylabel(r"$\log_{10}(M_{\mathrm{CGM}} \, / M_{200})$")
    if n >= 3:
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_{\odot}})$")
        
if variable == "stars":
    fig_name = "shm_boost_efficiencies.pdf"
elif variable == "ism":
    fig_name = "ism_boost_efficiencies.pdf"
elif variable == "cgm":
    fig_name = "cgm_boost_efficiencies.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)


py.show()
