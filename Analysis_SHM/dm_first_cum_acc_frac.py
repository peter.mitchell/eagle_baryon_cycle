import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us
import match_searchsorted as ms
import measure_particles_functions as mpf

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = [0.0083, 0.5, 1.0, 2.0, 3.0]

denominator = "m200_host"
#denominator = "mchalo" # Use subhalo DM mass as denominator instead of M200 - note that for centrals this is just mchalo (I double-checked)

sum_sats = True # Choose whether to add the smooth acc of DM onto (non-merged) satellites into the numerator (and denominator for mchalo option)

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star","mass_star_init", "node_index"]
props += ["m200_host","isInterpolated", "mchalo"]

props_integrator = ["stars","dm_first_acc"]

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

if denominator == "mchalo":
    dm2tot_factor = 1.0

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

# In this case we need the actual Mdm values, which are in the master subhalo catalogue, but not in the processed measurements files
if denominator == "mchalo" and sum_sats:
    mass_dm_list = []
    nI_list = []

    for z in z_plot:
        i_snap = np.argmin(abs(z_snapshots-z))

        snap = snapshots[i_snap]

        snapnum_group = File["Snap_"+str(snap)]
        subhalo_group = snapnum_group["subhalo_properties"]
        g2Msun = 1./(1.989e33)
        mass_dm_list.append( subhalo_group["Mdm"][:] * g2Msun * 1.989e43 /h ) 
        nI_list.append( subhalo_group["nodeIndex"][:] )

File.close()

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

data = []

File = h5py.File(input_path+filename,"r")

for iz, z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    if sum_sats:
        ok = (np.isnan(subhalo_group["mass_star"][:])==False)
    else:
        ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
    data.append({})
    for prop in props:
        data[-1][prop] = subhalo_group[prop][ok]

    if denominator == "mchalo" and sum_sats:
        # Match with base subhalo catalogue in order to get the dark matter subhalo mass of each subhalo
        ptr = ms.match(data[-1]["node_index"], nI_list[iz])
        ok_match = ptr >= 0
        data[-1]["Mdm"] = np.zeros(len(data[-1]["node_index"]))
        data[-1]["Mdm"][ok_match] = mass_dm_list[iz][ptr][ok_match]

filename2 = "integrator_output_"+sim_name+".hdf5"
File2 = h5py.File(ode_path+filename2,"r")

for iz,z in enumerate(z_plot):
    i_snap = np.argmin(abs(z_snapshots-z))

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    snapnum_group = File["Snap_"+str(snap)]
    subhalo_group = snapnum_group["subhalo_properties"]
    if sum_sats:
        ok = (np.isnan(subhalo_group["mass_star"][:])==False)
    else:
        ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)

    snapnum_group2 = File2["Snap_"+str(snap)]

    for prop in props_integrator:

        data[iz][prop] = snapnum_group2[prop][ok]

File2.close()
File.close()

######### Plotting ################
print "Plotting"

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.7
ylo = 0.0; yhi = 1.5

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    c_list = ["k","b","c","g","r","m"]

    for iz,z in enumerate(z_plot):
        i_snap = np.argmin(abs(z_snapshots-z))

        cent = data[iz]["subgroup_number"]==0
        if sum_sats:
            data[iz]["dm_first_acc"][cent] = mpf.Sum_Common_ID([data[iz]["dm_first_acc"]], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
            if denominator == "mchalo":
                data[iz]["Mdm"][cent] = mpf.Sum_Common_ID([data[iz]["Mdm"]], data[iz]["group_number"], data[iz]["group_number"][cent])[0]
                data[iz][denominator] = data[iz]["Mdm"]

        frac_dm_first = data[iz]["dm_first_acc"][cent] * dm2tot_factor / data[iz][denominator][cent]

        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data[iz]["m200_host"][cent]),frac_dm_first,weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, med, c=c_list[iz], label=sim_name,linewidth=1.2)
        #py.plot(mid_bin, lo, c=c_list[iz],alpha=0.8)
        #py.plot(mid_bin, hi, c=c_list[iz],alpha=0.8)


    py.ylabel(r"Fraction DM-1st-time")
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
    
    py.axhline(1.0,c="k",linestyle='--', alpha=0.4)

    #py.legend(loc="upper left",frameon=False)

fig_name = "dm_first_cum_acc_frac.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
