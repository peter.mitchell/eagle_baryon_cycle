# This script looks at the output of the ODE integrator compared to the raw data for a single galaxy
import numpy as np
import h5py
import os
import warnings
import sys
sys.path.append("../.")
import utilities_statistics as us

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"
z_plot = 0.0083

props = ["mass_stars_30kpc", "group_number", "subgroup_number", "mass_star"]
props += ["mass_gas_NSF_ISM", "mass_gas_NSF", "mass_gas_SF"]
props += ["m200_host","isInterpolated"]
props += ["mass_outside_galaxy_0p25vmax", "mass_outside_0p25vmax"]

props_integrator = ["cgm","ism","stars"]

show_ej = False # Choose whether fourth panel shows ej or wind

fV_str = "_0p25vmax"

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
ode_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name+ ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(catalogue_path+filename_subcat,"r")
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()

i_snap = np.argmin(abs(z_snapshots-z_plot))

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

snap = snapshots[i_snap]
snip = snipshots[i_snap]

snapnum_group = File["Snap_"+str(snap)]

subhalo_group = snapnum_group["subhalo_properties"]

ok = (np.isnan(subhalo_group["mass_star"][:])==False) & (subhalo_group["subgroup_number"][:]==0)
data = {}
for prop in props:
    data[prop] = subhalo_group[prop][ok]

File.close()

filename2 = "integrator_output_"+sim_name+".hdf5"
File2 = h5py.File(ode_path+filename2,"r")
snapnum_group2 = File2["Snap_"+str(snap)]

for prop in props_integrator:

    data[prop] = snapnum_group2[prop][ok]

File2.close()

filename3 = "integrator_decoupled_output_"+sim_name+".hdf5"
File3 = h5py.File(ode_path+filename3,"r")
snapnum_group3 = File3["Snap_"+str(snap)]

for prop in props_integrator:

    data[prop+"_de"] = snapnum_group3[prop][ok]

File3.close()

filename4 = "integrator_bean_counter_output_"+sim_name+".hdf5"
File4 = h5py.File(ode_path+filename4,"r")
snapnum_group4 = File4["Snap_"+str(snap)]

for prop in props_integrator:

    data[prop+"_bc"] = snapnum_group4[prop][ok]

File4.close()


######### Plotting ################
print "Plotting"

ok = (data["subgroup_number"]==0) & (data["isInterpolated"]==0)
from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*3],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.06,
                    'figure.subplot.top':0.99,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 15.
ylo = -1.; yhi = 1.

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))

py.figure()
np.seterr(all='ignore')
nrow = 3; ncol = 1
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        mcgm = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]

        prob = data["cgm_bc"] < 0
        data["cgm_bc"][prob] = 1e-6
        prob = data["cgm_de"] < 0
        data["cgm_de"][prob] = 1e-6

        py.axhline(0.0,c="k",alpha=0.3)

        mean_ratio = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mcgm[ok],data["cgm"][ok])
        mean_ratio_bc = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mcgm[ok],data["cgm_bc"][ok])
        mean_ratio_de = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mcgm[ok],data["cgm_de"][ok])

        mcgm[mcgm==0] = 1e-9
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["cgm"][ok]/mcgm[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_bc, lo_bc, hi_bc, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["cgm_bc"][ok]/mcgm[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_de, lo_de, hi_de, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["cgm_de"][ok]/mcgm[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, np.log10(med), c="k",linewidth=1.1, label="Fiducial")
        py.plot(mid_bin, np.log10(lo), c="k", alpha=0.3)
        py.plot(mid_bin, np.log10(hi), c="k", alpha=0.3)

        py.plot(mid_bin, np.log10(med_bc), c="r",linewidth=1.1, label="Simple addition")
        py.plot(mid_bin, np.log10(lo_bc), c="r", alpha=0.3)
        py.plot(mid_bin, np.log10(hi_bc), c="r", alpha=0.3)

        py.plot(mid_bin, np.log10(med_de), c="g",linewidth=1.1, label="Decoupled")
        py.plot(mid_bin, np.log10(lo_de), c="g", alpha=0.3)
        py.plot(mid_bin, np.log10(hi_de), c="g", alpha=0.3)

        py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')
        py.plot(mid_bin, np.log10(mean_ratio_bc), c="r", linestyle=':')
        py.plot(mid_bin, np.log10(mean_ratio_de), c="g", linestyle=':')

        py.ylabel(r"$\log_{10}(M_{\mathrm{CGM}} \, / M_{\mathrm{CGM,true}})$")
        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))

        legend1 = py.legend(loc='lower center',frameon=False,numpoints=1,prop={'size': 9})

    if i == 1:
        mism = data["mass_gas_SF"] + data["mass_gas_NSF_ISM"]

        prob = data["ism_bc"] < 0
        data["ism_bc"][prob] = 1e-6
        prob = data["ism_de"] < 0
        data["ism_de"][prob] = 1e-6

        py.axhline(0.0,c="k",alpha=0.3)

        mean_ratio = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mism[ok],data["ism"][ok])
        mean_ratio_bc = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mism[ok],data["ism_bc"][ok])
        mean_ratio_de = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mism[ok],data["ism_de"][ok])

        mism[mism==0] = 1e-9
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["ism"][ok]/mism[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_bc, lo_bc, hi_bc, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["ism_bc"][ok]/mism[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_de, lo_de, hi_de, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["ism_de"][ok]/mism[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, np.log10(med), c="k",linewidth=1.1)
        py.plot(mid_bin, np.log10(lo), c="k", alpha=0.3)
        py.plot(mid_bin, np.log10(hi), c="k", alpha=0.3)

        py.plot(mid_bin, np.log10(med_bc), c="r",linewidth=1.1)
        py.plot(mid_bin, np.log10(lo_bc), c="r", alpha=0.3)
        py.plot(mid_bin, np.log10(hi_bc), c="r", alpha=0.3)

        py.plot(mid_bin, np.log10(med_de), c="g",linewidth=1.1)
        py.plot(mid_bin, np.log10(lo_de), c="g", alpha=0.3)
        py.plot(mid_bin, np.log10(hi_de), c="g", alpha=0.3)

        py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')
        py.plot(mid_bin, np.log10(mean_ratio_bc), c="r", linestyle=':')
        py.plot(mid_bin, np.log10(mean_ratio_de), c="g", linestyle=':')

        py.ylabel(r"$\log_{10}(M_{\mathrm{ISM}} \, / M_{\mathrm{ISM,true}})$")
        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))

    if i == 2:
        mstar = data["mass_star"]
        py.axhline(0.0,c="k",alpha=0.3)
        
        mean_ratio = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mstar[ok],data["stars"][ok])
        mean_ratio_bc = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mstar[ok],data["stars_bc"][ok])
        mean_ratio_de = us.Calculate_Mean_Ratio(bin_mh,np.log10(data["m200_host"][ok]),mstar[ok],data["stars_de"][ok])

        mstar[mstar==0] = 1e-9
        med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["stars"][ok]/mstar[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_bc, lo_bc, hi_bc, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["stars_bc"][ok]/mstar[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
        med_de, lo_de, hi_de, mid_bin = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"][ok]),data["stars_de"][ok]/mstar[ok],weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

        py.plot(mid_bin, np.log10(med), c="k",linewidth=1.1)
        py.plot(mid_bin, np.log10(lo), c="k", alpha=0.3)
        py.plot(mid_bin, np.log10(hi), c="k", alpha=0.3)

        py.plot(mid_bin, np.log10(med_bc), c="r",linewidth=1.1)
        py.plot(mid_bin, np.log10(lo_bc), c="r", alpha=0.3)
        py.plot(mid_bin, np.log10(hi_bc), c="r", alpha=0.3)

        py.plot(mid_bin, np.log10(med_de), c="g",linewidth=1.1)
        py.plot(mid_bin, np.log10(lo_de), c="g", alpha=0.3)
        py.plot(mid_bin, np.log10(hi_de), c="g", alpha=0.3)

        py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')
        py.plot(mid_bin, np.log10(mean_ratio_bc), c="r", linestyle=':')
        py.plot(mid_bin, np.log10(mean_ratio_de), c="g", linestyle=':')

        py.ylabel(r"$\log_{10}(M_{\mathrm{\star}} \, / M_{\mathrm{\star,true}})$")
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")
        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


fig_name = "integrator_accuracy_bean_counter_decoupled.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
