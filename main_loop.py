import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, part_data_fof, track_lists, sim_props, modes, output_names, fVmax_info = inputs

    time_track_select = 0.0
    time_track_ptr = 0.0
    time_ptr_actual = 0.0

    # Unpack modes
    debug, ih_choose, write_SNe_energy_fraction, ism_definition, skip_dark_matter, optimise_sats, TLo_cut, extra_wind_measurements, extra_accretion_measurements, test_mode, bound_only_mode = modes

    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_t_dict = {}; halo_outflow_dict = {}; halo_emass_dict = {}; halo_mass_dict = {}; halo_shell_dict = {}

    output_mass_names, output_emass_names, output_outflow_names, output_t_names, output_shell_names = output_names
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0
    if write_SNe_energy_fraction:
        for name in output_emass_names:
            halo_emass_dict[name] = 0.0
    for name in output_outflow_names:
        halo_outflow_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ps, sim_props_ts, sim_props_ns, boxsize, tret_bins_ts = sim_props

    for name in output_t_names:
        bins_r_track_ej = np.array([0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.3, 2.6, 3.0, 4.0, 5.0, 10.0, 1e9])
        bins_r_track_wi = np.array([0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.3, 1.6, 2.0, 3.0, 4.0, 5.0, 10.0, 1e9])
        if len(bins_r_track_ej) != len(bins_r_track_wi):
            print "Woops, messed up rmax bins"; quit()
        n_r_bins_track = len(bins_r_track_ej[1:])

        # Ratio of current halo mass to halo mass of object particle was transferred from
        n_mej_bins = 10 # Same as tret
        mej_bins = np.array([ 9.0, 10.0, 10.5, 11.0, 11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 15.0])

        if "n_r" in name:
            halo_t_dict[name] = np.zeros(n_r_bins_track)
        else:
            halo_t_dict[name] = np.zeros(len(tret_bins_ts)-1)

    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    expansion_factor_ps = sim_props_ps["expansion_factor"]
    expansion_factor_ns = sim_props_ns["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    redshift_ps = sim_props_ps["redshift"]
    redshift_ns = sim_props_ns["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    fVmax_cuts = fVmax_info[0]

    # Unpack particle data
    if extra_accretion_measurements and bound_only_mode:
        gas_ps, gas_ts, gas_ns, star_ps, star_ts, star_ns, dm_ps, dm_ts, dm_ns, bh_ps, bh_ts, bh_ns, gas_unbound_ts, part_unbound_ns, gas_halo_reheat_ps, gas_halo_reheat_ts, gas_halo_reheat_ns, gas_ejecta_ts = part_data
    elif extra_accretion_measurements:
        gas_ps, gas_ts, gas_ns, star_ps, star_ts, star_ns, dm_ps, dm_ts, dm_ns, bh_ps, bh_ts, bh_ns, gas_unbound_ts, part_unbound_ns, gas_halo_reheat_ps, gas_halo_reheat_ts, gas_halo_reheat_ns, gas_ejecta_ts, gas_wind_ts = part_data
    else:
        gas_ps, gas_ts, gas_ns, star_ps, star_ts, star_ns, dm_ps, dm_ts, dm_ns, bh_ps, bh_ts, bh_ns, gas_unbound_ts, part_unbound_ns, gas_halo_reheat_ps, gas_halo_reheat_ts, gas_halo_reheat_ns, gas_ejecta_ts = part_data

    star_gas_grn_sorted, star_gas_all_progen_sorted, dm_all_progen_sorted, gas_ns_all_fof_sorted, star_ns_all_fof_sorted, bh_grn_sorted, bh_all_progen_sorted = part_data_fof

    # Unpack tracked particle lists    
    wind_ts, ism_reheat_ts, ejecta_ts, halo_reheat_ts, nsfism_ts, ism_ts, wind_cumul_ts, descendant_index_list_ps, id_gas_ejecta_list_ps, from_mp_gas_ejecta_list_ps = track_lists


    # If this is an interpolated halo on _ts, it will not contain any particles and we should not do any further calculations
    # If this is an interpolated halo on _ps, most of the calculations will be wrong - so skip also
    # (will miss legitimate accretion that happens but capturing this would require linking to subhalo before it was interpolated)
    halo_isInterpolated = subhalo_props["isInterpolated"][i_halo]
    halo_isInterpolated_progen = subhalo_props["isInterpolated"][i_halo]
    
    # There was a rare edge case where r200 = 0, causing histograms to crash
    r200_temp = subhalo_props["r200_host"][i_halo]

    if halo_isInterpolated == 1 or halo_isInterpolated_progen == 1 or r200_temp == 0.0:
        for name in output_mass_names:
            halo_mass_dict[name] = np.nan
        if write_SNe_energy_fraction:
            for name in output_emass_names:
                halo_emass_dict[name] = np.nan
        for name in output_outflow_names:
            halo_outflow_dict[name] = np.nan
        for name in output_t_names:
            if "n_r" in name:
                halo_t_dict[name] = np.zeros(n_r_bins_track)+np.nan
            else:
                halo_t_dict[name] = np.zeros(len(tret_bins_ts)-1)+np.nan
        rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
        n_rbins = len(rmid)
        for name in output_shell_names:
            halo_shell_dict[name] = np.zeros(n_rbins)+np.nan

        track_lists = [wind_ts, ism_reheat_ts, ejecta_ts, halo_reheat_ts, nsfism_ts, ism_ts, wind_cumul_ts]
        timing_info = [0.0, 0.0, 0.0, 0.0]
        return halo_mass_dict, halo_emass_dict, halo_outflow_dict, halo_t_dict, halo_shell_dict, track_lists, timing_info
    # If halo is interpolated on _ns, we can still do most of the calculations, but should skip the wind parts that rely on _ns information
    halo_isInterpolated_ns = subhalo_props["isInterpolated_ns"][i_halo]
    if halo_isInterpolated_ns == 1:
        do_ns_calculation = False
    else:
        do_ns_calculation = True

    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    if test_mode and i_halo == 0:
        print "m200 = ", np.log10(subhalo_props["m200_host"][i_halo])

    # If optimize_sat==True, we skip most of the computations for satellite subhaloes, and only do the bare essentials
    if optimise_sats and halo_subgroup_number > 0:
        full_calculation = False
    else:
        full_calculation = True

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = subhalo_props["r200_host"][i_halo]
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    
    halo_x_progen = subhalo_props["x_halo_progen"][i_halo]
    halo_y_progen = subhalo_props["y_halo_progen"][i_halo]
    halo_z_progen = subhalo_props["z_halo_progen"][i_halo]
    halo_vx_progen = subhalo_props["vx_halo_progen"][i_halo]
    halo_vy_progen = subhalo_props["vy_halo_progen"][i_halo]
    halo_vz_progen = subhalo_props["vz_halo_progen"][i_halo]
    halo_vmax_progen = subhalo_props["vmax_progen"][i_halo]
    halo_r200_host_progen = subhalo_props["r200_host_progen"][i_halo]
    halo_group_number_progen = subhalo_props["group_number_progen"][i_halo]
    halo_subgroup_number_progen = subhalo_props["subgroup_number_progen"][i_halo]
    halo_subhalo_index_progen = subhalo_props["subhalo_index_progen"][i_halo]
    
    halo_x_ns = subhalo_props["x_halo_ns"][i_halo]
    halo_y_ns = subhalo_props["y_halo_ns"][i_halo]
    halo_z_ns = subhalo_props["z_halo_ns"][i_halo]
    halo_vx_ns = subhalo_props["vx_halo_ns"][i_halo]
    halo_vy_ns = subhalo_props["vy_halo_ns"][i_halo]
    halo_vz_ns = subhalo_props["vz_halo_ns"][i_halo]
    halo_vmax_ns = subhalo_props["vmax_ns"][i_halo]
    halo_r200_host_ns = subhalo_props["r200_host_ns"][i_halo]
    halo_group_number_ns = subhalo_props["group_number_ns"][i_halo]
    halo_subgroup_number_ns = subhalo_props["subgroup_number_ns"][i_halo]
    halo_subhalo_index_ns = subhalo_props["subhalo_index_ns"][i_halo]


    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ###### Select central gas particles
    select_gas = mpf.Match_Index(halo_subhalo_index, gas_ts.subhalo_index_unique, gas_ts.particle_subhalo_index)

    id_gas = mpf.Index_Array(gas_ts.data["id"],select_gas,None)

    # Now sort by ID to avoid needing to sort for each ptr call (do this for other particle datasets also)
    order_gas = np.argsort(id_gas)
    id_gas = id_gas[order_gas]

    # Convert to Msun
    g2Msun = 1./(1.989e33)
    mass_gas = np.copy(mpf.Index_Array(gas_ts.data["mass"],select_gas,order_gas))
    mass_gas *= 1.989e43 * g2Msun / h

    metallicity_gas = mpf.Index_Array(gas_ts.data["metallicity"],select_gas,order_gas)
    density_gas = mpf.Index_Array(gas_ts.data["density"],select_gas,order_gas)
    temperature_gas = mpf.Index_Array(gas_ts.data["temperature"],select_gas,order_gas)
    internal_energy_gas = mpf.Index_Array(gas_ts.data["internal_energy"],select_gas,order_gas)

    if write_SNe_energy_fraction:
        oxygen_gas = mpf.Index_Array(gas_ts.data["oxygen"],select_gas,order_gas)
        iron_gas = mpf.Index_Array(gas_ts.data["iron"],select_gas,order_gas)

    # compute sf threshold
    xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
    nh_gas = density_gas * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
    temp = np.copy(metallicity_gas) # To avoid raising zero metallicity particles to a negative power
    ok = temp > 0
    temp[ok] = np.power(temp[ok]/0.002,-0.64)
    temp[ok==False] = 100
    nh_thresh = np.min( (np.zeros_like(nh_gas)+10.0 , 0.1 * temp), axis=0)
    
    # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
    T_eos = 8e3 * np.power(nh_gas/0.1, 4/3.)
    SF = (nh_gas > nh_thresh) & (np.log10(temperature_gas) < np.log10(T_eos) +0.5)

    coordinates_gas = mpf.Index_Array(gas_ts.data["coordinates"],select_gas,order_gas)

    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_halo = [halo_x, halo_y, halo_z]
    coordinates_gas = mpf.Centre_Halo(coordinates_gas, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
    # Put velocity into proper coordinates (kms)
    velocity_gas = np.copy(mpf.Index_Array(gas_ts.data["velocity"],select_gas,order_gas)) * expansion_factor_ts**0.5
    # change velocities to halo frame
    velocity_gas += - np.array([halo_vx, halo_vy, halo_vz]) # note halo velocities were already in proper peculiar velocity units

    r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
    r_gas_norm = np.swapaxes(np.array([coordinates_gas[:,0],coordinates_gas[:,1],coordinates_gas[:,2]]) / r_gas,0,1)
    cv_gas = np.sum(r_gas_norm * velocity_gas,axis=1)

    if write_SNe_energy_fraction:
        tmax_gas = mpf.Index_Array(gas_ts.data["Tmax"],select_gas,order_gas)
        dT_SNe = 10**7.5 # K
        dT_AGN_thr = 10**8 # K
        dir_heated = tmax_gas > dT_SNe
        dir_heated_SNe = (tmax_gas > dT_SNe) & (tmax_gas < dT_AGN_thr)

    in_master_ejecta = mpf.Index_Array(gas_ts.data["in_master_ejecta"], select_gas, order_gas)
    if extra_accretion_measurements:
        mchalo_ej_master_ejecta = mpf.Index_Array(gas_ts.data["mchalo_ej_master_ejecta"],select_gas, order_gas)
    in_master_wind = mpf.Index_Array(gas_ts.data["in_master_wind"], select_gas, order_gas)
    from_SF_master_wind = mpf.Index_Array(gas_ts.data["from_SF_master_wind"], select_gas, order_gas)
    if extra_accretion_measurements:
        mchalo_ej_master_wind = mpf.Index_Array(gas_ts.data["mchalo_ej_master_wind"],select_gas, order_gas)
    

    ##### Select star particles that belong to desired group, subgroup
    select_star = mpf.Match_Index(halo_subhalo_index, star_ts.subhalo_index_unique, star_ts.particle_subhalo_index)

    id_star = mpf.Index_Array(star_ts.data["id"],select_star,None)
    order_star = np.argsort(id_star)
    id_star = id_star[order_star]

    # Convert to Msun
    mass_star = np.copy(mpf.Index_Array(star_ts.data["mass"],select_star,order_star))
    mass_star *= 1.989e43 * g2Msun / h

    mass_star_init = np.copy(mpf.Index_Array(star_ts.data["mass_init"],select_star,order_star))
    mass_star_init *= 1.989e43 * g2Msun / h

    metallicity_star = mpf.Index_Array(star_ts.data["metallicity"],select_star,order_star)

    coordinates_star = mpf.Index_Array(star_ts.data["coordinates"],select_star,order_star)
    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_star = mpf.Centre_Halo(coordinates_star, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))

    # Work out which star particles formed between now and the previous timestep
    aform_star = mpf.Index_Array(star_ts.data["aform"],select_star,order_star)
    fresh_star = aform_star > expansion_factor_ps

    t_star, junk = uc.t_Universe(aform_star, omm, h)
    age_star = (t_ts - t_star)*1e3 # Myr

    if write_SNe_energy_fraction:
        fb_e_fraction = mpf.Index_Array(star_ts.data["fb_e_fraction"],select_star,order_star)

    in_master_ejecta_star = mpf.Index_Array(star_ts.data["in_master_ejecta"], select_star, order_star)
    in_master_wind_star = mpf.Index_Array(star_ts.data["in_master_wind"],select_star,order_star)
    from_SF_master_wind_star = mpf.Index_Array(star_ts.data["from_SF_master_wind"], select_star, order_star)
    if extra_accretion_measurements:
        mchalo_ej_master_wind_star = mpf.Index_Array(star_ts.data["mchalo_ej_master_wind"], select_star, order_star)

    ##### Select dm particles that belong to desired group, subgroup (note only used for some ISM defns)
    if not skip_dark_matter:
        select_dm = mpf.Match_Index(halo_subhalo_index, dm_ts.subhalo_index_unique, dm_ts.particle_subhalo_index)

        id_dm = mpf.Index_Array(dm_ts.data["id"],select_dm,None)
    
        if extra_wind_measurements:
            coordinates_dm = mpf.Index_Array(dm_ts.data["coordinates"],select_dm,None)
            coordinates_dm = mpf.Centre_Halo(coordinates_dm, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    ###### Select central progenitor gas particles #########
    select_gas_progen = mpf.Match_Index(halo_subhalo_index_progen, gas_ps.subhalo_index_unique, gas_ps.particle_subhalo_index)

    id_gas_progen = mpf.Index_Array(gas_ps.data["id"],select_gas_progen,None)
    order_gas_progen = np.argsort(id_gas_progen)
    id_gas_progen = id_gas_progen[order_gas_progen]

    # Without full FoF information, we can approximate if particle is lost from FOF group by checking if the particle is unbound from any halo on _ts/_ns
    if not full_calculation:
        unbound_ts_ns_progen = mpf.Index_Array(gas_ps.data["unbound_ts_ns"],select_gas_progen,order_gas_progen)

    mass_gas_progen = np.copy(mpf.Index_Array(gas_ps.data["mass"],select_gas_progen,order_gas_progen))
    mass_gas_progen *= 1.989e43 * g2Msun / h

    metallicity_gas_progen = mpf.Index_Array(gas_ps.data["metallicity"],select_gas_progen,order_gas_progen)
    temperature_gas_progen = mpf.Index_Array(gas_ps.data["temperature"],select_gas_progen,order_gas_progen)
    density_gas_progen = mpf.Index_Array(gas_ps.data["density"],select_gas_progen,order_gas_progen)

    if write_SNe_energy_fraction:
        oxygen_gas_progen = mpf.Index_Array(gas_ps.data["oxygen"],select_gas_progen,order_gas_progen)
        iron_gas_progen = mpf.Index_Array(gas_ps.data["iron"],select_gas_progen,order_gas_progen)

    # compute sf threshold
    nh_gas_progen = density_gas_progen * xh_sf_thresh * h**2 * expansion_factor_ps**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
    temp = np.copy(metallicity_gas_progen)
    ok = temp > 0
    temp[ok] = np.power(temp[ok]/0.002,-0.64)
    temp[ok==False] = 100
    nh_thresh = np.min( (np.zeros_like(nh_gas_progen)+10.0 , 0.1 * temp), axis=0)
    # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
    T_eos = 8e3 * np.power(nh_gas_progen/0.1, 4/3.)
    SF_progen = (nh_gas_progen > nh_thresh) & (np.log10(temperature_gas_progen) < np.log10(T_eos) +0.5)

    coordinates_gas_progen = mpf.Index_Array(gas_ps.data["coordinates"],select_gas_progen,order_gas_progen)
    coordinates_halo_progen = [halo_x_progen, halo_y_progen, halo_z_progen]
    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_gas_progen = mpf.Centre_Halo(coordinates_gas_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
    
    if write_SNe_energy_fraction:
        tmax_gas_progen = mpf.Index_Array(gas_ps.data["Tmax"],select_gas_progen,order_gas_progen)
        dT_SNe = 10**7.5 # K
        dT_AGN_thr = 10**8 # K
        dir_heated_progen = tmax_gas_progen > dT_SNe
        dir_heated_progen_SNe = (tmax_gas_progen > dT_SNe) & (tmax_gas_progen < dT_AGN_thr)

    ##### Select progenitor star particles that belong to desired group, subgroup
    select_star_progen = mpf.Match_Index(halo_subhalo_index_progen, star_ps.subhalo_index_unique, star_ps.particle_subhalo_index)
    
    id_star_progen = mpf.Index_Array(star_ps.data["id"],select_star_progen,None)
    order_star_progen = np.argsort(id_star_progen)
    id_star_progen = id_star_progen[order_star_progen]

    mass_star_progen = np.copy(mpf.Index_Array(star_ps.data["mass"],select_star_progen,order_star_progen))
    mass_star_progen *= 1.989e43 * g2Msun / h
    mass_star_init_progen = np.copy(mpf.Index_Array(star_ps.data["mass_init"],select_star_progen,order_star_progen))
    mass_star_init_progen *= 1.989e43 * g2Msun / h

    ##### Select progenitor dm/bh particles that belong to desired group, subgroup (note only used for some ISM defns)
    if not skip_dark_matter:
        select_dm_progen = mpf.Match_Index(halo_subhalo_index_progen, dm_ps.subhalo_index_unique, dm_ps.particle_subhalo_index)
    
    ####### Select black holes ###############
    select_bh_progen = mpf.Match_Index(halo_subhalo_index_progen, bh_ps.subhalo_index_unique, bh_ps.particle_subhalo_index)
    id_bh_progen = mpf.Index_Array(bh_ps.data["id"],select_bh_progen,None)
    mass_bh_progen = np.copy(mpf.Index_Array(bh_ps.data["mass"],select_bh_progen,None))
    mass_bh_progen *= 1.989e43 * g2Msun / h # Msun

    # Total mass of gas that has been accreted onto black holes in this subhalo
    mass_bh_seed = 1e5 / h # As per Schaye et al. (2015)
    mass_acc_bh_progen = mass_bh_progen - mass_bh_seed * mpf.Index_Array(bh_ps.data["nseed"],select_bh_progen,None)
    if len(mass_acc_bh_progen)>0 and mass_acc_bh_progen.min() < 0.0:
        if mass_acc_bh_progen.min() < -1:
            print "Warning: at least one accreted progenitor black hole mass is negative - implies a possible error in the calculation of the seed mass"
            print "Sorted list of accreted black hole masses", np.sort(mass_acc_bh_progen)
        mass_acc_bh_progen[mass_acc_bh_progen<0.0] = 0.0

    select_bh = mpf.Match_Index(halo_subhalo_index, bh_ts.subhalo_index_unique, bh_ts.particle_subhalo_index)
    id_bh = mpf.Index_Array(bh_ts.data["id"],select_bh,None)
    mass_bh = np.copy(mpf.Index_Array(bh_ts.data["mass"],select_bh,None))
    mass_bh *= 1.989e43 * g2Msun / h # Msun
        
    # Compute the formation time of the most massive black hole particle in this subhalo
    if len(mass_bh)>0:
        ind_max = np.argmax(mass_bh)
        halo_mass_dict["aform_bh"] = mpf.Index_Array(bh_ts.data["aform"],select_bh,None)[ind_max]

    if debug and i_halo == ih_choose:
        print "black hole mass for this subhalo", np.sum(mass_bh)

    # Total mass of gas that has been accreted onto black holes in this subhalo
    mass_acc_bh = mass_bh - mass_bh_seed * mpf.Index_Array(bh_ts.data["nseed"],select_bh,None)
    if len(mass_acc_bh)>0 and mass_acc_bh.min() < 0.0:
        if mass_acc_bh.min() < -1:
            print "Warning: at least one accreted black hole mass is negative - implies a possible error in the calculation of the seed mass"
            print "Sorted list of accreted black hole masses", np.sort(mass_acc_bh)
        mass_acc_bh[mass_acc_bh<0.0] = 0.0

    # Select particles that belong to the ISM
    if ism_definition == "Joop_version":
        gas_props = [coordinates_gas, temperature_gas, nh_gas, SF]
        halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]
        tdyn_ts = t_ts * 0.1 # Approximation of the halo dynamical time, in Gyr
        sim_props = [redshift_ts, tdyn_ts, h]
        ISM =  mpf.Select_ISM_simple(gas_props, halo_props , sim_props)

    elif ism_definition == "Mitchell18_adjusted":
        coordinates_dm = mpf.Index_Array(dm_ts.data["coordinates"],select_dm,None)
        coordinates_dm = mpf.Centre_Halo(coordinates_dm, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
        coordinates_bh = mpf.Index_Array(bh_ts.data["coordinates"],select_bh,None)
        coordinates_bh = mpf.Centre_Halo(coordinates_bh, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

        coordinates_parts = [coordinates_gas, coordinates_dm, coordinates_star, coordinates_bh]
        gas_props = [internal_energy_gas, temperature_gas, nh_gas, velocity_gas, SF]
        mass_part = [mass_gas, mass_dm * 1.989e43 * g2Msun / h, mass_star, mass_bh]
        halo_props = [halo_r200_host, halo_group_number, halo_subgroup_number, halo_vmax]
        sim_props = [redshift_ts, h, boxsize, omm]
        ISM =  mpf.Select_ISM_M18(coordinates_parts, gas_props, mass_part, halo_props , sim_props)
    else:
        print "Error: ISM definition", ism_definition, "not supported"
        quit()

    if extra_wind_measurements and not ism_definition == "Mitchell18_adjusted":
        coordinates_bh = mpf.Index_Array(bh_ts.data["coordinates"],select_bh,None)
        coordinates_bh = mpf.Centre_Halo(coordinates_bh, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    if debug and i_halo == ih_choose:
        print "ISM", len(ISM), len(ISM[ISM])

    # Select particles that belong to the ISM
    if ism_definition == "Joop_version":
        gas_props = [coordinates_gas_progen, temperature_gas_progen, nh_gas_progen, SF_progen]
        halo_props = [halo_r200_host_progen, halo_subgroup_number_progen, halo_vmax_progen]
        tdyn_ps = t_ps * 0.1 # Approximation of the halo dynamical time, in Gyr
        sim_props = [redshift_ps, tdyn_ps, h]

        ISM_progen =  mpf.Select_ISM_simple(gas_props, halo_props , sim_props)
    else:
        coordinates_dm_progen = mpf.Index_Array(dm_ps.data["coordinates"],select_dm_progen,None)
        coordinates_dm_progen = mpf.Centre_Halo(coordinates_dm_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
        coordinates_star_progen = mpf.Index_Array(star_ps.data["coordinates"],select_star_progen,order_star_progen)
        coordinates_star_progen = mpf.Centre_Halo(coordinates_star_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
        coordinates_bh_progen = mpf.Index_Array(bh_ps.data["coordinates"],select_bh_progen,None)
        coordinates_bh_progen = mpf.Centre_Halo(coordinates_bh_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc 
        internal_energy_gas_progen = mpf.Index_Array(gas_ps.data["internal_energy"],select_gas_progen,order_gas_progen)
        velocity_gas_progen = np.copy(mpf.Index_Array(gas_ps.data["velocity"],select_gas_progen,order_gas_progen)) * expansion_factor_ps**0.5
        velocity_gas_progen += - np.array([halo_vx_progen, halo_vy_progen, halo_vz_progen]) 

        coordinates_parts = [coordinates_gas_progen, coordinates_dm_progen, coordinates_star_progen, coordinates_bh_progen]
        gas_props = [internal_energy_gas_progen, temperature_gas_progen, nh_gas_progen, velocity_gas_progen, SF_progen]
        mass_part = [mass_gas_progen, mass_dm * 1.989e43 * g2Msun / h, mass_star_progen, mass_bh_progen]
        halo_props = [halo_r200_host_progen, halo_group_number_progen, halo_subgroup_number_progen, halo_vmax_progen]
        sim_props = [redshift_ps, h, boxsize, omm]

        ISM_progen =  mpf.Select_ISM_M18(coordinates_parts, gas_props, mass_part, halo_props , sim_props)

    if debug and i_halo == ih_choose:
        print "ISM_progen", len(ISM_progen), len(ISM_progen[ISM_progen])

    if debug and i_halo == ih_choose:
        print "ISM_progen", len(ISM_progen), len(ISM_progen[ISM_progen])

    # Append star and gas progen lists together (for calculating accretion of stars onto satellites)
    id_star_gas = np.append(id_star, id_gas)
    if extra_accretion_measurements:
        mass_star_gas = np.append(mass_star, mass_gas)
    temperature_star_gas = np.append(np.zeros(len(id_star)),temperature_gas)
    if write_SNe_energy_fraction:
        tmax_star_gas = np.append(np.zeros(len(id_star)),tmax_gas)
    fam_star_gas = np.append(np.ones_like(id_star),np.ones_like(id_gas)+1)
    in_master_wind_star_gas = np.append(in_master_wind_star, in_master_wind)
    in_master_ejecta_star_gas = np.append(in_master_ejecta_star, in_master_ejecta)
    if extra_accretion_measurements:
        mchalo_ej_master_wind_star_gas = np.append(mchalo_ej_master_wind_star, mchalo_ej_master_wind)

    ####### Select and concatenate all ejected particles from all progenitors of this fof group
    if full_calculation and do_ns_calculation:
        select_fof_ns = mpf.Match_Index(halo_group_number_ns, gas_ns_all_fof_sorted.grn_unique, gas_ns_all_fof_sorted.grn_index)
        select_fof_ns_star = mpf.Match_Index(halo_group_number_ns, star_ns_all_fof_sorted.grn_unique, star_ns_all_fof_sorted.grn_index)

    if full_calculation:
        select_progenitors = np.zeros_like(descendant_index_list_ps) < 0 # init to false
        select_sub = subhalo_props["group_number"] == halo_group_number
        for i_sub in range(len(subhalo_props["group_number"][select_sub])):
            select_progenitors = select_progenitors | (subhalo_props["node_index"][select_sub][i_sub] == descendant_index_list_ps)
        select_progenitors = np.where(select_progenitors)[0]

        id_gas_ejecta_fof_progen_list = []
        from_mp_gas_ejecta_fof_progen_list = []
        for select_progenitor_i in select_progenitors:
            id_gas_ejecta_fof_progen_list = np.append(np.array(id_gas_ejecta_fof_progen_list),np.array(id_gas_ejecta_list_ps[select_progenitor_i]))
            from_mp_gas_ejecta_fof_progen_list = np.append(np.array(from_mp_gas_ejecta_fof_progen_list),np.array(from_mp_gas_ejecta_list_ps[select_progenitor_i]))

        # Find which FoF group this subhalo belongs to and retrieve appropriate particle lists
        select_fof_as = mpf.Match_Index(halo_group_number, star_gas_grn_sorted.grn_unique, star_gas_grn_sorted.grn_index)
        select_fof_all_progen = mpf.Match_Index(halo_group_number, star_gas_all_progen_sorted.grn_unique, star_gas_all_progen_sorted.grn_index)
        if not skip_dark_matter:
            select_fof_all_progen_dm = mpf.Match_Index(halo_group_number, dm_all_progen_sorted.grn_unique, dm_all_progen_sorted.grn_index)

        id_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["id"],select_fof_all_progen,None)
        order_star_gas_all_progen = np.argsort(id_star_gas_all_progen)
        id_star_gas_all_progen = id_star_gas_all_progen[order_star_gas_all_progen]

        group_number_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["group_number"],select_fof_all_progen,order_star_gas_all_progen)
        subgroup_number_descendant_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["subgroup_number_descendant"],select_fof_all_progen,order_star_gas_all_progen)
        coordinates_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["coordinates"],select_fof_all_progen,order_star_gas_all_progen)
        coordinates_star_gas_all_progen = mpf.Centre_Halo(coordinates_star_gas_all_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
        temperature_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["temperature"],select_fof_all_progen,order_star_gas_all_progen)

        if ism_definition == "Joop_version":
            density_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["density"],select_fof_all_progen,order_star_gas_all_progen)
            metallicity_star_gas_all_progen = mpf.Index_Array(star_gas_all_progen_sorted.data["metallicity"],select_fof_all_progen,order_star_gas_all_progen)
            # For the galaxy merger calculation we need to know what was in the ISM of all progenitors

            # compute sf threshold
            xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
            nh_star_gas_all_progen = density_star_gas_all_progen * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
            temp = np.copy(metallicity_star_gas_all_progen) # To avoid raising zero metallicity particles to a negative power
            ok = temp > 0
            temp[ok] = np.power(temp[ok]/0.002,-0.64)
            temp[ok==False] = 100
            nh_thresh = np.min( (np.zeros_like(nh_star_gas_all_progen)+10.0 , 0.1 * temp), axis=0)

            # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
            T_eos = 8e3 * np.power(nh_star_gas_all_progen/0.1, 4/3.)
            SF_star_gas_all_progen = (nh_star_gas_all_progen > nh_thresh) & (np.log10(temperature_star_gas_all_progen) < np.log10(T_eos) +0.5)

            gas_props = [coordinates_star_gas_all_progen, temperature_star_gas_all_progen, nh_star_gas_all_progen, SF_star_gas_all_progen]
            halo_props = [halo_r200_host_progen, halo_subgroup_number_progen, halo_vmax_progen]
            tdyn_ps = t_ps * 0.1 # Approximation of the halo dynamical time, in Gyr
            sim_props = [redshift_ps, tdyn_ps, h]
            ISM_star_gas_all_progen =  mpf.Select_ISM_simple(gas_props, halo_props , sim_props)
            # Note, this won't select NSF ISM particles correctly for particles in non-main-progenitor subhaloes - should be ok though!

        else:
            # Make the approximation that ISM only comes from the main progenitor subhalo
            # This will put the galaxy merger contribution to zero, and increase the smooth cooling contribution
            # Cross-match with progen particles to determine which are in ISM of the main progenitor
            ISM_star_gas_all_progen = np.zeros_like(id_star_gas_all_progen) < 0
            if len(id_gas_progen[ISM_progen]) > 0 and len(id_star_gas_all_progen) > 0:
                ptr = ms.match(id_star_gas_all_progen, id_gas_progen[ISM_progen],arr2_sorted=True)
                ok_match = ptr >= 0
                ISM_star_gas_all_progen[ok_match] = True
            SF_star_gas_all_progen = np.zeros_like(id_star_gas_all_progen) < 0
            if len(id_gas_progen[SF_progen]) > 0 and len(id_star_gas_all_progen) > 0:
                ptr = ms.match(id_star_gas_all_progen, id_gas_progen[SF_progen],arr2_sorted=True)
                ok_match = ptr >= 0
                SF_star_gas_all_progen[ok_match] = True
        
        from_mp_star_gas_all_progen = np.zeros_like(id_star_gas_all_progen) < 0
        if len(id_gas_progen) > 0 and len(id_star_gas_all_progen) > 0:
            ptr = ms.match(id_star_gas_all_progen, id_gas_progen,arr2_sorted=True)
            ok_match = ptr >= 0
            from_mp_star_gas_all_progen[ok_match] = True

        if not skip_dark_matter:
            id_dm_all_progen = mpf.Index_Array(dm_all_progen_sorted.data["id"],select_fof_all_progen_dm, None)

        id_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["id"],select_fof_as,None)
        order_star_gas_as = np.argsort(id_star_gas_as)
        id_star_gas_as = id_star_gas_as[order_star_gas_as]

        subgroup_number_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["subgroup_number"],select_fof_as,order_star_gas_as)
        mass_gas_only_star_gas_as = np.copy(mpf.Index_Array(star_gas_grn_sorted.data["mass_gas_only"],select_fof_as,order_star_gas_as))
        mass_star_gas_as = np.copy(mpf.Index_Array(star_gas_grn_sorted.data["mass"],select_fof_as,order_star_gas_as))
        mass_star_gas_as *= 1.989e43 * g2Msun / h # Msun
        mass_gas_only_star_gas_as *= 1.989e43 * g2Msun / h
        metallicity_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["metallicity"],select_fof_as,order_star_gas_as)
        coordinates_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["coordinates"],select_fof_as,order_star_gas_as)
        coordinates_star_gas_as = mpf.Centre_Halo(coordinates_star_gas_as, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
        velocity_star_gas_as = np.copy(mpf.Index_Array(star_gas_grn_sorted.data["velocity"],select_fof_as,order_star_gas_as))
        velocity_star_gas_as *= expansion_factor_ts**0.5 # pkms
        velocity_star_gas_as += - np.array([halo_vx, halo_vy, halo_vz])
        r_star_gas_as = np.sqrt(np.square(coordinates_star_gas_as[:,0])+np.square(coordinates_star_gas_as[:,1])+np.square(coordinates_star_gas_as[:,2]))
        r_star_gas_as_norm = np.swapaxes(np.array([coordinates_star_gas_as[:,0],coordinates_star_gas_as[:,1],coordinates_star_gas_as[:,2]]) / r_star_gas_as,0,1)
        cv_star_gas_as = np.sum(r_star_gas_as_norm * velocity_star_gas_as,axis=1)


        if write_SNe_energy_fraction:
            dir_heated_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["dir_heated"],select_fof_as,order_star_gas_as)
            oxygen_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["oxygen"],select_fof_as,order_star_gas_as)
            iron_star_gas_as = mpf.Index_Array(star_gas_grn_sorted.data["iron"],select_fof_as,order_star_gas_as)

            if extra_wind_measurements:
                fb_e_fraction_sat = mpf.Index_Array(star_gas_grn_sorted.data["fb_e_fraction"],select_fof_as,order_star_gas_as)
                sat_star = (subgroup_number_star_gas_as >0) & (fb_e_fraction_sat>0)
                fb_e_fraction_sat = fb_e_fraction_sat[sat_star]
                mass_star_init_sat = np.copy(mpf.Index_Array(star_gas_grn_sorted.data["mass_init"],select_fof_as,order_star_gas_as)[sat_star])
                mass_star_init_sat *= 1.989e43 * g2Msun / h

                coordinates_star_sat = coordinates_star_gas_as[sat_star]

                aform_star_sat = mpf.Index_Array(star_gas_grn_sorted.data["aform"],select_fof_as,order_star_gas_as)[sat_star]
                t_star_sat, junk = uc.t_Universe(aform_star_sat, omm, h)
                age_star_sat = (t_ts - t_star_sat)*1e3 # Myr

                select_fof_bh = mpf.Match_Index(halo_group_number, bh_grn_sorted.grn_unique, bh_grn_sorted.grn_index)
                select_fof_bh_all_progen = mpf.Match_Index(halo_group_number, bh_all_progen_sorted.grn_unique, bh_all_progen_sorted.grn_index)
                
                subgroup_number_bh_sat = mpf.Index_Array(bh_grn_sorted.data["subgroup_number"],select_fof_bh,None)
                sat_bh = (subgroup_number_bh_sat>0)
                subgroup_number_bh_sat = subgroup_number_bh_sat[sat_bh]
                mass_bh_sat = np.copy(mpf.Index_Array(bh_grn_sorted.data["mass"],select_fof_bh,None)[sat_bh])
                mass_bh_sat *= 1.989e43 * g2Msun / h # Msun

                nseed_bh_sat = mpf.Index_Array(bh_grn_sorted.data["nseed"],select_fof_bh,None)[sat_bh]
                id_bh_sat = mpf.Index_Array(bh_grn_sorted.data["id"],select_fof_bh,None)[sat_bh]
                coordinates_bh_sat = mpf.Index_Array(bh_grn_sorted.data["coordinates"],select_fof_bh,None)[sat_bh]
                coordinates_bh_sat = mpf.Centre_Halo(coordinates_bh_sat, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

                mass_bh_all_progen = np.copy(mpf.Index_Array(bh_all_progen_sorted.data["mass"],select_fof_bh_all_progen,None))
                mass_bh_all_progen *= 1.989e43 * g2Msun / h # Msun
                nseed_bh_all_progen = mpf.Index_Array(bh_all_progen_sorted.data["nseed"],select_fof_bh_all_progen,None)
                id_bh_all_progen = mpf.Index_Array(bh_all_progen_sorted.data["id"],select_fof_bh_all_progen,None)

        # Cross-match with FoF particle list to determine which are ISM of this subhalo
        ISM_star_gas_as = np.zeros_like(id_star_gas_as) < 0
        if len(id_gas[ISM]) > 0 and len(id_star_gas_as) > 0:
            ptr = ms.match(id_star_gas_as, id_gas[ISM],arr2_sorted=True)
            ok_match = ptr >= 0
            ISM_star_gas_as[ok_match] = True
        SF_star_gas_as = np.zeros_like(id_star_gas_as) < 0
        if len(id_gas[SF]) > 0 and len(id_star_gas_as) > 0:
            ptr = ms.match(id_star_gas_as, id_gas[SF],arr2_sorted=True)
            ok_match = ptr >= 0
            SF_star_gas_as[ok_match] = True
        is_star_star_gas_as = np.zeros_like(id_star_gas_as) < 0
        if len(id_star) > 0 and len(id_star_gas_as) > 0:
            ptr = ms.match(id_star_gas_as, id_star,arr2_sorted=True)
            ok_match = ptr >= 0
            is_star_star_gas_as[ok_match] = True


    ######## Select particles that were bound from this subhalo on _ps and are now unbound on _ts, _ns ###########
    select_unbound_ts = mpf.Match_Index(halo_subhalo_index_progen, gas_unbound_ts.subhalo_index_unique, gas_unbound_ts.particle_subhalo_index)
    
    coordinates_gas_unbound_ts = mpf.Index_Array(gas_unbound_ts.data["coordinates"],select_unbound_ts,None)
    coordinates_gas_unbound_ts = mpf.Centre_Halo(coordinates_gas_unbound_ts, coordinates_halo, boxsize, scaling = 1e3 /h / (1+redshift_ts))

    id_gas_unbound_ts = mpf.Index_Array(gas_unbound_ts.data["id"],select_unbound_ts,None)
    group_number_gas_unbound_ts = mpf.Index_Array(gas_unbound_ts.data["group_number_particle"], select_unbound_ts,None)
    if write_SNe_energy_fraction:
        dir_heated_unbound_ts = mpf.Index_Array(gas_unbound_ts.data["dir_heated"],select_unbound_ts,None)
        dir_heated_SNe_unbound_ts = mpf.Index_Array(gas_unbound_ts.data["dir_heated_SNe"],select_unbound_ts,None)
    velocity_gas_unbound_ts = np.copy(mpf.Index_Array(gas_unbound_ts.data["velocity"], select_unbound_ts,None)) * expansion_factor_ts**0.5
    velocity_gas_unbound_ts += - np.array([halo_vx, halo_vy, halo_vz])

    if do_ns_calculation:
        select_unbound_ns = mpf.Match_Index(halo_subhalo_index_progen, part_unbound_ns.subhalo_index_unique, part_unbound_ns.particle_subhalo_index)

        coordinates_part_unbound_ns = mpf.Index_Array(part_unbound_ns.data["coordinates"],select_unbound_ns,None)
        coordinates_halo_ns = [halo_x_ns, halo_y_ns, halo_z_ns]
        coordinates_part_unbound_ns = mpf.Centre_Halo(coordinates_part_unbound_ns, coordinates_halo_ns, boxsize, 1e3 /h / (1+redshift_ns))
        id_part_unbound_ns = mpf.Index_Array(part_unbound_ns.data["id"],select_unbound_ns,None)
        group_number_part_unbound_ns = mpf.Index_Array(part_unbound_ns.data["group_number"],select_unbound_ns,None)

    ######### Select particles from the main halo_reheat_list (note this one only gets reset at the start of the timestep ######

    select_halo_reheat_ps = mpf.Match_Index(halo_subhalo_index, gas_halo_reheat_ps.subhalo_index_unique, gas_halo_reheat_ps.particle_subhalo_index)
    coordinates_gas_halo_reheat_ps_fixed = mpf.Index_Array(gas_halo_reheat_ps.data["coordinates"],select_halo_reheat_ps,None)
    coordinates_gas_halo_reheat_ps_fixed = mpf.Centre_Halo(coordinates_gas_halo_reheat_ps_fixed, coordinates_halo_progen, boxsize, scaling = 1e3 /h / (1+redshift_ps))

    select_halo_reheat_ts = mpf.Match_Index(halo_subhalo_index, gas_halo_reheat_ts.subhalo_index_unique, gas_halo_reheat_ts.particle_subhalo_index)

    id_gas_halo_reheat_ts_fixed = mpf.Index_Array(gas_halo_reheat_ts.data["id"],select_halo_reheat_ts,None)
    order_gas_halo_reheat_ts_fixed = np.argsort(id_gas_halo_reheat_ts_fixed)
    id_gas_halo_reheat_ts_fixed = id_gas_halo_reheat_ts_fixed[order_gas_halo_reheat_ts_fixed]

    coordinates_gas_halo_reheat_ts_fixed = mpf.Index_Array(gas_halo_reheat_ts.data["coordinates"],select_halo_reheat_ts,order_gas_halo_reheat_ts_fixed)
    coordinates_gas_halo_reheat_ts_fixed = mpf.Centre_Halo(coordinates_gas_halo_reheat_ts_fixed, coordinates_halo, boxsize, scaling= 1e3 /h / (1+redshift_ts))

    if write_SNe_energy_fraction:
        dir_heated_gas_halo_reheat_ts_fixed = mpf.Index_Array(gas_halo_reheat_ts.data["dir_heated"],select_halo_reheat_ts,order_gas_halo_reheat_ts_fixed)

    if do_ns_calculation:
        select_halo_reheat_ns = mpf.Match_Index(halo_subhalo_index, gas_halo_reheat_ns.subhalo_index_unique, gas_halo_reheat_ns.particle_subhalo_index)
        coordinates_gas_halo_reheat_ns_fixed = mpf.Index_Array(gas_halo_reheat_ns.data["coordinates"],select_halo_reheat_ns,None)
        coordinates_gas_halo_reheat_ns_fixed = mpf.Centre_Halo(coordinates_gas_halo_reheat_ns_fixed, coordinates_halo_ns, boxsize, scaling = 1e3 /h / (1+redshift_ns))


    select_ejecta_ts = mpf.Match_Index(halo_subhalo_index, gas_ejecta_ts.subhalo_index_unique, gas_ejecta_ts.particle_subhalo_index)

    id_gas_ejecta_ts_fixed = mpf.Index_Array(gas_ejecta_ts.data["id"],select_ejecta_ts,None)
    if len(id_gas_ejecta_ts_fixed) > 0:
        order_gas_ejecta_ts_fixed = np.argsort(id_gas_ejecta_ts_fixed)
        id_gas_ejecta_ts_fixed = id_gas_ejecta_ts_fixed[order_gas_ejecta_ts_fixed]

        in_master_wind_ejecta_ts_fixed = mpf.Index_Array(gas_ejecta_ts.data["in_master_wind"], select_ejecta_ts, order_gas_ejecta_ts_fixed)
    else:
        in_master_wind_ejecta_ts_fixed = np.array([])

    if extra_accretion_measurements and not bound_only_mode:

        coordinates_gas_ejecta_ts_fixed = mpf.Index_Array(gas_ejecta_ts.data["coordinates"],select_ejecta_ts,order_gas_ejecta_ts_fixed)
        coordinates_gas_ejecta_ts_fixed = mpf.Centre_Halo(coordinates_gas_ejecta_ts_fixed, coordinates_halo, boxsize, scaling= 1e3 /h / (1+redshift_ts))

        select_wind_ts = mpf.Match_Index(halo_subhalo_index, gas_wind_ts.subhalo_index_unique, gas_wind_ts.particle_subhalo_index)

        id_gas_wind_ts_fixed = mpf.Index_Array(gas_wind_ts.data["id"],select_wind_ts,None)
        order_gas_wind_ts_fixed = np.argsort(id_gas_wind_ts_fixed)
        id_gas_wind_ts_fixed = id_gas_wind_ts_fixed[order_gas_wind_ts_fixed]

        coordinates_gas_wind_ts_fixed = mpf.Index_Array(gas_wind_ts.data["coordinates"],select_wind_ts,order_gas_wind_ts_fixed)
        coordinates_gas_wind_ts_fixed = mpf.Centre_Halo(coordinates_gas_wind_ts_fixed, coordinates_halo, boxsize, scaling= 1e3 /h / (1+redshift_ts))
        

    ###### Select particles from the descendant subhalo on the next output

    if full_calculation and do_ns_calculation:
        # Get IDs, coordinates and determine if SF for all particles in the FoF group of the descendant
        id_gas_fof_ns = mpf.Index_Array(gas_ns_all_fof_sorted.data["id"],select_fof_ns,None)
        order_gas_fof_ns = np.argsort(id_gas_fof_ns)
        id_gas_fof_ns = id_gas_fof_ns[order_gas_fof_ns]

        metallicity_gas_fof_ns = mpf.Index_Array(gas_ns_all_fof_sorted.data["metallicity"],select_fof_ns,order_gas_fof_ns)
        density_gas_fof_ns = mpf.Index_Array(gas_ns_all_fof_sorted.data["density"],select_fof_ns,order_gas_fof_ns)
        coordinates_gas_fof_ns = mpf.Index_Array(gas_ns_all_fof_sorted.data["coordinates"],select_fof_ns,order_gas_fof_ns)
        temperature_gas_fof_ns = mpf.Index_Array(gas_ns_all_fof_sorted.data["temperature"],select_fof_ns,order_gas_fof_ns)

        # Note it's a bit silly to do this here - would be slightly more efficient to do this in the FoF step
        # compute sf threshold
        xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
        nh_gas_fof_ns = density_gas_fof_ns * xh_sf_thresh * h**2 * expansion_factor_ns**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
        temp = np.copy(metallicity_gas_fof_ns)
        ok = temp > 0
        temp[ok] = np.power(temp[ok]/0.002,-0.64)
        temp[ok==False] = 100
        nh_thresh = np.min( (np.zeros_like(nh_gas_fof_ns)+10.0 , 0.1 * temp), axis=0)
        # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
        T_eos = 8e3 * np.power(nh_gas_fof_ns/0.1,4/3.)
        SF_fof_ns = (nh_gas_fof_ns > nh_thresh) & (np.log10(temperature_gas_fof_ns) < np.log10(T_eos) +0.5)

        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_gas_fof_ns = mpf.Centre_Halo(coordinates_gas_fof_ns, coordinates_halo_ns, boxsize) * 1e3 /h / (1+redshift_ns) # pkpc

        id_star_fof_ns = np.sort(mpf.Index_Array(star_ns_all_fof_sorted.data["id"], select_fof_ns_star,None))

    if do_ns_calculation:
        # Determine if particles in the SUBHALO of the descendant are in the ISM or not 
        # (in principle I should work out the ISM for all subhaloes in the descendant's FoF group - but this would be painful and should make no difference)
        select_gas_ns = mpf.Match_Index(halo_subhalo_index_ns, gas_ns.subhalo_index_unique, gas_ns.particle_subhalo_index)

        id_gas_ns = mpf.Index_Array(gas_ns.data["id"],select_gas_ns,None)
        order_gas_ns = np.argsort(id_gas_ns)
        id_gas_ns = id_gas_ns[order_gas_ns]

        mass_gas_ns = np.copy(mpf.Index_Array(gas_ns.data["mass"],select_gas_ns,order_gas_ns))
        mass_gas_ns *= 1.989e43 * g2Msun / h
        metallicity_gas_ns =mpf.Index_Array(gas_ns.data["metallicity"],select_gas_ns,order_gas_ns)
        density_gas_ns = mpf.Index_Array(gas_ns.data["density"],select_gas_ns,order_gas_ns)
        temperature_gas_ns = mpf.Index_Array(gas_ns.data["temperature"],select_gas_ns,order_gas_ns)
        nh_gas_ns = density_gas_ns * xh_sf_thresh * h**2 * expansion_factor_ns**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
        temp = np.copy(metallicity_gas_ns)
        ok = temp > 0
        temp[ok] = np.power(temp[ok]/0.002,-0.64)
        temp[ok==False] = 100
        nh_thresh = np.min( (np.zeros_like(nh_gas_ns)+10.0 , 0.1 * temp), axis=0)
        # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
        T_eos = 8e3 * np.power(nh_gas_ns/0.1 , 4/3.)
        SF_ns = (nh_gas_ns > nh_thresh) & (np.log10(temperature_gas_ns) < np.log10(T_eos) +0.5)
        coordinates_gas_ns = mpf.Index_Array(gas_ns.data["coordinates"],select_gas_ns,order_gas_ns)
        coordinates_halo_ns = [halo_x_ns, halo_y_ns, halo_z_ns]
        coordinates_gas_ns = mpf.Centre_Halo(coordinates_gas_ns, coordinates_halo_ns, boxsize) * 1e3 /h / (1+redshift_ns)

        if not full_calculation:
            select_star_ns = mpf.Match_Index(halo_subhalo_index_ns, star_ns.subhalo_index_unique, star_ns.particle_subhalo_index)
            id_star_ns = np.sort(mpf.Index_Array(star_ns.data["id"],select_star_ns,None))

        # Select particles that belong to the ISM
        if ism_definition == "Joop_version":
            gas_props = [coordinates_gas_ns, temperature_gas_ns, nh_gas_ns, SF_ns]
            halo_props = [halo_r200_host_ns, halo_subgroup_number_ns, halo_vmax_ns]
            tdyn_ns = t_ns * 0.1 # Approximation of the halo dynamical time, in Gyr
            sim_props = [redshift_ns, tdyn_ns, h]

            ISM_ns =  mpf.Select_ISM_simple(gas_props, halo_props , sim_props)

        else:
            select_dm_ns = mpf.Match_Index(halo_subhalo_index_ns, dm_ns.subhalo_index_unique, dm_ns.particle_subhalo_index)
            coordinates_dm_ns = mpf.Index_Array(dm_ns.data["coordinates"],select_dm_ns,None)
            coordinates_dm_ns = mpf.Centre_Halo(coordinates_dm_ns, coordinates_halo_ns, boxsize) * 1e3 /h / (1+redshift_ns)

            select_bh_ns = mpf.Match_Index(halo_subhalo_index_ns, bh_ns.subhalo_index_unique, bh_ns.particle_subhalo_index)
            mass_bh_ns = np.copy(mpf.Index_Array(bh_ns.data["mass"],select_bh_ns,None))
            mass_bh_ns *= 1.989e43 * g2Msun / h

            coordinates_bh_ns = mpf.Index_Array(bh_ns.data["coordinates"],select_bh_ns,None)
            coordinates_bh_ns = mpf.Centre_Halo(coordinates_bh_ns, coordinates_halo_ns, boxsize) * 1e3 /h / (1+redshift_ns)

            mass_star_ns = np.copy(mpf.Index_Array(star_ns.data["mass"],select_star_ns,None))
            mass_star_ns *= 1.989e43 * g2Msun / h
            coordinates_star_ns = mpf.Index_Array(star_ns.data["coordinates"],select_star_ns,None)
            coordinates_star_ns = mpf.Centre_Halo(coordinates_star_ns, coordinates_halo_ns, boxsize) * 1e3 /h / (1+redshift_ns)

            internal_energy_gas_ns = mpf.Index_Array(gas_ns.data["internal_energy"],select_gas_ns,None)
            velocity_gas_ns = np.copy(mpf.Index_Array(gas_ns.data["velocity"],select_gas_ns,None)) * expansion_factor_ns**0.5
            velocity_gas_ns += - np.array([halo_vx_ns, halo_vy_ns, halo_vz_ns])

            coordinates_parts = [coordinates_gas_ns, coordinates_dm_ns, coordinates_star_ns, coordinates_bh_ns]
            gas_props = [internal_energy_gas_ns, temperature_gas_ns, nh_gas_ns, velocity_gas_ns, SF_ns]
            mass_part = [mass_gas_ns, mass_dm * 1.989e43 * g2Msun / h, mass_star_ns, mass_bh_ns]
            halo_props = [halo_r200_host_ns, halo_group_number_ns, halo_subgroup_number_ns, halo_vmax_ns]
            sim_props = [redshift_ns, h, boxsize, omm]

            ISM_ns =  mpf.Select_ISM_M18(coordinates_parts, gas_props, mass_part, halo_props , sim_props)

        if debug and i_halo == ih_choose:
            print "ISM_ns", len(ISM_ns), len(ISM_ns[ISM_ns])

        if full_calculation:
            # Cross-match with FoF particle list for descendant subhalo to determine which are ISM in the descendant subhalo (but not other subhalos within the descendant FoF group)
            ISM_fof_ns = np.zeros_like(id_gas_fof_ns) < 0
            if len(id_gas_ns[ISM_ns]) > 0 and len(id_gas_fof_ns) > 0:
                ptr = ms.match(id_gas_fof_ns, id_gas_ns[ISM_ns],arr2_sorted=True)
                ok_match = ptr >= 0
                ISM_fof_ns[ok_match] = True

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ########################## Do the rate measurements #####################################

    ###### Identify dm accretion  #################
    if not skip_dark_matter and full_calculation:
        if len(id_dm_all_progen) > 0 and len(id_dm) > 0:
            ptr_accrete_dm, time_ptr_actual = ms.match(id_dm, id_dm_all_progen,time_taken = time_ptr_actual) # id_dm_all_progen gets matched once so ok not to presort
            accreted_dm = ptr_accrete_dm < 0

            halo_mass_dict["mass_diffuse_dm_accretion"] = len(id_dm[accreted_dm]) * mass_dm * 1.989e43 * g2Msun / h

        elif len(id_dm_all_progen) == 0:
            halo_mass_dict["mass_diffuse_dm_accretion"] = len(id_dm) * mass_dm* 1.989e43 * g2Msun / h
        
    

    ###### Identify baryonic (re)accretion onto central subhalo (also include contribution to cooling, SF & stellar recycling) #################
    if full_calculation and len(id_star_gas_all_progen) > 0 and len(id_star_gas) > 0:
        ptr, time_ptr_actual = ms.match(id_star_gas, id_star_gas_all_progen,time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0
            
        accreted = ok_match == False

        # Exclude halo reheat particles from the (main) accretion terms
        accreted_cool = np.copy(accreted) # But not from cooling terms

        if len(halo_reheat_ts["id"]) > 0:
            # Note in principle tracked lists can change dynamically within the loop - so I'll avoid presorting for now
            ptr, time_ptr_actual = ms.match(id_star_gas, halo_reheat_ts["id"],time_taken=time_ptr_actual)

            halo_outflow_dict["mass_reaccreted_hreheat"] += np.sum(halo_reheat_ts["mass"][ptr][ptr>=0]) # Note this only applies to the most conservative velocity cut

            # If we are using multiple wind velocity cuts, halo reheat particles can still contribute to accretion/cooling terms for the less conservative cuts
            if len(fVmax_cuts)>1:
                for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                    if fVmax_cut == fVmax_cuts[-1]:
                        continue # No need to check for the most conservative velocity cut

                    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"            

                    accreted_hreheat = accreted & (ptr>=0) & (halo_reheat_ts["vpmax"][ptr] > fVmax_cut)

                    halo_outflow_dict["mass_reaccreted"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat])
                    halo_outflow_dict["mZ_reaccreted"+fV_str] += np.sum((halo_reheat_ts["mass"]*halo_reheat_ts["metallicity"])[ptr][accreted_hreheat])

                    accreted_hreheat_from_mp = accreted & (ptr>=0) & (halo_reheat_ts["vpmax"][ptr] > fVmax_cut) & (halo_reheat_ts["from_mp"][ptr]==1)
                    halo_outflow_dict["mass_reaccreted_from_mp"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat_from_mp])
                    halo_outflow_dict["mZ_reaccreted_from_mp"+fV_str] += np.sum((halo_reheat_ts["mass"]*halo_reheat_ts["metallicity"])[ptr][accreted_hreheat_from_mp])

                    cold = temperature_star_gas < TLo_cut
                    halo_outflow_dict["mass_reaccreted_TLo"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat&cold])
                    halo_outflow_dict["mass_reaccreted_TLo_from_mp"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat_from_mp&cold])

                    if write_SNe_energy_fraction:
                        cold = tmax_star_gas < TLo_cut
                        halo_emass_dict["mass_reaccreted_TmaxLo"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat&cold])
                        halo_emass_dict["mass_reaccreted_TmaxLo_from_mp"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat_from_mp&cold])

                    # Cross-reference accreted particles against wind (meaning ejected from ISM of progenitor galaxy) particles for this subhalo
                    if len(wind_ts["id"]) > 0 & len(halo_reheat_ts["id"][ptr][accreted_hreheat]) > 0:
                        ptr_wind = ms.match(halo_reheat_ts["id"][ptr][accreted_hreheat], wind_ts["id"])
                        from_wind = ptr_wind >= 0
                        halo_outflow_dict["mass_reaccreted_from_wind"+fV_str] += np.sum(halo_reheat_ts["mass"][ptr][accreted_hreheat][from_wind])
                        halo_outflow_dict["mZ_reaccreted_from_wind"+fV_str] += np.sum((halo_reheat_ts["mass"]*halo_reheat_ts["metallicity"])[ptr][accreted_hreheat][from_wind])

                    if i_cut == fVmax_info[1]:
                        # Compute distribution of return times for returning particles
                        nt_ej_i, junk = np.histogram(np.array(halo_reheat_ts["time"][ptr][accreted_hreheat]),bins=tret_bins_ts,weights=np.array(halo_reheat_ts["mass"])[ptr][accreted_hreheat])
                        halo_t_dict["mass_t_ejecta_ret"] += nt_ej_i
                        
                        if extra_accretion_measurements and not bound_only_mode:
                            # Note - distances are only properly defined for accretion from the main progenitor
                            r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                            ok_nan = np.isnan(halo_reheat_ts["rmax"][ptr][accreted_hreheat_from_mp])==False
                            nr_ej_i, junk = np.histogram(np.array(halo_reheat_ts["rmax"][ptr][accreted_hreheat_from_mp][ok_nan])/r200,bins=bins_r_track_ej,weights=np.array(halo_reheat_ts["mass"])[ptr][accreted_hreheat_from_mp][ok_nan])
                            halo_t_dict["n_r_ejecta_ret"] += nr_ej_i
                        
            accreted = accreted & (ptr < 0)

        # Cross-match accreted gas against ejected particles to compute gas return rate
        if len(id_star_gas[accreted]) > 0 and len(id_gas_ejecta_fof_progen_list)>0:
            ptr, time_ptr_actual = ms.match(id_star_gas[accreted], id_gas_ejecta_fof_progen_list,time_taken=time_ptr_actual)
            reaccreted = np.copy(accreted)
            primordial = np.copy(accreted)
            reaccreted[accreted] = ptr >= 0
            primordial[accreted] = ptr < 0

            reaccreted_from_mp = np.copy(reaccreted)
            reaccreted_from_mp[accreted] = reaccreted[accreted] & (from_mp_gas_ejecta_fof_progen_list[ptr] == 1)

        else:
            reaccreted = np.zeros_like(accreted) < 0
            primordial = accreted
            reaccreted_from_mp = np.copy(reaccreted)

        if len(ejecta_ts["id"]) > 0 and len(id_star_gas[reaccreted])>0:
            ptr, time_ptr_actual = ms.match(np.array(ejecta_ts["id"]), id_star_gas[reaccreted],time_taken=time_ptr_actual)

            # Compute distribution of return times for returning particles
            nt_ej_i, junk = np.histogram(np.array(ejecta_ts["t"])[ptr>=0],bins=tret_bins_ts,weights=np.array(ejecta_ts["mass"])[ptr>=0])
            halo_t_dict["mass_t_ejecta_ret"] += nt_ej_i

            if extra_accretion_measurements and not bound_only_mode:
                r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                ok_nan = (np.isnan(ejecta_ts["rmax"][ptr>=0])==False) & (from_mp==1)
                nr_ej_i, junk = np.histogram(np.array(ejecta_ts["rmax"])[ptr>=0][ok_nan]/r200,bins=bins_r_track_ej,weights=np.array(ejecta_ts["mass"])[ptr>=0][ok_nan])
                halo_t_dict["n_r_ejecta_ret"] += nr_ej_i

        # Compute inter-halo transfer
        fof_transfer = primordial & in_master_ejecta_star_gas
        primordial[in_master_ejecta_star_gas] = False
    
        # Contribution of accreted stars that formed between now and the previous timestep to (mass) accretion, cooling, SF and stellar mass loss
        accreted_star_SF = accreted[fam_star_gas==1] & fresh_star
                                                    
        halo_outflow_dict["mass_fof_transfer"] += np.sum(mass_star_init[accreted_star_SF & fof_transfer[fam_star_gas==1]])
        halo_outflow_dict["mass_praccreted"] += np.sum(mass_star_init[accreted_star_SF & primordial[fam_star_gas==1]])
        halo_outflow_dict["mZ_fof_transfer"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF & fof_transfer[fam_star_gas==1]])
        halo_outflow_dict["mZ_praccreted"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF & primordial[fam_star_gas==1]])

        # We can assume that anything that comes in as a new star probably came in cold..
        halo_outflow_dict["mass_praccreted_TLo"] += np.sum(mass_star_init[accreted_star_SF & primordial[fam_star_gas==1]])
        halo_outflow_dict["mass_fof_transfer_TLo"] += np.sum(mass_star_init[accreted_star_SF & fof_transfer[fam_star_gas==1]])
        
        if write_SNe_energy_fraction:
            halo_emass_dict["mass_praccreted_TmaxLo"] += np.sum(mass_star_init[accreted_star_SF & primordial[fam_star_gas==1]])
            halo_emass_dict["mass_fof_transfer_TmaxLo"] += np.sum(mass_star_init[accreted_star_SF & fof_transfer[fam_star_gas==1]])

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_reaccreted"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted[fam_star_gas==1]])
            halo_outflow_dict["mZ_reaccreted"+fV_str] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF & reaccreted[fam_star_gas==1]])

            halo_outflow_dict["mass_reaccreted_from_mp"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted_from_mp[fam_star_gas==1]])
            halo_outflow_dict["mZ_reaccreted_from_mp"+fV_str] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF & reaccreted_from_mp[fam_star_gas==1]])

            halo_outflow_dict["mass_reaccreted_TLo"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted[fam_star_gas==1]])
            halo_outflow_dict["mass_reaccreted_TLo_from_mp"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted_from_mp[fam_star_gas==1]])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_reaccreted_TmaxLo"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted[fam_star_gas==1]])
                halo_emass_dict["mass_reaccreted_TmaxLo_from_mp"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted_from_mp[fam_star_gas==1]])
        
            # Cross-reference accreted particles against wind (meaning ejected from ISM of progenitor galaxy) particles for this subhalo
            if len(wind_ts["id"]) > 0 and len(mass_star_init[accreted_star_SF & reaccreted[fam_star_gas==1]]) > 0:
                ptr_wind = ms.match(id_star[accreted_star_SF & reaccreted[fam_star_gas==1]], wind_ts["id"])
                from_wind = ptr_wind >= 0
                halo_outflow_dict["mass_reaccreted_from_wind"+fV_str] += np.sum(mass_star_init[accreted_star_SF & reaccreted[fam_star_gas==1]][from_wind])
                halo_outflow_dict["mZ_reaccreted_from_wind"+fV_str] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF & reaccreted[fam_star_gas==1]][from_wind])


        accreted_star_SF = accreted_cool[fam_star_gas==1] & fresh_star # Allow hreheat to cooling / SF terms

        # Need to separate mass of accreted & newly formed stars because of recycling
        first_infall_star = np.zeros_like(id_star[accreted_star_SF])==0
        from_SF_star = np.zeros_like(id_star[accreted_star_SF])
        # Compute contribution from previously ejected particles (from ISM) to cooling rate
        # Note, the total cooling, n_cooled, includes this part - so don't double count in analysis
        if len(id_star[accreted_star_SF]) > 0 and len(wind_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(id_star[accreted_star_SF], wind_ts["id"],time_taken=time_ptr_actual)
            recooled_star = ptr >= 0
            if len(wind_ts["from_SF"][ptr][recooled_star])>0:
                from_SF_star[ recooled_star & (wind_ts["from_SF"][ptr]==1) ] = 1

            first_infall_star[ptr<0] = True

        else:
            recooled_star = np.zeros_like(id_star[accreted_star_SF])< 0

        galactic_transfer_star = in_master_wind_star[accreted_star_SF] & (recooled_star==False)

        from_SF_star[ galactic_transfer_star] =  from_SF_master_wind_star[accreted_star_SF][galactic_transfer_star]
        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_master_wind_star[accreted_star_SF][galactic_transfer_star]),bins=mej_bins, weights=mass_star_init[accreted_star_SF][galactic_transfer_star])
            halo_t_dict["mass_galtran_mchalo_ratio"] += counts

            first_infall_star[galactic_transfer_star] = False

        halo_outflow_dict["mass_prcooled"] += np.sum(mass_star_init[accreted_star_SF][first_infall_star])
        halo_outflow_dict["mass_galtran"] += np.sum(mass_star_init[accreted_star_SF][galactic_transfer_star])
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][first_infall_star])
        halo_outflow_dict["mZ_galtran"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][galactic_transfer_star])

        # Gas that is cooling onto the SF ISM for the first time
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[accreted_star_SF][first_infall_star]) 
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][first_infall_star])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[accreted_star_SF][recooled_star&(from_SF_star==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][recooled_star&(from_SF_star==0)])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[accreted_star_SF][galactic_transfer_star&(from_SF_star==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][galactic_transfer_star&(from_SF_star==0)])

        halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_star_init[accreted_star_SF][galactic_transfer_star&(from_SF_star==1)])
        halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][galactic_transfer_star&(from_SF_star==1)])
        
        # Particles that have been outside the halo always pass the velocity cut for recooled
        for fVmax_cut in fVmax_cuts:
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_star_init[accreted_star_SF][recooled_star])
            halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][recooled_star])
            
            # Gas that is cooling onto the SF ISM having been in the SF ISM in the past
            halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_star_init[accreted_star_SF][recooled_star&(from_SF_star==1)])
            halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_star*mass_star_init)[accreted_star_SF][recooled_star&(from_SF_star==1)])
            
        if len(wind_ts["id"]) > 0 and len(id_star[accreted_star_SF][recooled_star])>0:
            ptr, time_ptr_actual = ms.match(np.array(wind_ts["id"]), id_star[accreted_star_SF][recooled_star],time_taken=time_ptr_actual,arr2_sorted=True)

            # Compute distribution of return times for returning particles
            nt_wi_i, junk = np.histogram(np.array(wind_ts["t"])[ptr>=0],bins=tret_bins_ts, weights=np.array(wind_ts["mass"])[ptr>=0])
            halo_t_dict["mass_t_wind_ret"] += nt_wi_i

            from_mp = wind_ts["from_mp"][ptr>=0] == 1
            halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_star_init[accreted_star_SF][recooled_star][ptr][ptr>=0][from_mp])
            halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((mass_star_init*metallicity_star)[accreted_star_SF][recooled_star][ptr][ptr>=0][from_mp])

            halo_mass_dict["mass_new_stars_init_recooled_from_mp"] += np.sum(mass_star_init[accreted_star_SF][recooled_star][ptr][ptr>=0][from_mp])

            if extra_accretion_measurements and not bound_only_mode:
                r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                ok_nan = (np.isnan(wind_ts["rmax"][ptr>=0])==False) & (from_mp == 1)
                nr_wi_i, junk = np.histogram(np.array(wind_ts["rmax"])[ptr>=0][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(wind_ts["mass"])[ptr>=0][ok_nan])
                halo_t_dict["n_r_wind_ret"] += nr_wi_i

        halo_mass_dict["n_new_stars"] += len(mass_star_init[accreted_star_SF])
        halo_mass_dict["mass_new_stars"] += np.sum(mass_star[accreted_star_SF])
        halo_mass_dict["mZ_new_stars"] += np.sum((mass_star*metallicity_star)[accreted_star_SF])
        halo_mass_dict["mass_new_stars_init"] += np.sum(mass_star_init[accreted_star_SF])
        halo_mass_dict["mZ_new_stars_init"] += np.sum((mass_star_init*metallicity_star)[accreted_star_SF]) # Note that metallicity of stars is conserved through stellar evolution, so this is correct
        halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[accreted_star_SF])

        halo_mass_dict["mass_new_stars_init_prcooled"] += np.sum(mass_star_init[accreted_star_SF][first_infall_star])
        halo_mass_dict["mZ_new_stars_init_prcooled"] += np.sum((mass_star_init*metallicity_star)[accreted_star_SF][first_infall_star])
        halo_mass_dict["mass_new_stars_init_galtran"] += np.sum(mass_star_init[accreted_star_SF][galactic_transfer_star])
        halo_mass_dict["mZ_new_stars_init_galtran"] += np.sum((mass_star_init*metallicity_star)[accreted_star_SF][galactic_transfer_star])
        halo_mass_dict["mass_new_stars_init_recooled"] += np.sum(mass_star_init[accreted_star_SF][recooled_star])
        halo_mass_dict["mZ_new_stars_init_recooled"] += np.sum((mass_star_init*metallicity_star)[accreted_star_SF][recooled_star])

        # Contribution of accreted gas to (mass) accretion, reaccretion & cooling
        accreted_gas = accreted[fam_star_gas==2]
        reaccreted_gas = accreted_gas & reaccreted[fam_star_gas==2]
        reaccreted_gas_from_mp = accreted_gas & reaccreted_from_mp[fam_star_gas==2]
        primordial_gas = accreted_gas & primordial[fam_star_gas==2]
        fof_transfer_gas = accreted_gas & (fof_transfer[fam_star_gas==2])

        halo_outflow_dict["mass_praccreted"] += np.sum(mass_gas[primordial_gas ])
        halo_outflow_dict["mass_fof_transfer"] += np.sum(mass_gas[fof_transfer_gas ])
        halo_outflow_dict["mZ_praccreted"] += np.sum((metallicity_gas*mass_gas)[primordial_gas ])
        halo_outflow_dict["mZ_fof_transfer"] += np.sum((metallicity_gas*mass_gas)[fof_transfer_gas ])

        cold = temperature_gas < TLo_cut
        halo_outflow_dict["mass_praccreted_TLo"] += np.sum(mass_gas[primordial_gas &cold])
        halo_outflow_dict["mass_fof_transfer_TLo"] += np.sum(mass_gas[fof_transfer_gas &cold])

        if write_SNe_energy_fraction:
            cold_tmax = tmax_gas < TLo_cut
            halo_emass_dict["mass_praccreted_TmaxLo"] += np.sum(mass_gas[primordial_gas &cold_tmax])
            halo_emass_dict["mass_fof_transfer_TmaxLo"] += np.sum(mass_gas[fof_transfer_gas &cold_tmax])

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_reaccreted"+fV_str] += np.sum(mass_gas[reaccreted_gas ])
            halo_outflow_dict["mZ_reaccreted"+fV_str] += np.sum((metallicity_gas*mass_gas)[reaccreted_gas ])

            halo_outflow_dict["mass_reaccreted_from_mp"+fV_str] += np.sum(mass_gas[reaccreted_gas_from_mp ])
            halo_outflow_dict["mZ_reaccreted_from_mp"+fV_str] += np.sum((metallicity_gas*mass_gas)[reaccreted_gas_from_mp ])

            halo_outflow_dict["mass_reaccreted_TLo"+fV_str] += np.sum(mass_gas[reaccreted_gas &cold ])
            halo_outflow_dict["mass_reaccreted_TLo_from_mp"+fV_str] += np.sum(mass_gas[reaccreted_gas_from_mp &cold])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_reaccreted_TmaxLo"+fV_str] += np.sum(mass_gas[reaccreted_gas & cold_tmax ])
                halo_emass_dict["mass_reaccreted_TmaxLo_from_mp"+fV_str] += np.sum(mass_gas[reaccreted_gas_from_mp & cold_tmax])

            # Cross-reference accreted particles against wind (meaning ejected from ISM of progenitor galaxy) particles for this subhalo
            if len(wind_ts["id"]) > 0 and len(mass_gas[reaccreted_gas]) > 0:
                ptr_wind = ms.match(id_gas[reaccreted_gas], wind_ts["id"])
                from_wind = ptr_wind >= 0
                halo_outflow_dict["mass_reaccreted_from_wind"+fV_str] += np.sum(mass_gas[reaccreted_gas][from_wind])
                halo_outflow_dict["mZ_reaccreted_from_wind"+fV_str] += np.sum((metallicity_gas*mass_gas)[reaccreted_gas][from_wind])
            
        accreted_gas = accreted_cool[fam_star_gas==2] # Allow hreheat to cool terms
        
        prcooled_gas = np.zeros_like(id_gas[accreted_gas&(SF|ISM)]) == 0
        from_SF_gas = np.zeros_like(id_gas[accreted_gas&(SF|ISM)])
        if len(id_gas[accreted_gas&(SF|ISM)]) > 0 and len(wind_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(id_gas[accreted_gas&(ISM|SF)], wind_ts["id"],time_taken=time_ptr_actual)
            recooled_gas = ptr >= 0
            from_SF_gas[recooled_gas] = wind_ts["from_SF"][ptr][recooled_gas]
            prcooled_gas = ptr < 0

            recooled_gas_from_mp = recooled_gas & (wind_ts["from_mp"][ptr] == 1)

            # Add recooled particles to the nsfism_ts tracked list
            ptr, time_ptr_actual = ms.match(id_gas[accreted_gas&(ISM&(SF==False))], wind_ts["id"],time_taken=time_ptr_actual)
            ok_temp = ptr >= 0

            nsfism_ts["id"] = np.append(np.array(nsfism_ts["id"]), id_gas[accreted_gas&((SF==False)&ISM)][ok_temp])
            nsfism_ts["from_SF"] = np.append(np.array(nsfism_ts["from_SF"]), wind_ts["from_SF"][ptr][ok_temp])
            nsfism_ts["from_wind"] = np.append(np.array(nsfism_ts["from_wind"]), np.ones_like(id_gas[accreted_gas&((SF==False)&ISM)][ok_temp]))
            nsfism_ts["vpmax"] = np.append(np.array(nsfism_ts["vpmax"]), np.ones_like(id_gas[accreted_gas&((SF==False)&ISM)][ok_temp])+1e9)

            mpf.Check_Dict(nsfism_ts, "14y67qgfauj")


        else:
            recooled_gas = np.zeros_like(id_gas[accreted_gas&(ISM|SF)])< 0
            recooled_gas_from_mp = np.zeros_like(id_gas[accreted_gas&(ISM|SF)])< 0

        # Note to future self, you could in principle have material that goes ireheat of another galaxy that would not be counted as galtran here.
        # For now I'm going to assume this is negligible - but in principe it might not be....
        # You might want to redo this in the future
        
        galactic_transfer_gas = (recooled_gas == False) & in_master_wind[accreted_gas & (SF|ISM)]
        prcooled_gas[galactic_transfer_gas] = False
        from_SF_gas[galactic_transfer_gas] = from_SF_master_wind[accreted_gas & (SF|ISM)][galactic_transfer_gas]
        
        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_master_wind[accreted_gas & (SF|ISM)][galactic_transfer_gas]),bins=mej_bins, weights=mass_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas])
            halo_t_dict["mass_galtran_mchalo_ratio"] += counts

            
        halo_outflow_dict["mass_prcooled"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][prcooled_gas])
        halo_outflow_dict["mass_galtran"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas])
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][prcooled_gas])
        halo_outflow_dict["mZ_galtran"] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][galactic_transfer_gas])

        if write_SNe_energy_fraction:
            cold_tmax = tmax_gas < TLo_cut
            halo_emass_dict["mass_prcooled_TmaxLo"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][prcooled_gas&cold_tmax[accreted_gas&(SF|ISM)]])
            halo_emass_dict["mass_galtran_TmaxLo"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas&cold_tmax[accreted_gas&(SF|ISM)]])
            halo_emass_dict["mass_prcooled_TmaxHi"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][prcooled_gas&(cold_tmax[accreted_gas&(SF|ISM)]==False)])
            halo_emass_dict["mass_galtran_TmaxHi"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas&(cold_tmax[accreted_gas&(SF|ISM)]==False)])

        halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas&(from_SF_gas==1)&(SF[accreted_gas&(SF|ISM)])])
        halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][galactic_transfer_gas&(from_SF_gas==1)&(SF[accreted_gas&(SF|ISM)])])

        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][prcooled_gas&(SF[accreted_gas&(SF|ISM)])])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][prcooled_gas&(SF[accreted_gas&(SF|ISM)])])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas&(from_SF_gas==0)&(SF[accreted_gas&(SF|ISM)])])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][recooled_gas&(from_SF_gas==0)&(SF[accreted_gas&(SF|ISM)])])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas&(from_SF_gas==0)&(SF[accreted_gas&(SF|ISM)])])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][galactic_transfer_gas&(from_SF_gas==0)&(SF[accreted_gas&(SF|ISM)])])

        ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[accreted_gas&(SF|ISM)][prcooled_gas])
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[accreted_gas&(SF|ISM)][prcooled_gas]))
        ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas])
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[accreted_gas&(SF|ISM)][galactic_transfer_gas])+2)
        ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[accreted_gas&(SF|ISM)][recooled_gas])
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[accreted_gas&(SF|ISM)][recooled_gas&(recooled_gas_from_mp==False)])+1)
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[accreted_gas&(SF|ISM)][recooled_gas_from_mp])+4)
        mpf.Check_Dict(ism_ts, "ag7a8ytgds")

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas])
            halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][recooled_gas])

            halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas&(from_SF_gas==1)&(SF[accreted_gas&(SF|ISM)])])
            halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][recooled_gas&(from_SF_gas==1)&(SF[accreted_gas&(SF|ISM)])])

            halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas_from_mp])
            halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_gas*mass_gas)[accreted_gas&(SF|ISM)][recooled_gas_from_mp])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_recooled_TmaxLo"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas&cold_tmax[accreted_gas&(SF|ISM)]])
                halo_emass_dict["mass_recooled_TmaxLo_from_mp"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas_from_mp&cold_tmax[accreted_gas&(SF|ISM)]])
                halo_emass_dict["mass_recooled_TmaxHi"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas&(cold_tmax[accreted_gas&(SF|ISM)]==False)])
                halo_emass_dict["mass_recooled_TmaxHi_from_mp"+fV_str] += np.sum(mass_gas[accreted_gas&(SF|ISM)][recooled_gas_from_mp&(cold_tmax[accreted_gas&(SF|ISM)]==False)])

        if len(wind_ts["id"]) > 0 and len(id_gas[accreted_gas&(SF|ISM)][recooled_gas])>0:
            ptr, time_ptr_actual = ms.match(np.array(wind_ts["id"]), id_gas[accreted_gas&(SF|ISM)][recooled_gas],time_taken=time_ptr_actual,arr2_sorted=True)
            
            # Compute distribution of return times for returning particles
            nt_wi_i, junk = np.histogram(np.array(wind_ts["t"])[ptr>=0],bins=tret_bins_ts, weights=np.array(wind_ts["mass"])[ptr>=0])
            halo_t_dict["mass_t_wind_ret"] += nt_wi_i

    elif full_calculation and len(id_star_gas_all_progen) == 0 and len(id_star_gas) > 0:
        # In this case, we make the assumption that everything is smooth pristine accretion / cooling, or is galtran/fof_transfer
        # (and so is not part of halo reheat, and is not recycled/recooled)

        # Compute inter-halo transfer
        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_master_ejecta_star_gas[in_master_ejecta_star_gas]), bins=mej_bins, weights = mass_star_gas[in_master_ejecta_star_gas])
            halo_t_dict["mass_fof_transfer_mchalo_ratio"] += counts

        fof_transfer = in_master_ejecta_star_gas
            
        pristine = fof_transfer == False
        halo_outflow_dict["mass_fof_transfer"] += np.sum(mass_star_init[fresh_star & fof_transfer[fam_star_gas==1]])
        halo_outflow_dict["mass_fof_transfer"] += np.sum(mass_gas[fof_transfer[fam_star_gas==2]])
        halo_outflow_dict["mass_praccreted"] += np.sum(mass_star_init[fresh_star & pristine[fam_star_gas==1]])
        halo_outflow_dict["mass_praccreted"] += np.sum(mass_gas[pristine[fam_star_gas==2]])
        
        halo_outflow_dict["mZ_fof_transfer"] += np.sum((metallicity_star*mass_star_init)[fresh_star & fof_transfer[fam_star_gas==1]])
        halo_outflow_dict["mZ_fof_transfer"] += np.sum((metallicity_gas*mass_gas)[fof_transfer[fam_star_gas==2]])
        halo_outflow_dict["mZ_praccreted"] += np.sum((metallicity_star*mass_star_init)[fresh_star & pristine[fam_star_gas==1]])
        halo_outflow_dict["mZ_praccreted"] += np.sum((metallicity_gas*mass_gas)[pristine[fam_star_gas==2]])

        galactic_transfer = in_master_wind_star_gas

        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_master_wind_star_gas[galactic_transfer]),bins=mej_bins, weights=mass_star_gas[galactic_transfer])
            halo_t_dict["mass_galtran_mchalo_ratio"] += counts

        first_infall = galactic_transfer==False

        cooled = ISM | SF
        halo_outflow_dict["mass_galtran"]  += np.sum(mass_star_init[fresh_star & galactic_transfer[fam_star_gas==1]])
        halo_outflow_dict["mass_galtran"] += np.sum(mass_gas[cooled & galactic_transfer[fam_star_gas==2]])
        halo_outflow_dict["mass_prcooled"] += np.sum(mass_star_init[fresh_star & first_infall[fam_star_gas==1]])
        halo_outflow_dict["mass_prcooled"] += np.sum(mass_gas[first_infall[fam_star_gas==2]])

        halo_outflow_dict["mZ_galtran"]  += np.sum((metallicity_star*mass_star_init)[fresh_star & galactic_transfer[fam_star_gas==1]])
        halo_outflow_dict["mZ_galtran"] += np.sum((metallicity_gas*mass_gas)[cooled & galactic_transfer[fam_star_gas==2]])
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_star*mass_star_init)[fresh_star & first_infall[fam_star_gas==1]])
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_gas*mass_gas)[first_infall[fam_star_gas==2]])
        
        halo_mass_dict["n_new_stars"] += len(mass_star_init[fresh_star])
        halo_mass_dict["mass_new_stars"] += np.sum(mass_star[fresh_star])
        halo_mass_dict["mZ_new_stars"] += np.sum((mass_star*metallicity_star)[fresh_star])
        halo_mass_dict["mass_new_stars_init"] += np.sum(mass_star_init[fresh_star])
        halo_mass_dict["mZ_new_stars_init"] += np.sum((mass_star_init*metallicity_star)[fresh_star])
        halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[fresh_star])

        halo_mass_dict["mass_new_stars_init_prcooled"] += np.sum(mass_star_init[fresh_star&first_infall[fam_star_gas==1]])
        halo_mass_dict["mZ_new_stars_init_prcooled"] += np.sum((metallicity_star*mass_star_init)[fresh_star&first_infall[fam_star_gas==1]])
        halo_mass_dict["mass_new_stars_init_galtran"] += np.sum(mass_star_init[fresh_star&galactic_transfer[fam_star_gas==1]])
        halo_mass_dict["mZ_new_stars_init_galtran"] += np.sum((metallicity_star*mass_star_init)[fresh_star&galactic_transfer[fam_star_gas==1]])

        ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[cooled])
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[cooled&first_infall[fam_star_gas==2]]))
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[cooled&galactic_transfer[fam_star_gas==2]])+2)
        mpf.Check_Dict(ism_ts, "a9uf789ergtu")

    # For centrals, compute the gas accretion through mergers
    if halo_subgroup_number == 0:

        # Find particles that have merged with the main progenitor FoF group of this subhalo (that were bound to other FoF groups on the previous step)
        ok_mp = np.where(group_number_star_gas_all_progen != halo_group_number_progen)[0]

        # Note the all_progen list here has already had "smooth accretion" low-mass progenitors removed, and so do not count towards the merger rate.
        if len(id_star_gas_all_progen[ok_mp]) > 0 and len(id_star_gas_as) > 0:

            ptr, time_ptr_actual = ms.match(id_star_gas_as, id_star_gas_all_progen[ok_mp],time_taken=time_ptr_actual,arr2_sorted=True)
            ok_match = ptr >= 0
            
            merged = ok_match

            halo_outflow_dict["mass_hmerged"] += np.sum(mass_gas_only_star_gas_as[ merged ])

            # Contribution from freshly accreted stars formed within previous step
            if len(id_star_gas) > 0 and len(id_star_gas_as[merged]) > 0:
                ptr, time_ptr_actual = ms.match(id_star_gas, id_star_gas_as[merged],time_taken=time_ptr_actual,arr2_sorted=True)
                merged = ptr >= 0

                halo_outflow_dict["mass_hmerged"] += np.sum(mass_star_init[fresh_star & merged[fam_star_gas==1]]) # Account for any stars that formed within dt
                                
            if debug and i_halo == ih_choose:
                if i_halo == ih_choose:
                    print ""
                    print "Ratio of merger/smooth", np.log10(halo_outflow_dict["mass_hmerged"]), np.log10(halo_outflow_dict["mass_praccreted"])
                    print ""

    # For centrals, compute the gaseous inflow onto the ISM through galaxy mergers
    if halo_subgroup_number == 0:

        # Find particles that have merged with the main progenitor subhalo of this subhalo (that were bound to non-main-progenitor subhaloes on the previous step)
        ok_mp = from_mp_star_gas_all_progen == False

        # Determine if the merging particles came from the ISM of the merging progenitor
        merge_ism = ISM_star_gas_all_progen[ok_mp] | SF_star_gas_all_progen[ok_mp]
        merge_cgm = merge_ism == False

        # Note the all_progen list here has already had "smooth accretion" low-mass progenitors removed, and so do not count towards the merger rate.
        if len(id_star_gas_all_progen[ok_mp][merge_ism]) > 0 and len(id_gas[ISM|SF]) > 0:
            ptr, time_ptr_actual = ms.match(id_gas[ISM|SF], id_star_gas_all_progen[ok_mp][merge_ism],time_taken=time_ptr_actual,arr2_sorted=True)
            ok_match = ptr >= 0
            
            merged = ok_match

            halo_outflow_dict["mass_gmerged_ISM"] += np.sum(mass_gas[ISM|SF][ merged ])

            ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[ISM|SF][merged])
            ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF|ISM][merged])+3)
            mpf.Check_Dict(ism_ts, "87asdasdgyujhg")

        # Contribution from freshly accreted stars formed within previous step
        if len(id_star_gas) > 0 and len(id_star_gas_all_progen[ok_mp][merge_ism]) > 0:
            ptr, time_ptr_actual = ms.match(id_star[fresh_star], id_star_gas_all_progen[ok_mp][merge_ism],time_taken=time_ptr_actual,arr2_sorted=True)
            merged = ptr >= 0

            halo_outflow_dict["mass_gmerged_ISM"] += np.sum(mass_star_init[fresh_star][merged]) # Account for any stars that formed within dt
            halo_mass_dict["n_new_stars"] += len(mass_star_init[fresh_star][merged])
            halo_mass_dict["mass_new_stars"] += np.sum(mass_star[fresh_star][merged])
            halo_mass_dict["mZ_new_stars"] += np.sum((metallicity_star*mass_star)[fresh_star][merged])
            halo_mass_dict["mass_new_stars_init"] += np.sum(mass_star_init[fresh_star][merged])
            halo_mass_dict["mZ_new_stars_init"] += np.sum((metallicity_star*mass_star_init)[fresh_star][merged])
            halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[fresh_star][merged])

            halo_mass_dict["mass_new_stars_init_merge"] += np.sum(mass_star_init[fresh_star][merged])
            halo_mass_dict["mZ_new_stars_init_merge"] += np.sum((metallicity_star*mass_star_init)[fresh_star][merged])

        if len(id_star_gas_all_progen[ok_mp][merge_cgm]) > 0 and len(id_gas[ISM|SF]) > 0:
            ptr, time_ptr_actual = ms.match(id_gas[ISM|SF], id_star_gas_all_progen[ok_mp][merge_cgm],time_taken=time_ptr_actual,arr2_sorted=True)
            ok_match = ptr >= 0
            
            merged = ok_match

            halo_outflow_dict["mass_gmerged_CGM"] += np.sum(mass_gas[ISM|SF][ merged ])

            # Note origin is a little loose here, could be considered recycling, first-time infall - I'm happy to just call it merge for now
            ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[ISM|SF][merged])
            ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF|ISM][merged])+3)
            mpf.Check_Dict(ism_ts, "afya87dgfyhsaujd")

        # Contribution from freshly accreted stars formed within previous step
        if len(id_star_gas) > 0 and len(id_star_gas_all_progen[ok_mp][merge_cgm]) > 0:
            ptr, time_ptr_actual = ms.match(id_star[fresh_star], id_star_gas_all_progen[ok_mp][merge_cgm],time_taken=time_ptr_actual,arr2_sorted=True)
            merged = ptr >= 0

            halo_outflow_dict["mass_gmerged_CGM"] += np.sum(mass_star_init[fresh_star][merged]) # Account for any stars that formed within dt

            halo_mass_dict["n_new_stars"] += len(mass_star_init[fresh_star][merged])
            halo_mass_dict["mass_new_stars"] += np.sum(mass_star[fresh_star][merged])
            halo_mass_dict["mZ_new_stars"] += np.sum((metallicity_star*mass_star)[fresh_star][merged])
            halo_mass_dict["mass_new_stars_init"] += np.sum(mass_star_init[fresh_star][merged])
            halo_mass_dict["mZ_new_stars_init"] += np.sum((metallicity_star*mass_star_init)[fresh_star][merged])
            halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[fresh_star][merged])

            # Note origin is a little loose here, could be considered recycling, first-time infall - I'm happy to just call it merge for now
            halo_mass_dict["mass_new_stars_init_merge"] += np.sum(mass_star_init[fresh_star][merged])
            halo_mass_dict["mZ_new_stars_init_merge"] += np.sum((metallicity_star*mass_star_init)[fresh_star][merged])

    ###### Identify baryonic accretion onto satellite subhaloes ############
    # For now, I'm not going to distinguish between recycling/transfer here
    # To do this properly, I would need to combine the ejecta/wind lists of subhaloes together (EDIT-this is done though no?)
    # Same for halo_reheat, for now I'm just using halo_reheat of central subhalo
    # Note: as of right now, another issue is that I'm not r200 rebinding particles that are in satellite subhaloes
    if full_calculation:
        sats = (is_star_star_gas_as ==False) & (subgroup_number_star_gas_as!=halo_subgroup_number)
        id_gas_sats = id_star_gas_as[sats]
        mass_gas_sats = mass_star_gas_as[sats]
        #ISM_sats = (SF_star_gas_as[sats] | ISM_star_gas_as[sats])

        if len(id_star_gas_all_progen) > 0 and len(id_gas_sats) > 0:
            ptr, time_ptr_actual = ms.match(id_gas_sats, id_star_gas_all_progen,time_taken=time_ptr_actual,arr2_sorted=True)
            ok_match = ptr >= 0

            accreted_sat = ok_match == False

            # Exclude halo reheat particles
            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                if i_cut == fVmax_info[1]:
                    break
            if len(halo_reheat_ts["id"])>0 and len(halo_reheat_ts["id"][halo_reheat_ts["vpmax"] < fVmax_cut]) > 0: # Only using central halo reheat list here..., with fiducial velocity cut
                ptr, time_ptr_actual = ms.match(id_gas_sats, halo_reheat_ts["id"][halo_reheat_ts["vpmax"] < fVmax_cut],time_taken=time_ptr_actual)
                accreted_sat = accreted_sat & (ptr < 0)

            halo_outflow_dict["mass_smacc_sat"] += np.sum(mass_gas_sats[accreted_sat])
            

    ###### Identify particles that have joined the star-forming reservoir from the diffuse gas halo reservoir (or non-merging satellites)
    if full_calculation:
        CGM_progen = (SF_star_gas_all_progen==False) & (ISM_star_gas_all_progen==False)
        CGM_progen = CGM_progen & (temperature_star_gas_all_progen>0) # exclude any stars
        CGM_progen = CGM_progen & ((subgroup_number_descendant_star_gas_all_progen != halo_subgroup_number) | from_mp_star_gas_all_progen) # Exclude galaxy mergers

    if full_calculation and len(id_gas[SF]) > 0 and len(id_star_gas_all_progen[CGM_progen]) > 0:
        ptr, time_ptr_actual = ms.match(id_gas[SF], id_star_gas_all_progen[CGM_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0
        
        cooled = ok_match

        # Exclude ism reheat particles from the accretion terms
        if len(ism_reheat_ts["id"]) > 0:
            ptr, time_ptr_actual = ms.match(id_gas[SF], ism_reheat_ts["id"],time_taken=time_ptr_actual)
            
            cooled_SF = cooled & (ptr >= 0)

            cooled = cooled & (ptr < 0)
            halo_outflow_dict["mass_recooled_ireheat"] += np.sum(ism_reheat_ts["mass"][ptr][ptr>=0])

            # ism reheat particles can contribute to _SF cooling terms
            prcooled_SF = cooled_SF & (ism_reheat_ts["from_SF"][ptr] == 0)

            galtran_SF = cooled_SF & (ism_reheat_ts["from_SF"][ptr]==0) & (from_SF_master_wind[SF]==1)
            prcooled_SF[galtran_SF] = False

            halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][prcooled_SF])
            halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas[SF]*mass_gas[SF])[prcooled_SF])
            halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_gas[SF][galtran_SF])
            halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_gas[SF]*mass_gas[SF])[galtran_SF])

            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                recooled_SF = cooled_SF & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut)
                halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_gas[SF][recooled_SF])
                halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_gas[SF]*mass_gas[SF])[recooled_SF])

                recooled_SF_from_mp = cooled_SF & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut) & (ism_reheat_ts["from_mp"][ptr]==1)
                halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_gas[SF][recooled_SF_from_mp])
                halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_gas[SF]*mass_gas[SF])[recooled_SF_from_mp])

                recooled_SF_from_SF = cooled_SF & (ism_reheat_ts["from_SF"][ptr] == 1) & ((ism_reheat_ts["from_wind"][ptr] == 1) | (ism_reheat_ts["vpmax"][ptr] > fVmax_cut))
                halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_gas[SF][recooled_SF_from_SF])
                halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_gas[SF]*mass_gas[SF])[recooled_SF_from_SF])

                if i_cut == fVmax_info[1]:
                    # Compute distribution of return times for returning particles
                    nt_wind_i, junk = np.histogram(np.array(ism_reheat_ts["time"][ptr][recooled_SF]),bins=tret_bins_ts,weights=np.array(ism_reheat_ts["mass"])[ptr][recooled_SF])
                    halo_t_dict["mass_t_wind_ret"] += nt_wind_i

                    if extra_accretion_measurements and not bound_only_mode:
                        r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                        ok_nan = (ism_reheat_ts["rmax"][ptr][recooled_SF]==False) & (ism_reheat_ts["from_mp"][ptr][recooled_SF]==1)
                        nr_wi_i, junk = np.histogram(np.array(ism_reheat_ts["rmax"])[ptr][recooled_SF][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(ism_reheat_ts["mass"])[ptr][recooled_SF][ok_nan])
                        halo_t_dict["n_r_wind_ret"] += nr_wi_i

                    ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[SF][recooled_SF])
                    ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF][recooled_SF&(recooled_SF_from_mp==False)])+1)
                    ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF][recooled_SF_from_mp])+4)
                    mpf.Check_Dict(ism_ts, "875284tghgf")

                if write_SNe_energy_fraction:
                    recooled_SF_cold = recooled_SF & (tmax_gas[SF]<TLo_cut)
                    recooled_SF_cold_from_mp = recooled_SF_from_mp & (tmax_gas[SF]<TLo_cut)
                    recooled_SF_hot = recooled_SF & (tmax_gas[SF]>TLo_cut)
                    recooled_SF_hot_from_mp = recooled_SF_from_mp & (tmax_gas[SF]>TLo_cut)

                    halo_emass_dict["mass_recooled_TmaxLo"+fV_str] += np.sum(mass_gas[SF][recooled_SF_cold])
                    halo_emass_dict["mass_recooled_TmaxLo_from_mp"+fV_str] += np.sum(mass_gas[SF][recooled_SF_cold_from_mp])
                    halo_emass_dict["mass_recooled_TmaxHi"+fV_str] += np.sum(mass_gas[SF][recooled_SF_hot])
                    halo_emass_dict["mass_recooled_TmaxHi_from_mp"+fV_str] += np.sum(mass_gas[SF][recooled_SF_hot_from_mp])

        if len(id_gas[SF][cooled]) > 0 and len(wind_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(id_gas[SF][cooled], wind_ts["id"],time_taken=time_ptr_actual)
            recooled = ptr >= 0
            from_SF = np.zeros_like(recooled)
            from_SF[recooled] = wind_ts["from_SF"][ptr][recooled]
            prcooled = ptr < 0
            recooled_from_mp = recooled & (wind_ts["from_mp"][ptr] == 1)

        else:
            recooled = np.zeros_like(id_gas[SF][cooled])< 0
            prcooled = np.zeros_like(id_gas[SF][cooled]) == 0
            from_SF = np.zeros_like(id_gas[SF][cooled])
            recooled_from_mp = np.copy(recooled)

        # Note to future self, you could in principle have material that goes ireheat of another galaxy that would not be counted as galtran here.
        # For now I'm going to assume this is negligible - but in principe it might not be....
        # You might want to redo this in the future
        # Compute galactic transfer contribution to the cooling rate

        galactic_transfer = in_master_wind[SF][cooled] & (recooled==False)

        from_SF[galactic_transfer] = from_SF_master_wind[SF][cooled][galactic_transfer]
        prcooled[galactic_transfer] = False

        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_master_wind[SF][cooled][galactic_transfer]),bins=mej_bins, weights=mass_gas[SF][cooled][galactic_transfer])
            halo_t_dict["mass_galtran_mchalo_ratio"] += counts

        halo_outflow_dict["mass_prcooled"] += np.sum(mass_gas[SF][cooled][prcooled])
        halo_outflow_dict["mass_galtran"] += np.sum(mass_gas[SF][cooled][galactic_transfer])
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_gas*mass_gas)[SF][cooled][prcooled])
        halo_outflow_dict["mZ_galtran"] += np.sum((metallicity_gas*mass_gas)[SF][cooled][galactic_transfer])

        if write_SNe_energy_fraction:
            cold_tmax = tmax_gas < TLo_cut
            halo_emass_dict["mass_prcooled_TmaxLo"] += np.sum(mass_gas[SF][cooled][prcooled][cold_tmax[SF][cooled][prcooled]])
            halo_emass_dict["mass_prcooled_TmaxHi"] += np.sum(mass_gas[SF][cooled][prcooled][(cold_tmax[SF][cooled][prcooled]==False)])
            halo_emass_dict["mass_galtran_TmaxLo"] += np.sum(mass_gas[SF][cooled][galactic_transfer][cold_tmax[SF][cooled][galactic_transfer]])
            halo_emass_dict["mass_galtran_TmaxHi"] += np.sum(mass_gas[SF][cooled][galactic_transfer][(cold_tmax[SF][cooled][galactic_transfer]==False)])

        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][cooled][prcooled])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled][prcooled])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][cooled][recooled&(from_SF==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled][recooled&(from_SF==0)])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][cooled][galactic_transfer&(from_SF==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled][galactic_transfer&(from_SF==0)])

        halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_gas[SF][cooled][galactic_transfer&(from_SF==1)])
        halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled][galactic_transfer&(from_SF==1)])

        ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[SF][cooled])
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF][cooled][prcooled]))
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF][cooled][galactic_transfer])+2)
        # Note integrity check performed below

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_gas[SF][cooled][recooled])
            halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_gas*mass_gas)[SF][cooled][recooled])

            halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_gas[SF][cooled][recooled_from_mp])
            halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_gas*mass_gas)[SF][cooled][recooled_from_mp])

            halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_gas[SF][cooled][recooled&(from_SF==1)])
            halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_gas*mass_gas)[SF][cooled][recooled&(from_SF==1)])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_recooled_TmaxLo"+fV_str] += np.sum(mass_gas[SF][cooled][recooled][cold_tmax[SF][cooled][recooled]])
                halo_emass_dict["mass_recooled_TmaxHi"+fV_str] += np.sum(mass_gas[SF][cooled][recooled][(cold_tmax[SF][cooled][recooled]==False)])
                halo_emass_dict["mass_recooled_TmaxLo_from_mp"+fV_str] += np.sum(mass_gas[SF][cooled][recooled_from_mp][cold_tmax[SF][cooled][recooled_from_mp]])
                halo_emass_dict["mass_recooled_TmaxHi_from_mp"+fV_str] += np.sum(mass_gas[SF][cooled][recooled_from_mp][(cold_tmax[SF][cooled][recooled_from_mp]==False)])

            if i_cut == fVmax_info[1]:
                ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF][cooled][recooled&(recooled_from_mp==False)])+1)
                ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[SF][cooled][recooled_from_mp])+4)
                mpf.Check_Dict(ism_ts, "sgsgu7yuhff")

        if len(wind_ts["id"]) > 0 and len(id_gas[SF][cooled][recooled])>0:
            ptr, time_ptr_actual = ms.match(np.array(wind_ts["id"]), id_gas[SF][cooled][recooled],time_taken=time_ptr_actual,arr2_sorted=True)
            
            # Compute distribution of return times for returning particles
            nt_wi_i, junk = np.histogram(np.array(wind_ts["t"])[ptr>=0],bins=tret_bins_ts, weights=np.array(wind_ts["mass"])[ptr>=0])
            halo_t_dict["mass_t_wind_ret"] += nt_wi_i

            if extra_accretion_measurements and not bound_only_mode:
                r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                ok_nan = (np.isnan(wind_ts["rmax"][ptr>=0])==False) & (wind_ts["from_mp"][ptr>=0]==1)
                nr_wi_i, junk = np.histogram(np.array(wind_ts["rmax"])[ptr>=0][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(wind_ts["mass"])[ptr>=0][ok_nan])
                halo_t_dict["n_r_wind_ret"] += nr_wi_i

    if debug and i_halo == ih_choose:
        print "at start of this step, number of particles in the ism/halo_reheated_lists", len(id_gas_ism_reheat_ts), len(halo_reheat_ts["id"])

    ###### Identify particles that have joined the non-star-forming ISM
    if full_calculation and len(id_gas[(SF==False)&ISM]) > 0 and len(id_star_gas_all_progen[CGM_progen]) > 0:
        ptr, time_ptr_actual = ms.match(id_gas[(SF==False)&ISM], id_star_gas_all_progen[CGM_progen],time_taken=time_ptr_actual,arr2_sorted=True)

        ok_match = ptr >= 0

        cooled = ok_match

        # Exclude ism reheat particles from the cooling terms
        if len(ism_reheat_ts["id"]) > 0:
            ptr, time_ptr_actual = ms.match(id_gas[(SF==False)&ISM], ism_reheat_ts["id"],time_taken=time_ptr_actual)

            # For multiple velocity cuts, ISM reheat particles can make into the cooling terms for less aggressive v cuts
            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                if fVmax_cut == fVmax_cuts[-1]:
                    continue # No need to do this for most severe cut
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                recooled_ireheat = cooled & (ptr>=0) & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut)
                recooled_ireheat_from_mp = recooled_ireheat & (ism_reheat_ts["from_mp"][ptr]==1)

                halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][recooled_ireheat])
                halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_gas*mass_gas)[(SF==False)&ISM][recooled_ireheat])

                halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][recooled_ireheat_from_mp])
                halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_gas*mass_gas)[(SF==False)&ISM][recooled_ireheat_from_mp])
                
                if i_cut == fVmax_info[1]:
                    # Compute distribution of return times for returning particles
                    nt_wind_i, junk = np.histogram(np.array(ism_reheat_ts["time"][ptr][recooled_ireheat]),bins=tret_bins_ts,weights=np.array(ism_reheat_ts["mass"])[ptr][recooled_ireheat])
                    halo_t_dict["mass_t_wind_ret"] += nt_wind_i

                    if extra_accretion_measurements and not bound_only_mode:
                        r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                        ok_nan = (ism_reheat_ts["rmax"][ptr][recooled_ireheat]==False) & (ism_reheat_ts["from_mp"][ptr][recooled_ireheat]==1)
                        nr_wi_i, junk = np.histogram(np.array(ism_reheat_ts["rmax"])[ptr][recooled_ireheat][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(ism_reheat_ts["mass"])[ptr][recooled_ireheat][ok_nan])
                        halo_t_dict["n_r_wind_ret"] += nr_wi_i

                    ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[(SF==False)&ISM][recooled_ireheat])
                    ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[(SF==False)&ISM][recooled_ireheat&(recooled_ireheat_from_mp==False)])+1)
                    ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[(SF==False)&ISM][recooled_ireheat_from_mp])+4)
                    mpf.Check_Dict(ism_ts, "5t672uywgfdsjugf")


                if write_SNe_energy_fraction:
                    recooled_cold = recooled_ireheat & (tmax_gas[(SF==False)&ISM]<TLo_cut)
                    recooled_cold_from_mp = recooled_ireheat_from_mp & (tmax_gas[(SF==False)&ISM]<TLo_cut)
                    recooled_hot = recooled_ireheat & (tmax_gas[(SF==False)&ISM]>TLo_cut)
                    recooled_hot_from_mp = recooled_ireheat_from_mp & (tmax_gas[(SF==False)&ISM]>TLo_cut)

                    halo_emass_dict["mass_recooled_TmaxLo"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][recooled_cold])
                    halo_emass_dict["mass_recooled_TmaxLo_from_mp"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][recooled_cold_from_mp])
                    halo_emass_dict["mass_recooled_TmaxHi"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][recooled_hot])
                    halo_emass_dict["mass_recooled_TmaxHi_from_mp"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][recooled_hot_from_mp])

                
            cooled = cooled & (ptr < 0)
            halo_outflow_dict["mass_recooled_ireheat"] += np.sum(ism_reheat_ts["mass"][ptr][ptr>=0])

            # Add returning particles to nsfism_ts tracked list
            nsfism_ts["id"] = np.append(np.array(nsfism_ts["id"]), id_gas[(SF==False)&ISM][ptr>=0])
            nsfism_ts["from_SF"] = np.append(np.array(nsfism_ts["from_SF"]), ism_reheat_ts["from_SF"][ptr][ptr>=0])
            nsfism_ts["from_wind"] = np.append(np.array(nsfism_ts["from_wind"]), ism_reheat_ts["from_wind"][ptr][ptr>=0])
            nsfism_ts["vpmax"] = np.append(np.array(nsfism_ts["vpmax"]), ism_reheat_ts["vpmax"][ptr][ptr>=0])

            mpf.Check_Dict(nsfism_ts, "fu827fh")


        if len(id_gas[(SF==False)&ISM][cooled]) > 0 and len(wind_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(id_gas[(SF==False)&ISM][cooled], wind_ts["id"],time_taken=time_ptr_actual)
            recooled = ptr >= 0
            prcooled = ptr < 0

            recooled_from_mp = recooled & (wind_ts["from_mp"][ptr] ==1)
 
            # Add recooled particles to the nsfism_ts tracked list
            nsfism_ts["id"] = np.append(np.array(nsfism_ts["id"]), id_gas[(SF==False)&ISM][cooled][recooled])
            nsfism_ts["from_SF"] = np.append(np.array(nsfism_ts["from_SF"]), wind_ts["from_SF"][ptr][recooled])
            nsfism_ts["from_wind"] = np.append(np.array(nsfism_ts["from_wind"]), np.ones_like(id_gas[(SF==False)&ISM][cooled][recooled]))
            nsfism_ts["vpmax"] = np.append(np.array(nsfism_ts["vpmax"]), np.ones_like(id_gas[(SF==False)&ISM][cooled][recooled]) + 1e9 )

            mpf.Check_Dict(nsfism_ts, "735rtygwfi8")

        else:
            recooled = np.zeros_like(id_gas[(SF==False)&ISM][cooled])< 0
            prcooled = np.zeros_like(id_gas[(SF==False)&ISM][cooled]) == 0
            recooled_from_mp = np.copy(recooled)

        # Note to future self, you could in principle have material that goes ireheat of another galaxy that would not be counted as galtran here.
        # For now I'm going to assume this is negligible - but in principe it might not be....
        # You might want to redo this in the future
        # Compute galactic transfer contribution to the cooling rate

        galactic_transfer = in_master_wind[(SF==False)&ISM][cooled] & (recooled==False)
        prcooled[galactic_transfer] = False

        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_wind_master[(SF==False)&ISM][cooled][galactic_transfer]),bins=mej_bins, weights=mass_gas[ISM&(SF==False)][cooled][galactic_transfer])
            halo_t_dict["mass_galtran_mchalo_ratio"] += counts

        halo_outflow_dict["mass_prcooled"] += np.sum(mass_gas[(SF==False)&ISM][cooled][prcooled])
        halo_outflow_dict["mass_galtran"] += np.sum(mass_gas[(SF==False)&ISM][cooled][galactic_transfer])           
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_gas*mass_gas)[(SF==False)&ISM][cooled][prcooled])
        halo_outflow_dict["mZ_galtran"] += np.sum((metallicity_gas*mass_gas)[(SF==False)&ISM][cooled][galactic_transfer])           

        if write_SNe_energy_fraction:
            cold_tmax = tmax_gas < TLo_cut
            halo_emass_dict["mass_prcooled_TmaxLo"] += np.sum(mass_gas[(SF==False)&ISM][cooled][prcooled][cold_tmax[(SF==False)&ISM][cooled][prcooled]])
            halo_emass_dict["mass_prcooled_TmaxHi"] += np.sum(mass_gas[(SF==False)&ISM][cooled][prcooled][(cold_tmax[(SF==False)&ISM][cooled][prcooled]==False)])
            halo_emass_dict["mass_galtran_TmaxLo"] += np.sum(mass_gas[(SF==False)&ISM][cooled][galactic_transfer][cold_tmax[(SF==False)&ISM][cooled][galactic_transfer]])
            halo_emass_dict["mass_galtran_TmaxHi"] += np.sum(mass_gas[(SF==False)&ISM][cooled][galactic_transfer][(cold_tmax[(SF==False)&ISM][cooled][galactic_transfer]==False)])

        ism_ts["id"] = np.append(np.array(ism_ts["id"]), id_gas[(SF==False)&ISM][cooled])
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[(SF==False)&ISM][cooled][prcooled]))
        ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[(SF==False)&ISM][cooled][galactic_transfer])+2)
        # Note integrity check performed below

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"            

            halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][cooled][recooled])
            halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_gas*mass_gas)[(SF==False)&ISM][cooled][recooled])

            halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][cooled][recooled_from_mp])
            halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_gas*mass_gas)[(SF==False)&ISM][cooled][recooled_from_mp])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_recooled_TmaxLo"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][cooled][recooled][cold_tmax[(SF==False)&ISM][cooled][recooled]])
                halo_emass_dict["mass_recooled_TmaxHi"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][cooled][recooled][(cold_tmax[(SF==False)&ISM][cooled][recooled]==False)])
                halo_emass_dict["mass_recooled_TmaxLo_from_mp"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][cooled][recooled_from_mp][cold_tmax[(SF==False)&ISM][cooled][recooled_from_mp]])
                halo_emass_dict["mass_recooled_TmaxHi_from_mp"+fV_str] += np.sum(mass_gas[(SF==False)&ISM][cooled][recooled_from_mp][(cold_tmax[(SF==False)&ISM][cooled][recooled_from_mp]==False)])

            if i_cut == fVmax_info[1]:
                ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[(SF==False)&ISM][cooled][recooled&(recooled_from_mp==False)])+1)
                ism_ts["cool_origin"] = np.append(np.array(ism_ts["cool_origin"]), np.zeros_like(id_gas[(SF==False)&ISM][cooled][recooled_from_mp])+4)
                mpf.Check_Dict(ism_ts, "sgsgu7yuhff")

        
        if len(wind_ts["id"]) > 0 and len(id_gas[(SF==False)&ISM][cooled][recooled])>0:
            ptr, time_ptr_actual = ms.match(np.array(wind_ts["id"]), id_gas[(SF==False)&ISM][cooled][recooled],time_taken=time_ptr_actual,arr2_sorted=True)
            
            # Compute distribution of return times for returning particles
            nt_wi_i, junk = np.histogram(np.array(wind_ts["t"])[ptr>=0],bins=tret_bins_ts, weights=np.array(wind_ts["mass"])[ptr>=0])
            halo_t_dict["mass_t_wind_ret"] += nt_wi_i

            if extra_accretion_measurements and not bound_only_mode:
                r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                ok_nan = (np.isnan(wind_ts["rmax"][ptr>=0])==False) & (wind_ts["from_mp"][ptr>=0]==1)
                nr_wi_i, junk = np.histogram(np.array(wind_ts["rmax"])[ptr>=0][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(wind_ts["mass"])[ptr>=0][ok_nan])
                halo_t_dict["n_r_wind_ret"] += nr_wi_i

    ########## Identify particles that moved from NSF ISM to SF ISM ###########
    if full_calculation and len(id_gas_progen[(SF_progen==False)&ISM_progen]) > 0 and len(id_gas[SF]) > 0:
        ptr, time_ptr_actual = ms.match(id_gas[SF], id_gas_progen[(SF_progen==False)&ISM_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0
        cooled_SF = ok_match

        from_SF = np.zeros_like(id_gas[SF][cooled_SF])
        if len(id_gas[SF][cooled_SF]) > 0 and len(nsfism_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(id_gas[SF][cooled_SF], nsfism_ts["id"],time_taken=time_ptr_actual)
            from_SF[ptr>=0] = nsfism_ts["from_SF"][ptr][ptr>=0]

            recooled_SF = (ptr >= 0) & (nsfism_ts["from_wind"][ptr] == 1)
            prcooled_SF = (ptr < 0) | ((nsfism_ts["from_wind"][ptr]==0)&(nsfism_ts["from_SF"][ptr]==0))
        else:
            recooled_SF = np.zeros_like(id_gas[SF][cooled_SF])< 0
            prcooled_SF = np.zeros_like(id_gas[SF][cooled_SF]) == 0
            ptr = []

        # Compute galactic transfer contribution to the cooling rate
        galactic_transfer = in_master_wind[SF][cooled_SF] & (recooled_SF==False)
        from_SF[galactic_transfer] = from_SF_master_wind[SF][cooled_SF][galactic_transfer]
        prcooled_SF[galactic_transfer] = False

        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][cooled_SF][prcooled_SF])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled_SF][prcooled_SF])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][cooled_SF][recooled_SF&(from_SF==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled_SF][recooled_SF&(from_SF==0)])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_gas[SF][cooled_SF][galactic_transfer&(from_SF==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled_SF][galactic_transfer&(from_SF==0)])

        halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_gas[SF][cooled_SF][galactic_transfer&(from_SF==1)])
        halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_gas*mass_gas)[SF][cooled_SF][galactic_transfer&(from_SF==1)])

        for fVmax_cut in fVmax_cuts:
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"     

            if len(ptr) > 0:
                recooled_SF_i = recooled_SF | ((nsfism_ts["vpmax"][ptr] > fVmax_cut) & (ptr>=0))
            else:
                recooled_SF_i = recooled_SF

            halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_gas[SF][cooled_SF][recooled_SF_i&(from_SF==1)])
            halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_gas*mass_gas)[SF][cooled_SF][recooled_SF_i&(from_SF==1)])

    ########## Identify particles that moved from SF ISM to NSF ISM ###########
    if full_calculation and len(id_gas_progen[SF_progen]) > 0 and len(id_gas[(SF==False)&ISM]) > 0:
        ptr, time_ptr_actual = ms.match(id_gas[(SF==False)&ISM], id_gas_progen[SF_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        # Note here that in principle these particles are elidgible to join the wind (if NSF ISM is not being used)
        # I'm going to make the aproximation that particles that particles that would pass the wind criteria wwould then also leave the NSF ISM and pass the wind
        # criteria anyway on the next step. Given that NSF ISM included is fiducial choice, I think this is fine.

        nsfism_ts["id"] = np.append(np.array(nsfism_ts["id"]), id_gas[(SF==False)&ISM][ok_match])
        nsfism_ts["from_SF"] = np.append(np.array(nsfism_ts["from_SF"]), np.ones_like(id_gas[(SF==False)&ISM][ok_match]))
        nsfism_ts["from_wind"] = np.append(np.array(nsfism_ts["from_wind"]), np.zeros_like(id_gas[(SF==False)&ISM][ok_match]))
        nsfism_ts["vpmax"] = np.append(np.array(nsfism_ts["vpmax"]), np.zeros_like(id_gas[(SF==False)&ISM][ok_match]))

        mpf.Check_Dict(nsfism_ts, "297419fhdsk")

    # Find particles that were directly heated from outside ISM
    if full_calculation and write_SNe_energy_fraction and len(id_gas[dir_heated])>0 and len(id_gas_progen[(SF_progen==False)&(ISM_progen==False)&(dir_heated_progen==False)])>0:
        ptr, time_ptr_actual = ms.match(id_gas[dir_heated], id_gas_progen[(SF_progen==False)&(ISM_progen==False)&(dir_heated_progen==False)],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr>=0
            
        halo_emass_dict["mass_dir_heat_in_nism"] += np.sum(mass_gas[dir_heated][ok_match])
        halo_emass_dict["mZ_dir_heat_in_nism"] += np.sum((metallicity_gas*mass_gas)[dir_heated][ok_match])

        if debug and i_halo == ih_choose:
            print "number of directly heated particles in nism halo (that are still bound)", len(mass_gas[dir_heated][ok_match])

    if full_calculation and write_SNe_energy_fraction and len(id_gas[dir_heated_SNe])>0 and len(id_gas_progen[(SF_progen==False)&(ISM_progen==False)&(dir_heated_progen_SNe==False)])>0:
        ptr, time_ptr_actual = ms.match(id_gas[dir_heated_SNe], id_gas_progen[(SF_progen==False)&(ISM_progen==False)&(dir_heated_progen_SNe==False)],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr>=0

        halo_emass_dict["mass_dir_heat_SNe_in_nism"] += np.sum(mass_gas[dir_heated_SNe][ok_match])
        halo_emass_dict["mZ_dir_heat_SNe_in_nism"] += np.sum((metallicity_gas*mass_gas)[dir_heated_SNe][ok_match])

    # Find particles that were directly heated from inside ISM
    if write_SNe_energy_fraction and len(id_gas[dir_heated])>0 and len(id_gas_progen[(SF_progen|ISM_progen)&(dir_heated_progen==False)])>0 and full_calculation:
        ptr, time_ptr_actual = ms.match(id_gas[dir_heated], id_gas_progen[(SF_progen|ISM_progen)&(dir_heated_progen==False)],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr>=0
            
        halo_emass_dict["mass_dir_heat_in_ism"] += np.sum(mass_gas[dir_heated][ok_match])
        halo_emass_dict["mZ_dir_heat_in_ism"] += np.sum((metallicity_gas*mass_gas)[dir_heated][ok_match])

        if debug and i_halo == ih_choose:
            print "number of directly heated particles in ism halo (that are still bound)", len(mass_gas[dir_heated][ok_match])

    if full_calculation and write_SNe_energy_fraction and len(id_gas[dir_heated_SNe])>0 and len(id_gas_progen[(SF_progen|ISM_progen)&(dir_heated_progen_SNe==False)])>0:
        ptr, time_ptr_actual = ms.match(id_gas[dir_heated_SNe], id_gas_progen[(SF_progen|ISM_progen)&(dir_heated_progen_SNe==False)],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr>=0

        halo_emass_dict["mass_dir_heat_SNe_in_ism"] += np.sum(mass_gas[dir_heated_SNe][ok_match])
        halo_emass_dict["mZ_dir_heat_SNe_in_ism"] += np.sum((metallicity_gas*mass_gas)[dir_heated_SNe][ok_match])

    ##### Identify particles which have left the star-forming reservoir and joined the diffuse gas halo reservoir
    if full_calculation:
        CGM = (SF_star_gas_as==False) & (ISM_star_gas_as==False) & (is_star_star_gas_as==False)

    else:
        CGM = (SF==False) & (ISM==False)
        
        # Hacky way to not have to duplicate the code below
        mass_star_gas_as = mass_gas; coordinates_star_gas_as = coordinates_gas; cv_star_gas_as = cv_gas
        metallicity_star_gas_as = metallicity_gas; id_star_gas_as = id_gas
        if write_SNe_energy_fraction:
            dir_heated_star_gas_as = dir_heated; oxygen_star_gas_as = oxygen_gas; iron_star_gas_as = iron_gas

        if do_ns_calculation:
            id_gas_fof_ns = id_gas_ns # Particles that move into another subhalo on _ns will be lost this way, and will be considered wind. This is fine with my correct working definition
            SF_fof_ns = SF_ns; ISM_fof_ns = ISM_ns; coordinates_gas_fof_ns = coordinates_gas_ns
            id_star_fof_ns = id_star_ns

    if do_ns_calculation and len(id_star_gas_as[CGM]) > 0 and len(id_gas_progen[SF_progen]) > 0:
        ptr, time_ptr_actual = ms.match(id_star_gas_as[CGM], id_gas_progen[SF_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        reheated = ok_match
        halo_outflow_dict["mass_sf_ism_reheated"] += np.sum(mass_star_gas_as[CGM][reheated])
        halo_outflow_dict["n_sf_ism_reheated"] += len(mass_star_gas_as[CGM][reheated])

        ######## Identify which particles join the wind ######
        t_list = [t_ps, t_ts, t_ns]
        data_gas_ps_in = [coordinates_gas_progen[SF_progen][ptr][reheated]]
        data_gas_ts_in = [id_star_gas_as[CGM][reheated], coordinates_star_gas_as[CGM][reheated], mass_star_gas_as[CGM][reheated],cv_star_gas_as[CGM][reheated]]
        data_gas_ns_in = [id_gas_fof_ns, SF_fof_ns, ISM_fof_ns, coordinates_gas_fof_ns]
        halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]

        select_wind, fVmax = mpf.Reheated_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, id_star_fof_ns, halo_props, fVmax_cuts)

        # Here, select wind applies to the maximum of the range of requested radial velocity cuts. Now we calculate which particles make it for other cuts
        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            select_wind_i = (fVmax > fVmax_cut)
            fV_str = str(fVmax_cut).replace(".","p")+"vmax"
            halo_outflow_dict["mass_sf_ism_join_wind_"+fV_str] += np.sum(mass_star_gas_as[CGM][reheated][select_wind_i])
            halo_outflow_dict["n_sf_ism_join_wind_"+fV_str] += len(mass_star_gas_as[CGM][reheated][select_wind_i])
            halo_outflow_dict["mZ_sf_ism_join_wind_"+fV_str] += np.sum((metallicity_star_gas_as*mass_star_gas_as)[CGM][reheated][select_wind_i])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_join_ism_wind_dir_heat_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum(mass_star_gas_as[CGM][reheated][select_wind_i&dir_heated_star_gas_as[CGM][reheated]])
                halo_emass_dict["mO_join_ism_wind_"+fV_str] += np.sum((oxygen_star_gas_as*mass_star_gas_as)[CGM][reheated][select_wind_i])
                halo_emass_dict["mFe_join_ism_wind_"+fV_str] += np.sum((iron_star_gas_as*mass_star_gas_as)[CGM][reheated][select_wind_i])

            # For these measurements, adopt the desired "fiducial" cut
            if i_cut == fVmax_info[1]:
                wind_cumul_ts["mass_ism_wind"] += np.sum(mass_star_gas_as[CGM][reheated][select_wind_i])
                wind_cumul_ts["n_ism_wind"] += len(mass_star_gas_as[CGM][reheated][select_wind_i])
                    
        # Add ejected particles that are selected as being in the wind (including their mass) to the wind particle lists
        wind_ts["id"] = np.append(np.array(wind_ts["id"]), id_star_gas_as[CGM][reheated][select_wind])
        wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), mass_star_gas_as[CGM][reheated][select_wind])
        wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(id_star_gas_as[CGM][reheated][select_wind]))
        wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(id_star_gas_as[CGM][reheated][select_wind]))
        wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), metallicity_star_gas_as[CGM][reheated][select_wind])
        wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), np.ones_like(id_star_gas_as[CGM][reheated][select_wind]))
        wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(id_star_gas_as[CGM][reheated][select_wind]))
        if extra_accretion_measurements:
            wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(mass_star_gas_as[CGM][reheated][select_wind])+halo_mchalo)
            if not bound_only_mode:
                wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(mass_star_gas_as[CGM][reheated][select_wind])+np.nan)
        mpf.Check_Dict(wind_ts, "iasdx") # Check all quantities have been added

        # Add the remaining ejected particles which are not not selected in the wind (including their mass) to the reheated particle lists
        ism_reheat_ts["id"] = np.append(np.array(ism_reheat_ts["id"]), id_star_gas_as[CGM][reheated][select_wind==False])
        ism_reheat_ts["mass"] = np.append(np.array(ism_reheat_ts["mass"]), mass_star_gas_as[CGM][reheated][select_wind==False])
        ism_reheat_ts["time"] = np.append(np.array(ism_reheat_ts["time"]), np.zeros_like(mass_star_gas_as[CGM][reheated][select_wind==False]))
        ism_reheat_ts["from_mp"] = np.append(np.array(ism_reheat_ts["from_mp"]), np.ones_like(mass_star_gas_as[CGM][reheated][select_wind==False]))
        ism_reheat_ts["metallicity"] = np.append(np.array(ism_reheat_ts["metallicity"]), metallicity_star_gas_as[CGM][reheated][select_wind==False])
        if write_SNe_energy_fraction:
            ism_reheat_ts["oxygen"] = np.append(np.array(ism_reheat_ts["oxygen"]), oxygen_star_gas_as[CGM][reheated][select_wind==False])
            ism_reheat_ts["iron"] = np.append(np.array(ism_reheat_ts["iron"]), iron_star_gas_as[CGM][reheated][select_wind==False])
            
        if len(fVmax_cuts) > 1:
            ism_reheat_ts["vpmax"] = np.append(np.array(ism_reheat_ts["vpmax"]), fVmax[select_wind==False])
        ism_reheat_ts["from_SF"] = np.append(np.array(ism_reheat_ts["from_SF"]), np.ones_like(id_star_gas_as[CGM][reheated][select_wind==False]))
        ism_reheat_ts["from_wind"] = np.append(np.array(ism_reheat_ts["from_wind"]), np.zeros_like(id_star_gas_as[CGM][reheated][select_wind==False]))
        if extra_accretion_measurements:
            ism_reheat_ts["mchalo_ej"] = np.append(np.array(ism_reheat_ts["mchalo_ej"]), np.zeros_like(mass_star_gas_as[CGM][reheated][select_wind==False])+halo_mchalo)
            if not bound_only_mode:
                ism_reheat_ts["rmax"] = np.append(np.array(ism_reheat_ts["rmax"]), np.zeros_like(mass_star_gas_as[CGM][reheated][select_wind==False])+np.nan)
        mpf.Check_Dict(ism_reheat_ts,"asdfjk")

        if debug and i_halo == ih_choose:
            print "Reheating rate (SF->NSF)", halo_outflow_dict["n_sf_ism_reheated"], " , ISM ejection rate (SF->NSF)", len(id_star_gas_as[CGM][reheated][select_wind])
            if write_SNe_energy_fraction:
                print "Rate of those that were directly heated", len(mass_star_gas_as[CGM][reheated][select_wind&dir_heated_star_gas_as[CGM][reheated]])

    ##### Identify particles which have left the non-star-forming ISM and joined the diffuse gas halo reservoir
    if do_ns_calculation and len(id_star_gas_as[CGM]) > 0 and len(id_gas_progen[(SF_progen==False)&ISM_progen]) > 0:
        ptr, time_ptr_actual = ms.match(id_star_gas_as[CGM], id_gas_progen[(SF_progen==False)&ISM_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        # Check that these particles are not already in the wind for this subhalo
        if len(id_star_gas_as[CGM]) > 0 and len(wind_ts["id"]) > 0:
            ptr_check, time_ptr_actual = ms.match(id_star_gas_as[CGM], wind_ts["id"],time_taken=time_ptr_actual)
            ok_match = ok_match & (ptr_check<0)

        reheated_nsf = ok_match
        halo_outflow_dict["mass_nsf_ism_reheated"] += np.sum(mass_star_gas_as[CGM][reheated_nsf])
        halo_outflow_dict["n_nsf_ism_reheated"] += len(mass_star_gas_as[CGM][reheated_nsf])

        # Decide which reheated particles join the wind
        t_list = [t_ps, t_ts, t_ns]
        data_gas_ps_in = [coordinates_gas_progen[(SF_progen==False)&ISM_progen][ptr][reheated_nsf]]
        data_gas_ts_in = [id_star_gas_as[CGM][reheated_nsf],\
                          coordinates_star_gas_as[CGM][reheated_nsf],\
                          mass_star_gas_as[CGM][reheated_nsf],\
                          cv_star_gas_as[CGM][reheated_nsf]]
        data_gas_ns_in = [id_gas_fof_ns, SF_fof_ns, ISM_fof_ns, coordinates_gas_fof_ns]
        halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]

        select_wind, fVmax = mpf.Reheated_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, id_star_fof_ns, halo_props, fVmax_cuts)

        # Find the subset of the particles joining the wind that came from the SF ISM
        from_SF = np.zeros_like(id_star_gas_as[CGM][reheated_nsf])
        if len(select_wind[select_wind]) > 0 and len(nsfism_ts["id"]) > 0 and len(nsfism_ts["id"][nsfism_ts["from_SF"]==1])>0:
            ptr, time_ptr_actual = ms.match(id_star_gas_as[CGM][reheated_nsf], nsfism_ts["id"][nsfism_ts["from_SF"]==1],time_taken=time_ptr_actual)
            from_SF[ptr>=0] = 1

        # Find the subset of the particles that were in the wind previously
        from_wind = np.zeros_like(id_star_gas_as[CGM][reheated_nsf])
        if len(select_wind[select_wind]) > 0 and len(nsfism_ts["id"]) > 0 and len(nsfism_ts["id"][nsfism_ts["from_wind"]==1])>0:
            ptr, time_ptr_actual = ms.match(id_star_gas_as[CGM][reheated_nsf], nsfism_ts["id"][nsfism_ts["from_wind"]==1],time_taken=time_ptr_actual)
            from_wind[ptr>=0] = 1

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            select_wind_i = (fVmax > fVmax_cut)
            halo_outflow_dict["mass_nsf_ism_join_wind_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum(mass_star_gas_as[CGM][reheated_nsf][select_wind_i])
            halo_outflow_dict["n_nsf_ism_join_wind_"+str(fVmax_cut).replace(".","p")+"vmax"] += len(mass_star_gas_as[CGM][reheated_nsf][select_wind_i])
            halo_outflow_dict["mZ_nsf_ism_join_wind_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum((metallicity_star_gas_as*mass_star_gas_as)[CGM][reheated_nsf][select_wind_i])
          
            halo_outflow_dict["mass_nsf_ism_join_wind_from_SF_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum(mass_star_gas_as[CGM][reheated_nsf][(from_SF==1)&select_wind_i])
            halo_outflow_dict["mZ_nsf_ism_join_wind_from_SF_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum((metallicity_star_gas_as*mass_star_gas_as)[CGM][reheated_nsf][(from_SF==1)&select_wind_i])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_join_ism_wind_dir_heat_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum(mass_star_gas_as[CGM][reheated_nsf][select_wind_i&dir_heated_star_gas_as[CGM][reheated_nsf]])
                halo_emass_dict["mO_join_ism_wind_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum((oxygen_star_gas_as*mass_star_gas_as)[CGM][reheated_nsf][(from_SF==1)&select_wind_i])
                halo_emass_dict["mFe_join_ism_wind_"+str(fVmax_cut).replace(".","p")+"vmax"] += np.sum((iron_star_gas_as*mass_star_gas_as)[CGM][reheated_nsf][(from_SF==1)&select_wind_i])

            # For these measurements, adopt the desired "fiducial" cut
            if i_cut == fVmax_info[1]:
                wind_cumul_ts["mass_ism_wind"] += np.sum(mass_star_gas_as[CGM][reheated_nsf][select_wind_i])
                wind_cumul_ts["n_ism_wind"] += len(mass_star_gas_as[CGM][reheated_nsf][select_wind_i])

        # Add ejected particles that are wind selected (including their mass) to the wind particle lists
        wind_ts["id"] = np.append(np.array(wind_ts["id"]), id_star_gas_as[CGM][reheated_nsf][select_wind])
        wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), mass_star_gas_as[CGM][reheated_nsf][select_wind])
        wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), metallicity_star_gas_as[CGM][reheated_nsf][select_wind])
        wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(id_star_gas_as[CGM][reheated_nsf][select_wind]))
        wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(id_star_gas_as[CGM][reheated_nsf][select_wind]))
        wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), from_SF[select_wind])
        wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(id_star_gas_as[CGM][reheated_nsf][select_wind]))
        if extra_accretion_measurements:
            wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(mass_star_gas_as[CGM][reheated_nsf][select_wind])+halo_mchalo)
            if not bound_only_mode:
                wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(mass_star_gas_as[CGM][reheated_nsf][select_wind])+np.nan)
            

        mpf.Check_Dict(wind_ts, "asdhkjas")

        # Add particles which do not qualify as winds to the reheated list
        ism_reheat_ts["id"] = np.append(np.array(ism_reheat_ts["id"]), id_star_gas_as[CGM][reheated_nsf][select_wind==False])
        ism_reheat_ts["mass"] = np.append(np.array(ism_reheat_ts["mass"]), mass_star_gas_as[CGM][reheated_nsf][select_wind==False])
        ism_reheat_ts["metallicity"] = np.append(np.array(ism_reheat_ts["metallicity"]), metallicity_star_gas_as[CGM][reheated_nsf][select_wind==False])
        if write_SNe_energy_fraction:
            ism_reheat_ts["oxygen"] = np.append(np.array(ism_reheat_ts["oxygen"]), oxygen_star_gas_as[CGM][reheated_nsf][select_wind==False])
            ism_reheat_ts["iron"] = np.append(np.array(ism_reheat_ts["iron"]), iron_star_gas_as[CGM][reheated_nsf][select_wind==False])
        ism_reheat_ts["time"] = np.append(np.array(ism_reheat_ts["time"]), np.zeros_like(mass_star_gas_as[CGM][reheated_nsf][select_wind==False]))
        ism_reheat_ts["from_mp"] = np.append(np.array(ism_reheat_ts["from_mp"]), np.ones_like(mass_star_gas_as[CGM][reheated_nsf][select_wind==False]))
        if len(fVmax_cuts) > 1:
            ism_reheat_ts["vpmax"] = np.append(np.array(ism_reheat_ts["vpmax"]), fVmax[select_wind==False])
        ism_reheat_ts["from_SF"] = np.append(np.array(ism_reheat_ts["from_SF"]), from_SF[select_wind==False])
        ism_reheat_ts["from_wind"] = np.append(np.array(ism_reheat_ts["from_wind"]), from_wind[select_wind==False])
        if extra_accretion_measurements:
            ism_reheat_ts["mchalo_ej"] = np.append(np.array(ism_reheat_ts["mchalo_ej"]), np.zeros_like(mass_star_gas_as[CGM][reheated_nsf][select_wind==False])+halo_mchalo)
            if not bound_only_mode:
                ism_reheat_ts["rmax"] = np.append(np.array(ism_reheat_ts["rmax"]), np.zeros_like(mass_star_gas_as[CGM][reheated_nsf][select_wind==False])+np.nan)
            
        mpf.Check_Dict(ism_reheat_ts, "sdhfskd")

        if debug and i_halo == ih_choose:
            print "rate of reheating (nsf ism > nism)", len(id_star_gas_as[CGM][reheated_nsf]), " , ISM ejection rate (nsf ism -> nism)", len(id_star_gas_as[CGM][reheated_nsf][select_wind])
            if write_SNe_energy_fraction:
                print "rate of those that were directly heated", len(mass_star_gas_as[CGM][reheated_nsf][select_wind&dir_heated_star_gas_as[CGM][reheated_nsf]])

    # If we are not doing the full FoF calculation for this satellite subhalo, assume that particles lost from the satellite (but which are still bound to a FoF group) should be added to the wind
    # Note we don't know if they have been removed by feedback (as opposed to stripping or any other mechanisms)
    if not full_calculation and do_ns_calculation:
        if len(id_gas_progen[ISM_progen|SF_progen]) > 0 and len(id_star_gas) > 0 and len(id_gas_progen[unbound_ts_ns_progen]) > 0:
            ptr = ms.match(id_gas_progen[ISM_progen|SF_progen], id_star_gas)
            stripped = ptr < 0

            # Check they are still bound to a FoF group - otherwise these particles would be added to the wind twice later on
            ptr = ms.match(id_gas_progen[ISM_progen|SF_progen], id_gas_progen[unbound_ts_ns_progen],arr2_sorted=True)
            bound = ptr < 0

            stripped = stripped & bound

            wind_ts["id"] = np.append(np.array(wind_ts["id"]), id_gas_progen[ISM_progen|SF_progen][stripped])
            wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), mass_gas_progen[ISM_progen|SF_progen][stripped])
            wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), metallicity_gas_progen[ISM_progen|SF_progen][stripped])
            wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(id_gas_progen[ISM_progen|SF_progen][stripped]))
            wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(id_gas_progen[ISM_progen|SF_progen][stripped]))
            wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), SF_progen[ISM_progen|SF_progen][stripped]) # Approximate...
            wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(id_gas_progen[ISM_progen|SF_progen][stripped]))
            if extra_accretion_measurements:
                wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen[ISM_progen|SF_progen][stripped])+halo_mchalo)
                if not bound_only_mode:
                    wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(mass_gas_progen[ISM_progen|SF_progen][stripped])+np.nan)

    ###### Identify newly formed star particles that have come from the star-forming reservoir (or nsf ism)
    if len(id_star) > 0 and len(id_gas_progen[SF_progen|ISM_progen]) >0:
        ptr, time_ptr_actual = ms.match(id_star, id_gas_progen[SF_progen|ISM_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        new_stars = ok_match
        halo_mass_dict["n_new_stars"] += len(mass_star[new_stars])
        halo_mass_dict["mass_new_stars"] += np.sum(mass_star[new_stars])
        halo_mass_dict["mZ_new_stars"] += np.sum((mass_star*metallicity_star)[new_stars])
        halo_mass_dict["mass_new_stars_init"] += np.sum(mass_star_init[new_stars])
        halo_mass_dict["mZ_new_stars_init"] += np.sum((mass_star_init*metallicity_star)[new_stars])
        halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[new_stars])

        if full_calculation:

            # Find the origin of these particles
            # Note that a known issue is that if the main progenitor is a satellite galaxy, we will not have the origin of its ISM particles
            # Just don't count these missed particles to the SFR when split into first-infall,rec,transfer,merge
            if len(id_star[new_stars]) > 0 and len(ism_ts["id"]) > 0:
                ptr_orig = ms.match(id_star[new_stars], ism_ts["id"])
                ok_match_orig = ptr_orig >= 0

                temp = ok_match_orig & (ism_ts["cool_origin"][ptr_orig] == 0)
                halo_mass_dict["mass_new_stars_init_prcooled"] += np.sum(mass_star_init[new_stars][temp])
                temp = ok_match_orig & ((ism_ts["cool_origin"][ptr_orig] == 1) | (ism_ts["cool_origin"][ptr_orig] == 4))
                halo_mass_dict["mass_new_stars_init_recooled"] += np.sum(mass_star_init[new_stars][temp])
                temp = ok_match_orig & (ism_ts["cool_origin"][ptr_orig] == 4)
                halo_mass_dict["mass_new_stars_init_recooled_from_mp"] += np.sum(mass_star_init[new_stars][temp])
                temp = ok_match_orig & (ism_ts["cool_origin"][ptr_orig] == 2)
                halo_mass_dict["mass_new_stars_init_galtran"] += np.sum(mass_star_init[new_stars][temp])
                temp = ok_match_orig & (ism_ts["cool_origin"][ptr_orig] == 3)
                halo_mass_dict["mass_new_stars_init_merge"] += np.sum(mass_star_init[new_stars][temp])

            ##### For particles that came from NSF ISM, compute their contribution to the cooling rates (for ISM == SF only definition)
            cooled_SF = ok_match & ( SF_progen[SF_progen|ISM_progen][ptr] ==False )

            from_SF = np.zeros_like(id_star[cooled_SF])
            if len(id_star[cooled_SF]) > 0 and len(nsfism_ts["id"])>0:
                ptr, time_ptr_actual = ms.match(id_star[cooled_SF], nsfism_ts["id"],time_taken=time_ptr_actual)

                from_SF[ptr>=0] = nsfism_ts["from_SF"][ptr][ptr>=0]

                recooled_SF = (ptr >= 0) & (nsfism_ts["from_wind"][ptr]==1)
                prcooled_SF = (ptr < 0) | ((nsfism_ts["from_SF"][ptr]==0)&(nsfism_ts["from_wind"][ptr]==0))
            else:
                recooled_SF = np.zeros_like(id_star[cooled_SF])< 0
                prcooled_SF = np.zeros_like(id_star[cooled_SF]) == 0
                ptr = []


            galactic_transfer = (in_master_wind_star[cooled_SF]) & (recooled_SF==False)

            from_SF[galactic_transfer] = from_SF_master_wind_star[cooled_SF][galactic_transfer]
            prcooled_SF[galactic_transfer] = False

            halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[cooled_SF][prcooled_SF])
            halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[cooled_SF][prcooled_SF])
            halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[cooled_SF][recooled_SF&(from_SF==0)])
            halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[cooled_SF][recooled_SF&(from_SF==0)])

            halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[cooled_SF][galactic_transfer&(from_SF==0)])
            halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[cooled_SF][galactic_transfer&(from_SF==0)])

            halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_star_init[cooled_SF][galactic_transfer&(from_SF==1)])
            halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_star*mass_star_init)[cooled_SF][galactic_transfer&(from_SF==1)])

            for fVmax_cut in fVmax_cuts:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                if len(ptr) > 0:
                    recooled_SF_i = recooled_SF | ((nsfism_ts["vpmax"][ptr] > fVmax_cut) & (ptr>=0))
                else:
                    recooled_SF_i = recooled_SF

                halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_star_init[cooled_SF][recooled_SF_i&(from_SF==1)])
                halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_star*mass_star_init)[cooled_SF][recooled_SF_i&(from_SF==1)])
    
    ###### Identify newly formed star forming star particles that came directly from the non-star forming reservoir (later edit: and not from nsf ism)
    # later later edit - or from the ISM/CGM of other subhalos in the FOF group
    if full_calculation and len(id_star) > 0 and len(id_star_gas_all_progen[CGM_progen]) >0:
        ptr, time_ptr_actual = ms.match(id_star, id_star_gas_all_progen[CGM_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        new_stars = ok_match

        halo_mass_dict["mass_new_stars"] += np.sum(mass_star[new_stars])
        halo_mass_dict["mZ_new_stars"] += np.sum((metallicity_star*mass_star)[new_stars])
        halo_mass_dict["mass_new_stars_init"] += np.sum(mass_star_init[new_stars])
        halo_mass_dict["n_new_stars"] += len(mass_star[new_stars])
        halo_mass_dict["mZ_new_stars_init"] += np.sum((mass_star_init*metallicity_star)[new_stars])
        halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[new_stars])

        cooled = ok_match

        if len(ism_reheat_ts["id"]) > 0:
            ptr, time_ptr_actual = ms.match(id_star, ism_reheat_ts["id"],time_taken=time_ptr_actual)

            # Ism reheat can still contribute to cooling _SF cooling terms
            cooled_ir = cooled & (ptr>= 0)
            cooled_ir_SF = cooled & (ptr>=0)

            prcooled_ir_SF = cooled_ir_SF & (ism_reheat_ts["from_SF"][ptr] == 0)

            galtran_ir_SF = cooled_ir_SF & (ism_reheat_ts["from_SF"][ptr]==0) & (from_SF_master_wind_star == 1)
            prcooled_ir_SF[galtran_ir_SF] = False

            halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[prcooled_ir_SF])
            halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[prcooled_ir_SF])
            halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_star_init[galtran_ir_SF])
            halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_star*mass_star_init)[galtran_ir_SF])

            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                recooled_ir = cooled_ir & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut)
                recooled_ir_from_mp = cooled_ir & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut) & (ism_reheat_ts["from_mp"][ptr]==1)
                recooled_ir_SF = cooled_ir_SF & (ism_reheat_ts["from_SF"][ptr] == 1) & ((ism_reheat_ts["from_wind"][ptr] == 1) | (ism_reheat_ts["vpmax"][ptr] > fVmax_cut))

                halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_star_init[recooled_ir])
                halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_star*mass_star_init)[recooled_ir])

                halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_star_init[recooled_ir_from_mp])
                halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_star*mass_star_init)[recooled_ir_from_mp])

                halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_star_init[recooled_ir_SF])
                halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_star*mass_star_init)[recooled_ir_SF])

                if i_cut == fVmax_info[1]:
                    # Compute distribution of return times for returning particles
                    nt_wind_i, junk = np.histogram(np.array(ism_reheat_ts["time"][ptr][recooled_ir_SF]),bins=tret_bins_ts,weights=np.array(ism_reheat_ts["mass"])[ptr][recooled_ir_SF])
                    halo_t_dict["mass_t_wind_ret"] += nt_wind_i

                    if extra_accretion_measurements and not bound_only_mode:
                        r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                        ok_nan = (ism_reheat_ts["rmax"][ptr][recooled_ir_SF]==False) & (ism_reheat_ts["from_mp"][ptr][recooled_ir_SF]==1)
                        nr_wi_i, junk = np.histogram(np.array(ism_reheat_ts["rmax"])[ptr][recooled_ir_SF][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(ism_reheat_ts["mass"])[ptr][recooled_ir_SF][ok_nan])
                        halo_t_dict["n_r_wind_ret"] += nr_wi_i

                    halo_mass_dict["mass_new_stars_init_recooled"] += np.sum(mass_star_init[recooled_ir])
                    halo_mass_dict["mZ_new_stars_init_recooled"] += np.sum((metallicity_star*mass_star_init)[recooled_ir])

                    halo_mass_dict["mass_new_stars_init_recooled_from_mp"] += np.sum(mass_star_init[recooled_ir_from_mp])

            # Exclude ism reheat particles from the (remaining) cooling terms
            cooled = cooled & (ptr < 0)
            halo_outflow_dict["mass_recooled_ireheat"] += np.sum(ism_reheat_ts["mass"][ptr][ptr>=0])

        from_SF = np.zeros_like(id_star[cooled])
        if len(id_star[cooled]) > 0 and len(wind_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(id_star[cooled], wind_ts["id"],time_taken=time_ptr_actual)
            recooled = ptr >= 0
            recooled_from_mp = recooled & (wind_ts["from_mp"][ptr]==1)
            from_SF[recooled] = wind_ts["from_SF"][ptr][recooled]
            prcooled = ptr < 0
        else:
            recooled = np.zeros_like(id_star[cooled])< 0
            prcooled = np.zeros_like(id_star[cooled]) == 0
            recooled_from_mp = np.copy(recooled)

        # Note to future self, you could in principle have material that goes ireheat of a satellite that would not be counted as galtran here.
        # For now I'm going to assume this is negligible - but in principle it might not be....
        # You might want to redo this in the future

        galactic_transfer = (in_master_wind_star[cooled]) & (recooled==False)
        from_SF[galactic_transfer] = from_SF_master_wind_star[cooled][galactic_transfer]
        prcooled[galactic_transfer] = False

        if extra_accretion_measurements:
            counts, junk = np.histogram(np.log10(mchalo_ej_master_wind_star[cooled][galactic_transfer]),bins=mej_bins, weights=mass_star_init[cooled][galactic_transfer])
            halo_t_dict["mass_galtran_mchalo_ratio"] += counts

        halo_outflow_dict["mass_prcooled"] += np.sum(mass_star_init[cooled][prcooled])
        halo_outflow_dict["mass_galtran"] += np.sum(mass_star_init[cooled][galactic_transfer])
        halo_outflow_dict["mZ_prcooled"] += np.sum((metallicity_star*mass_star_init)[cooled][prcooled])
        halo_outflow_dict["mZ_galtran"] += np.sum((metallicity_star*mass_star_init)[cooled][galactic_transfer])

        halo_mass_dict["mass_new_stars_init_prcooled"] += np.sum(mass_star_init[cooled][prcooled])
        halo_mass_dict["mZ_new_stars_init_prcooled"] += np.sum((metallicity_star*mass_star_init)[cooled][prcooled])
        halo_mass_dict["mass_new_stars_init_galtran"] += np.sum(mass_star_init[cooled][galactic_transfer])
        halo_mass_dict["mZ_new_stars_init_galtran"] += np.sum((metallicity_star*mass_star_init)[cooled][galactic_transfer])
        halo_mass_dict["mass_new_stars_init_recooled"] += np.sum(mass_star_init[cooled][recooled])
        halo_mass_dict["mZ_new_stars_init_recooled"] += np.sum((metallicity_star*mass_star_init)[cooled][recooled])

        halo_mass_dict["mass_new_stars_init_recooled_from_mp"] += np.sum(mass_star_init[cooled][recooled_from_mp])

        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[cooled][prcooled])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[cooled][prcooled])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[cooled][recooled&(from_SF==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[cooled][recooled&(from_SF==0)])
        halo_outflow_dict["mass_prcooled_SF"] += np.sum(mass_star_init[cooled][galactic_transfer&(from_SF==0)])
        halo_outflow_dict["mZ_prcooled_SF"] += np.sum((metallicity_star*mass_star_init)[cooled][galactic_transfer&(from_SF==0)])

        halo_outflow_dict["mass_galtran_from_SF"] += np.sum(mass_star_init[cooled][galactic_transfer&(from_SF==1)])
        halo_outflow_dict["mZ_galtran_from_SF"] += np.sum((metallicity_star*mass_star_init)[cooled][galactic_transfer&(from_SF==1)])

        for fVmax_cut in fVmax_cuts:
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"            

            halo_outflow_dict["mass_recooled"+fV_str] += np.sum(mass_star_init[cooled][recooled])
            halo_outflow_dict["mZ_recooled"+fV_str] += np.sum((metallicity_star*mass_star_init)[cooled][recooled])

            halo_outflow_dict["mass_recooled_from_mp"+fV_str] += np.sum(mass_star_init[cooled][recooled_from_mp])
            halo_outflow_dict["mZ_recooled_from_mp"+fV_str] += np.sum((metallicity_star*mass_star_init)[cooled][recooled_from_mp])

            halo_outflow_dict["mass_recooled_from_SF"+fV_str] += np.sum(mass_star_init[cooled][recooled&(from_SF==1)])
            halo_outflow_dict["mZ_recooled_from_SF"+fV_str] += np.sum((metallicity_star*mass_star_init)[cooled][recooled&(from_SF==1)])

        # This should really be for the fiducial velocity cut...
        if len(wind_ts["id"]) > 0 and len(id_star[cooled][recooled])>0:
            ptr, time_ptr_actual = ms.match(np.array(wind_ts["id"]), id_star[cooled][recooled],time_taken=time_ptr_actual,arr2_sorted=True)
            # Compute distribution of return times for returning particles
            nt_wi_i, junk = np.histogram(np.array(wind_ts["t"])[ptr>=0],bins=tret_bins_ts, weights=np.array(wind_ts["mass"])[ptr>=0])
            halo_t_dict["mass_t_wind_ret"] += nt_wi_i

            if extra_accretion_measurements and not bound_only_mode:
                r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
                ok_nan = (np.isnan(wind_ts["rmax"][ptr>=0])==False) & (wind_ts["from_mp"][ptr>=0]==1)
                nr_wi_i, junk = np.histogram(np.array(wind_ts["rmax"])[ptr>=0][ok_nan]/r200,bins=bins_r_track_wi,weights=np.array(wind_ts["mass"])[ptr>=0][ok_nan])
                halo_t_dict["n_r_wind_ret"] += nr_wi_i

    ##### Identify particles which have left the reheated ISM list and rejoined the ISM or the SF gas, or become a star
    # Note it is very important that this block appear below the blocks containing cooling rate calculations (accretion of gas onto the ISM)
    if len(np.append(id_gas[SF|ISM],id_star)) > 0 and len(ism_reheat_ts["id"]) > 0:
        ptr, time_ptr_actual = ms.match(np.append(id_gas[SF|ISM],id_star), ism_reheat_ts["id"],time_taken=time_ptr_actual)
        ok_match = ptr >= 0

        # Remove returned particles from reheated ism lists (note in principle this introduces a bit of order dependence within the loop but the sorting means it shouldn't be significant)
        select_sub = np.where(subhalo_props["group_number"] == halo_group_number)[0]
        temp = 0
        if len(ism_reheat_ts["id"]) > 0 and len(np.append(id_gas[SF|ISM],id_star)[ok_match])>0:
            ptr, time_ptr_actual = ms.match(np.array(ism_reheat_ts["id"]), np.append(id_gas[SF|ISM],id_star)[ok_match],time_taken=time_ptr_actual)
            for name in ism_reheat_ts:
                ism_reheat_ts[name] = np.array(ism_reheat_ts[name])[ptr<0]

            mpf.Check_Dict(ism_reheat_ts, "jimbasdass")

    ##### Identify particles which have left the reheated ISM list and joined the wind (those that are still bound on this step)
    if do_ns_calculation and len(id_star_gas_as[CGM]) > 0 and len(ism_reheat_ts["id"]) > 0:
        ptr, time_ptr_actual = ms.match(ism_reheat_ts["id"],id_star_gas_as[CGM],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        # Decide which reheated particles join the wind
        t_list = [t_ps, t_ts, t_ns]
            
        # Find positions of the reheated particles on the previous step
        coordinates_gas_ism_reheat_ps = np.zeros((len(ism_reheat_ts["id"][ok_match]),3))+1e9 # Assume those that can't be found are at infinity
        if full_calculation:
            if len(id_star_gas_all_progen)>0 and len(ism_reheat_ts["id"][ok_match])>0:
                ptr_ps, time_ptr_actual = ms.match(ism_reheat_ts["id"][ok_match],id_star_gas_all_progen,time_taken=time_ptr_actual,arr2_sorted=True)
                ok_match_ps = ptr_ps >= 0
                coordinates_gas_ism_reheat_ps[ok_match_ps] = coordinates_star_gas_all_progen[ptr_ps][ok_match_ps]
        else:
            if len(id_gas_progen)>0 and len(ism_reheat_ts["id"][ok_match])>0:
                ptr_ps, time_ptr_actual = ms.match(ism_reheat_ts["id"][ok_match],id_gas_progen,time_taken=time_ptr_actual,arr2_sorted=True)
                ok_match_ps = ptr_ps >= 0
                coordinates_gas_ism_reheat_ps[ok_match_ps] = coordinates_gas_progen[ptr_ps][ok_match_ps]

        data_gas_ps_in = [coordinates_gas_ism_reheat_ps]
        data_gas_ts_in = [ism_reheat_ts["id"][ok_match], coordinates_star_gas_as[CGM][ptr][ok_match], ism_reheat_ts["mass"][ok_match], cv_star_gas_as[CGM][ptr][ok_match]]
        data_gas_ns_in = [id_gas_fof_ns, SF_fof_ns, ISM_fof_ns, coordinates_gas_fof_ns]
        halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]

        select_wind, fVmax = mpf.Reheated_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, id_star_fof_ns, halo_props, fVmax_cuts)

        # Find the subset of the particles joining the wind that came from the SF ISM
        from_SF = np.zeros_like(ism_reheat_ts["id"][ok_match])
        from_SF[ism_reheat_ts["from_SF"][ok_match] == 1] = 1

        # Find the subset of the particles joining the wind came from the main progenitor branch
        from_mp = np.zeros_like(ism_reheat_ts["id"][ok_match])
        from_mp[ism_reheat_ts["from_mp"][ok_match] == 1] = 1

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            select_wind_i = (fVmax > fVmax_cut)
 
            if len(fVmax_cuts) > 1:
                # This step is how we avoid double counting outflow events if we use multiple velocity cuts (but only one set of tracked reheat lists)
                select_wind_i = select_wind_i & (ism_reheat_ts["vpmax"][ok_match] < fVmax_cut) # Check particle hasn't already joined the wind for this v cut

            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
            halo_outflow_dict["mass_ism_reheated_join_wind"+fV_str] += np.sum(ism_reheat_ts["mass"][ok_match][select_wind_i])
            halo_outflow_dict["n_ism_reheated_join_wind"+fV_str] += len(ism_reheat_ts["mass"][ok_match][select_wind_i])
            halo_outflow_dict["mZ_ism_reheated_join_wind"+fV_str] += np.sum((ism_reheat_ts["metallicity"]*ism_reheat_ts["mass"])[ok_match][select_wind_i])

            halo_outflow_dict["mass_ism_reheated_join_wind_from_SF"+fV_str] += np.sum(ism_reheat_ts["mass"][ok_match][select_wind_i&(from_SF==1)])
            halo_outflow_dict["mZ_ism_reheated_join_wind_from_SF"+fV_str] += np.sum((ism_reheat_ts["metallicity"]*ism_reheat_ts["mass"])[ok_match][select_wind_i&(from_SF==1)])
            
            if write_SNe_energy_fraction:
                halo_emass_dict["mass_join_ism_wind_dir_heat"+fV_str] += np.sum(ism_reheat_ts["mass"][ok_match][select_wind_i&dir_heated_star_gas_as[CGM][ptr][ok_match]])
                halo_emass_dict["mO_join_ism_wind"+fV_str] += np.sum((ism_reheat_ts["oxygen"]*ism_reheat_ts["mass"])[ok_match][select_wind_i])
                halo_emass_dict["mFe_join_ism_wind"+fV_str] += np.sum((ism_reheat_ts["iron"]*ism_reheat_ts["mass"])[ok_match][select_wind_i])

            # For these measurements, adopt the desired "fiducial" cut
            if i_cut == fVmax_info[1]:
                wind_cumul_ts["mass_ism_wind"] += np.sum(ism_reheat_ts["mass"][ok_match][select_wind_i])
                wind_cumul_ts["n_ism_wind"] += len(ism_reheat_ts["mass"][ok_match][select_wind_i])

        # Update the maximum past fraction of vmax for the next snapshot
        if len(fVmax_cuts) > 1:
            ism_reheat_ts["vpmax"][ok_match] = np.max((ism_reheat_ts["vpmax"][ok_match],fVmax),axis=0)

        # Add ejected particles (including their mass) to the ejected particle lists
        wind_ts["id"] = np.append(np.array(wind_ts["id"]), ism_reheat_ts["id"][ok_match][select_wind])
        wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), ism_reheat_ts["mass"][ok_match][select_wind])
        wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), ism_reheat_ts["metallicity"][ok_match][select_wind])
        wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(ism_reheat_ts["id"][ok_match][select_wind]))
        wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), from_mp[select_wind])
        wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), from_SF[select_wind])
        wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(ism_reheat_ts["id"][ok_match][select_wind]))
        if extra_accretion_measurements:
            wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(ism_reheat_ts["mass"][ok_match][select_wind])+halo_mchalo)
            if not bound_only_mode:
                wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(ism_reheat_ts["mass"][ok_match][select_wind])+np.nan)
 
        mpf.Check_Dict(wind_ts, "aufhg")

        if debug and i_halo == ih_choose:
            print "rate of ISM reheated list particles joining the ISM wind", len(ism_reheat_ts["id"][ok_match][select_wind])
            if write_SNe_energy_fraction:
                 print "rate of those that were directly heated", len(ism_reheat_ts["mass"][ok_match][select_wind&dir_heated_ism_reheat_ts_temp])

        # Remove these particles from reheated ism lists
        id_temp = np.copy(ism_reheat_ts["id"][ok_match][select_wind])
        select_sub = np.where(subhalo_props["group_number"] == halo_group_number)[0]

        if len(ism_reheat_ts["id"]) > 0 and len(id_temp)>0:
            ptr, time_ptr_actual = ms.match(np.array(ism_reheat_ts["id"]),id_temp,time_taken=time_ptr_actual)
            for name in ism_reheat_ts:
                ism_reheat_ts[name] = np.array(ism_reheat_ts[name])[ptr<0]
            mpf.Check_Dict(ism_reheat_ts, "ahsdk2jyts")

    ######## For optimise_sats mode, assume particles that leave the satellite (but are still in a FoF group) join the wind
    if not full_calculation and do_ns_calculation:
        if len(ism_reheat_ts["id"]) > 0 and len(id_star_gas) > 0 and len(id_gas_progen[unbound_ts_ns_progen]) > 0:
            ptr = ms.match(ism_reheat_ts["id"], id_star_gas)
            stripped = ptr < 0

            # Check they are still bound to a FoF group - otherwise these particles would be added to the wind twice later on
            ptr = ms.match(ism_reheat_ts["id"], id_gas_progen[unbound_ts_ns_progen],arr2_sorted=True)
            bound = ptr < 0

            stripped = stripped & bound

            wind_ts["id"] = np.append(np.array(wind_ts["id"]), ism_reheat_ts["id"][stripped])
            wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), ism_reheat_ts["mass"][stripped])
            wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), ism_reheat_ts["metallicity"][stripped])
            wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(ism_reheat_ts["id"][stripped]))
            wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(ism_reheat_ts["id"][stripped]))
            wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), ism_reheat_ts["from_SF"][stripped]) # Approximate...
            wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(ism_reheat_ts["id"][stripped]))
            if extra_accretion_measurements:
                wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(ism_reheat_ts["mass"][stripped])+halo_mchalo)
                if not bound_only_mode:
                    wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(ism_reheat_ts["mass"][stripped])+np.nan)

            # Remove these particles from reheated ism lists
            id_temp = np.copy(ism_reheat_ts["id"][stripped])
            select_sub = np.where(subhalo_props["group_number"] == halo_group_number)[0]

            if len(ism_reheat_ts["id"]) > 0 and len(id_temp)>0:
                ptr, time_ptr_actual = ms.match(np.array(ism_reheat_ts["id"]),id_temp,time_taken=time_ptr_actual)
                for name in ism_reheat_ts:
                    ism_reheat_ts[name] = np.array(ism_reheat_ts[name])[ptr<0]
                mpf.Check_Dict(ism_reheat_ts, "ahsdasdasdafjdhgk2jyts")
            

    ##### Identify particles which have left the reheated halo list and rejoined the FoF group
    if len(id_star_gas_as) > 0 and len(halo_reheat_ts["id"]) > 0:
        ptr, time_ptr_actual = ms.match(id_star_gas_as, halo_reheat_ts["id"],time_taken=time_ptr_actual)
        ok_match = ptr >= 0

        # Remove returned particles from reheated list
        if len(halo_reheat_ts["id"]) > 0 and len(id_star_gas_as[ok_match])>0:
            ptr, time_ptr_actual = ms.match(np.array(halo_reheat_ts["id"]), id_star_gas_as[ok_match],time_taken=time_ptr_actual,arr2_sorted=True)
            for name in halo_reheat_ts:
                halo_reheat_ts[name] = np.array(halo_reheat_ts[name])[ptr<0]
            mpf.Check_Dict(halo_reheat_ts,"lasdhg28")
            
    ##### Identify unbound particles which have left the reheated halo list and joined the halo wind
    if do_ns_calculation and len(id_star_gas_as) > 0 and len(id_gas_halo_reheat_ts_fixed) > 0:
        ptr, time_ptr_actual = ms.match(id_gas_halo_reheat_ts_fixed,id_star_gas_as,time_taken=time_ptr_actual,arr2_sorted=True) # This is just to check the particles didn't sneak back into the halo somehow
        ok_match = ptr < 0
        # Note that for optimise_sat mode, particles that are in other subhaloes of the same FOF group will be incorrectly flagged as outside the FOF group here, but we can live with this I think

        # Decide which reheated particles join the wind
        t_list = [t_ps, t_ts, t_ns]

        # Particles with NaN coordinates are bound to a halo (but not subfind unbound) - which must another halo otherwise the particles wouldn't have passed the ptr check above
        # Particles with _ts NaN only are candidate flythrough (along with the subfind unbound parts - who have their actual coordinates)
        flythrough_cand = np.isnan(coordinates_gas_halo_reheat_ts_fixed[ok_match]) & (np.isnan(coordinates_gas_halo_reheat_ps_fixed[ok_match])==False) & (np.isnan(coordinates_gas_halo_reheat_ns_fixed[ok_match])==False)
        coordinates_gas_halo_reheat_ts_fixed[np.where(ok_match)[0]][flythrough_cand] = 0.5 * (coordinates_gas_halo_reheat_ps_fixed[ok_match][flythrough_cand] +coordinates_gas_halo_reheat_ns_fixed[ok_match][flythrough_cand])

        ok_match2 = (np.isnan(coordinates_gas_halo_reheat_ps_fixed[ok_match][:,0])==False) & (np.isnan(coordinates_gas_halo_reheat_ts_fixed[ok_match][:,0])==False) & (np.isnan(coordinates_gas_halo_reheat_ns_fixed[ok_match][:,0])==False)
        # Particles that fail the above check are most likely being carried away by a splashback halo, and so are not eligible to join the wind

        data_gas_ps_in = [coordinates_gas_halo_reheat_ps_fixed[ok_match][ok_match2]]
        data_gas_ts_in = [coordinates_gas_halo_reheat_ts_fixed[ok_match][ok_match2], id_gas_halo_reheat_ts_fixed[ok_match][ok_match2]]
        data_gas_ns_in = [coordinates_gas_halo_reheat_ns_fixed[ok_match][ok_match2]]

        halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]

        if len(ok_match2[ok_match2])>0:
        
            # Call different _Prelocated function because we have already found the positions of the particles on the current/next timestep
            select_wind, fVmax = mpf.Halo_Wind_Selection_Prelocated(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, halo_props, fVmax_cuts)

            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                select_wind_i = (fVmax > fVmax_cut)
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                if len(fVmax_cuts) > 1:
                    # This step is how we avoid double counting outflow events if we use multiple velocity cuts (but only one set of tracked reheat lists)
                    prev_joined = halo_reheat_ts["vpmax"] > fVmax_cut

                    if len(halo_reheat_ts["id"][prev_joined])>0 and len(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2])>0:
                        
                        ptr, time_ptr_actual = ms.match(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2],halo_reheat_ts["id"][prev_joined],time_taken=time_ptr_actual)
                        ok_match3 = ptr>=0

                        select_wind_i[ok_match3] = False

                # Note this ptr match is necessary because id_gas_halo_list_ts has particles added/deleted as we go through the loop
                # But the coordinates of the halo reheat particles are only computed at the start of the timestep (and so we have to allign the indicies here)
                if len(halo_reheat_ts["id"])>0 and len(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind_i])>0:
                    ptr, time_ptr_actual = ms.match(halo_reheat_ts["id"], id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind_i],time_taken=time_ptr_actual,arr2_sorted=True)
                    ok_match3 = ptr>=0

                    halo_outflow_dict["mass_halo_reheated_join_wind"+fV_str] += np.sum(halo_reheat_ts["mass"][ok_match3])
                    halo_outflow_dict["n_halo_reheated_join_wind"+fV_str] += len(halo_reheat_ts["mass"][ok_match3])
                    halo_outflow_dict["mZ_halo_reheated_join_wind"+fV_str] += np.sum((halo_reheat_ts["metallicity"]*halo_reheat_ts["mass"])[ok_match3])

                    if write_SNe_energy_fraction:
                        halo_emass_dict["mass_join_halo_wind_dir_heat"+fV_str] += np.sum(halo_reheat_ts["mass"][ok_match3 & dir_heated_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind_i][ptr] ])
                        halo_emass_dict["mO_join_halo_wind"+fV_str] += np.sum((halo_reheat_ts["oxygen"]*halo_reheat_ts["mass"])[ok_match3])
                        halo_emass_dict["mFe_join_halo_wind"+fV_str] += np.sum((halo_reheat_ts["iron"]*halo_reheat_ts["mass"])[ok_match3])

                # Count particles that join the halo wind which were in the ISM previously
                # For these measurements, adopt the desired "fiducial" cut
                if i_cut == fVmax_info[1]:

                    if len(wind_ts["id"])>0 and len(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind_i])>0:
                        ptr4, time_ptr_actual = ms.match(wind_ts["id"], id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind_i],time_taken=time_ptr_actual,arr2_sorted=True)
                        ok_match4 = ptr4>=0
                        first_time = ok_match4 & (wind_ts["left_halo"]==0)

                        halo_outflow_dict["n_join_halo_wind_from_ism"] += len(wind_ts["mass"][ok_match4])
                        halo_outflow_dict["mass_join_halo_wind_from_ism"] += np.sum(wind_ts["mass"][ok_match4])
                        halo_outflow_dict["mZ_join_halo_wind_from_ism"] += np.sum((wind_ts["metallicity"]*wind_ts["mass"])[ok_match4])
                        halo_outflow_dict["mass_join_halo_wind_from_ism_first_time"] += np.sum(wind_ts["mass"][first_time])

                        wind_cumul_ts["mass_ism_to_halo_wind_first_time"] += np.sum(wind_ts["mass"][first_time])
                        wind_cumul_ts["n_ism_to_halo_wind_first_time"] += len(wind_ts["mass"][first_time])

                        wind_ts["left_halo"][first_time] == 1

            # Add ejected particles (including their mass) to the ejected particle lists
            if len(halo_reheat_ts["id"])>0 and len(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind])>0:
                ptr, time_ptr_actual = ms.match(halo_reheat_ts["id"], id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind],time_taken=time_ptr_actual,arr2_sorted=True)
                ok_match3 = ptr>=0

                ejecta_ts["id"] = np.append(np.array(ejecta_ts["id"]), halo_reheat_ts["id"][ok_match3])
                ejecta_ts["mass"] = np.append(np.array(ejecta_ts["mass"]), halo_reheat_ts["mass"][ok_match3])
                ejecta_ts["metallicity"] = np.append(np.array(ejecta_ts["metallicity"]), halo_reheat_ts["metallicity"][ok_match3])
                ejecta_ts["t"] = np.append(np.array(ejecta_ts["t"]), np.zeros_like(halo_reheat_ts["id"][ok_match3]))
                ejecta_ts["from_mp"] = np.append(np.array(ejecta_ts["from_mp"]), halo_reheat_ts["from_mp"][ok_match3])
                if extra_accretion_measurements:
                    ejecta_ts["mchalo_ej"] = np.append(np.array(ejecta_ts["mchalo_ej"]), np.zeros_like(halo_reheat_ts["mass"][ok_match3])+halo_mchalo)
                    if not bound_only_mode:
                        ejecta_ts["rmax"] = np.append(np.array(ejecta_ts["rmax"]), np.zeros_like(halo_reheat_ts["mass"][ok_match3])+np.nan)

                mpf.Check_Dict(ejecta_ts, "nsdhf")

                if debug and i_halo == ih_choose:
                    print "rate of halo reheated list particles joining the wind", len(halo_reheat_ts["id"][ok_match3]), " , out of a possible ",len(halo_reheat_ts["id"])
                    if write_SNe_energy_fraction:
                        print "rate of those that were directly heated", len(halo_reheat_ts["mass"][ok_match3 & dir_heated_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind_i][ptr] ])

            # Update the maximum past fraction of vmax for the next snapshot
            if len(fVmax_cuts)>1:
                if len(halo_reheat_ts["id"])>0 and len(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2])>0:
                    ptr, time_ptr_actual = ms.match(halo_reheat_ts["id"], id_gas_halo_reheat_ts_fixed[ok_match][ok_match2],time_taken=time_ptr_actual,arr2_sorted=True)
                    ok_match3 = ptr>=0
                halo_reheat_ts["vpmax"][ok_match3] = np.max((halo_reheat_ts["vpmax"][ok_match3],fVmax[ptr][ok_match3]),axis=0)
            
            # Remove halo wind particles from reheated halo lists
            id_temp = np.copy(id_gas_halo_reheat_ts_fixed[ok_match][ok_match2][select_wind])
            if len(halo_reheat_ts["id"]) > 0 and len(id_temp)>0:
                ptr, time_ptr_actual = ms.match(np.array(halo_reheat_ts["id"]),id_temp,time_taken=time_ptr_actual,arr2_sorted=True)
                for name in halo_reheat_ts:
                    halo_reheat_ts[name] = np.array(halo_reheat_ts[name])[ptr<0]
                mpf.Check_Dict(halo_reheat_ts, "yasdj")

    ###### Identify gas progenitor particles which have been ejected entirely from the halo
    if do_ns_calculation and full_calculation and len(id_gas_progen) > 0 and len(id_star_gas_as) > 0:
        ptr_eject_gas, time_ptr_actual = ms.match(id_gas_progen, id_star_gas_as,time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr_eject_gas >= 0

        ejected = ok_match == False

    # Without FoF information for this subhalo, we can approximate this by asking which particles are outside ALL FoF groups on _ts/_ns
    elif do_ns_calculation and not full_calculation and len(id_gas_progen[unbound_ts_ns_progen]) > 0:
        ejected = unbound_ts_ns_progen
    else:
        ejected = np.zeros_like(id_gas_progen)<0
                
    if do_ns_calculation and len(ejected[ejected]) > 0:
        halo_outflow_dict["mass_halo_reheated"] += np.sum(mass_gas_progen[ejected])
        halo_outflow_dict["n_halo_reheated"] += len(mass_gas_progen[ejected])

        halo_outflow_dict["mass_sf_ism_reheated"] += np.sum(mass_gas_progen[SF_progen&ejected])
        halo_outflow_dict["n_sf_ism_reheated"] += len(mass_gas_progen[SF_progen&ejected])

        halo_outflow_dict["mass_nsf_ism_reheated"] += np.sum(mass_gas_progen[(SF_progen==False)&(ISM_progen)&ejected])
        halo_outflow_dict["n_nsf_ism_reheated"] += len(mass_gas_progen[(SF_progen==False)&(ISM_progen)&ejected])

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_sf_ism_join_wind"+fV_str] += np.sum(mass_gas_progen[SF_progen&ejected])
            halo_outflow_dict["n_sf_ism_join_wind"+fV_str] += len(mass_gas_progen[SF_progen&ejected])
            halo_outflow_dict["mZ_sf_ism_join_wind"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[SF_progen&ejected])

            halo_outflow_dict["mass_nsf_ism_join_wind"+fV_str] += np.sum(mass_gas_progen[(SF_progen==False)&(ISM_progen)&ejected])
            halo_outflow_dict["n_nsf_ism_join_wind"+fV_str] += len(mass_gas_progen[(SF_progen==False)&(ISM_progen)&ejected])
            halo_outflow_dict["mZ_nsf_ism_join_wind"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[(SF_progen==False)&(ISM_progen)&ejected])

            if write_SNe_energy_fraction:
                halo_emass_dict["mO_join_ism_wind"+fV_str] += np.sum((oxygen_gas_progen*mass_gas_progen)[ISM_progen&ejected])
                halo_emass_dict["mFe_join_ism_wind"+fV_str] += np.sum((iron_gas_progen*mass_gas_progen)[ISM_progen&ejected])

        wind_cumul_ts["mass_ism_wind"] += np.sum(mass_gas_progen[((SF_progen)|(ISM_progen))&ejected])
        wind_cumul_ts["n_ism_wind"] += len(mass_gas_progen[((SF_progen)|(ISM_progen))&ejected])

        t_list = [t_ps, t_ts, t_ns]
        data_gas_ps_in = [coordinates_gas_progen[ejected], id_gas_progen[ejected], mass_gas_progen[ejected]]
        data_gas_ts_in = [coordinates_gas_unbound_ts, id_gas_unbound_ts, group_number_gas_unbound_ts]
        data_gas_ns_in = [coordinates_part_unbound_ns, id_part_unbound_ns, group_number_part_unbound_ns]
        halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]

        if write_SNe_energy_fraction:
            data_gas_ts_in.append(dir_heated_unbound_ts)
            data_gas_ts_in.append(dir_heated_SNe_unbound_ts)
            select_wind, fVmax, dir_heated_unbound_ts_sorted, dir_heated_SNe_unbound_ts_sorted = mpf.Halo_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, halo_props, halo_group_number_ns,write_SNe_energy_fraction, fVmax_cuts)

            halo_emass_dict["mass_dir_heat_in_nism"] += np.sum(mass_gas_progen[ejected][(SF_progen[ejected]==False)&(ISM_progen[ejected]==False)&(dir_heated_progen[ejected]==False)&dir_heated_unbound_ts_sorted])
            halo_emass_dict["mZ_dir_heat_in_nism"] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][(SF_progen[ejected]==False)&(ISM_progen[ejected]==False)&(dir_heated_progen[ejected]==False)&dir_heated_unbound_ts_sorted])
            halo_emass_dict["mass_dir_heat_in_ism"] += np.sum(mass_gas_progen[ejected][(SF_progen[ejected]|ISM_progen[ejected])&(dir_heated_progen[ejected]==False)&dir_heated_unbound_ts_sorted])
            halo_emass_dict["mZ_dir_heat_in_ism"] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][(SF_progen[ejected]|ISM_progen[ejected])&(dir_heated_progen[ejected]==False)&dir_heated_unbound_ts_sorted])

            halo_emass_dict["mass_dir_heat_SNe_in_nism"] += np.sum(mass_gas_progen[ejected][(SF_progen[ejected]==False)&(ISM_progen[ejected]==False)&(dir_heated_progen_SNe[ejected]==False)&dir_heated_SNe_unbound_ts_sorted])
            halo_emass_dict["mZ_dir_heat_SNe_in_nism"] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][(SF_progen[ejected]==False)&(ISM_progen[ejected]==False)&(dir_heated_progen_SNe[ejected]==False)&dir_heated_SNe_unbound_ts_sorted])
            halo_emass_dict["mass_dir_heat_SNe_in_ism"] += np.sum(mass_gas_progen[ejected][(SF_progen[ejected]|ISM_progen[ejected])&(dir_heated_progen_SNe[ejected]==False)&dir_heated_SNe_unbound_ts_sorted])
            halo_emass_dict["mZ_dir_heat_SNe_in_ism"] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][(SF_progen[ejected]|ISM_progen[ejected])&(dir_heated_progen_SNe[ejected]==False)&dir_heated_SNe_unbound_ts_sorted])

            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                halo_emass_dict["mass_join_ism_wind_dir_heat"+fV_str] += np.sum(mass_gas_progen[ejected][(SF_progen[ejected]|ISM_progen[ejected])&dir_heated_unbound_ts_sorted])

        else:            
            select_wind, fVmax = mpf.Halo_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, halo_props, halo_group_number_ns,write_SNe_energy_fraction, fVmax_cuts)
                        
        if debug and i_halo == ih_choose:
            print "number of particles joining ISM wind by being ejected from FoF group", len(mass_gas_progen[((SF_progen|ISM_progen))&ejected])
            if write_SNe_energy_fraction:
                print "rate of those that were directly heated", len(mass_gas_progen[ejected][(SF_progen[ejected]|ISM_progen[ejected])&dir_heated_unbound_ts_sorted])
                print "number of particles being directly heated in halo/ISM", \
                    len(mass_gas_progen[ejected][(SF_progen[ejected]==False)&(ISM_progen[ejected]==False)&(dir_heated_progen[ejected]==False)&dir_heated_unbound_ts_sorted]),\
                    len(mass_gas_progen[ejected][(SF_progen[ejected]|ISM_progen[ejected])&(dir_heated_progen[ejected]==False)&dir_heated_unbound_ts_sorted])


        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            select_wind_i = (fVmax > fVmax_cut)
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_join_halo_wind"+fV_str] += np.sum(mass_gas_progen[ejected][select_wind_i])
            halo_outflow_dict["n_join_halo_wind"+fV_str] += len(mass_gas_progen[ejected][select_wind_i])
            halo_outflow_dict["mZ_join_halo_wind"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][select_wind_i])

            if write_SNe_energy_fraction:
                halo_emass_dict["mass_join_halo_wind_dir_heat"+fV_str] += np.sum(mass_gas_progen[ejected][dir_heated_unbound_ts_sorted&select_wind_i])
                halo_emass_dict["mO_join_halo_wind"+fV_str] += np.sum((oxygen_gas_progen*mass_gas_progen)[ejected][select_wind_i])
                halo_emass_dict["mFe_join_halo_wind"+fV_str] += np.sum((iron_gas_progen*mass_gas_progen)[ejected][select_wind_i])

        if debug and i_halo == ih_choose:
            if write_SNe_energy_fraction:
                print "rate of those that were directly heated", len(mass_gas_progen[ejected][dir_heated_unbound_ts_sorted&select_wind])

        # Add ejected particles (including their mass and metallicity at the time of ejection) to the ejected particle lists
        ejecta_ts["id"] = np.append(np.array(ejecta_ts["id"]), id_gas_progen[ejected][select_wind])
        ejecta_ts["mass"] = np.append(np.array(ejecta_ts["mass"]), mass_gas_progen[ejected][select_wind])
        ejecta_ts["metallicity"] = np.append(np.array(ejecta_ts["metallicity"]), metallicity_gas_progen[ejected][select_wind])
        ejecta_ts["t"] = np.append(np.array(ejecta_ts["t"]), np.zeros_like(id_gas_progen[ejected][select_wind]))
        ejecta_ts["from_mp"] = np.append(np.array(ejecta_ts["from_mp"]), np.ones_like(id_gas_progen[ejected][select_wind]))
        if extra_accretion_measurements:
            ejecta_ts["mchalo_ej"] = np.append(np.array(ejecta_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen[ejected][select_wind])+halo_mchalo)
            if not bound_only_mode:
                ejecta_ts["rmax"] = np.append(np.array(ejecta_ts["rmax"]), np.zeros_like(mass_gas_progen[ejected][select_wind])+np.nan)
    
        mpf.Check_Dict(ejecta_ts, "jh92fha")

        # Add the remaining ejected particles which are not not selected in the wind (including their mass) to the halo reheated particle lists
        halo_reheat_ts["id"] = np.append(np.array(halo_reheat_ts["id"]), id_gas_progen[ejected][select_wind==False])
        halo_reheat_ts["mass"] = np.append(np.array(halo_reheat_ts["mass"]), mass_gas_progen[ejected][select_wind==False])
        halo_reheat_ts["metallicity"] = np.append(np.array(halo_reheat_ts["metallicity"]), metallicity_gas_progen[ejected][select_wind==False])
        if write_SNe_energy_fraction:
            halo_reheat_ts["oxygen"] = np.append(np.array(halo_reheat_ts["oxygen"]), oxygen_gas_progen[ejected][select_wind==False])
            halo_reheat_ts["iron"] = np.append(np.array(halo_reheat_ts["iron"]), iron_gas_progen[ejected][select_wind==False])
        halo_reheat_ts["time"] = np.append(np.array(halo_reheat_ts["time"]), np.zeros_like(mass_gas_progen[ejected][select_wind==False]))
        halo_reheat_ts["from_mp"] = np.append(np.array(halo_reheat_ts["from_mp"]), np.ones_like(mass_gas_progen[ejected][select_wind==False]))
        if len(fVmax_cuts)>1:
            halo_reheat_ts["vpmax"] = np.append(np.array(halo_reheat_ts["vpmax"]), fVmax[select_wind==False])
        if extra_accretion_measurements:
            halo_reheat_ts["mchalo_ej"] = np.append(np.array(halo_reheat_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen[ejected][select_wind==False])+halo_mchalo)
            if not bound_only_mode:
                halo_reheat_ts["rmax"] = np.append(np.array(halo_reheat_ts["rmax"]), np.zeros_like(mass_gas_progen[ejected][select_wind==False])+np.nan)

        mpf.Check_Dict(halo_reheat_ts, "8hgjkasd87")

        # Add the subset of the ejected particles (regardless of whether they were considered halo wind particles or not) which were in the ISM or star forming reservoir previously to the ism wind particle lists
        wind_ts["id"] = np.append(np.array(wind_ts["id"]), id_gas_progen[ejected&(SF_progen|ISM_progen)])
        wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), mass_gas_progen[ejected&(SF_progen|ISM_progen)])
        wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), metallicity_gas_progen[ejected&(SF_progen|ISM_progen)])
        wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(id_gas_progen[ejected&(SF_progen|ISM_progen)]))
        wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(id_gas_progen[ejected&(SF_progen|ISM_progen)]))
        wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(id_gas_progen[ejected&(SF_progen|ISM_progen)])) # This will be updated shortly
        if extra_accretion_measurements:
            wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen[ejected&(SF_progen|ISM_progen)])+halo_mchalo)
            if not bound_only_mode:
                wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(mass_gas_progen[ejected&(SF_progen|ISM_progen)])+np.nan)
                
        # Find the subset of the particles joining the wind that came from the SF ISM
        from_SF = np.zeros_like(id_gas_progen[ejected&(SF_progen|ISM_progen)])
        from_SF[SF_progen[ejected&(SF_progen|ISM_progen)]] = 1
        if len(from_SF) > 0 and len(nsfism_ts["id"]) > 0 and len(nsfism_ts["id"][nsfism_ts["from_SF"]==1])>0:
            ptr, time_ptr_actual = ms.match(id_gas_progen[ejected&(SF_progen|ISM_progen)], nsfism_ts["id"][nsfism_ts["from_SF"]==1],time_taken=time_ptr_actual)
            from_SF[ptr>=0] = 1
        wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), from_SF)

        mpf.Check_Dict(wind_ts, "675tasghj")

        # Add the other subset of these that were in the ISM reheated list to the ism wind particle lists
        if len(ism_reheat_ts["id"])>0 and len(id_gas_progen[ejected])>0:
            ptr, time_ptr_actual = ms.match(ism_reheat_ts["id"], id_gas_progen[ejected],time_taken=time_ptr_actual,arr2_sorted=True)
            ok_match = (ptr>=0)

            # Double check we aren't double-counting w.r.t SF/NSF ISM reheated particles
            if len(ism_reheat_ts["id"]) > 0 and len(id_gas_progen[ejected&(SF_progen|ISM_progen)]) > 0:
                ptr2, time_ptr_actual = ms.match(ism_reheat_ts["id"], id_gas_progen[ejected&(SF_progen|ISM_progen)],time_taken=time_ptr_actual,arr2_sorted=True)
                ok_match = (ptr>=0) & (ptr2<0)

                #if len(ptr2[(ptr>=0)&(ptr2>=0)]) > 0:
                #    print ""
                #    print "Warning: almost double-counted ism wind events between SF+NSF and the ISM reheated list"
                #    print ""

            # Find the subset of the particles joining the wind that came from the SF ISM
            from_SF = np.zeros_like(id_gas_progen[ejected][ptr][ok_match])
            from_SF[ism_reheat_ts["from_SF"][ok_match] == 1] = 1

            for i_cut, fVmax_cut in enumerate(fVmax_cuts):
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                halo_outflow_dict["mass_ism_reheated_join_wind"+fV_str] += np.sum(mass_gas_progen[ejected][ptr][ok_match])
                halo_outflow_dict["n_ism_reheated_join_wind"+fV_str] += len(mass_gas_progen[ejected][ptr][ok_match])
                halo_outflow_dict["mZ_ism_reheated_join_wind"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][ptr][ok_match])

                halo_outflow_dict["mass_ism_reheated_join_wind_from_SF"+fV_str] += np.sum(mass_gas_progen[ejected][ptr][ok_match][from_SF==1])
                halo_outflow_dict["mZ_ism_reheated_join_wind_from_SF"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[ejected][ptr][ok_match][from_SF==1])

                if write_SNe_energy_fraction:
                    halo_emass_dict["mass_join_ism_wind_dir_heat"+fV_str] += np.sum(mass_gas_progen[ejected][ptr][ok_match&dir_heated_progen[ejected][ptr]])
                    halo_emass_dict["mO_join_ism_wind"+fV_str] += np.sum((oxygen_gas_progen*mass_gas_progen)[ejected][ptr][ok_match])
                    halo_emass_dict["mFe_join_ism_wind"+fV_str] += np.sum((iron_gas_progen*mass_gas_progen)[ejected][ptr][ok_match])

            wind_cumul_ts["mass_ism_wind"] += np.sum(mass_gas_progen[ejected][ptr][ok_match])
            wind_cumul_ts["n_ism_wind"] += len(mass_gas_progen[ejected][ptr][ok_match])

            wind_ts["id"] = np.append(np.array(wind_ts["id"]), id_gas_progen[ejected][ptr][ok_match])
            wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), mass_gas_progen[ejected][ptr][ok_match])
            wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), metallicity_gas_progen[ejected][ptr][ok_match])
            wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(id_gas_progen[ejected][ptr][ok_match]))
            wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(id_gas_progen[ejected][ptr][ok_match]))
            wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), from_SF)
            wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.zeros_like(id_gas_progen[ejected][ptr][ok_match])) # This will be updated shortly
            if extra_accretion_measurements:
                wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen[ejected][ptr][ok_match])+halo_mchalo)
                if not bound_only_mode:
                    wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(mass_gas_progen[ejected][ptr][ok_match])+np.nan)

            mpf.Check_Dict(wind_ts, "67582jahdfs")

            if debug and i_halo == ih_choose:
                print "rate of ism reheated_list particles joining the ism wind because of being halo ejected", len(id_gas_progen[ejected][ptr][ok_match])
                if write_SNe_energy_fraction:
                    print "rate of those that were directly heated", len(mass_gas_progen[ejected][ptr][ok_match&dir_heated_progen[ejected][ptr]])

            # then remove them from the ISM reheated list
            id_temp = np.copy(ism_reheat_ts["id"][ok_match])
            if len(ism_reheat_ts["id"]) > 0 and len(id_temp)>0:
                ptr, time_ptr_actual = ms.match(np.array(ism_reheat_ts["id"]),id_temp,time_taken=time_ptr_actual)
                for name in ism_reheat_ts:
                    ism_reheat_ts[name] = np.array(ism_reheat_ts[name])[ptr<0]
                mpf.Check_Dict(ism_reheat_ts,"u987df6ys")

        # Count how many of the halo wind particles came from the ISM
        # Note that there are two types of measurement we want to do here
        # 1) Count wind particles that are leaving the halo for the first time (used to compute fraction of wind that ever leaves R_200)
        # 2) Count wind particles that are leaving the halo - period (could be first, second, third time etc)
        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            select_wind_i = (fVmax > fVmax_cut)
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            if i_cut == fVmax_info[1]: # Use fiducial velocity cut (for halo wind check) for this measurement

                if len(wind_ts["id"])>0 and len(id_gas_progen[ejected][select_wind_i])>0:
                    ptr, time_ptr_actual = ms.match(wind_ts["id"], id_gas_progen[ejected][select_wind_i],time_taken=time_ptr_actual,arr2_sorted=True)
                    ok_match = ptr>=0
                    first_time = ok_match & (wind_ts["left_halo"]==0) # Wind particles that are being ejected from the halo for the first time

                    halo_outflow_dict["n_join_halo_wind_from_ism"] += len(wind_ts["mass"][ok_match])
                    halo_outflow_dict["mass_join_halo_wind_from_ism"] += np.sum(wind_ts["mass"][ok_match])
                    halo_outflow_dict["mZ_join_halo_wind_from_ism"] += np.sum((wind_ts["metallicity"]*wind_ts["mass"])[ok_match])

                    halo_outflow_dict["mass_join_halo_wind_from_ism_first_time"] += np.sum(wind_ts["mass"][first_time])

                    # Make the approximation that particles that left the halo at this step took half the timestep to do so
                    just_left = wind_ts["t"][ok_match] == 0
                    dt_ts = t_ts - t_ps
                    temp = wind_ts["t"][ok_match]
                    temp[just_left] += 0.5 * dt_ts
                    wind_ts["t"][ok_match] = temp

                    if len(wind_ts["t"][first_time]) > 0:
                        halo_outflow_dict["time_join_halo_wind_from_ism_first_time"] += np.mean(wind_ts["t"][first_time])
                    else:
                        halo_outflow_dict["time_join_halo_wind_from_ism_first_time"] += np.nan

                    wind_cumul_ts["mass_ism_to_halo_wind_first_time"] += np.sum(wind_ts["mass"][first_time])
                    wind_cumul_ts["n_ism_to_halo_wind_first_time"] += len(wind_ts["mass"][first_time])

                    wind_ts["left_halo"][ok_match] = 1

        if debug and i_halo == ih_choose:
            print "rate of halo reheated particles", len(id_gas_progen[ejected]), " , number of those in the wind", len(id_gas_progen[ejected][select_wind])

    elif do_ns_calculation and len(id_star_gas_as) == 0:
        halo_outflow_dict["mass_halo_reheated"] += np.sum(mass_gas_progen)
        halo_outflow_dict["n_halo_reheated"] += len(mass_gas_progen)
        halo_outflow_dict["mass_sf_ism_reheated"] += np.sum(mass_gas_progen[SF_progen])
        halo_outflow_dict["n_sf_ism_reheated"] += len(mass_gas_progen[SF_progen])
        halo_outflow_dict["mass_nsf_ism_reheated"] += np.sum(mass_gas_progen[(SF_progen==False)&ISM_progen])
        halo_outflow_dict["n_nsf_ism_reheated"] += len(mass_gas_progen[(SF_progen==False)&ISM_progen])

        for i_cut, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

            halo_outflow_dict["mass_join_halo_wind"+fV_str] += np.sum(mass_gas_progen)
            halo_outflow_dict["n_join_halo_wind"+fV_str] += len(mass_gas_progen)
            halo_outflow_dict["mZ_join_halo_wind"+fV_str] += np.sum(metallicity_gas_progen*mass_gas_progen)

            halo_outflow_dict["mass_sf_ism_join_wind"+fV_str] += np.sum(mass_gas_progen[SF_progen])
            halo_outflow_dict["n_sf_ism_join_wind"+fV_str] += len(mass_gas_progen[SF_progen])
            halo_outflow_dict["mZ_sf_ism_join_wind"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[SF_progen])

            halo_outflow_dict["mass_nsf_ism_join_wind"+fV_str] += np.sum(mass_gas_progen[(SF_progen==False)&ISM_progen])
            halo_outflow_dict["n_nsf_ism_join_wind"+fV_str] += len(mass_gas_progen[(SF_progen==False)&ISM_progen])
            halo_outflow_dict["mZ_nsf_ism_join_wind"+fV_str] += np.sum((metallicity_gas_progen*mass_gas_progen)[(SF_progen==False)&ISM_progen])

            if write_SNe_energy_fraction:
                halo_emass_dict["mO_join_ism_wind"+fV_str] += np.sum((oxygen_gas_progen*mass_gas_progen)[ISM_progen])
                halo_emass_dict["mFe_join_ism_wind"+fV_str] += np.sum((iron_gas_progen*mass_gas_progen)[ISM_progen])
                halo_emass_dict["mO_join_halo_wind"+fV_str] += np.sum((oxygen_gas_progen*mass_gas_progen))
                halo_emass_dict["mFe_join_halo_wind"+fV_str] += np.sum((iron_gas_progen*mass_gas_progen))
   
        wind_cumul_ts["mass_ism_wind"] += np.sum(mass_gas_progen[SF_progen|ISM_progen])
        wind_cumul_ts["n_ism_wind"] += len(mass_gas_progen[SF_progen|ISM_progen])
        wind_cumul_ts["mass_ism_to_halo_wind_first_time"] += np.sum(mass_gas_progen[SF_progen|ISM_progen])
        wind_cumul_ts["n_ism_to_halo_wind_first_time"] += len(mass_gas_progen[SF_progen|ISM_progen])
        
        # Add ejected particles (including their mass and metallicity at the time of ejection) to the ejected particle lists
        ejecta_ts["id"] = np.append(np.array(ejecta_ts["id"]), id_gas_progen)
        ejecta_ts["mass"] = np.append(np.array(ejecta_ts["mass"]), mass_gas_progen)
        ejecta_ts["metallicity"] = np.append(np.array(ejecta_ts["metallicity"]), metallicity_gas_progen)
        ejecta_ts["t"] = np.append(np.array(ejecta_ts["t"]), np.zeros_like(id_gas_progen))
        ejecta_ts["from_mp"] = np.append(np.array(ejecta_ts["from_mp"]), np.ones_like(id_gas_progen))
        if extra_accretion_measurements:
            ejecta_ts["mchalo_ej"] = np.append(np.array(ejecta_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen)+halo_mchalo)
            if not bound_only_mode:
                ejecta_ts["rmax"] = np.append(np.array(ejecta_ts["rmax"]), np.zeros_like(mass_gas_progen)+np.nan)
           
        # Add the subset of these that were in the star forming reservoir previously to the wind particle lists
        wind_ts["id"] = np.append(np.array(wind_ts["id"]), id_gas_progen[SF_progen|ISM_progen])
        wind_ts["mass"] = np.append(np.array(wind_ts["mass"]), mass_gas_progen[SF_progen|ISM_progen])
        wind_ts["metallicity"] = np.append(np.array(wind_ts["metallicity"]), metallicity_gas_progen[SF_progen|ISM_progen])
        wind_ts["t"] = np.append(np.array(wind_ts["t"]), np.zeros_like(id_gas_progen[SF_progen|ISM_progen]))
        wind_ts["from_mp"] = np.append(np.array(wind_ts["from_mp"]), np.ones_like(id_gas_progen[SF_progen|ISM_progen]))
        wind_ts["left_halo"] = np.append(np.array(wind_ts["left_halo"]), np.ones_like(id_gas_progen[SF_progen|ISM_progen]))
        if extra_accretion_measurements:
            wind_ts["mchalo_ej"] = np.append(np.array(wind_ts["mchalo_ej"]), np.zeros_like(mass_gas_progen[SF_progen|ISM_progen])+halo_mchalo)
            if not bound_only_mode:
                wind_ts["rmax"] = np.append(np.array(wind_ts["rmax"]), np.zeros_like(mass_gas_progen[SF_progen|ISM_progen])+np.nan)

        # Find the subset of the particles joining the wind that came from the SF ISM
        from_SF = np.zeros_like(id_gas_progen)
        from_SF[SF_progen] = 1
        if len(from_SF) > 0 and len(nsfism_ts["id"]) > 0 and len(nsfism_ts["id"][nsfism_ts["from_SF"]==1])>0:
            ptr, time_ptr_actual = ms.match(id_gas_progen, nsfism_ts["id"][nsfism_ts["from_SF"]==1],time_taken=time_ptr_actual)
            from_SF[ptr>=0] = 1
        wind_ts["from_SF"] = np.append(np.array(wind_ts["from_SF"]), from_SF[SF_progen|ISM_progen])

        mpf.Check_Dict(ejecta_ts,"aaa")
        mpf.Check_Dict(wind_ts,"aaa25")
        
        ptr_eject_gas = np.zeros_like(id_gas_progen)
    else:
        ptr_eject_gas = np.array([])

    # Compute gas mass accreted onto black holes
    if full_calculation and len(id_bh)>0 and len(id_bh_progen) >0:
        ptr, time_ptr_actual = ms.match(id_bh, id_bh_progen,time_taken=time_ptr_actual)
        ok_match = ptr >= 0

        halo_mass_dict["mass_acc_bh"] = np.sum( mass_acc_bh[ok_match] ) - np.sum(mass_acc_bh_progen[ptr][ok_match])
        if halo_mass_dict["mass_acc_bh"] < 0.0:
            print "Warning, black holes lost mass from the last snapshot to this one"
            print "value is ", halo_mass_dict["mass_acc_bh"]
            halo_mass_dict["mass_acc_bh"] = 0.0

        if debug and i_halo == ih_choose:
            print "black hole accretion rate for this subhalo", halo_mass_dict["mass_acc_bh"]

    # Compute metal mass injected into the gas
    if full_calculation and len(id_gas)>0 and len(id_gas_progen) >0:
        ptr, time_ptr_actual = ms.match(id_gas, id_gas_progen,time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        mZ_injected = np.sum((mass_gas*metallicity_gas)[ok_match] - (mass_gas_progen*metallicity_gas_progen)[ptr][ok_match])
        halo_mass_dict["mZ_injected"] += mZ_injected

    # Compute metal mass injected into the gas
    if full_calculation and len(id_gas)>0 and len(id_gas_progen[ISM_progen]) >0:
        ptr, time_ptr_actual = ms.match(id_gas, id_gas_progen[ISM_progen],time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        mZ_injected_ism = np.sum((mass_gas*metallicity_gas)[ok_match] - (mass_gas_progen*metallicity_gas_progen)[ptr][ok_match])
        halo_mass_dict["mZ_injected_ism"] += mZ_injected_ism

    # Compute stellar recycling for stars that already existed on the previous step
    if len(id_star_progen) > 0 and len(id_star) > 0:
        ptr, time_ptr_actual = ms.match(id_star, id_star_progen,time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        halo_mass_dict["mass_stellar_rec"] += np.sum(mass_star_progen[ptr][ok_match]) - np.sum(mass_star[ok_match])

    ####### Remove ejecta particles that have returned to the halo ############
    if len(ejecta_ts["id"]) > 0 and len(id_star_gas_as) >0:
        ptr, time_ptr_actual = ms.match(ejecta_ts["id"], id_star_gas_as,time_taken=time_ptr_actual,arr2_sorted=True)
        ok = ptr < 0 # Particles that are not in the FoF group on this _ts
        for name in ejecta_ts:
            ejecta_ts[name] = ejecta_ts[name][ok]
        mpf.Check_Dict(ejecta_ts,"ajskd297")

    # Equivalent for ISM
    temp = np.append(id_gas[ISM|SF], id_star)
    if len(wind_ts["id"]) > 0 and len(temp) >0:
        ptr, time_ptr_actual = ms.match(wind_ts["id"], temp,time_taken=time_ptr_actual)
        ok = ptr < 0 # Particles that are not in the FoF group on this _ts
        for name in wind_ts:
            wind_ts[name] = wind_ts[name][ok]
        mpf.Check_Dict(wind_ts,"874yhfg2ygws")

    # Equivalent for SF > NSF ISM particles
    temp = np.append(id_gas[SF|(ISM==False)], id_star)
    if len(nsfism_ts["id"]) > 0 and len(temp) > 0:
        ptr, time_ptr_actual = ms.match(nsfism_ts["id"], temp,time_taken=time_ptr_actual)
        ok = ptr < 0

        for name in nsfism_ts:
            nsfism_ts[name] = nsfism_ts[name][ok]
            
        mpf.Check_Dict(nsfism_ts, " tjkg3241 iaudsf")
        
    # For ISM tracking lists, we want to preserve information for particles that enter the ireheat list
    temp = np.append(id_gas[SF|ISM], ism_reheat_ts["id"])
    if len(ism_ts["id"]) > 0 and len(temp) > 0:
        ptr, time_ptr_actual = ms.match(ism_ts["id"], temp,time_taken=time_ptr_actual)
        ok = ptr >= 0

        for name in ism_ts:
            ism_ts[name] = ism_ts[name][ok]
            
        mpf.Check_Dict(ism_ts, " tasdiuhyaiusd dsf")

    ################ Do other measurements ####################

    halo_mass_dict["mass_gas_SF"] += np.sum(mass_gas[SF])
    halo_mass_dict["n_gas_SF"] += len(mass_gas[SF])
    halo_mass_dict["mZ_gas_SF"] += np.sum((mass_gas*metallicity_gas)[SF])

    halo_mass_dict["mass_gas_NSF"] += np.sum(mass_gas[SF==False])
    halo_mass_dict["n_gas_NSF"] += len(mass_gas[SF==False])
    halo_mass_dict["mZ_gas_NSF"] += np.sum((mass_gas*metallicity_gas)[SF==False])

    halo_mass_dict["mass_gas_NSF_ISM"] += np.sum(mass_gas[(SF==False)&ISM])
    halo_mass_dict["n_gas_NSF_ISM"] += len(mass_gas[(SF==False)&ISM])
    halo_mass_dict["mZ_gas_NSF_ISM"] += np.sum((mass_gas*metallicity_gas)[(SF==False)&ISM])

    halo_mass_dict["mass_star"] += np.sum(mass_star)
    halo_mass_dict["n_star"] += len(mass_star)
    halo_mass_dict["mass_star_init"] += np.sum(mass_star_init)
    halo_mass_dict["mZ_star"] += np.sum(mass_star*metallicity_star)

    halo_mass_dict["mass_bh"] += np.sum(mass_bh)

    halo_mass_dict["mass_new_stars_init_100_30kpc"] += np.sum(mass_star_init[(r_star<30)&(age_star<100)])
    halo_mass_dict["mass_stars_30kpc"] += np.sum(mass_star[r_star<30])
    halo_mass_dict["mZ_new_stars_100_30kpc"] += np.sum((metallicity_star*mass_star_init)[(r_star<30)&(age_star<100)])
    halo_mass_dict["mZ_stars_30kpc"] += np.sum((metallicity_star*mass_star)[r_star<30])

    if write_SNe_energy_fraction:
        halo_emass_dict["mO_ism"] += np.sum((mass_gas*oxygen_gas)[ISM])
        halo_emass_dict["mO_cgm"] += np.sum((mass_gas*oxygen_gas)[ISM==False])
        halo_emass_dict["mFe_ism"] += np.sum((mass_gas*iron_gas)[ISM])
        halo_emass_dict["mFe_cgm"] += np.sum((mass_gas*iron_gas)[ISM==False])

    # Find stars that have injected SNe type II energy over the previous snapshot (note this assume a certain time delay for type II SNe)
    if write_SNe_energy_fraction:
        SNe_delay = 30 # Myr
        dt = (t_ts - t_ps)*1e3
        halo_emass_dict["fb_e_times_mass_new_star"] += np.sum( (fb_e_fraction * mass_star_init)[(age_star > SNe_delay) & (age_star-SNe_delay < dt)] )
       
    # Check the uniqueness of the tracked lists
    if len(halo_reheat_ts["id"]) != len(np.unique(halo_reheat_ts["id"])):
        junk, index = np.unique(halo_reheat_ts["id"], return_index=True)
        for name in halo_reheat_ts:
            halo_reheat_ts[name] = halo_reheat_ts[name][index]

    if len(ism_reheat_ts["id"]) != len(np.unique(ism_reheat_ts["id"])):
        junk, index = np.unique(ism_reheat_ts["id"], return_index=True)
        for name in ism_reheat_ts:
            ism_reheat_ts[name] = ism_reheat_ts[name][index]

    if len(nsfism_ts["id"]) != len(np.unique(nsfism_ts["id"])):
        junk, index = np.unique(nsfism_ts["id"], return_index=True)
        for name in nsfism_ts:
            nsfism_ts[name] = nsfism_ts[name][index]

    if len(ism_ts["id"]) != len(np.unique(ism_ts["id"])):
        junk, index = np.unique(ism_ts["id"], return_index=True)
        for name in ism_ts:
            ism_ts[name] = ism_ts[name][index]

    if len(wind_ts["id"]) != len(np.unique(wind_ts["id"])):
        junk, index = np.unique(wind_ts["id"], return_index=True)
        for name in wind_ts:
            wind_ts[name] = wind_ts[name][index]

    if len(ejecta_ts["id"]) != len(np.unique(ejecta_ts["id"])):
        junk, index = np.unique(ejecta_ts["id"], return_index=True)
        for name in ejecta_ts:
            ejecta_ts[name] = ejecta_ts[name][index]

    # Measure wind/ejecta reservoirs
    for fVmax_cut in fVmax_cuts:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

        if len(id_gas) > 0 and len(np.array(wind_ts["id"]))> 0:
            ptr, time_ptr_actual = ms.match(id_gas, np.array(wind_ts["id"]),time_taken=time_ptr_actual)
            ok_match = ptr >= 0
                        
        else:
            ok_match = np.zeros(len(id_gas))<0

        ok_ireheat = np.zeros(len(id_gas))<0
        if fVmax_cut != fVmax_cuts[-1]:
            if len(id_gas) > 0 and len(np.array(ism_reheat_ts["id"])) > 0:
                ptr, time_ptr_actual = ms.match(id_gas, np.array(ism_reheat_ts["id"]),time_taken=time_ptr_actual)
                ok_match = ok_match | ( (ptr >= 0) & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut) )
                ok_ireheat = (ptr>=0) & (ism_reheat_ts["vpmax"][ptr] <= fVmax_cut)

        # Wind mass that is inside this subhalo
        halo_mass_dict["n_inside"+fV_str] += len(mass_gas[ok_match])
        halo_mass_dict["mass_inside"+fV_str] += np.sum(mass_gas[ok_match])
        halo_mass_dict["mZ_inside"+fV_str] += np.sum(mass_gas[ok_match] * metallicity_gas[ok_match])

        cold = temperature_gas < TLo_cut
        halo_mass_dict["mass_cgm_wind_TLo"+fV_str] += np.sum(mass_gas[ok_match & cold])

        if halo_subgroup_number == 0:
            r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
            outer = r_gas > 0.5 * r200
            halo_mass_dict["mass_cgm_wind_Router"+fV_str] += np.sum(mass_gas[ok_match & outer])


        # Total ejecta mass
        halo_mass_dict["n_outside"+fV_str] += len(ejecta_ts["mass"])
        halo_mass_dict["mZ_outside"+fV_str] += np.sum(np.array(ejecta_ts["mass"]) * np.array(ejecta_ts["metallicity"]))
        halo_mass_dict["mass_outside"+fV_str] += np.sum(np.array(ejecta_ts["mass"]))

        if len(halo_reheat_ts["id"]) > 0:
            ok_ejecta = halo_reheat_ts["vpmax"] > fVmax_cut

            halo_mass_dict["n_outside"+fV_str] += len(halo_reheat_ts["mass"][ok_ejecta])
            halo_mass_dict["mZ_outside"+fV_str] += np.sum(np.array(halo_reheat_ts["mass"][ok_ejecta]) * np.array(halo_reheat_ts["metallicity"][ok_ejecta]))
            halo_mass_dict["mass_outside"+fV_str] += np.sum(np.array(halo_reheat_ts["mass"][ok_ejecta]))

            halo_mass_dict["mass_hreheat"+fV_str] += np.sum(np.array(halo_reheat_ts["mass"][ok_ejecta==False]))
            halo_mass_dict["mZ_hreheat"+fV_str] += np.sum((np.array(halo_reheat_ts["mass"])*np.array(halo_reheat_ts["metallicity"]))[ok_ejecta==False])

        # Total wind mass
        halo_mass_dict["n_outside_galaxy"+fV_str] += len(wind_ts["mass"])
        halo_mass_dict["mass_outside_galaxy"+fV_str] += np.sum(np.array(wind_ts["mass"]))
        halo_mass_dict["mZ_outside_galaxy"+fV_str] += np.sum(np.array(wind_ts["metallicity"])*np.array(wind_ts["mass"]))

        # CGM ireheat mass
        halo_mass_dict["mass_cgm_ireheat"+fV_str]  += np.sum(mass_gas[ok_ireheat])
        halo_mass_dict["mZ_cgm_ireheat"+fV_str]  += np.sum((mass_gas*metallicity_gas)[ok_ireheat])

        # Don't think this will be needed here - but uncomment if not!
        #cold = temperature_gas < TLo_cut
        halo_mass_dict["mass_cgm_ireheat_TLo"+fV_str] += np.sum(mass_gas[ok_ireheat & cold])

        if halo_subgroup_number == 0:
            r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
            outer = r_gas > 0.5 * r200
            halo_mass_dict["mass_cgm_ireheat_Router"+fV_str] += np.sum(mass_gas[ok_ireheat & outer])

        if len(ism_reheat_ts["id"]) > 0:
            ok_wind = ism_reheat_ts["vpmax"] > fVmax_cut
            
            halo_mass_dict["n_outside_galaxy"+fV_str] += len(ism_reheat_ts["mass"][ok_wind])
            halo_mass_dict["mass_outside_galaxy"+fV_str] += np.sum(np.array(ism_reheat_ts["mass"][ok_wind]))
            halo_mass_dict["mZ_outside_galaxy"+fV_str] += np.sum(np.array(ism_reheat_ts["metallicity"][ok_wind])*np.array(ism_reheat_ts["mass"][ok_wind]))

    # Measure the mass of pristine material within the CGM of this subhalo
    if full_calculation and len(id_gas) > 0:
        ok_match = np.zeros_like(id_gas)<0
        ok_galtran = np.zeros_like(id_gas)<0
        if len(np.array(wind_ts["id"]))> 0:
            ptr, time_ptr_actual = ms.match(id_gas, np.array(wind_ts["id"]),time_taken=time_ptr_actual)
            ok_match = ok_match | (ptr >= 0)
        if len(np.array(ism_reheat_ts["id"]))>0:
            ptr, time_ptr_actual = ms.match(id_gas, np.array(ism_reheat_ts["id"]),time_taken=time_ptr_actual)
            ok_match = ok_match | (ptr >= 0)

        ok_galtran = in_master_wind & (ok_match==False)
        ok_match = ok_match | in_master_wind

        pristine_CGM = (SF==False) & (ISM==False) & (ok_match==False)
        galtran_CGM = (SF==False) & (ISM==False) & (ok_galtran)
        halo_mass_dict["mass_cgm_pristine"] += np.sum(mass_gas[pristine_CGM])
        halo_mass_dict["mZ_cgm_pristine"] += np.sum((mass_gas*metallicity_gas)[pristine_CGM])
        halo_mass_dict["mass_cgm_galtran"] += np.sum(mass_gas[galtran_CGM])
        halo_mass_dict["mZ_cgm_galtran"] += np.sum((mass_gas*metallicity_gas)[galtran_CGM])

        cold = temperature_gas < TLo_cut
        halo_mass_dict["mass_cgm_pristine_TLo"] += np.sum(mass_gas[pristine_CGM & cold])
        halo_mass_dict["mass_cgm_galtran_TLo"] += np.sum(mass_gas[galtran_CGM & cold])

        if halo_subgroup_number == 0:
            r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc
            outer = r_gas > 0.5 * r200
            halo_mass_dict["mass_cgm_pristine_Router"] += np.sum(mass_gas[pristine_CGM & outer])
            halo_mass_dict["mass_cgm_galtran_Router"] += np.sum(mass_gas[galtran_CGM & outer])
        

    # Compute the mass of galtran material in the ejected gas reservoir
    # Also compute the mass of ejecta from that came from ISM 
    # (closes a loophole where m_ej_wind != m_outside_galaxy - m_inside if there are wind particles in other subhaloes of this FoF group)
    # (or if there are wind particles in the halo_reheat reservoir (given velocity cut))
    if full_calculation and len(ejecta_ts["id"]) > 0:

        if len(wind_ts["id"]) > 0:
            ptr, time_ptr_actual = ms.match(ejecta_ts["id"], wind_ts["id"], time_taken=time_ptr_actual)
            ok_match = ptr>=0
            for fVmax_cut in fVmax_cuts:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                halo_mass_dict["mass_ej_wind"+fV_str] += np.sum(np.array(ejecta_ts["mass"][ok_match]))
                halo_mass_dict["mZ_ej_wind"+fV_str] += np.sum((np.array(ejecta_ts["mass"])*np.array(ejecta_ts["metallicity"]))[ok_match])
        else:
            ok_match = np.zeros_like(ejecta_ts["id"])<0

        if len(id_gas_ejecta_ts_fixed) > 0 and len(id_gas_ejecta_ts_fixed[in_master_wind_ejecta_ts_fixed]) > 0 and len(ejecta_ts["id"])>0:
            ptr, time_ptr_actual = ms.match(ejecta_ts["id"], id_gas_ejecta_ts_fixed[in_master_wind_ejecta_ts_fixed], time_taken=time_ptr_actual,arr2_sorted=True)
            ok_match = (ok_match==False) & (ptr >= 0) # Don't double count this subhaloes wind as galtran

            halo_mass_dict["mass_ej_galtran"] += np.sum(np.array(ejecta_ts["mass"][ok_match]))
            halo_mass_dict["mZ_ej_galtran"] += np.sum((np.array(ejecta_ts["mass"])*(np.array(ejecta_ts["metallicity"])))[ok_match])

    if full_calculation and len(halo_reheat_ts["id"]) > 0 and len(wind_ts["id"]) > 0:
        # Note that this is independent of ISM wind velocity cut, since everything outside R200 is auto-wind
        ptr, time_ptr_actual = ms.match(halo_reheat_ts["id"], wind_ts["id"], time_taken=time_ptr_actual)
        for fVmax_cut in fVmax_cuts:
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
            ok_match = (ptr>=0) & (halo_reheat_ts["vpmax"] > fVmax_cut)
            halo_mass_dict["mass_ej_wind"+fV_str]  += np.sum(np.array(halo_reheat_ts["mass"])[ok_match])
            halo_mass_dict["mZ_ej_wind"+fV_str]  += np.sum((np.array(halo_reheat_ts["metallicity"])*np.array(halo_reheat_ts["mass"]))[ok_match])

    if test_mode and i_halo == ih_choose:
        fVmax_cut = 0.25
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
        print ""
        print "mass_cgm_total", np.log10(halo_mass_dict["mass_gas_NSF"]-halo_mass_dict["mass_gas_NSF_ISM"])
        print "mass wind", np.log10(halo_mass_dict["mass_inside"+fV_str])
        print "mass pristine", np.log10(halo_mass_dict["mass_cgm_pristine"])
        print "mass galtran", np.log10(halo_mass_dict["mass_cgm_galtran"])
        print "mass ireheat", np.log10(halo_mass_dict["mass_cgm_ireheat"+fV_str])
        print "Sum of those", np.log10(halo_mass_dict["mass_inside"+fV_str] +halo_mass_dict["mass_cgm_pristine"]+halo_mass_dict["mass_cgm_galtran"] + halo_mass_dict["mass_cgm_ireheat"+fV_str])

    if extra_wind_measurements:
        # Do shell measurements
        r_shell_list = [10,30,50]
        cv_min_list = [0,50,150,250]
        shell_width = 5. # pkpc
        kpc2km = 3.0857 * 10**16
        s2yr = 31557600.0

        dr = 0.1
        rlo = np.arange(0.0,1.0,dr)
        rhi = rlo + dr
        rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
        vmin =[0.0, 0.125, 0.25, 0.5, 1.0]

        n_rbins = len(rmid)
        for name in output_shell_names:
            halo_shell_dict[name] = np.zeros(n_rbins)

        halo_shell_dict["shell_rmid"] += rmid

        if halo_subgroup_number == 0 and full_calculation:

            m_h = 1.6726 * 10**(-27)
            kms = 1e3
            cm = 1e2
            gamma = 5/3.0 # Ratio of specific heats
            xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye 15
            # See http://www.mpia.de/homes/dullemon/lectures/hydrodynamicsII/chap_1.pdf, pg 7
            pressure_gas = (gamma-1) * nh_gas / xh_sf_thresh* cm**3 * m_h * internal_energy_gas * kms**2 # kg m-1 s^-2 = Pa

            r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
            r_dm = np.sqrt(np.square(coordinates_dm[:,0])+np.square(coordinates_dm[:,1])+np.square(coordinates_dm[:,2]))
            r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))
            r_bh = np.sqrt(np.square(coordinates_bh[:,0])+np.square(coordinates_bh[:,1])+np.square(coordinates_bh[:,2]))

            r_all = np.concatenate((r_gas,r_star,r_dm,r_bh))
            mass_dm_temp = np.zeros_like(r_dm) + mass_dm * 1.989e43 * g2Msun / h
            mass_all = np.concatenate((mass_gas,mass_star,mass_dm_temp,mass_bh))
            order = np.argsort(r_all)
            mass_cum_sorted = np.cumsum(mass_all[order])
            inverse = np.argsort(order)
            mass_cum = mass_cum_sorted[inverse]
            mass_gas_cum = mass_cum[0:len(mass_gas)]

            G = 6.67408e-11 # m^3 kg^-1 s^-2
            Msun = 1.989e30 # kg
            kpc =  3.0857e19 # m
            kms = 1e3 # ms^-1

            # Spherically-averaged gravitational acceleration
            a_grav_gas = G * mass_gas_cum*Msun / (r_gas*kpc)**2 # m s^-2

            r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc

            if write_SNe_energy_fraction:

                r_star_sat= np.sqrt(np.square(coordinates_star_sat[:,0])+np.square(coordinates_star_sat[:,1])+np.square(coordinates_star_sat[:,2]))
                r_bh_sat = np.sqrt(np.square(coordinates_bh_sat[:,0])+np.square(coordinates_bh_sat[:,1])+np.square(coordinates_bh_sat[:,2]))

                SNe_delay = 30 # Myr
                dt = (t_ts - t_ps)*1e3
                explode = (age_star > SNe_delay) & (age_star-SNe_delay < dt)
                e_sn = 8.73e15 # ergs per gram - energy injected per unit mass (for Eagle reference model!)
                g2Msun = 1.989e33
                # prevent floating point issues
                #e_sn *= g2Msun # erg per Msun 

                Einjected_SNe = np.zeros_like(mass_star_init)
                # Was getting a problem with floating point overflow here, so rejigging the unit conversions
                Einjected_SNe[explode] = (fb_e_fraction * mass_star_init)[explode] / (dt*1e6) # erg /yr / e_sn= 10^-7 kg (m/s)2 /yr / e_sn
                # prevent floating point issues - this hack cancels the one just before
                #Einjected_SNe *= 1e-7 / (g2Msun*1e-3) * 1e-6 # Msun (km/s)^2 / yr / e_sn
                Einjected_SNe *= 1e-7 / (1*1e-3) * 1e-6 # Msun (km/s)^2 / yr / e_sn
                Einjected_SNe *= e_sn # Msun (km/s)^2 / yr

                Einjected_SNe_sat = np.zeros_like(mass_star_init_sat)
                explode = (age_star_sat > SNe_delay) & (age_star_sat-SNe_delay < dt)
                Einjected_SNe_sat[explode] = (fb_e_fraction_sat * mass_star_init_sat)[explode] / (dt*1e6)
                # prevent floating point issues - this hack cancels the one just before
                #Einjected_SNe_sat *= 1e-7 / (g2Msun*1e-3) * 1e-6 # Msun (km/s)^2 / yr / e_sn
                Einjected_SNe_sat *= 1e-7 / (1*1e-3) * 1e-6 # Msun (km/s)^2 / yr / e_sn
                Einjected_SNe_sat *= e_sn # Msun (km/s)^2 / yr

                for i_r in range(len(rlo)):
                    okr = (r_star<rhi[i_r]*r200) & (r_star > rlo[i_r]*r200)
                    halo_shell_dict["dEdt_SNe"][i_r] += np.sum(Einjected_SNe[okr])

                    okr = (r_star_sat<rhi[i_r]*r200) & (r_star_sat > rlo[i_r]*r200)
                    halo_shell_dict["dEdt_SNe_sat"][i_r] += np.sum(Einjected_SNe_sat[okr])

                # AGN feedback parameters
                eps_r = 0.1 # Radiative efficiency (fraction of accreted rest-mass energy which is radiated)
                eps_f = 0.15 # Coupling efficiency between radiated energy and the ISM
                c = 299792458 # ms-1
                # prevent floating point issues
                #c2 = c**2 * 1e7 * g2Msun *1e-3 # erg per Msun
                c2 = c**2 * 1e7 * 1 *1e-3
                e_bh = eps_r *eps_f / (1-eps_r)*c2 # energy injected per unit mass growth of black holes (erg per Msun)
                # Note this is obviously wrong for no-AGN runs - but leave this for post-processing to correct

                if len(id_bh_progen)>0 and len(id_bh)>0:
                    ptr, time_ptr_actual = ms.match(id_bh, id_bh_progen,time_taken=time_ptr_actual)
                    ok_match = ptr >= 0
                    dm_acc_bh = mass_acc_bh[ok_match] - mass_acc_bh_progen[ptr][ok_match]
                    Einjected_AGN = dm_acc_bh / (dt*1e6) # erg /yr / e_bh= 10^-7 kg (m/s)2 /yr / e_bh
                    # prevent floating point issues
                    #Einjected_AGN *= 1e-7 / (g2Msun*1e-3) * 1e-6 # Msun (km/s)^2 / yr /e_bh
                    Einjected_AGN *= 1e-7 / (1*1e-3) * 1e-6 # Msun (km/s)^2 / yr /e_bh
                    Einjected_AGN *= e_bh

                    for i_r in range(len(rlo)):
                        okr = (r_bh[ok_match]<rhi[i_r]*r200) & (r_bh[ok_match] > rlo[i_r]*r200)
                        halo_shell_dict["dEdt_AGN"][i_r] += np.sum(Einjected_AGN[okr])

                if len(id_bh_all_progen) > 0 and len(id_bh_sat)>0:

                    mass_acc_bh_sat = mass_bh_sat - mass_bh_seed * nseed_bh_sat
                    mass_acc_bh_all_progen = mass_bh_all_progen - mass_bh_seed * nseed_bh_all_progen

                    ptr, time_ptr_actual = ms.match(id_bh_sat, id_bh_all_progen,time_taken=time_ptr_actual)
                    ok_match = ptr >= 0

                    dm_acc_bh_sat = mass_acc_bh_sat[ok_match] - mass_acc_bh_all_progen[ptr][ok_match]
                    Einjected_AGN_sat = dm_acc_bh_sat / (dt*1e6)
                    # prevent floating point issues
                    #Einjected_AGN_sat *= 1e-7 / (g2Msun*1e-3) * 1e-6
                    Einjected_AGN_sat *= 1e-7 / (1*1e-3) * 1e-6
                    Einjected_AGN_sat *= e_bh

                    for i_r in range(len(rlo)):
                        okr = (r_bh_sat[ok_match]<rhi[i_r]*r200) & (r_bh_sat[ok_match] > rlo[i_r]*r200)
                        halo_shell_dict["dEdt_AGN_sat"][i_r] += np.sum(Einjected_AGN_sat[okr])

            for i_r in range(len(rlo)):
                okr = (r_gas<rhi[i_r]*r200) & (r_gas>rlo[i_r]*r200)

                halo_shell_dict["P50mw"][i_r] += us.Weighted_Percentile(pressure_gas[okr], mass_gas[okr],0.5) # kg m^-1 s^-2 = Pa
                halo_shell_dict["Pmean"][i_r] += np.sum(pressure_gas[okr]*mass_gas[okr])/np.sum(mass_gas[okr]) # kg m^-1 s^-2 = Pa
                halo_shell_dict["m_shell"][i_r] += np.sum(mass_gas[okr])
                if len(r_gas)>0:
                    ok_temp = np.argmin(abs(r_gas-rmid[i_r]*r200))
                    halo_shell_dict["a_grav"][i_r] += a_grav_gas[ok_temp] # m s^-2

                for vmin_i in vmin:
                    ok =  okr & (cv_gas>vmin_i * halo_vmax)

                    halo_shell_dict["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok])  / (dr) # Mstar
                    halo_shell_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok] *cv_gas[ok])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1
                    halo_shell_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok] *np.square(cv_gas[ok]))  / (dr*r200) / kpc2km * s2yr # Mstar km s^-1 yr-1
                    halo_shell_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += 0.5 * np.sum(mass_gas[ok] *cv_gas[ok] *np.sum(np.square(velocity_gas[ok]),axis=1))  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                    halo_shell_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok] * internal_energy_gas[ok] *cv_gas[ok])  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                    halo_shell_dict["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(cv_gas[ok], mass_gas[ok], 0.5) # kms^-1 (mass weighted)
                    halo_shell_dict["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(cv_gas[ok], mass_gas[ok], 0.9)
                    halo_shell_dict["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(cv_gas[ok], mass_gas[ok]*cv_gas[ok], 0.5) # kms^-1 (radial flux weighted)
                    halo_shell_dict["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(cv_gas[ok], mass_gas[ok]*cv_gas[ok], 0.9)

                    halo_shell_dict["P50mw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(pressure_gas[ok], mass_gas[ok],0.5) # kg m^-1 s^-2 = Pa
                    halo_shell_dict["P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(pressure_gas[ok], mass_gas[ok]*cv_gas[ok],0.5) # kg m^-1 s^-2 = Pa

                    # Compute for ISM material only
                    ok_ISM = ok & ISM
                    halo_shell_dict["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][i_r] += np.sum(mass_gas[ok_ISM] )  / (dr) # Mstar
                    halo_shell_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][i_r] += np.sum(mass_gas[ok_ISM] *cv_gas[ok_ISM])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1
                    halo_shell_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][i_r] += np.sum(mass_gas[ok_ISM] *np.square(cv_gas[ok_ISM]))  / (dr*r200) / kpc2km * s2yr # Mstar km s^-1 yr-1
                    halo_shell_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][i_r] += 0.5 * np.sum(mass_gas[ok_ISM] *cv_gas[ok_ISM] *np.sum(np.square(velocity_gas[ok_ISM]),axis=1))  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                    halo_shell_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"][i_r] += np.sum(mass_gas[ok_ISM] * internal_energy_gas[ok_ISM] *cv_gas[ok_ISM])  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1


                    for fVmax_cut in fVmax_cuts:
                        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                        if len(id_gas) > 0 and len(np.array(wind_ts["id"]))> 0:
                            ptr, time_ptr_actual = ms.match(id_gas, np.array(wind_ts["id"]),time_taken=time_ptr_actual)
                            ok_match = ptr >= 0

                        else:
                            ok_match = np.zeros(len(id_gas))<0

                        if fVmax_cut != fVmax_cuts[-1]:
                            if len(id_gas) > 0 and len(np.array(ism_reheat_ts["id"])) > 0:
                                ptr, time_ptr_actual = ms.match(id_gas, np.array(ism_reheat_ts["id"]),time_taken=time_ptr_actual)
                                ok_match = ok_match | ( (ptr >= 0) & (ism_reheat_ts["vpmax"][ptr] > fVmax_cut) )

                        ok2 = ok & ok_match # Subset that are in the ISM wind

                        halo_shell_dict["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += np.sum(mass_gas[ok2] )  / (dr) # Mstar
                        halo_shell_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += np.sum(mass_gas[ok2] *cv_gas[ok2])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1
                        halo_shell_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += np.sum(mass_gas[ok2] *np.square(cv_gas[ok2]))  / (dr*r200) / kpc2km * s2yr # Mstar km s^-1 yr-1
                        halo_shell_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += 0.5 * np.sum(mass_gas[ok2] *cv_gas[ok2] *np.sum(np.square(velocity_gas[ok2]),axis=1))  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                        halo_shell_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += np.sum(mass_gas[ok2] * internal_energy_gas[ok2] *cv_gas[ok2])  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                        halo_shell_dict["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += us.Weighted_Percentile(cv_gas[ok2], mass_gas[ok2], 0.5) # kms^-1
                        halo_shell_dict["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += us.Weighted_Percentile(cv_gas[ok2], mass_gas[ok2], 0.9)
                        halo_shell_dict["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += us.Weighted_Percentile(cv_gas[ok2], mass_gas[ok2]*cv_gas[ok2], 0.5) # kms^-1
                        halo_shell_dict["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"][i_r] += us.Weighted_Percentile(cv_gas[ok2], mass_gas[ok2]*cv_gas[ok2], 0.9)

    # Compute the full distribution of wind/ejecta particle times (number of snapshots outside the central galaxy/halo respectively)
    nt_ej_i, junk = np.histogram(np.array(ejecta_ts["t"]),bins=tret_bins_ts,weights=np.array(ejecta_ts["mass"]))
    nt_wi_i, junk = np.histogram(np.array(wind_ts["t"]),bins=tret_bins_ts, weights=np.array(wind_ts["mass"]))

    halo_t_dict["mass_t_ejecta"] += nt_ej_i
    halo_t_dict["mass_t_wind"] += nt_wi_i

    dt_ns = t_ns - t_ts
    ejecta_ts["t"] = np.array(ejecta_ts["t"])+dt_ns
    wind_ts["t"] = np.array(wind_ts["t"])+dt_ns

    if extra_accretion_measurements and not bound_only_mode:
        r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc

        r_ejecta = np.sqrt(np.square(coordinates_gas_ejecta_ts_fixed[:,0])+\
                           np.square(coordinates_gas_ejecta_ts_fixed[:,1])+\
                           np.square(coordinates_gas_ejecta_ts_fixed[:,2]))
        r_hreheat = np.sqrt(np.square(coordinates_gas_halo_reheat_ts_fixed[:,0])+\
                           np.square(coordinates_gas_halo_reheat_ts_fixed[:,1])+\
                           np.square(coordinates_gas_halo_reheat_ts_fixed[:,2]))
        r_wind = np.sqrt(np.square(coordinates_gas_wind_ts_fixed[:,0])+\
                           np.square(coordinates_gas_wind_ts_fixed[:,1])+\
                           np.square(coordinates_gas_wind_ts_fixed[:,2]))

        # Since these fixed lists were formed before the timestep, we need to check that the particles haven't returned on this step
        if len(id_gas_ejecta_ts_fixed)>0 and len(id_star_gas_as) > 0:
            ptr = ms.match(id_gas_ejecta_ts_fixed, id_star_gas_as)
            ok_ejecta = ptr < 0
        else:
            ok_ejecta = id_gas_ejecta_ts_fixed >= 0 # Dummy

        if len(id_gas_wind_ts_fixed)>0 and len(np.append(id_gas[ISM|SF],id_star)) > 0:
            ptr = ms.match(id_gas_wind_ts_fixed, np.append(id_gas[ISM|SF],id_star))
            ok_wind = ptr < 0
        else:
            ok_wind = id_gas_wind_ts_fixed >= 0 # Dummy

        # For convenience let's assume just do number weighting instead of mass weighting
        # (edit: This is a little silly now that I link these lists to the tracked list just below)
        # Note: these histograms are ignoring i/hreheat particles that are in the fiducial wind, but it would be a pain to implement this properly
        nr_ej_i, junk = np.histogram(r_ejecta[ok_ejecta]/r200,bins=bins_r_track_ej) 
        nr_wi_i, junk = np.histogram(r_wind[ok_wind]/r200,bins=bins_r_track_wi) 

        halo_t_dict["n_r_ejecta"] += nr_ej_i
        halo_t_dict["n_r_wind"] += nr_wi_i

        if len(ejecta_ts["rmax"]) > 0 and len(r_ejecta) > 0:
            ptr = ms.match(ejecta_ts["id"], id_gas_ejecta_ts_fixed)
            ok_match = ptr >= 0
            ejecta_ts["rmax"][np.isnan(ejecta_ts["rmax"]) & ok_match] = r_ejecta[ptr][ok_match&np.isnan(ejecta_ts["rmax"])]
            ejecta_ts["rmax"][ok_match] = np.max((ejecta_ts["rmax"][ok_match], r_ejecta[ptr][ok_match]),axis=0)

        if len(halo_reheat_ts["rmax"]) > 0 and len(r_hreheat) > 0:
            ptr = ms.match(halo_reheat_ts["id"], id_gas_halo_reheat_ts_fixed)
            ok_match = ptr >= 0
            halo_reheat_ts["rmax"][np.isnan(halo_reheat_ts["rmax"]) & ok_match] = r_hreheat[ptr][ok_match&np.isnan(halo_reheat_ts["rmax"])]
            halo_reheat_ts["rmax"][ok_match] = np.max((halo_reheat_ts["rmax"][ok_match], r_hreheat[ptr][ok_match]),axis=0)

        if len(wind_ts["rmax"]) > 0 and len(r_wind) > 0:
            ptr = ms.match(wind_ts["id"], id_gas_wind_ts_fixed)
            ok_match = ptr >= 0
            wind_ts["rmax"][np.isnan(wind_ts["rmax"]) & ok_match] = r_wind[ptr][ok_match&np.isnan(wind_ts["rmax"])]
            wind_ts["rmax"][ok_match] = np.max((wind_ts["rmax"][ok_match], r_wind[ptr][ok_match]),axis=0)

        if len(ism_reheat_ts["rmax"]) > 0 and len(id_gas)>0:

            ptr = ms.match(ism_reheat_ts["id"], id_gas)
            ok_match = ptr >= 0
            ism_reheat_ts["rmax"][np.isnan(ism_reheat_ts["rmax"]) & ok_match] = r_gas[ptr][ok_match&np.isnan(ism_reheat_ts["rmax"])]
            ism_reheat_ts["rmax"][ok_match] = np.max((ism_reheat_ts["rmax"][ok_match], r_gas[ptr][ok_match]),axis=0)

    if debug and i_halo == ih_choose:
        print "at the end of the step, number of parts in SF, nSF ISM, NSF NISM is", len(mass_gas[SF]), len(mass_gas[ISM&(SF==False)]), len(mass_gas) - len(mass_gas[SF]) - len(mass_gas[ISM&(SF==False)])
        print "at the end of the step, number of parts in ism/halo reheated lists is", len(ism_reheat_ts["id"]), len(halo_reheat_ts["id"])

    time_track_ptr += time.time() - time_ptr

    track_lists = [wind_ts, ism_reheat_ts, ejecta_ts, halo_reheat_ts, nsfism_ts, ism_ts, wind_cumul_ts]

    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop, time_ptr_actual]

    return halo_mass_dict, halo_emass_dict, halo_outflow_dict, halo_t_dict, halo_shell_dict, track_lists, timing_info
