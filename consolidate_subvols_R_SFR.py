# Note the main function of this script is to take measurements (say from R_SFR version or DM-only version) in the trim format
# and combine them with the (already-consolidated) measurements from the old file format

import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

#cat_format = "old"
cat_format = "trim" # Note here that I'm going to read trim R_SFR measurements and write them onto the old format

test_mode = False
subsample = False
n_per_bin = 1000
dark_matter = True
extra_wind_measurements = False
extra_accretion_measurements = False
rmax_measurements = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 2


# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_list = np.arange(1,201)[::-1] # starts from 1 for these trees
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub1d = 1'''


if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if "L0025" in sim_name:
        subvol_choose = [36]
        if "500" in sim_name:
            subvol_choose = [36,499,500,501,525]
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = [2000]
    else:
        print "err"
        quit()


output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)
    if subsample:
        output_base += "_subsample_"+str(n_per_bin)
    output_base += "_test_mode/"

input_path += "Processed_Catalogues/"

subhalo_prop_names = ["node_index", "subgroup_number","isInterpolated"]

name_list = ["mass_new_stars_init_sat", "mass_stellar_rec"]
        
if dark_matter:
    name_list += ["mass_diffuse_dm_accretion", "mass_diffuse_dm_pristine", "mass_hmerged_dm"]

# File to consolidate all processed galaxies within subvols into
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"a")

for snip, snap in zip(snip_list,snap_list):

    print "consolidating", snap, snip

    output_path = output_base + "Snip_"+str(snip)+"/"

    snap_group_str = "Snap_"+str(snap)
    if snap_group_str not in File:
        File.create_group(snap_group_str)
    snap_group = File[snap_group_str]

    subhalo_group_str = "subhalo_properties"
    if subhalo_group_str not in snap_group:
        snap_group.create_group(subhalo_group_str)
    subhalo_group = snap_group[subhalo_group_str]

    sub_input_list = []
    input_list = []
    for name in name_list:
        input_list.append([])   
    for name in subhalo_prop_names:
        sub_input_list.append([])

    if cat_format == "trim":
        nI_old_list = np.array([]).astype("int")

    # Find all the subvolume files (they won't be sorted in order)
    subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

    if len(subvols) == 0:
        print "There are no subvolume files. Run the pipeline first"
        File.close()
        quit()

    for n, subvol in enumerate(subvols):

        try:
            File_subvol = h5py.File(subvol,"r")
        except:
            print "Couldn't read subvol file", subvol, "moving on"
            continue

        print "Reading", subvol

        try:
            if cat_format == "old":
                halo_group = File_subvol["subhalo_data"]
            else:
                halo_group = File_subvol["Measurements"]
                halo_group_old = File_subvol["subhalo_data"]
        except:
            print "Read failed, skipping this subvol"
            continue

        if name_list[-1] in halo_group and name_list[0] in halo_group:

            # Choose which subhaloes to store
            # Old format refers to where we output everything. To save space we can cut un-needed subhaloes here
            # In the new format subhaloes are cut earlier on - so we don't need to do anything here
            if cat_format == "old":
                for name in name_list:
                    if name == "mass_gas_SF":
                        var1 = halo_group[name][:]
                    if name == "mass_gas_NSF":
                        var2 = halo_group[name][:]
                    if name == "mass_star":
                        var3 = halo_group[name][:]
                for name in subhalo_prop_names:
                    if name == "isInterpolated":
                        iI = halo_group[name][:]

                ok = ((np.isnan(var1)==False) & (var1 + var2 + var3 > 0)) | (iI==1)

            else:
                m200_cut = 969504631.0 
                order_sub = np.argsort(halo_group_old["mchalo"][:])[::-1]
                ok = (halo_group_old["m200_host"][:][order_sub] > m200_cut) & (halo_group_old["subgroup_number"][:][order_sub] == 0)

                n_for_limit = min(100,len(halo_group_old["m200_host"][:][order_sub]))
                mchalo_lim = np.median(np.sort(halo_group_old["mchalo"][:][order_sub][ok])[0:n_for_limit])

                n_sub_before_mask = len(halo_group_old["mchalo"][:])
                ok = ok | ((halo_group_old["subgroup_number"][:][order_sub]>0) & (halo_group_old["mchalo"][:][order_sub] > mchalo_lim)) | ((halo_group_old["isInterpolated"][:][order_sub]==1)&(halo_group_old["mchalo"][:][order_sub] > mchalo_lim))
                                   
            for i_name, name in enumerate(name_list):
                if cat_format == "old":
                    var = halo_group[name][:][ok]
                else:
                    var = halo_group[name][:]

                # Deal with 2d arrays and set data types correctly
                if input_list[i_name] == [] and len(var.shape) ==2:
                    input_list[i_name] = np.zeros((0,var.shape[1])).astype(var.dtype)
                elif len(input_list[i_name]) == 0:
                    input_list[i_name] = np.array([]).astype(var.dtype)

                input_list[i_name] = np.append(input_list[i_name], var,axis=0)

            for i_name, name in enumerate(subhalo_prop_names):

                if cat_format == "old":
                    var = halo_group[name][:][ok]
                else:
                    var = halo_group_old[name][:][order_sub][ok]

                if len(sub_input_list[i_name]) == 0:
                    sub_input_list[i_name] = np.array([]).astype(var.dtype)

                sub_input_list[i_name] = np.append(sub_input_list[i_name], var)

        else:
            print "subvol",n,"didn't contain any haloes"

        File_subvol.close()

    if len(input_list[0]) == 0:
        print "no measurements for this snipshot, moving on"
        continue

    ## Delete previous in the processed catalogue file
    for name in name_list:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]


    # Write measurements to catalogue file

    for i_name, name in enumerate(subhalo_prop_names):
        if name == "node_index":
            break
    nI = sub_input_list[i_name]

    ptr = ms.match(subhalo_group["node_index"][:], nI)
    ok_match = ptr >= 0

    for i_name, name in enumerate(name_list):
        if name == "mass_new_stars_init_sat":
            # For star formation rate, we actually want to overwrite the previous SFR measurement for satellites only
            print "Writing SFR for satellites"
            
            sfr = subhalo_group["mass_new_stars_init"][:]
            sat = (subhalo_group["subgroup_number"][:]>0) & (subhalo_group["isInterpolated"][:]==0)
            
            sfr[ok_match & sat] = input_list[i_name][ptr][ok_match&sat]
            
            cent = (subhalo_group["isInterpolated"][:]==0) & (subhalo_group["subgroup_number"][:]==0)
            print np.log10(np.sum(sfr[sat])), np.log10(np.sum(sfr[cent]))

            del subhalo_group["mass_new_stars_init"]
            subhalo_group.create_dataset("mass_new_stars_init", data=sfr)
        else:
            print "Writing", name
            output = np.zeros(len(subhalo_group["node_index"][:]))
            output[ok_match] = input_list[i_name][ptr][ok_match]
            subhalo_group.create_dataset(name, data=output)
    
File.close()
