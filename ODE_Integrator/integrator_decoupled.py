import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import utilities_statistics as us
import match_searchsorted as ms
import integrator_functions as ifu
import os
import time
import sys
sys.path.append("../.")
import measure_particles_functions as mpf
sys.path.append("/cosma/home/dphlss/d72fqv/Baryon_Cycle/Analysis_SHM/Stellar_Mass_Loss/.")
import stellar_mass_loss as ml

test_mode = False
fV_str = "_0p25vmax"

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"

# Highlight a particular galaxy in diagnostic plots                                                                                                                         
grn_select = 500; sgrn_select = 0

save_outputs = True
if "L0025" in sim_name:
    z_outputs = "all"
elif "L0100" in sim_name:
    z_outputs = [0.0, 0.5, 1.0, 2.0, 3.0]
save_rates = True
output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

# numerical parameters
tolerance = 1e-6

interpolate_z = False # Choose whether to interpolate between z_grid points (shouldn't be needed in principle)
disable_mergers = False # If True, only main progenitor (defined via stellar mass) is propagated down the tree
disable_stellar_recycling = False # If True, turn off stellar mass loss
instantaneous_recycling = True
r_ira = 0.4

######### Load snapshot/snipshot information ###############

filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(input_path+filename_subcat,"r")

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshots = tree_snapshot_info["snapshots_tree"][:][::-1]
z_snapshots = tree_snapshot_info["redshifts_tree"][:][::-1]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
snipshots = tree_snapshot_info["sn_i_a_pshots_simulation"][:][::-1]

File.close()

if save_outputs and z_outputs != "all":
    snap_outputs = []
    for z in z_outputs:
        snap_outputs.append( snapshots[1:-1][np.argmin(abs(z-z_snapshots[1:-1]))] )


########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
file_name = input_path+"/Processed_Catalogues/efficiencies_grid_"+sim_name+"_integrator"
if test_mode:
    file_name += "_test_mode"
file_name += ".hdf5"

file_grid = h5py.File(file_name,"r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

eff_names = ["facc_pristine_nonorm", "facc_reaccreted_nonorm"+fV_str, "facc_fof_nonorm", "facc_reheated_nonorm"+fV_str]
eff_names+= ["fcool_pristine_nonorm", "fcool_recooled_nonorm"+fV_str, "fcool_galtran_nonorm", "fcool_reheated_nonorm"+fV_str]
eff_names += ["fwind_ism_nonorm"+fV_str, "fwind_halo_nonorm"+fV_str]
eff_names += ["fism_reheated_nonorm"+fV_str, "fhalo_reheated_nonorm"+fV_str]
eff_names += ["sfr_nonorm", "fism_out_tot_nonorm", "fhalo_out_tot_nonorm"]

eff_dict = {}
for name in eff_names:
    eff_dict[name] = file_grid[name][:]

file_grid.close()

# Initialise 1d array of SFH (each point will be a single progenitor at a single snapshot)
# This is used to compute the stellar mass loss of all progenitors of galaxy at a given step
sfh_progens = [] # Mass (before mass loss) of stars formed at a given snap
t_progens = [] # Time the stars formed
Zstar_init_progens = [] # Metal mass in particles that formed at a given snap
nI_progens = np.array([]).astype("int")
dI_progens = np.array([]).astype("int")

# Open file of subhalo information
filename = "Processed_Catalogues/processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

print "preliminaries done, model starting"

ts_init = False
outsnap_list = []
outres_list = []
outres_rates = []

for i_snap, snapshot in enumerate(snapshots[1:-1]):
    
    print "integrating snap",snapshot,"of",snapshots.max()
 
    snapshot_ns = snapshots[1:][i_snap+1]
 
    # Compute timestep
    z_ts = z_snapshots[1:][i_snap]; z_ns = z_snapshots[1:][i_snap+1]
    a_ts = 1/(1+z_ts); a_ns = 1/(1+z_ns)
    t_ts = t_snapshots[1:][i_snap]; t_ns = t_snapshots[1:][i_snap+1]
    t_ps = t_snapshots[i_snap]


    dt = t_ts - t_ps

    # Identify closest grid timesteps
    lower = t_grid <= t_ts
    if len(t_grid[lower]) == 0:
        ind_t_lo = -1
    else:
        t_lo = t_grid[lower][np.argmin(abs(t_grid[lower]-t_ts))]
        ind_t_lo = np.argmin(abs(t_grid-t_lo))

    higher = t_grid >= t_ts
    if len(t_grid[higher]) == 0:
        ind_t_hi = 0
    else:
        t_hi = t_grid[higher][np.argmin(abs(t_grid[higher]-t_ts))]
        ind_t_hi = np.argmin(abs(t_grid-t_hi))

    ##### Load relevant subhalo data #####
    sub_names = ["group_number","subgroup_number","node_index","descendant_index","mchalo","m200_host"]
    sub_names += ["isInterpolated", "mass_new_stars_init", "mZ_new_stars_init"]
    sub_names += ["mass_star","mass_star_init","mass_gas_NSF", "mass_gas_NSF_ISM", "mass_gas_SF"]

    if not ts_init:
        if "Snap_"+str(snapshot) not in File:
            print "No data, skipping"
            continue

        snapnum_group = File["Snap_"+str(snapshot)]
        subhalo_group = snapnum_group["subhalo_properties"]

        if "node_index" not in subhalo_group:
            print "No data, skipping"; continue

        sub_dict_ts = {}
        for name in sub_names:
            sub_dict_ts[name] = subhalo_group[name][:]

        # Initialise baryon arrays for this snapshot (_ts)
        n_gal_ts = len(sub_dict_ts["group_number"])
        
        mres_dict_ts = {}
        res_names = ["cgm","ism","stars","ej","n_snap"]
        for name in res_names:
            mres_dict_ts[name] = np.zeros(n_gal_ts)
            
        ts_init = True
    else:
        sub_dict_ts = sub_dict_ns
        mres_dict_ts = mres_dict_ns

    n_gal_ts = len(sub_dict_ts["group_number"])

    if "Snap_"+str(snapshot_ns) not in File:
        print "No data, skipping"
        continue

    if snapshot_ns < snapshots[-1]:
        snapnum_group_ns = File["Snap_"+str(snapshot_ns)]
        subhalo_group_ns = snapnum_group_ns["subhalo_properties"]
    
        sub_dict_ns = {}
        for name in sub_names:
            sub_dict_ns[name] = subhalo_group_ns[name][:]


    ###### Integrate equations ##############

    sat = sub_dict_ts["subgroup_number"] > 0
    iI = sub_dict_ts["isInterpolated"] == 1
    evolve = (sat==False) & (iI==False)

    ########## Compute mass exchanges ##############

    # Compute smooth baronic accretion onto the subhalo
    fgrid = eff_dict["facc_pristine_nonorm"] + eff_dict["facc_reaccreted_nonorm"+fV_str] + eff_dict["facc_fof_nonorm"] + eff_dict["facc_reheated_nonorm"+fV_str] # Msun Gyr^-1

    mdot_cgm_acc = np.zeros(n_gal_ts)
    if interpolate_z:
        mdot_cgm_acc[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    else:
        mdot_cgm_acc[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    dm_cgm_acc = mdot_cgm_acc * dt

    # Compute smooth baryonic infall onto the ISM
    fgrid = eff_dict["fcool_pristine_nonorm"] + eff_dict["fcool_recooled_nonorm"+fV_str] + eff_dict["fcool_galtran_nonorm"] + eff_dict["fcool_reheated_nonorm"+fV_str] # Msun Gyr^-1

    mdot_ism_cool = np.zeros(n_gal_ts)
    if interpolate_z:
        mdot_ism_cool[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    else:
        mdot_ism_cool[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    dm_ism_cool = mdot_ism_cool * dt

    # Compute star formation
    fgrid = eff_dict["sfr_nonorm"]
    mdot_star_sfr = np.zeros(n_gal_ts)
    if interpolate_z:
        mdot_star_sfr[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    else:
        mdot_star_sfr[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    dm_star_sfr = mdot_star_sfr * dt

    # Compute outflows from ISM
    #fgrid = eff_dict["fwind_ism_nonorm"+fV_str] + eff_dict["fism_reheated_nonorm"+fV_str]
    fgrid = eff_dict["fism_out_tot_nonorm"]
    mdot_ism_out = np.zeros(n_gal_ts)
    if interpolate_z:
        mdot_ism_out[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    else:
        mdot_ism_out[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    dm_ism_out = mdot_ism_out * dt

    # Compute outflows from halo
    #fgrid = eff_dict["fwind_halo_nonorm"+fV_str] + eff_dict["fhalo_reheated_nonorm"+fV_str]
    fgrid = eff_dict["fhalo_out_tot_nonorm"]
    mdot_cgm_out = np.zeros(n_gal_ts)

    if interpolate_z:
        mdot_cgm_out[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    else:
        mdot_cgm_out[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
    dm_cgm_out = mdot_cgm_out * dt

    ######### Compute stellar recycling ########3

    if not disable_stellar_recycling and not instantaneous_recycling:

        # First update SFH arrays #
        sfh_progens = np.concatenate((sfh_progens, dm_star_sfr)) 
        nI_progens = np.concatenate((nI_progens, sub_dict_ts["node_index"]))
        dI_progens = np.concatenate((dI_progens, sub_dict_ts["descendant_index"]))

        # Assume stars formed on average at the halfway point between _ps and _ts
        t_progens = np.concatenate((t_progens, np.zeros_like(dm_star_sfr)+0.5*(t_ts+t_ps)))

        # For now I'm assuming that the metallicity of each galaxy is known
        Zstar_init_ts = np.zeros_like(dm_star_sfr)
        ok = sub_dict_ts["mZ_new_stars_init"] > 0
        Zstar_init_ts[ok] = sub_dict_ts["mZ_new_stars_init"][ok] / sub_dict_ts["mass_new_stars_init"][ok]
        Zstar_init_progens = np.concatenate((Zstar_init_progens, Zstar_init_ts))

        # Compute stellar mass loss for all progenitor points at _ts snapshot
        # Total mass loss from t=0 to _ts
        age_progens_ts = t_ts - t_progens

        mass_loss_ts = ml.compute_stellar_mass_loss( Zstar_init_progens, sfh_progens, age_progens_ts)
        # Total mass loss from t=0 to _ps
        age_progens_ps = t_ps - t_progens
        age_progens_ps[age_progens_ps<0] *= 0
        mass_loss_ps = ml.compute_stellar_mass_loss( Zstar_init_progens, sfh_progens, age_progens_ps)
   
        dm_star_rec_progens = mass_loss_ts - mass_loss_ps
   
        # Now sum together all progenitors (from all previous snapshot) of each _ts halo
        # First we need to reduce the range of numbers to allow floating point operations
        nI_progens_lite = np.arange(0,len(nI_progens))
        nI_ts_lite = np.zeros_like(sub_dict_ts["node_index"])
        ptr = ms.match(sub_dict_ts["node_index"], nI_progens)
        ok_match = ptr >= 0
        nI_ts_lite[ok_match] = nI_progens_lite[ptr][ok_match]

        dm_star_rec = mpf.Sum_Common_ID([dm_star_rec_progens], nI_progens_lite, nI_ts_lite)[0]


    elif not disable_stellar_recycling:
        dm_star_rec = dm_star_sfr * r_ira

    else:
        dm_star_rec = np.zeros_like(dm_cgm_out)

    ########## Apply limiters (using iterative approach) #################
    def limit_mass_exchange(loss_terms, gain_terms, mres):
        loss_sum = np.sum(loss_terms,axis=0)
        gain_sum = np.sum(gain_terms,axis=0)
        problem = loss_sum > mres + gain_sum
        rescale = loss_sum[problem] / (mres[problem] + gain_sum[problem])
        for term in loss_terms:
            term[problem] *= 1/(rescale* (1+tolerance))
        return len(problem[problem])

    need_limiters = True
    n_iter = 0
    while need_limiters:
        # CGM
        loss_terms = [dm_ism_cool, dm_cgm_out]
        gain_terms = [dm_cgm_acc, dm_ism_out]
        n_prob_cgm = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["cgm"])

        # ISM
        loss_terms = [dm_star_sfr, dm_ism_out]
        gain_terms = [dm_ism_cool, dm_star_rec]
        n_prob_ism = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["ism"])

        # Stars
        loss_terms = [dm_star_rec]
        gain_terms = [dm_star_sfr]
        n_prob_stars = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["stars"])
        
        # Ejected gas
        n_prob_ej = 0

        if n_prob_cgm == 0 and n_prob_ism == 0 and n_prob_stars == 0 and n_prob_ej == 0:
            need_limiters = False

        n_iter += 1
        if n_iter == 50:
            print "Warning: limiters are slow to converge"
        if n_iter > 1000:
            print "Error: limiters didn't converge after 1000 iterations"
            quit()
        
    ######### Increment masses ##############

    # accretion
    mres_dict_ts["cgm"] += dm_cgm_acc
    
    # cooling
    mres_dict_ts["cgm"] -= dm_ism_cool
    mres_dict_ts["ism"] += dm_ism_cool
    
    # star formation
    mres_dict_ts["ism"] -= dm_star_sfr
    mres_dict_ts["stars"] += dm_star_sfr

    # stellar recycling
    mres_dict_ts["stars"] -= dm_star_rec
    mres_dict_ts["ism"] += dm_star_rec

    # ISM outflow
    mres_dict_ts["ism"] -= dm_ism_out
    mres_dict_ts["cgm"] += dm_ism_out

    # Halo outflow
    mres_dict_ts["cgm"] -= dm_cgm_out

    if n_gal_ts > 0:
        if mres_dict_ts["ism"].min() < 0.0 or mres_dict_ts["cgm"].min() < 0.0 or mres_dict_ts["stars"].min() < 0.0 or mres_dict_ts["ej"].min() < 0.0:
            print "Error: something wen't wrong with the iterative limiters and at least one subhalo has negative baryonic mass"
            #quit()



    ######## Add mass to descendants ###########
    if snapshot_ns != snapshots[-1]:

        # To compute how many snapshots we've tracked each progenitor (n_snap), we need to find the oldest progenitor
        mres_dict_ts["n_snap"] = ifu.Keep_max_common_id(mres_dict_ts["n_snap"], sub_dict_ts["descendant_index"])

        # To run floating point operations (np.hist) on IDs, we need to reduce the range
        node_index_ns_lite = np.arange(0,len(sub_dict_ns["node_index"]))
        ptr = ms.match(sub_dict_ts["descendant_index"], sub_dict_ns["node_index"])
        ok_match = ptr >= 0
        descendant_index_ts_lite = node_index_ns_lite[ptr]
        descendant_index_ts_lite[ok_match==False] = -1


        if disable_mergers:
            # In this case, define "main" progenitor as the most massive (stellar mass) progenitor - set all other masses to zero
            mstar_mp = ifu.Keep_max_common_id( mres_dict_ts["stars"], sub_dict_ts["descendant_index"] )
            main_progen = mstar_mp > 0
            for name in mres_dict_ts:
                mres_dict_ts[name][main_progen==False] = 0.0

        # Sum subhalo masses from _ts that have a common descendant on _ns
        mres_dict_ns = mpf.Sum_Common_ID(mres_dict_ts, descendant_index_ts_lite, node_index_ns_lite,use_dict=True, dict_names=res_names)

        # Increment galaxies on _ns for which a progenitor was identified
        progenitor_found = mres_dict_ns["cgm"] > 0.0
        mres_dict_ns["n_snap"][progenitor_found] += 1

    if save_outputs:
        if z_outputs == "all" or snapshot in snap_outputs:
            if z_outputs != "all":
                print "Saving outputs for this snapshot"

            outsnap_list.append(snapshot)
            outres_list.append(mres_dict_ts)

            if save_rates:
                extra_outputs = ["dm_cgm_acc", "dm_ism_cool", "dm_star_sfr", "dm_star_rec", "dm_ism_out", "dm_cgm_out"]
                outres_rates.append([dm_cgm_acc, dm_ism_cool, dm_star_sfr, dm_star_rec, dm_ism_out, dm_cgm_out])
            else:
                extra_outputs = []

if save_outputs:
    # Note, output arrays should match those from processed_catalogue                                                                                                       
    print "writing to disk"

    # Delete existing hdf5 file if one exists before writing a new one                                                                                                      
    if test_mode:
        filename = "test_integrator_decoupled_output_"+sim_name
    else:
        filename = "integrator_decoupled_output_"+sim_name

    filename += ".hdf5"

    if os.path.isfile(output_path+filename):
        os.remove(output_path+filename)

    File_out = h5py.File(output_path+filename)

    for i_snap, snap_out in enumerate(outsnap_list):

        snap_group = File_out.create_group("Snap_"+str(snap_out))

        for name in res_names:
            snap_group.create_dataset(name, data=outres_list[i_snap][name])
        for i_name, name in enumerate(extra_outputs):
            snap_group.create_dataset(name, data=outres_rates[i_snap][i_name])

    File_out.close()





ok = (sub_dict_ts["subgroup_number"]==0) & (sub_dict_ts["isInterpolated"]==0)
ok2 = (sub_dict_ts["group_number"] == grn_select) & (sub_dict_ts["subgroup_number"] == sgrn_select)

from utilities_plotting import *

py.subplot(311)
mcgm = sub_dict_ts["mass_gas_NSF"] - sub_dict_ts["mass_gas_NSF_ISM"]
py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["cgm"][ok]/mcgm[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
py.axhline(0.0,c="k",alpha=0.3)

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["cgm"][ok]/mcgm[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
py.plot(mid_bin, med, c="k")
py.plot(mid_bin, lo, c="k")
py.plot(mid_bin, hi, c="k")

mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mcgm[ok],mres_dict_ts["cgm"][ok])
py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["cgm"][ok2] / mcgm[ok2]))

py.ylabel(r"$\log_{10}(M_{\mathrm{CGM}} \, / M_{\mathrm{CGM,true}})$")

py.subplot(312)
mism = sub_dict_ts["mass_gas_SF"] + sub_dict_ts["mass_gas_NSF_ISM"]
py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["ism"][ok]/mism[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
py.axhline(0.0,c="k",alpha=0.3)

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["ism"][ok]/mism[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
py.plot(mid_bin, med, c="k")
py.plot(mid_bin, lo, c="k")
py.plot(mid_bin, hi, c="k")

mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mism[ok],mres_dict_ts["ism"][ok])
py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

py.ylabel(r"$\log_{10}(M_{\mathrm{ISM}} \, / M_{\mathrm{ISM,true}})$")

py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["ism"][ok2] / mism[ok2]))

py.subplot(313)
#mstar_init = sub_dict_ts["mass_star_init"]
#py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["stars"][ok]/mstar_init[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")

mstar = sub_dict_ts["mass_star"]
py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["stars"][ok]/mstar[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
py.axhline(0.0,c="k",alpha=0.3)

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["stars"][ok]/mstar[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
py.plot(mid_bin, med, c="k")
py.plot(mid_bin, lo, c="k")
py.plot(mid_bin, hi, c="k")

mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mstar[ok],mres_dict_ts["stars"][ok])
py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

py.ylabel(r"$\log_{10}(M_{\mathrm{\star}} \, / M_{\mathrm{\star,true}})$")

py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["stars"][ok2] / mstar[ok2]))

py.show()

quit()

py.figure()

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mstar[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
py.plot(mid_bin, med, c="k")
py.plot(mid_bin, lo, c="k")
py.plot(mid_bin, hi, c="k")

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["stars"][ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
py.plot(mid_bin, med, c="r")
py.plot(mid_bin, lo, c="r")
py.plot(mid_bin, hi, c="r")

py.show()
