# The purpose of this script is to identify progenitors of selected haloes z=0, and then compute the "mass" of each tracer for each progenitor subhalo
# This is then used in the main integrator.py script (if tracers are enabled) to compute how many tracers each subhalo should spawn, given the smooth acc rate
# Note this strategy will miss particles that are not smoothly accreted (i.e. they first appear within a no-progenitor subhalo), but these should be subdominant

import numpy as np
import h5py
import match_searchsorted as ms
import os
import sys
sys.path.append("../.")
import measure_particles_functions as mpf

# Subhalo selection criteria
logm200_lo = 8.0
logm200_hi = 15.0
grn_lo = 36 # roughly 1e12 halo in 25 200
grn_hi = 36
#grn_lo = 204
#grn_hi = 204 # 1e11 halo in 25 200
sgrn_lo = 0
sgrn_hi = 0

catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

# 100 Mpc REF with 200 snips
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc REF with 200 snips
sim_name = "L0025N0376_REF_200_snip"


# Get snapshots/redshifts
filename = "subhalo_catalogue_" + sim_name + ".hdf5"
File = h5py.File(catalogue_path+filename,"r")
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation
File.close()

filename = "processed_subhalo_catalogue_" + sim_name + ".hdf5"
File = h5py.File(input_path+filename,"r")

filename_out = "ODE_integrator_tracer_pre-selection_" +sim_name+".hdf5"
if os.path.isfile(input_path+filename_out):
    print "Removing previous tracer pre-select file", input_path+filename_out
    os.remove(input_path+filename_out)
File_out = h5py.File(input_path+filename_out)

################ Loop over snapshots #######################
for i_snap in range(len(snapshots)):

    snap = snapshots[i_snap]
    snip = snipshots[i_snap]

    print "Processing snapshot", snap

    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue
    # No wind measurements on last snapshot
    if snap == snapshots.max():
        continue

    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]

    if "node_index" not in subhalo_group:
        print "No subhalo data for this snapshot, stopping here"
        quit()

    node_index_ts = subhalo_group["node_index"][:]
    m200_host = subhalo_group["m200_host"][:]

    if i_snap == 1:
        node_index_ns = node_index_ts

        grn = subhalo_group["group_number"][:]
        sgrn = subhalo_group["subgroup_number"][:]

        '''ok = sgrn==0
        a = np.argmin(abs(m200_host[ok]-1e11))
        print grn[ok][a]
        quit()'''

        select = (grn <= grn_hi) & (grn >= grn_lo) & (sgrn <= sgrn_hi) & (sgrn >= sgrn_lo) & (np.log10(m200_host) >= logm200_lo) & (np.log10(m200_host) <= logm200_hi)

        m200_host_descendant_z0 = np.copy(m200_host)
        
        m200_host_descendant_z0 = m200_host_descendant_z0[select]
        node_index_ns = node_index_ns[select]

        continue

    descendant_index_ts = subhalo_group["descendant_index"][:]
    ptr = ms.match(descendant_index_ts,node_index_ns)
    ok_match = ptr >= 0

    m200_host_descendant_ts = np.zeros_like(m200_host)
    m200_host_descendant_ts[ok_match] = m200_host_descendant_z0[ptr][ok_match]

    node_index_ns = node_index_ts[ok_match]
    m200_host_descendant_z0 = m200_host_descendant_z0[ptr][ok_match]

    if len(node_index_ns) == 0:
        print "No progentiors remain for the selected z=0 haloes, stopping here"
        quit()

    #print np.unique(m200_host_descendant_ts[ok_match], return_counts=True)

    snapnum_group_out = File_out.create_group("Snap_"+str(snap))
    snapnum_group_out.create_dataset("m200_host_descendant_z0", data=m200_host_descendant_ts)
