import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import utilities_statistics as us
import integrator_functions as ifu
import utilities_interpolation as ui
import os
import sys

test_mode = False
fV_str = "_0p25vmax"

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

z_choose = 1.26
#z_choose = 1.0
#z_choose = 0.87
# These three redshifts are the (only) 3 values from a single z_bin (for 28 snap)

sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

######### Load snapshot/snipshot information ###############

filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(input_path+filename_subcat,"r")

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshots = tree_snapshot_info["snapshots_tree"][:][::-1]
z_snapshots = tree_snapshot_info["redshifts_tree"][:][::-1]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
snipshots = tree_snapshot_info["sn_i_a_pshots_simulation"][:][::-1]

File.close()

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
file_name = input_path+"/Processed_Catalogues/efficiencies_grid_"+sim_name+"_integrator"
if test_mode:
    file_name += "_test_mode"
file_name += ".hdf5"

file_grid = h5py.File(file_name,"r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

z_min = 1/a_max -1
z_max = 1/a_min -1

eff_names = ["facc_pristine_nonorm", "facc_reaccreted_nonorm"+fV_str, "facc_fof_nonorm", "facc_reheated_nonorm"+fV_str]
eff_names+= ["fcool_pristine_nonorm", "fcool_recooled_nonorm"+fV_str, "fcool_galtran_nonorm", "fcool_reheated_nonorm"+fV_str]
eff_names += ["fwind_ism_nonorm"+fV_str, "fwind_halo_nonorm"+fV_str]
eff_names += ["fism_reheated_nonorm"+fV_str, "fhalo_reheated_nonorm"+fV_str]
eff_names += ["sfr_nonorm"]
eff_names += ["facc_tot_nonorm"]

eff_dict = {}
for name in eff_names:
    eff_dict[name] = file_grid[name][:]

file_grid.close()

# Open file of subhalo information
filename = "Processed_Catalogues/processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

i_snap = np.argmin(abs(z_snapshots-z_choose))
snapshot = snapshots[i_snap]
 
# Compute timestep
z_ts = z_snapshots[i_snap]
a_ts = 1/(1+z_ts)
t_ts = t_snapshots[i_snap]
t_ps = t_snapshots[i_snap-1]

dt = t_ts - t_ps

# Identify closest grid timesteps
lower = t_grid <= t_ts
if len(t_grid[lower]) == 0:
    ind_t_lo = -1
else:
    t_lo = t_grid[lower][np.argmin(abs(t_grid[lower]-t_ts))]
    ind_t_lo = np.argmin(abs(t_grid-t_lo))

higher = t_grid >= t_ts
if len(t_grid[higher]) == 0:
    ind_t_hi = 0
else:
    t_hi = t_grid[higher][np.argmin(abs(t_grid[higher]-t_ts))]
    ind_t_hi = np.argmin(abs(t_grid-t_hi))

##### Load relevant subhalo data #####
sub_names = ["group_number","subgroup_number","node_index","descendant_index","mchalo","m200_host"]
sub_names += ["isInterpolated", "mass_new_stars_init", "mZ_new_stars_init"]
sub_names += ["mass_star","mass_star_init","mass_gas_NSF", "mass_gas_NSF_ISM", "mass_gas_SF"]

sub_names += ["mass_sf_ism_join_wind_0p25vmax", "mass_nsf_ism_join_wind_0p25vmax", "mass_ism_reheated_join_wind_0p25vmax"]
sub_names += ["mass_galtran", "mass_prcooled", "mass_recooled_0p25vmax"]
sub_names += ["mass_sf_ism_reheated", "mass_nsf_ism_reheated","mass_recooled_ireheat","mass_recooled_0p5vmax"]
sub_names += ["mass_praccreted", "mass_reaccreted_0p25vmax", "mass_fof_transfer"]
sub_names += ["mass_join_halo_wind_0p25vmax", "mass_halo_reheated_join_wind_0p25vmax"]
sub_names += ["mass_reaccreted_0p5vmax", "mass_halo_reheated", "mass_reaccreted_hreheat"]
sub_names += ["mass_diffuse_dm_accretion"]

snapnum_group = File["Snap_"+str(snapshot)]
subhalo_group = snapnum_group["subhalo_properties"]

sub_dict_ts = {}
for name in sub_names:
    sub_dict_ts[name] = subhalo_group[name][:]

    
n_gal_ts = len(sub_dict_ts["group_number"])

'''#??????????????????????????
cgm_inflow = sub_dict_ts["mass_praccreted"] + sub_dict_ts["mass_reaccreted_0p5vmax"] + sub_dict_ts["mass_fof_transfer"] + sub_dict_ts["mass_reaccreted_hreheat"]

snapnum_group2 = File["Snap_"+str(snapshot+1)]
subhalo_group2 = snapnum_group2["subhalo_properties"]
sub_dict_ts2 = {}
for name in sub_names:
    sub_dict_ts2[name] = subhalo_group2[name][:]
cgm_inflow2 = sub_dict_ts2["mass_praccreted"] + sub_dict_ts2["mass_reaccreted_0p5vmax"] + sub_dict_ts2["mass_fof_transfer"] + sub_dict_ts2["mass_reaccreted_hreheat"]

snapnum_group3 = File["Snap_"+str(snapshot+2)]
subhalo_group3 = snapnum_group3["subhalo_properties"]
sub_dict_ts3 = {}
for name in sub_names:
    sub_dict_ts3[name] = subhalo_group3[name][:]
cgm_inflow3 = sub_dict_ts3["mass_praccreted"] + sub_dict_ts3["mass_reaccreted_0p5vmax"] + sub_dict_ts3["mass_fof_transfer"] + sub_dict_ts3["mass_reaccreted_hreheat"]

ok_measure = ((sub_dict_ts["mass_gas_NSF"]+sub_dict_ts["mass_gas_SF"]+sub_dict_ts["mass_star"]) >= 0)# & (np.isnan(sub_dict_ts["mass_halo_reheated_join_wind"+fV_str])==False)
ok_measure2 = ((sub_dict_ts2["mass_gas_NSF"]+sub_dict_ts2["mass_gas_SF"]+sub_dict_ts2["mass_star"]) >= 0)# & (np.isnan(sub_dict_ts2["mass_halo_reheated_join_wind"+fV_str])==False)
ok_measure3 = ((sub_dict_ts3["mass_gas_NSF"]+sub_dict_ts3["mass_gas_SF"]+sub_dict_ts3["mass_star"]) >= 0)# & (np.isnan(sub_dict_ts3["mass_halo_reheated_join_wind"+fV_str])==False)

print np.concatenate((cgm_inflow[ok_measure], cgm_inflow2[ok_measure2], cgm_inflow3[ok_measure3])).shape
cgm_inflow = np.concatenate((cgm_inflow[ok_measure], cgm_inflow2[ok_measure2], cgm_inflow3[ok_measure3]))
print np.unique(cgm_inflow, return_counts=True)

dt2 = t_snapshots[i_snap+1] - t_ts
dt3 = t_snapshots[i_snap+2] - t_snapshots[i_snap+1]
print dt, dt2, dt3

print dt, z_snapshots[i_snap], t_snapshots[i_snap]
print dt2, z_snapshots[i_snap+1], t_snapshots[i_snap+1]
print dt3, z_snapshots[i_snap+2], t_snapshots[i_snap+2]


quit()'''


###### Integrate equations ##############

sat = sub_dict_ts["subgroup_number"] > 0
iI = sub_dict_ts["isInterpolated"] == 1
evolve = (sat==False) & (iI==False)

########## Compute mass exchanges ##############

# Compute smooth baronic accretion onto the subhalo
#fgrid = eff_dict["facc_pristine_nonorm"] + eff_dict["facc_reaccreted_nonorm"+fV_str] + eff_dict["facc_fof_nonorm"] + eff_dict["facc_reheated_nonorm"+fV_str] # Msun Gyr^-1
fgrid = eff_dict["facc_tot_nonorm"] # Msun Gyr^-1


mdot_cgm_acc = np.zeros(n_gal_ts)
#mdot_cgm_acc[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
#                                                  np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
mdot_cgm_acc[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
dm_cgm_acc = mdot_cgm_acc * dt

# Compute smooth baryonic infall onto the ISM
fgrid = eff_dict["fcool_pristine_nonorm"] + eff_dict["fcool_recooled_nonorm"+fV_str] + eff_dict["fcool_galtran_nonorm"] + eff_dict["fcool_reheated_nonorm"+fV_str] # Msun Gyr^-1

mdot_ism_cool = np.zeros(n_gal_ts)
#mdot_ism_cool[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
#                                               np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
mdot_ism_cool[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
dm_ism_cool = mdot_ism_cool * dt

# Compute star formation
fgrid = eff_dict["sfr_nonorm"]
mdot_star_sfr = np.zeros(n_gal_ts)
#mdot_star_sfr[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
#                                               np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
mdot_star_sfr[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
dm_star_sfr = mdot_star_sfr * dt

# Compute outflows from ISM
fgrid = eff_dict["fwind_ism_nonorm"+fV_str] + eff_dict["fism_reheated_nonorm"+fV_str]
mdot_ism_out = np.zeros(n_gal_ts)
#mdot_ism_out[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
#                                                   np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
mdot_ism_out[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
dm_ism_out = mdot_ism_out * dt

# Compute outflows from halo
fgrid = eff_dict["fwind_halo_nonorm"+fV_str] + eff_dict["fhalo_reheated_nonorm"+fV_str]
mdot_cgm_out = np.zeros(n_gal_ts)
#mdot_cgm_out[evolve] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
#                                                   np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
mdot_cgm_out[evolve] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve]), bin_mh_mid)
dm_cgm_out = mdot_cgm_out * dt


'''cgm_inflow = sub_dict_ts["mass_praccreted"] + sub_dict_ts["mass_reaccreted_0p5vmax"] + sub_dict_ts["mass_fof_transfer"] + sub_dict_ts["mass_reaccreted_hreheat"]

mass_out = sub_dict_ts["mass_reaccreted_hreheat"] - (sub_dict_ts["mass_reaccreted"+fV_str]-sub_dict_ts["mass_reaccreted_0p5vmax"])
mass_out += sub_dict_ts["mass_fof_transfer"]
mass_out += sub_dict_ts["mass_reaccreted"+fV_str]
mass_out += sub_dict_ts["mass_praccreted"]

print np.unique(mass_out,return_counts=True)
print np.unique(cgm_inflow,return_counts=True)
quit()'''

########### Plotting ###################
ok = (sub_dict_ts["subgroup_number"]==0) & (sub_dict_ts["isInterpolated"]==0)

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*3,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':6})

# CGM inflow
py.subplot(231)

cgm_inflow = sub_dict_ts["mass_praccreted"] + sub_dict_ts["mass_reaccreted_0p5vmax"] + sub_dict_ts["mass_fof_transfer"] + sub_dict_ts["mass_reaccreted_hreheat"]

med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(dm_cgm_acc[ok] / cgm_inflow[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)

bin_mh = np.arange(9.0, 13.0,0.2)
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])
mean = np.zeros_like(bin_mh_mid)
for i in range(len(bin_mh_mid)):
    ok_i = ok & (np.log10(sub_dict_ts["m200_host"]) > bin_mh[i]) & (np.log10(sub_dict_ts["m200_host"]) < bin_mh[i+1])
    mean[i] = np.mean(dm_cgm_acc[ok_i]) / np.mean(cgm_inflow[ok_i])

py.plot(mid_bin, med, c="k",linewidth=1.2)
py.plot(mid_bin, lo, c="k",alpha=0.7)
py.plot(mid_bin, hi, c="k",alpha=0.7)
py.axhline(0.0,c="k", alpha=0.3,linestyle='--')

py.plot(bin_mh_mid, np.log10(mean), c="k", alpha=0.7, linestyle=':')

py.show()
quit()



