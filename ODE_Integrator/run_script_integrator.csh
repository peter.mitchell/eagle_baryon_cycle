#! /bin/tcsh -f

if ( ! ( $?datapath ) ) then
    # Get command line args
    echo 'number of arguments=' $#argv
    if( $#argv == 1) then
	set arg = $1
    endif
endif

echo run_script_integrator.csh - arguments are $arg

python integrator.py -arg $arg
