import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import utilities_statistics as us
import match_searchsorted as ms
import integrator_functions as ifu
import os
import time
import sys
sys.path.append("../.")
import measure_particles_functions as mpf
sys.path.append("/cosma/home/dphlss/d72fqv/Baryon_Cycle/Analysis_SHM/Stellar_Mass_Loss/.")
import stellar_mass_loss as ml
import argparse

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0752_Recal"

parser = argparse.ArgumentParser()
parser.add_argument("-arg", help = "subvolume",type=float)

if len(sys.argv)==1:
    pass
elif len(sys.argv)!=3:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()
arg = args.arg

variation = "" # Fiducial case

#variation = "_no_prev_cgm"
#variation = "_fixed_tau_infall"
#variation = "_fixed_inflows"
#variation = "_fixed_tau_ret_gal"
#variation = "_fixed_tau_ret_halo"
#variation = "_fixed_tau_ret_both"
#variation = "_fixed_eta_gal"
#variation = "_fixed_eta_halo"
#variation = "_fixed_eta_both"
#variation = "_fixed_tau_SF"
#variation = "_fixed_everything"
#variation = "_fixed_everything_deluxe"

#variation = "_only_prev_cgm"
#variation = "_only_tau_infall"
#variation = "_only_inflows"
#variation = "_only_tau_ret_gal"
#variation = "_only_tau_ret_halo"
#variation = "_only_tau_ret_both"
#variation = "_only_tau_SF"
#variation = "_only_eta_gal"
#variation = "_only_eta_halo"
#variation = "_only_eta_both"

#variation = "_no_ism_rec"
#variation = "_no_cgm_rec"
#variation = "_no_rec"
#variation = "_no_ism_out"
#variation = "_no_cgm_out"
#variation = "_no_out"
#variation = "_no_out_no_rec" # Sanity check - should be same as no_out!

##### NOTE TO SELF - CHECK ALL SUBMITTED JOBS ARE RUNNING BEFORE CHANGING TO NEXT VARIATION!! #############
# Note, I automatically turn off nuisance_terms for all of these
#variation = "_boost_eta_gal_"
#variation = "_boost_eta_halo_"
#variation = "_boost_tau_ret_gal_"
#variation = "_boost_tau_ret_halo_"
#variation = "_boost_prev_fb_"
#variation = "_boost_tau_infall_"
#variation = "_boost_tau_SF_"
#variation = "_no_nuisance" # This is just the fiducial model (no arg needed), but with nuisance terms always turned off

#variation = "_boost_tau_infall_tau_ret_halo_"
#variation = "_boost_tau_ret_"
#variation = "_boost_inflows_"

#variation = "_no_evo_eta_gal"
#variation = "_no_evo_eta_halo"
#variation = "_no_evo_tau_ret_gal"
#variation = "_no_evo_tau_ret_halo"
#variation = "_no_evo_prev_fb"
#variation = "_no_evo_tau_infall"
#variation = "_no_evo_tau_SF"
#variation = "_no_evo_all"
#variation = "_no_evo_all_no_srec"
#variation = "_no_evo_all_inc_srec"

#variation = "_evo_just_eta_gal"
#variation = "_evo_just_eta_halo"
#variation = "_evo_just_tau_ret_gal"
#variation = "_evo_just_tau_ret_halo"
#variation = "_evo_just_prev_fb"
#variation = "_evo_just_tau_infall"
#variation = "_evo_just_tau_SF"

#variation = "_no_evo2_tau_ret_gal" # This is as _no_evo, but without rescaling by the Hubble time
#variation = "_no_evo2_tau_ret_halo"
#variation = "_no_evo2_tau_infall"
#variation = "_no_evo2_tau_SF"
#variation = "_no_evo2_all"

#variation = "_sam_cooling"

try:
    variation
except:
    print "Need to set the variation"
    quit()


# Choose halo mass to use each varied efficiency from the above options (at a given redshift)
if "_only_" in variation or "_fixed_" in variation or variation == "_no_prev_cgm":
    if arg is None:
        anchor_mass = 1e12
        print "No argument passed in, using anchor mass=", anchor_mass
    else:
        anchor_mass = 10**arg
        print "Argument passed in, using anchor mass=", anchor_mass

if "_boost_" in variation:
    if arg is None:
        print "Error: no argument passed in for boost factor"
        quit()
    else:
        boost_factor = arg
        print "Argument passed in, using boost factor=", boost_factor
        boost_str = str(boost_factor).replace(".","p")
        variation += boost_str
else:
    boost_str = ""

if "L01" in sim_name:
    verbose = True
else:
    verbose = False

# Decide how to eject/reaccrete wind/non-wind "particles" from/onto CGM
#bias_mode = "none"
#bias_mode = "bias1"
bias_mode = "bias2"

# Valid options include
# 1) bias_mode = "none": Here it assumed that cgm wind particles have an equal probability to be ejected (reaccreted) relative to cgm non-wind particles
# 2) bias_mode = "bias1": Here I effectively compute from Eagle a separate eta_halo for cgm_wind/cgm_pr (b_wind =  CGM_out_wind/CGM_out)
# This means increasing eta_gal while holding eta_halo constant would not affect cgm_out_wind/cgm_out_pr (unless limiters are in effect)
# 3) bias_mode = "bias2": Here I compute from Eagle the excess probability (b_wind = (CGM_out,wind / CGM_out,pr) x (m_cgm_pr / m_cgm_wind)) 
# that wind particles are ejected relative to cgm_pr particles.
# This means increasing eta_gal while holding eta_halo constant would increase cgm_out_wind and decrease cgm_out_pr (holding cgm_out constant)
# This (for now) is my preferred option. Note that bias1 and bias2 should be equivalent for the reference model, they only influence (relative to each other) model variations

# Decide whether to do a Monte Carlo implementation of the Eqns
do_tracers = True
N_tr_des = 10000 # Number of tracers desired for each selected z=0 subhalo

plot_residuals = True
plot_mstar_mhalo = False
# Highlight a particular galaxy in diagnostic plots
grn_select = 1; sgrn_select = 0

if arg is not None:
    plot_residuals=False
    plot_mstar_mhalo=False

# If enabled, compute the stellar mass formed in the main progenitor object (both in the simulation and in the integrator)
compute_main_progenitor = False

# If enabled, compute the stellar mass formed in the central (not satellite) progenitors (both in the simulation and in the integrator)
compute_central_stellar_mass = False

# If enabled, count the cumulative first-time DM accretion for each halo
compute_dm_cumulative_acc = True

test_mode = False
fV_str = "_0p25vmax"

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"


save_outputs = True
if "L0025" in sim_name:
    z_outputs = "all"
elif "L0100" in sim_name:
    z_outputs = [0.0, 0.5, 1.0, 2.0, 3.0]
save_rates = True
output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/"

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

# Force true values
force_true_sfr = False

# Allow satellites to evolve like centrals, but only for first-time gas infall (from CGM to ISM), SF, and ISM outflows
evolve_sats = True
# In this case, set the SFR of satellites to their actual values
use_sat_sfr = True

# If enabled, correct galaxy reservoir masses for unaccounted for gain/loss terms
use_nuisance_terms = True

if variation == "_no_nuisance" or "boost" in variation:
    print "Nuisance terms are turned off for variation=",variation
    use_nuisance_terms = False

if use_nuisance_terms:
    use_sat_sfr = True
    evolve_sats = False

# numerical parameters
use_limiters = True
tolerance = 1e-6

interpolate_z = False # Choose whether to interpolate between z_grid points (last-checked 7/9/20, seems to make no difference)
disable_mergers = False # If True, only main progenitor (defined via stellar mass) is propagated down the tree
disable_stellar_recycling = False # If True, turn off stellar mass loss
instantaneous_recycling = False
r_ira = 0.4
tabulated_recycling = True # If true, use pre-tabuled non-instanteous recycling
if tabulated_recycling:
    instantaneous_recycling = False

if variation == "_no_evo_all_no_srec":
    print ""
    print "Turning off recycling for", variation
    print ""
    instantaneous_recycling = False
    tabulated_recycling = False
    disable_stellar_recycling = True
    #use_nuisance_terms = False

if "Recal" in sim_name:
    instantaneous_recycling = True

# Store options for output file
cosmo = {}; cosmo["omm"] = omm; cosmo["oml"] = oml; cosmo["h"] = h; cosmo["h"] = 0.6777; cosmo["omb"] = omb; cosmo["fb"] = fb; cosmo["dm2tot_factor"] = dm2tot_factor
options = {}
options["cosmo"] = cosmo; options["interpolate_z"] = interpolate_z
options["disable_mergers"] = disable_mergers
options["disable_stellar_recycling"] = disable_stellar_recycling
options["instantaneous_recycling"] = instantaneous_recycling; options["r_ira"] = r_ira
options["tabulated_recycling"] = tabulated_recycling
options["use_limiters"] = use_limiters; options["tolerance"] = tolerance
options["use_nuisance_terms"] = use_nuisance_terms
options["force_true_sfr"] = force_true_sfr; options["evolve_sats"] = evolve_sats; options["use_sat_sfr"] = use_sat_sfr
options["bias_mode"] = bias_mode

options["fV_str"] = fV_str
options["test_mode"] = test_mode

######### Load snapshot/snipshot information ###############

filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if "L0025N0376_REF_200_snip" in sim_name:
    filename_subcat = "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"
else:
    filename_subcat = filename

File = h5py.File(input_path+filename_subcat,"r")

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshots = tree_snapshot_info["snapshots_tree"][:][::-1]
z_snapshots = tree_snapshot_info["redshifts_tree"][:][::-1]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
snipshots = tree_snapshot_info["sn_i_a_pshots_simulation"][:][::-1]

File.close()

if save_outputs and z_outputs != "all":
    snap_outputs = []
    for z in z_outputs:
        snap_outputs.append( snapshots[1:-1][np.argmin(abs(z-z_snapshots[1:-1]))] )
        
########### Load tabulated efficiencies #########################

file_name = input_path+"/Processed_Catalogues/efficiencies_grid_"+sim_name+"_integrator"
if test_mode:
    file_name += "_test_mode"
file_name += ".hdf5"

file_grid = h5py.File(file_name,"r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

eff_names = []

if "Recal" not in sim_name:
    eff_names += ["frec_star_sfr_norm"]

eff_names += ["facc_pristine_prevent", 'fcool_pristine_mcgm_norm', "sfr_mism_norm", "ism_wind_tot_ml"+fV_str, "halo_wind_tot_ml"+fV_str]
eff_names += ["ism_reheat_ml"+fV_str, "halo_reheat_ml"+fV_str, "fcool_pristine_mpristine_norm"]
eff_names += ["fcool_recooled_mcgm_norm"+fV_str, "fcool_galtran_mcgm_norm", "fcool_reheated_mcgm_norm"+fV_str]
eff_names += ["facc_reaccreted_return"+fV_str,"fcool_recooled_return"+fV_str]

eff_names += ["facc_fof_prevent"]
eff_names += ["fism_reheated_mism_norm"+fV_str, "fhalo_reheated_mcgm_norm"+fV_str]
eff_names += ["fcool_reheated_mism_norm"+fV_str, "facc_reheated_mcgm_norm"+fV_str]
eff_names += ["fcool_pristine_mcgm_nowind_norm"+fV_str, "fcool_galtran_mcgm_nowind_norm"+fV_str]

if sim_name == "L0025N0376_REF_200_snip":
    eff_names += ["bias_cgm_out"+fV_str, "bias_reacc"+fV_str,"bias_cgm_out2"+fV_str, "bias_reacc2"+fV_str]

eff_dict = {}
for name in eff_names:
    eff_dict[name] = file_grid[name][:]

file_grid.close()

######## I added measurements of bias parameters later on, this is a stopgap ##############
if sim_name !=  "L0025N0376_REF_200_snip":
    file_name = input_path+"/Processed_Catalogues/efficiencies_grid_"+"L0025N0376_REF_200_snip"+"_integrator"
    if test_mode:
        file_name += "_test_mode"
    file_name += ".hdf5"

    file_grid = h5py.File(file_name,"r")

    # Check this is consistent with the main grid
    if (abs(file_grid["log_msub_grid"][:] - bin_mh_mid)).min() > 0.0:
        print "Error: bias grid doesn't allign with main eff grid"
        quit()

    if (abs(file_grid["a_grid"][:] - a_grid)).min() > 0.0001:
        print file_grid["a_grid"][:]
        print a_grid
        print "Error: bias grid doesn't allign with main eff grid"
        quit()

    eff_names = ["bias_cgm_out"+fV_str, "bias_reacc"+fV_str,"bias_cgm_out2"+fV_str, "bias_reacc2"+fV_str]

    for name in eff_names:
        eff_dict[name] = file_grid[name][:]

    file_grid.close()
########################################################################################

'''from utilities_plotting import *
for iz in range(len(z_grid)):
    #py.plot(bin_mh_mid, eff_dict["bias_cgm_out"+fV_str][iz])
    #py.plot(bin_mh_mid, eff_dict["bias_cgm_out2"+fV_str][iz])
    
    #py.plot(bin_mh_mid, eff_dict["bias_reacc"+fV_str][iz])
    py.plot(bin_mh_mid, eff_dict["bias_reacc2"+fV_str][iz])
    
py.show()
quit()'''


################### Implement model variations (when possible) #######################

# Preventative feedback (virial radius)
fix_prev = False
if variation == "_no_prev_cgm" or variation == "_fixed_everything" or variation == "_fixed_inflows" or variation == "_fixed_everything_deluxe":
    fix_prev = True
elif "_only_" in variation and variation != "_only_prev_cgm" and variation != "_only_inflows":
    fix_prev = True

fix_names = []

if fix_prev:
    eff_dict["facc_pristine_prevent"] = np.ones_like(eff_dict["facc_pristine_prevent"])
    print "Disabling preventative feedback at the virial radius"

    # also choose to fix fof transfer to anchor mass value                                                                                                        
    fix_names.append("facc_fof_prevent")

# Recycling (virial radius)
if variation == "_fixed_tau_ret_halo" or variation == "_fixed_tau_ret_both" or variation == "_fixed_everything" or variation == "_fixed_everything_deluxe":
    fix_names.append("facc_reaccreted_return"+fV_str)
elif "_only_" in variation and variation != "_only_tau_ret_halo" and variation != "_only_tau_ret_both":
    fix_names.append("facc_reaccreted_return"+fV_str)

# CGM 1st-time infall
if variation == "_fixed_tau_infall" or variation == "_fixed_everything" or variation == "_fixed_inflows" or variation == "_fixed_everything_deluxe":
    fix_names.append("fcool_pristine_mcgm_nowind_norm"+fV_str)
    fix_names.append("fcool_galtran_mcgm_nowind_norm"+fV_str)
elif "_only_" in variation and variation != "_only_tau_infall" and variation != "_only_inflows":
    fix_names.append("fcool_pristine_mcgm_nowind_norm"+fV_str)
    fix_names.append("fcool_galtran_mcgm_nowind_norm"+fV_str)

# Recycling (ISM)
fix_rec_ism = False
if variation == "_fixed_tau_ret_gal" or variation == "_fixed_tau_ret_both" or variation == "_fixed_everything" or variation == "_fixed_everything_deluxe":
    fix_rec_ism = True
elif "_only_" in variation and variation != "_only_tau_ret_gal" and variation != "_only_tau_ret_both":
    fix_rec_ism = True

if fix_rec_ism:
    fix_names.append("fcool_recooled_return"+fV_str)
    
# Star formation
if variation == "_fixed_tau_SF" or variation == "_fixed_everything" or variation == "_fixed_everything_deluxe":
    fix_names.append("sfr_mism_norm")
elif "_only_" in variation and variation != "_only_tau_SF":
    fix_names.append("sfr_mism_norm")

# Outflows (ISM)
if variation == "_fixed_eta_gal" or variation == "_fixed_eta_both" or variation == "_fixed_everything" or variation == "_fixed_everything_deluxe":
    fix_names.append("ism_wind_tot_ml"+fV_str)
elif "_only_" in variation and variation != "_only_eta_gal" and variation != "_only_eta_both":
    fix_names.append("ism_wind_tot_ml"+fV_str)

# Outflows (Halo)
if variation == "_fixed_eta_halo" or variation == "_fixed_eta_both" or variation == "_fixed_everything" or variation == "_fixed_everything_deluxe":
    fix_names.append("halo_wind_tot_ml"+fV_str)
elif "_only_" in variation and variation != "_only_eta_halo" and variation != "_only_eta_both":
    fix_names.append("halo_wind_tot_ml"+fV_str)

# For selected efficiencies, fix efficiency to the value at anchor halo mass for each redshift
for iz in range(len(t_grid)):
    for name in fix_names:
        if iz == 0:
            print "Fixing efficiency=", name, "to value at anchor mass", np.log10(anchor_mass)
        ok = (np.isnan(eff_dict[name][iz])==False) & (eff_dict[name][iz] != np.inf)
        i_anchor = np.argmin(abs(bin_mh_mid[ok]-np.log10(anchor_mass)))
        eff_dict[name][iz] = eff_dict[name][iz,ok][i_anchor]

off_names = []

if variation == "_no_ism_rec" or variation == "_no_rec" or variation == "_no_out_no_rec":
    off_names.append("fcool_recooled_return"+fV_str)
    
if variation == "_no_cgm_rec" or variation == "_no_rec" or variation == "_no_out_no_rec":
    off_names.append("facc_reaccreted_return"+fV_str)

if variation == "_no_ism_out" or variation == "_no_out" or variation == "_no_out_no_rec":
    off_names.append("ism_wind_tot_ml"+fV_str)

if variation == "_no_out" or variation == "_no_cgm_out" or variation == "_no_out_no_rec":
    off_names.append("halo_wind_tot_ml"+fV_str)

# For selected efficiencies, set to zero for all masses/redshifts
for name in off_names:
    print "Setting efficiency=", name, "to zero everywhere"
    eff_dict[name] *= 0

boost_names = []

if variation == "_boost_eta_gal_"+boost_str:
    boost_names.append("ism_wind_tot_ml"+fV_str)
if variation == "_boost_eta_halo_"+boost_str:
    boost_names.append("halo_wind_tot_ml"+fV_str)
if variation == "_boost_tau_ret_gal_"+boost_str or variation == "_boost_tau_ret_"+boost_str or variation == "_boost_inflows_"+boost_str:
    boost_names.append("fcool_recooled_return"+fV_str)
if variation == "_boost_tau_ret_halo_"+boost_str or variation == "_boost_tau_infall_tau_ret_halo_"+boost_str or variation == "_boost_tau_ret_"+boost_str or variation == "_boost_inflows_"+boost_str:
    boost_names.append("facc_reaccreted_return"+fV_str)
if variation == "_boost_prev_fb_"+boost_str:
    boost_names.append("facc_pristine_prevent")
    boost_names.append("facc_fof_prevent")
if variation == "_boost_tau_infall_"+boost_str or variation == "_boost_tau_infall_tau_ret_halo_"+boost_str or variation == "_boost_inflows_"+boost_str:
    boost_names.append("fcool_pristine_mcgm_nowind_norm"+fV_str)
    boost_names.append("fcool_galtran_mcgm_nowind_norm"+fV_str)
if variation == "_boost_tau_SF_"+boost_str:
    boost_names.append("sfr_mism_norm")

# For selected efficiencies, multiply by them by a fixed boost factor
for name in boost_names:

    # Cap preventative fb such that you can't get more than maximum value in each z bin
    if name == "facc_pristine_prevent" and boost_factor > 1:
        for iz in range(len(eff_dict[name][:,0])):
            ok = np.isnan(eff_dict[name][iz])==False
            max_value = np.max(eff_dict[name][iz][ok])
            eff_dict[name][iz] *= boost_factor
            capped = eff_dict[name][iz] > max_value
            eff_dict[name][iz][capped] = max_value
    elif name == "facc_fof_prevent" and boost_factor > 1: # This is a bit of a fudge, but I think it makes sense to just treat FoF exactly the same as pracc here
        for iz in range(len(eff_dict[name][:,0])):
            ok = np.isnan(eff_dict[name][iz])==False
            max_value = np.max(eff_dict[name][iz][ok])
            eff_dict[name][iz] *= boost_factor
            capped = eff_dict[name][iz] > max_value
            eff_dict[name][iz][capped] = max_value
    else:
        print "Boosting efficiency=", name, "by", boost_factor
        eff_dict[name] *= boost_factor

no_evo_names = []
tH_norm = []
if variation == "_no_evo_eta_gal" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_eta_gal")) or "_no_evo2_all" in variation:
    no_evo_names.append("ism_wind_tot_ml"+fV_str)
    tH_norm.append(False)
if variation == "_no_evo_eta_halo" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_eta_halo")) or "_no_evo2_all" in variation:
    no_evo_names.append("halo_wind_tot_ml"+fV_str)
    tH_norm.append(False)
if variation == "_no_evo_tau_ret_gal" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_tau_ret_gal")):
    no_evo_names.append("fcool_recooled_return"+fV_str)
    tH_norm.append(True)
if variation == "_no_evo2_tau_ret_gal" or "_no_evo2_all" in variation:
    no_evo_names.append("fcool_recooled_return"+fV_str)
    tH_norm.append(False)
if variation == "_no_evo_tau_ret_halo" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_tau_ret_halo")):
    no_evo_names.append("facc_reaccreted_return"+fV_str)
    tH_norm.append(True)
if variation == "_no_evo2_tau_ret_halo" or "_no_evo2_all" in variation:
    no_evo_names.append("facc_reaccreted_return"+fV_str)
    tH_norm.append(False)
if variation == "_no_evo_prev_fb" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_prev_fb")) or "_no_evo2_all" in variation:
    no_evo_names.append("facc_pristine_prevent")
    tH_norm.append(False)
    no_evo_names.append("facc_fof_prevent")
    tH_norm.append(False)
if variation == "_no_evo_tau_infall" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_tau_infall")):
    no_evo_names.append("fcool_pristine_mcgm_nowind_norm"+fV_str)
    tH_norm.append(True)
    no_evo_names.append("fcool_galtran_mcgm_nowind_norm"+fV_str)
    tH_norm.append(True)
if variation == "_no_evo2_tau_infall" or "_no_evo2_all" in variation:
    no_evo_names.append("fcool_pristine_mcgm_nowind_norm"+fV_str)
    tH_norm.append(False)
    no_evo_names.append("fcool_galtran_mcgm_nowind_norm"+fV_str)
    tH_norm.append(False)
if variation == "_no_evo_tau_SF" or "_no_evo_all" in variation or (("_evo_just" in variation) & (variation != "_evo_just_tau_SF")):
    no_evo_names.append("sfr_mism_norm")
    tH_norm.append(True)
if variation == "_no_evo2_tau_SF" or "_no_evo2_all" in variation:
    no_evo_names.append("sfr_mism_norm")
    tH_norm.append(False)
if variation == "_no_evo_all_inc_srec" in variation:
    no_evo_names.append("frec_star_sfr_norm")
    tH_norm.append(False)

# Set all efficiencies to their value at z_pivot (note nans will be extrapolated for higher mass galaxies at lower-z)
z_pivot = 1.0
iz_pivot = np.argmin(abs(z_pivot - z_grid))

for name, tH_norm_i in zip(no_evo_names, tH_norm):
    print "Removing redshift evolution from efficiency", name, "renorm by t_Hubble", tH_norm_i

    if tH_norm_i:
        for iz in range(len(eff_dict[name][:,0])):
            eff_dict[name][iz] = eff_dict[name][iz_pivot] * t_grid[iz_pivot] / t_grid[iz]
    else:
        for iz in range(len(eff_dict[name][:,0])):
            eff_dict[name][iz] = eff_dict[name][iz_pivot]


################################################################################################


# Initialise 1d array of SFH (each point will be a single progenitor at a single snapshot)
# This is used to compute the stellar mass loss of all progenitors of galaxy at a given step
sfh_progens = [] # Mass (before mass loss) of stars formed at a given snap
t_progens = [] # Time the stars formed
Zstar_init_progens = [] # Metal mass in particles that formed at a given snap
nI_progens = np.array([]).astype("int")
dI_progens = np.array([]).astype("int")

# Open file of subhalo information
filename = "Processed_Catalogues/processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

if do_tracers:
    filename_tr = "Processed_Catalogues/ODE_integrator_tracer_pre-selection_" +sim_name+".hdf5"
    File_tr = h5py.File(input_path+filename_tr,"r")

print "preliminaries done, model starting"

ts_init = False

outsnap_list = []
outres_list = []
outres_rates = []
if do_tracers:
    outtr_list = []
    outtr_snap_list = []

init_debug = False # Temp

for i_snap, snapshot in enumerate(snapshots[1:-1]):

    t_loop0 = time.time()
 
    snapshot_ns = snapshots[1:][i_snap+1]
 
    # Compute timestep
    z_ts = z_snapshots[1:][i_snap]; z_ns = z_snapshots[1:][i_snap+1]
    a_ts = 1/(1+z_ts); a_ns = 1/(1+z_ns)
    t_ts = t_snapshots[1:][i_snap]; t_ns = t_snapshots[1:][i_snap+1]
    t_ps = t_snapshots[i_snap]

    print "integrating snap",snapshot,"of",snapshots.max(), "redshift=", z_ts

    dt = t_ts - t_ps

    # Identify closest grid timesteps
    lower = t_grid <= t_ts
    if len(t_grid[lower]) == 0:
        ind_t_lo = -1
    else:
        t_lo = t_grid[lower][np.argmin(abs(t_grid[lower]-t_ts))]
        ind_t_lo = np.argmin(abs(t_grid-t_lo))

    higher = t_grid >= t_ts
    if len(t_grid[higher]) == 0:
        ind_t_hi = 0
    else:
        t_hi = t_grid[higher][np.argmin(abs(t_grid[higher]-t_ts))]
        ind_t_hi = np.argmin(abs(t_grid-t_hi))

    ##### Load relevant subhalo data #####
    t_loop1 = time.time()
    sub_names = ["group_number","subgroup_number","node_index","descendant_index","m200_host","mchalo"]
    sub_names += ["isInterpolated", "mass_new_stars_init", "mZ_new_stars_init"]
    sub_names += ["mass_diffuse_dm_pristine", "mass_reaccreted_0p5vmax", "mass_fof_transfer", "mass_reaccreted_hreheat"]
    sub_names += ["mass_reaccreted_0p25vmax", "mass_recooled_ireheat", "mass_recooled_0p25vmax", "mass_recooled_0p5vmax", "mass_galtran"]

    if plot_residuals or plot_mstar_mhalo:
        if snapshot == snapshots[-3] or use_nuisance_terms:
            sub_names += ["mass_star","mass_star_init","mass_gas_NSF", "mass_gas_NSF_ISM", "mass_gas_SF","mass_outside"+fV_str,"mass_outside_galaxy"+fV_str]

    elif use_nuisance_terms:
        sub_names += ["mass_star","mass_gas_NSF", "mass_gas_NSF_ISM", "mass_gas_SF","mass_outside"+fV_str,"mass_outside_galaxy"+fV_str]

    # Temp hack for debugging - can remove later
    if "mass_star" not in sub_names:
        sub_names += ["mass_star"]


    if use_nuisance_terms:
        sub_names += ["mass_praccreted","mass_prcooled","mass_nsf_ism_reheated","mass_sf_ism_reheated","mass_halo_reheated"]
        if "Recal" not in sim_name:
            sub_names += ["mass_stellar_rec"]
        sub_names += ["mass_join_halo_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str,"mass_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str]
        sub_names += ["mass_ism_reheated_join_wind"+fV_str,"mass_inside"+fV_str]

        # These measurements were only added recently (circa November 2020)
        if sim_name == "L0025N0376_REF_200_snip":
            sub_names += ["mass_reaccreted_from_wind_0p5vmax","mass_reaccreted_from_wind_0p25vmax"]
            sub_names += ["mass_join_halo_wind_from_ism"]

        # These are terms that are computed for satellites, but aren't used in this code.
        # These need to set to zero avoid being accidentally not being accounted for when we do the nuisance calculation.
        zero_sat_names = ["mass_halo_reheated", "mass_join_halo_wind"+fV_str, "mass_halo_reheated_join_wind"+fV_str]
        zero_sat_names+= ["mass_ism_reheated_join_wind"+fV_str, "mass_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str]
        zero_sat_names+= ["mass_nsf_ism_reheated","mass_sf_ism_reheated"]
        
    if not ts_init:
        if "Snap_"+str(snapshot) not in File:
            print "No data, skipping"
            continue

        snapnum_group = File["Snap_"+str(snapshot)]
        subhalo_group = snapnum_group["subhalo_properties"]

        if "node_index" not in subhalo_group:
            print "No data, skipping"; continue

        #print "temp hack_sel"
        #ok = (subhalo_group["mass_star"] > 0) | (subhalo_group["isInterpolated"]==1)

        sub_dict_ts = {}
        for name in sub_names:
            sub_dict_ts[name] = subhalo_group[name][:]#[ok]

        # Initialise baryon arrays for this snapshot (_ts)
        n_gal_ts = len(sub_dict_ts["group_number"])
        
        mres_dict_ts = {}
        res_names = ["cgm_wind","cgm_pr","ism","stars","ej","n_snap","ej_pr","ej_wind"]

        if use_nuisance_terms:
            res_names += ["cgm_wind_progen_actual","cgm_pr_progen_actual", "ism_progen_actual", "stars_progen_actual", "ej_pr_progen_actual", "ej_wind_progen_actual"]

        if compute_main_progenitor:
            res_names += ["stars_mp", "stars_mp_actual"]
        if compute_central_stellar_mass:
            res_names += ["stars_init", "stars_init_cent", "stars_init_actual", "stars_init_cent_actual"]
        if compute_dm_cumulative_acc:
            res_names += ["dm_first_acc"]

        for name in res_names:
            mres_dict_ts[name] = np.zeros(n_gal_ts)

        # Initiate tracer data
        if do_tracers:
            tracer_dict_ts = {}
            tracer_sub_names = ["group_number", "subgroup_number", "node_index", "descendant_index"]
            tracer_names = ["state"]

        ts_init = True
    else:
        sub_dict_ts = sub_dict_ns
        mres_dict_ts = mres_dict_ns
        
    if do_tracers:
        tr_group_str = "Snap_"+str(snapshot)
        if tr_group_str in File_tr:
            tracer_init_group = File_tr[tr_group_str]
            sub_dict_ts["m200_host_descendant_z0"] = tracer_init_group["m200_host_descendant_z0"][:]
        else:
            sub_dict_ts["m200_host_descendant_z0"] = np.zeros_like(sub_dict_ts["m200_host"])


    n_gal_ts = len(sub_dict_ts["group_number"])

    if "Snap_"+str(snapshot_ns) not in File or n_gal_ts == 0:
        print "No data, skipping"
        continue

    if snapshot_ns < snapshots[-1]:
        snapnum_group_ns = File["Snap_"+str(snapshot_ns)]
        subhalo_group_ns = snapnum_group_ns["subhalo_properties"]

        #print "temp hack_sel"
        #ok = (subhalo_group_ns["mass_star"][:] > 0) | (subhalo_group_ns["isInterpolated"][:]==1)

        sub_dict_ns = {}
        for name in sub_names:
            sub_dict_ns[name] = subhalo_group_ns[name][:]#[ok]

    ###### Integrate equations ##############
    t_loop2 = time.time()

    iI = sub_dict_ts["isInterpolated"] == 1
    sat = (sub_dict_ts["subgroup_number"] > 0) & (iI == False)
    if evolve_sats:
        evolve_all = (iI==False)
        evolve_cent = evolve_all & (sat==False)

        # Sometimes the maximum past halo mass is implausibly large for satellites, keep this in check
        problem = sat & (sub_dict_ts["mchalo"] > sub_dict_ts["m200_host"])
        sub_dict_ts["mchalo"][problem] = sub_dict_ts["m200_host"][problem] / dm2tot_factor

        # Set satellite halo mass to maximum past subhalo mass (dark matter - corrected)
        sub_dict_ts["m200_host"][sat] = sub_dict_ts["mchalo"][sat] * dm2tot_factor

        
    else:
        evolve_all = (sat==False) & (iI==False)
        evolve_cent = evolve_all

    ########## Compute mass exchanges ##############

    ############## CGM accretion ################################
    # Compute smooth 1st-time baronic accretion onto the subhalo
    fgrid = eff_dict["facc_pristine_prevent"] + eff_dict["facc_fof_prevent"]

    f_cgm_acc = np.zeros(n_gal_ts)
    if interpolate_z:
        f_cgm_acc[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_cgm_acc[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
        
    f_cgm_acc[evolve_cent] *= dm2tot_factor * fb * sub_dict_ts["mass_diffuse_dm_pristine"][evolve_cent] /dt # Msun Gyr^-1

    dm_cgm_pracc = f_cgm_acc * dt # Msun

    # Compute reaccretion
    fgrid = eff_dict["facc_reaccreted_return"+fV_str]

    f_cgm_acc = np.zeros(n_gal_ts)
    if interpolate_z:
        f_cgm_acc[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_cgm_acc[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    if bias_mode == "none":
        dm_cgm_reacc_wind = f_cgm_acc * dt * mres_dict_ts["ej_wind"] # Msun
        dm_cgm_reacc_pr = f_cgm_acc * dt * mres_dict_ts["ej_pr"] # Msun

    elif bias_mode == "bias1":
        fgrid = eff_dict["bias_reacc"+fV_str]
        b_reacc = np.zeros(n_gal_ts)
        if interpolate_z:
            b_reacc[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
        else:
            b_reacc[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

        dm_cgm_reacc_wind = f_cgm_acc * dt * (mres_dict_ts["ej_wind"]+mres_dict_ts["ej_pr"]) * b_reacc
        dm_cgm_reacc_pr = f_cgm_acc * dt * (mres_dict_ts["ej_wind"]+mres_dict_ts["ej_pr"]) * (1-b_reacc)

    elif bias_mode == "bias2":
        fgrid = eff_dict["bias_reacc2"+fV_str]
        b_reacc = np.zeros(n_gal_ts)
        if interpolate_z:
            b_reacc[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
        else:
            b_reacc[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

        dm_cgm_reacc = f_cgm_acc * dt * (mres_dict_ts["ej_wind"]+mres_dict_ts["ej_pr"])
        ok = mres_dict_ts["ej_wind"] + mres_dict_ts["ej_pr"] > 0
        dm_cgm_reacc_wind = np.copy(dm_cgm_reacc)
        dm_cgm_reacc_wind[ok] *= (b_reacc[ok] * mres_dict_ts["ej_wind"][ok]) / (b_reacc[ok] * mres_dict_ts["ej_wind"][ok] + mres_dict_ts["ej_pr"][ok])
        dm_cgm_reacc_pr = np.copy(dm_cgm_reacc)
        dm_cgm_reacc_pr[ok] *= mres_dict_ts["ej_pr"][ok] / (b_reacc[ok] * mres_dict_ts["ej_wind"][ok] + mres_dict_ts["ej_pr"][ok])
        
        prob = (b_reacc == 0) & (mres_dict_ts["ej_pr"] == 0)
        dm_cgm_reacc_pr[prob] = 0.0
        dm_cgm_reacc_wind[prob] = 0.0

    else:
        print "Error: bias_mode=",bias_mode,"is not recognised. Valid options (at time of writing) are 'none', 'bias1','bias2'"
        quit()

    prob = np.where(np.isnan(dm_cgm_reacc_pr))[0]
    if len(prob) > 0:
        print ""
        print dm_cgm_reacc_pr[prob][0]
        print b_reacc[prob]
        print mres_dict_ts["ej_wind"][prob], mres_dict_ts["ej_pr"][prob]
        quit()

    # Do halo reheat
    fgrid = eff_dict["facc_reheated_mcgm_norm"+fV_str]

    if "_deluxe" in variation:
        fgrid *= 0

    f_cgm_acc = np.zeros(n_gal_ts)
    if interpolate_z:
        f_cgm_acc[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                   np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_cgm_acc[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    dm_cgm_acc_hreheat_wind = f_cgm_acc * mres_dict_ts["cgm_wind"] * dt # Msun
    dm_cgm_acc_hreheat_pr = f_cgm_acc * mres_dict_ts["cgm_pr"] * dt # Msun
    
    ############## ISM accretion #####################################

    # Compute smooth 1st-time baryonic infall onto the ISM
    #fgrid = eff_dict["fcool_pristine_mcgm_norm"] # Note it would be better to use mpristine_norm - but I (currently) have no way to track pristine mass in CGM properly within the integrator
    #fgrid = eff_dict["fcool_pristine_mpristine_norm"]
    fgrid = eff_dict["fcool_pristine_mcgm_nowind_norm"+fV_str]

    f_ism_cool = np.zeros(n_gal_ts)
    if interpolate_z:
        f_ism_cool[evolve_all] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
    else:
        f_ism_cool[evolve_all] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
    
    #f_ism_cool *= mres_dict_ts["cgm"] # Msun Gyr^-1
    f_ism_cool *= mres_dict_ts["cgm_pr"] # Msun Gyr^-1

    if variation == "_sam_cooling":
        t_dyn = t_ts * 0.1
        mhalo_cut = 1e12
        f_ism_cool[evolve_all] = mres_dict_ts["cgm_pr"][evolve_all] / t_dyn
        f_ism_cool[sub_dict_ts["m200_host"] > mhalo_cut] = 0.0
        

    dm_ism_prcool = f_ism_cool * dt


    # Compute recycled infall
    fgrid = eff_dict["fcool_recooled_return"+fV_str]
    
    f_ism_cool = np.zeros(n_gal_ts)
    if interpolate_z:
        f_ism_cool[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_ism_cool[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    f_ism_cool *= (mres_dict_ts["ej_wind"] + mres_dict_ts["cgm_wind"]) # Msun Gyr^-1
    
    dm_ism_recool = f_ism_cool * dt
    

    # Compute galtran infall
    fgrid = eff_dict["fcool_galtran_mcgm_nowind_norm"+fV_str]

    if "_deluxe" in variation:
        fgrid *= 0

    f_ism_cool = np.zeros(n_gal_ts)
    if interpolate_z:
        f_ism_cool[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_ism_cool[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    f_ism_cool *= mres_dict_ts["cgm_pr"] # Msun Gyr^-1

    dm_ism_cool_other = f_ism_cool * dt

    # Compute ireheat infall
    fgrid =  eff_dict["fcool_reheated_mism_norm"+fV_str]

    if "_deluxe" in variation:
        fgrid *= 0

    f_ism_cool = np.zeros(n_gal_ts)
    if interpolate_z:
        f_ism_cool[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_ism_cool[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    f_ism_cool *= mres_dict_ts["ism"] # Msun Gyr^-1
    dm_ism_cool_other += f_ism_cool * dt

    ################### Star formation ##################################

    fgrid = eff_dict["sfr_mism_norm"]
    f_star_sfr = np.zeros(n_gal_ts)
    if interpolate_z:
        f_star_sfr[evolve_all] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
    else:
        f_star_sfr[evolve_all] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
    f_star_sfr *= mres_dict_ts["ism"] # Msun Gyr^-1
    dm_star_sfr = f_star_sfr * dt

    if force_true_sfr:
        dm_star_sfr[evolve_all] = sub_dict_ts["mass_new_stars_init"][evolve_all]
    if use_sat_sfr:
        ok = np.isnan(sub_dict_ts["mass_new_stars_init"])==False
        dm_star_sfr[sat&ok] = sub_dict_ts["mass_new_stars_init"][sat&ok]

        # Makes life hard for limiters if we try to force SF in a satellite with zero ISM mass!
        prob = mres_dict_ts["ism"] == 0.0
        dm_star_sfr[prob&sat&ok] *= 0.0

    ################## ISM outflows ########################################

    # First do wind
    fgrid = eff_dict["ism_wind_tot_ml"+fV_str]
       
    '''print ""
    print "TEMP HACK"
    print ""
    fgrid *= 0.0'''

    f_ism_out = np.zeros(n_gal_ts)
    if interpolate_z:
        f_ism_out[evolve_all] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
    else:
        f_ism_out[evolve_all] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)

    f_ism_out *= dm_star_sfr / dt # Msun Gyr^-1

    dm_ism_out = f_ism_out * dt

    # Then do reheat
    fgrid = eff_dict["fism_reheated_mism_norm"+fV_str]

    if "_deluxe" in variation:
        fgrid *= 0

    f_ism_out = np.zeros(n_gal_ts)
    if interpolate_z:
        f_ism_out[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_ism_out[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    f_ism_out *= mres_dict_ts["ism"]

    dm_ism_out_ireheat = f_ism_out * dt

    ################## CGM outflows ###############################################
    # First do wind
    fgrid = eff_dict["halo_wind_tot_ml"+fV_str]

    '''print ""
    print "ANOTHER TEMP HACK"
    print ""
    fgrid = np.copy(eff_dict["halo_wind_tot_ml"+fV_str])
    fgrid *= 10'''

    f_cgm_out = np.zeros(n_gal_ts)

    if interpolate_z:
        f_cgm_out[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_cgm_out[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    #eff_names += ["bias_cgm_out"+fV_str, "bias_reacc"+fV_str,"bias_cgm_out2"+fV_str, "bias_reacc2"+fV_str]
    if bias_mode == "none":

        dm_cgm_out_pr = f_cgm_out * dm_star_sfr * mres_dict_ts["cgm_pr"] / np.maximum(mres_dict_ts["cgm_pr"] + mres_dict_ts["cgm_wind"],np.zeros_like(mres_dict_ts["ism"])+1e-6) # Msun
        dm_cgm_out_wind = f_cgm_out * dm_star_sfr * mres_dict_ts["cgm_wind"] / np.maximum(mres_dict_ts["cgm_pr"] + mres_dict_ts["cgm_wind"],np.zeros_like(mres_dict_ts["ism"])+1e-6) # Msun

    elif bias_mode == "bias1":
        fgrid = eff_dict["bias_cgm_out"+fV_str]
        b_wind = np.zeros(n_gal_ts)
        if interpolate_z:
            b_wind[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
        else:
            b_wind[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

        dm_cgm_out_wind = f_cgm_out * dm_star_sfr * b_wind
        dm_cgm_out_pr = f_cgm_out * dm_star_sfr * (1-b_wind)

    elif bias_mode == "bias2":
        fgrid = eff_dict["bias_cgm_out2"+fV_str]
        b_wind = np.zeros(n_gal_ts)
        if interpolate_z:
            b_wind[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
        else:
            b_wind[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

        dm_cgm_out = f_cgm_out * dm_star_sfr

        ok = mres_dict_ts["cgm_wind"] + mres_dict_ts["cgm_pr"] > 0
        dm_cgm_out_wind = np.copy(dm_cgm_out)
        dm_cgm_out_wind[ok] *= b_wind[ok] * mres_dict_ts["cgm_wind"][ok] / (b_wind[ok] * mres_dict_ts["cgm_wind"][ok] + mres_dict_ts["cgm_pr"][ok])

        dm_cgm_out_pr = np.copy(dm_cgm_out)
        dm_cgm_out_pr[ok] *= mres_dict_ts["cgm_pr"][ok] / (b_wind[ok] * mres_dict_ts["cgm_wind"][ok] + mres_dict_ts["cgm_pr"][ok])

        prob = (b_wind == 0) & (mres_dict_ts["cgm_pr"] == 0)
        dm_cgm_out_pr[prob] = 0.0
        dm_cgm_out_wind[prob] = 0.0


    # Then do reheat
    fgrid = eff_dict["fhalo_reheated_mcgm_norm"+fV_str]

    if "_deluxe" in variation:
        fgrid *= 0

    f_cgm_out = np.zeros(n_gal_ts)

    if interpolate_z:
        f_cgm_out[evolve_cent] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                      np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)
    else:
        f_cgm_out[evolve_cent] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_cent]), bin_mh_mid)

    dm_cgm_out_hreheat_pr = f_cgm_out * dt * mres_dict_ts["cgm_pr"]
    dm_cgm_out_hreheat_wind = f_cgm_out * dt * mres_dict_ts["cgm_wind"]

    ######### Compute stellar recycling ########3

    if not disable_stellar_recycling and not instantaneous_recycling and not tabulated_recycling:

        # First update SFH arrays #
        sfh_progens = np.concatenate((sfh_progens, dm_star_sfr)) 
        nI_progens = np.concatenate((nI_progens, sub_dict_ts["node_index"]))
        dI_progens = np.concatenate((dI_progens, sub_dict_ts["descendant_index"]))

        # Assume stars formed on average at the halfway point between _ps and _ts
        t_progens = np.concatenate((t_progens, np.zeros_like(dm_star_sfr)+0.5*(t_ts+t_ps)))

        # For now I'm assuming that the metallicity of each galaxy is known
        Zstar_init_ts = np.zeros_like(dm_star_sfr)
        ok = sub_dict_ts["mZ_new_stars_init"] > 0
        Zstar_init_ts[ok] = sub_dict_ts["mZ_new_stars_init"][ok] / sub_dict_ts["mass_new_stars_init"][ok]
        Zstar_init_progens = np.concatenate((Zstar_init_progens, Zstar_init_ts))

        # Compute stellar mass loss for all progenitor points at _ts snapshot
        # Total mass loss from t=0 to _ts
        age_progens_ts = t_ts - t_progens

        mass_loss_ts = ml.compute_stellar_mass_loss( Zstar_init_progens, sfh_progens, age_progens_ts)
        # Total mass loss from t=0 to _ps
        age_progens_ps = t_ps - t_progens
        age_progens_ps[age_progens_ps<0] *= 0
        mass_loss_ps = ml.compute_stellar_mass_loss( Zstar_init_progens, sfh_progens, age_progens_ps)
   
        dm_star_rec_progens = mass_loss_ts - mass_loss_ps
   
        # Now sum together all progenitors (from all previous snapshot) of each _ts halo
        # First we need to reduce the range of numbers to allow floating point operations
        nI_progens_lite = np.arange(0,len(nI_progens))
        nI_ts_lite = np.zeros_like(sub_dict_ts["node_index"])
        ptr = ms.match(sub_dict_ts["node_index"], nI_progens)
        ok_match = ptr >= 0
        nI_ts_lite[ok_match] = nI_progens_lite[ptr][ok_match]

        dm_star_rec = mpf.Sum_Common_ID([dm_star_rec_progens], nI_progens_lite, nI_ts_lite)[0]


    elif not disable_stellar_recycling and instantaneous_recycling:
        dm_star_rec = dm_star_sfr * r_ira
    elif not disable_stellar_recycling and tabulated_recycling:

        fgrid = eff_dict["frec_star_sfr_norm"]
        f_star_rec = np.zeros(n_gal_ts)
        if interpolate_z:
            f_star_rec[evolve_all] = ifu.Interpolate_F_grid(fgrid, t_grid, t_ts, ind_t_lo, ind_t_hi,\
                                                       np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
        else:
            f_star_rec[evolve_all] = ifu.Interpolate_F_grid_mass(fgrid, t_ts, t_min, t_max, np.log10(sub_dict_ts["m200_host"][evolve_all]), bin_mh_mid)
        f_star_rec *= dm_star_sfr /dt # Msun Gyr^-1
        dm_star_rec = f_star_rec * dt

    else:
        dm_star_rec = np.zeros_like(dm_cgm_out)


    if use_nuisance_terms:

        for name in zero_sat_names:
            sub_dict_ts[name][sat] = 0.0

        # First encountered with 25 200 after a new consolidate - 14/10/20 - there is a single non-interpolated galaxy with nan masses
        problem = np.isnan(sub_dict_ts["mass_gas_NSF"]) & (sub_dict_ts["isInterpolated"]==0)
        iI[problem] = True

        ####### CGM wind #############
        dm_cgm_wind = sub_dict_ts["mass_inside"+fV_str] - mres_dict_ts["cgm_wind_progen_actual"]

        # We don't measure mass_reaccreted_hreheat_wind/pr, so have to make an approximation here
        dm_cgm_wind_acc_hreheat_actual = sub_dict_ts["mass_reaccreted_hreheat"] - (sub_dict_ts["mass_reaccreted"+fV_str] - sub_dict_ts["mass_reaccreted_0p5vmax"])
        mcgm_actual = sub_dict_ts["mass_gas_NSF"] - sub_dict_ts["mass_gas_NSF_ISM"]
        dm_cgm_pr_acc_hreheat_actual = np.copy(dm_cgm_wind_acc_hreheat_actual)
        ok = mcgm_actual > 0
        dm_cgm_pr_acc_hreheat_actual[ok] *= (mcgm_actual[ok]-sub_dict_ts["mass_inside"+fV_str][ok]) / mcgm_actual[ok]
        dm_cgm_wind_acc_hreheat_actual[ok] *= sub_dict_ts["mass_inside"+fV_str][ok] / mcgm_actual[ok]

        # Similarly, we don't measure hreheated ejected mass for wind/pr material
        dm_cgm_wind_out_hreheat_actual = sub_dict_ts["mass_halo_reheated"] - sub_dict_ts["mass_halo_reheated_join_wind"+fV_str] - sub_dict_ts["mass_join_halo_wind"+fV_str]
        prob = dm_cgm_wind_out_hreheat_actual < 0
        dm_cgm_wind_out_hreheat_actual[prob] *= 0.0
        dm_cgm_pr_out_hreheat_actual = np.copy(dm_cgm_wind_out_hreheat_actual)
        dm_cgm_pr_out_hreheat_actual[ok] *= (mcgm_actual[ok]-sub_dict_ts["mass_inside"+fV_str][ok]) / mcgm_actual[ok]
        dm_cgm_wind_out_hreheat_actual[ok] *= sub_dict_ts["mass_inside"+fV_str][ok] / mcgm_actual[ok]

        m_ej_wind_actual = sub_dict_ts["mass_outside_galaxy"+fV_str] - sub_dict_ts["mass_inside"+fV_str]
        if "mass_reaccreted_from_wind_0p5vmax" in sub_dict_ts:
            dm_cgm_wind_reacc_actual = sub_dict_ts["mass_reaccreted_from_wind"+fV_str]
            dm_cgm_pr_reacc_actual = sub_dict_ts["mass_reaccreted"+fV_str] - sub_dict_ts["mass_reaccreted_from_wind"+fV_str]

            dm_cgm_wind_out_ej_actual = sub_dict_ts["mass_join_halo_wind_from_ism"] # Note this uses 0.25 vmax cut
            dm_cgm_pr_out_ej_actual = sub_dict_ts["mass_join_halo_wind"+fV_str] + sub_dict_ts["mass_halo_reheated_join_wind"+fV_str] - sub_dict_ts["mass_join_halo_wind_from_ism"]

        else:
            # For older runs, we didn't measure the wind contribution to CGM reaccretion, so make some approximations here
            dm_cgm_wind_reacc_actual = sub_dict_ts["mass_reaccreted"+fV_str]
            dm_cgm_pr_reacc_actual = np.copy(dm_cgm_wind_reacc_actual)

            ok = sub_dict_ts["mass_outside"+fV_str] > 0
            if bias_mode == "none":
                dm_cgm_pr_reacc_actual[ok] *= (sub_dict_ts["mass_outside"+fV_str] - m_ej_wind_actual)[ok] / sub_dict_ts["mass_outside"+fV_str][ok]
                dm_cgm_wind_reacc_actual[ok] *= m_ej_wind_actual[ok] / sub_dict_ts["mass_outside"+fV_str][ok]
            elif bias_mode == "bias1":
                dm_cgm_pr_reacc_actual *= (1-b_reacc)
                dm_cgm_wind_reacc_actual *= b_reacc
            elif bias_mode == "bias2":
                denom = (sub_dict_ts["mass_outside"+fV_str] +(b_reacc-1) * m_ej_wind_actual)
                ok = ok & (denom > 0)
                dm_cgm_pr_reacc_actual[ok] *=  (sub_dict_ts["mass_outside"+fV_str] - m_ej_wind_actual)[ok] / denom[ok]
                dm_cgm_wind_reacc_actual[ok] *= b_reacc[ok] * m_ej_wind_actual[ok] / denom[ok]

            dm_cgm_wind_out_ej_actual = sub_dict_ts["mass_join_halo_wind"+fV_str] + sub_dict_ts["mass_halo_reheated_join_wind"+fV_str]
            dm_cgm_pr_out_ej_actual = np.copy(dm_cgm_wind_out_ej_actual)
            ok = mcgm_actual > 0

            if bias_mode == "none":
                dm_cgm_pr_out_ej_actual = dm_cgm_wind_out_ej_actual * (mcgm_actual-sub_dict_ts["mass_inside"+fV_str]) / np.maximum(mcgm_actual,np.zeros_like(mres_dict_ts["ism"])+1e-6)
                dm_cgm_wind_out_ej_actual *= sub_dict_ts["mass_inside"+fV_str] / np.maximum(mcgm_actual,np.zeros_like(mres_dict_ts["ism"])+1e-6)
            elif bias_mode == "bias1":
                dm_cgm_pr_out_ej_actual = dm_cgm_wind_out_ej_actual * (1-b_wind)
                dm_cgm_wind_out_ej_actual *= b_wind
            elif bias_mode == "bias2":
                denom = (mcgm_actual + (b_wind-1) * sub_dict_ts["mass_inside"+fV_str])
                ok = ok & (denom > 0)
                dm_cgm_pr_out_ej_actual[ok] *= (mcgm_actual - sub_dict_ts["mass_inside"+fV_str])[ok] / denom[ok]
                dm_cgm_wind_out_ej_actual[ok] *= b_wind[ok] * sub_dict_ts["mass_inside"+fV_str][ok] / denom[ok]
 
        dm_cgm_wind_acc_actual = dm_cgm_wind_reacc_actual + dm_cgm_wind_acc_hreheat_actual
        dm_cgm_wind_cooled_actual = sub_dict_ts["mass_recooled"+fV_str]
        dm_ism_out_wind_actual = sub_dict_ts["mass_sf_ism_join_wind"+fV_str] + sub_dict_ts["mass_nsf_ism_join_wind"+fV_str] + sub_dict_ts["mass_ism_reheated_join_wind"+fV_str]
        dm_cgm_wind_out_actual = dm_cgm_wind_out_hreheat_actual + dm_cgm_wind_out_ej_actual

        dm_cgm_wind_known = dm_cgm_wind_acc_actual - dm_cgm_wind_cooled_actual + dm_ism_out_wind_actual - dm_cgm_wind_out_actual
        dm_cgm_wind_nuisance = dm_cgm_wind - dm_cgm_wind_known
        pos = dm_cgm_wind_nuisance > 0
        dm_cgm_wind_nuisance_gain = abs(np.copy(dm_cgm_wind_nuisance))
        dm_cgm_wind_nuisance_loss = abs(np.copy(dm_cgm_wind_nuisance))
        dm_cgm_wind_nuisance_gain[(pos==False)|iI] = 0.0
        dm_cgm_wind_nuisance_loss[pos|iI] = 0.0

        ######## CGM pr ############
        dm_cgm_pr = (mcgm_actual- sub_dict_ts["mass_inside"+fV_str]) - mres_dict_ts["cgm_pr_progen_actual"]
        dm_cgm_pr_accreted_actual = sub_dict_ts["mass_praccreted"] +sub_dict_ts["mass_fof_transfer"] + dm_cgm_pr_reacc_actual + dm_cgm_pr_acc_hreheat_actual
        dm_cgm_pr_cooled_actual = sub_dict_ts["mass_prcooled"] +sub_dict_ts["mass_recooled_ireheat"] - (sub_dict_ts["mass_recooled"+fV_str]-sub_dict_ts["mass_recooled_0p5vmax"]) +sub_dict_ts["mass_galtran"]
        dm_ism_out_ireheat_actual = sub_dict_ts["mass_nsf_ism_reheated"] + sub_dict_ts["mass_sf_ism_reheated"] - dm_ism_out_wind_actual
        prob = dm_ism_out_ireheat_actual < 0
        dm_ism_out_ireheat_actual[prob] *= 0.0

        dm_cgm_pr_known =  dm_cgm_pr_accreted_actual - dm_cgm_pr_cooled_actual - dm_cgm_pr_out_hreheat_actual - dm_cgm_pr_out_ej_actual + dm_ism_out_ireheat_actual

        dm_cgm_pr_nuisance = dm_cgm_pr - dm_cgm_pr_known
        pos = dm_cgm_pr_nuisance > 0
        dm_cgm_pr_nuisance_gain = abs(np.copy(dm_cgm_pr_nuisance))
        dm_cgm_pr_nuisance_loss = abs(np.copy(dm_cgm_pr_nuisance))
        dm_cgm_pr_nuisance_gain[(pos==False)|iI] = 0.0
        dm_cgm_pr_nuisance_loss[pos|iI] = 0.0

        ######### ISM ###########
        dm_ism = sub_dict_ts["mass_gas_SF"] + sub_dict_ts["mass_gas_NSF_ISM"] - mres_dict_ts["ism_progen_actual"]
        dm_star_sfr_actual = sub_dict_ts["mass_new_stars_init"]
        if "Recal" not in sim_name:
            dm_star_rec_actual = sub_dict_ts["mass_stellar_rec"]
        else:
            dm_star_rec_actual = np.zeros_like(dm_ism)
        # Seems this can be a NaN sometimes, deal with it
        dm_star_sfr_actual[np.isnan(dm_star_sfr_actual)] = 0.0
        dm_star_rec_actual[np.isnan(dm_star_rec_actual)] = 0.0

        dm_ism_known = dm_cgm_pr_cooled_actual + dm_cgm_wind_cooled_actual - dm_ism_out_wind_actual - dm_ism_out_ireheat_actual + dm_star_rec_actual - dm_star_sfr_actual
        dm_ism_nuisance = dm_ism - dm_ism_known
        pos = dm_ism_nuisance > 0
        dm_ism_nuisance_gain = abs(np.copy(dm_ism_nuisance))
        dm_ism_nuisance_loss = abs(np.copy(dm_ism_nuisance))
        dm_ism_nuisance_gain[(pos==False)|iI] = 0.0
        dm_ism_nuisance_loss[pos|iI] = 0.0

        ####### Stars #############
        dm_star = sub_dict_ts["mass_star"] - mres_dict_ts["stars_progen_actual"]
        dm_star_known = dm_star_sfr_actual - dm_star_rec_actual
        dm_star_nuisance = dm_star - dm_star_known
        pos = dm_star_nuisance > 0
        dm_star_nuisance_gain = abs(np.copy(dm_star_nuisance))
        dm_star_nuisance_loss = abs(np.copy(dm_star_nuisance))
        dm_star_nuisance_gain[(pos==False)|iI] = 0.0
        dm_star_nuisance_loss[pos|iI] = 0.0

        ####### Non-wind ejected reservoir (>R_200) #######
        dm_ej_pr = sub_dict_ts["mass_outside"+fV_str] - m_ej_wind_actual - mres_dict_ts["ej_pr_progen_actual"]
        dm_ej_pr_known = dm_cgm_pr_out_ej_actual - dm_cgm_pr_reacc_actual
        dm_ej_pr_nuisance = dm_ej_pr - dm_ej_pr_known
        pos = dm_ej_pr_nuisance > 0
        dm_ej_pr_nuisance_gain = abs(np.copy(dm_ej_pr_nuisance))
        dm_ej_pr_nuisance_loss = abs(np.copy(dm_ej_pr_nuisance))
        dm_ej_pr_nuisance_gain[(pos==False)|iI] = 0.0
        dm_ej_pr_nuisance_loss[pos|iI] = 0.0

        dm_ej_wind = m_ej_wind_actual - mres_dict_ts["ej_wind_progen_actual"]
        dm_ej_wind_known = dm_cgm_wind_out_ej_actual - dm_cgm_wind_reacc_actual
        dm_ej_wind_nuisance = dm_ej_wind - dm_ej_wind_known
        pos = dm_ej_wind_nuisance > 0
        dm_ej_wind_nuisance_gain = abs(np.copy(dm_ej_wind_nuisance))
        dm_ej_wind_nuisance_loss = abs(np.copy(dm_ej_wind_nuisance))
        dm_ej_wind_nuisance_gain[(pos==False)|iI] = 0.0
        dm_ej_wind_nuisance_loss[pos|iI] = 0.0

    else:
        dm_cgm_pr_nuisance_gain = np.zeros_like(mres_dict_ts["cgm_pr"]); dm_cgm_pr_nuisance_loss = np.zeros_like(mres_dict_ts["cgm_pr"])
        dm_cgm_wind_nuisance_gain = np.zeros_like(mres_dict_ts["cgm_pr"]); dm_cgm_wind_nuisance_loss = np.zeros_like(mres_dict_ts["cgm_pr"])
        dm_ism_nuisance_gain = np.zeros_like(mres_dict_ts["cgm_pr"]); dm_ism_nuisance_loss = np.zeros_like(mres_dict_ts["cgm_pr"])
        dm_star_nuisance_gain = np.zeros_like(mres_dict_ts["cgm_pr"]); dm_star_nuisance_loss = np.zeros_like(mres_dict_ts["cgm_pr"])
        dm_ej_pr_nuisance_gain = np.zeros_like(mres_dict_ts["cgm_pr"]); dm_ej_pr_nuisance_loss = np.zeros_like(mres_dict_ts["cgm_pr"])
        dm_ej_wind_nuisance_gain = np.zeros_like(mres_dict_ts["cgm_pr"]); dm_ej_wind_nuisance_loss = np.zeros_like(mres_dict_ts["cgm_pr"])


    ########## Apply limiters (using iterative approach) #################
    t_loop3 = time.time()
    def limit_mass_exchange(loss_terms, gain_terms, mres,stop_evo,return_problem=False):
        loss_sum = np.sum(loss_terms,axis=0)
        gain_sum = np.sum(gain_terms,axis=0)
        problem = loss_sum > mres + gain_sum
        rescale = loss_sum[problem] / (mres[problem] + gain_sum[problem])

        output = {}

        if stop_evo:
            for term in loss_terms:
                term[problem] *= 0.0
            for term in gain_terms:
                term[problem] *= 0.0
            output["n_prob"] = 0
        else:
            for term in loss_terms:
                term[problem] *= 1/(rescale* (1+tolerance))
            output["n_prob"] = len(problem[problem])

        if return_problem:
            output["problem"] = problem

        return output

    if use_limiters:
        need_limiters = True
    else:
        need_limiters = False
    n_iter = 0
    stop_evo = False
    while need_limiters:
        if n_iter == 0:
            return_output=True
        else:
            return_output=False

        # Note: the order here matters for efficiency. Do the least coupled reservoirs first.
        # Stars
        loss_terms = [dm_star_rec,dm_star_nuisance_loss]
        gain_terms = [dm_star_sfr,dm_star_nuisance_gain]
        output_stars = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["stars"],stop_evo,return_output)
        n_prob_stars = output_stars["n_prob"]

        # Ejected gas (>r200, came from galaxy wind)
        loss_terms = [dm_cgm_reacc_wind,dm_ej_wind_nuisance_loss]
        gain_terms = [dm_cgm_out_wind,dm_ej_wind_nuisance_gain]
        output_ej_wind = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["ej_wind"],stop_evo,return_output)
        n_prob_ej_wind = output_ej_wind["n_prob"]

        # Ejected gas (>r200, but not from galaxy wind)
        loss_terms = [dm_cgm_reacc_pr, dm_ej_pr_nuisance_loss]
        gain_terms = [dm_cgm_out_pr, dm_ej_pr_nuisance_gain]
        output_ej_pr = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["ej_pr"],stop_evo,return_output)
        n_prob_ej_pr = output_ej_pr["n_prob"]

        # CGM (wind material - i.e., came from ISM)
        loss_terms = [dm_ism_recool, dm_cgm_out_wind, dm_cgm_out_hreheat_wind, dm_cgm_wind_nuisance_loss]
        gain_terms = [dm_cgm_reacc_wind, dm_cgm_acc_hreheat_wind, dm_ism_out, dm_cgm_wind_nuisance_gain]
        output_cgm_wind = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["cgm_wind"],stop_evo,return_output)
        n_prob_cgm_wind = output_cgm_wind["n_prob"]

        # CGM (that is not in the ISM wind - note includes ireheat and galtran material)
        loss_terms = [dm_ism_prcool, dm_ism_cool_other, dm_cgm_out_pr, dm_cgm_out_hreheat_pr, dm_cgm_pr_nuisance_loss]
        gain_terms = [dm_cgm_pracc, dm_cgm_reacc_pr,dm_cgm_pr_nuisance_gain,dm_ism_out_ireheat, dm_cgm_acc_hreheat_pr]
        output_cgm_pr = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["cgm_pr"],stop_evo,return_output)
        n_prob_cgm_pr = output_cgm_pr["n_prob"]

        # ISM
        loss_terms = [dm_star_sfr, dm_ism_out, dm_ism_out_ireheat,dm_ism_nuisance_loss]
        gain_terms = [dm_ism_prcool, dm_ism_recool, dm_ism_cool_other, dm_star_rec,dm_ism_nuisance_gain]
        output_ism = limit_mass_exchange(loss_terms, gain_terms, mres_dict_ts["ism"],stop_evo,return_output)
        n_prob_ism = output_ism["n_prob"]
               
        if n_prob_cgm_wind == 0 and n_prob_cgm_pr == 0 and n_prob_ism == 0 and n_prob_stars == 0 and n_prob_ej_wind == 0 and n_prob_ej_pr == 0:
            need_limiters = False

        n_prob_tot = max(n_prob_cgm_wind, n_prob_ism, n_prob_stars, n_prob_ej_pr, n_prob_ej_wind, n_prob_cgm_pr)

        n_iter += 1
        if n_iter == 25:
            print "Warning: limiters are slow to converge"
        if n_iter > 50 and not stop_evo:
            print "Warning: limiters didn't converge after 50 iterations"
            print "Setting evolution for the", n_prob_tot, "galaxies that failed to zero"
            stop_evo = True


    ######### Increment masses ##############
    t_loop4 = time.time()

    # accretion at R200
    mres_dict_ts["cgm_wind"] += dm_cgm_reacc_wind + dm_cgm_acc_hreheat_wind
    mres_dict_ts["cgm_pr"] += dm_cgm_pracc + dm_cgm_reacc_pr + dm_cgm_acc_hreheat_pr
    
    # cooling (accretion onto ISM)
    mres_dict_ts["cgm_wind"] -= dm_ism_recool
    mres_dict_ts["cgm_pr"] -= dm_ism_prcool + dm_ism_cool_other
    mres_dict_ts["ism"] += dm_ism_prcool + dm_ism_recool + dm_ism_cool_other

    # star formation
    mres_dict_ts["ism"] -= dm_star_sfr
    mres_dict_ts["stars"] += dm_star_sfr

    if compute_main_progenitor:
        mres_dict_ts["stars_mp"] += dm_star_sfr * (1-r_ira)
        mres_dict_ts["stars_mp_actual"][evolve_cent] += sub_dict_ts["mass_new_stars_init"][evolve_cent] * (1-r_ira)

    if compute_central_stellar_mass:
        mres_dict_ts["stars_init"] += dm_star_sfr
        mres_dict_ts["stars_init_cent"][evolve_cent] += dm_star_sfr[(sat==False)&(iI==False)]
        mres_dict_ts["stars_init_actual"][np.isnan(sub_dict_ts["mass_new_stars_init"])==False] += sub_dict_ts["mass_new_stars_init"][np.isnan(sub_dict_ts["mass_new_stars_init"])==False]
        mres_dict_ts["stars_init_cent_actual"][evolve_cent] += sub_dict_ts["mass_new_stars_init"][evolve_cent]

    # stellar recycling
    mres_dict_ts["stars"] -= dm_star_rec
    mres_dict_ts["ism"] += dm_star_rec

    # ISM outflow
    mres_dict_ts["ism"] -= dm_ism_out + dm_ism_out_ireheat
    mres_dict_ts["cgm_wind"] += dm_ism_out
    mres_dict_ts["cgm_pr"] += dm_ism_out_ireheat

    # Halo outflow
    mres_dict_ts["cgm_wind"] -= dm_cgm_out_wind + dm_cgm_out_hreheat_wind
    mres_dict_ts["cgm_pr"] -= dm_cgm_out_pr + dm_cgm_out_hreheat_pr

    # Halo ejecta
    mres_dict_ts["ej_wind"] += dm_cgm_out_wind
    mres_dict_ts["ej_wind"] -= dm_cgm_reacc_wind

    mres_dict_ts["ej_pr"] += dm_cgm_out_pr
    mres_dict_ts["ej_pr"] -= dm_cgm_reacc_pr


    # Nuisance terms
    mres_dict_ts["cgm_wind"] += dm_cgm_wind_nuisance_gain - dm_cgm_wind_nuisance_loss
    mres_dict_ts["cgm_pr"] += dm_cgm_pr_nuisance_gain - dm_cgm_pr_nuisance_loss
    mres_dict_ts["ism"] += dm_ism_nuisance_gain - dm_ism_nuisance_loss
    mres_dict_ts["stars"] += dm_star_nuisance_gain - dm_star_nuisance_loss
    mres_dict_ts["ej_wind"] += dm_ej_wind_nuisance_gain - dm_ej_wind_nuisance_loss
    mres_dict_ts["ej_pr"] += dm_ej_pr_nuisance_gain - dm_ej_pr_nuisance_loss

    if compute_dm_cumulative_acc:
        evolve_cent_sat = (iI ==False) & (np.isnan(sub_dict_ts["mass_diffuse_dm_pristine"])==False)
        mres_dict_ts["dm_first_acc"][evolve_cent_sat] += sub_dict_ts["mass_diffuse_dm_pristine"][evolve_cent_sat]

    if n_gal_ts > 0:
        if mres_dict_ts["ism"].min() < 0.0 or mres_dict_ts["cgm_pr"].min() < 0.0 or mres_dict_ts["cgm_wind"].min() < 0.0 or mres_dict_ts["stars"].min() < 0.0 or mres_dict_ts["ej_wind"].min() < 0.0 or mres_dict_ts["ej_pr"].min() < 0.0:
            if use_limiters:
                print "Error: something wen't wrong with the iterative limiters and at least one subhalo has negative baryonic mass"

                print "ism", np.unique(mres_dict_ts["ism"])
                print "cgm_pr", np.unique(mres_dict_ts["cgm_pr"])
                print "cgm_wind", np.unique(mres_dict_ts["cgm_wind"])
                print "stars", np.unique(mres_dict_ts["stars"])
                print "ej_pr", np.unique(mres_dict_ts["ej_pr"])
                print "ej_wind", np.unique(mres_dict_ts["ej_wind"])

                quit()
            else:
                print "Warning: at least one subhalo has negative baryonic mass"

        problem = np.isnan(mres_dict_ts["ism"]) | np.isnan(mres_dict_ts["stars"]) | np.isnan(mres_dict_ts["cgm_pr"]) | np.isnan(mres_dict_ts["cgm_wind"]) | np.isnan(mres_dict_ts["ej_pr"]) | np.isnan(mres_dict_ts["ej_wind"]) 
        if len(mres_dict_ts["ism"][problem]) > 0:
            print "Error: we have nans in the mres arrays"
            print "ism max", np.unique(mres_dict_ts["ism"]).max()
            print "cgm_pr max", np.unique(mres_dict_ts["cgm_pr"]).max()
            print "cgm_wind max", np.unique(mres_dict_ts["cgm_wind"]).max()
            print "stars max", np.unique(mres_dict_ts["stars"]).max()
            print "ej_pr max", np.unique(mres_dict_ts["ej_pr"]).max()
            print "ej_wind max", np.unique(mres_dict_ts["ej_wind"]).max()
            quit()


    ######## Add mass to descendants ###########
    if snapshot_ns != snapshots[-1]:

        # To compute how many snapshots we've tracked each progenitor (n_snap), we need to find the oldest progenitor
        mres_dict_ts["n_snap"] = ifu.Keep_max_common_id(mres_dict_ts["n_snap"], sub_dict_ts["descendant_index"])

        # To run floating point operations (np.hist) on IDs, we need to reduce the range
        node_index_ns_lite = np.arange(0,len(sub_dict_ns["node_index"]))
        ptr = ms.match(sub_dict_ts["descendant_index"], sub_dict_ns["node_index"])
        ok_match = ptr >= 0
        descendant_index_ts_lite = node_index_ns_lite[ptr]
        descendant_index_ts_lite[ok_match==False] = -1


        if disable_mergers or compute_main_progenitor:
            # In this case, define "main" progenitor as the most massive (stellar mass) progenitor - set all other masses to zero
            mstar_mp = ifu.Keep_max_common_id( mres_dict_ts["stars"], sub_dict_ts["descendant_index"] )
            main_progen = mstar_mp > 0
            if disable_mergers:
                for name in mres_dict_ts:
                    mres_dict_ts[name][main_progen==False] = 0.0
            if compute_main_progenitor:
                mres_dict_ts["stars_mp"][main_progen==False] = 0.0
                mres_dict_ts["stars_mp_actual"][main_progen==False] = 0.0
            
        if use_nuisance_terms:
            evolve = iI==False
            mres_dict_ts["cgm_wind_progen_actual"][evolve] = sub_dict_ts["mass_inside"+fV_str][evolve]
            mres_dict_ts["cgm_pr_progen_actual"][evolve] = sub_dict_ts["mass_gas_NSF"][evolve] - sub_dict_ts["mass_gas_NSF_ISM"][evolve] - sub_dict_ts["mass_inside"+fV_str][evolve]
            mres_dict_ts["ism_progen_actual"][evolve] = sub_dict_ts["mass_gas_SF"][evolve] + sub_dict_ts["mass_gas_NSF_ISM"][evolve]
            mres_dict_ts["stars_progen_actual"][evolve] = sub_dict_ts["mass_star"][evolve]
            mres_dict_ts["ej_wind_progen_actual"][evolve] = sub_dict_ts["mass_outside_galaxy"+fV_str][evolve] - sub_dict_ts["mass_inside"+fV_str][evolve]
            mres_dict_ts["ej_pr_progen_actual"][evolve] = sub_dict_ts["mass_outside"+fV_str][evolve] - mres_dict_ts["ej_wind_progen_actual"][evolve]

            # If I am correcting for unexplained changes in mass, I need to remove mass in interpolated haloes
            # Note this may cause some trouble when trying to find the main progenitor etc in post-processing
            mres_dict_ts["cgm_wind"][iI] = 0.0
            mres_dict_ts["cgm_pr"][iI] = 0.0
            mres_dict_ts["ism"][iI] = 0.0
            mres_dict_ts["stars"][iI] = 0.0
            mres_dict_ts["ej_wind"][iI] = 0.0
            mres_dict_ts["ej_pr"][iI] = 0.0

        # Sum subhalo masses from _ts that have a common descendant on _ns
        mres_dict_ns = mpf.Sum_Common_ID(mres_dict_ts, descendant_index_ts_lite, node_index_ns_lite,use_dict=True, dict_names=res_names)
        
        # Increment galaxies on _ns for which a progenitor was identified
        progenitor_found = mres_dict_ns["cgm_pr"] + mres_dict_ns["stars"] > 0.0
        mres_dict_ns["n_snap"][progenitor_found] += 1


    ######## Propagate tracers ############
    if do_tracers:

        ######### First create new tracers to trace gas that was pr-accreted this step ##############
        try:
            tracer_dict_ts = tracer_dict_ns
        except:
            pass

        # Desired number of new tracers per galaxy
        n_tr_new = np.zeros(n_gal_ts)
        ok_tr = sub_dict_ts["m200_host_descendant_z0"] > 0
        n_tr_new[ok_tr] = N_tr_des * dm_cgm_pracc[ok_tr] / (fb*sub_dict_ts["m200_host_descendant_z0"][ok_tr])
        
        # Compute integer number of new tracers per galaxy
        remainder = np.remainder(n_tr_new, np.ones_like(n_tr_new))
        n_tr_new_int = n_tr_new - remainder
        remainder_int = np.zeros_like(n_tr_new)
        P = np.random.random_sample(len(n_tr_new))
        remainder_int[ remainder > P] = 1
        n_tr_new = n_tr_new_int + remainder_int
        n_tr_new = n_tr_new.astype("int")
        
        for name in tracer_sub_names:
            if name in tracer_dict_ts:
                tracer_dict_ts[name] = np.concatenate((tracer_dict_ts[name], np.repeat( sub_dict_ts[name], n_tr_new)))
            else:
                tracer_dict_ts[name] = np.repeat( sub_dict_ts[name], n_tr_new)

        state = np.ones_like(np.repeat(n_tr_new,n_tr_new))  # State = 1 means the tracer is in CGM_pr
        if "state" in tracer_dict_ts:
            tracer_dict_ts["state"] = np.concatenate((tracer_dict_ts["state"], state))
        else:
            tracer_dict_ts["state"] = state

        ############## Exchange tracers between reservoirs ###################
        # Note at present tracers do not track the nuisance terms. They also do not account for hreheat (neither ejection, nor reacction)

        n_tr_ts = len(tracer_dict_ts["node_index"])
        state_new = np.copy(tracer_dict_ts["state"])

        # First we need to match tracers with subhaloes
        ptr = ms.match(tracer_dict_ts["node_index"], sub_dict_ts["node_index"])
        ok_match = ptr >= 0
        
        if len(ok_match[ok_match==False]) > 0:
            print "Error, there are tracers that cannot be matched to subhaloes"
            quit()


        ######## CGM_pr tracers #####
        dm_cgm_pr_loss = dm_ism_prcool + dm_ism_cool_other + dm_cgm_out_pr # + dm_cgm_out_hreheat_pr # For convenience - we exclude hreheat for tracers
        P_cgm_pr_loss = dm_cgm_pr_loss / mres_dict_ts["cgm_pr"]
        tr_cgm_pr_loss = (tracer_dict_ts["state"]==1) & (P_cgm_pr_loss[ptr] > np.random.random_sample(n_tr_ts))

        P_cool = (dm_ism_prcool + dm_ism_cool_other) / dm_cgm_pr_loss
        #P_ej = dm_cgm_out / dm_cgm_loss # This is just 1 - P_cool
        
        R = np.random.random_sample(n_tr_ts)
        tr_cool = tr_cgm_pr_loss & (P_cool[ptr] > R)
        tr_ej = tr_cgm_pr_loss & (P_cool[ptr] < R)

        state_new[tr_cool] = 3 # State 3 is ISM
        state_new[tr_ej] = 5 # State 5 is Halo no-wind ejecta (so outside R200)

        ##### CGM_wind tracers #########
        dm_cgm_wind_loss = dm_ism_recool + dm_cgm_out_wind # - we exclude hreheat for tracers
        P_cgm_wind_loss = dm_cgm_wind_loss / mres_dict_ts["cgm_wind"]
        tr_cgm_wind_loss = (tracer_dict_ts["state"]==2) & (P_cgm_wind_loss[ptr] > np.random.random_sample(n_tr_ts))

        P_cool = dm_ism_recool / dm_cgm_wind_loss
        
        tr_cool = tr_cgm_wind_loss & (P_cool[ptr] > R)
        tr_ej = tr_cgm_wind_loss & (P_cool[ptr] < R)

        state_new[tr_cool] = 3 # State 3 is ISM
        state_new[tr_ej] = 6 # State 5 is Halo wind ejecta (so outside R200)

        ######## ISM tracers ############
        dm_ism_loss = dm_ism_out + dm_ism_out_ireheat + dm_star_sfr
        P_ism_loss = dm_ism_loss / mres_dict_ts["ism"]
        tr_ism_loss = (tracer_dict_ts["state"]==3) & (P_ism_loss[ptr] > np.random.random_sample(n_tr_ts))

        P_out = (dm_ism_out + dm_ism_out_ireheat)  / dm_ism_loss
        
        tr_out = tr_ism_loss & (P_out[ptr] > R)
        tr_sfr = tr_ism_loss & (P_out[ptr] < R)

        P_wind = dm_ism_out / (dm_ism_out + dm_ism_out_ireheat)
        tr_wind = tr_out & (P_wind[ptr] > np.random.random_sample(n_tr_ts))
        tr_ireheat = tr_out & (tr_wind==False)

        state_new[tr_wind] = 2 # State 2 is CGM_wind
        state_new[tr_ireheat] = 1 # State 1 is CGM_pr
        state_new[tr_sfr] = 4 # State 4 is Stars

        ######## Star tracers #############
        P_srec = dm_star_rec / mres_dict_ts["stars"]
        tr_srec = (tracer_dict_ts["state"]==4) & (P_srec[ptr] > R)

        state_new[tr_srec] = 3 # State 3 is the ISM

        ############ Halo_pr ejecta tracers #########
        P_ret = dm_cgm_reacc_pr / mres_dict_ts["ej_pr"]
        tr_ret = (tracer_dict_ts["state"]==5) & (P_ret[ptr] > R)
        state_new[tr_ret] = 1 # State 1 is CGM_pr

        ############ Halo_wind ejecta tracers #########
        P_ret = dm_cgm_reacc_wind / mres_dict_ts["ej_wind"]
        tr_ret = (tracer_dict_ts["state"]==6) & (P_ret[ptr] > R)

        state_new[tr_ret] = 2 # State 2 is the CGM_wind

        # Update variables
        tracer_dict_ts["state"] = state_new
        

        '''print "GOT HERE"
        temp = np.unique(state_new, return_counts=True)
        norm = np.sum(temp[1])
        print temp, temp[1]*1.0/norm
        ok = sub_dict_ts["m200_host_descendant_z0"] > 0
        norm = np.sum((mres_dict_ts["cgm"] + mres_dict_ts["ism"] + mres_dict_ts["stars"] + mres_dict_ts["ej"])[ok])
        print np.sum(mres_dict_ts["cgm"][ok]/norm), np.sum(mres_dict_ts["ism"][ok]/norm),np.sum(mres_dict_ts["stars"][ok]/norm),np.sum(mres_dict_ts["ej"][ok]/norm)

        if snapshot == 110:
            quit()'''


        ############## Identify descenant subhaloes on the next step
        if snapshot_ns != snapshots[-1]:
            ptr = ms.match( tracer_dict_ts["descendant_index"] , sub_dict_ns["node_index"] )
            ok_match = ptr >= 0
            
            tracer_dict_ns = {}
            for name in tracer_sub_names:
                tracer_dict_ns[name] = sub_dict_ns[name][ptr][ok_match]

            tracer_dict_ns["state"] = tracer_dict_ts["state"][ok_match]
        
        ############### Store tracer data (to be written to disk later) #############
        outtr_list.append(tracer_dict_ts)
        outtr_snap_list.append(snapshot)


    if save_outputs:
        if z_outputs == "all" or snapshot in snap_outputs:
            if z_outputs != "all":
                print "Saving outputs for this snapshot"
        
            outsnap_list.append(snapshot)
            outres_list.append(mres_dict_ts)

            if save_rates:
                extra_outputs = ["dm_cgm_pracc", "dm_cgm_reacc_wind", "dm_cgm_reacc_pr", "dm_ism_prcool", "dm_ism_recool", "dm_ism_cool_other"]
                outres_rates.append([dm_cgm_pracc, dm_cgm_reacc_wind, dm_cgm_reacc_pr, dm_ism_prcool, dm_ism_recool, dm_ism_cool_other])
                extra_outputs += ["dm_star_sfr", "dm_star_rec", "dm_ism_out", "dm_ism_out_ireheat", "dm_cgm_out_wind", "dm_cgm_out_pr", "dm_cgm_out_hreheat_pr", "dm_cgm_out_hreheat_wind"]
                outres_rates[-1] += [dm_star_sfr, dm_star_rec, dm_ism_out, dm_ism_out_ireheat, dm_cgm_out_wind, dm_cgm_out_pr, dm_cgm_out_hreheat_pr, dm_cgm_out_hreheat_wind]
            else:
                extra_outputs = []

    t_loop5 = time.time()

    dt_io = t_loop2 - t_loop1
    dt_int = t_loop3 - t_loop2
    dt_lim = t_loop4 - t_loop3
    dt_dI = t_loop5 - t_loop4
    dt_loop = t_loop5 - t_loop0
    if verbose:
        print "Time breakdown: IO / Interpolation / Limiters / Descendant link / total", dt_io, dt_int, dt_lim, dt_dI, dt_loop

File.close()
if do_tracers:
    File_tr.close()

if save_outputs:
    # Note, output arrays should match those from processed_catalogue
    print "writing to disk"

    # Delete existing hdf5 file if one exists before writing a new one
    if test_mode:
        filename = "test_integrator_output_"+sim_name+variation
    else:
        filename = "integrator_output_"+sim_name+variation

    filename += ".hdf5"

    if os.path.isfile(output_path+filename):
        os.remove(output_path+filename)

    File_out = h5py.File(output_path+filename)

    for i_snap, snap_out in enumerate(outsnap_list):

        snap_group = File_out.create_group("Snap_"+str(snap_out))

        for name in res_names:
            snap_group.create_dataset(name, data=outres_list[i_snap][name])
        for i_name, name in enumerate(extra_outputs):
            snap_group.create_dataset(name, data=outres_rates[i_snap][i_name])

    if do_tracers:
        for i_snap, snap_out in enumerate(outtr_snap_list):
            tracer_dict_ts = outtr_list[i_snap]
            tracer_snap_group = File_out.create_group("Tracer_Data_Snap"+str(snap_out))
            for name in tracer_dict_ts:
                tracer_snap_group.create_dataset(name, data=tracer_dict_ts[name])
        
    option_group = File_out.create_group("options")
    for option in options:
        option_group.create_dataset(option, data=str(options[option]))
    File_out.close()






if plot_residuals:

    ok = (sub_dict_ts["subgroup_number"]==0) & (sub_dict_ts["isInterpolated"]==0)

    ok2 = (sub_dict_ts["group_number"] == grn_select) & (sub_dict_ts["subgroup_number"] == sgrn_select)

    from utilities_plotting import *

    py.subplot(221)
    mcgm = sub_dict_ts["mass_gas_NSF"] - sub_dict_ts["mass_gas_NSF_ISM"]
    mres_dict_ts["cgm"] = mres_dict_ts["cgm_pr"] + mres_dict_ts["cgm_wind"]
    py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["cgm"][ok]/mcgm[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
    py.axhline(0.0,c="k",alpha=0.3)

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["cgm"][ok]/mcgm[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, med, c="k")
    py.plot(mid_bin, lo, c="k")
    py.plot(mid_bin, hi, c="k")

    mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mcgm[ok],mres_dict_ts["cgm"][ok])
    py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

    py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["cgm"][ok2] / mcgm[ok2]))

    py.ylabel(r"$\log_{10}(M_{\mathrm{CGM}} \, / M_{\mathrm{CGM,true}})$")

    py.subplot(222)
    mism = sub_dict_ts["mass_gas_SF"] + sub_dict_ts["mass_gas_NSF_ISM"]
    py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["ism"][ok]/mism[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
    py.axhline(0.0,c="k",alpha=0.3)

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["ism"][ok]/mism[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, med, c="k")
    py.plot(mid_bin, lo, c="k")
    py.plot(mid_bin, hi, c="k")

    mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mism[ok],mres_dict_ts["ism"][ok])
    py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

    py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["ism"][ok2] / mism[ok2]))

    py.ylabel(r"$\log_{10}(M_{\mathrm{ISM}} \, / M_{\mathrm{ISM,true}})$")

    py.subplot(223)
    #mstar_init = sub_dict_ts["mass_star_init"]
    #py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["stars"][ok]/mstar_init[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")

    mstar = sub_dict_ts["mass_star"]
    py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["stars"][ok]/mstar[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
    py.axhline(0.0,c="k",alpha=0.3)

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["stars"][ok]/mstar[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, med, c="k")
    py.plot(mid_bin, lo, c="k")
    py.plot(mid_bin, hi, c="k")

    mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mstar[ok],mres_dict_ts["stars"][ok])
    py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

    py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["stars"][ok2] / mstar[ok2]))

    py.ylabel(r"$\log_{10}(M_{\mathrm{\star}} \, / M_{\mathrm{\star,true}})$")

    py.subplot(224)

    show_ej = False
    mej = sub_dict_ts["mass_outside"+fV_str]

    if not show_ej:
        mej = sub_dict_ts["mass_outside_galaxy"+fV_str]
        mres_dict_ts["ej"] = mres_dict_ts["ej_wind"] + mres_dict_ts["cgm_wind"]
    else:
        mres_dict_ts["ej"] = mres_dict_ts["ej_wind"] + mres_dict_ts["ej_pr"]

    py.scatter(np.log10(sub_dict_ts["m200_host"][ok]), np.log10(mres_dict_ts["ej"][ok]/mej[ok]), cmap="autumn", c=mres_dict_ts["n_snap"][ok], edgecolors="none")
    py.axhline(0.0,c="k",alpha=0.3)

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["ej"][ok]/mej[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, med, c="k")
    py.plot(mid_bin, lo, c="k")
    py.plot(mid_bin, hi, c="k")

    mean_ratio = us.Calculate_Mean_Ratio(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),mej[ok],mres_dict_ts["ej"][ok])
    py.plot(mid_bin, np.log10(mean_ratio), c="k", linestyle=':')

    py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["ej"][ok2] / mej[ok2]))

    if show_ej:
        py.ylabel(r"$\log_{10}(M_{\mathrm{ej}} \, / M_{\mathrm{ej,true}})$")
    else:
        py.ylabel(r"$\log_{10}(M_{\mathrm{wind}} \, / M_{\mathrm{wind,true}})$")

    py.show()


if plot_mstar_mhalo:

    ok = (sub_dict_ts["subgroup_number"]==0) & (sub_dict_ts["isInterpolated"]==0)
    ok2 = (sub_dict_ts["group_number"] == grn_select) & (sub_dict_ts["subgroup_number"] == sgrn_select)

    from utilities_plotting import *

    py.figure()

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mstar[ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, med, c="k")
    py.plot(mid_bin, lo, c="k")
    py.plot(mid_bin, hi, c="k")

    med, lo, hi, mid_bin = us.Calculate_Percentiles(bin_mh_mid,np.log10(sub_dict_ts["m200_host"][ok]),np.log10(mres_dict_ts["stars"][ok]),weights=None,min_count=1,lohi=(0.16,0.84),complete=None)
    py.plot(mid_bin, med, c="r")
    py.plot(mid_bin, lo, c="r")
    py.plot(mid_bin, hi, c="r")

    py.scatter(np.log10(sub_dict_ts["m200_host"][ok2]), np.log10(mres_dict_ts["stars"][ok2]))

    py.show()
