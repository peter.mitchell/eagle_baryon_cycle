#! /bin/tcsh -f

set MemPerNode = 12000

set logdir = /cosma7/data/dp004/d72fqv/Baryon_Cycle/ODE_output/LOG/

# For running with boost factors
set args = ( 0.1 0.25 0.5 2 4 10)
#set args = ( 2 4 10 )

set max_jobs = 6
set script = run_script_integrator.csh

set jobname_base = integrator
set logpath = ${logdir}/integrator

foreach arg ($args)
    set logname = ${logpath}_${arg}_%a_jobid%A.log
    set jobname = ${jobname_base}_${arg}
    cat <<EOF - ${script} | sbatch
#!/bin/tcsh -ef
#
#SBATCH --ntasks 1
#SBATCH -J ${jobname}
#SBATCH -o ${logname}
#SBATCH -p cordelia
#SBATCH -A durham
#SBATCH -t 10:00:00
#SBATCH --mem ${MemPerNode}
#

# Set parameters
set arg        = ${arg}

EOF
end
