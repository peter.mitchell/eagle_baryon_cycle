from scipy.interpolate import interp1d
import numpy as np

def Interpolate_F_grid_mass(f_grid, t_ts, t_min, t_max, log_msub, log_msub_grid, interp="log"):
    
    ind_t = np.where((t_ts >= t_min-0.01) & (t_ts <= t_max+0.01))[0][0]
    f_grid_ts = np.copy(f_grid[ind_t])

    ok = (np.isnan(f_grid_ts) == False) & (f_grid_ts != np.inf) & (f_grid_ts != -np.inf)
    log_msub_grid = log_msub_grid[ok]
    f_grid_ts = f_grid_ts[ok]

    if f_grid_ts.min() < 0.0:
        print "Error in f_grid, less than 0"
        quit()

    if interp == "linear":
        # First linearly interpolate within the tabulated range
        interp_1stpass = interp1d(log_msub_grid, f_grid_ts, kind='linear', bounds_error=False, fill_value=np.nan)
        fsub = interp_1stpass(log_msub)

        # Second, extrapolate beyond tabulated range by fixing to end-point values
        ok = np.isnan(fsub)
        interp_2ndpass = interp1d(log_msub_grid, f_grid_ts, kind='nearest', bounds_error=False, fill_value="extrapolate")
        fsub[ok] = interp_2ndpass(log_msub[ok])

    elif interp == "log":
        # Avoid issues in log interpolation
        zero = f_grid_ts == 0.0
        f_grid_ts[zero] = 1e-9

        # First linearly interpolate within the tabulated range
        interp_1stpass = interp1d(log_msub_grid, np.log10(f_grid_ts), kind='linear', bounds_error=False, fill_value=np.nan)
        log_fsub = interp_1stpass(log_msub)

        # Second, extrapolate beyond tabulated range by fixing to end-point values
        ok = np.isnan(log_fsub)
        interp_2ndpass = interp1d(log_msub_grid, np.log10(f_grid_ts), kind='nearest', bounds_error=False, fill_value="extrapolate")
        log_fsub[ok] = interp_2ndpass(log_msub[ok])

        fsub = 10**log_fsub

        if len(fsub[np.isnan(fsub)]) > 0:
            print "integrator_functions.Interpolate_F_grid(): We have nans in the interpolated rates: something went wrong"
            quit()

    else:
        print "Error, interp",interp,"not recognised"
        quit()

    return fsub

def Interpolate_F_grid(f_grid, t_grid, t_ts, ind_t_lo, ind_t_hi, log_msub, log_msub_grid, interp="log"):

    if t_ts < t_grid.min(): # Fix efficiencies to earliest grid point for earlier times
        f_grid_ts = f_grid[-1] # By convention, the time grid is sorted from z=0 to high-z

    elif t_ts > t_grid.max():
        if t_ts > t_grid.max()+0.01:
            print "Warning: wasn't expecting t_ts to be larger than the time grid"
            print t_ts, t_grid

        f_grid_ts = f_grid[0]

        # Deal with any infinities etc
        ok = (np.isnan(f_grid_ts) == False) & (f_grid_ts != np.inf) & (f_grid_ts != -np.inf)
        log_msub_grid = log_msub_grid[ok]
        f_grid_ts = f_grid_ts[ok]
        
    else:
        if t_ts < t_grid[ind_t_lo] or t_ts > t_grid[ind_t_hi]:
            print t_ts, t_grid[ind_t_lo], t_grid[ind_t_hi]
            print "Error: ind_t_lo or ind_t_hi have been calculated incorrectly"
            quit()

        f_grid_ts = f_grid[ind_t_lo] + (t_ts - t_grid[ind_t_lo]) * (f_grid[ind_t_hi]-f_grid[ind_t_lo]) / (t_grid[ind_t_hi] - t_grid[ind_t_lo])

        # If there are NaNs in one (but not both) of the redshift bins, fix those halo mass bins to the value in the other redshift bin
        fix_lo = np.isnan(f_grid[ind_t_lo]) & (np.isnan(f_grid[ind_t_hi]==False)==False)
        f_grid_ts[fix_lo] = f_grid[ind_t_hi][fix_lo]

        fix_hi = np.isnan(f_grid[ind_t_hi]) & (np.isnan(f_grid[ind_t_lo]==False)==False)
        f_grid_ts[fix_hi] = f_grid[ind_t_lo][fix_hi]

        # Do the same for any infinities
        fix_lo = (f_grid[ind_t_lo]==np.inf) & (f_grid[ind_t_hi]!=np.inf)
        f_grid_ts[fix_lo] = f_grid[ind_t_hi][fix_lo]

        fix_hi = (f_grid[ind_t_hi]==np.inf) & (f_grid[ind_t_lo]!=np.inf)
        f_grid_ts[fix_hi] = f_grid[ind_t_lo][fix_hi]
        
        # Fix rare double infinity problem
        fix_both = (f_grid[ind_t_lo]==np.inf) & (f_grid[ind_t_hi]==np.inf)
        f_grid_ts[fix_both] = 0.0

    if f_grid_ts.min() < 0.0:
        print "Error in f_grid, less than 0"
        quit()

    if interp == "linear":
        # First linearly interpolate within the tabulated range
        interp_1stpass = interp1d(log_msub_grid, f_grid_ts, kind='linear', bounds_error=False, fill_value=np.nan)
        fsub = interp_1stpass(log_msub)

        # Second, extrapolate beyond tabulated range by fixing to end-point values
        ok = np.isnan(fsub)
        interp_2ndpass = interp1d(log_msub_grid, f_grid_ts, kind='nearest', bounds_error=False, fill_value="extrapolate")
        fsub[ok] = interp_2ndpass(log_msub[ok])

    elif interp == "log":
        # Avoid issues in log interpolation
        zero = f_grid_ts == 0.0
        f_grid_ts[zero] = 1e-9

        # First linearly interpolate within the tabulated range
        interp_1stpass = interp1d(log_msub_grid, np.log10(f_grid_ts), kind='linear', bounds_error=False, fill_value=np.nan)
        log_fsub = interp_1stpass(log_msub)

        # Second, extrapolate beyond tabulated range by fixing to end-point values
        ok = np.isnan(log_fsub)
        interp_2ndpass = interp1d(log_msub_grid, np.log10(f_grid_ts), kind='nearest', bounds_error=False, fill_value="extrapolate")
        log_fsub[ok] = interp_2ndpass(log_msub[ok])

        fsub = 10**log_fsub

        if len(fsub[np.isnan(fsub)]) > 0:
            print "integrator_functions.Interpolate_F_grid(): We have nans in the interpolated rates: something went wrong"
            quit()

    else:
        print "Error, interp",interp,"not recognised"
        quit()

    return fsub


def Keep_max_common_id(var_in, ID_in):
    # For an array "var", with each element having a "non-unique" identifier "ID",
    # return an array with the same shape as var, 
    # but with every value in "var" that is not the highest value with a common ID set to zero
    # e.g. "var" = [ 2, 1, 5, 4, 3], "ID" = [ 0, 0, 0, 1, 0]
    # returns [ 0, 0, 5, 4, 0 ]
    
    order_var = np.argsort(var_in)[::-1]
    reverse_var = np.argsort(order_var)
    var = var_in[order_var]
    ID = ID_in[order_var]

    order_ID = np.argsort(ID)
    reverse_ID = np.argsort(order_ID)
    junk, ind = np.unique(ID, return_index=True)
    var_max = np.zeros_like(var)
    var_max[ind] += var[ind]

    var_max = var_max[reverse_var]
    
    return var_max
