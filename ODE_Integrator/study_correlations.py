import numpy as np
import h5py
import os
import sys
sys.path.append("../.")
import utilities_statistics as us

test_mode = False
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

z_choose = 1.0

#sim_name = "L0100N1504_REF_200_snip"
sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"

# Read catalogue data to select a galaxy/halo
filename_subcat = "subhalo_catalogue_" + sim_name
filename_subcat += ".hdf5"

File = h5py.File(catalogue_path+filename_subcat,"r")

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snapshots = snapshot_numbers
snipshots = sn_i_a_pshots_simulation

File.close()



props = ["mass_new_stars_init", "subgroup_number", "mass_star","isInterpolated", "m200_host"]
props += ["mass_gas_NSF_ISM", "mass_gas_NSF", "mass_gas_SF"]
props += ["mass_sf_ism_join_wind_0p25vmax", "mass_nsf_ism_join_wind_0p25vmax", "mass_ism_reheated_join_wind_0p25vmax"]
props += ["mass_galtran", "mass_prcooled", "mass_recooled_0p25vmax"]
props += ["mass_sf_ism_reheated", "mass_nsf_ism_reheated","mass_recooled_ireheat","mass_recooled_0p5vmax"]
props += ["mass_praccreted", "mass_reaccreted_0p25vmax", "mass_fof_transfer"]
props += ["mass_join_halo_wind_0p25vmax", "mass_halo_reheated_join_wind_0p25vmax"]
props += ["mass_reaccreted_0p5vmax", "mass_halo_reheated", "mass_reaccreted_hreheat"]
props += ["mass_diffuse_dm_accretion"]

# Read catalogue data to select a galaxy/halo
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"r")

i_snap = np.argmin(abs(z_snapshots-z_choose))
snap = snapshots[i_snap]
snip = snipshots[i_snap]

print "Looking at tree snapshot", snap

snapnum_group = File["Snap_"+str(snap)]
subhalo_group = snapnum_group["subhalo_properties"]
    
data = {}

for prop in props:
    data[prop] = subhalo_group[prop][:]

ok = (np.isnan(data["mass_new_stars_init"])==False) & (data["subgroup_number"]==0) & (data["isInterpolated"]==0)

for prop in props:
    data[prop] = data[prop][ok]

# CGM terms
cgm_inflow = data["mass_praccreted"] + data["mass_reaccreted_0p5vmax"] + data["mass_fof_transfer"] + data["mass_reaccreted_hreheat"]
cgm_outflow = data["mass_halo_reheated"]

# CGM/ISM terms
ism_inflow = data["mass_prcooled"] + data["mass_recooled_0p5vmax"] + data["mass_galtran"] + data["mass_recooled_ireheat"]
ism_outflow = data["mass_sf_ism_reheated"] + data["mass_nsf_ism_reheated"] 

# ISM terms
sfr = data["mass_new_stars_init"]

bin_mh = np.arange(10.,14.5,0.2)

from utilities_plotting import *
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*3,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':6})

py.subplot(231)

# CGM inflow
med, lo, hi, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(cgm_inflow),weights=None,min_count=1,lohi=(0.16,0.84))

# CGM inflow, controlled for (total) dm halo growth rates
control = data["mass_diffuse_dm_accretion"]
med2, lo2, hi2, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(cgm_inflow/control),weights=None,min_count=1,lohi=(0.16,0.84))
dlo = med2 - lo2; dhi = hi2 - med2

py.plot(bin_mh_mid, med - bin_mh_mid, c="k", linewidth=1.2, label="raw")
py.plot(bin_mh_mid, lo - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)
py.plot(bin_mh_mid, hi - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)

py.plot(bin_mh_mid, med + dhi - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5, label=r"Controlled for $\dot{M}_{\mathrm{DM}}^{\mathrm{in}}$")
py.plot(bin_mh_mid, med - dlo - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5)

py.ylabel(r"$\log(M_{\mathrm{CGM}}^{\mathrm{in}} / M_{200})$")
py.legend(loc="upper left",frameon=False)
#py.xlabel(r"$t \, / \mathrm{Gyr}$")
#py.ylabel(r"$M_{\mathrm{CGM}} \, / \mathrm{M_\odot}$")

py.subplot(232)

# CGM outflow
med, lo, hi, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(cgm_outflow),weights=None,min_count=1,lohi=(0.16,0.84))

# CGM outflow, controlled for star formation rates
control = sfr
med2, lo2, hi2, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(cgm_outflow/control),weights=None,min_count=1,lohi=(0.16,0.84))
dlo = med2 - lo2; dhi = hi2 - med2

py.plot(bin_mh_mid, med - bin_mh_mid, c="k", linewidth=1.2, label="Raw")
py.plot(bin_mh_mid, lo - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)
py.plot(bin_mh_mid, hi - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)

py.plot(bin_mh_mid, med + dhi - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5,label="Controlled for SFR")
py.plot(bin_mh_mid, med - dlo - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5)

py.ylabel(r"$\log(M_{\mathrm{CGM}}^{\mathrm{out}} / M_{200})$")
py.legend(loc="upper left",frameon=False)

py.subplot(233)

# ISM inflow
med, lo, hi, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(ism_inflow),weights=None,min_count=1,lohi=(0.16,0.84))

# ISM inflow, controlled for CGM mass
control = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]
med2, lo2, hi2, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(ism_inflow/control),weights=None,min_count=1,lohi=(0.16,0.84))
dlo = med2 - lo2; dhi = hi2 - med2

py.plot(bin_mh_mid, med - bin_mh_mid, c="k", linewidth=1.2, label="Raw")
py.plot(bin_mh_mid, lo - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)
py.plot(bin_mh_mid, hi - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)

py.plot(bin_mh_mid, med + dhi - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5, label=r"Controlled for $M_{\mathrm{CGM}}$")
py.plot(bin_mh_mid, med - dlo - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5)

py.ylabel(r"$\log(M_{\mathrm{ISM}}^{\mathrm{in}} / M_{200})$")
py.legend(loc="upper left",frameon=False)

py.subplot(234)

# ISM outflow
med, lo, hi, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(ism_outflow),weights=None,min_count=1,lohi=(0.16,0.84))

# ISM outflow, controlled for CGM mass
control = sfr
med2, lo2, hi2, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(ism_outflow/control),weights=None,min_count=1,lohi=(0.16,0.84))
dlo = med2 - lo2; dhi = hi2 - med2

py.plot(bin_mh_mid, med - bin_mh_mid, c="k", linewidth=1.2, label="Raw")
py.plot(bin_mh_mid, lo - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)
py.plot(bin_mh_mid, hi - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)

py.plot(bin_mh_mid, med + dhi - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5, label=r"Controlled for SFR")
py.plot(bin_mh_mid, med - dlo - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5)

py.ylabel(r"$\log(M_{\mathrm{ISM}}^{\mathrm{out}} / M_{200})$")
py.legend(loc="upper left",frameon=False)

py.subplot(235)

# SFR
med, lo, hi, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(sfr),weights=None,min_count=1,lohi=(0.16,0.84))

# ISM outflow, controlled for CGM mass
control = data["mass_gas_SF"] + data["mass_gas_NSF_ISM"]
med2, lo2, hi2, bin_mh_mid = us.Calculate_Percentiles(bin_mh,np.log10(data["m200_host"]),np.log10(sfr/control),weights=None,min_count=1,lohi=(0.16,0.84))
dlo = med2 - lo2; dhi = hi2 - med2

py.plot(bin_mh_mid, med - bin_mh_mid, c="k", linewidth=1.2, label="Raw")
py.plot(bin_mh_mid, lo - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)
py.plot(bin_mh_mid, hi - bin_mh_mid, c="k", linewidth=0.7, alpha=0.5)

py.plot(bin_mh_mid, med + dhi - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5, label=r"Controlled for SFR")
py.plot(bin_mh_mid, med - dlo - bin_mh_mid, c="r", linewidth=0.7, alpha=0.5)

py.ylabel(r"$\log(SFR / M_{200})$")
py.legend(loc="upper left",frameon=False)



#fig_name = "single_galaxy_stellar_mass_integration_test.pdf"
#py.rcParams.update({'savefig.dpi':150})
#py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_SHM/'+fig_name)
py.show()
