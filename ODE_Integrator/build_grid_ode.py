import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

test_mode = False
do_dm = True
do_bias_cgm_out = False
halo_mass = "m200"

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc REF with 28 snaps
'''sim_name = "L0025N0376_REF_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_final = 200
snap_list = np.arange(1,snap_final+1)[::-1] # These trees start at 1
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]'''

# 25 Mpc - recal (high-res), 202 snipshots
sim_name = "L0025N0752_Recal"
#sim_name = "L0025N0752_Recal_m200_smthr_abslo"
#sim_name = "L0025N0752_Recal_m200_smthr_abshi"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/tree_202.0.hdf5"
snap_final = 202
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snip_list = np.rint(snipshots[::-1]).astype("int")
snap_list = np.rint(snapshots[::-1]).astype("int")

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()

omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(9.0, 13.0,0.2)
# Use larger bins at high mass
bin_mh2 = np.array([13.0, 13.3, 13.6, 14.0, 15.0])
bin_mh = np.concatenate((bin_mh, bin_mh2))
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])


File_tree = h5py.File(tree_file,"r")
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()


lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

# Compute the median expansion factor for each bin
jimbo = [[]]
counter = 0
for i_snap, snap in enumerate(snap_list[:-1]):

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    if ind_bin_snap > len(jimbo)-1:
        jimbo.append([])
        
    jimbo[ind_bin_snap].append(a[i_snap])

bin_a_median = np.zeros(len(lna_mid_choose))
for n in range(len(lna_mid_choose)):
    bin_a_median[n] = np.median(jimbo[n])
print "median expansion factor for each redshift bin", bin_a_median

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

bin_mh_r200_med = np.zeros((len(a_max), len(bin_mh_mid)))
bin_mh_vmax_med = np.zeros((len(a_max), len(bin_mh_mid)))

name_list = ["mchalo","subgroup_number","m200_host","r200_host","vmax"]
name_list += ["mass_gas_SF", "mass_gas_NSF", "mass_gas_NSF_ISM", "mass_star", "mass_new_stars_init"]

name_list += ["mass_prcooled", "mass_galtran", "mass_praccreted", "mass_fof_transfer"]
name_list += ["mass_reaccreted_hreheat","mass_recooled_ireheat"]
name_list += ["mass_halo_reheated", "mass_nsf_ism_reheated", "mass_sf_ism_reheated"]
if "Recal" not in sim_name:
    name_list += ["mass_stellar_rec"]

if do_bias_cgm_out:
    name_list += ["mass_join_halo_wind_from_ism"]

if do_dm:
    name_list += ["mass_diffuse_dm_accretion", "mass_diffuse_dm_pristine"]
name_list += ["mass_cgm_pristine"]

fVmax_cuts = [0.25]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    name_list += ["mass_inside"+fV_str,"mass_outside_galaxy"+fV_str,"mass_outside"+fV_str]
    
    if do_bias_cgm_out:
        name_list += ["mass_reaccreted_from_wind"+fV_str]

fVmax_cuts_full = [0.25, 0.5]
for fVmax_cut in fVmax_cuts_full:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    
    name_list += ["mass_recooled"+fV_str, "mass_reaccreted"+fV_str]
    name_list += ["mass_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str,"mass_join_halo_wind"+fV_str]
    name_list += ["mass_ism_reheated_join_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str]

data_list = []
dt_list = []
t_list = []

for lna in lna_mid_choose:
    dt_list.append([])
    t_list.append([])
    data_list.append({})
    for name in name_list:
        data_list[-1][name] = []

f_name_list = ["fcool_pristine_nonorm","fcool_galtran_nonorm","facc_pristine_nonorm","facc_fof_nonorm","facc_tot_nonorm"]
f_name_list += ["sfr_nonorm", "sfr_mism_norm"]
if "Recal" not in sim_name:
    f_name_list += ["frec_star_sfr_norm"]
f_name_list += ["fcool_pristine_mpristine_norm", "fcool_pristine_mcgm_norm", "fcool_galtran_mcgm_norm", "facc_fof_mcgm_norm"]
f_name_list += ["fism_out_tot_nonorm", "fhalo_out_tot_nonorm"]

# NEW ###
if do_dm:
    f_name_list += ["facc_fof_prevent"]
# New ###

if do_dm:
    f_name_list += ["facc_pristine_prevent"]

for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    
    f_name_list += ["fcool_recooled_nonorm"+fV_str, "facc_reaccreted_nonorm"+fV_str]
    f_name_list += ["fwind_ism_nonorm"+fV_str, "fwind_halo_nonorm"+fV_str]

    f_name_list += ["fcool_reheated_nonorm"+fV_str, "facc_reheated_nonorm"+fV_str]
    f_name_list += ["fism_reheated_nonorm"+fV_str, "fhalo_reheated_nonorm"+fV_str]

    f_name_list += ["ism_wind_tot_ml"+fV_str, "halo_wind_tot_ml"+fV_str]
    f_name_list += ["ism_reheat_ml"+fV_str, "halo_reheat_ml"+fV_str]

    f_name_list += ["fcool_recooled_mcgm_norm"+fV_str, "fcool_reheated_mcgm_norm"+fV_str]
    f_name_list += ["facc_reaccreted_return"+fV_str,"fcool_recooled_return"+fV_str]

    # NEW ###
    f_name_list += ["fism_reheated_mism_norm"+fV_str, "fhalo_reheated_mcgm_norm"+fV_str]
    f_name_list += ["fcool_reheated_mism_norm"+fV_str, "facc_reheated_mcgm_norm"+fV_str]
    f_name_list += ["fcool_pristine_mcgm_nowind_norm"+fV_str, "fcool_galtran_mcgm_nowind_norm"+fV_str]
    # New ###

    if do_bias_cgm_out:
        f_name_list += ["bias_cgm_out"+fV_str, "bias_reacc"+fV_str, "bias_cgm_out2"+fV_str, "bias_reacc2"+fV_str]

f_med_dict = {}

for f_name in f_name_list:
    f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

f_med_dict2 = {}
f_name_list2 = ["bin_mh_med"]
for f_name in f_name_list2:
    f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))


filename = "processed_subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

print filename

print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    if name_list[-1] in subhalo_group:

        #All galaxies on this time bin have the same value of ts - I just need to know which bin that fills in within av

        data = subhalo_group[name_list[-1]][:]

        ok = (np.isnan(data)==False)

        for i_name, name in enumerate(name_list):

            data = subhalo_group[name][:]
            if name == "Group_R_Crit200_host" or name == "r200_host":
                data *= a[i_snap] # pMpc h^-1
            ngal_tot = len(data)

            data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])
            
        ngal = len(data[ok])
        print ngal, ngal_tot

        dt = t[i_snap] - t[i_snap+1]
        dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)
        t_list[ind_bin_snap] = np.append(t_list[ind_bin_snap], np.zeros(ngal)+t[i_snap])

        z = 1./a[i_snap]-1

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])
    else:
        print "no data for this snap"

for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]
    t_gal = t_list[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue
               

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = (data["mass_gas_NSF"]+data["mass_gas_SF"]+data["mass_star"]) >= 0
    dt = dt[ok_measure]
    t_gal = t_gal[ok_measure]
    for name in name_list:
        data[name] = data[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22

    R200 = data["r200_host"] /h * 1e3

    vmax = data["vmax"] # kms
    
    mchalo = data["mchalo"]
    if halo_mass == "m200":
        #print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["m200_host"]
    else:
        mchalo *= dm2tot_factor

    subgroup_number = data["subgroup_number"]

    #### Compute some percentile efficiencies ########
    cent = subgroup_number == 0
    weight_cent = np.zeros_like(dt[cent])
    dt_u = np.unique(dt)
    for dt_u_i in dt_u:
        ok2_cent = dt[cent] == dt_u_i
        weight_cent[ok2_cent] = dt[cent][ok2_cent] / len(dt[cent][ok2_cent])

    y = np.log10(mchalo[cent])
    f_med_dict2["bin_mh_med"][i_bin], jk,jk,jk,jk = us.Calculate_Percentiles(bin_mh, y, y, np.ones_like(weight_cent),5,lohi=(0.16,0.84),complete=0.8)

    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)

        weight_cent = np.zeros_like(dt[ok_cent])
        
        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            
        for f_name in f_name_list:
            
            for fVmax_cut in fVmax_cuts_full:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                if fV_str in f_name:
                    break

            if "fcool_pristine" in f_name:
                mass_out = data["mass_prcooled"]
            elif "fcool_recooled" in f_name:
                mass_out = data["mass_recooled"+fV_str]
            elif "fcool_galtran" in f_name:
                mass_out = data["mass_galtran"]
            elif "fcool_reheated" in f_name:
                mass_out = data["mass_recooled_ireheat"] - (data["mass_recooled"+fV_str]-data["mass_recooled_0p5vmax"])
            elif "facc_tot" in f_name:
                mass_out = data["mass_praccreted"] + data["mass_reaccreted_0p5vmax"] + data["mass_fof_transfer"] + data["mass_reaccreted_hreheat"]
            elif "facc_pristine" in f_name:
                mass_out = data["mass_praccreted"]
            elif "facc_reheated" in f_name:
                mass_out = data["mass_reaccreted_hreheat"] - (data["mass_reaccreted"+fV_str]-data["mass_reaccreted_0p5vmax"])
            elif "facc_reaccreted" in f_name:
                mass_out = data["mass_reaccreted"+fV_str]
            elif "facc_fof" in f_name:
                mass_out = data["mass_fof_transfer"]
            elif "facc_dm" in f_name:
                if "pristine" in f_name:
                    mass_out = data["mass_diffuse_dm_pristine"]
                else:
                    mass_out = data["mass_diffuse_dm_accretion"]
            elif "fism_reheated" in f_name or "ism_reheat_ml" in f_name:

                mass_out = np.copy(data["mass_nsf_ism_reheated"] + data["mass_sf_ism_reheated"])
                mass_out -= data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] 
                mass_out -= data["mass_ism_reheated_join_wind"+fV_str]
                
                # The problem here is that reheat list particles join the wind later than when they left ISM
                # This can lead to cases where value is below 0 - fix this artificially
                mass_out[mass_out<0] *= 0.0

            elif "fism_out_tot" in f_name:
                mass_out = data["mass_nsf_ism_reheated"] + data["mass_sf_ism_reheated"]

            elif "fhalo_reheated" in f_name or "halo_reheat_ml" in f_name:
                mass_out = np.copy(data["mass_halo_reheated"])
                mass_out -= data["mass_halo_reheated_join_wind"+fV_str] + data["mass_join_halo_wind"+fV_str]
                mass_out[mass_out<0] *= 0.0
                
            elif "fhalo_out_tot" in f_name:
                mass_out = data["mass_halo_reheated"]

            elif "fwind_ism" in f_name or "ism_wind_tot_ml" in f_name:
                mass_out = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
            elif "fwind_halo" in f_name or "halo_wind_tot_ml" in f_name:
                mass_out = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]

            elif "sfr" in f_name and "sfr_norm" not in f_name:
                mass_out = data["mass_new_stars_init"]
            elif "frec_star" in f_name:
                mass_out = data["mass_stellar_rec"]

            elif f_name == "bias_cgm_out"+fV_str:
                mass_out = data["mass_join_halo_wind_from_ism"]
            elif f_name == "bias_cgm_out2"+fV_str:
                mass_out = data["mass_join_halo_wind_from_ism"] * (data["mass_gas_NSF"] -data["mass_gas_NSF_ISM"] - data["mass_inside"+fV_str])
            elif f_name == "bias_reacc"+fV_str:
                mass_out = data["mass_reaccreted_from_wind"+fV_str]
            elif f_name == "bias_reacc2"+fV_str:
                mass_out = data["mass_reaccreted_from_wind"+fV_str] * (data["mass_outside"+fV_str]-data["mass_outside_galaxy"+fV_str]+data["mass_inside"+fV_str])

            else:
                print "No", f_name
                quit()

            ##################################################################

            if "nonorm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(weight_cent) # Msun Gyr^-1

            elif "subnorm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(fb*mchalo[ok_cent]*weight_cent) # Gyr^-1
            elif "dmnorm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum((1-fb)*mchalo[ok_cent]*weight_cent) # Gyr^-1
            elif "mism_norm" in f_name:
                mass_ism = data["mass_gas_SF"] + data["mass_gas_NSF_ISM"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_ism[ok_cent]*weight_cent) # Gyr^-1
            elif "mcgm_norm" in f_name:
                mass_cgm = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm[ok_cent]*weight_cent) # Gyr^-1
            # NEW
            elif "mcgm_nowind_norm" in f_name:
                mass_cgm_nowind = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"] - data["mass_inside"+fV_str]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm_nowind[ok_cent]*weight_cent) # Gyr^-1
            # NEW
            elif "minside_norm" in f_name:
                mass_inside = data["mass_inside"+fV_str]
                if "tscaled" in f_name:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent*t_gal[ok_cent]) / np.sum(mass_inside[ok_cent]*weight_cent)
                else:
                    f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_inside[ok_cent]*weight_cent) # Gyr^-1

            elif "mpristine_norm" in f_name:
                mass_cgm = np.copy(data["mass_cgm_pristine"])
                if "tscaled" in f_name:
                    mass_cgm *= 1./t_gal
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm[ok_cent]*weight_cent) # Gyr^-1


            elif "_prevent" in f_name:

                norm = data["mass_diffuse_dm_pristine"] * dm2tot_factor*fb
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent) # dimensionless

            elif "_return" in f_name:
                if "cool" in f_name:
                    norm = np.copy(data["mass_outside_galaxy"+fV_str])
                elif "acc" in f_name:
                    norm = np.copy(data["mass_outside"+fV_str])
                else:
                    print "yikes", f_name
                    quit()

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]*weight_cent) # Gyr^-1

            elif "_ml" in f_name or "sfr_norm" in f_name:
                norm = data["mass_new_stars_init"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent)

            elif f_name == "bias_cgm_out"+fV_str:
                norm = (data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str])
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent)
            elif f_name == "bias_cgm_out2"+fV_str:
                norm = (data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str] - data["mass_join_halo_wind_from_ism"]) * data["mass_inside"+fV_str]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent)
            elif f_name == "bias_reacc"+fV_str: # Note, we could also do averaging by doing mean of each term separately, rather than mean(term1*term3) / mean(term2*term4)
                norm = data["mass_reaccreted"+fV_str]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent)
            elif f_name == "bias_reacc2"+fV_str:
                norm = (data["mass_reaccreted"+fV_str]-data["mass_reaccreted_from_wind"+fV_str]) * (data["mass_outside_galaxy"+fV_str] - data["mass_inside"+fV_str])
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]/dt[ok_cent]*weight_cent)

            else:

                print "nope", f_name
                quit()


# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_integrator"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_msub_bins", data=bin_mh)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("a_median", data=bin_a_median)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

File_grid.create_dataset("R200_med", data = bin_mh_r200_med)
File_grid.create_dataset("vmax_med", data = bin_mh_vmax_med)

for f_name in f_name_list:
    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

for f_name in f_name_list2:
    File_grid.create_dataset(f_name, data=f_med_dict2[f_name])

File_grid.close()
