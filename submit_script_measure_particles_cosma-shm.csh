#! /bin/tcsh -f

######## READ READ READ
# 25/11/20
# In another context I managed to submit to cosma6-pauper using the group "dp004" rather than "durham" - this would probably fix this submission script also now that cosma-shm has gone
# (replaced with cosma7-shm)
######### READ READ READ











# Update 19/11/20 - It looks like the cosma-shm queue has been discontinued, and merged with cosma7-shm queue
# In addition, it seems I don't have permission, or something is not set correctly (with groups etc) for it to accept with cosma7-shm atm
# Just using cordelia for now, but I might need to resolve this in the future
#
# EDIT - use "sinfo" command to see the current node breakdown and usage
# It seems that there is now
#cosma7-mad  which is one node (mad01)
#cosma7-shm  which is one node (mad02 - ie I guess the one that used to be called cosma-shm)
#cosma7-shm2  which is one node (mad03)
#cosma7-shm-pauper which is a secondary queue for mad02 I guess

# Submit jobs to cordelia queue to measure rates etc for an array of subvolumes

# Choose number of joblib subprocesses to use
set n_jobs = 4

# Choose memory allocation per node (in Mb)
# For cosma-shm, total memory available is ~ 766 Gb, across 32 cores, so in principle 24 Gb per core
#set MemPerNode = MaxMemPerNode
#set MemPerNode = 383000 # Half memory
#set MemPerNode = 191000 # quarter memory
set MemPerNode = 96000 # 1/8
#set MemPerNode = 48000 # 1/16

set logdir = /cosma7/data/dp004/d72fqv/Baryon_Cycle/LOG/

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/
#set simname = L0100N1504_REF_50_snip
### subvolume list for nsub_1d = 4
#set nvol = 0-63
#set snap = 50

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/
#set simname = L0100N1504_REF_200_snip
### subvolume list for nsub_1d = 4
##set nvol = 0-63
##set nvol = 18-18
##set nvol = "21,26,19,20,22,27,25" # Note - key here is to not put spaces in between the numbers/commas
## For now, set nvol custom list manually down below
#set snap = 200

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/REFERENCE/data/
#set simname = L0050N0752_REF_snap
#set nvol = 0-0
#set nsub1d = 1
#set snap = 28

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE/data/
##set simname = L0025N0376_REF_snap
#set simname = L0025N0376_REF_snap_par_test
## subvolume list for nsub_1d = 1
#set nsub1d = 1
#set nvol = 0-0
#set snap = 28

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/
#set simname = L0025N0376_REF_50_snip
### subvolume list for nsub_1d = 1
#set nsub1d = 1
#set nvol = 0-0
#set snap = 50

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/
#set simname = L0025N0376_REF_100_snip
### subvolume list for nsub_1d = 1
#set nsub1d = 1
#set nvol = 0-0
#set snap = 100

set datapath = /cosma7/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/
set simname = L0025N0376_REF_200_snip
## subvolume list for nsub_1d = 1
set nsub1d = 1
set nvol = 0-0
set snap = 200

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_300_snap
## subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 299

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_400_snap
## subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 399

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_500_snap
## subvolume list for nsub_1d = 1
#set nsub1d = 1
#set nvol = 0-0
#set snap = 499

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_1000_snap
## subvolume list for nsub_1d = 1
#set nsub1d = 1
#set nvol = 0-0
#set snap = 999

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/EagleVariation_fE1p00_ALPHA1p0e3/data/
#set simname = L0050N0752_CFB_snap
### subvolume list for nsub_1d = 2
#set nvol = 0-0
#set snap = 28

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/REF_NOAGN/data/
#set simname = L0050N0752_NOAGN_snap
## subvolume list for nsub_1d = 1 and partition at msub > 13.5
#set nsub1d = 1
#set nvol = 0-0
#set snap = 28

set max_jobs = 64
set script = run_script_measure_particles.csh

set jobname = mp_${simname}
set name = measure_particles_${simname}
set logname = ${logdir}/${name}_${snap}_ivol%a_jobid%A.log
\mkdir -p ${logname:h}

cat <<EOF - ${script} | sbatch --array=${nvol}%${max_jobs}
#!/bin/tcsh -ef
#
#SBATCH --ntasks 1
#SBATCH -J ${jobname}
#SBATCH -o ${logname}
#SBATCH -p cosma7-shm
#SBATCH -A durham
#SBATCH -t 72:00:00
#SBATCH --mem ${MemPerNode}
#SBATCH -c ${n_jobs}
#

# Set parameters
set datapath     = ${datapath}
set simname = ${simname}
set snap        = ${snap}
set n_jobs      = ${n_jobs}
set nsub1d     = ${nsub1d}
@ subvol        = \${SLURM_ARRAY_TASK_ID}

EOF


