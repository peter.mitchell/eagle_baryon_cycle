#! /bin/tcsh -f

# Submit jobs to cordelia queue to measure rates etc for an array of subvolumes

# Choose number of joblib subprocesses to use (this will set the number of used cpus)
set n_jobs = 4

# Choose memory allocation per node
# For cosma5, this ~ 120 Gb (blocking 16 cores though)
#set MemPerNode = MaxMemPerNode
set MemPerNode = 60000 # should block 8 cores
#set MemPerNode = 30000 # Think this is in Mb - 30000 is 30Gb - should block 4 cores
#set MemPerNode = 15000 # - should block 2 cores (to be verified) - dc beni prevented me from being sure
#set MemPerNode = 22500 # should block 3 cores (to be verified)

set logdir = /cosma7/data/dp004/d72fqv/Baryon_Cycle/LOG/

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/
#set simname = L0100N1504_REF_50_snip
### subvolume list for nsub_1d = 4
#set nvol = 1-64
#set snap = 50

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/
#set simname = L0100N1504_REF_200_snip
### subvolume list for nsub_1d = 4
##set nvol = 0-63
##set nvol = 18-18
##set nvol = "21,26,19,20,22,27,25" # Note - key here is to not put spaces in between the numbers/commas
## For now, set nvol custom list manually down below
#set snap = 200

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE/data/
#set simname = L0025N0376_REF_snap
## subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 28
#set nsub1d = 1

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/
#set simname = L0025N0376_REF_50_snip
### subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 50

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/
#set simname = L0025N0376_REF_100_snip
### subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 100

set datapath = /cosma7/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/
set simname = L0025N0376_REF_200_snip
## subvolume list for nsub_1d = 1
set nvol = 0-0
set snap = 200
set nsub1d = 1

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_300_snap
#set nsub1d = 1
# subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 299

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_400_snap
## subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 399

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_500_snap
## subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 499

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/
#set simname = L0025N0376_REF_1000_snap
## subvolume list for nsub_1d = 1
#set nvol = 0-0
#set snap = 999

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/EagleVariation_fE1p00_ALPHA1p0e3/data/
#set simname = L0050N0752_CFB_snap
### subvolume list for nsub_1d = 2
#set nvol = 0-0
#set snap = 28

#set datapath = /cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/REF_NOAGN/data/
#set simname = L0050N0752_NOAGN_snap
## subvolume list for nsub_1d = 1 and partition at msub > 13.5
#set nvol = 1-8
#set snap = 28

set max_jobs = 64
set script = run_script_measure_particles.csh

set jobname = measure_particles_${simname}
set name = measure_particles_${simname}
set logname = ${logdir}/${name}_${snap}_ivol%a_jobid%A.log
\mkdir -p ${logname:h}

cat <<EOF - ${script} | sbatch --array=${nvol}%${max_jobs}
#!/bin/tcsh -ef
#
#SBATCH --ntasks 1
#SBATCH -J ${jobname}
#SBATCH -o ${logname}
#SBATCH -p cordelia
#SBATCH -A durham
#SBATCH -t 168:00:00
#SBATCH --mem ${MemPerNode}
#

# Set parameters
set datapath     = ${datapath}
set simname = ${simname}
set snap        = ${snap}
set n_jobs      = ${n_jobs}
set nsub1d      = ${nsub1d}
@ subvol        = \${SLURM_ARRAY_TASK_ID}

EOF


