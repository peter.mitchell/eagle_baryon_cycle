#! /bin/tcsh -f

if ( ! ( $?datapath ) ) then
    # Get command line args
    echo 'number of arguments=' $#argv
    if( $#argv == 6) then
	set datapath = $1
	set simname = $2
	set snap = $3
	set n_jobs = $4	
	set nsub1d = $5
	set subvol = $6
    endif
endif

echo run_script_measure_particles.csh - arguments are $datapath, $simname, $snap, $n_jobs, $nsub1d, $subvol

python measure_particles.py -subvol $subvol -datapath $datapath -simname $simname -snap $snap -nsub1d $nsub1d -n_jobs $n_jobs -test_mode False
