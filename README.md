Data processing pipeline and scientific analysis/modelling code for understanding gas flows in cosmological simulations of galaxy formation. Scientific publications based on this pipeline can be found here:
https://arxiv.org/abs/2103.10966,
https://arxiv.org/abs/2005.10262,
https://arxiv.org/pdf/1910.09566.pdf,
https://arxiv.org/pdf/2101.11021.pdf,
https://arxiv.org/pdf/1910.05377.pdf

This is intended to be deployed on a supercomputing facility where the simulation data is locally available, where high-memory, many-core nodes are available for processing, and where the SLURM batch queue system is installed.

Note that this is (obviously) highly bespoke scientific code that was developed for a single system / class of data sets - it is not intended to be installed / distributed elsewhere. It is publically available in the name of scientific reproducibility, and also to give an example of my professional coding/data processing experience.
