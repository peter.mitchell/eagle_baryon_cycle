import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, sim_props, modes, output_names= inputs

    time_track_select = 0.0
    time_track_ptr = 0.0
    time_ptr_actual = 0.0

    # Unpack modes
    debug, ih_choose, write_SNe_energy_fraction, ism_definition, skip_dark_matter, optimise_sats, test_mode, bound_only_mode = modes

    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_t_dict = {}; halo_outflow_dict = {}; halo_emass_dict = {}; halo_mass_dict = {}; halo_shell_dict = {}

    output_mass_names = output_names[0]
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ps, sim_props_ts, sim_props_ns, boxsize = sim_props


    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    expansion_factor_ps = sim_props_ps["expansion_factor"]
    expansion_factor_ns = sim_props_ns["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    redshift_ps = sim_props_ps["redshift"]
    redshift_ns = sim_props_ns["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    gas_ps, gas_ts, gas_ns, star_ps, star_ts, star_ns = part_data


    # If this is an interpolated halo on _ts, it will not contain any particles and we should not do any further calculations
    # If this is an interpolated halo on _ps, most of the calculations will be wrong - so skip also
    # (will miss legitimate accretion that happens but capturing this would require linking to subhalo before it was interpolated)
    halo_isInterpolated = subhalo_props["isInterpolated"][i_halo]
    halo_isInterpolated_progen = subhalo_props["isInterpolated"][i_halo]
    
    # There was a rare edge case where r200 = 0, causing histograms to crash
    r200_temp = subhalo_props["r200_host"][i_halo]

    if halo_isInterpolated == 1 or halo_isInterpolated_progen == 1 or r200_temp == 0.0:
        for name in output_mass_names:
            halo_mass_dict[name] = np.nan
        timing_info = [0.0, 0.0, 0.0, 0.0]
        return halo_mass_dict,  timing_info
    # If halo is interpolated on _ns, we can still do most of the calculations, but should skip the wind parts that rely on _ns information
    halo_isInterpolated_ns = subhalo_props["isInterpolated_ns"][i_halo]
    if halo_isInterpolated_ns == 1:
        do_ns_calculation = False
    else:
        do_ns_calculation = True

    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    if test_mode and i_halo == 0:
        print "m200 = ", np.log10(subhalo_props["m200_host"][i_halo])

    # If optimize_sat==True, we skip most of the computations for satellite subhaloes, and only do the bare essentials
    if optimise_sats and halo_subgroup_number > 0:
        full_calculation = False
    else:
        full_calculation = True

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = subhalo_props["r200_host"][i_halo]
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    
    halo_x_progen = subhalo_props["x_halo_progen"][i_halo]
    halo_y_progen = subhalo_props["y_halo_progen"][i_halo]
    halo_z_progen = subhalo_props["z_halo_progen"][i_halo]
    halo_vx_progen = subhalo_props["vx_halo_progen"][i_halo]
    halo_vy_progen = subhalo_props["vy_halo_progen"][i_halo]
    halo_vz_progen = subhalo_props["vz_halo_progen"][i_halo]
    halo_vmax_progen = subhalo_props["vmax_progen"][i_halo]
    halo_r200_host_progen = subhalo_props["r200_host_progen"][i_halo]
    halo_group_number_progen = subhalo_props["group_number_progen"][i_halo]
    halo_subgroup_number_progen = subhalo_props["subgroup_number_progen"][i_halo]
    halo_subhalo_index_progen = subhalo_props["subhalo_index_progen"][i_halo]
    
    halo_x_ns = subhalo_props["x_halo_ns"][i_halo]
    halo_y_ns = subhalo_props["y_halo_ns"][i_halo]
    halo_z_ns = subhalo_props["z_halo_ns"][i_halo]
    halo_vx_ns = subhalo_props["vx_halo_ns"][i_halo]
    halo_vy_ns = subhalo_props["vy_halo_ns"][i_halo]
    halo_vz_ns = subhalo_props["vz_halo_ns"][i_halo]
    halo_vmax_ns = subhalo_props["vmax_ns"][i_halo]
    halo_r200_host_ns = subhalo_props["r200_host_ns"][i_halo]
    halo_group_number_ns = subhalo_props["group_number_ns"][i_halo]
    halo_subgroup_number_ns = subhalo_props["subgroup_number_ns"][i_halo]
    halo_subhalo_index_ns = subhalo_props["subhalo_index_ns"][i_halo]


    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ##### Select star particles that belong to desired group, subgroup
    select_star = mpf.Match_Index(halo_subhalo_index, star_ts.subhalo_index_unique, star_ts.particle_subhalo_index)

    id_star = mpf.Index_Array(star_ts.data["id"],select_star,None)
    order_star = np.argsort(id_star)
    id_star = id_star[order_star]

    # Convert to Msun
    g2Msun = 1./(1.989e33)
    mass_star = np.copy(mpf.Index_Array(star_ts.data["mass"],select_star,order_star))
    mass_star *= 1.989e43 * g2Msun / h

    mass_star_init = np.copy(mpf.Index_Array(star_ts.data["mass_init"],select_star,order_star))
    mass_star_init *= 1.989e43 * g2Msun / h

    ###### Select central progenitor gas particles #########
    select_gas_progen = mpf.Match_Index(halo_subhalo_index_progen, gas_ps.subhalo_index_unique, gas_ps.particle_subhalo_index)

    id_gas_progen = mpf.Index_Array(gas_ps.data["id"],select_gas_progen,None)
    order_gas_progen = np.argsort(id_gas_progen)
    id_gas_progen = id_gas_progen[order_gas_progen]

    ##### Select progenitor star particles that belong to desired group, subgroup
    select_star_progen = mpf.Match_Index(halo_subhalo_index_progen, star_ps.subhalo_index_unique, star_ps.particle_subhalo_index)
    
    id_star_progen = mpf.Index_Array(star_ps.data["id"],select_star_progen,None)
    order_star_progen = np.argsort(id_star_progen)
    id_star_progen = id_star_progen[order_star_progen]

    mass_star_progen = np.copy(mpf.Index_Array(star_ps.data["mass"],select_star_progen,order_star_progen))
    mass_star_progen *= 1.989e43 * g2Msun / h

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ########################## Do the rate measurements #####################################


    ###### Identify newly formed star particles that were in the main progenitor subhalo on _ps
    if len(id_star) > 0 and len(id_gas_progen) >0:
        ptr, time_ptr_actual = ms.match(id_star, id_gas_progen,time_taken=time_ptr_actual,arr2_sorted=True)
        ok_match = ptr >= 0

        new_stars = ok_match
        halo_mass_dict["mass_new_stars_init_sat"] += np.sum(mass_star_init[new_stars])
        halo_mass_dict["mass_stellar_rec"] += np.sum((mass_star_init-mass_star)[new_stars])

    # Compute stellar recycling for stars that already existed on the previous step
    if len(id_star_progen) > 0 and len(id_star) > 0:
        ptr = ms.match(id_star, id_star_progen)
        ok_match = ptr >= 0

        halo_mass_dict["mass_stellar_rec"] += np.sum(mass_star_progen[ptr][ok_match]) - np.sum(mass_star[ok_match])

    '''if i_halo == ih_choose:
        print ""
        print halo_mass_dict["mass_new_stars_init_sat"], halo_mass_dict["mass_stellar_rec"]
        if halo_mass_dict["mass_new_stars_init_sat"] > 0:
            print halo_mass_dict["mass_stellar_rec"]/halo_mass_dict["mass_new_stars_init_sat"]
        print ""'''

    time_track_ptr += time.time() - time_ptr

    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop, time_ptr_actual]

    return halo_mass_dict, timing_info
