import time
t1 = time.time()
t0 = t1

# Where to read/write cat files
#catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

verbose = False
debug = False

extra_wind_measurements = False
if extra_wind_measurements:
    print "Running with extra wind measurements"
extra_accretion_measurements = False
if extra_accretion_measurements:
    print "Running with extra accretion measurements"

subsample =False
n_per_bin = 1000

use_restart =True
restart_interval = 1 # Snapshot frequency with which restarts are written

# In addition to saving temporary restarts (which are deleted as calculation proceeds), can choose to permanently store tracked information at certain redshifts
save_track_lists = True
# For 28 snapshots, these are 27, 23, 19, 15 
z_save = [0.1, 0.5, 1.0, 2.0]

if restart_interval > 1:
    print "I think I broke restart interval > 1 - will be deleted with current implementation!"
    quit()

# If we true, we make the approximation that all particles that have left the FoF group are definitely in a halo wind
# This significantly reduces the IO cost, but will break dir_heated measurements (and anything else that needs properties (besides coordinates) outside of haloes)
bound_only_mode = True
if bound_only_mode:
    print "Running in bound-only mode"

    # When possible, retain memory between steps (this increases memory usage but reduces IO cost)
    retain_memory = True
    if retain_memory:
        print "Running in retain memory mode"
else:
    retain_memory = False
    print "Not runing in bound-only mode"

# If true, find redefine the central subhalo as being all particles within r200_crit
# (that are not subfind bound to other satellites - and I think also not part of other FoF groups)
central_r200 = True

ism_definition = "Joop_version"
#ism_definition = "Mitchell18_adjusted"
print "Running with ism definition", ism_definition

# Depending on the ISM defn, we may be able to skip reading the DM particles. This means the DM accretion/merger rates won't be computed.
# But these can be run in a separate script later if needed.
if ism_definition == "Joop_version" and not extra_wind_measurements:
    skip_dark_matter = True
    print "Skipping dark matter"
else:
    skip_dark_matter = False

# f_Vmax cut - the velocity fraction(s) of Vmax used to select particles in a wind.
# Note, using anything under 0.125 won't work too well with ISM wind cut, because I take min(0.125,fVmax_cuts.max()) as the instantatneous velocity cut  
fVmax_cuts = [0.125, 0.25, 0.5]
i_cut_fid = 1 # This is the index of the above list that sets which cut we will use for "cumulative" measurements
#fVmax_cuts = [0.25]
#i_cut_fid = 0
fVmax_info = [fVmax_cuts,i_cut_fid]

ns_tdyn = True # Set to true to use a fixed fraction of the halo dynamical time to use for _ns
# tdyn_frac_ns is a parameter that controls the fraction of a halo dynamical time used for finding _ns for wind slection
tdyn_frac_ns = 0.25

if ns_tdyn:
    print "Looking a fixed time interval into the future for _ns, using tdyn_frac_ns", tdyn_frac_ns

# Time (as fraction of halo dynamical time) to remove particles from reheat lists for ISM/halo
reheat_tdyn_frac_ism = 3
reheat_tdyn_frac_halo = 3
print "Cleaning ireheat/hreheat after", reheat_tdyn_frac_ism, reheat_tdyn_frac_halo

# Temperature cut for gas accretion partition between cold/hot
TLo_cut = 10**5.5 # Kelvin

# Smooth acc thr (Msun) - mass threshold below which merging subhaloes are considered as smooth accretion for inflows
# Decide whether to use fixed mass cuts, or cuts that vary as a fixed fraction of m200
mass_cuts = "absolute"
#mass_cuts = "fraction"
if mass_cuts != "absolute" and mass_cuts != "fraction":
    print "Error: mass_cuts=",mass_cuts,"must be 'absolute' or 'fraction'"
    quit()

mfrac_cut = 1e-3 # For now lets use this for both m200 and smooth acr for now
if mass_cuts == "fraction":
    print "Using a fractional mass cut for smooth acr thr of", mfrac_cut

# This corresponds to 100 dm particles at standard eagle resolution
smooth_acc_thr = 969504631.0
# This corresponds to 1000 dm particles at standard eagle resolution
#smooth_acc_thr = 9695046310.0
# This corresponds to 10 dm particles at standard eagle resolution
#smooth_acc_thr = 96950463.1
if mass_cuts == "absolute":
    print "Using smooth accretion threshold mass", smooth_acc_thr

# If true, cut all halos below this halo mass in the analysis (note gas/dm halo accretion rates still include haloes down to smooth acc thr)
# But these haloes will not be included for wind/ejecta type analysis - so this is also the minimum mass to be included in galactic transfer and recycling measurements
# This is desirable because this way we control the definition of those quantities, rather than it just being set implicitly by the simulation resolution.
cut_m200 = True
m200_cut = 969504631.0 # Msun - 100 dm particles at standard Eagle resolution
#m200_cut = 9695046310.0 # Msun - 1000 dm particles at standard Eagle resolution
#m200_cut = 96950463.1 # Msun - 10 dm particles at standard Eagle resolution
if cut_m200:
    print "Running with m200 cut of", m200_cut
else:
    print "Running without an m200 cut"

#m200_cut = 0.0
#print ""
#print "Trying with no subhalo mass cut"
#print ""

# Low-mass satellites are (when added together) very expensive inside the main loop if they have to cross-match against the (often much larger) FOF particle lists
# To mitigate this, the below option approximates the solution for satellite wind/ejecta etc, and does not perform any measurements for the satellites themselves
optimise_sats = True
if optimise_sats:
    print "Running with satellite optimisations"

# Maximum subvolume size (at a given snapshot) before splitting the subvolume into 8 smaller subvolumes when performing IO
# and doing a first round of particle selection - This is only used if we are using Peano-Hilbert IO
volmax = (0.6777 * 25 * 1e5)**3 # Mpc h^-1

import sys
import argparse
import measure_particles_functions as mpf

parser = argparse.ArgumentParser()
parser.add_argument("-subvol", help = "subvolume",type=int)
parser.add_argument("-nsub1d", help = "number of subvolumes",type=int)
parser.add_argument("-datapath", help = "directory containting simulation data")
parser.add_argument("-simname", help = "sim/tree name")
parser.add_argument("-snap", help = "final snapshot to measure for",type=int)
parser.add_argument("-test_mode", type=mpf.str2bool)
parser.add_argument("-n_jobs", help = "number of parallel joblib subprocesses",type=int)

if len(sys.argv)!=15:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()
 
subvol = args.subvol
nsub1d = args.nsub1d
DATDIR = args.datapath
sim_name = args.simname
snap_end = args.snap
test_mode = args.test_mode
n_jobs_parallel=args.n_jobs # Number of sub-processes to spawn for executing the joblib loops

# If there is one subvolume, it's always better to do custom IO if possible
if nsub1d == 1:
    volmax = 1e5**3

if test_mode:
    print ""
    print "Running in test mode"
    print ""
    output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_test_mode/"

    skip_start_hack = False
    if skip_start_hack:
        print ""
        print "Warning: hacking test mode to skip starting snapshots"
        print ""

    use_restart = False

else:
    output_path = catalogue_path + sim_name+"_subvols_nsub1d_"+str(nsub1d)+"/"
    if subsample:
        output_path = catalogue_path+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
    skip_start_hack = False

# For snapshot data, star particles have a variable that enables the calculation of the injected supernova energy
# I'm hijacking this to also ask if we want to compute rate of particles being directly heated in the ISM wind and halo wind
if "snap" in sim_name:
    write_SNe_energy_fraction = True
else:
    write_SNe_energy_fraction = False

import read_eagle as re
import numpy as np
import warnings
np.seterr(all='ignore')
import os
import h5py
from joblib import Parallel, delayed
from os import listdir
import shutil
import match_searchsorted as ms
import utilities_cosmology as uc
import measure_particles_io as mpio
import measure_particles_classes as mpc
import main_loop as main

# First let's work out the relationship between tree snapshots and simulation snapshots/snipshots
cat_path = catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5"

# Not-ideal hacky solution to not wanting duplicate catalogue files for variant analysis runs
if "L0025N0752_Recal" in sim_name:
    cat_path = catalogue_path + "subhalo_catalogue_L0025N0752_Recal.hdf5"
if "L0025N0376_REF_200_snip" in sim_name:
    cat_path = catalogue_path + "subhalo_catalogue_L0025N0376_REF_200_snip.hdf5"

n_tries = 0
success = True
# This can fail if two jobs try to read the catalogue at the same time - get round this by waiting 
while n_tries < 50:
    try:
        cat_file = h5py.File(cat_path,"r")
        tree_snapshot_info = cat_file["tree_snapshot_info"]
        snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
        redshifts = tree_snapshot_info["redshifts_tree"][:]
        sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
        cosmic_time = tree_snapshot_info["cosmic_times_tree"][:]
        cat_file.close()
        success = True
        break
    except:
        print "Failed to read base catalogue, trying again in 10 seconds", cat_path
        n_tries += 1
        time.sleep(10)

if not success:
    print "Error: failed to read base catalogue, stopping here"
    quit()

# Bad convention to refer to simulation snipshots/snapshots as "snipshots" from here on in
# And to refer to tree outputs as "snapshots" from here on in
ok = snapshot_numbers <= snap_end
snap_list = snapshot_numbers[ok][::-1]
snip_list = sn_i_a_pshots_simulation[ok][::-1]
cosmic_t_list = cosmic_time[ok][::-1]
redshifts = redshifts[ok][::-1]

# Determine which snapshots to permanently save tracked information for
if save_track_lists:
    snap_save = []
    for z_save_i in z_save:
        ok = (z_save_i <= redshifts.max()) & (z_save_i >= redshifts.min())
        if ok:
            i_snap = np.argmin(abs(redshifts - z_save_i))
            snap_save_i = snap_list[i_snap]
        else:
            snap_save_i = -99
        snap_save.append(snap_save_i)


# Added on 18/2/2020 - prevent tret bins from depending on restart redshift.
cosmic_t_list_initial = np.copy(cosmic_t_list)

def UnConcatenate(arrays,nparts):
    
    list_out = []
    nparts_cumul = np.cumsum(nparts)
    for i_sub in range(len(nparts)):
        if i_sub==0:
            ind_prev = 0
        else:
            ind_prev = nparts_cumul[i_sub-1]
        list_out.append(arrays[ind_prev:nparts_cumul[i_sub]])

    return list_out


'''skip = 4
print ""
print "temp hack, skipping first",skip,"outputs"
print ""
snip_list = snip_list[skip:]
snap_list = snap_list[skip:]
cosmic_t_list = cosmic_t_list[skip:]'''

# Variable names of the various tracked quantities
ejecta_names = ["id", "metallicity", "mass", "t","from_mp"] # Particles ejected from the FOF group (in the wind)
wind_names = ["id","mass","t","metallicity","from_SF","from_mp","left_halo"] # Particles ejected from the ISM (in the wind)
ireheat_names = ["id","mass","time","metallicity","from_SF","from_wind","from_mp"] # Particles ejected from the FOF group (not in the wind)
hreheat_names = ["id","metallicity","mass","time","from_mp"] # Particles ejected from the ISM (not in the wind)
if extra_accretion_measurements:
    ejecta_names += ["mchalo_ej"]; wind_names += ["mchalo_ej"]
    hreheat_names += ["mchalo_ej"]; ireheat_names += ["mchalo_ej"]
if extra_accretion_measurements and not bound_only_mode:
    ejecta_names += ["rmax"]; wind_names += ["rmax"]
    hreheat_names += ["rmax"]; ireheat_names += ["rmax"]
if write_SNe_energy_fraction:
    ireheat_names += ["oxygen","iron"]
    hreheat_names += ["oxygen","iron"]
if len(fVmax_cuts) > 1:
    hreheat_names += ["vpmax"]
    ireheat_names += ["vpmax"]
nsfism_names = ["id","from_SF","from_wind","vpmax"] # Particles in the non-star-forming ISM
ism_names = ["id", "cool_origin"] # Things we track in ism, irrespective of SF or NSF
cumul_names = ["mass_ism_wind", "n_ism_wind", "mass_ism_to_halo_wind_first_time", "n_ism_to_halo_wind_first_time"] # Cumulative properties integrated over the galaxies merger tree to date

cumul_out_names = ["mass_ism_wind_cumul_list_ps", "n_ism_wind_cumul_list_ps", "mass_ism_to_halo_wind_first_time_cumul_list_ps", "n_ism_to_halo_wind_first_time_cumul_list_ps"] # This will be used only to do IO to/from restart files

retain_ts = False
retain_ns = False

####### Look for a restart file and use it if use_restart set to True #########
if use_restart:
    for snip in snip_list[:-1]:
        output_path_ts = output_path + "Snip_"+str(snip)+"/"
        restart_path = output_path_ts + "restart_file_"+sim_name+"_snip_"+str(snip)+"_"+str(subvol)+".hdf5"

        if os.path.isfile(restart_path):
            print "Found restart file at snip", snip,"restarting at this point"

            restart_file = h5py.File(restart_path,"r")

            descendant_index_list_ps = restart_file["descendent_index_list"][:]
            grn_list_ps = restart_file["grn_list_ps"][:]
            sgrn_list_ps = restart_file["sgrn_list_ps"][:]

            cumul_data_ps = {}
            for name, name2 in zip(cumul_names, cumul_out_names):
                cumul_data_ps[name] = restart_file[name2][:]
                
            nejecta_list_ps = restart_file["nejecta_list_ps"][:]
            ejecta_data_ps = {}
            for name in ejecta_names:
                ejecta_data_ps[name] = UnConcatenate(restart_file[name+"_gas_ejecta_list_ps"][:], nejecta_list_ps)

            nhalo_reheat_list_ps = restart_file["nhalo_reheat_list_ps"][:]
            hreheat_data_ps = {}
            for name in hreheat_names:
                hreheat_data_ps[name] = UnConcatenate(restart_file[name+"_gas_halo_reheat_list_ps"][:],nhalo_reheat_list_ps)

            nism_reheat_list_ps = restart_file["nism_reheat_list_ps"][:]
            ireheat_data_ps = {}
            for name in ireheat_names:
                ireheat_data_ps[name] = UnConcatenate(restart_file[name+"_gas_ism_reheat_list_ps"][:],nism_reheat_list_ps)

            # First n stands for "number of", not a typo!
            nnsfism_list_ps = restart_file["nnsfism_list_ps"][:]
            nsfism_data_ps = {}
            for name in nsfism_names:
                nsfism_data_ps[name] = UnConcatenate(restart_file[name+"_gas_nsfism_list_ps"][:],nnsfism_list_ps)

            nism_list_ps = restart_file["nism_list_ps"][:]
            ism_data_ps = {}
            for name in ism_names:
                ism_data_ps[name] = UnConcatenate(restart_file[name+"_gas_ism_list_ps"][:],nism_list_ps)

            nwind_list_ps = restart_file["nwind_list_ps"][:]
            wind_data_ps = {}
            for name in wind_names:
                wind_data_ps[name] = UnConcatenate(restart_file[name+"_gas_wind_list_ps"][:], nwind_list_ps)

            ok_snip = np.where(snip_list >= snip)[0]
            #ok_snip = np.append(ok_snip[0]-1,ok_snip) # Need the first _ps
            
            snip_list_tot = np.copy(snip_list)

            snip_list = snip_list[ok_snip]
            snap_list = snap_list[ok_snip]
            cosmic_t_list = cosmic_t_list[ok_snip]
            redshifts = redshifts[ok_snip]

            restart_file.close()
                        
            break

print "measuring rates for snipshots/snapshots", snip_list[0:-1]

t2 = time.time()
print "finished preliminaries, time elapsed", t2-t1

if use_restart:
    restart_counter = 0

for i_snip, snip_ts in enumerate(snip_list[:-1]):

    #if test_mode and snip_ts < 150:
    #    print "TEMP HACK", redshifts[i_snip]
    #    continue

    # For certain applications, we want to know where we are in the snapshot loop without being affected by the restart resets
    try:
        i_snip_glob = np.argmin(abs(snip_ts - snip_list_tot))
    except:
        i_snip_glob = i_snip

    # No _ps on first snapshots/snipshot
    # For restart, this skip also serves us to move us to the correct starting point
    if i_snip == 0:
        continue
    
    # Special case for broken first snapshots in 50 Mpc ref run
    if sim_name == "L0050N0752_REF_snap" and i_snip <= 2:
        print "skipping first output because broken"
        continue

    snip_ps = snip_list[i_snip-1]

    t1 = time.time()
    
    print ""
    print "measuring for snip (simulation), snap (tree), time (Gyr), redshift = ", snip_ts, snap_list[i_snip], cosmic_t_list[i_snip], redshifts[i_snip]

    # This is supposed to help with getting python to actually communicate something to the log file
    sys.stdout.flush()

    ######## Work out which snapshot to read as the _ns for wind selection ###############
    cosmic_t_ts = cosmic_t_list[i_snip] # Cosmic time at current 
    tdyn_ts = cosmic_t_ts * 0.1 # Approximate halo dynamical time in Gyr at this timestep
    snap_ts = snap_list[i_snip] # Current tree snapshot

    ok = np.where(snap_list == snap_ts)[0][0]
    t_remain = cosmic_t_list[ok:]
    dt = t_remain[1:] - t_remain[0]

    # tdyn_frac_ns is a parameter set at the top
    d_snap_ns = np.argmin( abs(dt - tdyn_ts * tdyn_frac_ns)) +1 # Number of snapshots into the future that should be read for _ns

    if not ns_tdyn:
        d_snap_ns = 1

    if d_snap_ns > 1:
        print "_ns skip is", d_snap_ns

    snip_ns = snip_list[i_snip + d_snap_ns]
    snap_ns = snap_ts + d_snap_ns

    # Read subvol file to load halo catalogue and selection boundaries
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    if not os.path.exists(output_path_ts):
        print "Couldn't find directory",output_path_ts,"moving on"
        continue

    print "Attempting to read", output_path_ts+filename_ts
    File = h5py.File(output_path_ts+filename_ts,"a")

    # Read selection boundary
    try:
        xyz_min_ts = File["select_region/xyz_min"][:]
        xyz_max_ts = File["select_region/xyz_max"][:]
    except:
        print output_path_ts+filename_ts, "doesn't exist, moving to next snipshot"
        File.close()
        continue

    # Quit if there are no subhaloes in this subvolume (this assumes that future subvols will also not have subhaloes)
    if np.isnan(xyz_min_ts[0]):
        print "No haloes inside this subvolume, moving to next snipshot"
        File.close()
        continue
    try:
        # Read in selection boundary for next snipshot
        output_path_ns = output_path + "Snip_"+str(snip_ns)+"/"
        filename_ns = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ns)+"_"+str(subvol)+".hdf5"
        File_ns = h5py.File(output_path_ns+filename_ns,"r")
        
        xyz_max_ns = File_ns["select_region/xyz_max"][:]
        xyz_min_ns = File_ns["select_region/xyz_min"][:]
    except:
        if not i_snip+1 == len(snip_list): # There is no next snipshot once we reach z=0
            print "Error: failed to read selection boundary for the next snipshot, skipping this snapshots"
            continue

    # Want to read in a big enough volume to accomodate both the subhalo & progenitor samples for this snipshot and the subhalo/progenitor samples for the next snipshot
    boxsize = mpio.Get_Box_Size(sim_name, DATDIR,snip_ts)
 
    for i_dim in range(3):
        if xyz_max_ts[i_dim] > xyz_max_ns[i_dim] + 0.5*boxsize:
            xyz_max_ns[i_dim]+= boxsize; xyz_min_ns[i_dim] += boxsize
        if xyz_max_ns[i_dim] > xyz_max_ts[i_dim] + 0.5*boxsize:
            xyz_max_ts[i_dim] += boxsize; xyz_min_ts[i_dim] += boxsize

    if retain_memory and d_snap_ns == 1:
        # In this case, we can carry over the particle data from _ns > _ts > _ps
        for i_dim in range(3):
            xyz_max_ns[i_dim] = max(xyz_max_ns[i_dim], xyz_max_ts[i_dim])
            xyz_min_ns[i_dim] = min(xyz_min_ns[i_dim], xyz_min_ts[i_dim])

    if nsub1d >1 or test_mode:
        print "for _ts, reading particles from volume with vertices: x=", xyz_min_ts[0],xyz_max_ts[0], " y=", xyz_min_ts[1], xyz_max_ts[1], " z=",xyz_min_ts[2],xyz_max_ts[2] 
        print "for _ns, reading particles from volume with vertices: x=", xyz_min_ns[0],xyz_max_ns[0], " y=", xyz_min_ns[1], xyz_max_ns[1], " z=",xyz_min_ns[2],xyz_max_ns[2] 

    # Read halo catalogue
    subhalo_group = File["subhalo_data"]
    
    subhalo_props = {}

    try:
        mchalo_list = subhalo_group["mchalo"][:]
    except:
        print "Error: first run copy_mchalo.py, then rerun select_subvolumes.py"
        quit()

    order_sub = np.argsort(mchalo_list)[::-1]
    mchalo_list = mchalo_list[order_sub]

    subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host", "isInterpolated"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]

    for name in subhalo_prop_names:
        subhalo_props[name] = subhalo_group[name][:][order_sub]

    # Select halos above the mass cut
    if cut_m200:
        ok = (subhalo_props["m200_host"] > m200_cut) & (subhalo_props["subgroup_number"] == 0)
        
        n_for_limit = min(100,len(subhalo_props["m200_host"]))
        mchalo_lim = np.median(np.sort(subhalo_props["mchalo"][ok])[0:n_for_limit])
        if test_mode:
            mchalo_lim = m200_cut # Not enough subhaloes in test mode to reliably evaluate the offset between m200 and mchalo (subfind mass) for centrals
            
        n_sub_before_mask = len(mchalo_list)
        ok = ok | ((subhalo_props["subgroup_number"]>0) & (subhalo_props["mchalo"] > mchalo_lim)) | ((subhalo_props["isInterpolated"]==1)&(subhalo_props["mchalo"] > mchalo_lim))
        
        # Keep track of non-selected satellite haloes, so we can control the smooth/merger halo accretion later
        removed_mask = np.where((ok==False) & (subhalo_props["subgroup_number"]>0) & (subhalo_props["m200_host"] > m200_cut))[0]
        removed_props_ts = {}
        for name in subhalo_prop_names:
            removed_props_ts[name] = subhalo_props[name][removed_mask]

        subhalo_mask = np.where(ok)[0]

        for name in subhalo_prop_names:
            subhalo_props[name] = subhalo_props[name][subhalo_mask]
        mchalo_list = mchalo_list[subhalo_mask]
        
        if len(mchalo_list)==0:
            print "After applying subhalo mass cut, there are no remaining subhalos, moving on"
            File.close()
            File_ns.close()
            continue
    else:
        removed_props_ts = {}
        for name in subhalo_prop_names:
            removed_props_ts[name] = np.zeros(0)

    if debug:
        ih_choose = np.argmax(mchalo_list)
    else:
        ih_choose = 0 # Dummy value

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ts = max(len(str(int(subhalo_props["subgroup_number"].max()))) +1 ,6)
    subhalo_props["subhalo_index"] = subhalo_props["group_number"].astype("int64") * 10**nsep_ts + subhalo_props["subgroup_number"].astype("int64")

    # Find satellites that have no central (to be used later)
    ptr_no_central = ms.match(subhalo_props["group_number"], subhalo_props["group_number"][subhalo_props["subgroup_number"]==0])
    no_central = (subhalo_props["subgroup_number"] > 0) & (ptr_no_central < 0)

    progenitor_group = File["progenitor_data"]
    
    subhalo_prop_names = ["group_number","subgroup_number","x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host","isInterpolated","node_index"]

    for name in subhalo_prop_names:
        name_progen = name+"_progen"
        subhalo_props[name_progen] = progenitor_group[name][:][order_sub]
        
    if cut_m200:
        for name in subhalo_prop_names:
            subhalo_props[name+"_progen"] = subhalo_props[name+"_progen"][subhalo_mask]

    all_progenitor_group = File["all_progenitor_data"]

    subhalo_prop_names =  ["group_number","subgroup_number","descendant_index","m200_host","mtot","isInterpolated"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","r200_host"]

    for name in subhalo_prop_names:
        name_all_progen = name+"_all_progen"
        subhalo_props[name_all_progen] = all_progenitor_group[name][:]
        
    if cut_m200:
        ptr = ms.match(subhalo_props["descendant_index_all_progen"], np.append(subhalo_props["node_index"],removed_props_ts["node_index"]))
        ok_match = ptr >= 0
        for name in subhalo_prop_names:
            subhalo_props[name+"_all_progen"] = subhalo_props[name+"_all_progen"][ok_match]

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ps = max(len(str(int(np.append(subhalo_props["subgroup_number_progen"],subhalo_props["subgroup_number_all_progen"]).max()))) +1 ,6)
    subhalo_props["subhalo_index_progen"] = subhalo_props["group_number_progen"].astype("int64") * 10**nsep_ps + subhalo_props["subgroup_number_progen"].astype("int64")
    subhalo_props["subhalo_index_all_progen"] = subhalo_props["group_number_all_progen"]* 10**nsep_ps + subhalo_props["subgroup_number_all_progen"]
    
    # Error checking
    ptr = ms.match(subhalo_props["group_number_progen"], subhalo_props["group_number_all_progen"])
    if len(ptr[ptr<0])>0:
        print "Error, halos in the progen list are not present in the all progen list"
        print np.unique(subhalo_props["group_number_progen"])
        print np.unique(subhalo_props["group_number_all_progen"])
        quit()

    # Read halo properties for _ns output (in order to identify the positions of the descendants of haloes from this output) - if _ns is not the next output in the tree we need to walk down the tree to find the correct descendant
    n_tree_walk = snap_ns - snap_ts
    for i_walk in range(n_tree_walk):

        snip_i_walk = snip_list[i_snip+i_walk+1]

        print "Reading", output_path + "Snip_"+str(snip_i_walk)+"/"
        
        filename_i_walk = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_i_walk)+"_"+str(subvol)+".hdf5"
        output_path_i_walk = output_path + "Snip_"+str(snip_i_walk)+"/"
        File_ns = h5py.File(output_path_i_walk+filename_i_walk,"r")

        subhalo_group_ns = File_ns["subhalo_data"]
        node_index_list_ns = subhalo_group_ns["node_index"][:]

        if i_walk == 0:
            ptr = ms.match(subhalo_props["descendant_index"], node_index_list_ns)
        else:
            ptr = ms.match(subhalo_props["descendant_index_ns"], node_index_list_ns) # Note descendant left over from prev iteration through the loop
        ok_match = ptr>=0

        subhalo_props["descendant_index_ns"] = np.zeros_like(mchalo_list).astype("int")-1e7 # Note it's important this is an ints array - not float
        subhalo_props["descendant_index_ns"][ok_match] = subhalo_group_ns["descendant_index"][:][ptr][ok_match]

        if i_walk != n_tree_walk-1:
            File_ns.close()

    if len(subhalo_props["descendant_index"][ok_match]) != len(subhalo_props["descendant_index"]):
        print "Warning: " + str(len(subhalo_props["descendant_index"][ok_match==False])) +" of "+str(len(subhalo_props["descendant_index"]))+" haloes could not find a descendant on the next simulation output"
        
    subhalo_prop_names = ["group_number","subgroup_number"]
    subhalo_prop_types =  ["int",         "int",          ]

    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host","isInterpolated"]
    subhalo_prop_types +=  ["float", "float", "float", "float", "float", "float", "float", "float","int"]

    for name, prop_type in zip(subhalo_prop_names,subhalo_prop_types):
        name_ns = name+"_ns"
        if prop_type == "int":
            subhalo_props[name_ns] = np.zeros_like(mchalo_list).astype("int")-1000000
        elif prop_type == "float":
            subhalo_props[name_ns] = np.zeros_like(mchalo_list) + np.nan
        else:
            "Error: prop type",prop_type,"not understood"
            quit()
        subhalo_props[name_ns][ok_match] = subhalo_group_ns[name][:][ptr][ok_match]

    File_ns.close()

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    if len(mchalo_list[ok_match])>0:
        nsep_ns = max(len(str(int(subhalo_props["subgroup_number_ns"][ok_match].max()))) +1 ,6)
    else:
        nsep_ns = 6
    subhalo_props["subhalo_index_ns"] = subhalo_props["group_number_ns"].astype("int64") * 10**nsep_ns + subhalo_props["subgroup_number_ns"].astype("int64")

    ############### Initialize output dicts ###########################

    output_mass_names = ["mass_gas_SF","n_gas_SF","mass_gas_NSF","n_gas_NSF","mass_gas_NSF_ISM","n_gas_NSF_ISM","mass_bh","mass_star","mass_star_init","mass_new_stars","mass_new_stars_init"]
    output_mass_names += ["n_star","n_new_stars","mass_acc_bh","aform_bh", "mass_stellar_rec"]
    output_mass_names += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]
    output_mass_names += ["mass_cgm_pristine", "mass_cgm_pristine_TLo", "mZ_cgm_pristine", "mass_cgm_galtran", "mass_cgm_galtran_TLo"]
    output_mass_names += ["mass_cgm_pristine_Router", "mass_cgm_galtran_Router"]
    output_mass_names += ["mZ_cgm_galtran", "mass_ej_galtran", "mZ_ej_galtran"]
    output_mass_names += ["mZ_new_stars_100_30kpc", "mZ_stars_30kpc", "mZ_gas_SF", "mZ_gas_NSF", "mZ_gas_NSF_ISM", "mZ_star", "mZ_new_stars" , "mZ_new_stars_init"]
    output_mass_names += ["mZ_outside", "mZ_outside_galaxy","mZ_injected","mZ_injected_ism"]
    output_mass_names += ["mass_new_stars_init_prcooled", "mass_new_stars_init_galtran", "mass_new_stars_init_recooled", "mass_new_stars_init_recooled_from_mp", "mass_new_stars_init_merge"] # Assume fid vmax cut for recooled here
    output_mass_names += ["mZ_new_stars_init_prcooled", "mZ_new_stars_init_galtran", "mZ_new_stars_init_recooled","mZ_new_stars_init_merge"]
    if not skip_dark_matter:
        output_mass_names += ["mass_diffuse_dm_accretion"]

    for fVmax_cut in fVmax_cuts:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

        output_mass_names += ["n_inside"+fV_str,"mass_inside"+fV_str,"mZ_inside"+fV_str,"mZ_outside"+fV_str,"mZ_outside_galaxy"+fV_str]
        output_mass_names += ["n_outside"+fV_str,"n_outside_galaxy"+fV_str,"mass_outside_galaxy"+fV_str,"mass_outside"+fV_str]
        output_mass_names += ["mass_cgm_ireheat"+fV_str,"mass_cgm_ireheat_TLo"+fV_str, "mass_cgm_ireheat_Router"+fV_str]
        output_mass_names += ["mass_cgm_wind_TLo"+fV_str, "mass_cgm_wind_Router"+fV_str] 
        output_mass_names += ["mZ_cgm_ireheat"+fV_str, "mass_hreheat"+fV_str, "mZ_hreheat"+fV_str]
        output_mass_names += ["mass_ej_wind"+fV_str, "mZ_ej_wind"+fV_str]

    output_mass_dict = {}

    for name in output_mass_names:
        output_mass_dict[name] = np.zeros(len(mchalo_list))
    
    if write_SNe_energy_fraction:
        output_emass_names = ["fb_e_times_mass_new_star", "mass_dir_heat_in_ism", "mass_dir_heat_in_nism","mZ_dir_heat_in_ism", "mZ_dir_heat_in_nism"]
        output_emass_names += ["mass_dir_heat_SNe_in_ism", "mass_dir_heat_SNe_in_nism","mZ_dir_heat_SNe_in_ism", "mZ_dir_heat_SNe_in_nism"]
        output_emass_names += ["mO_ism", "mO_cgm", "mFe_ism", "mFe_cgm"]
        output_emass_names += ["mass_prcooled_TmaxLo", "mass_galtran_TmaxLo","mass_praccreted_TmaxLo","mass_fof_transfer_TmaxLo"]
        output_emass_names += ["mass_prcooled_TmaxHi", "mass_galtran_TmaxHi","mass_praccreted_TmaxHi","mass_fof_transfer_TmaxHi"]

        for fVmax_cut in fVmax_cuts:
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
            output_emass_names += ["mass_join_ism_wind_dir_heat"+fV_str, "mass_join_halo_wind_dir_heat"+fV_str]
            output_emass_names += ["mO_join_ism_wind"+fV_str, "mFe_join_ism_wind"+fV_str]
            output_emass_names += ["mO_join_halo_wind"+fV_str, "mFe_join_halo_wind"+fV_str]
            output_emass_names += ["mass_recooled_TmaxLo"+fV_str, "mass_recooled_TmaxLo_from_mp"+fV_str,"mass_reaccreted_TmaxLo"+fV_str, "mass_reaccreted_TmaxLo_from_mp"+fV_str]
            output_emass_names += ["mass_recooled_TmaxHi"+fV_str, "mass_recooled_TmaxHi_from_mp"+fV_str,"mass_reaccreted_TmaxHi"+fV_str, "mass_reaccreted_TmaxHi_from_mp"+fV_str]

        output_emass_dict = {}
        for name in output_emass_names:
            output_emass_dict[name] = np.zeros(len(mchalo_list))
    else:
        output_emass_names = []

    output_outflow_names = ["mass_sf_ism_reheated","n_sf_ism_reheated","mass_nsf_ism_reheated","n_nsf_ism_reheated","mass_halo_reheated","n_halo_reheated"]
    output_outflow_names += ["n_join_halo_wind_from_ism","mass_join_halo_wind_from_ism","mZ_join_halo_wind_from_ism","mass_join_halo_wind_from_ism_first_time"]
    output_outflow_names += ["mass_ism_wind_cumul", "n_ism_wind_cumul", "mass_ism_to_halo_wind_first_time_cumul","n_ism_to_halo_wind_first_time_cumul"]
    output_outflow_names += ["mass_hmerged","mass_gmerged_CGM","mass_gmerged_ISM","time_join_halo_wind_from_ism_first_time", "mass_smacc_sat"]

    output_outflow_names += ["mass_prcooled", "mass_galtran", "mass_praccreted"]
    output_outflow_names += ["mass_fof_transfer","mass_reaccreted_hreheat","mass_recooled_ireheat"]
    output_outflow_names += ["mZ_prcooled", "mZ_galtran", "mZ_praccreted", "mZ_fof_transfer"]
    output_outflow_names += ["mass_galtran_from_SF", "mass_prcooled_SF"]
    output_outflow_names += ["mZ_galtran_from_SF", "mZ_prcooled_SF"]
    output_outflow_names += ["mass_praccreted_TLo", "mass_fof_transfer_TLo"]

    for fVmax_cut in fVmax_cuts:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

        output_outflow_names += ["mass_sf_ism_join_wind"+fV_str,"n_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str,"n_nsf_ism_join_wind"+fV_str,"mass_join_halo_wind"+fV_str,"n_join_halo_wind"+fV_str]
        output_outflow_names += ["mass_nsf_ism_join_wind_from_SF"+fV_str, "mZ_nsf_ism_join_wind_from_SF"+fV_str, "mass_ism_reheated_join_wind_from_SF"+fV_str, "mZ_ism_reheated_join_wind_from_SF"+fV_str]
        output_outflow_names += ["mass_ism_reheated_join_wind"+fV_str,"n_ism_reheated_join_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str,"n_halo_reheated_join_wind"+fV_str]
        output_outflow_names += ["mZ_sf_ism_join_wind"+fV_str,"mZ_nsf_ism_join_wind"+fV_str,"mZ_join_halo_wind"+fV_str,"mZ_ism_reheated_join_wind"+fV_str,"mZ_halo_reheated_join_wind"+fV_str]

        output_outflow_names += [ "mass_recooled"+fV_str, "mass_reaccreted"+fV_str, "mZ_recooled"+fV_str,  "mZ_reaccreted"+fV_str, "mass_recooled_from_SF"+fV_str, "mZ_recooled_from_SF"+fV_str]
        output_outflow_names += [ "mass_recooled_from_mp"+fV_str, "mass_reaccreted_from_mp"+fV_str, "mZ_recooled_from_mp"+fV_str,  "mZ_reaccreted_from_mp"+fV_str]
        output_outflow_names += [ "mass_reaccreted_TLo"+fV_str, "mass_reaccreted_TLo_from_mp"+fV_str]
        output_outflow_names += [ "mass_reaccreted_from_wind"+fV_str, "mZ_reaccreted_from_wind"+fV_str]

    output_outflow_dict = {}
    for name in output_outflow_names:
        output_outflow_dict[name] = np.zeros(len(mchalo_list))

    if extra_wind_measurements:
        # Shell measurements (the binning here should match the same in main_loop.py)
        vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
        rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
        n_rbins = len(rmid)
        output_shell_names = ["shell_rmid"]

        output_shell_names += ["P50mw","Pmean","a_grav","m_shell"]
        if write_SNe_energy_fraction:
            output_shell_names += ["dEdt_SNe","dEdt_AGN","dEdt_SNe_sat", "dEdt_AGN_sat"]
        for vmin_i in vmin:
            
            output_shell_names += ["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["P50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            output_shell_names += ["P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

            output_shell_names += ["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
            output_shell_names += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
            output_shell_names += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
            output_shell_names += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
            output_shell_names += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]

            for fVmax_cut in fVmax_cuts:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                output_shell_names += ["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                output_shell_names += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
    else:
        output_shell_names = []

    output_shell_dict = {}
    for name in output_shell_names:
        output_shell_dict[name] = np.zeros((len(mchalo_list),n_rbins))

    output_t_names = ["mass_t_wind","mass_t_wind_ret","mass_t_ejecta","mass_t_ejecta_ret"]
    # Tret output dicts will be initiated later, once we know the cosmological parameters
    if extra_accretion_measurements:
        output_t_names += ["mass_galtran_mchalo_ratio", "mass_fof_transfer_mchalo_ratio"]
        if not bound_only_mode:
            output_t_names += ["n_r_wind", "n_r_ejecta","n_r_wind_ret","n_r_ejecta_ret"]
    
    ############# On first snipshot, initialize the tracked ejecta lists #############

    # On restart, this list is already defined so need to do this init step
    try:
        wind_data_ps["id"] = wind_data_ps["id"]
    except:
        descendant_index_list_ps = subhalo_props["node_index"]
        grn_list_ps = subhalo_props["group_number_progen"]
        sgrn_list_ps = subhalo_props["subgroup_number_progen"]

        ejecta_data_ps = {} # Stores data of particles that have been ejected (as a wind) from the halo
        for name in ejecta_names:
            ejecta_data_ps[name] = []

        hreheat_data_ps = {} # Stores data of particles that have left a halo (but not joined the wind)
        for name in hreheat_names:
            hreheat_data_ps[name] = []

        wind_data_ps = {} # Stores data of particles that have been ejected (in the wind) from the ISM
        for name in wind_names:
            wind_data_ps[name] = []
            
        ireheat_data_ps = {} # Stores data of particles that have left the ISM (but not joined the wind)
        for name in ireheat_names:
            ireheat_data_ps[name] = []

        nsfism_data_ps = {}
        for name in nsfism_names:
            nsfism_data_ps[name] = []

        ism_data_ps = {}
        for name in ism_names:
            ism_data_ps[name] = []

        cumul_data_ps = {} # Keep track of some cumulative wind properties
        for name in cumul_names:
            cumul_data_ps[name] = np.zeros_like(mchalo_list)

        for i_halo in range(len(descendant_index_list_ps)):
            
            for name in ejecta_names:
                ejecta_data_ps[name].append([])

            for name in ireheat_names:
                ireheat_data_ps[name].append([])

            for name in wind_names:
                wind_data_ps[name].append([])

            for name in hreheat_names:
                hreheat_data_ps[name].append([])

            for name in nsfism_names:
                nsfism_data_ps[name].append([])

            for name in ism_names:
                ism_data_ps[name].append([])

    ############ Loop over haloes list and init the ejected particles lists for this snipshot ################
    print "Computing ejecta lists"
    t_ejecta_list = time.time()

    def Ejecta_setup(i_halo):
        ejecta_data_ts_i = {}
        for name in ejecta_names:
            if name == "id": # np.append() in main loop will turn ints into floats if you give an empty list
                ejecta_data_ts_i[name] = np.array([]).astype("int")
            else:
                ejecta_data_ts_i[name] = []
            
        hreheat_data_ts_i = {}
        for name in hreheat_names:
            if name == "id": # np.append() in main loop will turn ints into floats if you give an empty list
                hreheat_data_ts_i[name] = np.array([]).astype("int")
            else:
                hreheat_data_ts_i[name] = []

        wind_data_ts_i = {}
        for name in wind_names:
            if name == "id": # np.append() in main loop will turn ints into floats if you give an empty list
                wind_data_ts_i[name] = np.array([]).astype("int")
            else:
                wind_data_ts_i[name] = []
            
        ireheat_data_ts_i = {}
        for name in ireheat_names:
            if name == "id": # np.append() in main loop will turn ints into floats if you give an empty list
                ireheat_data_ts_i[name] = np.array([]).astype("int")
            else:
                ireheat_data_ts_i[name] = []

        nsfism_data_ts_i = {}
        for name in nsfism_names:
            if name == "id": # np.append() in main loop will turn ints into floats if you give an empty list
                nsfism_data_ts_i[name] = np.array([]).astype("int")
            else:
                nsfism_data_ts_i[name] = []

        ism_data_ts_i = {}
        for name in ism_names:
            if name == "id": # np.append() in main loop will turn ints into floats if you give an empty list
                ism_data_ts_i[name] = np.array([]).astype("int")
            else:
                ism_data_ts_i[name] = []

        temp_grn_hr = []
        temp_sgrn_hr = []
        # Edit: 21/6/21 - this is always turned on now - to enable computing galtran contribution to ejecta without
        # needing to cross-ref master lists with particle lists inside the main loop
        temp_grn_ej = []; temp_sgrn_ej = []
        if extra_accretion_measurements and not bound_only_mode:
            temp_grn_wi = []; temp_sgrn_wi = []

        halo_node_index = subhalo_props["node_index"][i_halo]
        halo_group_number_progen = subhalo_props["group_number_progen"][i_halo]
        halo_subgroup_number_progen = subhalo_props["subgroup_number_progen"][i_halo]

        # Add ejected particles from each progenitor of this subhalo
        select_progenitors = np.where(halo_node_index == descendant_index_list_ps)[0]
        main_progenitor = (grn_list_ps[select_progenitors] == halo_group_number_progen) & (sgrn_list_ps[select_progenitors] == halo_subgroup_number_progen)

        for i_progen, select_progenitor_i in enumerate(select_progenitors):
            for name in ejecta_names:
                temp = np.array(ejecta_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                ejecta_data_ts_i[name] = np.append(np.array(ejecta_data_ts_i[name]), temp)

            temp_grn_ej = np.append(np.array(temp_grn_ej), np.zeros_like(ejecta_data_ps["id"][select_progenitor_i])+subhalo_props["group_number"][i_halo])
            temp_sgrn_ej = np.append(np.array(temp_sgrn_ej), np.zeros_like(ejecta_data_ps["id"][select_progenitor_i])+subhalo_props["subgroup_number"][i_halo])

            for name in hreheat_names:
                temp = np.array(hreheat_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                hreheat_data_ts_i[name] = np.append(np.array(hreheat_data_ts_i[name]), temp)

            temp_grn_hr = np.append(np.array(temp_grn_hr), np.zeros_like(hreheat_data_ps["id"][select_progenitor_i])+subhalo_props["group_number"][i_halo])
            temp_sgrn_hr = np.append(np.array(temp_sgrn_hr), np.zeros_like(hreheat_data_ps["id"][select_progenitor_i])+subhalo_props["subgroup_number"][i_halo])
            
            for name in wind_names:
                temp = np.array(wind_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                wind_data_ts_i[name] = np.append(np.array(wind_data_ts_i[name]), temp)

            if extra_accretion_measurements and not bound_only_mode:
                temp_grn_wi = np.append(np.array(temp_grn_wi), np.zeros_like(wind_data_ps["id"][select_progenitor_i])+subhalo_props["group_number"][i_halo])
                temp_sgrn_wi = np.append(np.array(temp_sgrn_wi), np.zeros_like(wind_data_ps["id"][select_progenitor_i])+subhalo_props["subgroup_number"][i_halo])


            for name in ireheat_names:
                temp = np.array(ireheat_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                ireheat_data_ts_i[name] = np.append(np.array(ireheat_data_ts_i[name]), temp)
            for name in nsfism_names:
                temp = np.array(nsfism_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                nsfism_data_ts_i[name] = np.append(np.array(nsfism_data_ts_i[name]), temp)

            for name in ism_names:
                temp = np.array(ism_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                ism_data_ts_i[name] = np.append(np.array(ism_data_ts_i[name]), temp)

                # Add ejected particles from pruned satellites if the satellite has > 100 tracked particles
        # This is intended to catch cases where large numbers of tracked particles get associated with a tiny satellite by mistake due to tree shennanigans.
        if cut_m200 and subhalo_props["subgroup_number"][i_halo] == 0:
            removed_node_indicies = removed_props_ts["node_index"][np.where(subhalo_props["group_number"][i_halo] == removed_props_ts["group_number"])[0]]
            for removed_node_index in removed_node_indicies:
                select_progenitors = np.where(removed_node_index == descendant_index_list_ps)[0]

                for select_progenitor_i in select_progenitors:

                    # Only do this if we suspect something has gone badly wrong (i.e. we've inadvertently removed > 100 particles)
                    if len(ejecta_data_ps["id"][select_progenitor_i]) < 100 and len(wind_data_ps["id"][select_progenitor_i]) < 100:
                        continue

                    for name in ejecta_names:
                        temp = np.array(ejecta_data_ps[name][select_progenitor_i])
                        if name == "from_mp":
                            temp *= 0 # These should never come from the main progenitor object
                        ejecta_data_ts_i[name] = np.append(np.array(ejecta_data_ts_i[name]), temp)
                    temp_grn_ej = np.append(np.array(temp_grn_ej), np.zeros_like(ejecta_data_ps["id"][select_progenitor_i])+subhalo_props["group_number"][i_halo])
                    temp_sgrn_ej = np.append(np.array(temp_sgrn_ej), np.zeros_like(ejecta_data_ps["id"][select_progenitor_i])+subhalo_props["subgroup_number"][i_halo])


                    for name in hreheat_names:
                        temp = np.array(hreheat_data_ps[name][select_progenitor_i])
                        if name == "from_mp":
                            temp *= 0
                        hreheat_data_ts_i[name] = np.append(np.array(hreheat_data_ts_i[name]), temp)

                    temp_grn_hr = np.append(np.array(temp_grn_hr), np.zeros_like(hreheat_data_ps["id"][select_progenitor_i])+subhalo_props["group_number"][i_halo])
                    temp_sgrn_hr = np.append(np.array(temp_sgrn_hr), np.zeros_like(hreheat_data_ps["id"][select_progenitor_i])+subhalo_props["subgroup_number"][i_halo])

                    for name in wind_names:
                        temp = np.array(wind_data_ps[name][select_progenitor_i])
                        if name == "from_mp":
                            temp *= 0
                        wind_data_ts_i[name] = np.append(np.array(wind_data_ts_i[name]), temp)
                    
                    if extra_accretion_measurements and not bound_only_mode:
                        temp_grn_wi = np.append(np.array(temp_grn_wi), np.zeros_like(wind_data_ps["id"][select_progenitor_i])+subhalo_props["group_number"][i_halo])
                        temp_sgrn_wi = np.append(np.array(temp_sgrn_wi), np.zeros_like(wind_data_ps["id"][select_progenitor_i])+subhalo_props["subgroup_number"][i_halo])

                    for name in ireheat_names:
                        temp = np.array(ireheat_data_ps[name][select_progenitor_i])
                        if name == "from_mp":
                            temp *= 0
                        ireheat_data_ts_i[name] = np.append(np.array(ireheat_data_ts_i[name]), temp)
                    for name in nsfism_names:
                        temp = np.array(nsfism_data_ps[name][select_progenitor_i])
                        if name == "from_mp":
                            temp *= 0
                        nsfism_data_ts_i[name] = np.append(np.array(nsfism_data_ts_i[name]), temp)
                    for name in ism_names:
                        temp = np.array(ism_data_ps[name][select_progenitor_i])
                        if name == "from_mp":
                            temp *= 0
                        ism_data_ts_i[name] = np.append(np.array(ism_data_ts_i[name]), temp)
            

        # Update the clock for reheat list particles both ISM and halo
        cosmic_t_ps = cosmic_t_list[i_snip-1]
        dt_ts = cosmic_t_ts - cosmic_t_ps

        ireheat_data_ts_i["time"] += dt_ts
        hreheat_data_ts_i["time"] += dt_ts
        
        # Remove particles that have spent more than a halo dynamical time inside the reheat list
        if len(ireheat_data_ts_i["time"])>0:
            keep = ireheat_data_ts_i["time"] < 0.1 * cosmic_t_ts * reheat_tdyn_frac_ism # Approximate t_dyn as 0.1 cosmic time

            for name in ireheat_names:
                ireheat_data_ts_i[name] = ireheat_data_ts_i[name][keep]
                            
        if len(hreheat_data_ts_i["time"])>0:
            keep = hreheat_data_ts_i["time"] < 0.1 * cosmic_t_ts *reheat_tdyn_frac_halo # Approximate t_dyn as 0.1 cosmic time

            for name in hreheat_names:
                hreheat_data_ts_i[name] = hreheat_data_ts_i[name][keep]

            temp_grn_hr = temp_grn_hr[keep]
            temp_sgrn_hr = temp_sgrn_hr[keep]

        # Check uniqueness of the ejected particle lists
        if len(ejecta_data_ts_i["id"]) != len(np.unique(ejecta_data_ts_i["id"])):            
            junk, index = np.unique(ejecta_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(ejecta_names):
                ejecta_data_ts_i[name] = ejecta_data_ts_i[name][index]
                
            temp_grn_ej = temp_grn_ej[index]
            temp_sgrn_ej = temp_sgrn_ej[index]

        gas_ejecta_ts_grn_i = temp_grn_ej
        gas_ejecta_ts_sgrn_i = temp_sgrn_ej
            
        if len(hreheat_data_ts_i["id"]) != len(np.unique(hreheat_data_ts_i["id"])):            
            junk, index = np.unique(hreheat_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(hreheat_names):
                hreheat_data_ts_i[name] = hreheat_data_ts_i[name][index]

            temp_grn_hr = temp_grn_hr[index]
            temp_sgrn_hr = temp_sgrn_hr[index]

        gas_halo_reheat_ts_grn_i = temp_grn_hr
        gas_halo_reheat_ts_sgrn_i = temp_sgrn_hr

        if len(wind_data_ts_i["id"]) != len(np.unique(wind_data_ts_i["id"])):            
            junk, index = np.unique(wind_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(wind_names):
                wind_data_ts_i[name] = wind_data_ts_i[name][index]

            if extra_accretion_measurements and not bound_only_mode:
                temp_grn_wi = temp_grn_wi[index]
                temp_sgrn_wi = temp_sgrn_wi[index]

        if extra_accretion_measurements and not bound_only_mode:
            gas_wind_ts_grn_i = temp_grn_wi
            gas_wind_ts_sgrn_i = temp_sgrn_wi


        if len(ireheat_data_ts_i["id"]) != len(np.unique(ireheat_data_ts_i["id"])):            
            junk, index = np.unique(ireheat_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(ireheat_names):
                ireheat_data_ts_i[name] = ireheat_data_ts_i[name][index]

        if len(nsfism_data_ts_i["id"]) != len(np.unique(nsfism_data_ts_i["id"])):            
            junk, index = np.unique(nsfism_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(nsfism_names):
                nsfism_data_ts_i[name] = nsfism_data_ts_i[name][index]

        if len(ism_data_ts_i["id"]) != len(np.unique(ism_data_ts_i["id"])):            
            junk, index = np.unique(ism_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(ism_names):
                ism_data_ts_i[name] = ism_data_ts_i[name][index]

        output = [gas_halo_reheat_ts_grn_i, gas_halo_reheat_ts_sgrn_i]
        output += [gas_ejecta_ts_grn_i, gas_ejecta_ts_sgrn_i]
        if extra_accretion_measurements and not bound_only_mode:
            output += [gas_wind_ts_grn_i, gas_wind_ts_sgrn_i]
        output += [hreheat_data_ts_i, ejecta_data_ts_i, wind_data_ts_i, ireheat_data_ts_i, nsfism_data_ts_i, ism_data_ts_i]
        return output

    output = Parallel(n_jobs=n_jobs_parallel)(delayed(Ejecta_setup)(i_halo) for i_halo in range(len(mchalo_list)))

    output = np.array(output)
    n_data = len(output)/len(mchalo_list)

    gas_halo_reheat_ts = mpc.DataSet() # This is data for finding the instantaneous properties of tracked particles
    gas_halo_reheat_ts.add_data(np.concatenate((output[:,0])),"group_number")
    gas_halo_reheat_ts.add_data(np.concatenate((output[:,1])),"subgroup_number")

    # In this mode, we track the positions of wind and ejecta particles
    # Copying the way this is done for "gas_halo_reheat_ts"
    
    gas_ejecta_ts = mpc.DataSet()
    gas_ejecta_ts.add_data(np.concatenate((output[:,2])),"group_number")
    gas_ejecta_ts.add_data(np.concatenate((output[:,3])),"subgroup_number")

    n_extra = 4
    if extra_accretion_measurements and not bound_only_mode:
        gas_wind_ts = mpc.DataSet()
        gas_wind_ts.add_data(np.concatenate((output[:,4])),"group_number")
        gas_wind_ts.add_data(np.concatenate((output[:,5])),"subgroup_number")
        n_extra = 6

    t_repack_ejecta = time.time()
    # Unpack the data (note if this is slow - you could get rid of this for loop by doing _i dicts as lists instead
    hreheat_data_ts = {}
    ejecta_data_ts = {}
    wind_data_ts = {}
    ireheat_data_ts = {}
    nsfism_data_ts = {}
    ism_data_ts = {}
    for i_halo in range(len(subhalo_props["group_number"])):
        for name in hreheat_names:
            if i_halo == 0:
                hreheat_data_ts[name] = []
            hreheat_data_ts[name].append(output[i_halo,n_extra][name]) 
        for name in ejecta_names:
            if i_halo == 0:
                ejecta_data_ts[name] = []
            ejecta_data_ts[name].append(output[i_halo,n_extra+1][name])
        for name in wind_names:
            if i_halo == 0:
                wind_data_ts[name] = []
            wind_data_ts[name].append(output[i_halo,n_extra+2][name])
        for name in ireheat_names:
            if i_halo == 0:
                ireheat_data_ts[name] = []
            ireheat_data_ts[name].append(output[i_halo,n_extra+3][name])
        for name in nsfism_names:
            if i_halo == 0:
                nsfism_data_ts[name] = []
            nsfism_data_ts[name].append(output[i_halo,n_extra+4][name])
        for name in ism_names:
            if i_halo == 0:
                ism_data_ts[name] = []
            ism_data_ts[name].append(output[i_halo,n_extra+5][name])
    
    # Note the difference is hreheat_data_ts is split into lists for each subhalo
    # gas_halo_reheat_ts is a single list of all particles that are in hreheat on _ts
    gas_halo_reheat_ts.add_data(np.concatenate(hreheat_data_ts["id"]),"id")
    gas_ejecta_ts.add_data(np.concatenate(ejecta_data_ts["id"]),"id")
    if extra_accretion_measurements and not bound_only_mode:
        gas_wind_ts.add_data(np.concatenate(wind_data_ts["id"]),"id")

    if verbose:
        print "time spent unpacking the _ts ejecta lists was", time.time() - t_repack_ejecta
    output = []
            
    print "Finished ejecta/reheat lists, time taken was", time.time() -t_ejecta_list

    ############## Read Eagle particle data #######################
    t2 = time.time()

    # 4/4/18 Structure here is that we read in the data for previous snipshot _ps, current snipshot _ts, next snipshot _ns - this triple read is done for every snipshot
    # Note this means that we triple the IO cost - the reason for doing this is that it allows us to discard unwanted data to save memory as we go through
    # If the code becomes IO limited at some point in the future - and memory isn't a big problem then you should change this approach!

    gas_names_in_ps = ['Mass', "Density", "Metallicity", "Temperature", 'Coordinates']
    gas_names_out_ps = ['mass', "density", "metallicity", "temperature", 'coordinates']

    if ism_definition == "Mitchell18_adjusted":
        gas_names_in_ps += ["InternalEnergy","Velocity"]
        gas_names_out_ps += ["internal_energy","velocity"]

    if write_SNe_energy_fraction:
        gas_names_in_ps += ["MaximumTemperature","ElementAbundance/Oxygen", "ElementAbundance/Iron"]
        gas_names_out_ps += ["Tmax",             "oxygen",                  "iron"                 ]

    all_gas_names_in_ps = []; all_gas_names_out_ps = []
    if not bound_only_mode:
        all_gas_names_in_ps += ['Coordinates']
        all_gas_names_out_ps += ['coordinates']

    dm_names_in_ps = []; dm_names_out_ps = []
    if not skip_dark_matter:
        dm_names_in_ps += ["Coordinates"]
        dm_names_out_ps += ["coordinates"]

    star_names_in_ps = ['Mass', "InitialMass", 'Coordinates']
    star_names_out_ps = ['mass', "mass_init",'coordinates']
    
    bh_names_in_ps = ['BH_Mass', "BH_CumlNumSeeds", 'Coordinates']
    bh_names_out_ps = ['mass', "nseed",'coordinates']

    data_names_in_ps = [gas_names_in_ps, dm_names_in_ps, star_names_in_ps, bh_names_in_ps, all_gas_names_in_ps,[],[],[]]
    data_names_out_ps = [gas_names_out_ps, dm_names_out_ps, star_names_out_ps, bh_names_out_ps, all_gas_names_out_ps,[],[],[]]

    ####### _ps #############
    print "Reading particle data for _ps (previous snipshot)"

    # Decide how many sub-subvolumes we want for doing IO (and some intial particle selection)
    # The answer is ideally 1, but if memory is limited we can reduce memory cost by splitting the IO into smaller steps, discarding un-needed particles as we go (this is much less important for bound_only_mode)
    vol_ps = np.product(xyz_max_ts-xyz_min_ts) # Mpc h^-1

    if vol_ps > volmax:
        nsubvol_pts = 8
        
        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ts
        
        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ts[0]-xyz_min_ts[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ts[1]-xyz_min_ts[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ts[2]-xyz_min_ts[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1
        
        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ts[0]-xyz_min_ts[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ts[1]-xyz_min_ts[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ts[2]-xyz_min_ts[2])*0.5
    else:
        nsubvol_pts = 1

    if retain_ts: # Do we carry over particle data from previous step?
        if verbose:
            print "Using previous _ts data on _ps"
        nsubvol_pts = 1  # In this case we are not doing any _ps IO on this step, so the below loop is redundant
        gas_ps_in = gas_ts; dm_ps_in = dm_ts; star_ps_in = star_ts; bh_ps_in = bh_ts; sim_props_ps = sim_props_ts
        all_gas_ps_in = gas_ps_in

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_temp = time.time()

        t_core_io_i = time.time()

        # Read in particle data for previous snapshot
        if not retain_ts:
            if nsub1d > 1 or test_mode or not bound_only_mode:
                # Use JCH Hash-Table IO (for efficiently reading in sub-regions of the box)
                if bound_only_mode:
                    read_all = ""
                    gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, sim_props_ps = mpio.Read_All_Particle_Data(snip_ps, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ps,data_names_out_ps,read_all=read_all,verbose=verbose)
                    all_gas_ps_in = gas_ps_in

                else:
                    read_all = "g"
                    gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, all_gas_ps_in, sim_props_ps = mpio.Read_All_Particle_Data(snip_ps, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ps,data_names_out_ps,read_all=read_all,verbose=verbose)


            else:

                if not bound_only_mode:
                    print "Custom IO not implemented for non-bound particles"
                    quit()
                # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
                data_names_in_ps = gas_names_in_ps+ dm_names_in_ps+ star_names_in_ps+ bh_names_in_ps
                data_names_out_ps = gas_names_out_ps+ dm_names_out_ps+ star_names_out_ps+ bh_names_out_ps
                part_types = np.concatenate(( np.zeros(len(gas_names_in_ps)), np.ones(len(dm_names_in_ps)), np.zeros(len(star_names_in_ps))+4, np.zeros(len(bh_names_in_ps))+5 )).astype("int")

                gas_ps_in, dm_ps_in, star_ps_in, bh_ps_in, sim_props_ps = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ps, sim_name, data_names_in_ps,data_names_out_ps,part_types,njobs=n_jobs_parallel,verbose=verbose)
                all_gas_ps_in = gas_ps_in

        t_core_io = time.time() - t_core_io_i

        # Create arrays of halo properties for _ps
        # For _ps, we need to be careful to select particles that are only in subhalo progenitors (and not complete FoF groups which tend to include massive halos)
        # Create a unique id for each subhalo out of group_number and subgroup_number (this is because we don't have node_index for particles)                    
        nsep = max(len(str(int(subhalo_props["subgroup_number_all_progen"].max()))) +1 ,6)

        cat_list_all_progen = subhalo_props["group_number_all_progen"].astype("int64") * 10**nsep + subhalo_props["subgroup_number_all_progen"].astype("int64")
        cat_list_progen = subhalo_props["group_number_progen"].astype("int64") * 10**nsep + subhalo_props["subgroup_number_progen"].astype("int64")

        cat_ps_master_list = np.concatenate((cat_list_progen, cat_list_all_progen)) # (Note to self - isn't this redundant - I thought the code crashes if there are progen halos not in the all_progen list?) 
        cat_ps_master_list, ind_unique = np.unique(cat_ps_master_list,return_index=True)

        t_rebind_i = time.time()
        if central_r200 and not retain_ts:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            # Note if retain_ts is true, we already did the required rebinding on an earlier step
            if verbose:
                print "rebinding _ps"

            halo_coordinates_ps = [np.append(subhalo_props["x_halo_progen"],subhalo_props["x_halo_all_progen"])[ind_unique],\
                                       np.append(subhalo_props["y_halo_progen"],subhalo_props["y_halo_all_progen"])[ind_unique],\
                                       np.append(subhalo_props["z_halo_progen"],subhalo_props["z_halo_all_progen"])[ind_unique]]
            grn_temp = np.append(subhalo_props["group_number_progen"], subhalo_props["group_number_all_progen"])[ind_unique]
            sgrn_temp = np.append(subhalo_props["subgroup_number_progen"], subhalo_props["subgroup_number_all_progen"])[ind_unique]
            r200_temp = np.append(subhalo_props["r200_host_progen"], subhalo_props["r200_host_all_progen"])[ind_unique]
            iI_temp = np.append(subhalo_props["isInterpolated_progen"], subhalo_props["isInterpolated_all_progen"])[ind_unique]

            # Gas rebinding
            # Note - this horrible block is outside a function (and repeated many times) because of the JobLib pecularities (pickle-related)
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_ps, r200_temp, boxsize)
            temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)

            gas_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ps_in)

            if not bound_only_mode:
                mpf.Rebind_All_Particles_From_Bound(all_gas_ps_in, gas_ps_in)

            # Dark matter rebinding
            if len(dm_names_in_ps) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ps_in)

            # Star rebinding
            if len(star_ps_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                star_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ps_in)

            # BH rebinding
            if len(bh_ps_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(bh_ps_in, grn_temp, sgrn_temp, halo_coordinates_ps,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                bh_ps_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, bh_ps_in)

        t_rebind = time.time() - t_rebind_i
        
        # Having read in positions of all gas particles (including those not bound) - we want to extract anything from the non-bound particles now before discarding the list
        bound_parts_ps = all_gas_ps_in.is_bound()
        not_bound_parts_ps = bound_parts_ps == False
        
        # Identify all of the spatial positions of all non-bound particles that are in any of the halo_reheat_lists
        if i_subvol == 0:
            gas_halo_reheat_ps = mpc.DataSet()

        if central_r200:
            ok_unbound_ps = not_bound_parts_ps|all_gas_ps_in.data["subfind_unbound"] # Subfind unbound = True means particles that have been bound to central by Rebind, but were considered unbound by subfind before
        else:
            ok_unbound_ps = not_bound_parts_ps

        if len(gas_halo_reheat_ts.data["id"]) > 0 and len(all_gas_ps_in.data["id"][ok_unbound_ps])>0:
        
            if extra_accretion_measurements and not bound_only_mode:
                ptr = ms.match(gas_halo_reheat_ts.data["id"], all_gas_ps_in.data["id"]) # In this case we want to know maxium radius of all ejecta particles (including those that are only in hreheat because of the most conservative velocity cut)
            else:
                ptr = ms.match(gas_halo_reheat_ts.data["id"], all_gas_ps_in.data["id"][ok_unbound_ps]) # Note we allow subfind unbound here to allow likely flythrough particles a chance to join the wind
            ok_match = ptr >= 0
            if i_subvol == 0:
                gas_halo_reheat_ps.add_data(gas_halo_reheat_ts.data["id"],"id")
                gas_halo_reheat_ps.add_data(gas_halo_reheat_ts.data["group_number"],"group_number_descendant")
                gas_halo_reheat_ps.add_data(gas_halo_reheat_ts.data["subgroup_number"],"subgroup_number_descendant")
                gas_halo_reheat_ps.add_data(np.zeros((len(gas_halo_reheat_ts.data["id"]),3)), "coordinates")

            if extra_accretion_measurements and not bound_only_mode:
                gas_halo_reheat_ps.data["coordinates"][ok_match] = all_gas_ps_in.data["coordinates"][ptr][ok_match]
            else:
                gas_halo_reheat_ps.data["coordinates"][ok_match] = all_gas_ps_in.data["coordinates"][ok_unbound_ps][ptr][ok_match]

            # Search for bound particles and give Nan as their position, to enable the status of particles later on in wind calculations
            if not extra_accretion_measurements or bound_only_mode:
                if len(gas_halo_reheat_ps.data["id"]) > 0:
                    ptr = ms.match(gas_halo_reheat_ps.data["id"], np.append(all_gas_ps_in.data["id"][ok_unbound_ps==False], star_ps_in.data["id"]) )
                    ok_match = ptr >= 0

                    gas_halo_reheat_ps.data["coordinates"][ok_match] = np.nan

        elif i_subvol == 0:
            gas_halo_reheat_ps.add_data(np.zeros((0,3)),"coordinates")
            gas_halo_reheat_ps.add_data(np.zeros(0),"id")
            gas_halo_reheat_ps.add_data(np.zeros(0),"group_number_descendant")
            gas_halo_reheat_ps.add_data(np.zeros(0),"subgroup_number_descendant")
        
        # Put boolean array out of scope (in hopes garbage collector will do its job) - this probably does nothing
        not_bound_parts_ps = None; bound_parts_ps = None

        # Put all gas dataset out of scope
        all_gas_ps_in = None

        # Remove non-bound particles and bound particles from unwanted halos/subhalo
        cat_gas_list_ps = gas_ps_in.data["group_number"].astype("int64") * 10**nsep + gas_ps_in.data["subgroup_number"].astype("int64")
        if len(cat_ps_master_list) > 0 and len(cat_gas_list_ps) > 0:
            ptr = ms.match(cat_gas_list_ps, cat_ps_master_list,arr2_sorted=True)
            ok_match = ptr >= 0
            gas_ps_in.select_bool(ok_match)

        if not skip_dark_matter:
            cat_dm_list_ps = dm_ps_in.data["group_number"].astype("int64") * 10**nsep + dm_ps_in.data["subgroup_number"].astype("int64")
            if len(cat_ps_master_list) > 0 and len(cat_dm_list_ps) > 0:
                ptr = ms.match(cat_dm_list_ps, cat_ps_master_list,arr2_sorted=True)
                ok_match = ptr >= 0
                dm_ps_in.select_bool(ok_match)

        cat_star_list_ps = star_ps_in.data["group_number"].astype("int64") * 10**nsep + star_ps_in.data["subgroup_number"].astype("int64")
        if len(cat_ps_master_list) > 0 and len(cat_star_list_ps) > 0:
            ptr = ms.match(cat_star_list_ps, cat_ps_master_list,arr2_sorted=True)
            ok_match = ptr >= 0
            star_ps_in.select_bool(ok_match)

        cat_bh_list_ps = bh_ps_in.data["group_number"].astype("int64") * 10**nsep + bh_ps_in.data["subgroup_number"].astype("int64")
        if len(cat_ps_master_list) > 0 and len(cat_bh_list_ps) > 0:
            ptr = ms.match(cat_bh_list_ps, cat_ps_master_list,arr2_sorted=True)
            ok_match = ptr >= 0
            bh_ps_in.select_bool(ok_match)

        # Init datasets
        if i_subvol == 0:
            gas_ps = mpc.DataSet(); dm_ps = mpc.DataSet(); star_ps = mpc.DataSet(); bh_ps = mpc.DataSet()

        # Merge datasets
        if nsubvol_pts > 1:
            gas_ps.merge_dataset(gas_ps_in)
            star_ps.merge_dataset(star_ps_in)
            bh_ps.merge_dataset(bh_ps_in)
            if not skip_dark_matter:
                dm_ps.merge_dataset(dm_ps_in)

            # Put the input datasets out of scope
            gas_ps_in = None; dm_ps_in = None; star_ps_in = None; bh_ps_in = None
        else:
            gas_ps = gas_ps_in; dm_ps = dm_ps_in; star_ps = star_ps_in; bh_ps = bh_ps_in
    
        # Put the input datasets out of scope
        cat_gas_list_ps = None; cat_dm_list_ps = None; cat_star_list_ps = None; cat_bh_list_ps = None


    ########## _ts ##############
    print "Reading particle data for _ts (current snipshot)"

    gas_names_in_ts = ['Mass', "Density", "Metallicity", "Temperature", "InternalEnergy", 'Coordinates', "Velocity"]
    gas_names_out_ts = ['mass', "density", "metallicity", "temperature", "internal_energy", 'coordinates', "velocity"]

    if write_SNe_energy_fraction:
        gas_names_in_ts += ["ElementAbundance/Iron","ElementAbundance/Oxygen"]
        gas_names_out_ts += ["iron",                 "oxygen"]

    all_gas_names_in_ts = []
    all_gas_names_out_ts = []
    if not bound_only_mode:
        all_gas_names_in_ts += ['Coordinates']
        all_gas_names_out_ts += ['coordinates']

    dm_names_in_ts = []; dm_names_out_ts = []
    if not skip_dark_matter:
        dm_names_in_ts = ["Coordinates"]
        dm_names_out_ts = ["coordinates"]

    star_names_in_ts = ['Mass', "InitialMass", 'Coordinates',"StellarFormationTime","Metallicity"]
    star_names_out_ts = ['mass', "mass_init",'coordinates',"aform","metallicity"]

    if write_SNe_energy_fraction:
        star_names_in_ts.append("Feedback_EnergyFraction")
        star_names_out_ts.append("fb_e_fraction")

        if not bound_only_mode:
            all_gas_names_in_ts.append("MaximumTemperature")
            all_gas_names_out_ts.append("Tmax")

        gas_names_in_ts.append("MaximumTemperature")
        gas_names_out_ts.append("Tmax")

    bh_names_in_ts = ['BH_Mass', "BH_CumlNumSeeds", 'Coordinates',"BH_FormationTime"]
    bh_names_out_ts = ['mass', "nseed",'coordinates',"aform"]

    data_names_in_ts = [gas_names_in_ts, dm_names_in_ts, star_names_in_ts, bh_names_in_ts, all_gas_names_in_ts,[],[],[]]
    data_names_out_ts = [gas_names_out_ts, dm_names_out_ts, star_names_out_ts, bh_names_out_ts, all_gas_names_out_ts,[],[],[]]

    if retain_ns: # Do we carry over particle data from previous step?
        if verbose:
            print "Using previous _ns data on _ts"
        nsubvol_pts = 1  # In this case we are not doing any _ts IO on this step, so the below loop is redundant
        gas_ts_in = gas_ns; dm_ts_in = dm_ns; star_ts_in = star_ns; bh_ts_in = bh_ns; sim_props_ts = sim_props_ns
        all_gas_ts_in = gas_ts_in

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_core_io_i = time.time()
            
        if not retain_ns:
            if nsub1d > 1 or test_mode or not bound_only_mode:
                if bound_only_mode:
                    read_all = ""
                    gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)
                    all_gas_ts_in = gas_ts_in
                else:
                    read_all = "g"
                    gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, all_gas_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)

            else:
                # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
                data_names_in_ts = gas_names_in_ts+ dm_names_in_ts+ star_names_in_ts+ bh_names_in_ts
                data_names_out_ts = gas_names_out_ts+ dm_names_out_ts+ star_names_out_ts+ bh_names_out_ts
                part_types = np.concatenate(( np.zeros(len(gas_names_in_ts)), np.ones(len(dm_names_in_ts)), np.zeros(len(star_names_in_ts))+4, np.zeros(len(bh_names_in_ts))+5 )).astype("int")
                
                gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ts, sim_name, data_names_in_ts,data_names_out_ts,part_types,njobs=n_jobs_parallel,verbose=verbose)
                all_gas_ts_in = gas_ts_in

        t_core_io += time.time() - t_core_io_i

        t_rebind_i = time.time()
        if central_r200 and not retain_ns:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            if verbose:
                print "rebinding _ts"

            # In this case we need to rebind for haloes from _ts on this step, and _ps on the next step
            if retain_memory and i_snip+1 < len(snip_list[0:-1]):
                
                snip_next_step = snip_list[i_snip+1]
                filename_next_step = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_next_step)+"_"+str(subvol)+".hdf5"
                output_path_next_step = output_path + "Snip_"+str(snip_next_step)+"/"
                print "For _ts rebinding, reading", output_path_next_step+filename_next_step
                File_next_step = h5py.File(output_path_next_step+filename_next_step,"r")
                
                all_progenitor_group = File_next_step["all_progenitor_data"]

                x_next_step = all_progenitor_group["x_halo"][:]
                y_next_step = all_progenitor_group["y_halo"][:]
                z_next_step = all_progenitor_group["z_halo"][:]
                grn_next_step = all_progenitor_group["group_number"][:]
                sgrn_next_step = all_progenitor_group["subgroup_number"][:]
                r200_next_step = all_progenitor_group["r200_host"][:]
                iI_next_step = all_progenitor_group["isInterpolated"][:]

                File_next_step.close()

                nsep = max(len(str(int(sgrn_next_step.max()))) +1 ,6)

                cat_list_next_step = grn_next_step.astype("int64") * 10**nsep + sgrn_next_step.astype("int64")
                cat_list = subhalo_props["group_number"].astype("int64") * 10**nsep + subhalo_props["subgroup_number"].astype("int64")

                cat_master_list = np.concatenate((cat_list_next_step,cat_list))
                cat_master_list, ind_unique = np.unique(cat_master_list,return_index=True)

                halo_coordinates_temp = [np.append(x_next_step,subhalo_props["x_halo"])[ind_unique],\
                                       np.append(y_next_step,subhalo_props["y_halo"])[ind_unique],\
                                       np.append(z_next_step,subhalo_props["z_halo"])[ind_unique]]
                grn_temp = np.append(grn_next_step, subhalo_props["group_number"])[ind_unique]
                sgrn_temp = np.append(sgrn_next_step, subhalo_props["subgroup_number"])[ind_unique]
                r200_temp = np.append(r200_next_step, subhalo_props["r200_host"])[ind_unique]
                iI_temp = np.append(iI_next_step, subhalo_props["isInterpolated"])[ind_unique]
            else:
                # Otherwise we are rebinding for _ts haloes on this step only
                halo_coordinates_temp = [subhalo_props["x_halo"], subhalo_props["y_halo"], subhalo_props["z_halo"]]
                grn_temp = subhalo_props["group_number"]; sgrn_temp = subhalo_props["subgroup_number"]; r200_temp = subhalo_props["r200_host"]; iI_temp = subhalo_props["isInterpolated"]

            # gas rebinding
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
            temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_temp, r200_temp, boxsize)
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
            gas_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ts_in)

            if not bound_only_mode:
                mpf.Rebind_All_Particles_From_Bound(all_gas_ts_in, gas_ts_in)

            if len(dm_names_in_ps) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ts_in)

            if len(star_ts_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                star_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ts_in)

            if len(bh_ts_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(bh_ts_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                bh_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, bh_ts_in)
        
        else:
            grn_temp = subhalo_props["group_number"] # Used to decide which particles to keep at the end of this sub-loop


        t_rebind += time.time() - t_rebind_i

        bound_parts_ts = all_gas_ts_in.is_bound()
        not_bound_parts_ts = bound_parts_ts == False

        if central_r200:
            # In this case include subfind unbound particles as being unbound for the unbound particle list (this is used to identify "flythrough" wind particles later on)
            ok_unbound_ts = not_bound_parts_ts | all_gas_ts_in.data["subfind_unbound"]
        else:
            ok_unbound_ts = not_bound_parts_ts

        if len(gas_halo_reheat_ts.data["id"]) > 0 and len(all_gas_ts_in.data["id"][ok_unbound_ts])>0:
            if i_subvol == 0:
                gas_halo_reheat_ts.add_data(np.zeros((len(gas_halo_reheat_ts.data["id"]),3))+1e9,"coordinates")

            ptr = ms.match(gas_halo_reheat_ts.data["id"], all_gas_ts_in.data["id"][ok_unbound_ts])
            ok_match = ptr >= 0
            gas_halo_reheat_ts.data["coordinates"][ok_match] = all_gas_ts_in.data["coordinates"][ok_unbound_ts][ptr][ok_match]
            
            if write_SNe_energy_fraction:
                if i_subvol == 0:
                    gas_halo_reheat_ts.add_data(np.zeros(len(gas_halo_reheat_ts.data["id"]))<0,"dir_heated")

                dT_SNe = 10.0**7.5 # K
                dT_AGN_thr = 10.0**8 # K
                gas_halo_reheat_ts.data["dir_heated"][ok_match] = all_gas_ts_in.data["Tmax"][ok_unbound_ts][ptr][ok_match] > dT_SNe
                
            # Search for bound particles and give Nan as their position, to enable the status of particles later on in wind calculations
            if len(gas_halo_reheat_ts.data["id"]) > 0:
                ptr = ms.match(gas_halo_reheat_ts.data["id"], np.append(all_gas_ts_in.data["id"][ok_unbound_ts==False], star_ts_in.data["id"]) )
                ok_match = ptr >= 0
                
                gas_halo_reheat_ts.data["coordinates"][ok_match] = np.nan

        elif i_subvol == 0:
            gas_halo_reheat_ts.add_data(np.zeros((0,3)),"coordinates")
            if write_SNe_energy_fraction:
                gas_halo_reheat_ts.add_data(np.zeros(0)<0,"dir_heated")

        # Make a list of all gas particles which are bound on _ps but unbound on _ts #
        # This is going to be used to more efficiently identify the coordinates of these gas particles later when identifying halo winds
        # Edit: 31/10/2019 - this is now also going to be used to identify FOF ejecta for satellites and low-mass centrals
        ptr = ms.match(gas_ps.data["id"], all_gas_ts_in.data["id"][ok_unbound_ts])
        ok_match = ptr>=0

        if "unbound_ts_ns" not in gas_ps.names:
            gas_ps.add_append_data(ok_match,"unbound_ts_ns") # Boolean that is True if particle is going to be unbound on _ts (will check _ns later)
        else:
            print "Hmm unexpected (unless using sub-subvolume IO, which is no longer allowed), stopping here"
            quit()

        if i_subvol == 0:
            gas_unbound_ts = mpc.DataSet()
        gas_unbound_ts.add_append_data(gas_ps.data["id"][ok_match],"id")
        gas_unbound_ts.add_append_data(gas_ps.data["group_number"][ok_match],"group_number_progen")
        gas_unbound_ts.add_append_data(gas_ps.data["subgroup_number"][ok_match],"subgroup_number_progen")

        gas_unbound_ts.add_append_data(all_gas_ts_in.data["coordinates"][ok_unbound_ts][ptr][ok_match],"coordinates")
        gas_unbound_ts.add_append_data(all_gas_ts_in.data["group_number"][ok_unbound_ts][ptr][ok_match], "group_number_particle")
        
        # Find any (formerly bound) particles that left the box (or left FoF+SO groups for bound_only_mode) - given them a grn of zero, and put at infinity
        ptr_box = ms.match(gas_ps.data["id"], np.append(star_ts_in.data["id"],all_gas_ts_in.data["id"]))
        ok_unbound_ps = ptr_box < 0

        if nsubvol_pts > 1:
            print "Stopping here. The sub-subvolume IO option no longer works with the above lines r.e. finding unbound particles that have left the 'box'"
            print "We will end up adding every single particle to gas_unbound_ts if we do this...."
            quit()

        gas_ps.data["unbound_ts_ns"][ok_unbound_ps] = True

        gas_unbound_ts.add_append_data(gas_ps.data["id"][ok_unbound_ps],"id")
        gas_unbound_ts.add_append_data(gas_ps.data["group_number"][ok_unbound_ps],"group_number_progen")
        gas_unbound_ts.add_append_data(gas_ps.data["subgroup_number"][ok_unbound_ps],"subgroup_number_progen")
        n_add = len(ptr[ok_unbound_ps])
        gas_unbound_ts.add_append_data(np.zeros((n_add,3))+1e9,"coordinates")
        gas_unbound_ts.add_append_data(np.zeros(n_add)-67, "group_number_particle")
        
        if write_SNe_energy_fraction:
            dT_SNe = 10**7.5 # K
            dT_AGN_thr = 10**8
            gas_unbound_ts.add_append_data(all_gas_ts_in.data["Tmax"][ok_unbound_ts][ptr][ok_match]>dT_SNe,"dir_heated")
            gas_unbound_ts.add_append_data((all_gas_ts_in.data["Tmax"][ok_unbound_ts][ptr][ok_match]>dT_SNe)&(all_gas_ts_in.data["Tmax"][ok_unbound_ts][ptr][ok_match]<dT_AGN_thr),"dir_heated_SNe")
            # Assume particles outside the box were directly heated (not sure this actually does anything though)
            gas_unbound_ts.add_append_data(np.zeros(n_add)>-1,"dir_heated")
            gas_unbound_ts.add_append_data(np.zeros(n_add)>-1,"dir_heated_SNe")

        # To avoid reading gas velocity data for unbound particles, just set to 1e9 for anything unbound
        gas_unbound_ts.add_append_data(np.zeros_like(gas_unbound_ts.data["coordinates"])+1e9,"velocity")
        
        if not bound_only_mode:
            bound_parts_ts = gas_ts_in.is_bound()
            not_bound_parts_ts = bound_parts_ts == False

            if central_r200:
                # In this case include subfind unbound particles as being unbound for the unbound particle list (this is used to identify "flythrough" wind particles later on)
                ok_unbound_ts = not_bound_parts_ts | gas_ts_in.data["subfind_unbound"]
            else:
                ok_unbound_ts = not_bound_parts_ts
        
        ptr = ms.match(gas_unbound_ts.data["id"], gas_ts_in.data["id"][ok_unbound_ts])
        ok_match = ptr>=0
        gas_unbound_ts.data["velocity"][ok_match] = gas_ts_in.data["velocity"][ok_unbound_ts][ptr][ok_match]

        # If asked for, find the coordinates of all wind and ejecta particles
        if extra_accretion_measurements and not bound_only_mode:
            if len(gas_ejecta_ts.data["id"]) > 0:
                if i_subvol == 0:
                    # Assume particles that we can't find are at infinity
                    gas_ejecta_ts.add_data(np.zeros((len(gas_ejecta_ts.data["id"]),3))+1e9,"coordinates")

                ptr = ms.match(gas_ejecta_ts.data["id"], all_gas_ts_in.data["id"])
                ok_match = ptr >= 0
                gas_ejecta_ts.data["coordinates"][ok_match] = all_gas_ts_in.data["coordinates"][ptr][ok_match]
                ptr = ms.match(gas_ejecta_ts.data["id"], star_ts_in.data["id"])
                ok_match = ptr >= 0
                gas_ejecta_ts.data["coordinates"][ok_match] = star_ts_in.data["coordinates"][ptr][ok_match]
            elif i_subvol == 0:
                gas_ejecta_ts.add_data(np.zeros((0,3)),"coordinates")

            if len(gas_wind_ts.data["id"]) > 0:
                if i_subvol == 0:
                    gas_wind_ts.add_data(np.zeros((len(gas_wind_ts.data["id"]),3))+1e9,"coordinates")

                ptr = ms.match(gas_wind_ts.data["id"], all_gas_ts_in.data["id"])
                ok_match = ptr >= 0
                gas_wind_ts.data["coordinates"][ok_match] = all_gas_ts_in.data["coordinates"][ptr][ok_match]
                ptr = ms.match(gas_wind_ts.data["id"], star_ts_in.data["id"])
                ok_match = ptr >= 0
                gas_wind_ts.data["coordinates"][ok_match] = star_ts_in.data["coordinates"][ptr][ok_match]
            elif i_subvol == 0:
                gas_wind_ts.add_data(np.zeros((0,3)),"coordinates")

        # Put all gas dataset out of scope
        all_gas_ts_in = None


        if not retain_memory: # If we are passing on the particle data, we will want unbound particles to find their properties for halo reheat
            bound_parts_ts = gas_ts_in.is_bound()
            gas_ts_in.select_bool(bound_parts_ts)

        # De-scope boolean arrays
        bounds_parts_ts = None; not_bound_parts = None; ok_unbound_ps = None; ok_unbound_ts = None

        # Remove bound particles from other particle types
        if not retain_memory:
            star_ts_in.select_bool(star_ts_in.is_bound())
            bh_ts_in.select_bool(bh_ts_in.is_bound())
            if len(dm_names_in_ts) > 0:
                dm_ts_in.select_bool(dm_ts_in.is_bound())
        
        # Keep only particles bound to subhalos from this subvolume (as opposed to bound to any halo in the simulation)
        if not retain_memory: # As before, we will want the other particles for halo_reheat on the next step if we pass the data on
            gas_ts_in.ptr_prune(grn_temp,"group_number")
            star_ts_in.ptr_prune(grn_temp,"group_number")
            bh_ts_in.ptr_prune(grn_temp,"group_number")
            if len(dm_names_in_ts) > 0:
                dm_ts_in.ptr_prune(grn_temp,"group_number")


        if nsubvol_pts > 1:

            # Init datasets
            if i_subvol == 0:
                gas_ts = mpc.DataSet(); dm_ts = mpc.DataSet(); star_ts = mpc.DataSet(); bh_ts = mpc.DataSet()

            # Merge datasets
            gas_ts.merge_dataset(gas_ts_in)
            star_ts.merge_dataset(star_ts_in)
            bh_ts.merge_dataset(bh_ts_in)
            if not skip_dark_matter:
                dm_ts.merge_dataset(dm_ts_in)

            # Put the input datasets out of scope
            gas_ts_in = None; dm_ts_in = None; star_ts_in = None; bh_ts_in = None
        else:
            gas_ts = gas_ts_in; dm_ts = dm_ts_in; star_ts = star_ts_in; bh_ts = bh_ts_in


    # Construct master list of ejecta and wind particles to account for inter-galaxy/halo transfer of gas
    id_ejecta_master = gas_ejecta_ts.data["id"]
    if bound_only_mode or not extra_accretion_measurements:
        # This concat may have been performed earlier as part of the gas_wind_ts dataset
        id_wind_master = np.concatenate(wind_data_ts["id"])
    else:
        id_wind_master = gas_wind_ts.data["id"]
    from_SF_wind_master = np.concatenate(wind_data_ts["from_SF"])
    if extra_accretion_measurements:
        mchalo_ej_wind_master = np.concatenate(wind_data_ts["mchalo_ej"])
        mchalo_ej_ejecta_master = np.concatenate(ejecta_data_ts["mchalo_ej"])

    ####### _ns ###########
    print "reading particle data for _ns (next snipshot)"

    gas_names_in_ns = ['Mass', "Density", "Metallicity", "Temperature", 'Coordinates']
    gas_names_out_ns = ['mass', "density", "metallicity", "temperature", 'coordinates']

    if write_SNe_energy_fraction:
        gas_names_in_ns += ["MaximumTemperature","ElementAbundance/Oxygen", "ElementAbundance/Iron"]
        gas_names_out_ns += ["Tmax",             "oxygen",                  "iron"                 ]

    if ism_definition == "Mitchell18_adjusted":
        gas_names_in_ns += ["InternalEnergy","Velocity"]
        gas_names_out_ns += ["internal_energy", "velocity"]

    all_gas_names_in_ns = []; all_gas_names_out_ns = []
    if not bound_only_mode:
        all_gas_names_in_ns += ['Coordinates']
        all_gas_names_out_ns += ['coordinates']

    dm_names_in_ns = []
    dm_names_out_ns = []

    star_names_in_ns = ['Mass','Coordinates']
    star_names_out_ns = ['mass','coordinates']
    
    bh_names_in_ns = []
    bh_names_out_ns = []

    if ism_definition == "Mitchell18_adjusted":
        dm_names_in_ns += ["Coordinates"]
        dm_names_out_ns += ["coordinates"]

        bh_names_in_ns += ['BH_Mass', 'Coordinates']
        bh_names_out_ns += ['mass', 'coordinates']

    if d_snap_ns == 1 and retain_memory: # If we are going to carry this data over to later snapshots, we need to ensure we read all variables that will be needed later
        # Note that I'm assuming here that any _ps variables will also be in _ts
        for name_in, name_out in zip(gas_names_in_ts,gas_names_out_ts):
            if name_in not in gas_names_in_ns:
                gas_names_in_ns.append(name_in); gas_names_out_ns.append(name_out)
        for name_in, name_out in zip(all_gas_names_in_ts,all_gas_names_out_ts):
            if name_in not in all_gas_names_in_ns:
                all_gas_names_in_ns.append(name_in); all_gas_names_out_ns.append(name_out)
        for name_in, name_out in zip(dm_names_in_ts,dm_names_out_ts):
            if name_in not in dm_names_in_ns:
                dm_names_in_ns.append(name_in); dm_names_out_ns.append(name_out)
        for name_in, name_out in zip(star_names_in_ts,star_names_out_ts):
            if name_in not in star_names_in_ns:
                star_names_in_ns.append(name_in); star_names_out_ns.append(name_out)
        for name_in, name_out in zip(bh_names_in_ts,bh_names_out_ts):
            if name_in not in bh_names_in_ns:
                bh_names_in_ns.append(name_in); bh_names_out_ns.append(name_out)
        
    data_names_in_ns = [gas_names_in_ns, dm_names_in_ns, star_names_in_ns, bh_names_in_ns, all_gas_names_in_ns,[],[],[]]
    data_names_out_ns = [gas_names_out_ns, dm_names_out_ns, star_names_out_ns, bh_names_out_ns, all_gas_names_out_ns,[],[],[]]

    if vol_ps > volmax:
        nsubvol_ns = 8
        
        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ns
        
        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ns[0]-xyz_min_ns[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ns[1]-xyz_min_ns[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ns[2]-xyz_min_ns[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1
        
        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ns[0]-xyz_min_ns[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ns[1]-xyz_min_ns[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ns[2]-xyz_min_ns[2])*0.5
    else:
        nsubvol_ns = 1

    for i_subvol in range(nsubvol_ns):
        
        if nsubvol_ns > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_ns
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ns
            xyz_subvol_max_i = xyz_max_ns


        t_core_io_i = time.time()

        if nsub1d > 1 or test_mode or not bound_only_mode:
            if bound_only_mode:
                read_all = ""
                gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, sim_props_ns = mpio.Read_All_Particle_Data(snip_ns, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ns,data_names_out_ns, read_all=read_all,verbose=verbose)
                all_gas_ns_in = gas_ns_in
            else:
                read_all = "g"
                gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, all_gas_ns_in, sim_props_ns = mpio.Read_All_Particle_Data(snip_ns, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ns,data_names_out_ns, read_all=read_all,verbose=verbose)

        else:
            # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
            data_names_in_ns = gas_names_in_ns+ dm_names_in_ns+ star_names_in_ns+ bh_names_in_ns
            data_names_out_ns = gas_names_out_ns+ dm_names_out_ns+ star_names_out_ns+ bh_names_out_ns
            part_types = np.concatenate(( np.zeros(len(gas_names_in_ns)), np.ones(len(dm_names_in_ns)), np.zeros(len(star_names_in_ns))+4, np.zeros(len(bh_names_in_ns))+5 )).astype("int")

            gas_ns_in, dm_ns_in, star_ns_in, bh_ns_in, sim_props_ns = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ns, sim_name, data_names_in_ns,data_names_out_ns,part_types,njobs=n_jobs_parallel,verbose=verbose)
            all_gas_ns_in = gas_ns_in

        t_core_io += time.time() - t_core_io_i
        
        t_rebind_i = time.time()
        if central_r200:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo

            # For _ns, we could in principle have duplicate entries in the subhalo lists, so need to make a unique list here (just for re-binding)
            nsep = max(len(str(int(subhalo_props["subgroup_number_ns"][np.isnan(subhalo_props["subgroup_number_ns"])==False].max()))) +1 ,6)
            sub_ind_ns = subhalo_props["group_number_ns"]* 10**nsep + subhalo_props["subgroup_number_ns"]
            junk, ind_unique = np.unique(sub_ind_ns, return_index=True)
            grn_temp = subhalo_props["group_number_ns"][ind_unique]
            sgrn_temp = subhalo_props["subgroup_number_ns"][ind_unique]
            r200_temp = subhalo_props["r200_host_ns"][ind_unique]
            iI_temp = subhalo_props["isInterpolated_ns"][ind_unique]

            halo_coordinates_temp = [subhalo_props["x_halo_ns"][ind_unique],subhalo_props["y_halo_ns"][ind_unique],subhalo_props["z_halo_ns"][ind_unique]]

            # If we are carrying this data over to future steps, we need to ensure rebinding is performed for all relevant halos (this is going to be a bit of a painful block of code)
            if d_snap_ns == 1 and retain_memory:
                
                for i_temp in range(2):
                    i_next_step = i_snip + i_temp + 1

                    if i_next_step < len(snip_list[0:-1]):

                        snip_next_step = snip_list[i_next_step]
                        filename_next_step = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_next_step)+"_"+str(subvol)+".hdf5"
                        output_path_next_step = output_path + "Snip_"+str(snip_next_step)+"/"
                        if verbose:
                            print "For _ns rebinding, reading", output_path_next_step+filename_next_step
                        File_next_step = h5py.File(output_path_next_step+filename_next_step,"r")

                        if i_temp == 0:
                            subhalo_group_temp = File_next_step["subhalo_data"]
                        else:
                            subhalo_group_temp = File_next_step["all_progenitor_data"]

                        x_next_step = subhalo_group_temp["x_halo"][:]
                        y_next_step = subhalo_group_temp["y_halo"][:]
                        z_next_step = subhalo_group_temp["z_halo"][:]
                        grn_next_step = subhalo_group_temp["group_number"][:]
                        sgrn_next_step = subhalo_group_temp["subgroup_number"][:]
                        r200_next_step = subhalo_group_temp["r200_host"][:]
                        iI_next_step = subhalo_group_temp["isInterpolated"][:]

                        File_next_step.close()

                        nsep = max(len(str(int(sgrn_next_step.max()))) +1 ,6)

                        cat_list_next_step = grn_next_step.astype("int64") * 10**nsep + sgrn_next_step.astype("int64")
                        cat_list = grn_temp.astype("int64") * 10**nsep + sgrn_temp.astype("int64")

                        cat_master_list = np.concatenate((cat_list_next_step,cat_list))
                        cat_master_list, ind_unique = np.unique(cat_master_list,return_index=True)

                        halo_coordinates_temp = [np.append(x_next_step,halo_coordinates_temp[0])[ind_unique],\
                                                 np.append(y_next_step,halo_coordinates_temp[1])[ind_unique],\
                                                 np.append(z_next_step,halo_coordinates_temp[2])[ind_unique]]
                        grn_temp = np.append(grn_next_step, grn_temp)[ind_unique]
                        sgrn_temp = np.append(sgrn_next_step, sgrn_temp)[ind_unique]
                        r200_temp = np.append(r200_next_step, r200_temp)[ind_unique]
                        iI_temp = np.append(iI_next_step, iI_temp)[ind_unique]

            if verbose:
                print "rebinding _ns"

            # gas rebinding
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
            temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_temp, r200_temp, boxsize)
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
            gas_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ns_in)

            if not bound_only_mode:
                mpf.Rebind_All_Particles_From_Bound(all_gas_ns_in, gas_ns_in)

            if len(dm_names_in_ns) > 0:
                # dark matter rebinding
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ns_in)

            if len(star_names_in_ns) > 0:
                # star rebinding
                if len(star_ns_in.data["id"])>0:
                    particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                    temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                    output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                    star_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ns_in)

            if len(bh_names_in_ns) > 0:
                # bh rebinding
                if len(bh_ns_in.data["id"])>0:
                    particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(bh_ns_in, grn_temp, sgrn_temp, halo_coordinates_temp,r200_temp, iI_temp, boxsize, verbose=verbose)
                    temp = np.arange(0,len(grn_temp))[sgrn_temp==0]
                    output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                    bh_ns_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, bh_ns_in)

        t_rebind += time.time() - t_rebind_i

        bound_parts_ns = all_gas_ns_in.is_bound()
        not_bound_parts_ns = bound_parts_ns == False

        if i_subvol == 0:
            gas_halo_reheat_ns = mpc.DataSet()

        if central_r200:
            ok_unbound_ns = not_bound_parts_ns|all_gas_ns_in.data["subfind_unbound"]
        else:
            ok_unbound_ns = not_bound_parts_ns

        if len(gas_halo_reheat_ts.data["id"]) > 0 and len(all_gas_ns_in.data["id"][ok_unbound_ns])>0:
            if i_subvol == 0:
                gas_halo_reheat_ns.add_data(gas_halo_reheat_ts.data["id"],"id")
                gas_halo_reheat_ns.add_data(gas_halo_reheat_ts.data["group_number"],"group_number_progen")
                gas_halo_reheat_ns.add_data(gas_halo_reheat_ts.data["subgroup_number"],"subgroup_number_progen")
                gas_halo_reheat_ns.add_data(np.zeros((len(gas_halo_reheat_ts.data["id"]),3))+2e9,"coordinates")

            ptr = ms.match(gas_halo_reheat_ts.data["id"], all_gas_ns_in.data["id"][ok_unbound_ns])
            ok_match = ptr >= 0
            gas_halo_reheat_ns.data["coordinates"][ok_match] = all_gas_ns_in.data["coordinates"][ok_unbound_ns][ptr][ok_match]
            
            # Search for bound particles and give Nan as their position, to enable the status of particles later on in wind calculations
            if len(gas_halo_reheat_ns.data["id"]) > 0:
                ptr = ms.match(gas_halo_reheat_ns.data["id"], np.append(all_gas_ns_in.data["id"][ok_unbound_ns==False], star_ns_in.data["id"]) )
                ok_match = ptr >= 0
                
                gas_halo_reheat_ns.data["coordinates"][ok_match] = np.nan

        elif i_subvol == 0:
            gas_halo_reheat_ns.add_data(np.zeros((0,3)),"coordinates")
            gas_halo_reheat_ns.add_data(np.zeros(0),"id")
            gas_halo_reheat_ns.add_data(np.zeros(0),"group_number_progen")
            gas_halo_reheat_ns.add_data(np.zeros(0),"subgroup_number_progen")
            
        # As a second step, identify the coordinates of the same particles on the next step (searching both all gas list and bound star list)
        ptr = ms.match(gas_unbound_ts.data["id"], np.append(all_gas_ns_in.data["id"],star_ns_in.data["id"]))
        ok_match = ptr>=0

        if i_subvol == 0:
            part_unbound_ns = mpc.DataSet()

        part_unbound_ns.add_append_data(np.append(all_gas_ns_in.data["coordinates"],star_ns_in.data["coordinates"],axis=0)[ptr][ok_match],"coordinates")
        part_unbound_ns.add_append_data(np.append(all_gas_ns_in.data["group_number"],star_ns_in.data["group_number"])[ptr][ok_match],"group_number")
        part_unbound_ns.add_append_data(gas_unbound_ts.data["id"][ok_match],"id")
        part_unbound_ns.add_append_data(gas_unbound_ts.data["group_number_progen"][ok_match],"group_number_progen")
        part_unbound_ns.add_append_data(gas_unbound_ts.data["subgroup_number_progen"][ok_match],"subgroup_number_progen")

        # Flag particles that will rejoin a FOF group on _ns (used for satellites to identify ejecta) - assume positively matched particles are not genuine ejecta 
        if len(gas_ps.data["id"][gas_ps.data["unbound_ts_ns"]])> 0 and len(part_unbound_ns.data["id"][part_unbound_ns.data["group_number"]>=0])> 0:
            ptr = ms.match(gas_ps.data["id"][gas_ps.data["unbound_ts_ns"]], part_unbound_ns.data["id"][part_unbound_ns.data["group_number"]>=0])
            rebound_ns = ptr >= 0
            temp = gas_ps.data["unbound_ts_ns"][gas_ps.data["unbound_ts_ns"]]
            temp[rebound_ns] = False
            gas_ps.data["unbound_ts_ns"][gas_ps.data["unbound_ts_ns"]] = temp
            temp = None

        # Put all gas dataset out of scope
        all_gas_ns_in = None

        if d_snap_ns != 1 or retain_memory == False:
            bound_parts_ns = gas_ns_in.is_bound()

            gas_ns_in.select_bool(bound_parts_ns)
            star_ns_in.select_bool(star_ns_in.is_bound())

            gas_ns_in.ptr_prune(grn_temp,"group_number")
            star_ns_in.ptr_prune(grn_temp,"group_number")

        if len(dm_names_in_ns) > 0:
            if d_snap_ns != 1 or retain_memory == False:
                dm_ns_in.select_bool(dm_ns_in.is_bound())
                dm_ns_in.ptr_prune(grn_temp,"group_number")

        if len(bh_names_in_ns) > 0:
            if d_snap_ns != 1 or retain_memory == False:
                bh_ns_in.select_bool(bh_ns_in.is_bound())
                bh_ns_in.ptr_prune(grn_temp,"group_number")
            

        # Init datasets
        if i_subvol == 0:
            gas_ns = mpc.DataSet(); dm_ns = mpc.DataSet(); star_ns = mpc.DataSet(); bh_ns = mpc.DataSet()

        if nsubvol_ns > 1:

            # Merge datasets
            gas_ns.merge_dataset(gas_ns_in)
            if ism_definition == "Mitchell18_adjusted":
                dm_ns.merge_dataset(dm_ns_in)
                bh_ns.merge_dataset(bh_ns_in)
                star_ns.merge_dataset(star_ns_in)

            # Put the input datasets out of scope
            gas_ns_in = None; dm_ns_in = None; star_ns_in = None; bh_ns_in = None
        else:
            gas_ns = gas_ns_in; dm_ns = dm_ns_in; star_ns = star_ns_in; bh_ns = bh_ns_in

        bound_parts_ns = None; not_bound_parts_ns = None

    print "finished io, time elapsed", time.time() - t2, "time elapsed for core IO", t_core_io, "time for rebinding", t_rebind

    t4 = time.time()

    ####### Unpack and get simulation characteristics for this point of the snipshot loop ########
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    expansion_factor_ps = sim_props_ps["expansion_factor"]
    expansion_factor_ns = sim_props_ns["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    redshift_ps = sim_props_ps["redshift"]
    redshift_ns = sim_props_ns["redshift"]
    mass_dm = sim_props_ts["mass_dm"]

    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    # Work out 10 time bins from now to the first snapshot, logarithmically spaced in lookback time
    # These are used to compute the binned ejected/return time distributions for wind/ejecta particles
    t_ls = uc.t_Universe(1.0, omm, h)[0]
    tlb_fs = t_ls - cosmic_t_list_initial[0] # 18/2/2020 - fixed this so that restarts don't change the bin definitions!
    tlb_ts = t_ls - cosmic_t_list[i_snip]

    n_t_bins = 10
    dlntlb = (np.log(tlb_fs)-np.log(tlb_ts))/(n_t_bins)
    tret_bins_ts = np.exp( np.arange( np.log(tlb_ts), np.log(tlb_fs)+dlntlb, dlntlb) )[0:n_t_bins+1] - tlb_ts

    #print tret_bins_ts, tlb_fs, tlb_ts
    #continue

    output_t_dict = {}
    for name in output_t_names:
        n_r_bins_track = 13 # Should match n_r_bins in main_loop
        if "n_r" in name:
            output_t_dict[name] = np.zeros((len(mchalo_list),n_r_bins_track))
        else:
            output_t_dict[name] = np.zeros((len(mchalo_list),len(tret_bins_ts)-1))







    ##################### Sort the main particle arrays and create indexes for each subhalo (for efficient indexing within the main loop) ##############
    
    print "Sorting particle lists and creating indexes"
    time_sort = time.time()

    gas_ps_subhalo_index = gas_ps.set_index(nsep_ps, return_subhalo_index=True)
    gas_ts.set_index(nsep_ts)
    gas_ns.set_index(nsep_ns)

    gas_unbound_ts.set_index(nsep_ps,extra_string="_progen") # nsep_ps is used because this list is going to be sorted on the basis of group_number/subgroup_number from _ps
    part_unbound_ns.set_index(nsep_ps,extra_string="_progen") # same here

    if not skip_dark_matter:
        dm_ps_subhalo_index = dm_ps.set_index(nsep_ps, return_subhalo_index=True)
        dm_ts.set_index(nsep_ts)
        if ism_definition == "Mitchell18_adjusted":
            dm_ns.set_index(nsep_ns)

    star_ps_subhalo_index = star_ps.set_index(nsep_ps, return_subhalo_index=True)
    star_ts.set_index(nsep_ts)
    star_ns.set_index(nsep_ns)

    bh_ps_subhalo_index = bh_ps.set_index(nsep_ps, return_subhalo_index=True)
    bh_ts.set_index(nsep_ts)
    if ism_definition == "Mitchell18_adjusted":
        bh_ns.set_index(nsep_ns)

    gas_halo_reheat_ps.set_index(nsep_ts,extra_string="_descendant")
    gas_halo_reheat_ts.set_index(nsep_ts)
    gas_halo_reheat_ns.set_index(nsep_ts,extra_string="_progen")

    gas_ejecta_ts.set_index(nsep_ts)
    if extra_accretion_measurements and not bound_only_mode:
        gas_wind_ts.set_index(nsep_ts)

    cumul_list_ps = []
    for name in cumul_names:
        cumul_list_ps.append(cumul_data_ps[name])

    # John gave the interpolated halos huge IDs, which I need to edit here to make sure we can convert to a float
    # Hacky workaround is the following (which should still allow nodeindex and descendant index to match
    
    descendant_index_list_ps_temp = np.copy(descendant_index_list_ps)
    finished = False
    while not finished:
        problem_di = descendant_index_list_ps_temp > 1e15
        descendant_index_list_ps_temp[problem_di] -= 10000000000000000
        if len(problem_di[problem_di])==0:
            finished = True

    ni_temp = np.copy(subhalo_props["node_index"])
    iI = subhalo_props["isInterpolated"] == 1

    finished = False
    while not finished:
        problem_ni = ni_temp > 1e15
        ni_temp[problem_ni] -= 10000000000000000
        if len(problem_ni[problem_ni])==0:
            finished = True

    iI = subhalo_props["isInterpolated"] == 1
    if len(ni_temp[iI]) != len(np.unique(ni_temp[iI])):
        print "Error: we have duplicate node index entries after trying to correct for very large node_index of interpolated haloes"
        print "This could be because of the correct, but could also be from the subvol catalogue in principle"
        print "Commented out below I wrote a trial solution to this - which you can try"
        for ni in ni_temp:
            if len(ni_temp[ni==ni_temp])>1:
                print ni
        quit()

    '''duplicates = True
    while duplicates:
        if len(ni_temp) == len(np.unique(ni_temp)):
            duplicates = False
        else:
            print "hello"
            nI = subhalo_props["isInterpolated"] == 0
            iI = nI == False
            for i_sub in range(len(iI[iI])):
                if ni_temp[iI][i_sub] in ni_temp[nI]:
                    temp = ni_temp[nI]
                    temp[i_sub] += 1
                    ni_temp[nI] = temp
                    change = ni_temp[iI][i_sub]-1 == descendant_index_list_ps_temp
                    descendant_index_list_ps_temp[change] += 1
                    if len(change[change])>0:
                        print "yes2", len(change[change])

                    print "yes"'''

    cumul_list_ts = mpf.Sum_Common_ID(cumul_list_ps, descendant_index_list_ps_temp, ni_temp)
    cumul_data_ts = {}
    for i_name, name in enumerate(cumul_names):
        cumul_data_ts[name] = cumul_list_ts[i_name]

    print "Sorting/index creation completed, took", time.time() - time_sort, "seconds"







    ##################### Create arrays for each FoF group (this is an optimization step to avoid repeating this step for satellites within the main loop) ##############
    print "Computing FoF lists"
    time_fof = time.time()
    
    # Create tools to efficiently index the particle arrays within the main loop

    ####### Start with the list of particles bound to each _ts FoF group #######
    star_gas_grn_sorted = mpc.DataSet()

    star_gas_grn_sorted.add_data( np.append(gas_ts.data["group_number"],star_ts.data["group_number"]) , "group_number")
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["subgroup_number"],star_ts.data["subgroup_number"]) , "subgroup_number")
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["id"],star_ts.data["id"]) , "id")
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["coordinates"],star_ts.data["coordinates"],axis=0) , "coordinates")
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["mass"],np.zeros_like(star_ts.data["mass"])) , "mass_gas_only") # This is for computing gas halo merger rates, neglect any stars with age < dt for now  
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["mass"],star_ts.data["mass"]) , "mass")
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["velocity"],np.zeros_like(star_ts.data["coordinates"]),axis=0) , "velocity")
    star_gas_grn_sorted.add_data( np.append(gas_ts.data["metallicity"],star_ts.data["metallicity"]) , "metallicity")
   
    if write_SNe_energy_fraction:
        dT_SNe = 10.0**7.5
        star_gas_grn_sorted.add_data( np.append( gas_ts.data["Tmax"] > dT_SNe, np.zeros_like(star_ts.data["mass"])<0), "dir_heated")
        star_gas_grn_sorted.add_data( np.append( gas_ts.data["oxygen"], np.zeros_like(star_ts.data["mass"])), "oxygen")
        star_gas_grn_sorted.add_data( np.append( gas_ts.data["iron"], np.zeros_like(star_ts.data["mass"])), "iron")
        if extra_wind_measurements:
            star_gas_grn_sorted.add_data( np.append(np.zeros_like(gas_ts.data["mass"]),star_ts.data["mass_init"]) , "mass_init")
            star_gas_grn_sorted.add_data( np.append(np.zeros_like(gas_ts.data["mass"]),star_ts.data["fb_e_fraction"]) , "fb_e_fraction")
            star_gas_grn_sorted.add_data( np.append(np.ones_like(gas_ts.data["mass"]),star_ts.data["aform"]) , "aform")

    # For efficiently indexing all particles associated with a given FoF group on _ts, need to sort by group_number
    star_gas_grn_sorted.sort("group_number")

    star_gas_grn_sorted.grn_unique, star_gas_grn_sorted.grn_index = np.unique(star_gas_grn_sorted.data["group_number"], return_index=True)
    star_gas_grn_sorted.grn_index = np.append(star_gas_grn_sorted.grn_index,len(star_gas_grn_sorted.data["group_number"]))

    if write_SNe_energy_fraction and extra_wind_measurements:
        bh_grn_sorted = mpc.DataSet()
        bh_grn_sorted.add_data(bh_ts.data["group_number"],"group_number")
        bh_grn_sorted.add_data(bh_ts.data["subgroup_number"],"subgroup_number")
        bh_grn_sorted.add_data(bh_ts.data["id"],"id")
        bh_grn_sorted.add_data(bh_ts.data["mass"],"mass")
        bh_grn_sorted.add_data(bh_ts.data["nseed"],"nseed")
        bh_grn_sorted.add_data(bh_ts.data["coordinates"],"coordinates")
        bh_grn_sorted.sort("group_number")
        bh_grn_sorted.grn_unique, bh_grn_sorted.grn_index = np.unique(bh_grn_sorted.data["group_number"], return_index=True)
        bh_grn_sorted.grn_index = np.append(bh_grn_sorted.grn_index,len(bh_grn_sorted.data["group_number"]))
    else:
        bh_grn_sorted = []

    ###### Now do the list of particles bound to all progenitors of each _ts FoF group #########
    ptr = ms.match(subhalo_props["descendant_index_all_progen"], np.append(subhalo_props["node_index"],removed_props_ts["node_index"]))
    ok_match = ptr>=0
    if len(ok_match[ok_match]) < len(ok_match):
        print "Error: There are _ps halos all the all progenitor list who do not have a descendant in the _ts halos list"
        print subhalo_props["descendant_index_all_progen"]
        print subhalo_props["node_index"]
        print removed_props_ts["node_index"]
        quit()

    ptr2 = ms.match(np.append(gas_ps_subhalo_index,star_ps_subhalo_index) , subhalo_props["subhalo_index_all_progen"])
    ok_match2 = ptr2>=0

    if len(ok_match2[ok_match2]) < len(ok_match2) and retain_ts == False:

        print "Error: there are particles in gas_ps that are not bound on _ts to a subhalo in the halos list"
        print np.append(gas_ps.data["group_number"],star_ps.data["group_number"])[ok_match2==False]
        print np.append(gas_ps.data["group_number"],star_ps.data["group_number"])[ok_match2]
        print np.unique(subhalo_props["group_number_all_progen"])
        print np.unique(subhalo_props["group_number_progen"])
        quit()

    # This is where we define the distinction between smooth accretion and merger accretion of gas/dm
    # Note this is going to exclude central progenitors below the mass limit, but not low-mass satellites of progenitors of this halo (inc the main progenitor)

    ind_temp = subhalo_props["group_number_all_progen"]*1e6 + subhalo_props["subgroup_number_all_progen"]
    ind_temp2 = subhalo_props["group_number_progen"]*1e6 + subhalo_props["subgroup_number_progen"]
    ptr_temp2 = ms.match(ind_temp, ind_temp2)
    main_progens = ptr_temp2>=0
    
    if mass_cuts == "absolute":
        smooth_check = (subhalo_props["m200_host_all_progen"] > smooth_acc_thr) | main_progens
    elif mass_cuts == "fraction":
        ptr_temp = ms.match(subhalo_props["descendant_index_all_progen"],subhalo_props["node_index"])
        ok_match_temp = ptr_temp>=0
        m200_host_all_progen_descendant = np.zeros_like(subhalo_props["m200_host_all_progen"])
        m200_host_all_progen_descendant[ok_match_temp] = subhalo_props["m200_host"][ptr_temp][ok_match_temp]
        smooth_check = (subhalo_props["m200_host_all_progen"] > mfrac_cut * m200_host_all_progen_descendant) | main_progens
    else:
        print "Error: mass_cuts",mass_cuts
        quit()

    star_gas_all_progen_sorted = mpc.DataSet()
    star_gas_all_progen_sorted.add_data(np.append(subhalo_props["group_number"],removed_props_ts["group_number"])[ptr][ptr2][ok_match2],"group_number_descendant")
    star_gas_all_progen_sorted.add_data(np.append(subhalo_props["subgroup_number"],removed_props_ts["subgroup_number"])[ptr][ptr2][ok_match2],"subgroup_number_descendant")
    star_gas_all_progen_sorted.add_data(np.append(gas_ps.data["id"],star_ps.data["id"])[ok_match2], "id")
    star_gas_all_progen_sorted.add_data(np.append(gas_ps.data["group_number"],star_ps.data["group_number"])[ok_match2], "group_number")
    star_gas_all_progen_sorted.add_data(np.append(gas_ps.data["coordinates"],star_ps.data["coordinates"],axis=0)[ok_match2], "coordinates")
    star_gas_all_progen_sorted.add_data(np.append(gas_ps.data["temperature"],np.zeros_like(star_ps.data["mass"]))[ok_match2], "temperature")
    star_gas_all_progen_sorted.add_data(np.append(gas_ps.data["metallicity"],np.zeros_like(star_ps.data["mass"]))[ok_match2], "metallicity")
    star_gas_all_progen_sorted.add_data(np.append(gas_ps.data["density"],np.zeros_like(star_ps.data["mass"]))[ok_match2], "density")

    star_gas_all_progen_sorted.select_bool(smooth_check[ptr2][ok_match2]) # Note to self, this implementation means that gas_ps will contain some particles that are not in star_gas_all_progen - shouldn't be worth worrying about though

    star_gas_all_progen_sorted.sort("group_number_descendant")

    star_gas_all_progen_sorted.grn_unique, star_gas_all_progen_sorted.grn_index = np.unique(star_gas_all_progen_sorted.data["group_number_descendant"], return_index=True)
    star_gas_all_progen_sorted.grn_index = np.append(star_gas_all_progen_sorted.grn_index,len(star_gas_all_progen_sorted.data["group_number_descendant"]))

    dm_all_progen_sorted = mpc.DataSet()
    if not skip_dark_matter:
        # Create the sorted (and so indexable) all progen list for dark matter (to compute smooth dm accretion rate)
        ptr2 = ms.match(dm_ps_subhalo_index, subhalo_props["subhalo_index_all_progen"])
        ok_match2 = ptr2>=0
        if len(ok_match2[ok_match2]) < len(ok_match2) and retain_ts ==False:
            print "Error: there are particles in dm_ps that are not bound on _ts to a subhalo in the halos list"
            quit()

        dm_all_progen_sorted.add_data(np.append(subhalo_props["group_number"],removed_props_ts["group_number"])[ptr][ptr2][ok_match2],"group_number_descendant")
        dm_all_progen_sorted.add_data(np.append(subhalo_props["mchalo"],removed_props_ts["mchalo"])[ptr][ptr2][ok_match2],"mchalo_descendant")
        dm_all_progen_sorted.add_data(dm_ps.data["id"][ok_match2], "id")

        dm_all_progen_sorted.select_bool(smooth_check[ptr2][ok_match2])

        dm_all_progen_sorted.sort("group_number_descendant")
        dm_all_progen_sorted.grn_unique, dm_all_progen_sorted.grn_index = np.unique(dm_all_progen_sorted.data["group_number_descendant"], return_index=True)
        dm_all_progen_sorted.grn_index = np.append(dm_all_progen_sorted.grn_index,len(dm_all_progen_sorted.data["id"]))

    if write_SNe_energy_fraction and extra_wind_measurements:
        bh_all_progen_sorted = mpc.DataSet()
        ptr2 = ms.match(bh_ps_subhalo_index, subhalo_props["subhalo_index_all_progen"])
        ok_match2 = ptr2>=0
        bh_all_progen_sorted.add_data(np.append(subhalo_props["group_number"],removed_props_ts["group_number"])[ptr][ptr2][ok_match2],"group_number_descendant")
        bh_all_progen_sorted.add_data(np.append(subhalo_props["mchalo"],removed_props_ts["mchalo"])[ptr][ptr2][ok_match2],"mchalo_descendant")
        bh_all_progen_sorted.add_data(bh_ps.data["id"][ok_match2], "id")
        bh_all_progen_sorted.add_data(bh_ps.data["mass"][ok_match2], "mass")
        bh_all_progen_sorted.add_data(bh_ps.data["nseed"][ok_match2], "nseed")

        bh_all_progen_sorted.select_bool(smooth_check[ptr2][ok_match2])

        bh_all_progen_sorted.sort("group_number_descendant")
        bh_all_progen_sorted.grn_unique, bh_all_progen_sorted.grn_index = np.unique(bh_all_progen_sorted.data["group_number_descendant"], return_index=True)
        bh_all_progen_sorted.grn_index = np.append(bh_all_progen_sorted.grn_index,len(bh_all_progen_sorted.data["id"]))
    else:
        bh_all_progen_sorted = []

    ###### Now do the list of particles of all particles bound to the descendants of each _ts FoF group ############

    gas_ns_all_fof_sorted = mpc.DataSet()
    gas_ns_all_fof_sorted.add_data(gas_ns.data["group_number"],"group_number")
    gas_ns_all_fof_sorted.add_data(gas_ns.data["id"],"id")
    gas_ns_all_fof_sorted.add_data(gas_ns.data["metallicity"],"metallicity")
    gas_ns_all_fof_sorted.add_data(gas_ns.data["density"],"density")
    gas_ns_all_fof_sorted.add_data(gas_ns.data["coordinates"],"coordinates")
    gas_ns_all_fof_sorted.add_data(gas_ns.data["temperature"],"temperature")

    gas_ns_all_fof_sorted.sort("group_number")
    gas_ns_all_fof_sorted.grn_unique, gas_ns_all_fof_sorted.grn_index = np.unique(gas_ns_all_fof_sorted.data["group_number"], return_index=True)
    gas_ns_all_fof_sorted.grn_index = np.append(gas_ns_all_fof_sorted.grn_index,len(gas_ns_all_fof_sorted.data["group_number"]))

    star_ns_all_fof_sorted = mpc.DataSet()
    star_ns_all_fof_sorted.add_data(star_ns.data["group_number"],"group_number")
    star_ns_all_fof_sorted.add_data(star_ns.data["id"],"id")

    star_ns_all_fof_sorted.sort("group_number")
    star_ns_all_fof_sorted.grn_unique, star_ns_all_fof_sorted.grn_index = np.unique(star_ns_all_fof_sorted.data["group_number"], return_index=True)
    star_ns_all_fof_sorted.grn_index = np.append(star_ns_all_fof_sorted.grn_index,len(star_ns_all_fof_sorted.data["group_number"]))

    print "FoF lists computed, took", time.time() - time_fof, "seconds"

    ########### Associate master list information with _ts data ###########
    # So that we don't have to do cross-referencing within the main loop

    if len(id_ejecta_master) > 0:
        order_ejecta_master = np.argsort(id_ejecta_master) # Do this here to avoid duplicating the operation
        ptr = ms.match(gas_ts.data["id"], id_ejecta_master,arr2_index=order_ejecta_master)
        ok_match = ptr >= 0

        gas_ts.add_data(ok_match, "in_master_ejecta")
        if extra_accretion_measurements:
            gas_ts.add_data(np.full_like(gas_ts.data["mass"],np.nan), "mchalo_ej_master_ejecta")
            gas_ts.data["mchalo_ej_master_ejecta"][ok_match] = mchalo_ej_ejecta_master[ptr][ok_match]

        ptr = ms.match(star_ts.data["id"], id_ejecta_master, arr2_index=order_ejecta_master)
        ok_match = ptr >= 0
        
        star_ts.add_data(ok_match, "in_master_ejecta")

    else:
        gas_ts.add_data(np.zeros_like(gas_ts.data["id"]) < 0, "in_master_ejecta")
        if extra_accretion_measurements:
            gas_ts.add_data(np.full_like(gas_ts.data["mass"],np.nan), "mchalo_ej_master_ejecta")
        star_ts.add_data(np.zeros_like(star_ts.data["id"])<0, "in_master_ejecta")

    if len(id_wind_master) > 0:
        order_wind_master = np.argsort(id_wind_master)
        ptr = ms.match(gas_ts.data["id"], id_wind_master, arr2_index=order_wind_master)
        ok_match = ptr >= 0
        
        gas_ts.add_data(ok_match, "in_master_wind")
        
        gas_ts.add_data(np.zeros_like(gas_ts.data["id"]), "from_SF_master_wind")
        gas_ts.data["from_SF_master_wind"][ok_match] = from_SF_wind_master[ptr][ok_match]

        if extra_accretion_measurements:
            gas_ts.add_data(np.full_like(gas_ts.data["mass"],np.nan), "mchalo_ej_master_wind")
            gas_ts.data["mchalo_ej_master_wind"][ok_match] = mchalo_ej_wind_master[ptr][ok_match]

        ptr = ms.match(star_ts.data["id"], id_wind_master, arr2_index=order_wind_master)
        ok_match = ptr >= 0

        star_ts.add_data(ok_match, "in_master_wind")
        star_ts.add_data(np.zeros_like(star_ts.data["id"]), "from_SF_master_wind")
        star_ts.data["from_SF_master_wind"][ok_match] = from_SF_wind_master[ptr][ok_match]

        if extra_accretion_measurements:
            star_ts.add_data(np.full_like(star_ts.data["mass"], np.nan), "mchalo_ej_master_wind")
            star_ts.data["mchalo_ej_wind"][ok_match] = mchalo_ej_wind_master[ptr][ok_match]

    else:
        gas_ts.add_data(np.zeros_like(gas_ts.data["id"]) < 0, "in_master_wind")
        star_ts.add_data(np.zeros_like(star_ts.data["id"]) < 0, "in_master_wind")
        gas_ts.add_data(np.zeros_like(gas_ts.data["id"]), "from_SF_master_wind")
        star_ts.add_data(np.zeros_like(star_ts.data["id"]), "from_SF_master_wind")
        if extra_accretion_measurements:
            gas_ts.add_data(np.full_like(gas_ts.data["mass"],np.nan), "mchalo_ej_master_wind")
            star_ts.add_data(np.full_like(star_ts.data["mass"],np.nan), "mchalo_ej_master_wind")

    if len(gas_ejecta_ts.data["id"]) > 0 and len(id_wind_master)>0:
        ptr = ms.match(gas_ejecta_ts.data["id"], id_wind_master, arr2_index=order_wind_master)
        ok_match = ptr >= 0
        gas_ejecta_ts.add_data(ok_match, "in_master_wind")
    else:
        gas_ejecta_ts.add_data(np.zeros_like(gas_ejecta_ts.data["id"])<0, "in_master_wind")


    # Pack particle data to pass into function
    part_data = [gas_ps, gas_ts, gas_ns, star_ps, star_ts, star_ns, dm_ps, dm_ts, dm_ns, bh_ps, bh_ts, bh_ns, gas_unbound_ts,part_unbound_ns, gas_halo_reheat_ps, gas_halo_reheat_ts, gas_halo_reheat_ns, gas_ejecta_ts]
    if extra_accretion_measurements:
        if not bound_only_mode:
            part_data += [gas_wind_ts]
    part_data_fof = [star_gas_grn_sorted, star_gas_all_progen_sorted, dm_all_progen_sorted, gas_ns_all_fof_sorted, star_ns_all_fof_sorted, bh_grn_sorted,bh_all_progen_sorted]

    # Pack simulation info
    sim_props = [sim_props_ps, sim_props_ts, sim_props_ns, boxsize, tret_bins_ts]

    # Pack boolean options
    modes = [debug, ih_choose, write_SNe_energy_fraction, ism_definition, skip_dark_matter, optimise_sats, TLo_cut, extra_wind_measurements, extra_accretion_measurements, test_mode, bound_only_mode]

    # Pack up output variable names information
    output_names = [output_mass_names, output_emass_names, output_outflow_names, output_t_names, output_shell_names]

    #################### Main loop ####################################################
    time_track_select = 0
    time_track_ptr = 0
    time_ptr_actual = 0

    time_pack = time.time()

    if verbose:
        print "time spent packing the data", time.time()-time_pack
    time_main = time.time()

    def Dummy_function(i_halo):

        wind_data_ts_i = {}
        for name in wind_names:
            wind_data_ts_i[name] = wind_data_ts[name][i_halo]
        ejecta_data_ts_i = {}
        for name in ejecta_names:
            ejecta_data_ts_i[name] = ejecta_data_ts[name][i_halo]
        hreheat_data_ts_i = {}
        for name in hreheat_names:
            hreheat_data_ts_i[name] = hreheat_data_ts[name][i_halo]
        ireheat_data_ts_i = {}
        for name in ireheat_names:
            ireheat_data_ts_i[name] = ireheat_data_ts[name][i_halo]
        nsfism_data_ts_i = {}
        for name in nsfism_names:
            nsfism_data_ts_i[name] = nsfism_data_ts[name][i_halo]
        ism_data_ts_i = {}
        for name in ism_names:
            ism_data_ts_i[name] = ism_data_ts[name][i_halo]
        cumul_data_ts_i = {}
        for name in cumul_names:
            cumul_data_ts_i[name] = cumul_data_ts[name][i_halo]

        track_lists = [wind_data_ts_i, ireheat_data_ts_i, ejecta_data_ts_i, hreheat_data_ts_i, nsfism_data_ts_i, ism_data_ts_i, cumul_data_ts_i, descendant_index_list_ps, ejecta_data_ps["id"], ejecta_data_ps["from_mp"]]
        inputs = [ i_halo, i_snip_glob, subhalo_props, part_data, part_data_fof, track_lists, sim_props, modes, output_names, fVmax_info]

        return main.Subhalo_Loop(inputs)

    output = Parallel(n_jobs=n_jobs_parallel)(delayed(Dummy_function)(i_halo) for i_halo in range(len(subhalo_props["group_number"])))
    
    if verbose:
        print "time spent on main loop", time.time() -time_main
    time_unpack = time.time()

    # Unpack the data
    time_track_subloop = 0.0
    for i_halo in range(len(subhalo_props["group_number"])):

        halo_mass_dict, halo_emass_dict, halo_outflow_dict, halo_t_dict, halo_shell_dict, track_lists, timing_info = output[i_halo]
        
        time_track_select += timing_info[0]
        time_track_ptr += timing_info[1]
        time_track_subloop += timing_info[2]
        time_ptr_actual += timing_info[3]

        wind_ts, ism_reheat_ts, ejecta_ts, halo_reheat_ts, nsfism_ts, ism_ts, wind_cumul_ts = track_lists
        for name in wind_names:
            wind_data_ts[name][i_halo] = wind_ts[name]
        for name in ireheat_names:
            ireheat_data_ts[name][i_halo] = ism_reheat_ts[name]
        for name in nsfism_names:
            nsfism_data_ts[name][i_halo] = nsfism_ts[name]
        for name in ism_names:
            ism_data_ts[name][i_halo] = ism_ts[name]
        for name in hreheat_names:
            hreheat_data_ts[name][i_halo] = halo_reheat_ts[name]
        for name in ejecta_names:
            ejecta_data_ts[name][i_halo] = ejecta_ts[name]
        for name in cumul_names:
            cumul_data_ts[name][i_halo] = wind_cumul_ts[name]

        halo_outflow_dict["mass_ism_wind_cumul"] = cumul_data_ts["mass_ism_wind"][i_halo]
        halo_outflow_dict["n_ism_wind_cumul"] = cumul_data_ts["n_ism_wind"][i_halo]
        halo_outflow_dict["mass_ism_to_halo_wind_first_time_cumul"] = cumul_data_ts["mass_ism_to_halo_wind_first_time"][i_halo]
        halo_outflow_dict["n_ism_to_halo_wind_first_time_cumul"] = cumul_data_ts["n_ism_to_halo_wind_first_time"][i_halo]

        for name in output_mass_names:
            output_mass_dict[name][i_halo] += halo_mass_dict[name]
        for name in output_emass_names:
            output_emass_dict[name][i_halo] += halo_emass_dict[name]
        for name in output_outflow_names:
            output_outflow_dict[name][i_halo] += halo_outflow_dict[name]
        for name in output_t_names:
            output_t_dict[name][i_halo] += halo_t_dict[name]

        for name in output_shell_names:
            output_shell_dict[name][i_halo] += halo_shell_dict[name]

    print "time spent unpacking the data", time.time() - time_unpack

    ih_choose = np.argmax(mchalo_list)

    # Update tracked ejecta lists for next step in the snipshot loop
    ejecta_data_ps = dict(ejecta_data_ts)
    hreheat_data_ps = dict(hreheat_data_ts)
    ireheat_data_ps = dict(ireheat_data_ts)
    nsfism_data_ps = dict(nsfism_data_ts)
    ism_data_ps = dict(ism_data_ts)
    wind_data_ps = dict(wind_data_ts)
    cumul_data_ps = dict(cumul_data_ts)

    descendant_index_list_ps = subhalo_props["descendant_index"]
    grn_list_ps = subhalo_props["group_number"]
    sgrn_list_ps = subhalo_props["subgroup_number"]

    ############### Write restarts to disk if requested ############################

    if use_restart and restart_counter >= restart_interval:
        restart_counter = 0
        write_restart = True
    else:
        write_restart = False

    if use_restart and write_restart:
        restart_path = output_path_ts + "restart_file_"+sim_name+"_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"

        # Check if the restart file already exists, and delete if that is the case
        if os.path.isfile(restart_path):
            os.remove(restart_path)
            print "Overwriting existing restart file at", restart_path
        else:
            print "Writing restart file to", restart_path

        restart_file = h5py.File(restart_path)
        
        restart_file.create_dataset("descendent_index_list", data=descendant_index_list_ps)
        restart_file.create_dataset("grn_list_ps", data=grn_list_ps)
        restart_file.create_dataset("sgrn_list_ps", data=sgrn_list_ps)
        
        for name, name2 in zip(cumul_names, cumul_out_names):
            restart_file.create_dataset(name2, data=cumul_data_ps[name])


        nejecta_list_ps = np.zeros_like(descendant_index_list_ps)
        for isub in range(len(ejecta_data_ps["id"])):
            nejecta_list_ps[isub] = len(ejecta_data_ps["id"][isub])
        restart_file.create_dataset("nejecta_list_ps",data=nejecta_list_ps)

        for name in ejecta_names:
            restart_file.create_dataset(name+"_gas_ejecta_list_ps",data=np.concatenate(ejecta_data_ps[name]))

        nhalo_reheat_list_ps = np.zeros_like(descendant_index_list_ps)
        for isub in range(len(hreheat_data_ps["id"])):
            nhalo_reheat_list_ps[isub] = len(hreheat_data_ps["id"][isub])
        restart_file.create_dataset("nhalo_reheat_list_ps",data=nhalo_reheat_list_ps)

        for name in hreheat_names:
            restart_file.create_dataset(name+"_gas_halo_reheat_list_ps",data=np.concatenate(hreheat_data_ps[name]))

        nism_reheat_list_ps = np.zeros_like(descendant_index_list_ps)
        for isub in range(len(ireheat_data_ps["id"])):
            nism_reheat_list_ps[isub] = len(ireheat_data_ps["id"][isub])
        restart_file.create_dataset("nism_reheat_list_ps",data=nism_reheat_list_ps)

        for name in ireheat_names:
            restart_file.create_dataset(name+"_gas_ism_reheat_list_ps",data=np.concatenate(ireheat_data_ps[name]))

        nnsfism_list_ps = np.zeros_like(descendant_index_list_ps)
        for isub in range(len(nsfism_data_ps["id"])):
            nnsfism_list_ps[isub] = len(nsfism_data_ps["id"][isub])
        restart_file.create_dataset("nnsfism_list_ps",data=nnsfism_list_ps)

        for name in nsfism_names:
            restart_file.create_dataset(name+"_gas_nsfism_list_ps",data=np.concatenate(nsfism_data_ps[name]))

        nism_list_ps = np.zeros_like(descendant_index_list_ps)
        for isub in range(len(ism_data_ps["id"])):
            nism_list_ps[isub] = len(ism_data_ps["id"][isub])
        restart_file.create_dataset("nism_list_ps",data=nism_list_ps)

        for name in ism_names:
            restart_file.create_dataset(name+"_gas_ism_list_ps",data=np.concatenate(ism_data_ps[name]))
        
        nwind_list_ps = np.zeros_like(descendant_index_list_ps)
        for isub in range(len(wind_data_ps["id"])):
            nwind_list_ps[isub] = len(wind_data_ps["id"][isub])
        restart_file.create_dataset("nwind_list_ps",data=nwind_list_ps)

        for name in wind_names:
            restart_file.create_dataset(name+"_gas_wind_list_ps",data=np.concatenate(wind_data_ps[name]))

        restart_file.close()

        # If enabled, copy this restart file to another path so that it is stored permanently
        if save_track_lists and snap_ts in snap_save:
            saved_restart_path = output_path_ts + "saved_restart_file_"+sim_name+"_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
            print "Saving a permanent copy of the restart file for this snapshot (this overwrites any previous saved file)"
            shutil.copy(restart_path, saved_restart_path)
        
    # Delete previous restart file after writing this one (or delete restart at the end)
    if use_restart:
        if write_restart or snip_ts == snip_list[:-1][-1]:

            if snip_ts == snip_list[:-1][-1]:
                snips_prev = snip_list[snip_list <= snip_ts][::-1]
            else:
                snips_prev = snip_list[snip_list < snip_ts][::-1]
            
            for snip_prev in snips_prev:
                output_path_prev = output_path + "Snip_"+str(snip_prev)+"/"
                restart_path_prev = output_path_prev + "restart_file_"+sim_name+"_snip_"+str(snip_prev)+"_"+str(subvol)+".hdf5"

                if os.path.isfile(restart_path_prev):
                    print "removing previous restart at", restart_path_prev
                    os.remove(restart_path_prev)

        restart_counter += 1

    ################### Write output data to disk #############################################
    
    # With new trimmed output format, we need to decide which subhalo properties to output
    subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host"]
    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host", "isInterpolated"]

    output_group_str = "Measurements"
    if output_group_str not in File:
        output_group = File.create_group(output_group_str)
    else:
        output_group = File[output_group_str]

    for dset in output_mass_names+output_outflow_names+output_t_names+output_emass_names+output_shell_names+["tret_bins","node_index_progen","group_number_progen","subgroup_number_progen"]+subhalo_prop_names:
        for thing in output_group:
            if thing == dset:
                del output_group[dset]

    for name in subhalo_prop_names:
        output_group.create_dataset(name, data=subhalo_props[name])

    output_group.create_dataset("node_index_progen", data=subhalo_props["node_index_progen"])
    output_group.create_dataset("group_number_progen", data=subhalo_props["group_number_progen"])
    output_group.create_dataset("subgroup_number_progen", data=subhalo_props["subgroup_number_progen"])

    for name in output_mass_names:
        output_group.create_dataset(name, data=output_mass_dict[name])

    for name in output_emass_names:
        output_group.create_dataset(name, data=output_emass_dict[name])

    for name in output_outflow_names:
        output_group.create_dataset(name, data=output_outflow_dict[name])

    for name in output_shell_names:

        if name == "shell_rmid":
            output_group.create_dataset(name, data=output_shell_dict[name][0])
        else:
            output_group.create_dataset(name, data=output_shell_dict[name])

    for name in output_t_names:
        output_group.create_dataset(name, data=output_t_dict[name])    

    output_group.create_dataset("tret_bins",data=tret_bins_ts)

    File.close()

    print "computing / write time", time.time()-t4
    print "select,matching block,ptr (actual), subloop time", time_track_select, time_track_ptr, time_ptr_actual, time_track_subloop
    print "total time for this snapshot", time.time() - t1

    # If _ns will be _ts on the next step, we have the option to retain the particle data
    if d_snap_ns == 1 and retain_memory:
        retain_ns = True
    else:
        retain_ns = False

    if retain_memory:
        retain_ts = True

print "Total time for the program in minutes", (time.time() - t0)/60.0
