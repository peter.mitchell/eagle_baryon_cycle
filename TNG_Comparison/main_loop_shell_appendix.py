import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, sim_props, output_names = inputs

    time_track_select = 0.0
    time_track_ptr = 0.0


    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_outflow_dict = {}; halo_mass_dict = {}

    output_mass_names, output_outflow_names = output_names
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0
    for name in output_outflow_names:
        halo_outflow_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ts, boxsize = sim_props
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    
    # Unpack particle data
    gas_ts, star_ts = part_data

    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = np.copy(subhalo_props["r200_host"][i_halo])*1e3/(1+redshift_ts)/h # pkpc 
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    

    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ###### Select central gas particles
    select_gas = mpf.Match_Index(halo_subhalo_index, gas_ts.subhalo_index_unique, gas_ts.particle_subhalo_index)

    # Convert to Msun
    g2Msun = 1./(1.989e33)
    mass_gas = np.copy(mpf.Index_Array(gas_ts.data["mass"],select_gas))
    mass_gas *= 1.989e43 * g2Msun / h

    coordinates_gas = mpf.Index_Array(gas_ts.data["coordinates"],select_gas)

    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_halo = [halo_x, halo_y, halo_z]
    coordinates_gas = mpf.Centre_Halo(coordinates_gas, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
    # Put velocity into proper coordinates (kms)
    velocity_gas = np.copy(mpf.Index_Array(gas_ts.data["velocity"],select_gas)) * expansion_factor_ts**0.5
    # change velocities to halo frame
    velocity_gas += - np.array([halo_vx, halo_vy, halo_vz]) # note halo velocities were already in proper peculiar velocity units

    r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
    r_gas_norm = np.swapaxes(np.array([coordinates_gas[:,0],coordinates_gas[:,1],coordinates_gas[:,2]]) / r_gas,0,1)
    cv_gas = np.sum(r_gas_norm * velocity_gas,axis=1)

    ##### Select star particles that belong to desired group, subgroup
    select_star = mpf.Match_Index(halo_subhalo_index, star_ts.subhalo_index_unique, star_ts.particle_subhalo_index)

    # Convert to Msun
    mass_star = np.copy(mpf.Index_Array(star_ts.data["mass"],select_star))
    mass_star *= 1.989e43 * g2Msun / h

    mass_star_init = np.copy(mpf.Index_Array(star_ts.data["mass_init"],select_star))
    mass_star_init *= 1.989e43 * g2Msun / h

    coordinates_star = mpf.Index_Array(star_ts.data["coordinates"],select_star)
    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_star = mpf.Centre_Halo(coordinates_star, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))

    # Work out which star particles formed between now and the previous timestep
    aform_star = mpf.Index_Array(star_ts.data["aform"],select_star)
    
    t_star, junk = uc.t_Universe(aform_star, omm, h)
    age_star = (t_ts - t_star)*1e3 # Myr

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ################ Do measurements ####################

    halo_mass_dict["mass_stars_30kpc"] = np.sum(mass_star[r_star<30])
    halo_mass_dict["mass_new_stars_init_100_30kpc"] = np.sum(mass_star_init[(r_star<30)&(age_star<100)])
    
    r_shell_list = [10,30,50]
    cv_min_list = [0,50,150,250]
    shell_width = 5. # pkpc
    kpc2km = 3.0857 * 10**16
    s2yr = 31557600.0 

    for r_shell in r_shell_list:
        for cv_min in cv_min_list:
            ok = (r_gas<r_shell+0.5*shell_width) & (r_gas>r_shell-0.5*shell_width) & (cv_gas>cv_min)
            halo_outflow_dict["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)] = np.sum(mass_gas[ok] *cv_gas[ok] ) / shell_width / kpc2km * s2yr # Mstar km yr^-1 km^-1

    for cv_min in cv_min_list:
        ok = (r_gas<halo_r200_host) & (r_gas>halo_r200_host-shell_width) & (cv_gas>cv_min)
        halo_outflow_dict["dmdt_out_rvir_cvmin_"+str(cv_min)] = np.sum(mass_gas[ok] *cv_gas[ok] ) / shell_width / kpc2km * s2yr # Mstar km yr^-1 km^-1

    for cv_min in cv_min_list:
        ok = (r_gas<0.3*halo_r200_host) & (r_gas>0.2*halo_r200_host) & (cv_gas>cv_min)
        halo_outflow_dict["dmdt_out_0p25rvir_cvmin_"+str(cv_min)] = np.sum(mass_gas[ok] *cv_gas[ok])  / (0.1*halo_r200_host) / kpc2km * s2yr # Mstar km yr^-1 km^-1

        ok = (r_gas<0.55*halo_r200_host) & (r_gas>0.45*halo_r200_host) & (cv_gas>cv_min)
        halo_outflow_dict["dmdt_out_0p5rvir_cvmin_"+str(cv_min)] = np.sum(mass_gas[ok] *cv_gas[ok])  / (0.1*halo_r200_host) / kpc2km * s2yr # Mstar km yr^-1 km^-1

        ok = (r_gas<0.8*halo_r200_host) & (r_gas>0.7*halo_r200_host) & (cv_gas>cv_min)
        halo_outflow_dict["dmdt_out_0p75rvir_cvmin_"+str(cv_min)] = np.sum(mass_gas[ok] *cv_gas[ok])  / (0.1*halo_r200_host) / kpc2km * s2yr # Mstar km yr^-1 km^-1

    rlo = [0.2,    0.45, 0.7]
    rhi = [0.3,    0.55, 0.8]
    rname=["0p25","0p5","0p75"]
    vmin =[0.125, 0.25, 0.5, 1.0]
    vmin_str = ["0p125","0p25","0p5",""]

    for i_r in range(len(rlo)):
        for vmin_i, vs_i in zip(vmin,vmin_str):
            ok = (r_gas<rhi[i_r]*halo_r200_host) & (r_gas>rlo[i_r]*halo_r200_host) & (cv_gas>vmin_i * halo_vmax)
            halo_outflow_dict["dmdt_out_"+rname[i_r]+"rvir_"+vs_i+"vmax"] = np.sum(mass_gas[ok] *cv_gas[ok])  / (0.1*halo_r200_host) / kpc2km * s2yr # Mstar km yr^-1 km^-1

    for vmin_i,vs_i in zip(vmin, vmin_str):
        ok = (r_gas<halo_r200_host) & (r_gas>halo_r200_host-shell_width) & (cv_gas>vmin_i * halo_vmax)
        halo_outflow_dict["dmdt_out_rvir_"+vs_i+"vmax"] = np.sum(mass_gas[ok] *cv_gas[ok])  / shell_width / kpc2km * s2yr # Mstar km yr^-1 km^-1

    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop]

    return halo_mass_dict, halo_outflow_dict, timing_info
