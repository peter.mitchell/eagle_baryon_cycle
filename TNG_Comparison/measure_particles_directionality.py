import time
t1 = time.time()
t0 = t1

extra_stuff = True
inflows = True

n_jobs_parallel=1 # Number of sub-processes to spawn for executing the main measurement loop

verbose = True
debug = False

# If true, find redefine the central subhalo as being all particles within r200_crit (that are not subfind bound to other satellites - and I think also not part of other FoF groups)
central_r200 = True

# If true, cut all halos below this halo mass in the analysis
cut_m200 = True
m200_cut = 10.**11.75# Msun

cut_sats = True

subsample = False
n_per_bin = 1000

# Maximum subvolume size (at a given snapshot) before splitting the subvolume into 8 smaller subvolumes when performing IO and doing a first round of particle selection
volmax = (0.6777 * 25 * 1e5)**3 # Mpc h^-1
# Edit: 17/7/18 - tweaked 1.25 to 1.1 - this is for subvolumes 14/15 in the 100 Mpc run
# Edit: 11/1/19 - tweaked to 1.5 - this is to account for switch from cosma4 to cosma5 which has doubled memory available
# Edit: 30/1/19 - tweaked to dummy value - don't think I need to use this option anymore unless forced.
#volmax = (0.6777 * 25 )**3 # Mpc h^-1

import sys
import argparse
import sys
sys.path.append("../.")
import measure_particles_functions as mpf
import main_loop_directionality as main
from joblib import Parallel, delayed

parser = argparse.ArgumentParser()
parser.add_argument("-subvol", help = "subvolume",type=int)
parser.add_argument("-nsub1d", help = "number of subvolumes",type=int)
parser.add_argument("-datapath", help = "directory containting simulation data")
parser.add_argument("-simname", help = "sim/tree name")
parser.add_argument("-snap", help = "final snapshot to measure for",type=int)
parser.add_argument("-test_mode", type=mpf.str2bool)

if len(sys.argv)!=13:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()
 
subvol = args.subvol
nsub1d = args.nsub1d
DATDIR = args.datapath
sim_name = args.simname
snap_end = args.snap
test_mode = args.test_mode

# If there is one subvolume, it's always better to do custom IO
if nsub1d == 1:
    volmax = 1e5**3

catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print "Running in test mode"
    print ""
    output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_test_mode/"

    skip_start_hack = False
    if skip_start_hack:
        print ""
        print "Warning: hacking test mode to skip starting snapshots"
        print ""

else:
    output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"/"
    if subsample:
        output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
    skip_start_hack = False

import read_eagle as re
import numpy as np
import warnings
np.seterr(all='ignore')
import os
import h5py
from os import listdir
import match_searchsorted as ms
import utilities_cosmology as uc
import measure_particles_io as mpio
import measure_particles_classes as mpc


# First let's work out the relationship between tree snapshots and simulation snapshots/snipshots
cat_path = catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5"

n_tries = 0
success = True
# This can fail if two jobs try to read the catalogue at the same time - get round this by waiting 
while n_tries < 50:
    try:
        cat_file = h5py.File(cat_path)
        tree_snapshot_info = cat_file["tree_snapshot_info"]
        snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
        sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
        cosmic_time = tree_snapshot_info["cosmic_times_tree"][:]
        redshifts_tree = tree_snapshot_info["redshifts_tree"][:]
        cat_file.close()
        success = True
        break
    except:
        print "Failed to read base catalogue, trying again in 10 seconds", cat_path
        n_tries += 1
        time.sleep(10)

if not success:
    print "Error: failed to read base catalogue, stopping here"
    quit()


# Bad convention to refer to simulation snipshots/snapshots as "snipshots" from here on in
# And to refer to tree outputs as "snapshots" from here on in
ok = snapshot_numbers <= snap_end
snap_list = snapshot_numbers[ok][::-1]
snip_list = sn_i_a_pshots_simulation[ok][::-1]
cosmic_t_list = cosmic_time[ok][::-1]
redshifts_tree = redshifts_tree[ok][::-1]

def UnConcatenate(arrays,nparts):
    
    list_out = []
    nparts_cumul = np.cumsum(nparts)
    for i_sub in range(len(nparts)):
        if i_sub==0:
            ind_prev = 0
        else:
            ind_prev = nparts_cumul[i_sub-1]
        list_out.append(arrays[ind_prev:nparts_cumul[i_sub]])

    return list_out

print "TNG hack, only analysing z=2 snapshot"
z_target = 2.0
#print "Muratov hack, only analysing z=0.25 snapshot"
#z_target = 0.25
ind_choose = np.argmin(abs(z_target-redshifts_tree))

if inflows:
    snip_restart = snip_list[ind_choose]
    ind_choose += 1

snip_list = [snip_list[ind_choose]]
snap_list = [snap_list[ind_choose]]
cosmic_t_list = [cosmic_t_list[ind_choose]]
redshifts_tree = [redshifts_tree[ind_choose]]


# Variable names of the various tracked quantities
ejecta_names = ["id", "metallicity", "mass", "t","from_mp"] # Particles ejected from the FOF group (in the wind)
wind_names = ["id","mass","t","metallicity","from_SF","from_mp","left_halo"] # Particles ejected from the ISM (in the wind)




# In this case we read in a saved restart file so we know which particles belong to what component
if inflows:
    output_path_ts = output_path + "Snip_"+str(snip_restart)+"/"
    restart_path = output_path_ts + "saved_restart_file_"+sim_name+"_snip_"+str(snip_restart)+"_"+str(subvol)+".hdf5"

    if os.path.isfile(restart_path):
        print "Found restart file at snip", snip_restart,"restarting at this point"

        restart_file = h5py.File(restart_path,"r")

        descendant_index_list_ps = restart_file["descendent_index_list"][:]
        grn_list_ps = restart_file["grn_list_ps"][:]
        sgrn_list_ps = restart_file["sgrn_list_ps"][:]

        nejecta_list_ps = restart_file["nejecta_list_ps"][:]
        ejecta_data_ps = {}
        for name in ejecta_names:
            ejecta_data_ps[name] = UnConcatenate(restart_file[name+"_gas_ejecta_list_ps"][:], nejecta_list_ps)

        nwind_list_ps = restart_file["nwind_list_ps"][:]
        wind_data_ps = {}
        for name in wind_names:
            wind_data_ps[name] = UnConcatenate(restart_file[name+"_gas_wind_list_ps"][:], nwind_list_ps)

print "measuring rates for snipshots/snapshots", snip_list

t2 = time.time()
print "finished preliminaries, time elapsed", t2-t1

for i_snip, snip_ts in enumerate(snip_list):

    # For certain applications, we want to know where we are in the snapshot loop without being affected by the restart resets
    try:
        i_snip_glob = np.argmin(abs(snip_ts - snip_list_tot))
    except:
        i_snip_glob = i_snip

    t1 = time.time()
    
    print ""
    print "measuring for snip (simulation), snap (tree), time (Gyr) = ", snip_ts, snap_list[i_snip], cosmic_t_list[i_snip]

    # This is supposed to help with getting python to actually communicate something to the log file
    sys.stdout.flush()



    ######## Work out which snapshot to read as the _ns for wind selection ###############
    cosmic_t_ts = cosmic_t_list[i_snip] # Cosmic time at current 
    snap_ts = snap_list[i_snip] # Current tree snapshot

    ok = np.where(snap_list == snap_ts)[0][0]
    t_remain = cosmic_t_list[ok:]
    dt = t_remain[1:] - t_remain[0]

    # Read subvol file to load halo catalogue and selection boundaries
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    if not os.path.exists(output_path_ts):
        print "Couldn't find directory",output_path_ts,"moving on"
        continue
    
    File = h5py.File(output_path_ts+filename_ts)

    # Read selection boundary
    try:
        xyz_min_ts = File["select_region/xyz_min"][:]
        xyz_max_ts = File["select_region/xyz_max"][:]
    except:
        print output_path_ts+filename_ts, "doesn't exist, moving to next snipshot"
        continue

    # Quit if there are no subhaloes in this subvolume (this assumes that future subvols will also not have subhaloes)
    if np.isnan(xyz_min_ts[0]):
        print "No haloes inside this subvolume, moving to next snipshot"
        continue

    # Want to read in a big enough volume to accomodate both the subhalo & progenitor samples for this snipshot and the subhalo/progenitor samples for the next snipshot
    boxsize = mpio.Get_Box_Size(sim_name, DATDIR,snip_ts)

    # Read halo catalogue
    subhalo_group = File["subhalo_data"]
    
    subhalo_props = {}

    try:
        mchalo_list = subhalo_group["mchalo"][:]
    except:
        print "Error: first run copy_mchalo.py, then rerun select_subvolumes.py"
        quit()

    order_sub = np.argsort(mchalo_list)[::-1]
    mchalo_list = mchalo_list[order_sub]

    subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host","isInterpolated"]
    subhalo_prop_types =  ["float", "float", "int",         "int",            "int",       "int",             "float", "int"]

    subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]
    subhalo_prop_types +=  ["float", "float", "float", "float", "float", "float", "float", "float"]

    for name, prop_type in zip(subhalo_prop_names,subhalo_prop_types):
        if prop_type == "int":
            subhalo_props[name] = np.rint(subhalo_group[name][:][order_sub]).astype("int")
        elif prop_type == "float":
            subhalo_props[name] = subhalo_group[name][:][order_sub]
        else:
            "Error: prop type",prop_type,"not understood"
            quit()

    # Select halos above the mass cut
    if cut_m200:        
        ok = (subhalo_props["m200_host"] > m200_cut) & (subhalo_props["subgroup_number"] == 0)
        
        n_for_limit = max(100,len(subhalo_props["m200_host"]))
        mchalo_lim = np.median(np.sort(subhalo_props["mchalo"][ok])[0:n_for_limit])

        n_sub_before_mask = len(mchalo_list)

        if not cut_sats:
            ok = ok | ((subhalo_props["subgroup_number"]>0) & (subhalo_props["mchalo"] > mchalo_lim))
        subhalo_mask = np.where(ok)[0]

        for name in subhalo_prop_names:
            subhalo_props[name] = subhalo_props[name][subhalo_mask]
        mchalo_list = mchalo_list[subhalo_mask]

        if len(mchalo_list)==0:
            print "After applying subhalo mass cut, there are no remaining subhalos, moving on"
            continue

    if debug:
        ih_choose = np.argmax(mchalo_list)
    else:
        ih_choose = 0 # Dummy value

    # Build a unique index for each subhalo that can be constructed from knowing group_number and subgroup_number
    nsep_ts = max(len(str(int(subhalo_props["subgroup_number"].max()))) +1 ,6)
    subhalo_props["subhalo_index"] = subhalo_props["group_number"].astype("int64") * 10**nsep_ts + subhalo_props["subgroup_number"].astype("int64")

    progenitor_group = File["progenitor_data"]
    
    subhalo_prop_names = ["group_number","subgroup_number","x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]

    for name in subhalo_prop_names:
        name_progen = name+"_progen"
        subhalo_props[name_progen] = progenitor_group[name][:][order_sub]

    if cut_m200:
        for name in subhalo_prop_names:
            subhalo_props[name+"_progen"] = subhalo_props[name+"_progen"][subhalo_mask]

    ############### Initialize output dicts ###########################

    output_mass_names = ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]
    output_mass_dict = {}

    for name in output_mass_names:
        output_mass_dict[name] = np.zeros(len(mchalo_list))

    # Binning for "galacto-centric angle", ala Nelson 19
    output_outflow_names = []
    vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
    rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
    n_rbins = len(rmid)
    n_theta_bins = 72
    for vmin_i in vmin:
        output_outflow_names += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]

    if inflows:
        output_outflow_names += ["dmdt_in","dmdt_in_wind","dmdt_in_0p25vmax","dmdt_in_wind_0p25vmax"]

    output_outflow_dict = {}
    for name in output_outflow_names:
        output_outflow_dict[name] = np.zeros((len(mchalo_list),n_rbins,n_theta_bins))
        
    # Covering fraction quantities (fraction of solid angle at a given radius where the mean flux-weighted velocity exceeds some velocity cut)
    for vmin_i in vmin:
        name = "frac_omega_grt_"+str(vmin_i).replace(".","p")+"vmax"
        output_outflow_names += [name]
        output_outflow_dict[name] = np.zeros((len(mchalo_list),n_rbins))
        if extra_stuff:
            name = "P50fw_grt_"+str(vmin_i).replace(".","p")+"vmax"
            output_outflow_names += [name]
            output_outflow_dict[name] = np.zeros((len(mchalo_list),n_rbins))
        name = "mass_grt_"+str(vmin_i).replace(".","p")+"vmax"
        output_outflow_names += [name]
        output_outflow_dict[name] = np.zeros((len(mchalo_list),n_rbins))
    
    if extra_stuff:
        extra_names = ["dmdr","dpdr","dEkdr","dEtdr", "mshell","agrav","P50mw","Pmean","P50Vw"]
        for vmin_i in vmin:
            extra_names += ["dmdt_out_standard_"+str(vmin_i).replace(".","p")+"vmax", "dpdt_out_"+str(vmin_i).replace(".","p")+"vmax", "dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
            extra_names += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax", "v50fw_out_"+str(vmin_i).replace(".","p")+"vmax", "v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            extra_names += ["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax", "dpdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
            extra_names += ["dEkdr_out_"+str(vmin_i).replace(".","p")+"vmax", "dEtdr_out_"+str(vmin_i).replace(".","p")+"vmax"]
            extra_names += ["P50fw_out_"+str(vmin_i).replace(".","p")+"vmax", "P90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

        for name in extra_names:
            output_outflow_names += [name]
            output_outflow_dict[name] = np.zeros((len(mchalo_list),n_rbins))


    ########## Setup the wind/ejecta lists for _ts #########################

    def Ejecta_setup(i_halo):
        ejecta_data_ts_i = {}
        for name in ejecta_names:
            ejecta_data_ts_i[name] = []

        wind_data_ts_i = {}
        for name in wind_names:
            wind_data_ts_i[name] = []

        halo_node_index = subhalo_props["node_index"][i_halo]
        halo_group_number_progen = subhalo_props["group_number_progen"][i_halo]
        halo_subgroup_number_progen = subhalo_props["subgroup_number_progen"][i_halo]

        # Add ejected particles from each progenitor of this subhalo
        select_progenitors = np.where(halo_node_index == descendant_index_list_ps)[0]
        main_progenitor = (grn_list_ps[select_progenitors] == halo_group_number_progen) & (sgrn_list_ps[select_progenitors] == halo_subgroup_number_progen)

        for i_progen, select_progenitor_i in enumerate(select_progenitors):
            for name in ejecta_names:
                temp = np.array(ejecta_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                ejecta_data_ts_i[name] = np.append(np.array(ejecta_data_ts_i[name]), temp)

            for name in wind_names:
                temp = np.array(wind_data_ps[name][select_progenitor_i])
                if name == "from_mp":
                    temp[main_progenitor[i_progen]==False] = 0
                wind_data_ts_i[name] = np.append(np.array(wind_data_ts_i[name]), temp)
    
            
                # Add ejected particles from pruned satellites if the satellite has > 100 tracked particles
        

        # Check uniqueness of the ejected particle lists
        if len(ejecta_data_ts_i["id"]) != len(np.unique(ejecta_data_ts_i["id"])):
            junk, index = np.unique(ejecta_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(ejecta_names):
                ejecta_data_ts_i[name] = ejecta_data_ts_i[name][index]

        if len(wind_data_ts_i["id"]) != len(np.unique(wind_data_ts_i["id"])):
            junk, index = np.unique(wind_data_ts_i["id"], return_index=True)
            for i_name, name in enumerate(wind_names):
                wind_data_ts_i[name] = wind_data_ts_i[name][index]

        output = [ejecta_data_ts_i, wind_data_ts_i]
        return output

    output = Parallel(n_jobs=n_jobs_parallel)(delayed(Ejecta_setup)(i_halo) for i_halo in range(len(mchalo_list)))

    output = np.array(output)
    n_data = len(output)/len(mchalo_list)

    ejecta_data_ts = {}
    wind_data_ts = {}
    for i_halo in range(len(subhalo_props["group_number"])):

        for name in ejecta_names:
            if i_halo == 0:
                ejecta_data_ts[name] = []
            ejecta_data_ts[name].append(output[i_halo,0][name])
        for name in wind_names:
            if i_halo == 0:
                wind_data_ts[name] = []
            wind_data_ts[name].append(output[i_halo,1][name])

    ############## Read Eagle particle data #######################
    t2 = time.time()
    print "Reading particle data for _ts (current snipshot)"

    gas_names_in_ts = ['Mass','Coordinates', "Velocity","Temperature","Density","Metallicity"]
    gas_names_out_ts = ['mass', 'coordinates', "velocity","temperature","density","metallicity"]

    dm_names_in_ts = []
    dm_names_out_ts = []

    if extra_stuff:
        gas_names_in_ts += ["InternalEnergy"]
        gas_names_out_ts += ["internal_energy"]

        dm_names_in_ts += ["Coordinates"]
        dm_names_out_ts = ["coordinates"]

    star_names_in_ts = ["InitialMass", 'Coordinates',"Mass","StellarFormationTime"]
    star_names_out_ts = ["mass_init",'coordinates',"mass","aform"]

    bh_names_in_ts = []
    bh_names_out_ts = []

    data_names_in_ts = [gas_names_in_ts, dm_names_in_ts, star_names_in_ts, bh_names_in_ts, [],[],[],[]]
    data_names_out_ts = [gas_names_out_ts, dm_names_out_ts, star_names_out_ts, bh_names_out_ts, [],[],[],[]]

    # Decide how many sub-subvolumes we want for doing IO (and some intial particle selection)
    vol_ts = np.product(xyz_max_ts-xyz_min_ts) # Mpc h^-1

    if vol_ts > volmax:
        nsubvol_pts = 8

        xyz_subvol_min = np.zeros((8,3)) + xyz_min_ts

        ind_sub = 0
        for i_s in range(2):
            x_sub = i_s * (xyz_max_ts[0]-xyz_min_ts[0])*0.5
            for j_s in range(2):
                y_sub = j_s * (xyz_max_ts[1]-xyz_min_ts[1])*0.5
                for k_s in range(2):
                    z_sub = k_s * (xyz_max_ts[2]-xyz_min_ts[2])*0.5
                    xyz_subvol_min[ind_sub] += np.array([x_sub, y_sub, z_sub])
                    ind_sub += 1

        xyz_subvol_max = np.copy(xyz_subvol_min)
        xyz_subvol_max[:,0] += (xyz_max_ts[0]-xyz_min_ts[0])*0.5
        xyz_subvol_max[:,1] += (xyz_max_ts[1]-xyz_min_ts[1])*0.5
        xyz_subvol_max[:,2] += (xyz_max_ts[2]-xyz_min_ts[2])*0.5
    else:
        nsubvol_pts = 1

    for i_subvol in range(nsubvol_pts):
        
        if nsubvol_pts > 1:
            print "Reading particle data for sub-subvolume", i_subvol, " of ", nsubvol_pts
            xyz_subvol_min_i = xyz_subvol_min[i_subvol]
            xyz_subvol_max_i = xyz_subvol_max[i_subvol]
        else:
            xyz_subvol_min_i = xyz_min_ts
            xyz_subvol_max_i = xyz_max_ts

        t_core_io_i = time.time()
            
        if nsub1d > 1 or test_mode:
            read_all = ""
            gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_subvol_max_i, xyz_subvol_min_i, sim_name, DATDIR,data_names_in_ts,data_names_out_ts, read_all=read_all,verbose=verbose)
           
        else:
            # Use Custom IO, for efficiently reading in the entire box (assuming you have access to at least njobs worth of cores)
            data_names_in_ts = gas_names_in_ts+ dm_names_in_ts+ star_names_in_ts+ bh_names_in_ts
            data_names_out_ts = gas_names_out_ts+ dm_names_out_ts+ star_names_out_ts+ bh_names_out_ts
            part_types = np.concatenate(( np.zeros(len(gas_names_in_ts)), np.ones(len(dm_names_in_ts)), np.zeros(len(star_names_in_ts))+4, np.zeros(len(bh_names_in_ts))+5 )).astype("int")

            gas_ts_in, dm_ts_in, star_ts_in, bh_ts_in, sim_props_ts = mpio.Read_Particle_Data_JobLib(DATDIR, snip_ts, sim_name, data_names_in_ts,data_names_out_ts,part_types,njobs=n_jobs_parallel,verbose=verbose)

        t_core_io = time.time() - t_core_io_i

        t_rebind_i = time.time()
        if central_r200:
            # Rebinding procedure, where all non-bound particles within r200 (w.r.t crit) of a central are added to that central subhalo
            if verbose:
                print "rebinding _ts"
            halo_coordinates_ts = [subhalo_props["x_halo"],subhalo_props["y_halo"],subhalo_props["z_halo"]]
            
            # gas rebinding
            particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(gas_ts_in, subhalo_props["group_number"], subhalo_props["subgroup_number"], halo_coordinates_ts,subhalo_props["r200_host"], subhalo_props["isInterpolated"], boxsize, verbose=verbose)
            temp = np.arange(0,len(subhalo_props["group_number"]))[subhalo_props["subgroup_number"]==0]
            def JobLib_Rebind_Wrapper(i_subhalo):
                return mpf.Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coordinates_ts, subhalo_props["r200_host"], boxsize)
            output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
            gas_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, gas_ts_in)

            if len(dm_names_in_ts) > 0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(dm_ts_in, subhalo_props["group_number"], subhalo_props["subgroup_number"], halo_coordinates_ts,subhalo_props["r200_host"], subhalo_props["isInterpolated"], boxsize, verbose=verbose)
                temp = np.arange(0,len(subhalo_props["group_number"]))[subhalo_props["subgroup_number"]==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                dm_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, dm_ts_in)


            if len(star_ts_in.data["id"])>0:
                particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order = mpf.Rebind_Subhalos_JobLib_Pt1(star_ts_in, subhalo_props["group_number"], subhalo_props["subgroup_number"], halo_coordinates_ts,subhalo_props["r200_host"], subhalo_props["isInterpolated"], boxsize, verbose=verbose)
                temp = np.arange(0,len(subhalo_props["group_number"]))[subhalo_props["subgroup_number"]==0]
                output = Parallel(n_jobs=n_jobs_parallel)(delayed(JobLib_Rebind_Wrapper)(i_subhalo) for i_subhalo in temp)
                star_ts_in = mpf.Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, star_ts_in)

        t_rebind = time.time() - t_rebind_i

        bound_parts_ts = gas_ts_in.is_bound()
        gas_ts_in.select_bool(bound_parts_ts)

        # De-scope boolean arrays
        bounds_parts_ts = None

        # Remove bound particles from other particle types
        star_ts_in.select_bool(star_ts_in.is_bound())

        if len(dm_names_in_ts) > 0:
            dm_ts_in.select_bool(dm_ts_in.is_bound())

        # Keep only particles bound to subhalos from this subvolume (as opposed to bound to any halo in the simulation)
        gas_ts_in.ptr_prune(subhalo_props["group_number"],"group_number")
        star_ts_in.ptr_prune(subhalo_props["group_number"],"group_number")
        if len(dm_names_in_ts) > 0:
            dm_ts_in.ptr_prune(subhalo_props["group_number"],"group_number")

        # Init datasets
        if i_subvol == 0:
            gas_ts = mpc.DataSet(); star_ts = mpc.DataSet()
            if extra_stuff:
                dm_ts = mpc.DataSet()

        # Merge datasets
        gas_ts.merge_dataset(gas_ts_in)
        star_ts.merge_dataset(star_ts_in)
        if extra_stuff:
            dm_ts.merge_dataset(dm_ts_in)

        # Put the input datasets out of scope
        gas_ts_in = None; star_ts_in = None; dm_ts_in = None


    # Construct master list of ejecta and wind particles to account for inter-galaxy/halo transfer of gas
    id_ejecta_master = np.concatenate(ejecta_data_ts["id"])
    id_wind_master = np.concatenate(wind_data_ts["id"])

    # Only retain those particles that are bound to halos
    if len(id_ejecta_master) > 0 and len(gas_ts.data["id"]) > 0:
        ptr = ms.match(id_ejecta_master, gas_ts.data["id"])
        id_ejecta_master = id_ejecta_master[ptr>=0]
        # Sort for faster indexing when cross-matching later (and remove duplicate entries)
        id_ejecta_master,temp = np.unique(id_ejecta_master, return_index=True)

    if len(id_wind_master) > 0 and len(gas_ts.data["id"]) > 0:
        ptr = ms.match(id_wind_master, gas_ts.data["id"])
        id_wind_master = id_wind_master[ptr>=0]
        id_wind_master, temp = np.unique(id_wind_master, return_index=True)






    print "finished io, time elapsed", time.time() - t2, "time elapsed for core IO", t_core_io, "time for rebinding", t_rebind

    t4 = time.time()

    ####### Unpack and get simulation characteristics for this point of the snipshot loop ########
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    mass_dm = sim_props_ts["mass_dm"]

    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)



    ##################### Sort the main particle arrays and create indexes for each subhalo (for efficient indexing within the main loop) ##############
    
    print "Sorting particle lists and creating indexes"
    time_sort = time.time()

    gas_ts.set_index(nsep_ts)
    star_ts.set_index(nsep_ts)
    dm_ts.set_index(nsep_ts)

    print "Sorting/index creation completed, took", time.time() - time_sort, "seconds"

    # Pack particle data to pass into function
    part_data = [gas_ts, star_ts]
    if extra_stuff:
        part_data = [gas_ts,dm_ts,star_ts]
    if inflows:
        part_data += [id_ejecta_master, id_wind_master]

    # Pack simulation info
    sim_props = [sim_props_ts, boxsize, extra_stuff, inflows]

    # Pack up output variable names information
    output_names = [output_mass_names, output_outflow_names]

    #################### Main loop ####################################################
    time_track_select = 0
    time_track_ptr = 0

    time_pack = time.time()
    print "time spent packing the data", time.time()-time_pack
    time_main = time.time()

    def Dummy_function(i_halo):

        if inflows:
            wind_data_ts_i = {}
            for name in wind_names:
                wind_data_ts_i[name] = wind_data_ts[name][i_halo]
            ejecta_data_ts_i = {}
            for name in ejecta_names:
                ejecta_data_ts_i[name] = ejecta_data_ts[name][i_halo]

            track_lists = [wind_data_ts_i, ejecta_data_ts_i]
        else:
            track_lists = []

        inputs = [ i_halo, i_snip_glob, subhalo_props, part_data, sim_props, output_names, track_lists]

        return main.Subhalo_Loop(inputs)

    output = Parallel(n_jobs=n_jobs_parallel)(delayed(Dummy_function)(i_halo) for i_halo in range(len(subhalo_props["group_number"])))

    print "time spent on main loop", time.time() -time_main
    time_unpack = time.time()

    # Unpack the data
    time_track_subloop = 0.0
    for i_halo in range(len(subhalo_props["group_number"])):

        halo_mass_dict, halo_outflow_dict, timing_info = output[i_halo]
        
        time_track_select += timing_info[0]
        time_track_ptr += timing_info[1]
        time_track_subloop += timing_info[2]

        for name in output_mass_names:
            output_mass_dict[name][i_halo] += halo_mass_dict[name]
        for name in output_outflow_names:
            output_outflow_dict[name][i_halo] += halo_outflow_dict[name]

    print "time spent unpacking the data", time.time() - time_unpack

    ################### Write output data to disk #############################################

    dir_group_name = "directionality_group"
    if dir_group_name not in File:
        dir_group = File.create_group(dir_group_name)
    else:
        dir_group = File[dir_group_name]

    for dset in output_mass_names+output_outflow_names:
        for thing in dir_group:
            if thing == dset:
                del dir_group[dset]

    for dset in subhalo_props:
        for thing in dir_group:
            if thing == dset:
                del dir_group[dset]

    # Invert the argsort we did at the start
    inverse = np.argsort(order_sub)

    for name in output_mass_names:
        dir_group.create_dataset(name, data=output_mass_dict[name])

    for name in output_outflow_names:
        dir_group.create_dataset(name, data=output_outflow_dict[name])

    for name in subhalo_props:
        dir_group.create_dataset(name, data=subhalo_props[name])

    File.close()

    print "computing / write time", time.time()-t4
    print "select,ptr,subloop time", time_track_select, time_track_ptr, time_track_subloop
    print "total time for this snapshot", time.time() - t1

print "Total time for the program in minutes", (time.time() - t0)/60.0
