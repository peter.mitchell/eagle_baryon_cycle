import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

test_mode = False
subsample = True
n_per_bin = 1000

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 2


# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]
snap_list = np.arange(0,51)[::-1]
nsub1d = 1'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_list = np.arange(1,201)[::-1] # starts from 1 for these trees
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub1d = 1 '''


# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]
#snip_list = np.copy(snap_list)
#nsub1d = 1

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if sim_name == "L0025N0376_REF_snap" or sim_name == "L0025N0376_REF_50_snip":
        subvol_choose = 36
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = 2000
    else:
        print "err"
        quit()


input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
#output_base = input_path + sim_name+"_subvols/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)+"_test_mode/"

name_list = ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]
name_list += ["dmdt_out_r10_cvmin_0", "dmdt_out_r20_cvmin_0", "dmdt_out_r50_cvmin_0", "dmdt_out_r10_cvmin_150", "dmdt_out_r20_cvmin_150", "dmdt_out_r50_cvmin_150", "dmdt_out_r10_cvmin_250", "dmdt_out_r20_cvmin_250", "dmdt_out_r50_cvmin_250"]
name_list += ["dmdt_out_r10_cvmin_50", "dmdt_out_r20_cvmin_50", "dmdt_out_r50_cvmin_50"]
name_list += ["dmdt_out_rvir_cvmin_0", "dmdt_out_rvir_cvmin_50", "dmdt_out_rvir_cvmin_150", "dmdt_out_rvir_cvmin_250"]
name_list += ["dmdt_out_0p25rvir_cvmin_0"]

for snip, snap in zip(snip_list,snap_list):

    print "consolidating", snap, snip

    # Choose where to output subvol files and write directory if needed
    output_path = output_base + "Snip_"+str(snip)+"/"

    # Read catalogue data to select a galaxy/halo
    filename = "subhalo_catalogue_" + sim_name
    if test_mode:
        filename += "_test_mode"
    filename += ".hdf5"

    try:
        File = h5py.File(input_path+filename)
    except:
        print "Couldn't read", input_path+filename, "moving on"

    subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]

    group_number_cat = subhalo_group["GroupNumber_hydro"][:]
    subgroup_number_cat = subhalo_group["SubGroupNumber_hydro"][:]
    id_cat = group_number_cat * 1e7 + subgroup_number_cat

    mtotal = subhalo_group["Mtotal"][:]
    mchalo_cat = subhalo_group["mchalo"][:]

    input_list = []
    input_list_noprogen = []

    group_number_subvols = []
    subgroup_number_subvols = []
    group_number_noprogen = []
    subgroup_number_noprogen = []
    for name in name_list:
        input_list.append([])   

    # Find all the subvolume files (they won't be sorted in order)
    subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

    if len(subvols) == 0:
        print "There are no subvolume files. Run the pipeline first"
        quit()

    # Gather together measurements
    for n, subvol in enumerate(subvols):

        if test_mode and "_"+str(subvol_choose)+".hdf5" not in subvol:
            continue

        try:
            File_subvol = h5py.File(subvol)
        except:
            print "Couldn't read subvol file", subvol, "moving on"
            continue

        print "Reading", subvol

        halo_group = File_subvol["subhalo_data"]

        if name_list[-1] in halo_group and name_list[0] in halo_group:
            for i_name, name in enumerate(name_list):

                if input_list[i_name] == [] and len(halo_group[name][:].shape) ==2:
                    input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1]))

                input_list[i_name] = np.append(input_list[i_name], halo_group[name][:],axis=0)

            group_number_subvols = np.append(group_number_subvols, halo_group["group_number"][:])
            subgroup_number_subvols = np.append(subgroup_number_subvols, halo_group["subgroup_number"][:])
        else:
            print "subvol",n,"didn't contain any haloes"

        File_subvol.close()

    if len(group_number_subvols) == 0:
        print "no measurements for this snipshot, moving on"
        continue

    # Reorder galaxies into how they appear in the catalogue files
    id_subvols = group_number_subvols * 1e7 + subgroup_number_subvols
    ptr = ms.match(id_cat, id_subvols)
    ok_match = ptr >= 0 # Successfully matched galaxies

    group_number = group_number_subvols[ptr][ok_match]
    if len(group_number) != len(group_number_cat[mtotal>0]):
        print "Warning, measurements are missing for some galaxies, check the subvol logs to see if one crashed"

    output_list = []
    for input_i in input_list:
        if len(input_i.shape)==1:
            output = np.zeros(len(group_number_cat)) + np.nan
        else: # handle 2d arrays
            output = np.zeros((len(group_number_cat),input_i.shape[1])) + np.nan
        output[ok_match] = input_i[ptr][ok_match]
        output_list.append(output)

    # Delete previous ISM measurements in the catalogue file
    for name in name_list:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]


    print "Writing to", input_path+filename

    # Write measurements to catalogue file
    for name, output in zip(name_list, output_list):
        subhalo_group.create_dataset(name, data=output)
    
    File.close()
