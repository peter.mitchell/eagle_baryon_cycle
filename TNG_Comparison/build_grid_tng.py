import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

test_mode = False

#star_mass = "n_part" # in this mode, we just measure particle numbers, not particle masses
star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

halo_mass = "m200"


# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")


# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
snap_list = np.arange(1,201)[::-1] # starts from 1 for these trees
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub1d = 1
'''
# 50 Mpc no-AGN 28 snaps
#sim_name = "L0050N0752_NOAGN_snap"
#tree_file = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/tree_028.0.hdf5"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]
#snip_list = np.copy(snap_list)

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()

r_shell_list = [10,20,50]
cv_min_list = [0,50,150,250]

omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1))
bin_mstar_mid = 0.5*(bin_mstar[1:]+bin_mstar[0:-1])
bin_vmax = np.arange(1.4, 3.0,(3.0-1.4)/(n_bins+1))
bin_vmax_mid = 0.5*(bin_vmax[1:]+bin_vmax[0:-1])
bin_vcirc = np.arange(1.2, 2.8,(2.8-1.2)/(n_bins+1))
bin_vcirc_mid = 0.5*(bin_vcirc[1:]+bin_vcirc[0:-1])

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()

lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
if test_mode:
    output_base = input_path + sim_name+"_subvols_test_mode/"
else:
    output_base = input_path + sim_name+"_subvols/"

if star_mass == "particle_mass":
    name_list = ["mchalo","SubGroupNumber_hydro","Vmax","Group_M_Crit200_host", "Group_R_Crit200_host"]

    name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]
    name_list += ["dmdt_out_r10_cvmin_0", "dmdt_out_r20_cvmin_0", "dmdt_out_r50_cvmin_0", "dmdt_out_r10_cvmin_150", "dmdt_out_r20_cvmin_150", "dmdt_out_r50_cvmin_150", "dmdt_out_r10_cvmin_250", "dmdt_out_r20_cvmin_250", "dmdt_out_r50_cvmin_250"]
    name_list += ["dmdt_out_r10_cvmin_50", "dmdt_out_r20_cvmin_50", "dmdt_out_r50_cvmin_50"]
    name_list += ["dmdt_out_rvir_cvmin_0", "dmdt_out_rvir_cvmin_50", "dmdt_out_rvir_cvmin_150", "dmdt_out_rvir_cvmin_250"]
    name_list += ["dmdt_out_0p25rvir_cvmin_0"]

data_list = []
dt_list = []

for lna in lna_mid_choose:
    dt_list.append([])
    data_list.append({})
    for name in name_list:
        data_list[-1][name] = []

f_name_list = ["ml_r10_cvmin_0", "ml_r20_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r20_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r20_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r20_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_0", "ml_rvir_cvmin_50", "ml_rvir_cvmin_150","ml_rvir_cvmin_250","ml_0p25rvir_cvmin_0"]

f_name_list2 = ["ml_r10_cvmin_0_med", "ml_r20_cvmin_0_med", "ml_r50_cvmin_0_med","ml_r10_cvmin_50_med", "ml_r20_cvmin_50_med", "ml_r50_cvmin_50_med", "ml_r10_cvmin_150_med", "ml_r20_cvmin_150_med", "ml_r50_cvmin_150_med", "ml_r10_cvmin_250_med", "ml_r20_cvmin_250_med", "ml_r50_cvmin_250_med", "vmax_med","mstar_med","ml_rvir_cvmin_50_med"]
f_name_list2 += ["ml_r10_cvmin_0_lo", "ml_r20_cvmin_0_lo", "ml_r50_cvmin_0_lo","ml_r10_cvmin_50_lo", "ml_r20_cvmin_50_lo", "ml_r50_cvmin_50_lo", "ml_r10_cvmin_150_lo", "ml_r20_cvmin_150_lo", "ml_r50_cvmin_150_lo", "ml_r10_cvmin_250_lo", "ml_r20_cvmin_250_lo", "ml_r50_cvmin_250_lo","ml_rvir_cvmin_50_lo"]
f_name_list2 += ["ml_r10_cvmin_0_hi", "ml_r20_cvmin_0_hi", "ml_r50_cvmin_0_hi","ml_r10_cvmin_50_hi", "ml_r20_cvmin_50_hi", "ml_r50_cvmin_50_hi", "ml_r10_cvmin_150_hi", "ml_r20_cvmin_150_hi", "ml_r50_cvmin_150_hi", "ml_r10_cvmin_250_hi", "ml_r20_cvmin_250_hi", "ml_r50_cvmin_250_hi","ml_rvir_cvmin_50_hi"]
f_name_list2 += ["ml_r10_cvmin_0_complete", "ml_r20_cvmin_0_complete", "ml_r50_cvmin_0_complete","ml_r10_cvmin_50_complete", "ml_r20_cvmin_50_complete", "ml_r50_cvmin_50_complete", "ml_r10_cvmin_150_complete", "ml_r20_cvmin_150_complete", "ml_r50_cvmin_150_complete", "ml_r10_cvmin_250_complete", "ml_r20_cvmin_250_complete", "ml_r50_cvmin_250_complete","ml_rvir_cvmin_50_complete"]

f_med_dict = {}
f_sat_med_dict = {}
f_med_dict_mstar = {}
f_sat_med_dict_mstar = {}
f_med_dict_vmax = {}
f_sat_med_dict_vmax = {}
f_med_dict_vcirc = {}
f_sat_med_dict_vcirc = {}
f_med_dict2 = {}
f_sat_med_dict2 = {}
f_med_dict_mstar2 = {}
f_sat_med_dict_mstar2 = {}
f_med_dict_vmax2 = {}
f_sat_med_dict_vmax2 = {}

for f_name in f_name_list:
    f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vmax[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vmax[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

for f_name in f_name_list2:
    f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_mstar2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_mstar2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vmax2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vmax2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

filename = "subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename)

print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    if name_list[-1] in subhalo_group:
        
        data = subhalo_group[name_list[-1]][:]
        ok = np.isnan(data)==False

        for i_name, name in enumerate(name_list):
            data = subhalo_group[name][:]
            if name == "Group_R_Crit200_host":
                data *= a[i_snap] # pMpc h^-1
            ngal_tot = len(data)
            data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])

            
        ngal = len(data[ok])
        print ngal, ngal_tot

        dt = t[i_snap] - t[i_snap+1]
        dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])

    else:
        print "no data for this snap"


for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = (data["mass_stars_30kpc"]) >= 0
    dt = dt[ok_measure]
    for name in name_list:
        data[name] = data[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    mchalo = data["mchalo"]
    mchalo *= 1.989e43 * g2Msun / h
    mstars_30 = data["mass_stars_30kpc"]
    vmax = data["Vmax"]
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22
    vcirc = np.sqrt(G * data["Group_M_Crit200_host"] * 1.989e43 * g2Msun / h * Msun / (data[ "Group_R_Crit200_host"]*Mpc/h)) / 1e3 # kms^-1 - note comoving conversion was done earlier

    if halo_mass == "m200":
        print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["Group_M_Crit200_host"] * 1.989e43 * g2Msun / h

    # It's probably a better solution to output mass rather than particle numbers 
    if star_mass == "n_part":
        for name in name_list:
            if "n_" in name:
                data[name] *= mgas_part * 1.989e43 * g2Msun / h

    subgroup_number = data["SubGroupNumber_hydro"]

    #### Compute some percentile efficiencies ########
    cent = subgroup_number == 0
    weight_cent = np.zeros_like(dt[cent])
    dt_u = np.unique(dt)
    for dt_u_i in dt_u:
        ok2_cent = dt[cent] == dt_u_i
        weight_cent[ok2_cent] = dt[cent][ok2_cent] / len(dt[cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point

    dt_100 = 1e8 # 100 Myr in years

    temp = 0

    for r_shell in r_shell_list:
        for cv_min in cv_min_list:

            mass_out = data["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)]
            f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_med"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_lo"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_hi"][i_bin],\
                f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_complete"][i_bin],junk = \
                us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/(data["mass_new_stars_init_100_30kpc"][cent]/dt_100),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)


    f_med_dict2["vmax_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),vmax[cent],np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)
    f_med_dict2["mstar_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mstars_30[cent],np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)
    mass_out = data["dmdt_out_rvir_cvmin_50"]
    f_med_dict2["ml_rvir_cvmin_50_med"][i_bin], f_med_dict2["ml_rvir_cvmin_50_lo"][i_bin], f_med_dict2["ml_rvir_cvmin_50_hi"][i_bin], f_med_dict2["ml_rvir_cvmin_50_complete"][i_bin],junk = \
        us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/(data["mass_new_stars_init_100_30kpc"][cent]/dt_100),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

    for r_shell in r_shell_list:
        for cv_min in cv_min_list:

            mass_out = data["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)]
            f_med_dict_mstar2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_med"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_lo"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_hi"][i_bin],\
                f_med_dict_mstar2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_complete"][i_bin],junk = \
                us.Calculate_Percentiles(bin_mstar,np.log10(mstars_30[cent]),mass_out[cent]/(data["mass_new_stars_init_100_30kpc"][cent]/dt_100),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

    mass_out = data["dmdt_out_rvir_cvmin_50"]
    f_med_dict_mstar2["ml_rvir_cvmin_50_med"][i_bin], f_med_dict_mstar2["ml_rvir_cvmin_50_lo"][i_bin], f_med_dict_mstar2["ml_rvir_cvmin_50_hi"][i_bin], f_med_dict_mstar2["ml_rvir_cvmin_50_complete"][i_bin],junk = \
        us.Calculate_Percentiles(bin_mstar,np.log10(mstars_30[cent]),mass_out[cent]/(data["mass_new_stars_init_100_30kpc"][cent]/dt_100),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

    for r_shell in r_shell_list:
        for cv_min in cv_min_list:

            mass_out = data["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)]
            f_med_dict_vmax2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_med"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_lo"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_hi"][i_bin],\
                f_med_dict_vmax2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_complete"][i_bin],junk = \
                us.Calculate_Percentiles(bin_vmax,np.log10(vmax[cent]),mass_out[cent]/(data["mass_new_stars_init_100_30kpc"][cent]/dt_100),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

    mass_out = data["dmdt_out_rvir_cvmin_50"]
    f_med_dict_vmax2["ml_rvir_cvmin_50_med"][i_bin], f_med_dict_vmax2["ml_rvir_cvmin_50_lo"][i_bin], f_med_dict_vmax2["ml_rvir_cvmin_50_hi"][i_bin], f_med_dict_vmax2["ml_rvir_cvmin_50_complete"][i_bin],junk = \
        us.Calculate_Percentiles(bin_vmax,np.log10(vmax[cent]),mass_out[cent]/(data["mass_new_stars_init_100_30kpc"][cent]/dt_100),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])
        ok_mstar = (np.log10(mstars_30) > bin_mstar[j_bin]) & (np.log10(mstars_30) < bin_mstar[j_bin+1])
        ok_vmax = (np.log10(vmax) > bin_vmax[j_bin]) & (np.log10(vmax) < bin_vmax[j_bin+1])
        ok_vcirc = (np.log10(vcirc) > bin_vcirc[j_bin]) & (np.log10(vcirc) < bin_vcirc[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)
        ok_sat = ok & (subgroup_number > 0)

        ok_cent_mstar = ok_mstar & (subgroup_number == 0)
        ok_sat_mstar = ok_mstar & (subgroup_number >0)
        ok_cent_vmax = ok_vmax & (subgroup_number == 0)
        ok_sat_vmax = ok_vmax & (subgroup_number >0)
        ok_cent_vcirc = ok_vcirc & (subgroup_number == 0)
        ok_sat_vcirc = ok_vcirc & (subgroup_number >0)

        weight_cent = np.zeros_like(dt[ok_cent])
        weight_sat = np.zeros_like(dt[ok_sat])
        weight_cent_mstar = np.zeros_like(dt[ok_cent_mstar])
        weight_sat_mstar = np.zeros_like(dt[ok_sat_mstar])
        weight_cent_vmax = np.zeros_like(dt[ok_cent_vmax])
        weight_sat_vmax = np.zeros_like(dt[ok_sat_vmax])
        weight_cent_vcirc = np.zeros_like(dt[ok_cent_vcirc])
        weight_sat_vcirc = np.zeros_like(dt[ok_sat_vcirc])

        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            ok2_sat = dt[ok_sat] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            weight_sat[ok2_sat] = dt[ok_sat][ok2_sat] / len(dt[ok_sat][ok2_sat])

        dt_u = np.unique(dt[ok_mstar])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_mstar] == dt_u_i
            ok2_sat = dt[ok_sat_mstar] == dt_u_i
            weight_cent_mstar[ok2_cent] = dt[ok_cent_mstar][ok2_cent] / len(dt[ok_cent_mstar][ok2_cent])
            weight_sat_mstar[ok2_sat] = dt[ok_sat_mstar][ok2_sat] / len(dt[ok_sat_mstar][ok2_sat])

        dt_u = np.unique(dt[ok_vmax])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_vmax] == dt_u_i
            ok2_sat = dt[ok_sat_vmax] == dt_u_i
            weight_cent_vmax[ok2_cent] = dt[ok_cent_vmax][ok2_cent] / len(dt[ok_cent_vmax][ok2_cent])
            weight_sat_vmax[ok2_sat] = dt[ok_sat_vmax][ok2_sat] / len(dt[ok_sat_vmax][ok2_sat])

        dt_u = np.unique(dt[ok_vcirc])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_vcirc] == dt_u_i
            ok2_sat = dt[ok_sat_vcirc] == dt_u_i
            weight_cent_vcirc[ok2_cent] = dt[ok_cent_vcirc][ok2_cent] / len(dt[ok_cent_vcirc][ok2_cent])
            weight_sat_vcirc[ok2_sat] = dt[ok_sat_vcirc][ok2_sat] / len(dt[ok_sat_vcirc][ok2_sat])

        mass_new_stars_30 = data["mass_new_stars_init_100_30kpc"]
        for r_shell in r_shell_list:
            for cv_min in cv_min_list:

                mass_out = data["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)]
                f_name = "ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(mass_new_stars_30[ok_cent]/dt_100*weight_cent) # Dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat]*weight_sat) / np.sum(mass_new_stars_30[ok_sat]/dt_100*weight_sat)

                f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]*weight_cent_mstar) / np.sum(mass_new_stars_30[ok_cent_mstar]/dt_100*weight_cent_mstar) # Dimensionless
                f_sat_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_mstar]*weight_sat_mstar) / np.sum(mass_new_stars_30[ok_sat_mstar]/dt_100*weight_sat_mstar)

                f_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vmax]*weight_cent_vmax) / np.sum(mass_new_stars_30[ok_cent_vmax]/dt_100*weight_cent_vmax) # Dimensionless
                f_sat_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vmax]*weight_sat_vmax) / np.sum(mass_new_stars_30[ok_sat_vmax]/dt_100*weight_sat_vmax)

                f_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vcirc]*weight_cent_vcirc) / np.sum(mass_new_stars_30[ok_cent_vcirc]/dt_100*weight_cent_vcirc) # Dimensionless
                f_sat_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vcirc]*weight_sat_vcirc) / np.sum(mass_new_stars_30[ok_sat_vcirc]/dt_100*weight_sat_vcirc)

        for cv_min in cv_min_list:

            mass_out = data["dmdt_out_rvir_cvmin_"+str(cv_min)]
            f_name = "ml_rvir_cvmin_"+str(cv_min)

            f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(mass_new_stars_30[ok_cent]/dt_100*weight_cent) # Dimensionless
            f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat]*weight_sat) / np.sum(mass_new_stars_30[ok_sat]/dt_100*weight_sat)
            
            f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]*weight_cent_mstar) / np.sum(mass_new_stars_30[ok_cent_mstar]/dt_100*weight_cent_mstar) # Dimensionless
            f_sat_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_mstar]*weight_sat_mstar) / np.sum(mass_new_stars_30[ok_sat_mstar]/dt_100*weight_sat_mstar)
            
            f_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vmax]*weight_cent_vmax) / np.sum(mass_new_stars_30[ok_cent_vmax]/dt_100*weight_cent_vmax) # Dimensionless
            f_sat_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vmax]*weight_sat_vmax) / np.sum(mass_new_stars_30[ok_sat_vmax]/dt_100*weight_sat_vmax)

            f_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vcirc]*weight_cent_vcirc) / np.sum(mass_new_stars_30[ok_cent_vcirc]/dt_100*weight_cent_vcirc) # Dimensionless
            f_sat_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vcirc]*weight_sat_vcirc) / np.sum(mass_new_stars_30[ok_sat_vcirc]/dt_100*weight_sat_vcirc)

        mass_out = data["dmdt_out_0p25rvir_cvmin_0"]
        f_name = "ml_0p25rvir_cvmin_0"

        f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(mass_new_stars_30[ok_cent]/dt_100*weight_cent) # Dimensionless
        f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat]*weight_sat) / np.sum(mass_new_stars_30[ok_sat]/dt_100*weight_sat)
            
        f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]*weight_cent_mstar) / np.sum(mass_new_stars_30[ok_cent_mstar]/dt_100*weight_cent_mstar) # Dimensionless
        f_sat_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_mstar]*weight_sat_mstar) / np.sum(mass_new_stars_30[ok_sat_mstar]/dt_100*weight_sat_mstar)
            
        f_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vmax]*weight_cent_vmax) / np.sum(mass_new_stars_30[ok_cent_vmax]/dt_100*weight_cent_vmax) # Dimensionless
        f_sat_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vmax]*weight_sat_vmax) / np.sum(mass_new_stars_30[ok_sat_vmax]/dt_100*weight_sat_vmax)

        f_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vcirc]*weight_cent_vcirc) / np.sum(mass_new_stars_30[ok_cent_vcirc]/dt_100*weight_cent_vcirc) # Dimensionless
        f_sat_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vcirc]*weight_sat_vcirc) / np.sum(mass_new_stars_30[ok_sat_vcirc]/dt_100*weight_sat_vcirc)

        print f_med_dict_mstar[f_name][i_bin,j_bin]

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_tng.hdf5"
if test_mode:
    filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_tng_test_mode.hdf5"
if os.path.isfile(filename):
    os.remove(filename)
    print "REmoving", filename

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_mstar_grid", data=bin_mstar_mid)
File_grid.create_dataset("log_vmax_grid", data=bin_vmax_mid)
File_grid.create_dataset("log_vcirc_grid", data=bin_vcirc_mid)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

for f_name in f_name_list: 
    File_grid.create_dataset(f_name, data=f_med_dict[f_name])
    File_grid.create_dataset(f_name+"_sat", data=f_sat_med_dict[f_name])

    File_grid.create_dataset(f_name+"_mstar", data=f_med_dict_mstar[f_name])
    File_grid.create_dataset(f_name+"_sat_mstar", data=f_sat_med_dict_mstar[f_name])

    File_grid.create_dataset(f_name+"_vmax", data=f_med_dict_vmax[f_name])
    File_grid.create_dataset(f_name+"_sat_vmax", data=f_sat_med_dict_vmax[f_name])

    File_grid.create_dataset(f_name+"_vcirc", data=f_med_dict_vcirc[f_name])
    File_grid.create_dataset(f_name+"_sat_vcirc", data=f_sat_med_dict_vcirc[f_name])

for f_name in f_name_list2:
    File_grid.create_dataset(f_name, data=f_med_dict2[f_name])
    File_grid.create_dataset(f_name+"_sat", data=f_sat_med_dict2[f_name])

    File_grid.create_dataset(f_name+"_mstar", data=f_med_dict_mstar2[f_name])
    File_grid.create_dataset(f_name+"_sat_mstar", data=f_sat_med_dict_mstar2[f_name])

    File_grid.create_dataset(f_name+"_vmax", data=f_med_dict_vmax2[f_name])
    File_grid.create_dataset(f_name+"_sat_vmax", data=f_sat_med_dict_vmax2[f_name])

File_grid.close()
