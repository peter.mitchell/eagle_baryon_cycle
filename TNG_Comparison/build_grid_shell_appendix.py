import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

test_mode = False

#star_mass = "n_part" # in this mode, we just measure particle numbers, not particle masses
star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

halo_mass = "m200"


# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snap_list = np.rint(snap_list[::-1]).astype("int")


# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"'''

# 25 Mpc REF with 200 snips
#sim_name = "L0025N0376_REF_200_snip"
#tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
#snap_list = np.arange(1,201)[::-1] # starts from 1 for these trees

# 50 Mpc no-AGN 28 snaps
#sim_name = "L0050N0752_NOAGN_snap"
#tree_file = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/tree_028.0.hdf5"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]


r_shell_list = [10,30,50]
cv_min_list = [0,50,150,250]

omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1))
bin_mstar_mid = 0.5*(bin_mstar[1:]+bin_mstar[0:-1])
bin_vmax = np.arange(1.4, 3.0,(3.0-1.4)/(n_bins+1))
bin_vmax_mid = 0.5*(bin_vmax[1:]+bin_vmax[0:-1])
bin_vcirc = np.arange(1.2, 2.8,(2.8-1.2)/(n_bins+1))
bin_vcirc_mid = 0.5*(bin_vcirc[1:]+bin_vcirc[0:-1])


File_tree = h5py.File(tree_file,"r")
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

# Set values of ln(expansion factor) that will define the centre of each redshift bin
z_targets = [0.25, 2]
lna_mid_choose = []
for z_target in z_targets:
    z = (1./a-1)[np.argmin(abs(1./a -1 -z_target))]
    lna_mid_choose.append(np.log(1/(1+z)))
lna_mid_choose = np.array(lna_mid_choose) # z=0.25,0.5

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()

lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path2 = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
if test_mode:
    output_base = input_path + sim_name+"_subvols_test_mode/"
else:
    output_base = input_path + sim_name+"_subvols/"

if star_mass == "particle_mass":
    name_list = ["mchalo","SubGroupNumber_hydro","Vmax","Group_M_Crit200_host", "Group_R_Crit200_host"]

    name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc", "mass_new_stars_init","mass_star"]

    name_list += ["dmdt_out_r10_cvmin_0", "dmdt_out_r30_cvmin_0", "dmdt_out_r50_cvmin_0"]
    name_list += ["dmdt_out_r10_cvmin_50", "dmdt_out_r30_cvmin_50", "dmdt_out_r50_cvmin_50"]
    name_list += ["dmdt_out_r10_cvmin_150", "dmdt_out_r30_cvmin_150", "dmdt_out_r50_cvmin_150"]
    name_list += ["dmdt_out_r10_cvmin_250", "dmdt_out_r30_cvmin_250", "dmdt_out_r50_cvmin_250"]

    name_list += ["dmdt_out_rvir_cvmin_0", "dmdt_out_rvir_cvmin_50", "dmdt_out_rvir_cvmin_150", "dmdt_out_rvir_cvmin_250"]
    name_list += ["dmdt_out_0p25rvir_cvmin_0", "dmdt_out_0p25rvir_cvmin_50", "dmdt_out_0p25rvir_cvmin_150", "dmdt_out_0p25rvir_cvmin_250"]
    name_list += ["dmdt_out_0p5rvir_cvmin_0", "dmdt_out_0p5rvir_cvmin_50", "dmdt_out_0p5rvir_cvmin_150", "dmdt_out_0p5rvir_cvmin_250"]
    name_list += ["dmdt_out_0p75rvir_cvmin_0", "dmdt_out_0p75rvir_cvmin_50", "dmdt_out_0p75rvir_cvmin_150", "dmdt_out_0p75rvir_cvmin_250"]

    name_list += ["dmdt_out_rvir_0p125vmax", "dmdt_out_rvir_0p25vmax", "dmdt_out_rvir_0p5vmax", "dmdt_out_rvir_vmax"]
    name_list += ["dmdt_out_0p25rvir_0p125vmax", "dmdt_out_0p25rvir_0p25vmax", "dmdt_out_0p25rvir_0p5vmax", "dmdt_out_0p25rvir_vmax"]
    name_list += ["dmdt_out_0p5rvir_0p125vmax", "dmdt_out_0p5rvir_0p25vmax", "dmdt_out_0p5rvir_0p5vmax", "dmdt_out_0p5rvir_vmax"]
    name_list += ["dmdt_out_0p75rvir_0p125vmax", "dmdt_out_0p75rvir_0p25vmax", "dmdt_out_0p75rvir_0p5vmax", "dmdt_out_0p75rvir_vmax"]

    fV_str = "_"+str(0.25).replace(".","p")+"vmax"
    name_list2 = [ "mchalo","subgroup_number","vmax","m200_host", "r200_host"]
    name_list2 += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc", "mass_new_stars_init","mass_star"]    
    name_list2 += ["mass_sf_ism_join_wind"+fV_str, "mass_nsf_ism_join_wind"+fV_str, "mass_ism_reheated_join_wind"+fV_str]
    name_list2 += ["mass_join_halo_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str]
        
data_list = []
data_list2 = []
dt_list = []
dt_list2 = []

for lna in lna_mid_choose:
    dt_list.append([])
    dt_list2.append([])
    data_list.append({})
    data_list2.append({})
    for name in name_list:
        data_list[-1][name] = []
    for name in name_list2:
        data_list2[-1][name] = []

f_name_list = ["ml_r10_cvmin_0", "ml_r30_cvmin_0", "ml_r50_cvmin_0","ml_r10_cvmin_50", "ml_r30_cvmin_50", "ml_r50_cvmin_50", "ml_r10_cvmin_150", "ml_r30_cvmin_150", "ml_r50_cvmin_150", "ml_r10_cvmin_250", "ml_r30_cvmin_250", "ml_r50_cvmin_250","ml_rvir_cvmin_0", "ml_rvir_cvmin_50", "ml_rvir_cvmin_150","ml_rvir_cvmin_250"]
f_name_list += ["ism_wind_tot_ml","halo_wind_tot_ml"]

f_name_list2 = ["ml_r10_cvmin_0_med", "ml_r30_cvmin_0_med", "ml_r50_cvmin_0_med","ml_r10_cvmin_50_med", "ml_r30_cvmin_50_med", "ml_r50_cvmin_50_med", "ml_r10_cvmin_150_med", "ml_r30_cvmin_150_med", "ml_r50_cvmin_150_med", "ml_r10_cvmin_250_med", "ml_r30_cvmin_250_med", "ml_r50_cvmin_250_med", "vmax_med","mstar_med"]
f_name_list2 += ["ml_r10_cvmin_0_lo", "ml_r30_cvmin_0_lo", "ml_r50_cvmin_0_lo","ml_r10_cvmin_50_lo", "ml_r30_cvmin_50_lo", "ml_r50_cvmin_50_lo", "ml_r10_cvmin_150_lo", "ml_r30_cvmin_150_lo", "ml_r50_cvmin_150_lo", "ml_r10_cvmin_250_lo", "ml_r30_cvmin_250_lo", "ml_r50_cvmin_250_lo"]
f_name_list2 += ["ml_r10_cvmin_0_hi", "ml_r30_cvmin_0_hi", "ml_r50_cvmin_0_hi","ml_r10_cvmin_50_hi", "ml_r30_cvmin_50_hi", "ml_r50_cvmin_50_hi", "ml_r10_cvmin_150_hi", "ml_r30_cvmin_150_hi", "ml_r50_cvmin_150_hi", "ml_r10_cvmin_250_hi", "ml_r30_cvmin_250_hi", "ml_r50_cvmin_250_hi"]
f_name_list2 += ["ml_r10_cvmin_0_complete", "ml_r30_cvmin_0_complete", "ml_r50_cvmin_0_complete","ml_r10_cvmin_50_complete", "ml_r30_cvmin_50_complete", "ml_r50_cvmin_50_complete", "ml_r10_cvmin_150_complete", "ml_r30_cvmin_150_complete", "ml_r50_cvmin_150_complete", "ml_r10_cvmin_250_complete", "ml_r30_cvmin_250_complete", "ml_r50_cvmin_250_complete"]
f_name_list2 += ["ism_wind_tot_ml_med", "ism_wind_tot_ml_lo", "ism_wind_tot_ml_hi", "ism_wind_tot_ml_complete"]
f_name_list2 += ["halo_wind_tot_ml_med", "halo_wind_tot_ml_lo", "halo_wind_tot_ml_hi", "halo_wind_tot_ml_complete"]

cv_min_list = ["0","50","150","250"]
for cv_min in cv_min_list:
    f_name_list2 += ["ml_0p25rvir_cvmin_"+cv_min+"_med","ml_0p25rvir_cvmin_"+cv_min+"_lo","ml_0p25rvir_cvmin_"+cv_min+"_hi","ml_0p25rvir_cvmin_"+cv_min+"_complete"]
    f_name_list2 += ["ml_0p5rvir_cvmin_"+cv_min+"_med","ml_0p5rvir_cvmin_"+cv_min+"_lo","ml_0p5rvir_cvmin_"+cv_min+"_hi","ml_0p5rvir_cvmin_"+cv_min+"_complete"]
    f_name_list2 += ["ml_0p75rvir_cvmin_"+cv_min+"_med","ml_0p75rvir_cvmin_"+cv_min+"_lo","ml_0p75rvir_cvmin_"+cv_min+"_hi","ml_0p75rvir_cvmin_"+cv_min+"_complete"]
    f_name_list2 += ["ml_rvir_cvmin_"+cv_min+"_med","ml_rvir_cvmin_"+cv_min+"_lo","ml_rvir_cvmin_"+cv_min+"_hi","ml_rvir_cvmin_"+cv_min+"_complete"]

    f_name_list += ["ml_0p25rvir_cvmin_"+cv_min,"ml_0p5rvir_cvmin_"+cv_min,"ml_0p75rvir_cvmin_"+cv_min]

cv_min_list2 = ["","0p125","0p25","0p5"]
r_list = ["0p25","0p5","0p75",""]
for cv_min in cv_min_list2:
    for r in r_list:
        f_name_list2 += ["ml_"+r+"rvir_cvmin_"+cv_min+"vmax_med","ml_"+r+"rvir_cvmin_"+cv_min+"vmax_lo","ml_"+r+"rvir_cvmin_"+cv_min+"vmax_hi","ml_"+r+"rvir_cvmin_"+cv_min+"vmax_complete"]
        f_name_list += ["ml_"+r+"rvir_cvmin_"+cv_min+"vmax"]

f_med_dict = {}
f_sat_med_dict = {}
f_med_dict_mstar = {}
f_sat_med_dict_mstar = {}
f_med_dict_vmax = {}
f_sat_med_dict_vmax = {}
f_med_dict_vcirc = {}
f_sat_med_dict_vcirc = {}
f_med_dict2 = {}
f_sat_med_dict2 = {}
f_med_dict_mstar2 = {}
f_sat_med_dict_mstar2 = {}
f_med_dict_vmax2 = {}
f_sat_med_dict_vmax2 = {}

for f_name in f_name_list:
    f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vmax[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vmax[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

for f_name in f_name_list2:
    f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_mstar2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_mstar2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vmax2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vmax2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

filename = "subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

filename2 = "/Processed_Catalogues/processed_subhalo_catalogue_" + sim_name
filename2 += ".hdf5"
File2 = h5py.File(input_path2+filename2,"r")

print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    # For this appendix, I'm going to try only using a single snapshot for the Lagrangian measurements, plans may change later
    if np.sort(abs(np.log(a)[i_snap]-lna_mid_choose))[0] > 1e-6:
        continue
    else:
        print "Reading data for redshift", 1./a[i_snap]-1

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    subhalo_group2 = File2["Snap_"+str(snap)+"/subhalo_properties"]

    if "dmdt_out_rvir_0p125vmax" in subhalo_group:

        data = subhalo_group[name_list[-1]][:]
        ok = np.isnan(data)==False


        for i_name, name in enumerate(name_list):

            data = subhalo_group[name][:]
            if name == "Group_R_Crit200_host":
                data *= a[i_snap] # pMpc h^-1
            ngal_tot = len(data)
            data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])

            
        ngal = len(data[ok])
        #print ngal, ngal_tot

        dt = t[i_snap] - t[i_snap+1]
        dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)

        data = subhalo_group2[name_list2[-1]][:]
        ok2 = np.isnan(data)==False

        for i_name, name in enumerate(name_list2):
            
            data = subhalo_group2[name][:]
            if name == "Group_R_Crit200_host":
                data *= a[i_snap] # pMpc h^-1
            data_list2[ind_bin_snap][name] = np.append(data_list2[ind_bin_snap][name], data[ok2])

        ngal2 = len(data[ok2])

        dt_list2[ind_bin_snap] = np.append(dt_list2[ind_bin_snap], np.zeros(ngal2)+dt)

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])

    else:
        print "no data for this snap"

for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]
    data2 = data_list2[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]
    dt2 = dt_list2[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = data["mass_star"] >= 0
    dt = dt[ok_measure] 
    for name in name_list:
        data[name] = data[name][ok_measure]

    ok_measure = data2["mass_star"] >= 0
    dt2 = dt2[ok_measure]
    for name in name_list2:
        data2[name] = data2[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    mchalo = data["mchalo"]
    mchalo *= 1.989e43 * g2Msun / h
    vmax = data["Vmax"]
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22
    vcirc = np.sqrt(G * data["Group_M_Crit200_host"] * 1.989e43 * g2Msun / h * Msun / (data[ "Group_R_Crit200_host"]*Mpc/h)) / 1e3 # kms^-1 - note comoving conversion was done earlier

    if halo_mass == "m200":
        print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["Group_M_Crit200_host"] * 1.989e43 * g2Msun / h

    mchalo2 = data2["m200_host"]

    # It's probably a better solution to output mass rather than particle numbers 
    if star_mass == "n_part":
        for name in name_list:
            if "n_" in name:
                data[name] *= mgas_part * 1.989e43 * g2Msun / h

    subgroup_number = data["SubGroupNumber_hydro"]
    subgroup_number2 = data2["subgroup_number"]

    #### Compute some median + percentile efficiencies ########
    cent = subgroup_number == 0
    weight_cent = np.zeros_like(dt[cent])
    dt_u = np.unique(dt)
    for dt_u_i in dt_u:
        ok2_cent = dt[cent] == dt_u_i
        weight_cent[ok2_cent] = dt[cent][ok2_cent] / len(dt[cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point

    cent2 = subgroup_number2 == 0
    weight_cent2 = np.zeros_like(dt2[cent2])
    dt_u2 = np.unique(dt2)
    for dt_u_i in dt_u2:
        ok2_cent = dt2[cent2] == dt_u_i
        weight_cent2[ok2_cent] = dt2[cent2][ok2_cent] / len(dt2[cent2][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point

    temp = 0

    for r_shell in r_shell_list:
        for cv_min in cv_min_list:

            mass_out = data["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)]
            f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_med"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_lo"][i_bin], f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_hi"][i_bin],\
                f_med_dict2["ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)+"_complete"][i_bin],junk = \
                us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/(data["mass_new_stars_init"][cent]/(dt[cent]*1e9)),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

    r_shell_list2 = ["0p25","0p5","0p75",""]
    for r_shell in r_shell_list2:
        for cv_min in cv_min_list:

            mass_out = data["dmdt_out_"+r_shell+"rvir_cvmin_"+str(cv_min)]
            f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+str(cv_min)+"_med"][i_bin], f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+str(cv_min)+"_lo"][i_bin], f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+str(cv_min)+"_hi"][i_bin],f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+str(cv_min)+"_complete"][i_bin],junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/(data["mass_new_stars_init"][cent]/(dt[cent]*1e9)),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)

        for cv_min in cv_min_list2:

            mass_out = data["dmdt_out_"+r_shell+"rvir_"+cv_min+"vmax"]
            f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+cv_min+"vmax_med"][i_bin], f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+cv_min+"vmax_lo"][i_bin], f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+cv_min+"vmax_hi"][i_bin],f_med_dict2["ml_"+r_shell+"rvir_cvmin_"+cv_min+"vmax_complete"][i_bin],junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/(data["mass_new_stars_init"][cent]/(dt[cent]*1e9)),np.ones_like(weight_cent),0,lohi=(0.16,0.84),complete=0.8)


    mass_out = data2["mass_sf_ism_join_wind"+fV_str] + data2["mass_nsf_ism_join_wind"+fV_str] + data2["mass_ism_reheated_join_wind"+fV_str]
    f_med_dict2["ism_wind_tot_ml_med"][i_bin], f_med_dict2["ism_wind_tot_ml_lo"][i_bin], f_med_dict2["ism_wind_tot_ml_hi"][i_bin],f_med_dict2["ism_wind_tot_ml_complete"][i_bin],junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo2[cent2]),mass_out[cent2]/(data2["mass_new_stars_init"][cent2]),np.ones_like(weight_cent2),0,lohi=(0.16,0.84),complete=0.8)

    mass_out = data2["mass_join_halo_wind"+fV_str] + data2["mass_halo_reheated_join_wind"+fV_str]
    f_med_dict2["halo_wind_tot_ml_med"][i_bin], f_med_dict2["halo_wind_tot_ml_lo"][i_bin], f_med_dict2["halo_wind_tot_ml_hi"][i_bin],f_med_dict2["halo_wind_tot_ml_complete"][i_bin],junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo2[cent2]),mass_out[cent2]/(data2["mass_new_stars_init"][cent2]),np.ones_like(weight_cent2),0,lohi=(0.16,0.84),complete=0.8)


    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)
        ok_sat = ok & (subgroup_number > 0)

        weight_cent = np.zeros_like(dt[ok_cent])
        weight_sat = np.zeros_like(dt[ok_sat])

        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            ok2_sat = dt[ok_sat] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            weight_sat[ok2_sat] = dt[ok_sat][ok2_sat] / len(dt[ok_sat][ok2_sat])

        mass_new_stars = data["mass_new_stars_init"]

        ok2 = (np.log10(mchalo2) > bin_mh[j_bin]) & (np.log10(mchalo2) < bin_mh[j_bin+1])

        ok_cent2 = ok2 & (subgroup_number2 == 0)
        ok_sat2 = ok2 & (subgroup_number2 > 0)

        weight_cent2 = np.zeros_like(dt2[ok_cent2])
        weight_sat2 = np.zeros_like(dt2[ok_sat2])

        dt_u2 = np.unique(dt2[ok2])
        for dt_u_i in dt_u2:
            ok2_cent2 = dt2[ok_cent2] == dt_u_i
            ok2_sat2 = dt2[ok_sat2] == dt_u_i
            weight_cent2[ok2_cent2] = dt2[ok_cent2][ok2_cent2] / len(dt2[ok_cent2][ok2_cent2]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            weight_sat2[ok2_sat2] = dt2[ok_sat2][ok2_sat2] / len(dt2[ok_sat2][ok2_sat2])

        mass_new_stars2 = data2["mass_new_stars_init"]

        for r_shell in r_shell_list:
            for cv_min in cv_min_list:

                mass_out = data["dmdt_out_r"+str(r_shell)+"_cvmin_"+str(cv_min)]
                f_name = "ml_r"+str(r_shell)+"_cvmin_"+str(cv_min)

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(mass_new_stars[ok_cent]/(dt[ok_cent]*1e9)*weight_cent) # Dimensionless

        for r_shell in r_shell_list2:
            for cv_min in cv_min_list:
                
                mass_out = data["dmdt_out_"+r_shell+"rvir_cvmin_"+str(cv_min)]
                f_name = "ml_"+r_shell+"rvir_cvmin_"+str(cv_min)

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(mass_new_stars[ok_cent]/(dt[ok_cent]*1e9)*weight_cent) # Dimensionless

            for cv_min in cv_min_list2:

                mass_out = data["dmdt_out_"+r_shell+"rvir_"+str(cv_min)+"vmax"]
                f_name = "ml_"+r_shell+"rvir_cvmin_"+str(cv_min)+"vmax"

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(mass_new_stars[ok_cent]/(dt[ok_cent]*1e9)*weight_cent) # Dimensionless

        mass_out = data2["mass_sf_ism_join_wind"+fV_str] + data2["mass_nsf_ism_join_wind"+fV_str] + data2["mass_ism_reheated_join_wind"+fV_str]
        f_med_dict["ism_wind_tot_ml"][i_bin,j_bin] = np.sum(mass_out[ok_cent2]/dt2[ok_cent2]*weight_cent2) / np.sum(mass_new_stars2[ok_cent2]/dt2[ok_cent2]*weight_cent2)
        mass_out = data2["mass_join_halo_wind"+fV_str] + data2["mass_halo_reheated_join_wind"+fV_str]
        f_med_dict["halo_wind_tot_ml"][i_bin,j_bin] = np.sum(mass_out[ok_cent2]/dt2[ok_cent2]*weight_cent2) / np.sum(mass_new_stars2[ok_cent2]/dt2[ok_cent2]*weight_cent2)

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_shell_appendix.hdf5"
if test_mode:
    filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_shell_appendix_test_mode.hdf5"
if os.path.isfile(filename):
    os.remove(filename)
    print "REmoving", filename

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

for f_name in f_name_list: 
    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

for f_name in f_name_list2:
    File_grid.create_dataset(f_name, data=f_med_dict2[f_name])

File_grid.close()
