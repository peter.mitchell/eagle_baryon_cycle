import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us
import math

print ""
print "TEMP HACK"
from utilities_plotting import *
print ""

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, sim_props, output_names, track_lists = inputs

    time_track_select = 0.0
    time_track_ptr = 0.0


    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_outflow_dict = {}; halo_mass_dict = {}

    output_mass_names, output_outflow_names = output_names
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0
    for name in output_outflow_names:
        halo_outflow_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ts, boxsize, extra_stuff, inflows = sim_props
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    
    # Unpack particle data
    if extra_stuff and inflows:
        gas_ts, dm_ts, star_ts, id_ejecta_master, id_wind_master = part_data
    elif extra_stuff:
        gas_ts, dm_ts, star_ts = part_data
    elif inflows:
        gas_ts, star_ts, id_ejecta_master, id_wind_master = part_data
    else:
        gas_ts, star_ts = part_data

    if inflows:
        wind_ts, ejecta_ts = track_lists
        
    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = np.copy(subhalo_props["r200_host"][i_halo]) # code units
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    
    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ###### Select central gas particles
    select_gas = mpf.Match_Index(halo_subhalo_index, gas_ts.subhalo_index_unique, gas_ts.particle_subhalo_index)

    # Convert to Msun
    g2Msun = 1./(1.989e33)
    mass_gas = np.copy(mpf.Index_Array(gas_ts.data["mass"],select_gas,None))
    mass_gas *= 1.989e43 * g2Msun / h

    if inflows:
        id_gas = mpf.Index_Array(gas_ts.data["id"],select_gas,None)

        in_wind = np.zeros_like(id_gas) == 1
        if len(wind_ts["id"]) > 0 and len(id_gas) > 0:
            ptr = ms.match(id_gas, wind_ts["id"])
            ok_match = ptr >= 0
            in_wind[ok_match] = True

    coordinates_gas = mpf.Index_Array(gas_ts.data["coordinates"],select_gas,None)

    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_halo = [halo_x, halo_y, halo_z]
    coordinates_gas = mpf.Centre_Halo(coordinates_gas, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
    # Put velocity into proper coordinates (kms)
    velocity_gas = np.copy(mpf.Index_Array(gas_ts.data["velocity"],select_gas,None)) * expansion_factor_ts**0.5
    # change velocities to halo frame
    velocity_gas += - np.array([halo_vx, halo_vy, halo_vz]) # note halo velocities were already in proper peculiar velocity units

    r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
    r_gas_norm = np.swapaxes(np.array([coordinates_gas[:,0],coordinates_gas[:,1],coordinates_gas[:,2]]) / r_gas,0,1)
    cv_gas = np.sum(r_gas_norm * velocity_gas,axis=1)

    temperature_gas = mpf.Index_Array(gas_ts.data["temperature"],select_gas,None)
    density_gas = mpf.Index_Array(gas_ts.data["density"],select_gas,None)
    metallicity_gas = mpf.Index_Array(gas_ts.data["metallicity"],select_gas,None)
    if extra_stuff:
        internal_energy_gas = mpf.Index_Array(gas_ts.data["internal_energy"],select_gas,None)

    xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
    nh_gas = density_gas * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
    temp = np.copy(metallicity_gas) # To avoid raising zero metallicity particles to a negative power
    ok = temp > 0
    temp[ok] = np.power(temp[ok]/0.002,-0.64)
    temp[ok==False] = 100
    nh_thresh = np.min( (np.zeros_like(nh_gas)+10.0 , 0.1 * temp), axis=0)
    T_eos = 8e3 * np.power(nh_gas/0.1, 4/3.)
    SF = (nh_gas > nh_thresh) & (np.log10(temperature_gas) < np.log10(T_eos) +0.5)

    gas_props = [coordinates_gas, temperature_gas, nh_gas, SF]
    halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]
    tdyn_ts = t_ts * 0.1 # Approximation of the halo dynamical time, in Gyr
    sim_props = [redshift_ts, tdyn_ts, h]
    ISM =  mpf.Select_ISM_simple(gas_props, halo_props , sim_props)

    ##### Select star particles that belong to desired group, subgroup
    select_star = mpf.Match_Index(halo_subhalo_index, star_ts.subhalo_index_unique, star_ts.particle_subhalo_index)

    # Convert to Msun
    mass_star = np.copy(mpf.Index_Array(star_ts.data["mass"],select_star,None))
    mass_star *= 1.989e43 * g2Msun / h

    mass_star_init = np.copy(mpf.Index_Array(star_ts.data["mass_init"],select_star,None))
    mass_star_init *= 1.989e43 * g2Msun / h

    coordinates_star = mpf.Index_Array(star_ts.data["coordinates"],select_star,None)
    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_star = mpf.Centre_Halo(coordinates_star, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))

    # Work out which star particles formed between now and the previous timestep
    aform_star = mpf.Index_Array(star_ts.data["aform"],select_star,None)
    
    t_star, junk = uc.t_Universe(aform_star, omm, h)
    age_star = (t_ts - t_star)*1e3 # Myr

    if extra_stuff:
        select_dm = mpf.Match_Index(halo_subhalo_index, dm_ts.subhalo_index_unique, dm_ts.particle_subhalo_index)

        id_dm = mpf.Index_Array(dm_ts.data["id"],select_dm,None)
        coordinates_dm = mpf.Index_Array(dm_ts.data["coordinates"],select_dm,None)
        coordinates_dm = mpf.Centre_Halo(coordinates_dm, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

        m_h = 1.6726 * 10**(-27)
        kms = 1e3
        cm = 1e2
        gamma = 5/3.0 # Ratio of specific heats
        xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye 15
        # See http://www.mpia.de/homes/dullemon/lectures/hydrodynamicsII/chap_1.pdf, pg 7
        pressure_gas = (gamma-1) * nh_gas / xh_sf_thresh* cm**3 * m_h * internal_energy_gas * kms**2 # kg m-1 s^-2 = Pa

        r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
        r_dm = np.sqrt(np.square(coordinates_dm[:,0])+np.square(coordinates_dm[:,1])+np.square(coordinates_dm[:,2]))
        r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))

        r_all = np.concatenate((r_gas,r_star,r_dm))
        mass_dm_temp = np.zeros_like(r_dm) + mass_dm * 1.989e43 * g2Msun / h
        mass_all = np.concatenate((mass_gas,mass_star,mass_dm_temp))
        order = np.argsort(r_all)
        mass_cum_sorted = np.cumsum(mass_all[order])
        inverse = np.argsort(order)
        mass_cum = mass_cum_sorted[inverse]
        mass_gas_cum = mass_cum[0:len(mass_gas)]

        G = 6.67408e-11 # m^3 kg^-1 s^-2
        Msun = 1.989e30 # kg
        kpc =  3.0857e19 # m
        kms = 1e3 # ms^-1
        # Spherically-averaged gravitational acceleration
        a_grav_gas = G * mass_gas_cum*Msun / (r_gas*kpc)**2 # m s^-2

        # For volume-weighted quantities
        pseudo_volume_gas = mass_gas / nh_gas

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ################ Do measurements ####################

    halo_mass_dict["mass_stars_30kpc"] = np.sum(mass_star[r_star<30])
    halo_mass_dict["mass_new_stars_init_100_30kpc"] = np.sum(mass_star_init[(r_star<30)&(age_star<100)])


    ############ Work out galacto-centric coordinates (ala Nelson) ############

    def rotation_matrix(axis, theta):
        """
        Return the rotation matrix associated with counterclockwise rotation about
        the given axis by theta radians.
        """
        axis = np.asarray(axis)
        axis = axis/math.sqrt(np.dot(axis, axis))
        a = math.cos(theta/2.0)
        b, c, d = -axis*math.sin(theta/2.0)
        aa, bb, cc, dd = a*a, b*b, c*c, d*d
        bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
        return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                         [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                         [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])

    # Rotate galaxy such that the ISM angular momentum vector is orthogonal to the x-y plane
    x_gas = coordinates_gas[:,0]; y_gas = coordinates_gas[:,1]; z_gas = coordinates_gas[:,2]
    J_ISM = np.sum(np.expand_dims(mass_gas[ISM|SF],1) * np.cross(np.swapaxes(np.array([x_gas[ISM|SF],y_gas[ISM|SF],z_gas[ISM|SF]]),0,1),velocity_gas[ISM|SF]),axis=0)
    J_ISM_norm = J_ISM / np.sqrt(np.sum(np.square(J_ISM)))
    z_norm = np.array([0, 0, 1])

    v = np.cross(J_ISM_norm,z_norm)
    costheta = np.dot(z_norm,J_ISM_norm)
    theta = np.arccos(costheta)

    rot_matrix = rotation_matrix(v,theta)
    x_gas_prime,y_gas_prime,z_gas_prime = np.dot(rot_matrix, np.array([x_gas, y_gas, z_gas]))

    ### Compute galacto-centric angle ###
    xz_gas_norm = np.array([x_gas_prime,z_gas_prime])
    xz_gas_norm = xz_gas_norm / np.sqrt(np.sum(np.square(xz_gas_norm),axis=0))

    x_norm = np.array([1,0])
    costheta = np.dot(x_norm, xz_gas_norm)
    theta_gas = np.arccos(costheta)
    # To bicones (if they are present), need to distinguish between z +/- 0 here
    zneg = z_gas_prime < 0
    theta_gas[zneg] *= -1


    #### Compute spherical coordinates ### (Note we don't strictly need to rotate system for covering fraction measurements)
    theta_solid_gas = np.arccos(z_gas_prime / np.sqrt(np.square(x_gas_prime)+np.square(y_gas_prime)+np.square(z_gas_prime))) # Polar angle, between 0 and pi
    thi_solid_gas = np.arctan2(y_gas_prime,x_gas_prime) # Azimuthal angle, between 0 and 2pi
 
    '''choose = np.random.random_integers(0,len(x_gas),1)

    print ""
    print "theta,thi", solid_theta_gas[choose]*180/np.pi, solid_thi_gas[choose]*180/np.pi
    print "x,y,z", x_gas_prime[choose], y_gas_prime[choose], z_gas_prime[choose]
    print ""

    subplots = panelplot(2,2)
    for n, ax in enumerate(subplots):
        py.axes(ax)
        if n == 0:
            py.scatter(x_gas_prime[ISM], z_gas_prime[ISM])
            py.scatter(x_gas_prime[choose],z_gas_prime[choose],c="r",s=20)
            py.plot([0,x_gas_prime[choose]],[0,z_gas_prime[choose]],c="r")
        if n == 1:
            py.scatter(x_gas_prime[ISM], y_gas_prime[ISM])
            py.scatter(x_gas_prime[choose],y_gas_prime[choose],c="r",s=20)
            py.plot([0,x_gas_prime[choose]],[0,y_gas_prime[choose]],c="r")
        if n == 2:
            py.scatter(y_gas_prime[ISM], z_gas_prime[ISM])
            py.scatter(y_gas_prime[choose],z_gas_prime[choose],c="r",s=20)
            py.plot([0,y_gas_prime[choose]],[0,z_gas_prime[choose]],c="r")
        ax.set_aspect("equal")
    py.show()'''

    #### Do some very basic shell measurements
    vmin =[0.0, 0.125, 0.25, 0.5, 1.0]

    r200 = halo_r200_host *1e3/(1+redshift_ts)/h # pkpc  # pkpc

    dr = 0.1
    rbins = np.arange(0,1.1,dr)
    rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
    n_rbins = len(rmid)

    kpc2km = 3.0857 * 10**16
    s2yr = 31557600.0

    if extra_stuff:
        halo_outflow_dict["dmdr"] = np.zeros((len(rmid)))
        halo_outflow_dict["dpdr"] = np.zeros((len(rmid)))
        halo_outflow_dict["dEkdr"] = np.zeros((len(rmid)))
        halo_outflow_dict["dEtdr"] = np.zeros((len(rmid)))
        halo_outflow_dict["P50mw"] = np.zeros((len(rmid)))
        halo_outflow_dict["P50Vw"] = np.zeros((len(rmid)))
        halo_outflow_dict["Pmean"] = np.zeros((len(rmid)))
        halo_outflow_dict["mshell"] = np.zeros((len(rmid)))
        halo_outflow_dict["agrav"] = np.zeros((len(rmid)))
        
        for i_r in range(len(rmid)):
            ok = (r_gas<rbins[i_r+1]*r200) & (r_gas>rbins[i_r]*r200)

            halo_outflow_dict["dmdr"][i_r] += np.sum(mass_gas[ok])  / (dr) # Mstar
            halo_outflow_dict["dpdr"][i_r] += np.sum(mass_gas[ok]*cv_gas[ok])  / (dr) # Mstar kms^-1

            halo_outflow_dict["dEkdr"][i_r] += np.sum(mass_gas[ok]*np.sum(np.square(velocity_gas[ok]),axis=1))  / (dr) # Mstar km^2 s^-2
            halo_outflow_dict["dEtdr"][i_r] += np.sum(mass_gas[ok]*internal_energy_gas[ok])  / (dr) # Mstar km^2 s^-2

            halo_outflow_dict["P50mw"][i_r] += us.Weighted_Percentile(pressure_gas[ok], mass_gas[ok],0.5) # kg m^-1 s^-2 = Pa
            halo_outflow_dict["P50Vw"][i_r] += us.Weighted_Percentile(pressure_gas[ok], pseudo_volume_gas[ok],0.5) # kg m^-1 s^-2 = Pa
            halo_outflow_dict["Pmean"][i_r] += np.sum(pressure_gas[ok]*mass_gas[ok])/np.sum(mass_gas[ok]) # kg m^-1 s^-2 = Pa                                                         

            halo_outflow_dict["mshell"][i_r] += np.sum(mass_gas[ok]) # Msun
            if len(r_gas)>0:
                ok = np.argmin(abs(r_gas-rmid[i_r]*r200))
                halo_outflow_dict["agrav"][i_r] += a_grav_gas[ok] # m s^-2
                
        for vmin_i in vmin:
            halo_outflow_dict["dmdt_out_standard_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["P90fw_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dpdr_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dEkdr_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))
            halo_outflow_dict["dEtdr_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid)))

            okv =  (cv_gas>vmin_i * halo_vmax)
            for i_r in range(len(rmid)):
                ok = okv & (r_gas<rbins[i_r+1]*r200) & (r_gas>rbins[i_r]*r200)
                halo_outflow_dict["dmdt_out_standard_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok] *cv_gas[ok])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1                 
                halo_outflow_dict["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok] *np.square(cv_gas[ok]))  / (dr*r200) / kpc2km * s2yr # Mstar km s^-1 yr-1
                halo_outflow_dict["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += 0.5 * np.sum(mass_gas[ok] *cv_gas[ok] *np.sum(np.square(velocity_gas[ok]),axis=1))  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                halo_outflow_dict["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok] * internal_energy_gas[ok] *cv_gas[ok])  / (dr*r200) / kpc2km * s2yr # Mstar (kms^-1)^2 yr^-1
                halo_outflow_dict["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(cv_gas[ok], mass_gas[ok]*cv_gas[ok], 0.5) # kms^-1 (radial flux weighted)
                halo_outflow_dict["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(cv_gas[ok], mass_gas[ok]*cv_gas[ok], 0.9)

                halo_outflow_dict["P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(pressure_gas[ok], mass_gas[ok]*cv_gas[ok], 0.5) # kms^-1 (radial flux weighted)
                halo_outflow_dict["P90fw_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(pressure_gas[ok], mass_gas[ok]*cv_gas[ok], 0.9)

                halo_outflow_dict["dmdr_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok])  / (dr) # Mstar
                halo_outflow_dict["dpdr_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok]*cv_gas[ok])  / (dr) # Mstar kms^-1
                halo_outflow_dict["dEkdr_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok]*np.sum(np.square(velocity_gas[ok]),axis=1))  / (dr) # Mstar km^2 s^-2
                halo_outflow_dict["dEtdr_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok]*internal_energy_gas[ok])  / (dr) # Mstar km^2 s^-2
            

    #### Do ala Nelson directionality measurements ###
    n_theta_bins = 72
    theta_bins = np.pi * np.arange(-1, 1+2./n_theta_bins, 2./n_theta_bins)
    theta_mid = 0.5*(theta_bins[1:] + theta_bins[0:-1])

    for vmin_i in vmin:
        halo_outflow_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros((len(rmid),len(theta_mid)))
        for i_r in range(len(rmid)):

            for i_theta in range(n_theta_bins):
                ok = (r_gas<rbins[i_r+1]*r200) & (r_gas>rbins[i_r]*r200) & (cv_gas>vmin_i * halo_vmax) & (theta_gas > theta_bins[i_theta]) & (theta_gas < theta_bins[i_theta+1])
                halo_outflow_dict["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"][i_r,i_theta] = np.sum(mass_gas[ok] *cv_gas[ok])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1

    #print np.log10(halo_mchalo), len(mass_gas), len(mass_gas[in_wind]), len(mass_gas[cv_gas<0]), len(mass_gas[(cv_gas<0)&in_wind])

    if inflows:
        halo_outflow_dict["dmdt_in"] = np.zeros((len(rmid),len(theta_mid)))
        halo_outflow_dict["dmdt_in_wind"] = np.zeros((len(rmid),len(theta_mid)))
        halo_outflow_dict["dmdt_in_0p25vmax"] = np.zeros((len(rmid),len(theta_mid)))
        halo_outflow_dict["dmdt_in_wind_0p25vmax"] = np.zeros((len(rmid),len(theta_mid)))
        for i_r in range(len(rmid)):
            for i_theta in range(n_theta_bins):
                ok = (r_gas<rbins[i_r+1]*r200) & (r_gas>rbins[i_r]*r200) & (cv_gas < 0) & (theta_gas > theta_bins[i_theta]) & (theta_gas < theta_bins[i_theta+1])
                halo_outflow_dict["dmdt_in"][i_r,i_theta] =  np.sum(mass_gas[ok] *cv_gas[ok])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1

                ok2 = ok & (cv_gas < -0.25 * halo_vmax)
                halo_outflow_dict["dmdt_in_0p25vmax"][i_r,i_theta] =  np.sum(mass_gas[ok2] *cv_gas[ok2])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1

                ok_wind = ok & in_wind
                halo_outflow_dict["dmdt_in_wind"][i_r,i_theta] =  np.sum(mass_gas[ok_wind] *cv_gas[ok_wind])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1

                ok_wind2 = ok2 & in_wind
                halo_outflow_dict["dmdt_in_wind_0p25vmax"][i_r,i_theta] =  np.sum(mass_gas[ok_wind2] *cv_gas[ok_wind2])  / (dr*r200) / kpc2km * s2yr # Mstar yr^-1

    ########## Do covering fraction measurements ###############

    # Binning in solid angle (theta goes from z-axis pole (altitude), from 0 to pi, and thi (azimuth) goes around the x/y plane , from 0 to 2 pi)
    
    n_theta_bins = 10
    costheta_bins = np.arange(-1, 1+2./(n_theta_bins), 2./(n_theta_bins))[::-1]
    theta_bins = np.arccos(costheta_bins)
    dtheta = theta_bins[1:] - theta_bins[0:-1]
    theta_mid = 0.5*(theta_bins[1:]+theta_bins[0:-1])
 
    n_thi_bins = 10
    thi_bins = np.arange(-1.0, 1+1./(n_theta_bins), 1./(n_theta_bins))*np.pi
    thi_mid = 0.5*(thi_bins[1:]+thi_bins[0:-1])

    rad_momentum_gas = abs(cv_gas * mass_gas)

    for vmin_i in vmin:
        halo_outflow_dict["frac_omega_grt_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros(len(rmid))
        halo_outflow_dict["mass_grt_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros(len(rmid))
        if extra_stuff:
            halo_outflow_dict["P50fw_grt_"+str(vmin_i).replace(".","p")+"vmax"] = np.zeros(len(rmid))
            ok_p = mass_gas < 0

        for i_r in range(len(rmid)):
            exceeds = 0.0
            total = 0.0
            okr = (r_gas<rbins[i_r+1]*r200) & (r_gas>rbins[i_r]*r200)

            for i_theta in range(len(theta_mid)):
                ok_rtheta = okr & (theta_solid_gas > theta_bins[i_theta]) & (theta_solid_gas < theta_bins[i_theta+1])
                for i_thi in range(len(thi_mid)):
                    ok = ok_rtheta & (thi_solid_gas > thi_bins[i_thi]) & (thi_solid_gas < thi_bins[i_thi+1])
                    
                    # Note I'm not accounting for "SPH" here, so this is very approximate
                    if len(ok[ok]) > 0:
                        cv_gas_i = np.sum(rad_momentum_gas[ok] * cv_gas[ok]) / np.sum(rad_momentum_gas[ok])
                        total += 1 
                        if cv_gas_i > vmin_i*halo_vmax:
                            exceeds += 1
                            halo_outflow_dict["mass_grt_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += np.sum(mass_gas[ok])
                            if extra_stuff:
                                ok_p = ok_p | ok

            halo_outflow_dict["P50fw_grt_"+str(vmin_i).replace(".","p")+"vmax"][i_r] += us.Weighted_Percentile(pressure_gas[ok_p], mass_gas[ok_p]*cv_gas[ok_p], 0.5) # kms^-1 (radial flux weighted)

            if total > 0:
                halo_outflow_dict["frac_omega_grt_"+str(vmin_i).replace(".","p")+"vmax"][i_r] = exceeds/total

            '''print i_r, vmin_i
            py.figure()
            vmax = np.mean(abs(rad_momentum_gas[okr] *cv_gas[okr]))*10
            vmin = -vmax
            py.hist2d(thi_solid_gas[okr], theta_solid_gas[okr], weights=rad_momentum_gas[okr] * cv_gas[okr], vmin=vmin, vmax=vmax, cmap = "seismic")
            py.show()

        print vmin_i, halo_outflow_dict["frac_omega_grt_"+str(vmin_i).replace(".","p")+"vmax"]
    quit()'''
    

    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop]

    return halo_mass_dict, halo_outflow_dict, timing_info
