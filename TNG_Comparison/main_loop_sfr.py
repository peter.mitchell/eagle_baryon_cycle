import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, sim_props, output_names = inputs

    time_track_select = 0.0
    time_track_ptr = 0.0


    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_mass_dict = {}

    output_mass_names = output_names[0]
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ts, boxsize = sim_props
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    g2Msun = 1./(1.989e33)

    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    
    # Unpack particle data
    star_ts = part_data[0]

    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = np.copy(subhalo_props["r200_host"][i_halo])*1e3/(1+redshift_ts)/h # pkpc 
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    

    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ##### Select star particles that belong to desired group, subgroup
    select_star = mpf.Match_Index(halo_subhalo_index, star_ts.subhalo_index_unique, star_ts.particle_subhalo_index)

    # Convert to Msun
    mass_star = np.copy(mpf.Index_Array(star_ts.data["mass"],select_star))
    mass_star *= 1.989e43 * g2Msun / h

    mass_star_init = np.copy(mpf.Index_Array(star_ts.data["mass_init"],select_star))
    mass_star_init *= 1.989e43 * g2Msun / h

    coordinates_halo = [halo_x, halo_y, halo_z]
    coordinates_star = mpf.Index_Array(star_ts.data["coordinates"],select_star)
    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_star = mpf.Centre_Halo(coordinates_star, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))

    # Work out which star particles formed between now and the previous timestep
    aform_star = mpf.Index_Array(star_ts.data["aform"],select_star)
    
    t_star, junk = uc.t_Universe(aform_star, omm, h)
    age_star = (t_ts - t_star)*1e3 # Myr

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ################ Do measurements ####################

    halo_mass_dict["mass_stars_30kpc"] = np.sum(mass_star[r_star<30])
    halo_mass_dict["mass_new_stars_init_100_30kpc"] = np.sum(mass_star_init[(r_star<30)&(age_star<100)])
    
    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop]

    return halo_mass_dict, timing_info
