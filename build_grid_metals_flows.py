import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

test_mode = False
cumulative_pp = False
use_aperture = True

v50 = False

# Only include particles with a stellar mass above some value (if use apertue this stellar mass will be the 30kpc aperture mass)
# If you want no selection on stellar mass then set to a negative number
mstar_cut = -1e9
#mstar_cut = 0.0
#mstar_cut = 1.81e7 # 10 particles at standard Eagle resolution (assuming stars have starting gas mass, which they won't)

#star_mass = "n_part" # in this mode, we just measure particle numbers, not particle masses
star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

if star_mass == "n_part":
    print "In this case, I need to remove hard-coded particle mass (for recal)"
    quit()

halo_mass = "m200"

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 50 Mpc REF with 28 snaps
'''sim_name = "L0050N0752_REF_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 28 snaps
sim_name = "L0025N0376_REF_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]

# 25 Mpc REF with 28 snaps par test
'''sim_name = "L0025N0376_REF_snap_par_test"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
snap_final = 100
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/tree_100.0.hdf5"
snip_list = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
               73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
               145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
               217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
               289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
               361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_final = 200
snap_list = np.arange(1,snap_final+1)[::-1] # These trees start at 1
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]'''

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/tree_299.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 400 snapshots
'''sim_name = "L0025N0376_REF_400_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/tree_399.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 500 snapshots
#sim_name = "L0025N0376_REF_500_snap"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version"
'''sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/tree_499.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 1000 snapshots
'''sim_name = "L0025N0376_REF_1000_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/tree_999.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 50 Mpc no-AGN 28 snaps
'''sim_name = "L0050N0752_NOAGN_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.copy(snap_list)'''

# 25 Mpc - recal (high-res), 202 snipshots
'''sim_name = "L0025N0752_Recal"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/tree_202.0.hdf5"
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snip_list = np.rint(snipshots[::-1]).astype("int")
snap_list = np.rint(snapshots[::-1]).astype("int")'''

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()


omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units (AT FIDUCIAL EAGLE RESOLUTION ONLY!) - this is only used if we are using particles numbers (ala Neistein) instead of masses to circumvent stellar recycling
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1))
bin_mstar_mid = 0.5*(bin_mstar[1:]+bin_mstar[0:-1])

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()

lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

# Compute the median expansion factor for each bin
jimbo = [[]]
counter = 0
for i_snap, snap in enumerate(snap_list[:-1]):

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    if ind_bin_snap > len(jimbo)-1:
        jimbo.append([])
        
    jimbo[ind_bin_snap].append(a[i_snap])

bin_a_median = np.zeros(len(lna_mid_choose))
for n in range(len(lna_mid_choose)):
    bin_a_median[n] = np.median(jimbo[n])
print "median expansion factor for each redshift bin", bin_a_median

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

name_list = ["mchalo","subgroup_number","m200_host"]
name_list += ["mass_gas_SF", "mass_gas_NSF", "mass_star", "mass_new_stars_init"]
name_list += ["mZ_new_stars_init","mZ_gas_SF","mZ_gas_NSF"]
name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]
name_list += ["mass_gas_NSF_ISM","mZ_gas_NSF_ISM"]

name_list += ["mass_prcooled", "mass_galtran", "mass_praccreted"]
name_list += ["mass_fof_transfer"]
name_list += ["mZ_prcooled", "mZ_galtran", "mZ_praccreted","mZ_fof_transfer"]

if "snap" in sim_name:
    name_list += ["mass_dir_heat_SNe_in_ism","mZ_dir_heat_SNe_in_ism"]
    name_list += ["mO_ism", "mO_cgm", "mFe_ism", "mFe_cgm"]

fVmax_cuts = [0.25]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    name_list += ["mass_inside"+fV_str,"mass_outside_galaxy"+fV_str,"mass_outside"+fV_str]
    name_list += ["mZ_inside"+fV_str,"mZ_outside_galaxy"+fV_str,"mZ_outside"+fV_str]

    name_list += ["mass_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str,"mass_join_halo_wind"+fV_str]
    name_list += ["mass_ism_reheated_join_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str]
    name_list += ["mZ_nsf_ism_join_wind_from_SF"+fV_str, "mZ_ism_reheated_join_wind_from_SF"+fV_str]

    name_list += ["mZ_sf_ism_join_wind"+fV_str,"mZ_nsf_ism_join_wind"+fV_str,"mZ_join_halo_wind"+fV_str,"mZ_ism_reheated_join_wind"+fV_str,"mZ_halo_reheated_join_wind"+fV_str]

    name_list += [ "mass_recooled"+fV_str, "mass_reaccreted"+fV_str]
    name_list += [ "mZ_recooled"+fV_str, "mZ_reaccreted"+fV_str] # Note a non shell measurment has to go last!

    if "snap" in sim_name:
        name_list += ["mO_join_ism_wind"+fV_str, "mFe_join_ism_wind"+fV_str]
        name_list += ["mO_join_halo_wind"+fV_str, "mFe_join_halo_wind"+fV_str]

fVmax_cuts_extra = [0.125, 0.5]
for fVmax_cut in fVmax_cuts_extra:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    name_list += ["mass_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str,"mass_ism_reheated_join_wind"+fV_str]
    name_list += ["mZ_sf_ism_join_wind"+fV_str,"mZ_nsf_ism_join_wind"+fV_str,"mZ_ism_reheated_join_wind"+fV_str]

data_list = []
dt_list = []
t_list = []

for lna in lna_mid_choose:
    dt_list.append([])
    t_list.append([])
    data_list.append({})
    for name in name_list:
        data_list[-1][name] = []

f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","facc_pristine_subnorm","facc_fof_subnorm"]
f_name_list += ["fZcool_pristine_subnorm","fZcool_galtran_subnorm","fZacc_pristine_subnorm","fZacc_fof_subnorm"]
f_name_list += ["fcool_pristine_mcgm_norm","fZcool_pristine_mZcgm_norm"]
f_name_list += ["fSFR_ZSF_subSF_norm","fSFR_ZISM_subISM_norm","fZFR_subnorm"]
f_name_list += ["Zfr_Zratio"]

if "snap" in sim_name:
    f_name_list += ["ism_dir_heat_Zratio", "OFE_ratio_ism", "OFE_ratio_cgm"]

f_name_list_mstar = []

for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list += ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str,"halo_wind_tot_subnorm"+fV_str,"ism_wind_tot"+fV_str]

    f_name_list += ["ism_wind_tot_Zml"+fV_str, "halo_wind_tot_Zml"+fV_str,"ism_wind_tot_Zml_ZISM"+fV_str, "halo_wind_tot_Zml_ZISM"+fV_str,"ism_wind_tot_Zratio"+fV_str]
    f_name_list += ["ism_wind_tot_Z_subnorm"+fV_str, "halo_wind_tot_Z_subnorm"+fV_str,"ism_wind_tot_Zml_ZSFISM"+fV_str, "halo_wind_tot_Zml_ZSFISM"+fV_str ]
    f_name_list += ["ism_wind_tot_Zml_from_SF"+fV_str]
    f_name_list += ["ism_wind_tot_Zratio_ind_av"+fV_str, "halo_wind_ism_Zratio_ind_av"+fV_str, "halo_wind_cgm_Zratio_ind_av"+fV_str, "halo_wind_ism_Zratio"+fV_str, "halo_wind_cgm_Zratio"+fV_str]

    f_name_list += ["fcool_recooled_subnorm"+fV_str, "facc_reaccreted_subnorm"+fV_str, "facc_reaccreted_return"+fV_str,"fcool_recooled_return"+fV_str]
    f_name_list += ["fZcool_recooled_subnorm"+fV_str, "fZacc_reaccreted_subnorm"+fV_str, "fZacc_reaccreted_return"+fV_str,"fZcool_recooled_return"+fV_str]
    f_name_list += ["fZacc_reaccreted_return_tscaled_"+fV_str,"fZcool_recooled_return_tscaled_"+fV_str]

    f_name_list_mstar += ["ism_wind_tot_Zml"+fV_str, "ism_wind_tot_Zratio"+fV_str]

    if "snap" in sim_name:
        f_name_list += ["OFE_ratio_ism_wind"+fV_str,"OFE_ratio_halo_wind"+fV_str]

for fVmax_cut in fVmax_cuts_extra:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    f_name_list += ["ism_wind_tot_Zratio"+fV_str]

f_med_dict = {}
for f_name in f_name_list:    
    f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

f_med_dict_mstar = {}
for f_name in f_name_list_mstar:
    f_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

filename = "processed_subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    '''if i_snap > 3:
        if i_snap ==4:
            print ""
            print "temp hack"
            print ""
        continue'''

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    if name_list[-1] in subhalo_group:

        data = subhalo_group[name_list[-1]][:]

        if use_aperture:
            mstars = subhalo_group["mass_stars_30kpc"] # Msun
        else:
            mstars = subhalo_group["mass_star"] # Msun

        ok = (np.isnan(data)==False) & (mstars>mstar_cut)

        for i_name, name in enumerate(name_list):
            data = subhalo_group[name][:]
            if name == "Group_R_Crit200_host" or name == "r200_host":
                data *= a[i_snap] # pMpc h^-1
            ngal_tot = len(data)

            if len(data.shape) == 1:
                data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])
            elif len(data.shape) == 2:
                if data_list[ind_bin_snap][name] == []:
                    data_list[ind_bin_snap][name] = np.zeros((0, data.shape[1] ))
                data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok],axis=0)
            else:
                print "yikeso"
                quit()
            
        ngal = len(data[ok])
        print ngal, ngal_tot

        dt = t[i_snap] - t[i_snap+1]
        dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)

        t_list[ind_bin_snap] = np.append(t_list[ind_bin_snap],np.zeros(ngal)+t[i_snap])

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])
    else:
        print "no data for this snap"

for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]
    t_gal = t_list[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue
               

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = (data["mass_gas_NSF"]+data["mass_gas_SF"]+data["mass_star"]) >= 0
    dt = dt[ok_measure]
    t_gal = t_gal[ok_measure]
    for name in name_list:
        data[name] = data[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22

    if use_aperture:
        sfr = data["mass_new_stars_init_100_30kpc"] / 100 / 1e6 # Msun yr^-1
        mstars = data["mass_stars_30kpc"] # Msun
    else:
        sfr = data["mass_new_stars_init"] / dt * 1e-9 # Msun yr^-1
        mstars = data["mass_star"]

    ssfr = sfr / mstars * 1e9 # Gyr^-1

    mchalo = data["mchalo"]
    if halo_mass == "m200":
        print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["m200_host"]
        dm2tot_factor = 1.0 # In this case there is no need to correct dm mass to total mass

    subgroup_number = data["subgroup_number"]

    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])
        ok_mstar = (np.log10(mstars) > bin_mstar[j_bin]) & (np.log10(mstars) < bin_mstar[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)
        ok_cent_mstar = ok_mstar & (subgroup_number == 0)

        weight_cent = np.zeros_like(dt[ok_cent])
        weight_cent_mstar = np.zeros_like(dt[ok_cent_mstar])

        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point

        dt_u = np.unique(dt[ok_mstar])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_mstar] == dt_u_i
            weight_cent_mstar[ok2_cent] = dt[ok_cent_mstar][ok2_cent] / len(dt[ok_cent_mstar][ok2_cent])

        mass_new_stars = data["mass_new_stars_init"]
        mZ_new_stars = data["mZ_new_stars_init"]

        mZ_SF = data["mZ_gas_SF"]
        mZ_ISM = data["mZ_gas_SF"] + data["mZ_gas_NSF_ISM"]
        mZ_CGM = data["mZ_gas_NSF"] - data["mZ_gas_NSF_ISM"]
        mass_ISM = data["mass_gas_SF"] + data["mass_gas_NSF_ISM"]
        mass_CGM = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]

        for f_name in f_name_list:
            
            for fVmax_cut in fVmax_cuts:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                if fV_str in f_name:
                    break

            for fVmax_cut in fVmax_cuts_extra:
                fV_str_extra = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                if fV_str_extra in f_name:
                    fV_str = fV_str_extra
                    break

            if f_name == "ism_wind_tot_ml"+fV_str or f_name == "ism_wind_tot"+fV_str or f_name == "ism_wind_tot_subnorm"+fV_str:
                mass_out = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
            elif f_name == "ism_wind_tot_Zml"+fV_str or "ism_wind_tot_Z_subnorm" in f_name:
                mass_out = data["mZ_sf_ism_join_wind"+fV_str] + data["mZ_nsf_ism_join_wind"+fV_str] + data["mZ_ism_reheated_join_wind"+fV_str]
            elif f_name == "ism_wind_tot_Zml_from_SF"+fV_str:
                mass_out = data["mZ_sf_ism_join_wind"+fV_str] + data["mZ_nsf_ism_join_wind_from_SF"+fV_str] + data["mZ_ism_reheated_join_wind_from_SF"+fV_str]
            elif f_name == "ism_wind_tot_Zml_ZSFISM"+fV_str:
                mass_out = (data["mZ_sf_ism_join_wind"+fV_str] + data["mZ_nsf_ism_join_wind"+fV_str] + data["mZ_ism_reheated_join_wind"+fV_str]) * data["mass_gas_SF"]
            elif f_name == "ism_wind_tot_Zml_ZISM"+fV_str or f_name == "ism_wind_tot_Zratio"+fV_str:
                mass_out = (data["mZ_sf_ism_join_wind"+fV_str] + data["mZ_nsf_ism_join_wind"+fV_str] + data["mZ_ism_reheated_join_wind"+fV_str]) * (data["mass_gas_SF"]+data["mass_gas_NSF_ISM"])

            elif f_name == "halo_wind_tot_ml"+fV_str or f_name == "halo_wind_tot_subnorm"+fV_str:
                mass_out = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
            elif f_name == "halo_wind_tot_Zml"+fV_str or "halo_wind_tot_Z_subnorm" in f_name:
                mass_out = data["mZ_join_halo_wind"+fV_str] + data["mZ_halo_reheated_join_wind"+fV_str]
            elif f_name == "halo_wind_tot_Zml_ZSFISM"+fV_str:
                mass_out = (data["mZ_join_halo_wind"+fV_str] + data["mZ_halo_reheated_join_wind"+fV_str]) * data["mass_gas_SF"]
            elif f_name == "halo_wind_tot_Zml_ZISM"+fV_str or f_name == "halo_wind_ism_Zratio"+fV_str:
                mass_out = (data["mZ_join_halo_wind"+fV_str] + data["mZ_halo_reheated_join_wind"+fV_str]) * (data["mass_gas_SF"]+data["mass_gas_NSF_ISM"])
            elif f_name == "halo_wind_cgm_Zratio"+fV_str:
                mass_out = (data["mZ_join_halo_wind"+fV_str] + data["mZ_halo_reheated_join_wind"+fV_str]) * (data["mass_gas_NSF"]-data["mass_gas_NSF_ISM"])

            elif f_name == "ism_dir_heat_Zratio":
                mass_out = data["mZ_dir_heat_SNe_in_ism"] * (data["mass_gas_SF"]+data["mass_gas_NSF_ISM"])
            elif f_name == "Zfr_Zratio":
                mass_out = mZ_new_stars * (data["mass_gas_SF"]+data["mass_gas_NSF_ISM"])
                
            elif f_name == "OFE_ratio_ism_wind"+fV_str:
                mass_out = data["mO_join_ism_wind"+fV_str]
            elif f_name == "OFE_ratio_halo_wind"+fV_str:
                mass_out = data["mO_join_halo_wind"+fV_str]
            elif f_name == "OFE_ratio_ism":
                mass_out = data["mO_ism"]
            elif f_name == "OFE_ratio_cgm":
                mass_out = data["mO_cgm"]

            elif f_name == "fSFR_ZSF_subSF_norm":
                mass_out = mass_new_stars * mZ_SF
            elif f_name == "fSFR_ZISM_subISM_norm":
                mass_out = mass_new_stars * mZ_ISM
            elif f_name == "fZFR_subnorm":
                mass_out = mZ_new_stars

            elif "fcool_pristine" in f_name:
                mass_out = data["mass_prcooled"]
            elif "fcool_recooled" in f_name:
                mass_out = data["mass_recooled"+fV_str]
            elif "fcool_galtran" in f_name:
                mass_out = data["mass_galtran"]
            elif "facc_pristine" in f_name:
                mass_out = data["mass_praccreted"]
            elif "facc_reaccreted" in f_name:
                mass_out = data["mass_reaccreted"+fV_str]
            elif "facc_fof" in f_name:
                mass_out = data["mass_fof_transfer"]
            elif "fZcool_pristine" in f_name:
                mass_out = data["mZ_prcooled"]
            elif "fZcool_recooled" in f_name:
                mass_out = data["mZ_recooled"+fV_str]
            elif "fZcool_galtran" in f_name:
                mass_out = data["mZ_galtran"]
            elif "fZacc_pristine" in f_name:
                mass_out = data["mZ_praccreted"]
            elif "fZacc_reaccreted" in f_name:
                mass_out = data["mZ_reaccreted"+fV_str]
            elif "fZacc_fof" in f_name:
                mass_out = data["mZ_fof_transfer"]
            else:
                if f_name != "aform_bh" and f_name != "ism_wind_cum_ml" and "_injection_rate" not in f_name and f_name != "f_dir_heated_mass_in_ISM" and "_out" not in f_name and "ind_av" not in f_name:
                    print "No", f_name
                    quit()

        
            if "subnorm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor*fb*mchalo[ok_cent]*weight_cent) # Gyr^-1

            elif "subSF_norm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor*fb*mchalo[ok_cent]*weight_cent*data["mass_gas_SF"][ok_cent]) # Gyr^-1
            elif "subISM_norm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor*fb*mchalo[ok_cent]*weight_cent*(data["mass_gas_SF"]+data["mass_gas_NSF_ISM"])[ok_cent]) # Gyr^-1

            elif "mcgm_norm" in f_name:
                mass_cgm = data["mass_gas_NSF"] - data["mass_gas_NSF_ISM"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm[ok_cent]*weight_cent) # Gyr^-1

            elif "mZcgm_norm" in f_name:
                mass_cgm = data["mZ_gas_NSF"] - data["mZ_gas_NSF_ISM"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_cgm[ok_cent]*weight_cent) # Gyr^-1
            
            elif "_return" in f_name:
                if "fcool" in f_name:
                    norm = np.copy(data["mass_outside_galaxy"+fV_str])
                elif "fZcool" in f_name:
                    norm = np.copy(data["mZ_outside_galaxy"+fV_str])
                elif "facc" in f_name:
                    norm = np.copy(data["mass_outside"+fV_str])
                elif "fZacc" in f_name:
                    norm = np.copy(data["mZ_outside"+fV_str])
                else:
                    print "yikes", f_name
                    quit()

                if "tscaled" in f_name:
                    norm *= 1./t_gal # Msun Gyr^-1

                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(norm[ok_cent]*weight_cent) # Gyr^-1
            
            elif f_name == "ism_wind_tot"+fV_str:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(weight_cent) *1e-9# Msun yr ^-1

            elif f_name == "ism_wind_tot_Zratio"+fV_str:
                temp = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_ISM[ok_cent] * temp[ok_cent] /dt[ok_cent] * weight_cent)# Dimensionless
                if "0p25" in fV_str:
                    f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) / np.sum(mZ_ISM[ok_cent_mstar] * temp[ok_cent_mstar] /dt[ok_cent_mstar] * weight_cent_mstar)# Dimensionless

            elif f_name == "ism_wind_tot_Zratio_ind_av"+fV_str:
                mwind = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
                mZwind = data["mZ_sf_ism_join_wind"+fV_str] + data["mZ_nsf_ism_join_wind"+fV_str] + data["mZ_ism_reheated_join_wind"+fV_str]
                Zism_mean = np.sum(mZ_ISM[ok_cent] * weight_cent) / np.sum(mass_ISM[ok_cent] * weight_cent)
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mZwind[ok_cent]*weight_cent)/np.sum(mwind[ok_cent]*weight_cent) / Zism_mean

            elif f_name == "halo_wind_ism_Zratio"+fV_str:
                temp = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_ISM[ok_cent] * temp[ok_cent] /dt[ok_cent] * weight_cent)# Dimensionless

            elif f_name == "halo_wind_cgm_Zratio"+fV_str:
                temp = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_CGM[ok_cent] * temp[ok_cent] /dt[ok_cent] * weight_cent)# Dimensionless

            elif f_name == "halo_wind_ism_Zratio_ind_av"+fV_str:
                mwind = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
                mZwind = data["mZ_join_halo_wind"+fV_str] + data["mZ_halo_reheated_join_wind"+fV_str]
                Zism_mean = np.sum(mZ_ISM[ok_cent] * weight_cent) / np.sum(mass_ISM[ok_cent] * weight_cent)
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mZwind[ok_cent]*weight_cent)/np.sum(mwind[ok_cent]*weight_cent) / Zism_mean


            elif f_name == "halo_wind_cgm_Zratio_ind_av"+fV_str:
                mwind = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
                mZwind = data["mZ_join_halo_wind"+fV_str] + data["mZ_halo_reheated_join_wind"+fV_str]
                Zcgm_mean = np.sum(mZ_CGM[ok_cent] * weight_cent) / np.sum(mass_CGM[ok_cent] * weight_cent)
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mZwind[ok_cent]*weight_cent)/np.sum(mwind[ok_cent]*weight_cent) / Zcgm_mean

            elif f_name == "ism_dir_heat_Zratio":
                temp = data["mass_dir_heat_SNe_in_ism"]
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_ISM[ok_cent] * temp[ok_cent] /dt[ok_cent] * weight_cent)# Dimensionless

            elif f_name == "Zfr_Zratio":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_ISM[ok_cent] * mass_new_stars[ok_cent] /dt[ok_cent] * weight_cent)

            elif f_name == "OFE_ratio_ism_wind"+fV_str:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(data["mFe_join_ism_wind"+fV_str][ok_cent] * weight_cent)
            elif f_name == "OFE_ratio_halo_wind"+fV_str:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(data["mFe_join_halo_wind"+fV_str][ok_cent] * weight_cent)
            elif f_name == "OFE_ratio_ism":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(data["mFe_ism"][ok_cent] * weight_cent)
            elif f_name == "OFE_ratio_cgm":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(data["mFe_cgm"][ok_cent] * weight_cent)
            

            elif "_ml" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_new_stars[ok_cent]/dt[ok_cent]*weight_cent) # Dimensionless

 
            elif "_Zml_ZISM" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_ISM[ok_cent]*mass_new_stars[ok_cent]/dt[ok_cent]*weight_cent) # Dimensionless
                
            elif "_Zml_ZSFISM" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_SF[ok_cent]*mass_new_stars[ok_cent]/dt[ok_cent]*weight_cent) # Dimensionless

            elif "_Zml" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mZ_new_stars[ok_cent]/dt[ok_cent]*weight_cent) # Dimensionless
                if "ism" in f_name and "from_SF" not in f_name:
                    f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) / np.sum(mZ_new_stars[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) # Dimensionless
            else:

                print "nope", f_name
                quit()

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_metal_flows"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_mstar_grid", data=bin_mstar_mid)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("a_median", data=bin_a_median)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

for f_name in f_name_list:

    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

for f_name in f_name_list_mstar:
    File_grid.create_dataset(f_name+"_mstar", data=f_med_dict_mstar[f_name])

File_grid.close()
