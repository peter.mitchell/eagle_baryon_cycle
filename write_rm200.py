import numpy as np
import h5py
import os
import warnings
import match_searchsorted as ms
from os import listdir

# 100 Mpc REF with 50 snips
'''sim_name = "L0100N1504_REF_50_snip"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/"
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,
                98, 106, 114, 122, 130, 138, 146, 154, 164, 172, 180, 188,
                196, 204, 212, 220, 228, 236, 244, 252, 260, 268, 276, 284,
                292, 300, 308, 318, 326, 334, 342, 350, 358, 366, 374, 382,
                390, 398, 405][::-1]'''

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0100N1504/PE/REFERENCE/data/"
snap_final = 200
snipshots, snapshots = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")'''

# 50 Mpc reference run, 28 snapshots
'''sim_name = "L0050N0752_REF_snap"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/REFERENCE/data/"
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)'''

# 25 Mpc reference run, 28 snapshots
'''sim_name = "L0025N0376_REF_snap"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE/data/"
snap_final = 28
snapshots = np.arange(1,snap_final+1)[::-1]
snipshots = np.copy(snapshots)'''

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
snap_final = 100
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
               73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
               145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
               217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
               289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
               361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
snap_final = 200
snapshots = np.arange(1,snap_final+1)[::-1] # This one starts at one for some reason
snipshots = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]'''

# 25 Mpc REF with 300 snaps
'''sim_name = "L0025N0376_REF_300_snap"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
snap_final = 299
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")'''

# 25 Mpc REF with 400 snaps
#sim_name = "L0025N0376_REF_400_snap"
#data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#snap_final = 399
#snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
#snipshots = np.rint(snipshots[::-1]).astype("int")
#snapshots = np.rint(snapshots[::-1]).astype("int")

# 25 Mpc REF with 500 snaps
'''sim_name = "L0025N0376_REF_500_snap"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
snap_final = 499
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")'''

# 25 Mpc REF with 1000 snaps
sim_name = "L0025N0376_REF_1000_snap"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
snap_final = 999
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")

# 50 Mpc REF with 28 snips
'''sim_name = "L0050N0752_CFB_snap"
h=0.6777
box_size = 50 * h
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 50mpc box"'''

# 50 Mpc model with no AGN, 28 snapshots
'''sim_name = "L0050N0752_NOAGN_snap"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0050N0752/PE/EagleVariation_NOAGN/data/"
snap_final = 28
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = np.copy(snapshots)'''

# 25 Mpc - recal (high-res), 202 snipshots
'''sim_name = "L0025N0752_Recal"
data_path = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0752/PE/RECALIBRATED/data/"
snap_final = 202
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")'''

if len(snapshots)>len(snipshots):
    snapshots = snapshots[0:-1]
if len(snapshots)>len(snipshots):
    print "Error, len(snapshots) != len(snipshots)"
    quit()

# Choose which catalogue file to read
#input_path =       "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path =       "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

print "Reading/writing to", input_path+filename

File = h5py.File(input_path+filename)
warnings.filterwarnings('ignore')

snip_number_groups = []
redshift_groups = []
for direc in listdir(data_path):
    if not "snap" in sim_name:
        if not "groups_snip_" in direc:
            continue
    else:
        if not "groups_" in direc:            
            continue
        else:
            if "snip" in direc:
                continue

    if ".tar" in direc:
        continue

    #print direc
    #print direc[7:10], direc[12:]

    
    if "snap" in sim_name:
        snip_number_groups.append(int(direc[7:10]))
        redshift_groups.append(float(direc[12:19].replace("p",".")))
    else:
        snip_number_groups.append(int(direc[12:15]))
        redshift_groups.append(float(direc[17:].replace("p",".")))

for i_snap in range(len(snapshots)):

    snip = snipshots[i_snap]
    snap = snapshots[i_snap]
    if i_snap > 0:
        snap_ns = snapshots[i_snap-1]

    #if snip != 9:
    #    print "skip"
    #    continue

    print "reading/writing values for tree snapshot", snap
    
    # Read Eagle subfind/fof groups data
    z_group = redshift_groups[np.argmin(abs(np.array(snip_number_groups)-snip))]
    snip_z_path = str(snip).zfill(3)+"_z" + ("%3.3f"%z_group).zfill(7).replace(".","p")

    if "snap" in sim_name: 
        snip_path = data_path+"groups_"+ snip_z_path +"/"
    else:
        snip_path = data_path+"groups_snip_"+ snip_z_path +"/"

    nsubvol = 0
    for file_name in listdir(snip_path):
        if not "eagle_sub" in file_name:
            continue
        nsubvol += 1

    subfind_names = ["GroupNumber","SubGroupNumber"]
    dtypes = ["int","int"]
    subfind_data = {}
    for name,dtype in zip(subfind_names,dtypes):
        subfind_data[name] = np.array([]).astype(dtype)

    fof_names = ["Group_R_Crit200", "Group_M_Crit200"]
    dtypes = ["float","float"]
    fof_data = {}
    for name,dtype in zip(fof_names,dtypes):
        fof_data[name] = np.array([]).astype(dtype)

    for i_sub in range(nsubvol):
        #if i_sub > 0:
        #    print "testing skip"
        #    continue
        if "snap" in sim_name:
            subvol_path = snip_path+"eagle_subfind_tab_"+snip_z_path+"."+str(i_sub)+".hdf5"
        else:
            subvol_path = snip_path+"eagle_subfind_snip_tab_"+snip_z_path+"."+str(i_sub)+".hdf5"

        subvol_file = h5py.File(subvol_path)

        print "reading", i_sub

        try:
            for name in fof_names:
                data = subvol_file["FOF/"+name][:]
                fof_data[name] = np.append(fof_data[name], data)

                for name in subfind_names:
                    data = subvol_file["Subhalo/"+name][:]
                    subfind_data[name] = np.append(subfind_data[name], data)
            subvol_file.close()

        except:
            print "couldn't read data for subvolume", i_sub
            subvol_file.close()
            continue

    # Link FoF to subfind groups
    for name in fof_names:
        subfind_data[name+"_host"] = fof_data[name][subfind_data["GroupNumber"]-1]

    order = np.argsort(subfind_data["GroupNumber"])
    
    # Read my catalogue data
    snapnum_group = File["Snap_"+str(snap)]

    subhalo_group = snapnum_group["subhalo_properties"]
    noprogen_group = snapnum_group["subhalo_no_progenitor"]

    if not "isInterpolated" in subhalo_group:
        do_ts = False
    else:
        do_ts = True

    if do_ts:
        isInterpolated_subhalo = subhalo_group["isInterpolated"][:]
        group_number_subhalo = subhalo_group["GroupNumber_hydro"][:]
        subgroup_number_subhalo = subhalo_group["SubGroupNumber_hydro"][:]

        group_number_noprogen = noprogen_group["GroupNumber_hydro"][:]
        subgroup_number_noprogen = noprogen_group["SubGroupNumber_hydro"][:]

    if i_snap > 0:
        snapnum_group_ns = File["Snap_"+str(snap_ns)]

        progenitor_group = snapnum_group_ns["main_progenitors"]
        all_progenitor_group = snapnum_group_ns["all_subhalo_ns"]

        isInterpolated_progenitor = progenitor_group["isInterpolated"][:]
        group_number_progenitor = progenitor_group["GroupNumber_hydro"][:]
        subgroup_number_progenitor = progenitor_group["SubGroupNumber_hydro"][:]

        isInterpolated_all_progenitor = all_progenitor_group["isInterpolated"][:]
        group_number_all_progenitor = all_progenitor_group["GroupNumber"][:]
        subgroup_number_all_progenitor = all_progenitor_group["SubGroupNumber"][:]
    
    # Match the arrays
    nsep = max(len(str(int(subfind_data["SubGroupNumber"].max()))) +1 ,6)

    cat_subfind = subfind_data["GroupNumber"] * 10**nsep + subfind_data["SubGroupNumber"]

    if do_ts:
        cat_subhalo = group_number_subhalo *10**nsep + subgroup_number_subhalo
        ptr_sub = ms.match(cat_subhalo, cat_subfind)
        ok_sub = (ptr_sub >= 0) & (isInterpolated_subhalo==0)

        cat_noprogen = group_number_noprogen *10**nsep + subgroup_number_noprogen
        ptr_no = ms.match(cat_noprogen, cat_subfind)
        ok_no = (ptr_no >= 0)

    if i_snap > 0:
        cat_progenitor = group_number_progenitor *10**nsep + subgroup_number_progenitor
        ptr_prog = ms.match(cat_progenitor, cat_subfind)
        ok_prog = (ptr_prog >= 0) & (isInterpolated_progenitor==0)

        cat_all_progenitor = group_number_all_progenitor *10**nsep + subgroup_number_all_progenitor
        ptr_all = ms.match(cat_all_progenitor, cat_subfind)
        ok_all = (ptr_all >= 0) & (isInterpolated_all_progenitor==0)

    # Write data to disk
    
    for name,dtype in zip(fof_names,dtypes):
        name += "_host"

        # Delete any pre-existing data
        for dset in subhalo_group:
            if name == dset:
                del subhalo_group[name]

        for dset in noprogen_group:
            if name == dset:
                del noprogen_group[name]

        if i_snap > 0:
            for dset in progenitor_group:
                if name == dset:
                    del progenitor_group[name]

            for dset in all_progenitor_group:
                if name == dset:
                    del all_progenitor_group[name]

        # write the data
        if do_ts:
            data_subhalo = np.zeros(len(group_number_subhalo)).astype(dtype) + np.nan
            data_subhalo[ok_sub] = subfind_data[name][ptr_sub][ok_sub]
            subhalo_group.create_dataset(name, data=data_subhalo)

            data_noprogen = np.zeros(len(group_number_noprogen)).astype(dtype) + np.nan
            data_noprogen[ok_no] = subfind_data[name][ptr_no][ok_no]
            noprogen_group.create_dataset(name, data=data_noprogen)

        if i_snap>0:
            data_progenitor = np.zeros(len(group_number_progenitor)).astype(dtype) + np.nan
            data_progenitor[ok_prog] = subfind_data[name][ptr_prog][ok_prog]
            progenitor_group.create_dataset(name, data=data_progenitor)
            
            data_all_progenitor = np.zeros(len(group_number_all_progenitor)).astype(dtype) + np.nan
            data_all_progenitor[ok_all] = subfind_data[name][ptr_all][ok_all]
            all_progenitor_group.create_dataset(name, data=data_all_progenitor)


File.close()
