import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

#cat_format = "old"
cat_format = "trim"

test_mode = False
subsample = False
n_per_bin = 1000
dark_matter = True
extra_wind_measurements = False
extra_accretion_measurements = False
rmax_measurements = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 2'''

# 50 Mpc REF with 28 snaps
'''sim_name = "L0050N0752_REF_snap"
nsub1d = 1
snip_list = np.arange(0,29)[::-1]
snap_list = np.arange(0,29)[::-1]'''

# 25 Mpc Ref with 28 snaps
'''sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_snap_par_test"
#sim_name = "L0025N0376_REF_snap_par_test2"
snip_list = np.arange(0,29)[::-1]
snap_list = np.arange(0,29)[::-1]
nsub1d = 1'''


# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]
snap_list = np.arange(0,51)[::-1]
nsub1d = 1'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
snap_list = np.arange(0,101)[::-1]
snip_list = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
               73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
               145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
               217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
               289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
               361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]
nsub1d = 1'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip_not_bound_only"
#sim_name = "L0025N0376_REF_200_snip_track_star"
#sim_name = "L0025N0376_REF_200_snip_smthr_abshi"
#sim_name = "L0025N0376_REF_200_snip_m200_smthr_abshi"
#sim_name = "L0025N0376_REF_200_snip_smthr_frac"
snap_list = np.arange(1,201)[::-1] # starts from 1 for these trees
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub1d = 1'''

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 1'''

# 25 Mpc ref 400 snapshots
#sim_name = "L0025N0376_REF_400_snap"
#snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
#snip_list = np.rint(snip_list[::-1]).astype("int")
#snap_list = np.rint(snap_list[::-1]).astype("int")

# 25 Mpc ref 500 snapshots
'''sim_name = "L0025N0376_REF_500_snap"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 1'''

# 25 Mpc ref 1000 snapshots
#sim_name = "L0025N0376_REF_1000_snap"
#snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
#snip_list = np.rint(snip_list[::-1]).astype("int")
#snap_list = np.rint(snap_list[::-1]).astype("int")
#nsub1d = 1

# 50 Mpc constant fb model with 28 snaps
#sim_name = "L0050N0752_CFB_snap"
#snip_list = np.arange(0,29)[::-1]
#snap_list = np.arange(0,29)[::-1]

# 50 Mpc model with no AGN, 28 snapshots
'''sim_name = "L0050N0752_NOAGN_snap"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.copy(snap_list)
nsub1d = 1'''

# 25 Mpc - recal (high-res), 202 snipshots
sim_name = "L0025N0752_Recal"
#sim_name = "L0025N0752_Recal_m200_smthr_abslo"
#sim_name = "L0025N0752_Recal_m200_smthr_abshi"
snap_final = 202
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snip_list = np.rint(snipshots[::-1]).astype("int")
snap_list = np.rint(snapshots[::-1]).astype("int")
nsub1d = 1

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if "L0025" in sim_name:
        subvol_choose = [36]
        if "500" in sim_name:
            subvol_choose = [36,499,500,501,525]
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = [2000]
    else:
        print "err"
        quit()


output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)
    if subsample:
        output_base += "_subsample_"+str(n_per_bin)
    output_base += "_test_mode/"

input_path += "Processed_Catalogues/"

subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host","isInterpolated"]

subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]

if cat_format == "trim":
    subhalo_prop_names += ["group_number_progen", "subgroup_number_progen", "node_index_progen"]

name_list = ["mass_gas_SF", "mass_gas_NSF", "mass_star", "mass_star_init", "mass_new_stars","mass_new_stars_init"]
name_list += ["mass_bh", "aform_bh","mass_acc_bh"]
name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]
name_list += ["mZ_new_stars_100_30kpc", "mZ_stars_30kpc", "mZ_gas_SF", "mZ_gas_NSF", "mZ_gas_NSF_ISM", "mZ_star", "mZ_new_stars", "mZ_outside","mZ_outside_galaxy","mZ_injected", "mZ_injected_ism"]
name_list += ["mZ_new_stars_init"]
name_list += ["mass_stellar_rec"]

name_list += ["mass_halo_reheated","mass_gas_NSF_ISM"]
name_list += ["mass_nsf_ism_reheated", "mass_sf_ism_reheated"]

name_list += ["mass_join_halo_wind_from_ism","mZ_join_halo_wind_from_ism"]
name_list += ["time_join_halo_wind_from_ism_first_time","mass_join_halo_wind_from_ism_first_time"]
name_list += ["mass_ism_wind_cumul","mass_ism_to_halo_wind_cumul"]

name_list += ["mass_hmerged","mass_gmerged_ISM","mass_gmerged_CGM"]
name_list += ["mass_prcooled", "mass_galtran", "mass_praccreted"]
name_list += ["mass_fof_transfer","mass_reaccreted_hreheat","mass_recooled_ireheat"]
name_list += ["mZ_prcooled", "mZ_galtran", "mZ_praccreted", "mZ_fof_transfer"]
name_list += ["mass_galtran_from_SF", "mass_prcooled_SF"]
name_list += ["mZ_galtran_from_SF", "mZ_prcooled_SF"]

name_list += ["mass_new_stars_init_prcooled", "mass_new_stars_init_galtran", "mass_new_stars_init_recooled", "mass_new_stars_init_merge", "mass_new_stars_init_recooled_from_mp"]
name_list += ["mZ_new_stars_init_prcooled", "mZ_new_stars_init_galtran", "mZ_new_stars_init_recooled","mZ_new_stars_init_merge"]
name_list += ["mass_smacc_sat"]

name_list += ["mass_cgm_pristine", "mass_cgm_pristine_TLo", "mass_cgm_pristine_Router"]

name_list += ["mZ_cgm_pristine", "mass_cgm_galtran", "mZ_cgm_galtran"]
name_list += ["mass_ej_galtran", "mZ_ej_galtran"]
name_list += ["mass_cgm_galtran_TLo", "mass_cgm_galtran_Router"]

if dark_matter:
    name_list += ["mass_diffuse_dm_accretion", "mass_hmerged_dm"]
    name_list += ["mass_diffuse_dm_pristine"]
name_list += ["mass_praccreted_TLo", "mass_fof_transfer_TLo"]

if "snap" in sim_name:
    name_list += ["fb_e_times_mass_new_star","mass_dir_heat_in_ism","mass_dir_heat_in_nism"]
    name_list += ["mZ_dir_heat_in_ism","mZ_dir_heat_in_nism"]
    name_list += ["mZ_dir_heat_SNe_in_ism","mZ_dir_heat_SNe_in_nism","mass_dir_heat_SNe_in_ism","mass_dir_heat_SNe_in_nism"]
    name_list += ["mO_ism", "mO_cgm", "mFe_ism", "mFe_cgm"]

if extra_wind_measurements:
    name_list += ["P50mw","Pmean","a_grav","m_shell"]

    if "snap" in sim_name:
        name_list += ["dEdt_SNe","dEdt_AGN","dEdt_SNe_sat", "dEdt_AGN_sat"]

    vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
    for vmin_i in vmin:

        name_list += ["P50mw_out_"+str(vmin_i).replace(".","p")+"vmax","P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

        name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

        name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
    
name_list += ["mass_t_ejecta","mass_t_wind","mass_t_ejecta_ret","mass_t_wind_ret"]

fVmax_cuts = [0.125, 0.25, 0.5]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    name_list += ["n_inside"+fV_str,"mass_inside"+fV_str,"mZ_inside"+fV_str,"mZ_outside"+fV_str,"mZ_outside_galaxy"+fV_str]
    name_list += ["n_outside"+fV_str,"n_outside_galaxy"+fV_str,"mass_outside_galaxy"+fV_str,"mass_outside"+fV_str]
    name_list += ["mass_cgm_wind_Router"+fV_str, "mass_cgm_wind_TLo"+fV_str]

    name_list += ["mass_ej_wind"+fV_str, "mZ_ej_wind"+fV_str, "mass_hreheat"+fV_str, "mZ_hreheat"+fV_str]
    name_list += ["mass_cgm_ireheat"+fV_str, "mZ_cgm_ireheat"+fV_str]
    name_list += ["mass_cgm_ireheat_TLo"+fV_str, "mass_cgm_ireheat_Router"+fV_str]

    name_list += ["mass_sf_ism_join_wind"+fV_str,"n_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str,"n_nsf_ism_join_wind"+fV_str,"mass_join_halo_wind"+fV_str,"n_join_halo_wind"+fV_str]
    name_list += ["mass_ism_reheated_join_wind"+fV_str,"n_ism_reheated_join_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str,"n_halo_reheated_join_wind"+fV_str]
    name_list += ["mZ_sf_ism_join_wind"+fV_str,"mZ_nsf_ism_join_wind"+fV_str,"mZ_join_halo_wind"+fV_str,"mZ_ism_reheated_join_wind"+fV_str,"mZ_halo_reheated_join_wind"+fV_str]
    name_list += ["mass_nsf_ism_join_wind_from_SF"+fV_str, "mZ_nsf_ism_join_wind_from_SF"+fV_str, "mass_ism_reheated_join_wind_from_SF"+fV_str, "mZ_ism_reheated_join_wind_from_SF"+fV_str]

    name_list += [ "mass_recooled"+fV_str, "mass_reaccreted"+fV_str, "mZ_recooled"+fV_str,  "mZ_reaccreted"+fV_str, "mass_recooled_from_SF"+fV_str, "mZ_recooled_from_SF"+fV_str]

    name_list += [ "mass_recooled_from_mp"+fV_str, "mass_reaccreted_from_mp"+fV_str, "mZ_recooled_from_mp"+fV_str,  "mZ_reaccreted_from_mp"+fV_str]
    #name_list += ["mass_reaccreted_from_wind"+fV_str, "mZ_reaccreted_from_wind"+fV_str]
    name_list += ["mass_reaccreted_TLo"+fV_str, "mass_reaccreted_TLo_from_mp"+fV_str]

    if "snap" in sim_name:
        name_list += ["mass_join_ism_wind_dir_heat"+fV_str, "mass_join_halo_wind_dir_heat"+fV_str]
        name_list += ["mO_join_ism_wind"+fV_str, "mFe_join_ism_wind"+fV_str, "mO_join_halo_wind"+fV_str,"mFe_join_halo_wind"+fV_str]

    if extra_wind_measurements:
        for vmin_i in vmin:

            name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

            name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

if extra_accretion_measurements:
    name_list += ["mass_galtran_mchalo_ratio", "mass_fof_transfer_mchalo_ratio"]
    if rmax_measurements:
        name_list += ["n_r_wind", "n_r_ejecta","n_r_wind_ret","n_r_ejecta_ret"]
        
# File to consolidate all processed galaxies within subvols into
filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
#filename += "_temp_hack"
#print ""
#print "TEMP HACK"
#print ""
filename += ".hdf5"

File = h5py.File(input_path+filename,"a")

for snip, snap in zip(snip_list,snap_list):

    print "consolidating", snap, snip

    #if snip > 300:
    #    print "skip"
    #    continue

    output_path = output_base + "Snip_"+str(snip)+"/"

    snap_group_str = "Snap_"+str(snap)
    if snap_group_str not in File:
        File.create_group(snap_group_str)
    snap_group = File[snap_group_str]

    subhalo_group_str = "subhalo_properties"
    if subhalo_group_str not in snap_group:
        snap_group.create_group(subhalo_group_str)
    subhalo_group = snap_group[subhalo_group_str]

    sub_input_list = []
    input_list = []
    for name in name_list:
        input_list.append([])   
    for name in subhalo_prop_names:
        sub_input_list.append([])

    # Find all the subvolume files (they won't be sorted in order)
    subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

    if len(subvols) == 0:
        print "There are no subvolume files. Run the pipeline first"
        File.close()
        quit()

    # Gather together measurements
    if extra_wind_measurements:
        extra_read = True
    else:
        extra_read = False
    for n, subvol in enumerate(subvols):

        if test_mode:
            skip = True
            for subvol_i in subvol_choose:
                if "_"+str(subvol_i)+".hdf5" in subvol:
                    skip = False
            if skip:
                continue

        try:
            File_subvol = h5py.File(subvol,"r")
        except:
            print "Couldn't read subvol file", subvol, "moving on"
            continue

        print "Reading", subvol

        try:
            if cat_format == "old":
                halo_group = File_subvol["subhalo_data"]
            else:
                halo_group = File_subvol["Measurements"]
        except:
            print "Read failed, skipping this subvol"
            continue

        if extra_read:
            extra_names = ["shell_rmid"]
            if "mass_t_ejecta" in name_list:
                extra_names.append("tret_bins")
            extra_data = []

            try:
                for name in extra_names:
                    extra_data.append( halo_group[name][:])

                extra_read = False
            except:
                pass

        if name_list[-1] in halo_group and name_list[0] in halo_group:

            # Choose which subhaloes to store
            # Old format refers to where we output everything. To save space we can cut un-needed subhaloes here
            # In the new format subhaloes are cut earlier on - so we don't need to do anything here
            if cat_format == "old":
                for name in name_list:
                    if name == "mass_gas_SF":
                        var1 = halo_group[name][:]
                    if name == "mass_gas_NSF":
                        var2 = halo_group[name][:]
                    if name == "mass_star":
                        var3 = halo_group[name][:]
                for name in subhalo_prop_names:
                    if name == "isInterpolated":
                        iI = halo_group[name][:]

                ok = ((np.isnan(var1)==False) & (var1 + var2 + var3 > 0)) | (iI==1)

            # Check all the required variables are available. If not find out which ones and inform the user.
            problem = []
            for name in name_list:
                if name not in halo_group:
                    problem.append(name)
            if len(problem) > 0:
                print "Error: The following variables are not in the output files"
                print problem
                quit()

            for i_name, name in enumerate(name_list):

                if cat_format == "old":
                    var = halo_group[name][:][ok]
                else:
                    var = halo_group[name][:]

                # Deal with 2d arrays and set data types correctly
                if input_list[i_name] == [] and len(var.shape) ==2:
                    input_list[i_name] = np.zeros((0,var.shape[1])).astype(var.dtype)
                elif len(input_list[i_name]) == 0:
                    input_list[i_name] = np.array([]).astype(var.dtype)

                input_list[i_name] = np.append(input_list[i_name], var,axis=0)

            for i_name, name in enumerate(subhalo_prop_names):

                if cat_format == "old":
                    var = halo_group[name][:][ok]
                else:
                    var = halo_group[name][:]

                if len(sub_input_list[i_name]) == 0:
                    sub_input_list[i_name] = np.array([]).astype(var.dtype)

                sub_input_list[i_name] = np.append(sub_input_list[i_name], var)

        else:
            print "subvol",n,"didn't contain any haloes"

        File_subvol.close()

    if len(input_list[0]) == 0:
        print "no measurements for this snipshot, moving on"
        continue

    # Delete previous in the processed catalogue file
    for name in name_list:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]

    for name in subhalo_prop_names:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]

    if extra_read:
        for name in extra_names:
            for thing in subhalo_group:
                if thing == name:
                    del subhalo_group[name]

    # Write measurements to catalogue file
    for i_name, name in enumerate(name_list):
        subhalo_group.create_dataset(name, data=input_list[i_name])

    for i_name, name in enumerate(subhalo_prop_names):
        subhalo_group.create_dataset(name, data=sub_input_list[i_name])

    if extra_read:
        for i_name, name in enumerate(extra_names):
            subhalo_group.create_dataset(name, data=extra_data[i_name])
    
File.close()
