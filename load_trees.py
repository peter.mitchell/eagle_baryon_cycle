import numpy as np
from os import listdir
import h5py


def Load_Trees(subfind_path, tree_path, nsnap, subhalo_quantity_list, subhalo_type_list, part_index, tree_quantity_list, tree_type_list, test_mode=False):

    subhalo_output_list = []
    for subhalo_quantity, subhalo_type in zip(subhalo_quantity_list,subhalo_type_list):

        if subhalo_type == "float":
            subhalo_output_list.append([])
        elif subhalo_type=="int":
            subhalo_output_list.append(np.rint(np.zeros(0)).astype("int"))
        else:
            print "Load_Trees(): subhalo_type", subhalo_type, "not understood"
            quit()

    tree_output_list = []
    for tree_quantity,tree_type in zip(tree_quantity_list,tree_type_list):
        if tree_type == "float":
            tree_output_list.append([])
        elif tree_type=="int":
            tree_output_list.append(np.rint(np.zeros(0)).astype("int"))
        else:
            print "Load_Trees(): tree_type", tree_type, "not understood"
            quit()

    # Count number of subvolumes
    nsubvol = 0
    for file_name in listdir(subfind_path):
        if not "subfind" in file_name:
            continue
        nsubvol += 1

    # Loop over subvolumes to read in complete subhalo catalogue
    for i_sub in range(nsubvol):

        nsnap_str = str(nsnap)
        while len(nsnap_str) < 3:
            nsnap_str = "0" + nsnap_str

        if i_sub == 0:
            # Read tree files
            tree_file = h5py.File(tree_path+"tree_"+nsnap_str+"."+str(i_sub)+".hdf5")

            expansion_factor = tree_file["outputTimes/expansion"][:]
            snapshots = tree_file["outputTimes/snapshotNumber"][:]

            tree_file.close()


        if ((i_sub <30) | (i_sub > 35)) and test_mode:
            if i_sub < 30:
                continue
            else:
                print "skipping"
                break

        print "Reading tree/subhalo information for ",i_sub, "of", nsubvol

        try:
            subvol_path = subfind_path+"subfind_"+nsnap_str+"."+str(i_sub)+".hdf5"
            subvol_file = h5py.File(subvol_path)
        except:
            subvol_path = subfind_path+"subfind."+str(i_sub)+".hdf5"
            subvol_file = h5py.File(subvol_path)

        for i_quant, quantity in enumerate(subhalo_quantity_list):
            if part_index[i_quant] is None:
                subhalo_output_list[i_quant] = np.concatenate(( subhalo_output_list[i_quant], subvol_file["Subhalo/"+quantity][:]))
            else:
                subhalo_output_list[i_quant] = np.concatenate(( subhalo_output_list[i_quant], subvol_file["Subhalo/"+quantity][:][:,part_index[i_quant]]))

        subvol_file.close()

        # Read tree files
        tree_file = h5py.File(tree_path+"tree_"+nsnap_str+"."+str(i_sub)+".hdf5")

        for i_quant, quantity in enumerate(tree_quantity_list):
            tree_output_list[i_quant] = np.concatenate(( tree_output_list[i_quant], tree_file["haloTrees/"+quantity][:]))
            
        tree_file.close()

    output_list = subhalo_output_list + tree_output_list

    return output_list, expansion_factor, snapshots
