# This is a version of build grid that only does the v50 measurements for the 100 Mpc run (as they are only available for subset of the snapshots)

import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

test_mode = False
cumulative_pp = True
use_aperture = True

# Only include particles with a stellar mass above some value (if use apertue this stellar mass will be the 30kpc aperture mass)
# If you want no selection on stellar mass then set to a negative number
mstar_cut = -1e9
#mstar_cut = 0.0
#mstar_cut = 1.81e7 # 10 particles at standard Eagle resolution (assuming stars have starting gas mass, which they won't)

#star_mass = "n_part" # in this mode, we just measure particle numbers, not particle masses
star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

halo_mass = "m200"

# 25 Mpc REF with 28 snaps
#sim_name = "L0025N0376_REF_snap"
#tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]
#snip_list = np.arange(0,snap_final+1)[::-1]

# 100 Mpc REF with 200 snips
sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")


if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()


omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units (AT FIDUCIAL EAGLE RESOLUTION ONLY!)
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1))
bin_mstar_mid = 0.5*(bin_mstar[1:]+bin_mstar[0:-1])
bin_vmax = np.arange(1.4, 3.0,(3.0-1.4)/(n_bins+1))
bin_vmax_mid = 0.5*(bin_vmax[1:]+bin_vmax[0:-1])
bin_vcirc = np.arange(1.4, 2.8,(2.8-1.4)/(n_bins+1))
bin_vcirc_mid = 0.5*(bin_vcirc[1:]+bin_vcirc[0:-1])
bin_sfr = np.arange(-3.0, 2.0,(5.0)/(n_bins+1))
bin_sfr_mid = 0.5*(bin_sfr[1:]+bin_sfr[0:-1])
bin_ssfr = np.arange(-3.0, 2.0,(5.0)/(n_bins+1))
bin_ssfr_mid = 0.5*(bin_ssfr[1:]+bin_ssfr[0:-1])

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()

lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

name_list = ["mchalo","subgroup_number","vmax","m200_host", "r200_host"]
name_list += ["mass_gas_SF", "mass_gas_NSF", "mass_star", "mass_new_stars_init"]

vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
for vmin_i in vmin:

    name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
    name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

fVmax_cuts = [0.125, 0.25, 0.5]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    for vmin_i in vmin:

        name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
        name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
        name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
        name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

# Note a non shell measurment has to go last!
name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]

# 3 plots
# 1) fraction of direct heating that took place inside ISM
# 2) fraction of ISM wind that was directly heated
# 3) fraction of halo wind that was directly heated

data_list = []
dt_list = []

for lna in lna_mid_choose:
    dt_list.append([])
    data_list.append({})
    for name in name_list:
        data_list[-1][name] = []

f_name_list = []
f_name_list2 = ["vmax_med"]

for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    for vmin_i in vmin:

        f_name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
        f_name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
        f_name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
        f_name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

        f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
        f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
        f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
        f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
        
        f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
        f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
        f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
        f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]

for vmin_i in vmin:

    f_name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
    f_name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
    f_name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
    f_name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

    f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]
    f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]
    
    f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
    f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]
    f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]

f_med_dict = {}
f_med_dict2 = {}

for f_name in f_name_list:
    
    if "_out" in f_name:
        n_rbins = 10
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
    else:
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

for f_name in f_name_list2:
    if "_out" in f_name:
        n_rbins = 10
        f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
    else:
        f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

filename = "processed_subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")
print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    '''if i_snap > 3:
        if i_snap ==4:
            print ""
            print "temp hack"
            print ""
        continue'''

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    if "v90fw_out_"+str(vmin_i).replace(".","p")+"vmax" in subhalo_group:
        if len(subhalo_group["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"][:]) != len(subhalo_group[name_list[-1]][:]):
            print "Skipping"
            skip = True
        else:
            skip = False
    else:
        print "Skipping"
        skip = True

    if name_list[-1] in subhalo_group:

        data = subhalo_group[name_list[-1]][:]

        if use_aperture:
            mstars = subhalo_group["mass_stars_30kpc"] # Msun
        else:
            mstars = subhalo_group["mass_star"] # Msun

        ok = (np.isnan(data)==False) & (mstars>mstar_cut)

        if not skip:
            for i_name, name in enumerate(name_list):

                data = subhalo_group[name][:]
                if name == "Group_R_Crit200_host":
                    data *= a[i_snap] # pMpc h^-1
                ngal_tot = len(data)

                if len(data.shape) == 1:
                    data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])
                elif len(data.shape) == 2:
                    if data_list[ind_bin_snap][name] == []:
                        data_list[ind_bin_snap][name] = np.zeros((0, data.shape[1] ))
                    data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok],axis=0)
                else:
                    print "yikeso"
                    quit()

            ngal = len(data[ok])
            print ngal, ngal_tot

            dt = t[i_snap] - t[i_snap+1]
            dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])
    else:
        print "no data for this snap"

for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue
               

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = (data["mass_gas_NSF"]+data["mass_gas_SF"]+data["mass_star"]) >= 0
    dt = dt[ok_measure]
    for name in name_list:
        data[name] = data[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    mchalo = data["mchalo"]
    mstars = data["mass_star"]
    vmax = data["vmax"]
    R200 = data["r200_host"] /h * 1e3 # pkpc - comoving was already converted earlier
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22
    vcirc = np.sqrt(G * data["m200_host"]*Msun / (data[ "r200_host"]*Mpc/h)) / 1e3 # kms^-1 - note comoving conversion was done earlier

    sfr = data["mass_new_stars_init"] / dt * 1e-9 # Msun yr^-1
    ssfr = sfr / mstars * 1e9 # Gyr^-1

    if use_aperture:
        sfr = data["mass_new_stars_init_100_30kpc"] / 100 / 1e6 # Msun yr^-1
        mstars = data["mass_stars_30kpc"] # Msun

    if halo_mass == "m200":
        print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["m200_host"]
        dm2tot_factor = 1.0 # In this case there is no need to correct dm mass to total mass

    # It's probably a better solution to output mass rather than particle numbers 
    if star_mass == "n_part":
        for name in name_list:
            if "n_" in name:
                data[name] *= mgas_part * 1.989e43 * g2Msun / h

    subgroup_number = data["subgroup_number"]

    #### Compute some percentile efficiencies ########
    cent = subgroup_number == 0
    weight_cent = np.zeros_like(dt[cent])
    dt_u = np.unique(dt)
    for dt_u_i in dt_u:
        ok2_cent = dt[cent] == dt_u_i
        weight_cent[ok2_cent] = dt[cent][ok2_cent] / len(dt[cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point

    # Loop over radial velocity cuts
    for fVmax_cut in fVmax_cuts:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

        for vmin_i in vmin:

            v_str = "_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"

            # Bins with no particles in the shell shouldn't have 0 for the median velocity
            issue = data["v50mw"+v_str] == 0
            data["v50mw"+v_str][issue] = np.nan
            issue = data["v50fw"+v_str] == 0
            data["v50fw"+v_str][issue] = np.nan

            issue = data["v90mw"+v_str] == 0
            data["v90mw"+v_str][issue] = np.nan
            issue = data["v90fw"+v_str] == 0
            data["v90fw"+v_str][issue] = np.nan

            for i_r in range(n_rbins):

                f_med_dict2["v50mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                f_med_dict2["v90mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

                f_med_dict2["v50fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                f_med_dict2["v90fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

    for vmin_i in vmin:
        for i_r in range(n_rbins):

            v_str = "_out_"+str(vmin_i).replace(".","p")+"vmax"

            # Bins with no particles in the shell shouldn't have 0 for the median velocity
            issue = data["v50mw"+v_str] == 0
            data["v50mw"+v_str][issue] = np.nan
            issue = data["v50fw"+v_str] == 0
            data["v50fw"+v_str][issue] = np.nan

            issue = data["v90mw"+v_str] == 0
            data["v90mw"+v_str][issue] = np.nan
            issue = data["v90fw"+v_str] == 0
            data["v90fw"+v_str][issue] = np.nan

            f_med_dict2["v50mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
            f_med_dict2["v90mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
            f_med_dict2["v50fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
            f_med_dict2["v90fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

    f_med_dict2["vmax_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),vmax[cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])
        ok_cent = ok & (subgroup_number == 0)
        
        weight_cent = np.zeros_like(dt[ok_cent])
        
        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            

        mass_new_stars = data["mass_new_stars_init"]

        for f_name in f_name_list:
            
            for fVmax_cut in fVmax_cuts:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                if fV_str in f_name:
                    break


            if "_out" in f_name: # Mean shell flux
                for i_r in range(n_rbins):
                    f_med_dict[f_name][i_bin,j_bin,i_r] = np.sum(data[f_name][:,i_r][ok_cent]*weight_cent) / np.sum(weight_cent)

            else:
                print "nope", f_name
                quit()

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass+"_v50_only"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_mstar_grid", data=bin_mstar_mid)
File_grid.create_dataset("log_vmax_grid", data=bin_vmax_mid)
File_grid.create_dataset("log_vcirc_grid", data=bin_vcirc_mid)
File_grid.create_dataset("log_sfr_grid", data=bin_sfr_mid)
File_grid.create_dataset("log_ssfr_grid", data=bin_ssfr_mid)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

for f_name in f_name_list:

    File_grid.create_dataset(f_name, data=f_med_dict[f_name])

for f_name in f_name_list2:
    File_grid.create_dataset(f_name, data=f_med_dict2[f_name])

File_grid.close()
