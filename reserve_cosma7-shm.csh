#!/bin/tcsh -ef
#
#SBATCH --ntasks 1
#SBATCH -J reserve
#SBATCH -p cosma7-shm
#SBATCH -A dp004

# How long to reserve for (72 hours is the maximum)
#SBATCH -t 72:00:00

# Choose memory allocation per node (in Mb)
# For cosma7-shm, total memory available is ~ 1.5 Tb, across 112 cores, so in principle 13.4 Gb per core
#set MemPerNode = MaxMemPerNode
#set MemPerNode = 857000 # 64 cores worth of memory
#set MemPerNode = 570000 # 3/8 memory
#set MemPerNode = 383000 # quarter memory
#set MemPerNode = 191000 # 1/8 memory
#set MemPerNode = 96000 # 1/16
#set MemPerNode = 48000 # 1/32
#set MemPerNode = 24000 # 1/64

# Amount of memory to reserve
#SBATCH --mem 570000

# Number of cores to reserve
# Cosma7-shm has 112 cores
#SBATCH -c 56
#

srun sleep 200d
