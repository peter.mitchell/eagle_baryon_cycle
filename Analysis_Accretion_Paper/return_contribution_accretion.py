import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 28 snapshots variant
#sim_name = "L0025N0376_REF_snap_par_test"

# 25 Mpc ref with 50 snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc recal with 200 snipshots
#sim_name = "L0025N0752_Recal"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
f_name_list += ["facc_merge_subnorm", "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.1,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2*1.05],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 14.3

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1.1

facc_med = f_med_dict["facc_pristine_subnorm"]
freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
ffof_med = f_med_dict["facc_fof_subnorm"]
fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]

facc_merge = f_med_dict["facc_merge_subnorm"]
fcool_merge = f_med_dict["fcool_merge_ism_subnorm"]# + f_med_dict["fcool_merge_cgm_subnorm"]
# Changed my mind here, this gas has not been in the ISM before so it belongs to the "first-infall", even if it isn't "smooth"
fcool_pristine_med += f_med_dict["fcool_merge_cgm_subnorm"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        ax.set_position([0.09, 0.5, 0.42, 0.42])
    if i == 2:
        ax.set_position([0.09, 0.08, 0.42, 0.42])
    if i == 1:
        ax.set_position([0.57, 0.5, 0.42, 0.42])
    if i == 3:
        ax.set_position([0.57, 0.08, 0.42, 0.42])

    if i == 0:
        show_z = [0]
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                
                y1 = facc_med[n]; y2 = freacc_med[n]; y3 = ffof_med[n]
                y4 = facc_merge[n]
                ytot = y1+y2+y3+y4
                y1 *= 1./ytot; y2 *= 1./ytot; y3 *= 1./ytot; y4 *= 1./ytot

                y2 += y1
                y3 += y2

                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()
                    temp = np.max(np.where(ok==False)[0])+2
                    
                    py.fill_between(bin_mh_med[n][:temp],np.zeros_like(y1[:temp]),y1[:temp],color="b",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp], y1[:temp],y2[:temp],color="r",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp], y2[:temp],y3[:temp],color="c",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp], y3[:temp],np.ones_like(y3[:temp]),color="k",alpha=0.3)

                    y1[ok==False] = np.nan; y2[ok==False] = np.nan; y3[ok==False] = np.nan
                    y4[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.fill_between(bin_mh_med[n],np.zeros_like(y1),y1,color="b",alpha=0.7,label="First infall")
                py.fill_between(bin_mh_med[n],y1,y2,color="r",alpha=0.7,label="Recycled")
                py.fill_between(bin_mh_med[n],y2,y3,color="c",alpha=0.7,label="Transfer")
                py.fill_between(bin_mh_med[n],y3,np.ones_like(y3),color="k",alpha=0.7,label="Merger")

                py.legend(loc="upper left", frameon=False,numpoints=1,bbox_to_anchor=(0.1, 1.2),ncol=4,prop={'size': 10})

        py.ylabel(r"$\mathrm{Fraction} \,\, [\,\mathrm{Halo \,\, scale}\, , \, 0.0<z<0.3 \,]$")
        ylo = 0.0; yhi = 1.0

    if i == 1:
        show_z = [3]
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                
                y1 = facc_med[n]; y2 = freacc_med[n]; y3 = ffof_med[n]
                y4 = facc_merge[n]
                ytot = y1+y2+y3+y4
                y1 *= 1./ytot; y2 *= 1./ytot; y3 *= 1./ytot; y4 *= 1./ytot

                y2 += y1
                y3 += y2

                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()
                    temp = np.max(np.where(ok==False)[0])+2
                    
                    py.fill_between(bin_mh_med[n][:temp],np.zeros_like(y1[:temp]),y1[:temp],color="b",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp], y1[:temp],y2[:temp],color="r",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp], y2[:temp],y3[:temp],color="c",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp], y3[:temp],np.ones_like(y3[:temp]),color="k",alpha=0.3)

                    y1[ok==False] = np.nan; y2[ok==False] = np.nan; y3[ok==False] = np.nan
                    y4[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.fill_between(bin_mh_med[n],np.zeros_like(y1),y1,color="b",alpha=0.7,label="First infall")
                py.fill_between(bin_mh_med[n],y1,y2,color="r",alpha=0.7,label="Recycled")
                py.fill_between(bin_mh_med[n],y2,y3,color="c",alpha=0.7,label="Transfer")
                py.fill_between(bin_mh_med[n],y3,np.ones_like(y3),color="k",alpha=0.7,label="Merger")

        py.ylabel(r"$\mathrm{Fraction} \,\, [\,\mathrm{Halo \,\, scale}\, , \, 1.5<z<2.4 \,]$")
        ylo = 0.0; yhi = 1.0


    if i == 2:
        show_z = [0]
        for n in range(len(facc_med[:,0])):
            if n in show_z:

                y1 = fcool_pristine_med[n]; y2 = fcool_recooled_med[n]; y3 = fcool_galtran_med[n]
                y4 = fcool_merge[n]
                ytot = y1+y2+y3+y4
                y1 *= 1./ytot; y2 *= 1./ytot; y3 *= 1./ytot
                y4 *= 1./ytot

                y2 += y1; y3 += y2; y4 += y3

                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()
                    temp = np.max(np.where(ok==False)[0])+2
                    py.fill_between(bin_mh_med[n][:temp],np.zeros_like(y1[:temp]),y1[:temp],color="b",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp],y1[:temp],y2[:temp],color="r",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp],y2[:temp],y3[:temp],color="c",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp],y3[:temp],y4[:temp],color="k",alpha=0.3)
                    y1[ok==False] = np.nan; y2[ok==False] = np.nan; y3[ok==False] = np.nan
                    y4[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.fill_between(bin_mh_med[n],np.zeros_like(y1),y1,color="b",alpha=0.7)
                py.fill_between(bin_mh_med[n],y1,y2,color="r",alpha=0.7)
                py.fill_between(bin_mh_med[n],y2,y3,color="c",alpha=0.7)
                py.fill_between(bin_mh_med[n],y3,y4,color="k",alpha=0.7)

        py.ylabel(r"$\mathrm{Fraction} \,\, [\, \mathrm{galaxy \, \, scale} \, , \, 0.0<z<0.3 \,]$")
        ylo = 0.0; yhi = 0.99
        py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")


    if i == 3:
        show_z = [3]
        for n in range(len(facc_med[:,0])):
            if n in show_z:

                y1 = fcool_pristine_med[n]; y2 = fcool_recooled_med[n]; y3 = fcool_galtran_med[n]
                y4 = fcool_merge[n]
                ytot = y1+y2+y3+y4
                y1 *= 1./ytot; y2 *= 1./ytot; y3 *= 1./ytot
                y4 *= 1./ytot

                y2 += y1; y3 += y2; y4 += y3

                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()
                    temp = np.max(np.where(ok==False)[0])+2
                    py.fill_between(bin_mh_med[n][:temp],np.zeros_like(y1[:temp]),y1[:temp],color="b",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp],y1[:temp],y2[:temp],color="r",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp],y2[:temp],y3[:temp],color="c",alpha=0.3)
                    py.fill_between(bin_mh_med[n][:temp],y3[:temp],y4[:temp],color="k",alpha=0.3)
                    y1[ok==False] = np.nan; y2[ok==False] = np.nan; y3[ok==False] = np.nan
                    y4[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.fill_between(bin_mh_med[n],np.zeros_like(y1),y1,color="b",alpha=0.7)
                py.fill_between(bin_mh_med[n],y1,y2,color="r",alpha=0.7)
                py.fill_between(bin_mh_med[n],y2,y3,color="c",alpha=0.7)
                py.fill_between(bin_mh_med[n],y3,y4,color="k",alpha=0.7)

        py.ylabel(r"$\mathrm{Fraction} \,\, [\, \mathrm{galaxy \, \, scale} \, , \, 1.5<z<2.4 \,]$")
        ylo = 0.0; yhi = 0.99
        py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")
    
    
    #for tick in ax.yaxis.get_major_ticks():
    #    tick.label1On=True
        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

fig_name = "return_contribution_gas_accretion_rates_subnorm_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
