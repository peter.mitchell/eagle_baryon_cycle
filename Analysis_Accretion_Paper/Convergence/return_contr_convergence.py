import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

sim_name_list = ["L0025N0376_REF_snap", "L0025N0376_REF_50_snip", "L0025N0376_REF_100_snip", "L0025N0376_REF_200_snip","L0025N0376_REF_500_snap", "L0025N0376_REF_1000_snap"]
snap_list = [28, 50, 100, 200, 500,1000]
label_list = [r"$N_{\mathrm{snap}} = 28$", r"$N_{\mathrm{snap}} = 50$", r"$N_{\mathrm{snap}} = 100$", r"$N_{\mathrm{snap}} = 200$", r"$N_{\mathrm{snap}} = 500$", r"$N_{\mathrm{snap}} = 1000$"]

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!



########### Load tabulated efficiencies #########################

output = []
for sim_name in sim_name_list:

    # Neistein uses dark matter subhalo mass as "halo mass"
    if test_mode:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
    else:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

    print sim_name

    bin_mh_mid = file_grid["log_msub_grid"][:]
    bin_mh_med = file_grid["bin_mh_med"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]


    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
    f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
    f_name_list += ["facc_merge_subnorm", "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm"]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    output_i = bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict
    output.append(output_i)


from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'legend.fontsize':8})

xlo = 10.; xhi = 15.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

lw = 1
ls_list = ["-", '-', "--",'--',":",":"]


for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0]

    if i == 0:
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):

                    bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict = output[i_sim]
                    facc_med = f_med_dict["facc_pristine_subnorm"]
                    freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
                    ffof_med = f_med_dict["facc_fof_subnorm"]
                    fmerge_med = f_med_dict["facc_merge_subnorm"]

                    y = freacc_med[n] / (facc_med[n] + freacc_med[n] + ffof_med[n] +fmerge_med[n])

                    if n == 0:
                        py.plot(bin_mh_med[n],y,c=c_list[i_sim],linewidth=lw,linestyle=ls_list[n],label=(label_list[i_sim]))
                    else:
                        py.plot(bin_mh_med[n],y,c=c_list[i_sim],linewidth=lw,linestyle=ls_list[n])

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1)
        py.gca().add_artist(legend1)

        lines = []; labels = []
        ls_list2 = ['-','--',':']
        for n in range(len(show_z)):
            z_str = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z[n]]-1,1./a_min[show_z[n]]-1)
            l, = py.plot(bin_mh_med[n],bin_mh_med[n]-99,c="k",linewidth=lw, ls=ls_list2[n])
            lines.append(l)
            labels.append(z_str)
        py.legend(handles=lines,labels=labels, loc="lower left",frameon=False,numpoints=1)

        ylo = 0.; yhi = 0.75


    if i == 1:
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):

                    bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict = output[i_sim]
                    fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
                    fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
                    fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]
                    fcool_merge = f_med_dict["fcool_merge_ism_subnorm"] + f_med_dict["fcool_merge_cgm_subnorm"]
                    y = fcool_recooled_med[n] / (fcool_pristine_med[n] + fcool_recooled_med[n] + fcool_galtran_med[n] + fcool_merge[n])

                    if n == show_z[0]:
                        py.plot(bin_mh_med[n],y,c=c_list[i_sim],linewidth=lw,linestyle=ls_list[n],label=label_list[i_sim])
                    else:
                        py.plot(bin_mh_med[n],y,c=c_list[i_sim],linewidth=lw,linestyle=ls_list[n])

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = 0.; yhi = 0.75


    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")









if test_mode:
    fig_name = "return_contr_convergence_test_mode.pdf"
else:
    fig_name = "return_contr_convergence.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
