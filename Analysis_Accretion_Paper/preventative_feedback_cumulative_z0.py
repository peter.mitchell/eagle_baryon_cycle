import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

# 2 notes to self
# 1) At the moment I'm not excluding recycled DM accretion, which is a mistake
# 2) There is a choice for the galaxy scale accretion whether to include gas that came from the CGM of merging satellites
#    For now I'm going to ignore this contribution (it's kind of ambiguous whether this is merger or smooth acc anyway)

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:][0]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fpracc_cum_pp","fpracc_dm_cum_pp"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.97,
                    'legend.fontsize':7,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

fpracc = f_med_dict["fpracc_cum_pp"]
fpracc_dm = f_med_dict["fpracc_dm_cum_pp"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        y1 = fpracc / 10**bin_mh_med
        y2 = fpracc_dm * fb * dm2tot_factor / 10**bin_mh_med

        if show_incomplete:
            if complete_cut == "1newstar":
                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][0] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][0] == 0)
            elif complete_cut == "100star":
                ok = bin_mh_mid >= 10.8
            else:
                print "No"; quit()

            temp = np.max(np.where(ok==False)[0])+2

            py.plot(bin_mh_med[:temp], np.log10(y1[:temp]/y2[:temp]),c="k",linewidth=lw,alpha=0.3)
            #py.plot(bin_mh_med[:temp], np.log10(y2[:temp]),c="k",linewidth=lw,alpha=0.3, linestyle='--')
            y1[ok==False] = np.nan
            y2[ok==False] = np.nan
        else:
            ok = bin_mh_mid > 0

        py.plot(bin_mh_med[ok],np.log10(y1[ok]/y2[ok]),c="k",linewidth=lw)
        #py.plot(bin_mh_med[ok],np.log10(y2[ok]),c="k",linewidth=lw, linestyle='--', label=r"$\frac{\Omega_b}{\Omega_m-\Omega_b} \times$ Dark Matter")
                
        #py.ylabel(r"$\log(M_{\mathrm{in,halo}}^{\mathrm{1st-infall}} \, / M_{200})$")
        py.ylabel(r"$\log(M_{\mathrm{in,halo}}^{\mathrm{1st-infall,gas}} \, / \frac{\Omega_b}{\Omega_m-\Omega_b} M_{\mathrm{in,halo}}^{\mathrm{1st-infall, DM}})$")

        ylo = -1.5; yhi = -0.5

        py.legend(loc='lower left',frameon=False,numpoints=1,prop={'size': 9})

    py.axhline(0.0,linestyle='--',alpha=0.6,c="k")
        
    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200}(z=0) \, / \mathrm{M_\odot})$")

fig_name = "preventative_feedback_cumulative_z0.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
