import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

show_mitra = True
show_henriques = True
show_lagos18 = True
show_lacey16 = True
show_somerville15 = True # Same as Somerville08 in this respect
show_oppenheimer10 = True
# For Oppenheimer, he plots the median return time, putting non-returned particles at t_ret = time to z=1
# He plots this for particles ejected at z=1, (by their halo mass at this time)
# Note I'm plotting their vzw model

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

f_name_list = ["fcool_recooled_return"+fV_str, "facc_reaccreted_return"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2.5],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.; xhi = 15.

#c_list = ["k","m","b","c","g","y","tab:orange","r"]
ls_list = ["-","-","-","--"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

frecool = f_med_dict["fcool_recooled_return"+fV_str]
freacc = f_med_dict["facc_reaccreted_return"+fV_str]

for i,ax in enumerate(subplots):
    py.axes(ax)

    # Shrink current axis's height by 10% on the bottom
    #box = ax.get_position()
    #ax.set_position([box.x0, box.y0 + box.height * 0.1,
    #                 box.width, box.height * 0.8])
    if i == 1:
        ax.set_position([0.19, 0.08, 0.79, 0.36])
    if i == 0:
        ax.set_position([0.19, 0.44, 0.79, 0.36])

    
    show_z = [0,3]
    if i == 0:
        for n in range(len(frecool[:,0])):
            if n in show_z:
                y = freacc[n]
                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()

                    temp = np.max(np.where(ok==False)[0])+2
                    py.plot(bin_mh_med[n][:temp], np.log10(y[:temp]),c="k",linewidth=lw,alpha=0.3,linestyle=ls_list[n])
                    y[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c="k",linewidth=lw, linestyle=ls_list[n],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))

                if show_lacey16:
                    tdyn = t_grid[n] * 0.1
                    alpha_ret = 0.64
                    tret = tdyn / alpha_ret # Gyr
                    py.axhline(np.log10(1/tret),linewidth=lw,c="g", linestyle=ls_list[n])

                if show_somerville15:
                    tdyn = t_grid[n] * 0.1
                    alpha_ret = 0.1
                    tret = tdyn / alpha_ret # Gyr
                    py.axhline(np.log10(1/tret),linewidth=lw,c="y", linestyle=ls_list[n])
                    

        if show_henriques:
            # This is taken from Table S1 of Henriques 15
            # No redshift evolution in this case
            gamma = 30 # Gyr
            py.plot(bin_mh_mid, np.log10( 1./ (gamma * 1e10 / 10**bin_mh_mid)),c="c",linewidth=lw)

        if show_lagos18:
            # This is taken from Table 3 of Lagos18
            gamma = 25 # Gyr
            py.plot(bin_mh_mid, np.log10( 1./ (gamma * 1e10 / 10**bin_mh_mid)),c="m",linewidth=lw)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}}^{\mathrm{recycled}} / M_{\mathrm{ej,halo}} \, / \mathrm{Gyr^{-1}})$")

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,prop={'size': 10})

        py.gca().add_artist(legend1)

        line1, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="k",linewidth=lw)
        label1 = r"EAGLE"
        line2, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="r",linewidth=lw)
        label2="Mitra et al. (2015)"
        line3, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="c",linewidth=lw)
        label3="Henriques et al. (2015)"
        line4, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="g",linewidth=lw)
        label4 ="Lacey et al. (2016)"
        line5, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="y",linewidth=lw)
        label5 ="Somerville et al. (2015)"
        line6, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="m",linewidth=lw)
        label6 ="Lagos et al. (2018)"
        line7, = py.plot(bin_mh_mid-99,bin_mh_mid-99,c="b",linewidth=lw)
        label7 ="Oppenheimer et al. (2010)"
        lines = [line1]
        labels = [label1]
        if show_mitra:
            lines += [line2]; labels += [label2]
        if show_henriques:
            lines += [line3]; labels += [label3]
        if show_lacey16:
            lines += [line4]; labels += [label4]
        if show_somerville15:
            lines += [line5]; labels += [label5]
        if show_lagos18:
            lines += [line6]; labels += [label6]
        if show_oppenheimer10:
            lines += [line7]; labels += [label7]

        # Put a legend below current axis
        #ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
        #          fancybox=True, shadow=True, ncol=5)

        py.legend(handles=lines, labels=labels,loc="upper left", frameon=False,numpoints=1,bbox_to_anchor=(0.1, 1.6))

        ylo = -2.3; yhi = 1.2
        
    if i == 1:
        for n in range(len(freacc[:,0])):
            if n in show_z:
                y = frecool[n]
                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()

                    temp = np.max(np.where(ok==False)[0])+2
                    py.plot(bin_mh_med[n][:temp], np.log10(y[:temp]),c="k",linewidth=lw,alpha=0.3,linestyle=ls_list[n])
                    y[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c="k",linestyle=ls_list[n],linewidth=lw, label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))

                if show_mitra:
                    t1 = 0.52
                    t2 = -0.32
                    t3 = -0.45
                    z = z_grid[n]
                    t_ret_mitra = t1 * (1+z)**t2 * (10**bin_mh_mid/1e12)**t3 # Gyr??
                    py.plot(bin_mh_mid, np.log10(1./t_ret_mitra), c="r", linewidth=lw,linestyle=ls_list[n])

        if show_oppenheimer10:
            logm, log_tret_med = np.loadtxt("oppenheimer_vzw_return_time.dat",unpack=True)
            py.plot(logm, np.log10(1./10**log_tret_med)+9, c="b",linewidth=lw)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^{\mathrm{recycled}} / M_{\mathrm{ej,ISM}} \, / \mathrm{Gyr^{-1}})$")

        #py.legend(loc='upper right',frameon=False,numpoints=1)
      
        ylo = -3.0; yhi = 1.0
        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "return_efficiency_sam_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
