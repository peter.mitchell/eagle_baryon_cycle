import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_dm = True
show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

include_mergers = True

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
if show_dm:
    f_name_list += ["facc_dm_dmnorm"]
if include_mergers:
    f_name_list += ["facc_merge_subnorm", "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm"]
    if show_dm:
        f_name_list += ["facc_dm_merge_dmnorm"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.0; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

facc_med = f_med_dict["facc_pristine_subnorm"]
freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
ffof_med = f_med_dict["facc_fof_subnorm"]
fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]

facc_tot = facc_med + freacc_med + ffof_med
fcool_tot = fcool_pristine_med + fcool_recooled_med + fcool_galtran_med

if show_dm:
    facc_dm = f_med_dict["facc_dm_dmnorm"]

if include_mergers:
    facc_merge = f_med_dict["facc_merge_subnorm"]
    fcool_merge = f_med_dict["fcool_merge_ism_subnorm"] + f_med_dict["fcool_merge_cgm_subnorm"]

    facc_tot += facc_merge
    fcool_tot += fcool_merge

    if show_dm:
        facc_dm_merge = f_med_dict["facc_dm_merge_dmnorm"]
        facc_dm += facc_dm_merge

for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0,1,2,3,4,5,6]
    if i == 0:
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                
                y = facc_tot[n]
                if show_dm:
                    y2 = facc_dm[n]
                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = (bin_mh_med[n] >= 10.8)  | (np.isnan(bin_mh_med[n]))
                    else:
                        print "No"; quit()

                else:
                    ok = bin_mh_med[n] > 0

                temp = np.max(np.where(ok==False)[0])+2

                # Highest mass point in this bin is an outlier in many of the plots
                if "L0100" in sim_name and n == 4:
                    ok2 = (bin_mh_mid != 13.45)
                    y[ok2==False] = np.nan
                    y2[ok2==False] = np.nan

                py.plot(bin_mh_med[n][:temp], np.log10(y[:temp]),c=c_list[n],linewidth=lw,alpha=0.3)
                y[ok==False] = np.nan

                if show_dm:
                    py.plot(bin_mh_med[n][:temp], np.log10(y2[:temp]),c=c_list[n],linewidth=0.8,alpha=0.3,linestyle='--')
                    y2[ok==False] = np.nan
                
                py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw, label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                py.scatter(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],edgecolors="none",s=5)

                if show_dm:
                    py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c=c_list[n],linewidth=0.8,linestyle='--')

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} \, / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,prop={'size': 7})
        py.gca().add_artist(legend1)

        l1, = py.plot(bin_mh_med[n],bin_mh_med[n]-99,c="k",linewidth=lw,label="Gas")
        l2, = py.plot(bin_mh_med[n],bin_mh_med[n]-99,c="k",linewidth=0.8,label="Dark matter",linestyle='--')
        py.legend(handles=[l1,l2],labels=[r"Gas","Dark matter"], loc="lower left",frameon=False,numpoints=1)

        ylo = -2.75; yhi = 0.75

    if i == 1:
        for n in range(len(freacc_med[:,0])):
            if n in show_z:

                y = fcool_tot[n]
                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = (bin_mh_med[n] >= 10.8) | (np.isnan(bin_mh_med[n]))
                    else:
                        print "No"; quit()
                else:
                    ok = bin_mh_med[n] > 0

                temp = np.max(np.where(ok==False)[0])+2

                if "L0100" in sim_name and n == 4:
                    ok2 = bin_mh_mid !=  13.45
                    y[ok2==False] = np.nan
                
                py.plot(bin_mh_med[n][:temp], np.log10(y[:temp]),c=c_list[n],linewidth=lw,alpha=0.3)
                y[ok==False] = np.nan

                py.plot(bin_mh_med[n][ok],np.log10(y[ok]),label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n],linewidth=lw)
                py.scatter(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}} \, / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        #py.legend(loc='upper right',frameon=False,numpoints=1)
      
        ylo = -3.0; yhi = 0.5

        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "gas_accretion_rates_subnorm.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
