import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 snipshots
########sim_name = "L0100N1504_REF_200_snip"
#WARNING WARNING - tret bins are broken for 100 Mpc at the moment, as they only go back to the restart redshift for each subvol!
# Note - this is not an issue for runs which didn't need a restart - and is fixed properly as of 18/2/2020

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

tret_bins = file_grid["tret_bins"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fret_ism_t_dist", "fret_halo_t_dist"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':10,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

for i,ax in enumerate(subplots):
    py.axes(ax)

    #for tick in ax.xaxis.get_major_ticks():
    #    tick.label1On=True

    show_mh = 14

    show_z = [0,1,2,3,4,5]


    if i == 0:
        
        for i_z in range(len(tret_bins[:,0])):
            if i_z not in show_z:
                continue


            x = 0.5*(tret_bins[i_z][1:] + tret_bins[i_z][0:-1])
            y = np.cumsum(f_med_dict["fret_halo_t_dist"][i_z][show_mh])
            y *= 1./y[-1]

            jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[i_z]-1,1./a_min[i_z]-1)
            py.plot(x,y,c=c_list[i_z],linewidth=lw,label=jimbo)

        py.ylabel(r"$f(<\Delta t_{\mathrm{rec}}) \, [\,\mathrm{Halo-scale}\,]$")

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        #py.annotate(jimbo,(4.5,0.58))

        #py.legend(loc='lower right',frameon=False,numpoints=1)

        xlo = 0.; xhi= 12.0
        ylo = 0.0; yhi = 1.05

    if i == 1:

        for i_z in range(len(tret_bins[:,0])):
            if i_z not in show_z:
                continue

            x = 0.5*(tret_bins[i_z][1:] + tret_bins[i_z][0:-1])
            y = np.cumsum(f_med_dict["fret_ism_t_dist"][i_z][show_mh])
            y *= 1./y[-1]

            jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[i_z]-1,1./a_min[i_z]-1)
            py.plot(x,y,c=c_list[i_z],linewidth=lw, label=jimbo)

        py.ylabel(r"$f(<\Delta t_{\mathrm{rec}}) \, [\,\mathrm{Galaxy-scale}\,]$")

        #py.annotate(jimbo,(8,np.max(y)))

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(4.5,0.58))

        py.legend(loc='lower right',frameon=False,numpoints=1)

        xlo = 0.; xhi= 12.0
        ylo = 0.0; yhi = 1.05


    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\Delta t_{\mathrm{rec}} \, / \mathrm{Gyr}$")

fig_name = "tret_t_dist.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
