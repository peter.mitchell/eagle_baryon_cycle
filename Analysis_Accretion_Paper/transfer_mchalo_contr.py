import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 snipshots
########sim_name = "L0100N1504_REF_200_snip"
#WARNING WARNING - tret bins are broken for 100 Mpc at the moment, as they only go back to the restart redshift for each subvol!
# Note - this is not an issue for runs which didn't need a restart - and is fixed properly as of 18/2/2020

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fret_galtran_mchalo_ratio", "fret_fof_transfer_mchalo_ratio"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.1,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':7,
                    'axes.labelsize':10,
                    'legend.fontsize':8})

xlo = 9.0; xhi = 13.9

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1

for i,ax in enumerate(subplots):
    py.axes(ax)

    #for tick in ax.xaxis.get_major_ticks():
    #    tick.label1On=True

    #bin_mh = np.arange(8.0, 15.1,0.2)

    n_mej_bins = 10 # Same as tret
    mej_bins = np.array([ 9.0, 10.0, 10.5, 11.0, 11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 15.0])

    if i == 0:
        show_z = 0
        show_mh = 19
    
        x = mej_bins
        y = np.log10(f_med_dict["fret_galtran_mchalo_ratio"][show_z][show_mh] / (x[1:]-x[0:-1]))

        for n in range(len(x[1:])):
            py.plot( [x[n],x[n]], [0,y[n]] , c="k", linewidth=lw)
            py.plot( [x[n+1],x[n+1]], [0,y[n]] , c="k",linewidth=lw)
            py.plot( [x[n],x[n+1]], [y[n],y[n]] , c="k",linewidth=lw)

        py.ylabel(r"$\log_{10}\left(\frac{\mathrm{d} \dot{M}_{\mathrm{in,ISM}}^{\mathrm{transfer}}}{\mathrm{d} \log_{10}(M_{\mathrm{DM}}^{\mathrm{s}}/M_\odot)} \, / \mathrm{M_\odot Gyr^{-1}}\right)$")

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(8,np.max(y)))

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(11,8.2))

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = 6.5; yhi = 8.5

    if i == 1:
        show_z = 0
        show_mh = 10
    
        x = mej_bins
        y = np.log10(f_med_dict["fret_galtran_mchalo_ratio"][show_z][show_mh] / (x[1:]-x[0:-1]))

        for n in range(len(x[1:])):
            py.plot( [x[n],x[n]], [0,y[n]] , c="k", linewidth=lw)
            py.plot( [x[n+1],x[n+1]], [0,y[n]] , c="k",linewidth=lw)
            py.plot( [x[n],x[n+1]], [y[n],y[n]] , c="k",linewidth=lw)

        #py.ylabel(r"$\mathrm{d} \dot{M}_{\mathrm{in,ISM}}^{\mathrm{transfer}} /  \mathrm{d} \log_{10}(M_{\mathrm{DM}}^{\mathrm{s}}) \, / \mathrm{M_\odot Gyr^{-1} \mathrm{dex}^{-1}}$")

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(8,np.max(y)))

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(11,4.6))

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = 3.0; yhi = 5.0


    if i == 2:
        show_z = 0
        show_mh = 19

        x = mej_bins
        y = np.log10(f_med_dict["fret_fof_transfer_mchalo_ratio"][show_z][show_mh] / (x[1:]-x[0:-1]))

        for n in range(len(x[1:])):
            py.plot( [x[n],x[n]], [0,y[n]] , c="k",linewidth=lw)
            py.plot( [x[n+1],x[n+1]], [0,y[n]] , c="k",linewidth=lw)
            py.plot( [x[n],x[n+1]], [y[n],y[n]] , c="k",linewidth=lw)

        py.ylabel(r"$\log_{10}\left(\frac{\mathrm{d} \dot{M}_{\mathrm{in,halo}}^{\mathrm{transfer}}}{\mathrm{d} \log_{10}(M_{\mathrm{DM}}^{\mathrm{s}}/M_\odot)} \, / \mathrm{M_\odot Gyr^{-1}}\right)$")

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(2,np.max(y)))

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(11,8.6))

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = 6.9; yhi = 8.9
        py.xlabel(r"$\log_{10}(M_{\mathrm{DM}}^{s} \, / \mathrm{M_\odot})$")
    

    if i == 3:
        show_z = 0
        show_mh = 10

        x = mej_bins
        y = np.log10(f_med_dict["fret_fof_transfer_mchalo_ratio"][show_z][show_mh] / (x[1:]-x[0:-1]))

        for n in range(len(x[1:])):
            py.plot( [x[n],x[n]], [0,y[n]] , c="k",linewidth=lw)
            py.plot( [x[n+1],x[n+1]], [0,y[n]] , c="k",linewidth=lw)
            py.plot( [x[n],x[n+1]], [y[n],y[n]] , c="k",linewidth=lw)

        #py.ylabel(r"$\mathrm{d} \dot{M}_{\mathrm{in,halo}}^{\mathrm{transfer}} /  \mathrm{d} \log_{10}(M_{\mathrm{DM}}^{\mathrm{s}}) \, / \mathrm{M_\odot Gyr^{-1} \mathrm{dex}^{-1}}$")

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(2,np.max(y)))

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(11,7.6))

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = 5.9; yhi = 7.9
        py.xlabel(r"$\log_{10}(M_{\mathrm{DM}}^{s} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

fig_name = "transfer_mchalo_contr.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
