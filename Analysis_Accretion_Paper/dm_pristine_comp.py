import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["facc_dm_dmnorm", "facc_dm_pristine_dmnorm"]
f_name_list += ["facc_dm_merge_dmnorm"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

facc_dm = f_med_dict["facc_dm_dmnorm"]
facc_dm_pristine = f_med_dict["facc_dm_pristine_dmnorm"]
facc_dm_rec = facc_dm - facc_dm_pristine
facc_dm_merge = f_med_dict["facc_dm_merge_dmnorm"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0,2,4,6]
    if i == 0:
        for n in range(len(facc_dm_pristine[:,0])):
            if n in show_z:
                
                py.plot(bin_mh_mid,np.log10(facc_dm_pristine[n]),c=c_list[n],linewidth=0.8,linestyle='-')
                py.plot(bin_mh_mid,np.log10(facc_dm_rec[n]),c=c_list[n],linewidth=0.8,linestyle='--')
                py.plot(bin_mh_mid,np.log10(facc_dm_merge[n]),c=c_list[n],linewidth=0.8,linestyle=':')

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} \, / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,prop={'size': 7})
        py.gca().add_artist(legend1)

        l1, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,label="Pristine DM")
        l2, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=0.8,label="Rec+Tran DM",linestyle='--')
        l3, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=0.8,label="Merge DM",linestyle=':')
        py.legend(handles=[l1,l2,l3],labels=[r"Pristine DM","Rec+Tran DM", "Merge DM"], loc="lower left",frameon=False,numpoints=1)

        ylo = -2.; yhi = 0.5

        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "dm_pristine_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
