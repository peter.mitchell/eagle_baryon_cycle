import numpy as np
import h5py
import utilities_statistics as us
import os
import warnings
import utilities_cosmology as uc

z = 0.1

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_200_snip"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if not os.path.exists(catalogue_path+filename):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename
    quit()

File = h5py.File(catalogue_path+filename)
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

snap = snapshot_numbers[1]

File.close()

filename = "processed_subhalo_catalogue_" + sim_name + ".hdf5"
File = h5py.File(input_path+filename,"r")

subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]

sgrn = subhalo_group["subgroup_number"][:]
m200 = subhalo_group["m200_host"][:]
mstars_30 = subhalo_group["mass_stars_30kpc"][:]

track_group = File["Snap_"+str(snap)+"/all_progen_tracked_properties"]

mass_pracc_evo = track_group["mass_praccreted"][:]
mass_dm_pr_evo = track_group["mass_diffuse_dm_pristine"][:]

mh_bins_lo = [10.8, 11.8, 12.8]
mh_bins_hi = [11.2, 12.2, 13.2]

h=0.6777
omm = 0.307
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

from utilities_plotting import *

nrow = 1; ncol = 3
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.12,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*1.3],
                    'figure.subplot.left':0.06,
                    'figure.subplot.bottom':0.12,
                    'figure.subplot.top':0.89,
                    'figure.subplot.right':0.99,
                    'font.size':10,
                    'axes.labelsize':11,
                    'legend.fontsize':10})

py.figure()
subplots = panelplot(nrow,ncol)

for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.yaxis.get_major_ticks():
        tick.label1On=True
        tick.label2On=False

    if i == 0:
        mh_bin_lo = mh_bins_lo[0]
        mh_bin_hi = mh_bins_hi[0]
    elif i == 1:
        mh_bin_lo = mh_bins_lo[1]
        mh_bin_hi = mh_bins_hi[1]        
    elif i == 2:
        mh_bin_lo = mh_bins_lo[2]
        mh_bin_hi = mh_bins_hi[2]

    ok = (sgrn == 0) & (np.log10(m200) > mh_bin_lo) & (np.log10(m200) < mh_bin_hi)

    y1 = mass_pracc_evo[ok][:,1:]
    y2 = mass_dm_pr_evo[ok][:,1:]
    t_plot = t_snapshots[1:]
    
    med = False
    if med:
        y1_med = np.median(y1,axis=0)
        y2_med = np.median(y2,axis=0) * fb * dm2tot_factor
    else:
        y1_med = np.mean(y1,axis=0)
        y2_med = np.mean(y2,axis=0) * fb * dm2tot_factor

    py.plot(t_plot, np.log10(y1_med), c="k",label="Gas, first infall",lw=1.2)
    py.plot(t_plot, np.log10(y2_med), c="k",label= r"$\frac{\Omega_b}{\Omega_m-\Omega_b} \times$ Dark matter, first infall",lw=1.2,linestyle='--')

    if i == 0:
        yhi = 10.25
        ylo = yhi - 2.
    elif i == 1:
        yhi = 11.25
        ylo = yhi - 2.
    elif i == 2:
        yhi = 12.25
        ylo = yhi - 2.
    py.ylim((ylo,yhi))
    xlo = 0; xhi = 14
    py.xlim((xlo,xhi))

    offset = 0.65
    jimbo = r"$%3.1f < \log_{10}(M_{200}(z=0) \, / \mathrm{M_\odot}) < %3.1f$" % (mh_bin_lo,mh_bin_hi)
    py.annotate(jimbo,(2.5,ylo+offset))

    py.legend(frameon=False,numpoints=1,loc="lower right")
    py.xlabel(r"$t \, / \mathrm{Gyr}$")
    
    if i == 0:
        py.ylabel(r"$\log_{10}(M \, / \mathrm{M_\odot})$")
    
    z_ticks = [0, 0.5, 1, 2, 3, 4, 6]
    n_ticks = len(z_ticks)
    tick_labels = []
    for m in range(n_ticks):
        tick_labels.append(str(z_ticks[m]))
    t_locs, junk = uc.t_Universe(1./(1.+np.array(z_ticks)),omm,h)

    ax_z = ax.twiny()
    ax_z.set_xticks(t_locs)
    ax_z.set_xticklabels(tick_labels)
    ax_z.set_xlabel(r'$z$')
    ax_z.set_xlim((xlo,xhi))

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

fig_name = "preventative_feedback_cumulative_evo.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
