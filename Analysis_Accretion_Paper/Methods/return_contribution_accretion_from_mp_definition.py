import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# Note - from_mp at halo scale is bugged in the current reduction
# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 28 snapshots variant
#sim_name = "L0025N0376_REF_snap_par_test"

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
f_name_list += ["facc_reaccreted_from_mp_subnorm"+fV_str, "fcool_recooled_from_mp_subnorm"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'figure.subplot.right':0.99,
                    'axes.labelsize':9,
                    'legend.fontsize':10})

# Note using slightly higher xlo here because of inconsistency between 25 Mpc and 100 Mpc for reaccretion at low mass
xlo = 10.0; xhi = 15.0
if "0025" in sim_name:
    xhi = 13.49

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

facc_med = f_med_dict["facc_pristine_subnorm"]
freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
ffof_med = f_med_dict["facc_fof_subnorm"]
fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]

freacc_med_fmp = f_med_dict["facc_reaccreted_from_mp_subnorm"+fV_str]
fcool_recooled_med_fmp = f_med_dict["fcool_recooled_from_mp_subnorm"+fV_str]

ffof_med_fmp = ffof_med + freacc_med - freacc_med_fmp
fcool_galtran_med_fmp = fcool_galtran_med + fcool_recooled_med - fcool_recooled_med_fmp

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        show_z = [0]
        for n in range(len(facc_med[:,0])):
            if n in show_z:

                #py.plot(bin_mh_med[n],np.log10(facc_med[n]),c=c_list[n],linewidth=lw)
                #py.scatter(bin_mh_med[n],np.log10(facc_med[n]),c=c_list[n],edgecolors="none",s=5)

                y1 = freacc_med[n]; y2 = ffof_med[n]; y3 = freacc_med_fmp[n]; y4 = ffof_med_fmp[n]

                ok = bin_mh_mid >= 10.8 # 100 stars for Ref

                temp = np.max(np.where(ok==False)[0])+2
                py.plot(bin_mh_med[n][:temp],np.log10(y1[:temp]),c="k",linestyle="-",linewidth=lw,alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y2[:temp]),c="k",linestyle="--",linewidth=lw,alpha=0.3)

                py.plot(bin_mh_med[n][:temp],np.log10(y3[:temp]),c="r",linestyle="-",linewidth=lw*0.7, alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y4[:temp]),c="r",linestyle="--",linewidth=lw*0.7, alpha=0.3)

                y1[ok==False] = np.nan
                y2[ok==False] = np.nan
                y3[ok==False] = np.nan
                y4[ok==False] = np.nan
                py.plot(bin_mh_med[n][ok],np.log10(y1[ok]),c="k",linestyle="-",linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c="k",linestyle="--",linewidth=lw)

                py.plot(bin_mh_med[n][ok],np.log10(y3[ok]),c="r",linestyle="-",linewidth=lw*0.7)
                py.plot(bin_mh_med[n][ok],np.log10(y4[ok]),c="r",linestyle="--",linewidth=lw*0.7)

        #py.plot(bin_mh_med[n], bin_mh_med[n]-99,c="k",label="Pristine",linewidth=lw)
        py.plot(bin_mh_mid, bin_mh_mid-99,c="k",label="Recycled",linestyle="-",linewidth=lw)
        py.plot(bin_mh_mid, bin_mh_mid-99,c="k",label="Transfer",linestyle="--",linewidth=lw)

        py.plot(bin_mh_mid, bin_mh_mid-99,c="r",label="Recycled (MP-only)",linestyle="-",linewidth=lw*0.7)
        py.plot(bin_mh_mid, bin_mh_mid-99,c="r",label="Transfer (MP-only)",linestyle="--",linewidth=lw*0.7)

        py.legend(loc='upper left',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")
        ylo = -2.96; yhi = 0.5

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(12.2,0.125), size=10)

    if i == 1:
        show_z = [0]
        for n in range(len(freacc_med[:,0])):
            if n in show_z:
                y1 = fcool_recooled_med[n]; y2 = fcool_galtran_med[n]; y3 = fcool_recooled_med_fmp[n]; y4 = fcool_galtran_med_fmp[n]

                py.plot(bin_mh_med[n],np.log10(fcool_pristine_med[n]-99),label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n],linewidth=lw)
                #py.scatter(bin_mh_med[n],np.log10(fcool_pristine_med[n]),c=c_list[n],edgecolors="none",s=5)

                ok = (bin_mh_mid >= 10.8) # 100 stars for Ref
                temp = np.max(np.where(ok==False)[0])+2

                py.plot(bin_mh_med[n][:temp],np.log10(y1[:temp]),c="k",linestyle='-',linewidth=lw,alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y2[:temp]),c="k",linestyle='--',linewidth=lw,alpha=0.3)

                py.plot(bin_mh_med[n][:temp],np.log10(y3[:temp]),c="r",linestyle='-',linewidth=lw,alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y4[:temp]),c="r",linestyle='--',linewidth=lw,alpha=0.3)

                y1[ok==False] = np.nan
                y2[ok==False] = np.nan
                y3[ok==False] = np.nan
                y4[ok==False] = np.nan


                py.plot(bin_mh_med[n][ok],np.log10(y1[ok]),c="k",linestyle='-',linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c="k",linestyle='--',linewidth=lw)

                py.plot(bin_mh_med[n][ok],np.log10(y3[ok]),c="r",linestyle='-',linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y4[ok]),c="r",linestyle='--',linewidth=lw)

        #py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ism}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")
        ylo = -5.; yhi = -0.51

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(12.2,-1.375), size=10)


    
    #for tick in ax.yaxis.get_major_ticks():
    #    tick.label1On=True
        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "return_contribution_gas_accretion_rates_subnorm_"+sim_name+"_from_mp_def.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
