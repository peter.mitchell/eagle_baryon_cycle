import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# No particular reason to use 25 Mpc here, but I did my mistake at the beginning, and now its easier to reduce the y-axis dynamic range to continue that ;) - makes no difference
# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 28 snapshots variant
#sim_name = "L0025N0376_REF_snap_par_test"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip_not_bound_only" # Was checking here if bound only makes a difference - doesn't seem to that I can see

part_mass = "particle_mass"

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5")

#for thing in file_grid:
#    print thing
#quit()

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","facc_pristine_subnorm","facc_fof_subnorm"]
f_name_list += [ "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm", "facc_merge_subnorm"]

fVmax_cuts = [0.125, 0.25, 0.5]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    f_name_list += ["fcool_recooled_subnorm"+fV_str,"facc_reaccreted_subnorm"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'axes.labelsize':9,
                    'legend.fontsize':9})

# Note using slightly higher xlo here because of inconsistency between 25 Mpc and 100 Mpc for reaccretion at low mass
xlo = 10.0; xhi = 15.0
if "0025" in sim_name:
    xhi = 13.49

c_list = ["k","m","b","c","g","y","tab:orange","r"]
ls_list = ["--","-",":"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

facc_med = f_med_dict["facc_pristine_subnorm"]
ffof_med = f_med_dict["facc_fof_subnorm"]
fmerge_med = f_med_dict["facc_merge_subnorm"]

fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]
fgalmerge_med = f_med_dict["fcool_merge_ism_subnorm"] + f_med_dict["fcool_merge_cgm_subnorm"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [2]

    if i == 0:
        for n in range(len(facc_med[:,0])):
            if n in show_z:

                for iV, fVmax_cut in enumerate(fVmax_cuts):
                    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                    freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str][n]
                    facc_tot = facc_med[n] + freacc_med + ffof_med[n] + fmerge_med[n]

                    ok = bin_mh_med[n] >= 10.8 # 100 stars for Ref
                    temp = np.max(np.where(ok==False)[0])+2
                    py.plot(bin_mh_med[n][:temp],np.log10(facc_tot[:temp]),c="k",linewidth=lw,linestyle=ls_list[iV],alpha=0.3)
                    py.plot(bin_mh_med[n][:temp],np.log10(freacc_med[:temp]),c="c",linewidth=lw,linestyle=ls_list[iV],alpha=0.3)

                    freacc_med[ok==False] = np.nan
                    facc_tot[ok==False] = np.nan

                    py.plot(bin_mh_med[n][ok],np.log10(facc_tot[ok]),c="k",linewidth=lw,linestyle=ls_list[iV])
                    py.plot(bin_mh_med[n][ok],np.log10(freacc_med[ok]),c="c",linewidth=lw,linestyle=ls_list[iV])

        label=(r"$%3.1f < z < %3.1f$" % (1./a_max[show_z[0]]-1,1./a_min[show_z[0]]-1))
        py.annotate(label,(10.4,0.), size=10)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")
        ylo = -2.49; yhi = 1.0

        jimbo = r"Total gas accretion (halo)"
        py.plot(bin_mh_mid, bin_mh_mid-99, c="k", linewidth=lw, linestyle='-', label=jimbo)
        jimbo = r"Recycled gas accretion (halo)"
        py.plot(bin_mh_mid, bin_mh_mid-99, c="c", linewidth=lw, linestyle='-',label=jimbo)
        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1)
        py.gca().add_artist(legend1)

        labels = []
        lines = []
        for iV, fVmax_cut in enumerate(fVmax_cuts):
            fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
            label=(r"$V_{\mathrm{rad}} > $"+str(fVmax_cut)+"$ \, V_{\mathrm{max}}$")
            labels.append(label)
            line, = py.plot(bin_mh_mid, bin_mh_mid-99, c="k", linewidth=lw, linestyle=ls_list[iV])
            lines.append(line)

        py.legend(handles=lines,labels=labels,loc='lower right',frameon=False,numpoints=1,fontsize=10)
            

    if i == 1:
        for n in range(len(facc_med[:,0])):
            if n in show_z:

                for iV, fVmax_cut in enumerate(fVmax_cuts):
                    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

                    fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str][n]
                    fcool_tot = fcool_pristine_med[n] + fcool_galtran_med[n] + fcool_recooled_med + fgalmerge_med[n]

                    ok = bin_mh_med[n] >= 10.8 # 100 stars for Ref
                    temp = np.max(np.where(ok==False)[0])+2
                    py.plot(bin_mh_med[n][:temp],np.log10(fcool_tot[:temp]),c="k",linewidth=lw,linestyle=ls_list[iV],alpha=0.3)
                    py.plot(bin_mh_med[n][:temp],np.log10(fcool_recooled_med[:temp]),c="c",linewidth=lw,linestyle=ls_list[iV],alpha=0.3)

                    fcool_recooled_med[ok==False] = np.nan
                    fcool_tot[ok==False] = np.nan

                    py.plot(bin_mh_med[n][ok],np.log10(fcool_tot[ok]),c="k",linewidth=lw,linestyle=ls_list[iV])
                    py.plot(bin_mh_med[n][ok],np.log10(fcool_recooled_med[ok]),c="c",linewidth=lw,linestyle=ls_list[iV])

        label=(r"$%3.1f < z < %3.1f$" % (1./a_max[show_z[0]]-1,1./a_min[show_z[0]]-1))
        py.annotate(label,(10.4,-0.7), size=10)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")
        ylo = -3.25; yhi = 0.25

        jimbo = r"Total gas accretion (ISM)"
        py.plot(bin_mh_mid, bin_mh_mid-99, c="k", linewidth=lw, linestyle='-', label=jimbo)
        jimbo = r"Recycled gas accretion (ISM)"
        py.plot(bin_mh_mid, bin_mh_mid-99, c="c", linewidth=lw, linestyle='-',label=jimbo)
        legend1 = py.legend(loc='upper left',frameon=False,numpoints=1)
        py.gca().add_artist(legend1)


    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "gas_accretion_vmax_effect.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
