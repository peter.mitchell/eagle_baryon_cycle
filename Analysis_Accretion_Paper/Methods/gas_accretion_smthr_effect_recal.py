import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

in7 = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

sim_name_list = ["L0025N0752_Recal","L0025N0752_Recal_m200_smthr_abslo","L0025N0752_Recal_m200_smthr_abshi"]
input_path_list = [in7,in7,in7]
file_type_list = ["new","new","new"]
name_list = ["Fiducial mass cut", "10 times lower mass cut", "10 times higher mass cut"]

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.12,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,4.98],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.08,
                    'figure.subplot.top':0.99,
                    'axes.labelsize':9,
                    'legend.fontsize':9})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]
lw = 1

py.figure()
np.seterr(all='ignore')
nrow =2; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 10.0; xhi = 13.5


########### Load tabulated efficiencies #########################

i_sim = -1
for sim_name,input_path,file_type,name_sim in zip(sim_name_list,input_path_list,file_type_list,name_list):
    i_sim += 1

    # Neistein uses dark matter subhalo mass as "halo mass"
    print "reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"
    if file_type == "old":
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")
    else:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    bin_mh_med = file_grid["bin_mh_med"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
    f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
    #f_name_list += ["facc_merge_subnorm", "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm"]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    ls_list = ['-','--',':','-.',':','--','-']
    #index_a_grid_show = [0,2,4]
    index_a_grid_show = [2]

    for i,ax in enumerate(subplots):
        py.axes(ax)

        if i == 0:

            lines = []; labels = []

            for n in index_a_grid_show:

                y1 = f_med_dict["facc_pristine_subnorm"][n]
                y2 = f_med_dict["facc_reaccreted_subnorm"+fV_str][n]
                y3 = f_med_dict["facc_fof_subnorm"][n]

                ok = bin_mh_mid >= 10.4 # 100 stars for Recal
                temp = np.max(np.where(ok==False)[0])+2
                py.plot(bin_mh_med[n][:temp],np.log10(y1[:temp]),c="b",linestyle=ls_list[i_sim],linewidth=lw,alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y2[:temp]),c="r",linestyle=ls_list[i_sim],linewidth=lw,alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y3[:temp]),c="c",linestyle=ls_list[i_sim],linewidth=lw,alpha=0.3)

                y1[ok==False] = np.nan
                y2[ok==False] = np.nan
                y3[ok==False] = np.nan

                py.plot(bin_mh_med[n][ok],np.log10(y1[ok]),c="b",linestyle=ls_list[i_sim],linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c="r",linestyle=ls_list[i_sim],linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y3[ok]),c="c",linestyle=ls_list[i_sim],linewidth=lw)


                if i_sim == 0:
                    for i_sim2 in range(len(sim_name_list)):
                        labels.append(name_list[i_sim2])
                        line, = py.plot(bin_mh_mid, bin_mh_mid-99, c="k", linestyle=ls_list[i_sim2], linewidth=lw)
                        lines.append(line)

            
                    py.legend(handles=lines,labels=labels,loc='upper left',frameon=False,numpoints=1,fontsize=9)

                    #label=(r"$%3.1f < z < %3.1f$" % (1./a_max[index_a_grid_show[0]]-1,1./a_min[index_a_grid_show[0]]-1))
                    #py.annotate(label,(10.2,-0.5), size=10)

            ylo = -3.0; yhi = 0.0
            py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} \, / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr}^{-1})$")
            #py.legend(loc='lower center',frameon=False,numpoints=1)





        if i == 1:

            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                y1 = f_med_dict["fcool_pristine_subnorm"][n]
                y2 = f_med_dict["fcool_recooled_subnorm"+fV_str][n]
                y3 = f_med_dict["fcool_galtran_subnorm"][n]

                ok = bin_mh_mid >= 10.4 # 100 stars for Recal
                temp = np.max(np.where(ok==False)[0])+2

                py.plot(bin_mh_med[n][:temp],np.log10(y1[:temp]),label=label,c="b",linestyle=ls_list[i_sim],linewidth=lw, alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y2[:temp]),label=label,c="r",linestyle=ls_list[i_sim],linewidth=lw, alpha=0.3)
                py.plot(bin_mh_med[n][:temp],np.log10(y3[:temp]),label=label,c="c",linestyle=ls_list[i_sim],linewidth=lw, alpha=0.3)

                y1[ok==False] = np.nan
                y2[ok==False] = np.nan
                y3[ok==False] = np.nan

                py.plot(bin_mh_med[n][ok],np.log10(y1[ok]),c="b",linestyle=ls_list[i_sim],linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c="r",linestyle=ls_list[i_sim],linewidth=lw)
                py.plot(bin_mh_med[n][ok],np.log10(y3[ok]),c="c",linestyle=ls_list[i_sim],linewidth=lw)


            #py.legend(loc='lower center',frameon=False,numpoints=1)
            py.ylabel(r"$\log( \dot{M}_{\mathrm{in,ism}}^{\mathrm{1st-infall}}  \, /  f_{\mathrm{B}} M_{200}  \, / \mathrm{Gyr}^{-1})$")
            ylo = -4.; yhi = -1.01
            py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

            if i_sim == 0:
                labels = [r"First infall", r"Recycled", r"Transferred"]
                c_list = ["b","r","c"]
                lines = []
                for j in range(3):
                    line, = py.plot(bin_mh_mid, bin_mh_mid-99, c=c_list[j], linewidth=lw)
                    lines.append(line)

                py.legend(handles=lines,labels=labels,loc='lower right',frameon=False,numpoints=1,fontsize=10)

                label=(r"$%3.1f < z < %3.1f$" % (1./a_max[index_a_grid_show[0]]-1,1./a_min[index_a_grid_show[0]]-1))
                py.annotate(label,(11.5,-2.9), size=10)

        #py.axvline(np.log10(969504631.0), c="k", linewidth=0.5, linestyle='-')
        #py.axvline(np.log10(9695046310.0), c="k", linewidth=0.5, linestyle=':')
        #py.axvline(np.log10(96950463.1), c="k", linewidth=0.5, linestyle='--')


        py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


fig_name = "gas_accretion_smthr_effect_recal.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
