import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

# Note to self, there is a choice with this figure r.e. whether to include the CGM of satellite galaxies (either/or to accretion rate, and CGM mass)
# My decision for now is no: we are plotting the efficiency with which the CGM of the central subhalo gets down onto the ISM

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True
complete_cut = "100star"
#complete_cut = "1newstar"

# 100 Mpc ref with 200 snipshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fcool_pristine_mcgm_norm"]
f_name_list += ["fcool_pristine_mpristine_norm","fcool_pristine_mpristine_norm_tscaled"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete and complete_cut == "1newstar":
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str, "ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]
    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.; xhi = 14.5

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

#facc_med = f_med_dict["fcool_pristine_mcgm_norm"]
facc_med = f_med_dict["fcool_pristine_mpristine_norm"]
facc_ts = f_med_dict["fcool_pristine_mpristine_norm_tscaled"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0,1,2,3,4,5,6]
    '''if i == 0:
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                y = facc_med[n]
                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()

                    py.plot(bin_mh_med[n], np.log10(y),c=c_list[n],linewidth=lw,alpha=0.3)
                    y[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw, label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                py.scatter(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],edgecolors="none",s=5)
                
        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^{\mathrm{1st-infall}} / M_{\mathrm{CGM}}^{\mathrm{1st-infall}} \, / \mathrm{Gyr^{-1}})$")
        
        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -3.; yhi = 0.75'''

    if i == 0:
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                y = facc_ts[n]
                if show_incomplete:
                    if complete_cut == "1newstar":
                        ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                    elif complete_cut == "100star":
                        ok = bin_mh_mid >= 10.8
                    else:
                        print "No"; quit()

                    py.plot(bin_mh_med[n], np.log10(y),c=c_list[n],linewidth=lw,alpha=0.3)
                    y[ok==False] = np.nan
                else:
                    ok = bin_mh_mid > 0

                py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw, label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                py.scatter(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],edgecolors="none",s=5)
                
        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^{\mathrm{1st-infall}} \, t / M_{\mathrm{CGM}}^{\mathrm{1st-infall}})$")
        py.axhline(0.0,alpha=0.7,c="k",linestyle='--')
        
        py.legend(loc='lower left',frameon=False,numpoints=1)

        ylo = -2.; yhi = 0.75
        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "infall_efficiency_pristine.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
