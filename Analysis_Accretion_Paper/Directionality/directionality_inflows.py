import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

test_mode = False
subsample = False
n_per_bin = 1000

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 2
snap = 175'''

sim_name = "L0025N0376_REF_200_snip"
snap_list = np.arange(1,201)[::-1]
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub1d = 1
#snap = 177 # z=0.25
snap = 101 # z=2

snip = snip_list[np.argmin(abs(snap_list-snap))]

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)
    if subsample:
        output_base += "_subsample_"+str(n_per_bin)
    output_base += "_test_mode/"

subhalo_prop_names = ["m200_host"]
subhalo_prop_types =  ["float"]

name_list = ["mass_stars_30kpc"]
name_list += ["dmdt_in","dmdt_in_wind","dmdt_in_0p25vmax", "dmdt_in_wind_0p25vmax"]

print "plotting for snapshot", snap


############# Read data ###################################

output_path = output_base + "Snip_"+str(snip)+"/"

sub_input_list = []
input_list = []
for name in name_list:
    input_list.append([])   
for name in subhalo_prop_names:
    sub_input_list.append([])

# Find all the subvolume files (they won't be sorted in order)
subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

if len(subvols) == 0:
    print "There are no subvolume files. Run the pipeline first"
    File.close()
    quit()

# Gather together measurements
for n, subvol in enumerate(subvols):

    if test_mode:
        skip = True
        for subvol_i in subvol_choose:
            if "_"+str(subvol_i)+".hdf5" in subvol:
                skip = False
        if skip:
            continue

    try:
        File_subvol = h5py.File(subvol,"r")
    except:
        print "Couldn't read subvol file", subvol, "moving on"
        continue

    print "Reading", subvol

    try:
        halo_group = File_subvol["directionality_group"]
    except:
        print "Read failed, skipping this subvol"
        continue

    if name_list[-1] in halo_group and name_list[0] in halo_group:
        for i_name, name in enumerate(name_list):

            # Deal with 2d arrays
            if input_list[i_name] == [] and len(halo_group[name][:].shape) ==2:
                input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1]))
            elif input_list[i_name] == [] and len(halo_group[name][:].shape) ==3:
                 input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1],halo_group[name][:].shape[2]))

            input_list[i_name] = np.append(input_list[i_name], halo_group[name][:],axis=0)

        for i_name, name in enumerate(subhalo_prop_names):

            prop_type = subhalo_prop_types[i_name]
            if prop_type == "int":
                if n == 0:
                    sub_input_list[i_name] = np.array([]).astype("int")
                temp = halo_group[name][:]

            elif prop_type == "float":
                temp = halo_group[name][:]
            else:
                "Error: prop type",prop_type,"not understood"
                quit()

            sub_input_list[i_name] = np.append(sub_input_list[i_name], temp)

    else:
        print "subvol",n,"didn't contain any haloes"

    File_subvol.close()

# Do mean stacking for v>0.25Vmax for now, over some finite halo mass interval
mhalo_lo = 11.75
mhalo_hi = 12.25

mhalo = sub_input_list[0]
ok = (np.log10(mhalo) > mhalo_lo) & (np.log10(mhalo) < mhalo_hi)

use_0p25 = False
if not use_0p25:
    for i_name, name in enumerate(name_list):
        if name == "dmdt_in":
            galangle = np.mean(input_list[i_name][ok],axis=0)
        elif name == "dmdt_in_wind":
            galangle_wind = np.mean(input_list[i_name][ok],axis=0)
else:
    for i_name, name in enumerate(name_list):
        if name == "dmdt_in_0p25vmax":
            galangle = np.mean(input_list[i_name][ok],axis=0)
        elif name == "dmdt_in_wind_0p25vmax":
            galangle_wind = np.mean(input_list[i_name][ok],axis=0)
    


n_theta_bins = 72
theta_bins = np.pi * np.arange(-1, 1+2./n_theta_bins, 2./n_theta_bins)
theta_mid = 0.5*(theta_bins[1:] + theta_bins[0:-1])

dr = 0.1
rbins = np.arange(0,1.1,dr)
rmid = [0.05, 0.15,0.25,0.35,0.45,0.55,0.65,0.75,0.85,0.95]
n_rbins = len(rmid)

rmid_grid = np.repeat(rmid, n_theta_bins)
theta_mid_grid = np.repeat(theta_mid, n_rbins)
theta_mid_grid = np.zeros_like(rmid_grid)
n = -1
for i in range(len(rmid_grid)):
    n += 1
    theta_mid_grid[i] = theta_mid[n]
    if n == len(theta_mid)-1:
        n = -1

galangle_grid = np.ravel(galangle)
galangle_grid_wind = np.ravel(galangle_wind)

from utilities_plotting import *

nrow = 1; ncol = 2

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.2,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*1.3],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'font.size':10,
                    'legend.fontsize':10,
                    'axes.labelsize':11})
 
#py.figure()
np.seterr(all='ignore')
fig, axes = py.subplots(nrow,ncol)

for i,ax in enumerate(axes.flat):
    py.axes(ax)

    if i == 0:
        galangle_grid_use = galangle_grid
        #for tick in ax.xaxis.get_major_ticks():
        #    tick.label1On = False
        image = py.hist2d(rmid_grid, theta_mid_grid, bins = [rbins,theta_bins], weights=np.log10(-galangle_grid),cmap="viridis")
        #image[-1].norm.vmin = -2.0
        #image[-1].norm.vmax = -0.5

    else:
        galangle_grid_use = galangle_grid_wind
        image2 = py.hist2d(rmid_grid, theta_mid_grid, bins = [rbins,theta_bins], weights=np.log10(-galangle_grid_wind),cmap="viridis")
        #image2[-1].norm.vmin = -3.0
        #image2[-1].norm.vmax = -1.5
    
    py.xlabel(r"$r \, / R_{\mathrm{vir}}$")
    py.ylabel(r"$\mathrm{Galactocentric \, Angle}$")

    y_ticks = np.array([-np.pi, -np.pi*0.5, 0.0, np.pi*0.5, np.pi])
    tick_labels = [r"$-\pi$", r"$-\pi /2$", r"$0$", r"$\pi /2$", r"$\pi$"]

    ax.set_yticks(y_ticks)
    ax.set_yticklabels(tick_labels)

    if i == 0:
        cbar_label = r'$\log_{10}(\dot{M}_{\mathrm{in}} \, / \mathrm{M_\odot yr^{-1}})$'
        fig.subplots_adjust(top=0.86)
        cbar_ax = fig.add_axes([0.05, 0.95, 0.45, 0.03])
        cbar = fig.colorbar(mappable=image[3], cax=cbar_ax, label=cbar_label,orientation = "horizontal")
        cbar.ax.tick_params(labelsize=6)

    if i == 1:
        cbar_ax2 = fig.add_axes([0.55, 0.95, 0.45, 0.03])
        cbar2 = fig.colorbar(mappable=image2[3], cax=cbar_ax2, label=cbar_label,orientation = "horizontal")
        cbar2.ax.tick_params(labelsize=6)

    #py.colorbar(label="test",orientation="horizontal")
        
fig_name = "directionality_inflows_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
