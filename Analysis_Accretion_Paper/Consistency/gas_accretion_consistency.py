import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

in6 = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
in7 = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

#sim_name_list = ["L0025N0376_REF_snap_par_test2","L0025N0376_REF_snap","L0025N0376_REF_snap_par_test"]
#input_path_list = [in7,in7,in7]
#file_type_list = ["new","new","new"]
#name_list = ["No M200 cut, no sat cut","M200 cut, no sat cut", "M200 cut, sat cut"]

#sim_name_list = ["L0025N0376_REF_snap","L0025N0376_REF_snap_par_test","L0025N0376_REF_snap_par_test2"]
#input_path_list = [in7,in7,in7]
#file_type_list = ["new","new","new"]
#name_list = ["absolute smooth acr thr", "Fractional smooth acc thr","10 x absolute smooth acc thr"]

sim_name_list = ["L0025N0376_REF_200_snip","L0025N0376_REF_200_snip_track_star"]
input_path_list = [in7,in7]
file_type_list = ["new","new"]
name_list = ["fid", "track star"]

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!


from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[6.64*2,4.98],
                    'figure.subplot.left':0.17,
                    'figure.subplot.bottom':0.17,
                    'figure.subplot.top':0.98,
                    'font.size':7,
                    'legend.fontsize':6})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 4
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0


########### Load tabulated efficiencies #########################

i_sim = -1
for sim_name,input_path,file_type,name_sim in zip(sim_name_list,input_path_list,file_type_list,name_list):
    i_sim += 1

    # Neistein uses dark matter subhalo mass as "halo mass"
    print "reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"
    if file_type == "old":
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5","r")
    else:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
    f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
    f_name_list += ["facc_merge_subnorm", "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm"]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    ls_list = [':','--','-','-.',':','--','-']
    index_a_grid_show = [0,2,4,6]
    #index_a_grid_show = [0,1,2,3,4,5,6]
    #index_a_grid_show = [5,6,7,8]

    for i,ax in enumerate(subplots):
        py.axes(ax)

        if i == 0:

            ism_in = f_med_dict["fcool_pristine_subnorm"]

            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(ism_in[n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
                py.scatter(bin_mh_mid,np.log10(ism_in[n]),c=c_list[n],edgecolors="none",s=5)

            py.legend(loc='upper right',frameon=False,numpoints=1)
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,ism}}^{\mathrm{pristine}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            ylo = -2.75; yhi = 0.0

        if i == 1:

            ism_in = f_med_dict["fcool_galtran_subnorm"]

            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(ism_in[n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
                py.scatter(bin_mh_mid,np.log10(ism_in[n]),c=c_list[n],edgecolors="none",s=5)

            py.legend(loc='upper right',frameon=False,numpoints=1)
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,ism}}^{galtran} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            ylo = -2.75; yhi = 0.0

        if i == 2:

            ism_in = f_med_dict["fcool_recooled_subnorm"+fV_str]

            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(ism_in[n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
                py.scatter(bin_mh_mid,np.log10(ism_in[n]),c=c_list[n],edgecolors="none",s=5)

            py.legend(loc='upper right',frameon=False,numpoints=1)
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,ism}}^{recool} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            ylo = -2.75; yhi = 0.0

        if i == 3:

            ism_in = f_med_dict["fcool_merge_ism_subnorm"]+f_med_dict["fcool_merge_cgm_subnorm"]

            for n in index_a_grid_show:

                c_index = float(n)/(len(a_grid)-1)

                if i_sim == 0:
                    label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(ism_in[n]),label=label,c=c_list[n],linestyle=ls_list[i_sim])
                py.scatter(bin_mh_mid,np.log10(ism_in[n]),c=c_list[n],edgecolors="none",s=5)

            py.legend(loc='upper right',frameon=False,numpoints=1)
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,ism}}^{merge} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            ylo = -2.75; yhi = 0.0


        if i == 4:

            halo_in = f_med_dict["facc_pristine_subnorm"]

            for n in index_a_grid_show:

                if n == 0:
                    label = name_sim
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                py.scatter(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],edgecolors="none",s=5)

            ylo = -2.25; yhi = 0.5
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,halo}}^{\mathrm{pristine}} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            py.legend(loc='upper right',frameon=False,numpoints=1)


        if i == 5:

            halo_in = f_med_dict["facc_fof_subnorm"]


            for n in index_a_grid_show:

                if n == 0:
                    label = sim_name
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                py.scatter(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],edgecolors="none",s=5)

            ylo = -2.25; yhi = 0.5
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,halo}}^{fof} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            py.legend(loc='upper right',frameon=False,numpoints=1)

        if i == 6:

            halo_in = f_med_dict["facc_reaccreted_subnorm"+fV_str]


            for n in index_a_grid_show:

                if n == 0:
                    label = sim_name
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                py.scatter(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],edgecolors="none",s=5)

            ylo = -2.25; yhi = 0.5
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,halo}}^{reaccreted} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            py.legend(loc='upper right',frameon=False,numpoints=1)

        if i == 7:

            halo_in = f_med_dict["facc_merge_subnorm"]


            for n in index_a_grid_show:

                if n == 0:
                    label = sim_name
                else:
                    label = ""

                py.plot(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],linestyle=ls_list[i_sim],label=label)
                py.scatter(bin_mh_mid,np.log10(halo_in[n]),c=c_list[n],edgecolors="none",s=5)

            ylo = -2.25; yhi = 0.5
            py.ylabel(r"$\log(\langle \dot{M}_{\mathrm{in,halo}}^{merge} \rangle \, / \langle f_{\mathrm{B}} M_{\mathrm{H}} \rangle \, / \mathrm{Gyr}^{-1})$")
            py.legend(loc='upper right',frameon=False,numpoints=1)


        if i >3:
            py.xlabel(r"$\log(M_{\mathrm{H}} \, / \mathrm{M_\odot})$")

        #py.ylim((ylo, yhi))
        py.xlim((xlo, xhi))


if test_mode:
    fig_name = "gas_accretion_consistency_test_mode.pdf"
else:
    fig_name = "gas_accretion_consistency.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
