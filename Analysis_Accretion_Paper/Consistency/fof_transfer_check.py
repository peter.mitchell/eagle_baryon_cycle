import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 snapshots
sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 28 snapshots variant
#sim_name = "L0025N0376_REF_snap_par_test"

# 25 Mpc ref with 50 snipshots
#sim_name = "L0025N0376_REF_50_snip"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_test_mode.hdf5")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+".hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

filename = "processed_subhalo_catalogue_" + sim_name
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename,"r")

snap1 = 27
subhalo_group = File["Snap_"+str(snap1)+"/subhalo_properties"]
m200 = subhalo_group["m200_host"][:]
sgrn = subhalo_group["subgroup_number"][:]
fof = subhalo_group["mass_fof_transfer"][:]
sfr = subhalo_group["mass_new_stars_init"][:]

ok = sgrn==0
y1 = (fof/m200)[ok]
#y1 = (fof)[ok]
#y1 = (sfr)[ok]
#x1 = m200[ok]

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.0

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

facc_med = f_med_dict["facc_pristine_subnorm"]
freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
ffof_med = f_med_dict["facc_fof_subnorm"]
fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0]
    if i == 0:
        for n in range(len(facc_med[:,0])):
            if n in show_z:

                py.plot(bin_mh_mid,np.log10(ffof_med[n]),c=c_list[n],linestyle=":",linewidth=lw)

        py.plot(bin_mh_mid, bin_mh_mid-99,c="k",label="Transfer",linestyle=":",linewidth=lw)

        py.legend(loc='upper left',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")
        ylo = -3.49; yhi = 0.5

    if i == 1:
        for n in range(len(freacc_med[:,0])):
            if n in show_z:

                py.scatter(np.log10(x1),np.log10(y1),c=c_list[n])

        py.legend(loc='upper right',frameon=False,numpoints=1)
        #py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")
        ylo = -5.5; yhi = -0.5
    
    #for tick in ax.yaxis.get_major_ticks():
    #    tick.label1On=True
        
    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

py.show()
