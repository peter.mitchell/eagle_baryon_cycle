import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_statistics as us

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"
part_mass_actual = 1.81*10**6

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

bins_r_track_ej = np.array([0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.3, 2.6, 3.0, 4.0, 5.0, 10.0, 1e9])
bins_r_track_wi = np.array([0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.3, 1.6, 2.0, 3.0, 4.0, 5.0, 10.0, 1e9])

rmax_mid_ej = 0.5 * (bins_r_track_ej[0:-1] + bins_r_track_ej[1:])
rmax_mid_wi = 0.5 * (bins_r_track_wi[0:-1] + bins_r_track_wi[1:])

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]
R200_med = file_grid["R200_med"][:]

ind = np.argmin(abs(bin_mh_mid-12))
print R200_med[0,ind]
print file_grid["vmax_med"][:][0,ind]
vmax = file_grid["vmax_med"][:][0,ind]
Gyr = 365.25*1e9*24*60**2
kpc = 3.09*10**19
dr = 0.25 * vmax *1e3* t_grid[0]*0.1*0.25*Gyr / kpc
drR = dr / R200_med[0,ind]
print "dr, dr/R, z=0, mhalo=1e12"
print dr
print drR

print "m200, dr/R as f(M_200) at z=0"
print bin_mh_mid
print 0.25 * file_grid["vmax_med"][:][0] *1e3* t_grid[0]*0.1*0.25*Gyr / kpc / R200_med[0]

dr_z1 = 0.25 * file_grid["vmax_med"][:][2,ind] *1e3* t_grid[2]*0.1*0.25*Gyr / kpc
dr_z2 = 0.25 * file_grid["vmax_med"][:][3,ind] *1e3* t_grid[3]*0.1*0.25*Gyr / kpc
print "dr, z=1,2, m200 = 1e12"
print dr_z1, dr_z2

print "m200, dr/R as f(M_200) at z=1,2"
print bin_mh_mid
print 0.25 * file_grid["vmax_med"][:][2] *1e3* t_grid[0]*0.1*0.25*Gyr / kpc / R200_med[2]
print 0.25 * file_grid["vmax_med"][:][3] *1e3* t_grid[0]*0.1*0.25*Gyr / kpc / R200_med[3]

quit()

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["f_r_wind", "f_r_ejecta","f_r_wind_ret","f_r_ejecta_ret"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.17,'figure.subplot.hspace':0.22,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.1,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':10,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1

for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    #show_mh = 9 #10.9
    show_mh = 14 #11.9

    show_z = [0,2,3]

    if i == 0:
        
        for iz in range(len(f_med_dict["f_r_ejecta"][:,0])):
            if iz not in show_z:
                continue

            x = 0.5*(bins_r_track_ej[1:] + bins_r_track_ej[0:-1])
            x[0] = 1.0
            y = np.cumsum(f_med_dict["f_r_ejecta"][iz][show_mh])
            y *= 1./y[-2]

            label = r"$%3.1f < z < %3.1f, \,\, \bar{R}_{\mathrm{vir}} = %2i \, \mathrm{pkpc}$" % (1./a_max[iz]-1,1./a_min[iz]-1,R200_med[iz][show_mh])

            py.plot(x,y,c=c_list[iz],linewidth=lw,label=label)

        py.ylabel(r"$f(<r/R_{\mathrm{vir}}) \, [\,\mathrm{Halo-scale}\,]$")

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(3.3,0.4))

        py.legend(loc='lower right',frameon=False,numpoints=1)

        xlo = 1.0; xhi= 8.0
        ylo = 0.0; yhi = np.max(y)*1.05

        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")


    if i == 1:

        for iz in range(len(f_med_dict["f_r_ejecta"][:,0])):
            if iz not in show_z:
                continue

            x = 0.5*(bins_r_track_ej[1:] + bins_r_track_ej[0:-1])           
            y = np.cumsum(f_med_dict["f_r_ejecta_ret"][iz][show_mh])
            y *= 1./y[-2]

            x = np.append([0.75],x)
            y = np.append([0],y)

            label = r"$%3.1f < z < %3.1f, \,\, \bar{R}_{\mathrm{vir}} = %2i \, \mathrm{pkpc}$" % (1./a_max[iz]-1,1./a_min[iz]-1,R200_med[iz][show_mh])

            py.plot(x,y,c=c_list[iz],linewidth=lw,label=label)
        
        py.ylabel(r"$f(<r_{\mathrm{max}}/R_{\mathrm{vir}}) \, [\,\mathrm{Halo-scale}\,]$")

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(1.3,0.4))

        py.legend(loc='lower right',frameon=False,numpoints=1)

        xlo = 0.75; xhi= 2.2
        ylo = 0.0; yhi = np.max(y)*1.05

        py.xlabel(r"$r_{\mathrm{max}} \, / R_{\mathrm{vir}}$")
    

    if i == 2:

        for iz in range(len(f_med_dict["f_r_ejecta"][:,0])):
            if iz not in show_z:
                continue
                
            x = 0.5*(bins_r_track_wi[1:] + bins_r_track_wi[0:-1])
            y = np.cumsum(f_med_dict["f_r_wind"][iz][show_mh])
            y *= 1./y[-2]

            x = np.append([0.0],x)
            y = np.append([0],y)

            label = r"$%3.1f < z < %3.1f, \,\, \bar{R}_{\mathrm{vir}} = %2i \, \mathrm{pkpc}$" % (1./a_max[iz]-1,1./a_min[iz]-1,R200_med[iz][show_mh])

            py.plot(x,y,c=c_list[iz],linewidth=lw,label=label)

        py.ylabel(r"$f(<r/R_{\mathrm{vir}}) \, [\,\mathrm{Galaxy-scale}\,]$")

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(2.7,0.4))

        py.legend(loc='lower right',frameon=False,numpoints=1)

        xlo = 0.0; xhi= 8.0
        ylo = 0.0; yhi = np.max(y)*1.05

        py.xlabel(r"$r \, / R_{\mathrm{vir}}$")

    if i == 3:

        for iz in range(len(f_med_dict["f_r_ejecta"][:,0])):
            if iz not in show_z:
                continue

            x = 0.5*(bins_r_track_wi[1:] + bins_r_track_wi[0:-1])
            y = np.cumsum(f_med_dict["f_r_wind_ret"][iz][show_mh])
            y *= 1./y[-2]

            x = np.append([0.0],x)
            y = np.append([0],y)

            label = r"$%3.1f < z < %3.1f, \,\, \bar{R}_{\mathrm{vir}} = %2i \, \mathrm{pkpc}$" % (1./a_max[iz]-1,1./a_min[iz]-1,R200_med[iz][show_mh])

            py.plot(x,y,c=c_list[iz],linewidth=lw,label=label)

        py.ylabel(r"$f(<r_{\mathrm{max}}/R_{\mathrm{vir}}) \, [\,\mathrm{Galaxy-scale}\,]$")

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(0.5,0.4))

        py.legend(loc='lower right',frameon=False,numpoints=1)

        xlo = 0.; xhi= 1.5
        ylo = 0.0; yhi = np.max(y)*1.05

        py.xlabel(r"$r_{\mathrm{max}} \, / R_{\mathrm{vir}}$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

fig_name = "rmax_dists.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
