import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# Recal analysis doesn't have mergers atm
include_mergers = False

show_incomplete = True
complete_cut = "100star"

show_dm = False # 6/3/2020 - yes I check the DM are consistent with each other ;)

sim_name_list = ["L0025N0376_REF_200_snip", "L0025N0752_Recal"]
label_list = ["Ref, L0025N0376", "Recal, L0025N0752"]
star_lim = [10.8, 10.4] # log M_200 at z=0 corresponding to 100 star particles

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
output = []

for sim_name in sim_name_list:
    if test_mode:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
    else:
        print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    bin_mh_med = file_grid["bin_mh_med"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["fcool_pristine_subnorm","fcool_galtran_subnorm","fcool_recooled_subnorm"+fV_str]
    f_name_list += ["facc_pristine_subnorm","facc_fof_subnorm","facc_reaccreted_subnorm"+fV_str]
    if show_dm:
        f_name_list += ["facc_dm_dmnorm"]
    if include_mergers:
        f_name_list += ["facc_merge_subnorm", "fcool_merge_ism_subnorm", "fcool_merge_cgm_subnorm", "facc_dm_merge_dmnorm"]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    output_i = bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict
    output.append(output_i)

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 10.; xhi = 13.49

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1
ls_list = ["-", "--","-.",":"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    #show_z = [0,1,2,3,4,5,6]

    if i == 0:
        show_z = [0,2,4]
        for n in range(len(a_grid)):
            if n in show_z:
                
                for i_sim, sim_name in enumerate(sim_name_list):
                    
                    bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict = output[i_sim]
                    facc_med = f_med_dict["facc_pristine_subnorm"]
                    freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
                    ffof_med = f_med_dict["facc_fof_subnorm"]

                    facc_tot = facc_med + freacc_med + ffof_med

                    if include_mergers:
                        facc_merger = f_med_dict["facc_merge_subnorm"]
                        facc_tot += facc_merger

                    y = facc_tot[n]

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        elif complete_cut == "100star":
                            ok = bin_mh_mid >= star_lim[i_sim]
                        else:
                            print "No"; quit()

                        temp = np.max(np.where(ok==False)[0])+2
                        py.plot(bin_mh_med[n][:temp], np.log10(y[:temp]),c=c_list[n],linewidth=lw,alpha=0.3,linestyle=ls_list[i_sim])
                        y[ok==False] = np.nan
                    else:
                        ok = bin_mh_mid > 0

                    if i_sim == 0:
                        py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                    else:
                        py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

                    if show_dm:
                        facc_dm = f_med_dict["facc_dm_dmnorm"][:]
                        py.plot(bin_mh_med[n], np.log10(facc_dm[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim+2])

        legend1 = py.legend(loc='lower right',frameon=False,numpoints=1,prop={'size': 9})
        py.gca().add_artist(legend1)

        lines = []; labels = []
        for i_sim, sim_name in enumerate(label_list):
            l, = py.plot(bin_mh_med[n],bin_mh_med[n]-99,c="k",linewidth=lw, ls=ls_list[i_sim])
            lines.append(l)
            labels.append(sim_name)

        py.legend(handles=lines,labels=labels, loc="upper left",frameon=False,numpoints=1)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        ylo = -2.49; yhi = 0.5

    if i == 1:
        show_z = [2]
        for n in range(len(a_grid)):
            if n in show_z:

                z_label = "$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)
                py.annotate(z_label, (10.1,-2.75),size=9)
                
                for i_sim, sim_name in enumerate(sim_name_list):

                    if i_sim == 0:
                        ls = "-"
                    if i_sim == 1:
                        ls= "--"
                    
                    bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict = output[i_sim]
                    facc_med = f_med_dict["facc_pristine_subnorm"]
                    freacc_med = f_med_dict["facc_reaccreted_subnorm"+fV_str]
                    ffof_med = f_med_dict["facc_fof_subnorm"]

                    if include_mergers:
                        facc_merger = f_med_dict["facc_merge_subnorm"]
                        facc_tot += facc_merger

                    y1 = facc_med[n]; y2 = freacc_med[n]; y3 = ffof_med[n]

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        elif complete_cut == "100star":
                            ok = bin_mh_mid >= star_lim[i_sim]
                        else:
                            print "No"; quit()

                        temp = np.max(np.where(ok==False)[0])+2
                        py.plot(bin_mh_med[n][:temp], np.log10(y1[:temp]),c="k",linewidth=lw,alpha=0.3,linestyle=ls)
                        py.plot(bin_mh_med[n][:temp], np.log10(y2[:temp]),c="c",linewidth=lw,alpha=0.3,linestyle=ls)
                        py.plot(bin_mh_med[n][:temp], np.log10(y3[:temp]),c="r",linewidth=lw,alpha=0.3,linestyle=ls)
                        y[ok==False] = np.nan
                    else:
                        ok = bin_mh_mid > 0

                    py.plot(bin_mh_med[n][ok],np.log10(y1[ok]),c="k",linewidth=lw,linestyle=ls)
                    py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c="c",linewidth=lw,linestyle=ls)
                    py.plot(bin_mh_med[n][ok],np.log10(y3[ok]),c="r",linewidth=lw,linestyle=ls)

        py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,linestyle="-",label="First infall")
        py.plot(bin_mh_mid,bin_mh_mid-99,c="c",linewidth=lw,linestyle="-",label="Recycled")
        py.plot(bin_mh_mid,bin_mh_mid-99,c="r",linewidth=lw,linestyle="-",label="Transfer")
                    
        legend2 = py.legend(loc='lower right',frameon=False,numpoints=1,prop={'size': 9})
        py.gca().add_artist(legend2)

        l1, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw)
        l2, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,linestyle='--')
        lines = [l1,l2]
        labels = [label_list[0],label_list[1]]

        py.legend(handles=lines,labels=labels, loc="lower left",frameon=False,numpoints=1)

        
        ylo = -3.49; yhi = -0.5
        

    if i == 2:
        show_z = [0,2,4]
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):
                    
                    bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict = output[i_sim]
                    fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
                    fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
                    fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]

                    fcool_tot = fcool_pristine_med + fcool_recooled_med + fcool_galtran_med

                    if include_mergers:
                        fcool_merge = f_med_dict["fcool_merge_ism_subnorm"] + f_med_dict["fcool_merge_cgm_subnorm"]
                        fcool_tot += fcool_merge

                    y = fcool_tot[n]

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        elif complete_cut == "100star":
                            ok = bin_mh_mid >= star_lim[i_sim]
                        else:
                            print "No"; quit()

                        temp = np.max(np.where(ok==False)[0])+2
                        py.plot(bin_mh_med[n][:temp], np.log10(y[:temp]),c=c_list[n],linewidth=lw,alpha=0.3,linestyle=ls_list[i_sim])
                        y[ok==False] = np.nan
                    else:
                        ok = bin_mh_mid > 0

                    if i_sim == 0:
                        py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim],label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                    else:
                        py.plot(bin_mh_med[n][ok],np.log10(y[ok]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

        legend2 = py.legend(loc='upper right',frameon=False,numpoints=1,prop={'size': 9})
        py.gca().add_artist(legend2)

        lines = []; labels = []
        for i_sim, sim_name in enumerate(label_list):
            l, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw, ls=ls_list[i_sim])
            lines.append(l)
            labels.append(sim_name)

        py.legend(handles=lines,labels=labels, loc="upper left",frameon=False,numpoints=1)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}} / f_{\mathrm{B}} M_{200} \, / \mathrm{Gyr^{-1}})$")

        ylo = -2.75; yhi = 0.25

    if i == 3:
        xhi = 13.5
        show_z = [2]
        for n in range(len(a_grid)):
            if n in show_z:

                z_label = (r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1))
                py.annotate(z_label, (10.1,-3.27),size=9)

                for i_sim, sim_name in enumerate(sim_name_list):

                    if i_sim == 0:
                        ls = "-"
                    if i_sim == 1:
                        ls= "--"
                    
                    bin_mh_mid, bin_mh_med, a_max, a_min, f_med_dict = output[i_sim]
                    fcool_pristine_med = f_med_dict["fcool_pristine_subnorm"]
                    fcool_recooled_med = f_med_dict["fcool_recooled_subnorm"+fV_str]
                    fcool_galtran_med = f_med_dict["fcool_galtran_subnorm"]

                    if include_mergers:
                        fcool_merge = f_med_dict["fcool_merge_ism_subnorm"] + f_med_dict["fcool_merge_cgm_subnorm"]

                    y1 = fcool_pristine_med[n]
                    y2 = fcool_recooled_med[n]
                    y3 = fcool_galtran_med[n]

                    if show_incomplete:
                        if complete_cut == "1newstar":
                            ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                        elif complete_cut == "100star":
                            ok = bin_mh_mid >= star_lim[i_sim]
                        else:
                            print "No"; quit()

                        temp = np.max(np.where(ok==False)[0])+2
                        py.plot(bin_mh_med[n][:temp], np.log10(y1[:temp]),c="k",linewidth=lw,alpha=0.3,linestyle=ls)
                        py.plot(bin_mh_med[n][:temp], np.log10(y2[:temp]),c="c",linewidth=lw,alpha=0.3,linestyle=ls)
                        py.plot(bin_mh_med[n][:temp], np.log10(y3[:temp]),c="r",linewidth=lw,alpha=0.3,linestyle=ls)
                        y[ok==False] = np.nan
                    else:
                        ok = bin_mh_mid > 0

                    py.plot(bin_mh_med[n][ok],np.log10(y1[ok]),c="k",linewidth=lw,linestyle=ls)
                    py.plot(bin_mh_med[n][ok],np.log10(y2[ok]),c="c",linewidth=lw,linestyle=ls)
                    py.plot(bin_mh_med[n][ok],np.log10(y3[ok]),c="r",linewidth=lw,linestyle=ls)

        py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,linestyle="-",label="First infall")
        py.plot(bin_mh_mid,bin_mh_mid-99,c="c",linewidth=lw,linestyle="-",label="Recycled")
        py.plot(bin_mh_mid,bin_mh_mid-99,c="r",linewidth=lw,linestyle="-",label="Transfer")
                    
        legend2 = py.legend(loc='lower right',frameon=False,numpoints=1,prop={'size': 9})
        py.gca().add_artist(legend2)

        l1, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw)
        l2, = py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,linestyle='--')
        lines = [l1,l2]
        labels = [label_list[0],label_list[1]]

        py.legend(handles=lines,labels=labels, loc="lower left",frameon=False,numpoints=1)

        ylo = -4.0; yhi = -1.

        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "gas_accretion_rates_subnorm_sim_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
