import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0752_Recal"

sim_name_list = ["L0025N0376_REF_200_snip",  "L0025N0752_Recal"]
label_list = ["Ref (25)", "Recal (25)"]

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################

output = []

for sim_name in sim_name_list:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    f_name_list = ["fcool_recooled_return"+fV_str, "facc_reaccreted_return_tscaled_"+fV_str,"fcool_recooled_return_tscaled_"+fV_str,"facc_reaccreted_return"+fV_str]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    output_i = [bin_mh_mid, a_min, a_max, f_med_dict]
    output.append(output_i)

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1
ls_list = ["-","--"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    #show_z = [0,1,2,3,4,5,6]
    show_z = [0,2,4]
    if i == 0:
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):

                    bin_mh_mid, a_min, a_max, f_med_dict = output[i_sim]

                    freacc = f_med_dict["facc_reaccreted_return"+fV_str]
                    
                    if i_sim == 0:
                        py.plot(bin_mh_mid,np.log10(freacc[n]),c=c_list[n],linewidth=lw,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),linestyle=ls_list[i_sim])
                    else:
                        py.plot(bin_mh_mid,np.log10(freacc[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}}^{\mathrm{recycled}} / M_{\mathrm{ej,halo}} \, / \mathrm{Gyr^{-1}})$")

        py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -2.49; yhi = 1.0
        

    if i == 1:
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):

                    bin_mh_mid, a_min, a_max, f_med_dict = output[i_sim]

                    freacc_tscaled = f_med_dict["facc_reaccreted_return_tscaled_"+fV_str]

                    if i_sim == 0:
                        py.plot(bin_mh_mid,np.log10(freacc_tscaled[n]),c=c_list[n],linewidth=lw,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),linestyle=ls_list[i_sim])
                    else:
                        py.plot(bin_mh_mid,np.log10(freacc_tscaled[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}}^{\mathrm{recycled}} \, t \, / M_{\mathrm{ej,halo}})$")
        py.axhline(0.0,linestyle='--',alpha=0.7,linewidth=lw)

        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -2.49; yhi = 1.0

    if i == 2:
        for n in range(len(a_grid)):
            if n in show_z:

                for i_sim, sim_name in enumerate(sim_name_list):

                    bin_mh_mid, a_min, a_max, f_med_dict = output[i_sim]

                    frecool = f_med_dict["fcool_recooled_return"+fV_str]
                    
                    if n == show_z[0]:
                        py.plot(bin_mh_mid,np.log10(frecool[n]),c=c_list[n],linewidth=lw,label=label_list[i_sim],linestyle=ls_list[i_sim])
                    else:
                        py.plot(bin_mh_mid,np.log10(frecool[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^{\mathrm{recycled}} / M_{\mathrm{ej,ISM}} \, / \mathrm{Gyr^{-1}})$")

        py.legend(loc='upper right',frameon=False,numpoints=1)
      
        ylo = -3.0; yhi = 0.5
        

    if i == 3:
        for n in range(len(a_grid)):
            if n in show_z:
                for i_sim, sim_name in enumerate(sim_name_list):

                    bin_mh_mid, a_min, a_max, f_med_dict = output[i_sim]

                    frecool_tscaled = f_med_dict["fcool_recooled_return_tscaled_"+fV_str]
                    
                    py.plot(bin_mh_mid,np.log10(frecool_tscaled[n]),c=c_list[n],linewidth=lw,linestyle=ls_list[i_sim])

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^{\mathrm{recycled}} \, t \, / M_{\mathrm{ej,ISM}})$")
        py.axhline(0.0,linestyle='--',alpha=0.7,linewidth=lw)

        #py.legend(loc='upper right',frameon=False,numpoints=1)
      
        ylo = -3.0; yhi = 0.5

        
    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "return_efficiency_sim_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
