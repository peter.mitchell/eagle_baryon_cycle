# Plots the z=3>z=0 integrated mass of recooled gas, as a function of return time for the returning gas

import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_statistics as us

show_c16 = True
show_aa17 = True
show_g19 = True
show_T19 = True
show_u14 = True

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
catalogue_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip" # t_bins are messed up in current analysis

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

# Get the snapshot info
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"

if not os.path.exists(catalogue_path+filename):
    print "Can't read subhalo catalogue at path:", catalogue_path+filename
    quit()

File = h5py.File(catalogue_path+filename,"r")
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
File.close()

snap_use = snapshot_numbers[1] # Snapshot that cumulative measurements are stored on

#Load pre-computed return rates computed within Cumulative_Measurements/cumulative_measurements_tret.py
filename = "processed_subhalo_catalogue_" + sim_name
filename += ".hdf5"

if not os.path.exists(input_path+filename):
    print "Can't read subhalo catalogue at path:", input_path+filename
    quit()

File = h5py.File(input_path+filename,"r")
group_name = "all_progen_tracked_properties"
subhalo_group = File["Snap_"+str(snap_use)+"/"+group_name]

t_bins = subhalo_group["t_bins"][:]
m200_bins = subhalo_group["m200_bins"][:]
m200 = subhalo_group["m200"][:]
mass_recool_prebinned = subhalo_group["mass_recool_m200_t_binned"][:]
mass_recool = subhalo_group["mass_recool_t_binned"][:]

File.close()

# For each mass bin compute the mean median return time

t_mid = 0.5*(t_bins[1:]+t_bins[0:-1])
m200_mid = 0.5*(m200_bins[1:]+m200_bins[0:-1])

t_mid_med = np.zeros(len(m200_mid))+np.nan
for n in range(len(t_mid_med)):
    if mass_recool_prebinned[n].max() > 0:
        t_mid_med[n] = us.Weighted_Percentile(t_mid, weight_in = mass_recool_prebinned[n],perc=0.5)


tret_med = np.zeros(len(mass_recool[:,0]))
for n in range(len(mass_recool[:,0])):
    tret_med[n] = us.Weighted_Percentile(t_mid,weight_in = mass_recool[n],perc=0.5)

tret_mean_med = np.zeros(len(m200_mid))
for n in range(len(m200_mid)):
    ok = (np.log10(m200) > m200_bins[n]) & (np.log10(m200) < m200_bins[n+1])
    tret_mean_med[n] = np.mean(tret_med[ok])

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.21,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.15,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':7,
                    'axes.labelsize':10,
                    'legend.fontsize':8})

# Keep 9.5 - to make the point that FIRE distn is flat
xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

for i,ax in enumerate(subplots):
    py.axes(ax)

    #for tick in ax.xaxis.get_major_ticks():
    #    tick.label1On=True

    complete = m200_mid >= 10.8
    py.plot( m200_mid , tret_mean_med, c="k",linewidth=lw, alpha=0.3)
    py.plot( m200_mid[complete] , tret_mean_med[complete], c="k",linewidth=lw, label=r"EAGLE")

    if show_c16:
        logmh, tret_med = np.loadtxt("christensen16_tret_med_m200.dat",unpack=True)
        py.scatter(logmh, tret_med,c="r",edgecolors="none",s=12,label=r"C16")

    if show_aa17:
        mh, tret_med = np.loadtxt("angles_alcazar17_tret_med_m200.dat",unpack=True)
        py.scatter(np.log10(mh), tret_med*1e-3,c="c",edgecolors="none",s=12,label=r"FIRE",marker="X")

    #could add ubler

    if show_g19:
        # From his figure 7, the median return time is 500 Myr, bang on, averaged over his entire sample and all times
        # From his figure 2, the median halo mass is 8e11, the range is about 11.67-12.20
        py.scatter((np.log10(8e11)), (0.5),c="g",edgecolors="none",s=12,label=r"Auriga",marker="v")
        py.plot([11.67,12.2],[0.5,0.5],c="g")
        py.plot([11.67,11.67],[0.45,0.55],c="g")
        py.plot([12.2,12.2],[0.45,0.55],c="g")

    if show_T19:
        # From his equation 11, which is the best fit to the time for returning particles to return to ISM from "cold CGM" I think
        tret = 1.1 * (10**m200_mid / 1e11)**-0.18 # Gyr
        ok = (10**m200_mid > 6e9) & (10**m200_mid < 4e12)
        py.plot(m200_mid[ok],tret[ok],label="NIHAO",linestyle='--',c="y")

    if show_u14:
        tret, logmret = np.loadtxt("ubler14_tret_dist_977.dat",unpack=True)
        tret_med = us.Weighted_Percentile(tret, weight_in = 10**logmret,perc=0.5)
        mhalo = 12.9*10**11
        py.scatter((np.log10(mhalo)),(tret_med),label="U14",edgecolors="none",c="m",s=12,marker="^")

    #py.ylabel(r"$\mathrm{d} \dot{M}_{\mathrm{in,ISM}}^{\mathrm{recycled}} / \mathrm{d} t_{\mathrm{recycle}}   \, / \mathrm{M_\odot Gyr^{-2}}$")
    py.ylabel(r"$t_{\mathrm{rec}} \, / \mathrm{Gyr}$")
    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

    py.legend(loc='upper right',frameon=True,numpoints=1,scatterpoints=1)

    xlo = 9.5; xhi= 13.5
    ylo = 0.0; yhi = 2.0

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))

fig_name = "tret_med_cumul_m200_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
