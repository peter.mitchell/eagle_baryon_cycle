import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time
import utilities_statistics as us

show_g19 = True

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

tret_bins = file_grid["tret_bins"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fret_ism_t_dist"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.0,'figure.subplot.hspace':0.21,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.top':0.96,
                    'legend.fontsize':7,
                    'axes.labelsize':10,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

for i,ax in enumerate(subplots):
    py.axes(ax)

    for tick in ax.xaxis.get_major_ticks():
        tick.label1On=True

    show_mh = 19

    if i == 0:
        show_z = 1

        x = tret_bins[show_z]
        y = f_med_dict["fret_ism_t_dist"][show_z][show_mh]
        y *= 1./np.sum(y)

        xmid = 0.5*(x[1:]+x[0:-1])
        lo = us.Weighted_Percentile(xmid,y,0.25)
        med = us.Weighted_Percentile(xmid,y,0.5)
        hi = us.Weighted_Percentile(xmid,y,0.75)

        y *= 1./ (x[1:]-x[0:-1])

        for n in range(len(x[1:])):
            py.plot( [x[n],x[n]], [0,y[n]] , c="k", linewidth=lw,alpha=0.5)
            py.plot( [x[n+1],x[n+1]], [0,y[n]] , c="k",linewidth=lw,alpha=0.5)
            py.plot( [x[n],x[n+1]], [y[n],y[n]] , c="k",linewidth=lw,alpha=0.5)

        py.axvline(lo,c="k",linewidth=lw,linestyle='--')
        py.axvline(med,c="k",linewidth=lw,linestyle='-')
        py.axvline(hi,c="k",linewidth=lw,linestyle='--')

        if show_g19:
            # Taken from graph click of figure at tlb_launch ~ 4 Gyr (approximate)
            med_g19 = 0.52; lo_g19 = 0.174; hi_g19 = 0.841
            py.axvline(lo_g19,c="r",linewidth=lw,linestyle='--')
            py.axvline(med_g19,c="r",linewidth=lw,linestyle='-')
            py.axvline(hi_g19,c="r",linewidth=lw,linestyle='--')

        #py.ylabel(r"$\mathrm{d} \dot{M}_{\mathrm{in,ISM}}^{\mathrm{recycled}} / \mathrm{d} t_{\mathrm{recycle}}   \, / \mathrm{M_\odot Gyr^{-2}}$")
        py.ylabel(r"$\mathrm{PDF}$")

        jimbo = r"$%3.1f < z < %3.1f$" % (1./a_max[show_z]-1,1./a_min[show_z]-1)
        py.annotate(jimbo,(8.5,np.max(y)*0.7))

        log_dmh = 0.2
        jimbo = r"$%3.1f < \log_{10}(M_{200} \, / \mathrm{M_\odot}) < %3.1f$" % (bin_mh_mid[show_mh]-log_dmh*0.5,bin_mh_mid[show_mh]+log_dmh*0.5)
        py.annotate(jimbo,(4.1,np.max(y)*0.80))

        py.axvline(-99,c="r",linewidth=lw,label="Auriga")
        py.axvline(-99,c="k",linewidth=lw,label="Eagle")
        py.legend(loc='upper right',frameon=False,numpoints=1)        

        xlo = -0.5; xhi= 12.0
        ylo = 0.0; yhi = np.max(y)*1.1

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$t_{\mathrm{recycle}} \, / \mathrm{Gyr}$")

fig_name = "tret_t_dist_g19_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
