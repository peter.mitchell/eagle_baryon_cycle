import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

track_star = True
ireheat = False # Comes from first ref report - they wanted to know the impact of setting wind selection arbitarily low

if track_star and ireheat:
    print "no"
    quit()

show_g19 = True
show_aa17 = True

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

show_incomplete = True

# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 200 snipshots
#sim_name = "L0025N0376_REF_200_snip"
sim_name = "L0025N0376_REF_200_snip_track_star"

# 25 Mpc recal with 200 snipshots
#sim_name = "L0025N0752_Recal"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_inflows_only.hdf5")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mh_med = file_grid["bin_mh_med"][:][0]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fprcooled_cum_pp","frecooled_cum_pp"+fV_str,"frecooled_ireheat_cum_pp","frecooled_from_mp_cum_pp"+fV_str,"fgaltran_cum_pp","fmerge_ism_cum_pp", "fmerge_cgm_cum_pp"]

if track_star:
    f_name_list += ["sfr_prcooled_cum_pp","sfr_recooled_cum_pp","sfr_recooled_from_mp_cum_pp","sfr_galtran_cum_pp","sfr_merge_cum_pp"]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

# Read Grand 19 Auriga data
logmh_g19, fmerge_star_g19, fsat_wind_g19, f_strip_g19, f_merge_gas_g19, f_recool_g19, f_pristine_g19 = np.loadtxt("grand19_cumulative.dat",unpack=True)

tot_g19 = f_strip_g19 + fsat_wind_g19 + f_recool_g19 + f_pristine_g19 + f_merge_gas_g19
f_pristine_g19 *= 1./tot_g19
f_galtran_g19 = (fsat_wind_g19+f_strip_g19)/tot_g19
f_recool_g19 *= 1./tot_g19

order = np.argsort(logmh_g19)
logmh_g19 = logmh_g19[order]
f_pristine_g19 = f_pristine_g19[order]
f_galtran_g19 = f_galtran_g19[order]
f_recool_g19 = f_recool_g19[order]
f_merge_gas_g19 = f_merge_gas_g19[order]

# Read AnglesAlcazar 17 FIRE data
logmh_aa17, f_pristine_aa17, f_recool_aa17, f_galtran_aa17, f_merge_gas_aa17, f_merge_star_aa17 = np.loadtxt("angles_alcazar17_cumulative.dat",unpack=True)

tot_aa17 = f_galtran_aa17 + f_recool_aa17 + f_pristine_aa17 + f_merge_gas_aa17
f_pristine_aa17 *= 1./tot_aa17
f_galtran_aa17 *= 1./tot_aa17
f_recool_aa17 *= 1./tot_aa17
f_merge_gas_aa17 *= 1./tot_aa17

order = np.argsort(logmh_aa17)
logmh_aa17 = logmh_aa17[order]
f_pristine_aa17 = f_pristine_aa17[order]
f_galtran_aa17 = f_galtran_aa17[order]
f_recool_aa17 = f_recool_aa17[order]
f_merge_gas_aa17 = f_merge_gas_aa17[order]

# Read Chistensen 16 data
logmh_c16, log_pristine_c16_normed, log_tot_c16_normed = np.loadtxt("christensen16_cumulative.dat",unpack=True)

f_pristine_c16 = 10**log_tot_c16_normed / 10**log_pristine_c16_normed # yes this is correct..
f_rec_c16 = 1 - f_pristine_c16

print ""
print "Christensen 16 results"
print "Its not clear what to make of her results in terms of galtran / mergers etc"
print "Her methods section implies she includes satellite ISM in her defn of accretion - not easy to map"
print "In any her case, her measurement of first-time and recycled accretion fractions are"
print f_pristine_c16
print f_rec_c16
print ""

from utilities_plotting import *

#py.scatter(logmh_c16, f_pristine_c16)
#py.show()
#quit()

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49],
                    'figure.subplot.left':0.15,
                    'figure.subplot.bottom':0.19,
                    'figure.subplot.right':0.96,
                    'figure.subplot.top':0.97,
                    'legend.fontsize':9,
                    'axes.labelsize':9,
                    'legend.fontsize':9})

xlo = 11.0; xhi = 13.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 1; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1

fcool_pristine_med = f_med_dict["fprcooled_cum_pp"]
fcool_recooled_med = f_med_dict["frecooled_from_mp_cum_pp"+fV_str] # Use from mp def
fcool_galtran_med = f_med_dict["fgaltran_cum_pp"] + f_med_dict["frecooled_cum_pp"+fV_str] - fcool_recooled_med # Use from mp def
if ireheat:
    fcool_recooled_med = f_med_dict["frecooled_ireheat_cum_pp"]
    fcool_galtran_med = f_med_dict["fgaltran_cum_pp"]
fcool_merge_med = f_med_dict["fmerge_ism_cum_pp"]

fcool_pristine_med += f_med_dict["fmerge_cgm_cum_pp"]

if track_star:
    fcool_pristine_med = f_med_dict["sfr_prcooled_cum_pp"]
    fcool_recooled_med = f_med_dict["sfr_recooled_from_mp_cum_pp"]
    fcool_galtran_med = f_med_dict["sfr_galtran_cum_pp"] + f_med_dict["sfr_recooled_cum_pp"] - fcool_recooled_med
    fcool_merge_med = f_med_dict["sfr_merge_cum_pp"]

tot = fcool_pristine_med + fcool_recooled_med + fcool_galtran_med + fcool_merge_med
fcool_pristine_med *= 1./tot
fcool_recooled_med *= 1./tot
fcool_galtran_med *= 1./tot
fcool_merge_med *= 1./tot

for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        n = 0

        y1 = fcool_pristine_med
        y2 = fcool_recooled_med
        y3 = fcool_galtran_med

        if show_incomplete:
            ok = bin_mh_mid >= 10.8
            temp = np.max(np.where(ok==False)[0])+2

            py.plot(bin_mh_med[:temp], y1[:temp],c="k",linewidth=lw,alpha=0.3)
            py.plot(bin_mh_med[:temp], y2[:temp],c="k",linewidth=lw,alpha=0.3, linestyle='--')
            py.plot(bin_mh_med[:temp], y3[:temp],c="k",linewidth=lw,alpha=0.3, linestyle=':')
            y1[ok==False] = np.nan
            y2[ok==False] = np.nan
            y3[ok==False] = np.nan
        else:
            ok = bin_mh_mid > 0

        py.plot(bin_mh_med[ok],y1[ok],c=c_list[n],linewidth=lw)
        py.plot(bin_mh_med[ok],y2[ok],c=c_list[n],linestyle="--",linewidth=lw)
        py.plot(bin_mh_med[ok],y3[ok],c=c_list[n],linestyle=":",linewidth=lw)

        if show_g19:
            py.plot(logmh_g19, f_pristine_g19, c="r",linewidth=lw)
            py.plot(logmh_g19, f_recool_g19, c="r",linewidth=lw,linestyle='--')
            py.plot(logmh_g19, f_galtran_g19, c="r",linewidth=lw,linestyle=':')

        if show_aa17:
            py.plot(logmh_aa17, f_pristine_aa17, c="c",linewidth=lw)
            py.plot(logmh_aa17, f_recool_aa17, c="c",linewidth=lw,linestyle='--')
            py.plot(logmh_aa17, f_galtran_aa17, c="c",linewidth=lw,linestyle=':')

        py.plot(bin_mh_med, bin_mh_med-99,c="k",label="First infall",linewidth=lw)
        py.plot(bin_mh_med, bin_mh_med-99,c="k",label="Recycled",linestyle="--",linewidth=lw)
        py.plot(bin_mh_med, bin_mh_med-99,c="k",label="Transfer",linestyle=":",linewidth=lw)

        py.legend(loc='upper right',frameon=False,numpoints=1)
        legend1 = py.legend(loc='upper right',frameon=False,numpoints=1)   
        py.gca().add_artist(legend1)

        l1, = py.plot([-20,-20],[-20,20],label=r"EAGLE",c="k",linewidth=lw)
        l2, = py.plot([-20,-20],[-20,20],label=r"Auriga",c="r",linewidth=lw)
        l3, = py.plot([-20,-20],[-20,20],label=r"FIRE",c="c",linewidth=lw)

        py.legend(handles=[l1,l2,l3],labels=[r"EAGLE",r"Auriga",r"FIRE"], loc="upper left",frameon=False,numpoints=1)

        py.ylabel(r"$f_{\mathrm{accretion}}$")
        ylo = 0.0; yhi = 0.9

        
    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "cumulative_return_contribution_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Eagle_Inflows/'+fig_name)
py.show()
