import glob
import os
import h5py
import read_eagle as re
import numpy as np
import match_searchsorted as ms
import utilities_cosmology as uc

kpc2km = 3.0857e16
Gyr2s = 3.15576e16

def Check_Dict(data, flag):
    lens = []
    for name in data:
        lens.append(len(data[name]))
    if len(np.unique(lens)) != 1:
        print "Error: a data dictionary contains arrays of differing size"
        print "Grep '",flag,"' to find the point where this crash occured"
        quit()

def Rebind_Subhalos(part_data, halo_grn, halo_sgrn, halo_coord, r200_host, boxsize, verbose=False):

    # Set SO particles that don't belong to a FoF group to the central subhalo of the FoF group
    grn = np.abs(part_data.data["group_number"])

    if len(grn) == 0:
        return part_data

    # Apart from particles with grn == -1e9, these are the ones that aren't contained in bound part files - set back to -ve
    sgrn = np.copy(part_data.data["subgroup_number"])
    sgrn[grn==1e9] = -1e9
    grn[grn==1e9] = -1e9 # Note the grn==1e9 has be consistent with the value set in mpio module


    # Set FoF particles that are not bound to a subhalo to the central subhalo
    subfind_unbound = sgrn >= 1e8
    sgrn[subfind_unbound] = 0

    part_data.add_data(subfind_unbound,"subfind_unbound")

    nsep = max(len(str(int(sgrn.max()))) +1 ,6)
    subhalo_index = grn* 10**nsep + sgrn

    order = np.argsort(subhalo_index)
    grn_sorted = grn[order] # Note that indexing in this way does create a new array (not so for [blah:blah])
    sgrn_sorted = sgrn[order]
    coord_sorted = part_data.data["coordinates"][order]
    subhalo_index = subhalo_index[order]

    subhalo_index_unique, particle_index = np.unique(np.array(subhalo_index), return_index=True)
    particle_index = np.append(particle_index,len(grn))

    halo_subhalo_index = halo_grn * 10**nsep + halo_sgrn

    if len(np.unique(halo_subhalo_index)) != len(halo_subhalo_index):
        print "Error: duplicates"
        quit()

    if verbose:
        print ""
        print "Nbound part before", len(part_data.data["group_number"][(part_data.data["group_number"]>0)&(part_data.data["subgroup_number"]<1e7)])
        print "Nbound part after", len(grn_sorted[(grn_sorted>0)&(sgrn_sorted<1e7)])

    # Loop over central subhalos (we could remove satellite particles outside r200 of the host at a later point)
    for i_subhalo in range(len(halo_grn)):
        if halo_sgrn[i_subhalo] > 0:
            continue

        # Find particles in FoF group, and associated to this central subhalo
        match = np.where( halo_subhalo_index[i_subhalo] == subhalo_index_unique)[0]

        #if i_subhalo == 0:
        #   print "v1", halo_subhalo_index, subhalo_index_unique, i_subhalo

        if len(match)==0:
            continue
        elif len(match) > 1:
            print "Error: duplicate values"
            quit()

        index1 = particle_index[match[0]]
        index2 = particle_index[match[0]+1]
        coord_i = coord_sorted[index1:index2]

        # Centre on halo
        halo_coord_i = [halo_coord[0][i_subhalo], halo_coord[1][i_subhalo], halo_coord[2][i_subhalo]]
        coord_i = Centre_Halo(coord_i, halo_coord_i, boxsize)

        # Flag particles that are outside r200
        r_part = np.sqrt(np.sum(np.square(coord_i),axis=1))

        r200_i = r200_host[i_subhalo]
        outside = np.where(r_part > r200_i)

        # Set grn and sgrn of these particles to large -ve number (with mag larger than n subhalos in 100 Mpc ref eagle run)
        grn_sorted[index1:index2][outside] = -1e9
        sgrn_sorted[index1:index2][outside] = -1e9

        # To parallelise this - each iteration through loop should return the indicies of [index1:index2][outside]

    reverse_order = np.argsort(order)
    part_data.data["group_number"] = grn_sorted[reverse_order]
    part_data.data["subgroup_number"] = sgrn_sorted[reverse_order]

    if verbose:
        print "Nbound part final", len(part_data.data["group_number"][(part_data.data["group_number"]>0)&(part_data.data["subgroup_number"]<1e7)])
        print ""

    return part_data

def Rebind_All_Particles_From_Bound(all_part_data, bound_part_data):

    subfind_unbound = np.zeros_like(all_part_data.data["subgroup_number"]) != 0

    ok = bound_part_data.data["group_number"] >= 0
    if len(ok[ok]) > 0 and len(all_part_data.data["id"]) > 0:
        ptr = ms.match(all_part_data.data["id"], bound_part_data.data["id"][ok])
        ok2 = ptr >= 0
    
        all_part_data.data["group_number"][ok2==False] = -1e9
        all_part_data.data["subgroup_number"][ok2==False] = -1e9

        all_part_data.data["group_number"][ok2] = bound_part_data.data["group_number"][ok][ptr][ok2]
        all_part_data.data["subgroup_number"][ok2] = bound_part_data.data["subgroup_number"][ok][ptr][ok2]

        subfind_unbound[ok2] = bound_part_data.data["subfind_unbound"][ok][ptr][ok2]

    all_part_data.add_data(subfind_unbound,"subfind_unbound")


def Halo_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, halo_props, halo_group_number_ns,write_SNe_energy_fraction, fVmax_cuts):

    halo_r200, halo_subgroup_number, halo_vmax = halo_props

    coordinates_gas_ps_in, id_gas_ps_in, mass_gas_ps_in = data_gas_ps_in
    if write_SNe_energy_fraction:
        coordinates_gas_list_all_unbound_ts, id_gas_list_all_unbound_ts, group_number_gas_list_all_unbound_ts, dir_heated_all_unbound_ts, dir_heated_SNe_all_unbound_ts = data_gas_ts_in
    else:
        coordinates_gas_list_all_unbound_ts, id_gas_list_all_unbound_ts, group_number_gas_list_all_unbound_ts = data_gas_ts_in
    coordinates_part_list_all_unbound_ns, id_part_list_all_unbound_ns, group_number_part_list_all_unbound_ns = data_gas_ns_in

    use_ns = 2

    flag_ps = np.zeros_like(id_gas_ps_in) # 0 = bound to main FoF group, 1 = unbound
    flag_ts = np.ones_like(flag_ps)
    flag_ns = np.ones_like(flag_ps)

    coordinates_gas_ejecta_ts = np.zeros_like(coordinates_gas_ps_in)
    coordinates_gas_ejecta_ns = np.zeros_like(coordinates_gas_ps_in)
    group_number_gas_ejecta_ns = np.zeros_like(id_gas_ps_in)-62
    group_number_gas_ejecta_ts = np.zeros_like(id_gas_ps_in)-65
    if write_SNe_energy_fraction:
        dir_heated_ejecta_ts = np.zeros_like(id_gas_ps_in) <0
        dir_heated_SNe_ejecta_ts = np.zeros_like(id_gas_ps_in) <0

    # Find the locations of ejected particles on _ts
    if len(id_gas_ps_in)>0 and len(id_gas_list_all_unbound_ts)>0:
        ptr = ms.match(id_gas_ps_in, id_gas_list_all_unbound_ts)
        ok_match = ptr>=0
        
        coordinates_gas_ejecta_ts[ok_match] = coordinates_gas_list_all_unbound_ts[ptr][ok_match]

        # Non-matched particles have either become stars, have left the subvolume entirely (assume latter dominates so set positions to inifinty), or have become subfind unbound (regardless of rebinding)
        group_number_gas_ejecta_ts[ok_match] = group_number_gas_list_all_unbound_ts[ptr][ok_match]        

        coordinates_gas_ejecta_ts[ok_match==False] += 1e9
        if write_SNe_energy_fraction:
            dir_heated_ejecta_ts[ok_match] = dir_heated_all_unbound_ts[ptr][ok_match]
            dir_heated_SNe_ejecta_ts[ok_match] = dir_heated_SNe_all_unbound_ts[ptr][ok_match]
    elif len(id_gas_ps_in)>0:
        coordinates_gas_ejecta_ts = np.zeros_like(coordinates_gas_ps_in) + 1e9
    else:
        select_wind = np.zeros(0)<0
        fVmax = np.zeros(0)
        if write_SNe_energy_fraction:
            return select_wind, fVmax, dir_heated_ejecta_ts, dir_heated_SNe_ejecta_ts
        else:
            return select_wind, fVmax

    # Find positions ejected particles on _ns
    if len(id_gas_ps_in)>0 and len(id_part_list_all_unbound_ns)>0:
        ptr = ms.match(id_gas_ps_in, id_part_list_all_unbound_ns)
        ok_match = ptr>=0
        coordinates_gas_ejecta_ns[ok_match] = coordinates_part_list_all_unbound_ns[ptr][ok_match]
        # Non-matched particles are those that became non-bound stars or have have left the subvolume entirely (assume latter dominates so set positions to (slightly larger) infinity)
        coordinates_gas_ejecta_ns[ok_match==False] += 2e9
        # Work out if ejected particles are bound to the descendant's FoF group on _ns
        group_number_gas_ejecta_ns[ok_match] = group_number_part_list_all_unbound_ns[ptr][ok_match]
    else:
        coordinates_gas_ejecta_ns = np.zeros_like(coordinates_gas_ps_in) + 2e9

    flag_ns[group_number_gas_ejecta_ns == halo_group_number_ns] = 0 # Particles which have returned become rebound to the subhalo's FoF group by _ns

    # Find particles lost from the domain on _ts, but found again on _ns, assume the particle is halfway in between on _ts
    lost_found_ts = (group_number_gas_ejecta_ts == -67) & (group_number_gas_ejecta_ns != -62)
    coordinates_gas_ejecta_ts[lost_found_ts] = 0.5*(coordinates_gas_ps_in[lost_found_ts] + coordinates_gas_ejecta_ns[lost_found_ts])

    # Find particles that are bound to another fof group on _ts, but are unbound on _ns. Allow these particles to join the wind
    flythrough_wind = (group_number_gas_ejecta_ts == -65) & (group_number_gas_ejecta_ns < 0)
    coordinates_gas_ejecta_ts[flythrough_wind] = 0.5*(coordinates_gas_ps_in[flythrough_wind] + coordinates_gas_ejecta_ns[flythrough_wind])

    # Find particles that are bound to another fof group on _ts, and on _ns. Assume these particles have been carried away (splashback) rather than joining a wind.
    carried_away = (group_number_gas_ejecta_ts == -65) & (group_number_gas_ejecta_ns >= 0)
    flag_ts[carried_away] = 0; flag_ns[carried_away] = 0

    flag_list = np.array([flag_ps, flag_ts, flag_ns])
    coordinates_gas_ejecta_list = np.array([coordinates_gas_ps_in, coordinates_gas_ejecta_ts, coordinates_gas_ejecta_ns])

    # This array is not used in the Vmax_wind_selection call when using "halo_winds"
    cv_dummy = np.zeros_like(coordinates_gas_ejecta_list[:,:,0])

    select_wind, vmax_cut, fVmax = Vmax_wind_selection(coordinates_gas_ejecta_list, cv_dummy, t_list, halo_vmax, halo_r200, halo_subgroup_number, "halo_winds", use_ns, flag_list, fVmax_cuts)

    if write_SNe_energy_fraction:
        return select_wind, fVmax, dir_heated_ejecta_ts, dir_heated_SNe_ejecta_ts
    else:
        return select_wind, fVmax

def Halo_Wind_Selection_Prelocated(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, halo_props, fVmax_cuts):

    halo_r200, halo_subgroup_number, halo_vmax = halo_props

    coordinates_gas_ps_in = data_gas_ps_in[0]
    coordinates_gas_ts_in, id_gas_list_in = data_gas_ts_in
    coordinates_gas_ns_in = data_gas_ns_in[0]

    use_ns = 2

    # Note flags are just dummy values here. "Carried-away" particles are precluded from joining the wind within the relevant main_loop block
    flag_ps = np.zeros_like(id_gas_list_in) # 0 = bound to main FoF group, 1 = unbound
    flag_ts = np.ones_like(flag_ps)
    flag_ns = np.ones_like(flag_ps)

    flag_list = np.array([flag_ps, flag_ts, flag_ns])
    coordinates_gas_ejecta_list = np.array([coordinates_gas_ps_in, coordinates_gas_ts_in, coordinates_gas_ns_in])
    
    # This array is not called in the Vmax_wind_selectionc all when using "halo_winds"
    cv_dummy = np.zeros_like(coordinates_gas_ejecta_list[:,:,0])

    select_wind, vmax_cut, fVmax = Vmax_wind_selection(coordinates_gas_ejecta_list, cv_dummy, t_list, halo_vmax, halo_r200, halo_subgroup_number, "halo_winds", use_ns, flag_list, fVmax_cuts)

    return select_wind, fVmax

def Reheated_Wind_Selection(t_list, data_gas_ps_in, data_gas_ts_in, data_gas_ns_in, id_star_fof_ns, halo_props, fVmax_cuts):

    halo_r200, halo_subgroup_number, halo_vmax = halo_props

    coordinates_gas_ps_in  = data_gas_ps_in[0]
    id_gas_ts_in, coordinates_gas_ts_in, mass_gas_ts_in, cv_gas_ts_in = data_gas_ts_in
    id_gas_fof_ns, SF_fof_ns, ISM_fof_ns, coordinates_gas_fof_ns = data_gas_ns_in

    use_ns = 2

    flag_ps = np.zeros_like(id_gas_ts_in) # 0 = SF, 1 = NSF, 2 = star
    flag_ts = np.ones_like(flag_ps)
    flag_ns = np.ones_like(flag_ps)

    # Particles that have returned to the ISM (SF or NSF) on _ns should not be added to the wind
    if len(id_gas_ts_in) > 0 and len(id_gas_fof_ns[SF_fof_ns|ISM_fof_ns]) > 0:
        ptr_temp = ms.match(id_gas_ts_in, id_gas_fof_ns[SF_fof_ns|ISM_fof_ns],arr2_sorted=True)
        ok_match_temp = ptr_temp >=0
        flag_ns[ok_match_temp] = 0

    # Find gas particles that are bound to a descendant of the FoF group on the next step
    if len(id_gas_ts_in) > 0 and len(id_gas_fof_ns) > 0:
        ptr_ns = ms.match(id_gas_ts_in, id_gas_fof_ns,arr2_sorted=True)
        ok_match_ns = ptr_ns >=0
    else:
        ok_match_ns = np.zeros_like(id_gas_ts_in) <0

    coordinates_gas_reheat_ns = np.zeros_like(coordinates_gas_ts_in)
    if len(ok_match_ns[ok_match_ns])>0:
        coordinates_gas_reheat_ns[ok_match_ns] = coordinates_gas_fof_ns[ptr_ns][ok_match_ns]

    # Check for particles that have become a star on the next step
    if len(id_gas_ts_in) >0 and len(id_star_fof_ns)>0:
        ptr_ns = ms.match(id_gas_ts_in, id_star_fof_ns,arr2_sorted=True)
        ok_match_ns2 = ptr_ns >=0
        flag_ns[ok_match_ns2] = 2
    else:
        ok_match_ns2 = np.zeros_like(id_gas_ts_in)<0

    # Set coordinates of particles that can't be found in bound particles of descendants FoF group to a very large number (i.e. they are definitely in the galaxy wind)
    coordinates_gas_reheat_ns[(ok_match_ns==False) & (ok_match_ns2==False)] += 2e9

    flag_list = np.array([flag_ps, flag_ts, flag_ns])
    coordinates_gas_reheat_list = np.array([coordinates_gas_ps_in, coordinates_gas_ts_in, coordinates_gas_reheat_ns])
    
    select_wind, vmax_cut, fVmax = Vmax_wind_selection(coordinates_gas_reheat_list, cv_gas_ts_in, t_list, halo_vmax, halo_r200, halo_subgroup_number, "galaxy_winds", use_ns, flag_list, fVmax_cuts)

    return select_wind, fVmax

def Vmax_wind_selection(coordinates_list_in, cv_ts, t_list, vmax_ts, r200_ts, subgroup_number_ts, wind_type, use_ns, flag_list, fVmax_cuts):
    # This function was updated circa 11/2018 to provide better converged outflow rates w.r.t snapshot spacing

    if wind_type != "galaxy_winds" and wind_type != "halo_winds":
        raise Exception("wind_type not recognised")

    select_wind = np.zeros_like(cv_ts)>0

    x_part = coordinates_list_in[:,:,0]
    y_part = coordinates_list_in[:,:,1]
    z_part = coordinates_list_in[:,:,2]

    r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part) )

    # For ISM winds, flag = 0 means back in the ISM, flag = 2 means particle is a star
    r_part[1:][(flag_list[1:] == 0) | (flag_list[1:]==2)] = 0.0

    if not use_ns >= 2:
        print "Error, use_ns must be >=2"
        quit()

    vrad_ta_part_ns = (r_part[use_ns] - r_part[1]) * kpc2km / (t_list[use_ns]-t_list[1]) / Gyr2s
    
    if wind_type=="galaxy_winds":
        vmax_cut = max(np.array(fVmax_cuts)) # If we want results for multiple wind velocity cuts, we take the max value here (to define reheated lists) - the other cuts will be applied later
        vmax_cut2 = min(0.125, vmax_cut)

        select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) & (cv_ts / vmax_ts > vmax_cut2)
        select_wind = select_wind | (cv_ts / vmax_ts > 1)

    else:
        vmax_cut = max(np.array(fVmax_cuts))
        select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut)
        if subgroup_number_ts == 0:
            select_wind = select_wind & (r_part[1] > r200_ts)

    fVmax = vrad_ta_part_ns / vmax_ts # Fraction of vmax for the time-averaged velocity - used to apply different cuts after the fact
    if wind_type == "galaxy_winds":
        not_yet = cv_ts / vmax_ts <= vmax_cut2 # Deal with the particles that haven't joined the wind yet (but will soon)
        fVmax[not_yet] = np.min((cv_ts[not_yet]/vmax_ts, fVmax[not_yet]),axis=0)

    return select_wind, vmax_cut, fVmax

def Select_List(list_arrays, select):

    for i_list, list_i in enumerate(list_arrays):

        if len(select) != len(list_i):
            raise Exception("Boolean selection array did not match one of the arrays to be selected")

        list_i  = list_i[select]
        
        if i_list>0 and len_list != len(list_i):
            raise Exception("Arrays fed in do not all have equal length")

        len_list = len(list_i)

    return list_arrays

def Periodic_Box_Check(coordinates, xyz_min, xyz_max, boxlen):

    pr = coordinates[:,0] < xyz_min[0] - boxlen*0.5
    coordinates[:,0][pr] += boxlen

    pr = coordinates[:,1] < xyz_min[1] - boxlen*0.5
    coordinates[:,1][pr] += boxlen

    pr = coordinates[:,2] < xyz_min[2] - boxlen*0.5
    coordinates[:,2][pr] += boxlen

    pr = coordinates[:,0] > xyz_max[0] + boxlen*0.5
    coordinates[:,0][pr] -= boxlen

    pr = coordinates[:,1] > xyz_max[1] + boxlen*0.5
    coordinates[:,1][pr] -= boxlen

    pr = coordinates[:,2] > xyz_max[2] + boxlen*0.5
    coordinates[:,2][pr] -= boxlen

    return coordinates


def Centre_Halo(part_pos_in, halo_pos, boxsize, scaling=None):

    # Edited on 30/1/19 to copy input particle positions - to test if this resolves a crash in the joblib version

    part_pos = np.copy(part_pos_in)

    ok = np.where((np.isnan(part_pos[:,0]) == False) & (np.isnan(part_pos[:,1]) == False) & (np.isnan(part_pos[:,2]) == False) & (part_pos[:,0]<1e8) )[0]

    if len(part_pos) == 0:
        return part_pos

    '''# Correct for periodic boundary conditions
    if np.median(part_pos[:,0][ok]) > halo_pos[0] + 0.5 * boxsize:
        part_pos[:,0][ok] -= boxsize
    if halo_pos[0] > np.median(part_pos[:,0][ok]) + 0.5 * boxsize:
        part_pos[:,0][ok] += boxsize
    if np.median(part_pos[:,1][ok]) > halo_pos[1] + 0.5 * boxsize:
        part_pos[:,1][ok] -= boxsize
    if halo_pos[1] > np.median(part_pos[:,1][ok]) + 0.5 * boxsize:
        part_pos[:,1][ok] += boxsize
    if np.median(part_pos[:,2][ok]) > halo_pos[2] + 0.5 * boxsize:
        part_pos[:,2][ok] -= boxsize
    if halo_pos[2] > np.median(part_pos[:,2][ok]) + 0.5 * boxsize:
        part_pos[:,2][ok] += boxsize'''

    # Correct for periodic boundary conditions
    # Note, had to edit this on 22/11 because there can be situations particles associated with one halo can be on opposite sides of the box

    halo_pos = np.array(halo_pos)

    problem = np.where(part_pos[ok] > halo_pos+0.5*boxsize)[0]
    part_pos[ok][problem] -= boxsize
    problem = np.where(part_pos[ok] < halo_pos-0.5*boxsize)[0]
    part_pos[ok][problem] += boxsize

    part_pos[:,0][ok] -= halo_pos[0]
    part_pos[:,1][ok] -= halo_pos[1]
    part_pos[:,2][ok] -= halo_pos[2]

    if scaling is not None:
        part_pos[ok] *= scaling

    return part_pos

def Select_ISM_M18(coordinates_parts, gas_props, mass_part, halo_props, sim_props, verbose=False):
    # Select gas particles in a subhalo which are classed as ISM particles according to the Mitchell et al. (2018) criteria
    # Edit: the Mitchell 18 criteria have been slightly tweaked
    # Radius cutoff for rotating gas now applied at 2 r90 of SF ISM
    # More conservative cuts applied for gas to be in rotational equilibrium
    # Additional cut based on radial velocity / vmax for gas to be considered in rotational equilibrium

    # Unpack data
    coordinates_gas, coordinates_dm, coordinates_star, coordinates_bh = coordinates_parts
    
    internal_energy_gas, temperature_gas, density_gas, velocity_gas, SF = gas_props
    rvir_in, halo_group_number, halo_subgroup_number, halo_vmax = halo_props
    
    mass_gas, mass_dm, mass_star, mass_bh = mass_part
    redshift, h, boxsize, omm = sim_props

    central = halo_subgroup_number == 0
    mass_dm_list = np.ones_like(coordinates_dm[:,0])+mass_dm

    r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
    r_dm = np.sqrt(np.square(coordinates_dm[:,0])+np.square(coordinates_dm[:,1])+np.square(coordinates_dm[:,2]))
    r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))
    r_bh = np.sqrt(np.square(coordinates_bh[:,0])+np.square(coordinates_bh[:,1])+np.square(coordinates_bh[:,2]))

    r_gas_norm = np.swapaxes(np.array([coordinates_gas[:,0],coordinates_gas[:,1],coordinates_gas[:,2]]) / r_gas,0,1)

    # It can happen for _ns (when there is no descendant on that step) that you get empty lists, deal with that
    if len(r_gas) + len(r_dm) + len(r_star) + len(r_bh) == 0:
        return np.zeros_like(r_gas)<0

    # If not supplied, compute a proxy virial radius (r200) (only a proxy because we use only particles bound to subhalo - note though that precise value of r200 is not needed)

    if rvir_in < 0 or np.isnan(rvir_in):
        radius_all_part = np.append(np.append(r_star,r_gas), r_dm)
        mass_all_part = np.append(np.append(mass_star, mass_gas), mass_dm_list)

        radial_order = np.argsort(radius_all_part)
        mass_enclosed = np.cumsum(mass_all_part[radial_order])
        volume = 4*np.pi/3.0 * np.power(radius_all_part[radial_order],3) # kpc^3
        density_enclosed = mass_enclosed / volume # Msun / kpc^-3
        rho_crit = uc.Critical_Density(redshift,h,omm,1-omm) # Msun pMpc^-3
        rho_crit *= 1e-9
        ind_r200 = np.argmin(abs(density_enclosed - rho_crit * 200))
        rvir = radius_all_part[radial_order][ind_r200]
    else:
        rvir = rvir_in *1e3/(1+redshift)/h

    # Compute 3d half-stellar mass radius
    if len(r_star)>0:
        order = np.argsort(r_star)
        mass_star_sorted = mass_star[order]
        mass_star_cum = np.cumsum(mass_star_sorted)
        ind_half = np.argmin(abs(mass_star_cum-0.5*mass_star_cum[-1]))
        rhalf_star = np.sort(r_star)[ind_half]
    else:
        rhalf_star = 0

    ######### Compute mass profile and gas rotation/radial velocities ###########

    # Compute radial velocity for each gas particle
    velocity_gas_R = np.sum(r_gas_norm * velocity_gas,axis=1)
    # Compute radial velocity^2
    velocity_gas_R2 = np.square(velocity_gas_R)

    # Compute cartesian rotation velocity for each gas particle
    velocity_gas_T = np.cross(r_gas_norm,velocity_gas)
    # Compute rotation speed
    rotation_speed_gas = np.sqrt(np.square(velocity_gas_T[:,0])+np.square(velocity_gas_T[:,1])+np.square(velocity_gas_T[:,2]))
    # Compute rotation speed^2
    rotation_speed_gas2 = np.square(rotation_speed_gas)

    # Combine radius and mass arrays
    r_combine = np.concatenate((r_gas, r_dm, r_star, r_bh))
    mass_combine = np.concatenate((mass_gas, mass_dm_list, mass_star, mass_bh))

    # Sort by radius
    order_radial = np.argsort(r_combine)
    mass_combine = mass_combine[order_radial]

    # Cumulative mass profile
    mass_combine_enclosed = np.cumsum(mass_combine)

    # Invert the sort
    order_radial_inverse = np.argsort(order_radial)
    mass_combine_enclosed = mass_combine_enclosed[order_radial_inverse]

    # Compute enclosed mass (including all particle types) within each gas particle radius
    n_gas = len(mass_gas)
    mass_gas_enclosed = mass_combine_enclosed[0:n_gas]

    # Compute rotation vrs gravity
    G = 6.67408e-11 # m^3 kg^-1 s^-2
    Msun = 1.989e30 # kg
    kpc =  3.0857e19 # m
    kms = 1e3 # ms^-1

    e_grav = G * (mass_gas_enclosed*Msun) / (r_gas*kpc) / kms**2
    e_rotation = 0.5 * rotation_speed_gas2
    rotation_eq_parameter = 2 * e_rotation / e_grav

    e_radial = 0.5*velocity_gas_R2
    rotation_energy_vrs_radial_thermal = e_rotation / (e_radial + internal_energy_gas)

    # This (plus more stringent rot_eq_paramter cuts was added circa 11/2018) w.r.t Mitchell 18
    freefall_parameter = abs(velocity_gas_R / halo_vmax)
    
    # Note that density of gas is supposed to be in atoms per cc here!
    ism = ((temperature_gas < 1e5) | (density_gas > 500)) & (((density_gas > 0.03) & (r_gas < 2*rhalf_star)) | ( (freefall_parameter < 0.125) & (rotation_eq_parameter>0.83) & (rotation_eq_parameter<1.2)  & (rotation_energy_vrs_radial_thermal>2) ))

    # Additional step to remove blatant CGM gas at high-z based on > 0.5 rvir for centrals
    # This removes cgm gas (including star forming gas) that clearly has nothing to do with the central galaxy
    if central:
        ism = ism & (r_gas < 0.5 * rvir)

    # Additional step to prune ism defintion, remove gas beyond twice the 0.9 ism radius
    # This is helpful for star forming galaxies at low redshift
    # Compute 3d half-stellar mass radius
    '''if len(mass_gas[ism]) == 0:
        "no gas case"
    else:
        order = np.argsort(r_gas[ism])
        mass_ism_sorted = mass_gas[ism][order]
        mass_ism_cum = np.cumsum(mass_ism_sorted)
        ind_90 = np.argmin(abs(mass_ism_cum-0.9*mass_ism_cum[-1]))
        r90_ism = np.sort(r_gas[ism])[ind_90]
        ism = ism & (r_gas < 2 * r90_ism)'''

    # 21/3/18 - decided to change the r90 ism criteria to r90 SF ism criteria (for winds project)
    if len(mass_gas[ism&SF]) == 0:
        if verbose:
            print "no gas case"
    else:
        order = np.argsort(r_gas[ism&SF])
        mass_ism_sorted = mass_gas[ism&SF][order]
        mass_ism_cum = np.cumsum(mass_ism_sorted)
        ind_90 = np.argmin(abs(mass_ism_cum-0.9*mass_ism_cum[-1]))
        r90_ism = np.sort(r_gas[ism&SF])[ind_90]

        ism = ism & (r_gas < 2 * r90_ism)

    if len(mass_gas[ism]) == 0:
        if verbose:
            "no gas case"
    elif len(mass_star)>0:

        # Final step intended to solve dead galaxies that don't have a central ISM peak
        ism_fraction = np.sum(mass_gas[ism]) / (np.sum(mass_star)+np.sum(mass_gas[ism]))
        dead_correct = False
        if np.sum(mass_gas[ism]) < 0.1 * np.sum(mass_star):
            if rhalf_star > 0.0:
                ism = ism & (r_gas < 5 * rhalf_star)

            dead_correct = True

            ism_fraction_after = np.sum(mass_gas[ism]) / (np.sum(mass_star)+np.sum(mass_gas[ism]))

            if ism_fraction_after < 0.99 * ism_fraction and verbose:
                print "applying dead galaxy correction to galaxy", halo_group_number, halo_subgroup_number
                print "mass fraction of ism before the correction", ism_fraction
                print "after dead correction, fraction is now", np.sum(mass_gas[ism]) / (np.sum(mass_star)+np.sum(mass_gas[ism]))


    return ism


def Select_ISM_simple(gas_props, halo_props, sim_props, verbose=False):
    # Select gas particles in a subhalo which are classed as ISM particles 
    # In this case we use a simple defn of either a) star forming or
    # b) T<T_eos+0.5dex, nh>0.01 cm^-3, r < 0.2 rvir (central) or r < vmax / tdyn (satellite)

    # Unpack data
    coordinates_gas, temperature_gas, nh_gas, SF = gas_props
    rvir_in, halo_subgroup_number, halo_vmax = halo_props
    redshift, tdyn, h = sim_props

    central = halo_subgroup_number == 0
    
    r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
    
    # It can happen for _ns (when there is no descendant on that step) that you get empty lists, deal with that
    if len(r_gas) == 0:
        return np.zeros_like(r_gas)<0

    # For satellites, approximate rvir as vmax / tdyn (vmax doesn't quite track V_halo as f(z), but it does well enough)
    if central:
        rvir = rvir_in*1e3/(1+redshift)/h # pkpc
    if not central:
        km2kpc = 1./(3.0857e16)
        Gyr2s = 365.25*60**2*24*1e9
        rvir = (tdyn*Gyr2s) / (halo_vmax*km2kpc) # pkpc

    T_eos = 8e3 * (nh_gas/0.1)**(4/3.)

    ism  = (np.log10(temperature_gas) < np.log10(T_eos)+0.5) & (nh_gas > 0.01) & (r_gas < 0.2*rvir)
    ism = ism | SF

    return ism


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def Match_Index(index, index_list, index2_list):
    match = np.where(index == index_list)[0]

    if len(match) == 0:
        return [0,0]

    elif len(match) > 1:
        print "Error: Match_Index(), index_list contains duplicate values"
        print index_list
        quit()

    else:
        index2_i = index2_list[match[0]]
        index2_ip1 = index2_list[match[0]+1]
        
        return [index2_i, index2_ip1]

def Index_Array(array,index,order):
    if len(index)!=2:
        print "Error: Index_Array(), invalid index", index
        quit()
    else:
        # Note here that you are not returning a new array, this is not a copy
        # As such any changes to "array[index[0]:index[1]]" will also change "array"

        if order is None:
            return array[index[0]:index[1]]
        else:
            return array[index[0]:index[1]][order]

def Sum_Common_ID(quantities_list1, id_list1, id_list2_in,verbose=False,use_dict=False,dict_names=None):
    if verbose:
        print "Sum_Common_ID(): Building bins for histogram and mapping back onto unbinned array"

    # sort id list 2 and save the order
    order = np.argsort(id_list2_in)
    inverse = np.argsort(order)
    id_list2 = np.sort(id_list2_in)

    # John gave the interpolated halos huge IDs, which I need to edit here to make sure we can convert to a float
    if np.max(id_list2) > 1e15:
        print "Error in mpf.Sum_Common_ID, an input ID list 2 is too large to turn into a 64-bit float"
        print np.sort(id_list2)
        quit()

    if np.max(id_list1) > 1e15:
        print "Error in mpf.Sum_Common_ID, an input ID list 1 is too large to turn into a 64-bit float"
        print np.sort(id_list1)
        quit()

    id_bin2 = np.repeat(id_list2,2).astype('float64')
    id_bin2[1::2] += 0.5
    id_bin2[0:-1:2] += -0.5
    id_bin2 = np.unique(id_bin2)

    id_bin2_mid = 0.5*(id_bin2[0:-1]+id_bin2[1:])
    ok = ms.match(id_bin2_mid, id_list2) >= 0

    if verbose:
        print "Sum_Common_ID(): Done, now computing histograms for each quantity"

    if not use_dict:
        quantities_list2 = []
        for i_quant, quantity1 in enumerate(quantities_list1):

            quantity2 = np.histogram( id_list1, bins=id_bin2, weights=quantity1 )[0][ok]
            if verbose:
                print "Sum_Common_ID(): Histogram computed for quantity", i_quant,"of",len(quantities_list1)

            # Invert the sort at the top
            quantities_list2.append(quantity2[inverse])

    else:
        if dict_names is None:
            print "Error: mpf.Sum_Common_ID(): need to supply dict names"
            quit()
        quantities_list2 = {}
        for i_name, name in enumerate(dict_names):
            quantity1 = quantities_list1[name]
            quantity2 = np.histogram( id_list1, bins=id_bin2, weights=quantity1 )[0][ok]
            if verbose:
                print "Sum_Common_ID(): Histogram computed for quantity", i_name,"of",len(dict_names)

            # Invert the sort at the top
            quantities_list2[name] = quantity2[inverse]
            
    return quantities_list2








def Rebind_Subhalos_JobLib_Pt1(part_data, halo_grn, halo_sgrn, halo_coord, r200_host, isInterpolated, boxsize, verbose=False):

    # Set SO particles that don't belong to a FoF group to the central subhalo of the FoF group
    grn = np.abs(part_data.data["group_number"])

    if len(grn) == 0:
        return part_data

    # Apart from particles with grn == -1e9, these are the ones that aren't contained in bound part files - set back to -ve
    sgrn = np.copy(part_data.data["subgroup_number"])
    sgrn[grn==1e9] = -1
    grn[grn==1e9] = -1 # Note this 1e9 has be consistent with the value set in mpio module


    # Set FoF particles that are not bound to a subhalo to the central subhalo
    subfind_unbound = sgrn >= 1e8
    sgrn[subfind_unbound] = 0

    part_data.add_data(subfind_unbound,"subfind_unbound")
        
    nsep = max(len(str(int(sgrn.max()))) +1 ,6)
    subhalo_index = grn* 10**nsep + sgrn

    order = np.argsort(subhalo_index)
    grn_sorted = grn[order] # Note that indexing in this way does create a new array (not so for [blah:blah])
    sgrn_sorted = sgrn[order]
    coord_sorted = part_data.data["coordinates"][order]
    subhalo_index = subhalo_index[order]

    subhalo_index_unique, particle_index = np.unique(subhalo_index, return_index=True)
    particle_index = np.append(particle_index,len(grn))

    halo_subhalo_index = halo_grn * 10**nsep + halo_sgrn

    ok = isInterpolated == 0
    if len(np.unique(halo_subhalo_index[ok])) != len(halo_subhalo_index[ok]):
        print "Error: duplicates"
        print np.unique(halo_subhalo_index[ok],return_counts=True)
        quit()

    if verbose:
        print ""
        print "Nbound part before", len(part_data.data["group_number"][(part_data.data["group_number"]>0)&(part_data.data["subgroup_number"]<1e7)])
        print "Nbound part after", len(grn_sorted[(grn_sorted>0)&(sgrn_sorted<1e7)])

    return particle_index, subhalo_index_unique, halo_subhalo_index, coord_sorted, grn_sorted, sgrn_sorted, order

def Rebind_Subhalos_JobLib_Pt2(i_subhalo, halo_subhalo_index, particle_index, subhalo_index_unique, coord_sorted, halo_coord, r200_host, boxsize, verbose=False):

    # Find particles in FoF group, and associated to this central subhalo
    match = np.where( halo_subhalo_index[i_subhalo] == subhalo_index_unique)[0]

    if len(match)==0:
        return []
    elif len(match) > 1:
        print "Error: duplicate values"
        quit()

    index1 = particle_index[match[0]]
    index2 = particle_index[match[0]+1]
    coord_i = coord_sorted[index1:index2]

    # Centre on halo
    halo_coord_i = [halo_coord[0][i_subhalo], halo_coord[1][i_subhalo], halo_coord[2][i_subhalo]]
    coord_i = Centre_Halo(coord_i, halo_coord_i, boxsize)

    # Flag particles that are outside r200
    r_part = np.sqrt(np.sum(np.square(coord_i),axis=1))

    r200_i = r200_host[i_subhalo]
    outside = np.where(r_part > r200_i)

    output = np.arange(index1,index2)[outside]

    # To parallelise this - each iteration through loop should return the indicies of [index1:index2][outside]
    return output

def Rebind_Subhalos_JobLib_Pt3(output, grn_sorted, sgrn_sorted, order, part_data):
    if len(output)>0:
        outside = np.concatenate(np.array(output)).astype("int")

        # Set grn and sgrn of these particles to large -ve number (with mag larger than n subhalos in 100 Mpc ref eagle run)
        grn_sorted[outside] = -1e9
        sgrn_sorted[outside] = -1e9

    reverse_order = np.argsort(order)
    part_data.data["group_number"] = grn_sorted[reverse_order]
    part_data.data["subgroup_number"] = sgrn_sorted[reverse_order]
    return part_data
