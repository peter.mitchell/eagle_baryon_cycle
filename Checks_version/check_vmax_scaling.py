import numpy as np
import h5py
import os
import warnings
import match_searchsorted as ms
import utilities_statistics as us
import utilities_cosmology as uc

# 25 Mpc REF with 50 snips
sim_name = "L0025N0376_REF_50_snip"
h=0.6777
omm = 0.307
box_size = 25 * h
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1
redshift_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/redshift_list.txt"

# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1

snip_list, z_list = np.loadtxt(redshift_file,unpack=True)

# Choose which catalogue file to read
input_path =       "/gpfs/data/d72fqv/Baryon_Cycle/"
output_path_base = "/gpfs/data/d72fqv/Baryon_Cycle/"+sim_name+"_subvols/"

# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name + ".hdf5"
File = h5py.File(input_path+filename)
warnings.filterwarnings('ignore')

vmax_med = np.zeros((20, len(snapshots)))
z_list2 = np.zeros(len(snapshots))
t_list2 = np.zeros(len(snapshots))

i_snap = 0
for snap, snip in zip(snapshots,snipshots):

    z = z_list[np.argmin(abs(snip-snip_list))]
    z_list2[i_snap] = z
    t_list2[i_snap], junk = uc.t_Universe(1./(1.+z), omm, h)
    

    # Read the data
    snapnum_group = File["Snap_"+str(snap)]
       
    subhalo_group = snapnum_group["subhalo_properties"]

    subgroup_number_subhalo = subhalo_group["SubGroupNumber_hydro"][:]
    vmax_subhalo = subhalo_group["Vmax"][:]

    g2Msun = 1./(1.989e33)
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h # Msun

    nbins = 20
    xmin = 10.0
    xmax = 14.0
    dx = (xmax - xmin) / nbins


    bins = np.arange(xmin, xmax+dx, dx)

    med, lo_perc, hi_perc, mid_bin = us.Calculate_Percentiles(bins, np.log10(mchalo_subhalo), vmax_subhalo, min_count=1, lohi=(0.16,0.84))

    '''print ""
    print "snap, snip", snap, snip
    print "log(mhalo), vmax, vmax/mhalo^1/3"
    for n in range(nbins):
        print mid_bin[n], med[n], med[n] / (10**mid_bin[n])**(1/3.)'''

    vmax_med[:,i_snap] = med
    #print snip, med

    i_snap += 1

# This block is to figure out when delta t first exceeds some threshold.
print snapshots[::-1]
print snipshots[::-1]
print t_list2[::-1][1:]-t_list2[::-1][0:-1]
dt= t_list2[::-1][1:]-t_list2[::-1][0:-1]
dt_cut = 0.2 # Gyr
ok = dt>dt_cut
print np.array(snipshots)[::-1][ok][1], np.array(snapshots)[::-1][ok][1]

print ""
for n in range(len(snapshots[1:])):
    print snapshots[::-1][n], snapshots[::-1][n+1], dt[n]
quit()

a_list = 1./(1+z_list2)

for n in range(nbins):
    print ""
    print mid_bin[n]
    print "vmax sqrt(a)", np.sqrt(a_list) * vmax_med[n]
    print "vmax", vmax_med[n]
    print "vmax a^1/4", vmax_med[n] * a_list**0.25

    if n == 10:
        break

File.close()
