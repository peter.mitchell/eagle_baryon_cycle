import numpy as np

DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
sim_name = "L0025N0376_REF_50_snip"
temporal_convergence_mode = False

output_path = "/gpfs/data/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_test_mode/"

# Reference z=0 1e12 halo (50 snip)
subvol = 36
snap_list = [35,500]
snip_list = [281,500]
snip_first_ps = 273
snip_ts_start = 281

import numpy as np
np.seterr(all='ignore')
import os
import h5py
import match_searchsorted as ms
import glob
import read_eagle as re
from utilities_plotting import *
import utilities_cosmology as uc
import utilities_statistics as us
import plotting_functions_visualize as up


def Centre_Halo(part_pos, halo_pos, boxsize):

    ok = (np.isnan(part_pos[:,0]) == False) & (np.isnan(part_pos[:,1]) == False) & (np.isnan(part_pos[:,2]) == False)

    # Correct for periodic boundary conditions
    if np.median(part_pos[:,0][ok]) > halo_pos[0] + 0.5 * boxsize:
        part_pos[:,0][ok] -= boxsize
    if halo_pos[0] > np.median(part_pos[:,0][ok]) + 0.5 * boxsize:
        part_pos[:,0][ok] += boxsize
    if np.median(part_pos[:,1][ok]) > halo_pos[1] + 0.5 * boxsize:
        part_pos[:,1][ok] -= boxsize
    if halo_pos[1] > np.median(part_pos[:,1][ok]) + 0.5 * boxsize:
        part_pos[:,1][ok] += boxsize
    if np.median(part_pos[:,2][ok]) > halo_pos[2] + 0.5 * boxsize:
        part_pos[:,2][ok] -= boxsize
    if halo_pos[2] > np.median(part_pos[:,2][ok]) + 0.5 * boxsize:
        part_pos[:,2][ok] += boxsize

    part_pos[:,0] -= halo_pos[0]
    part_pos[:,1] -= halo_pos[1]
    part_pos[:,2] -= halo_pos[2]

    return part_pos

def Read_Particle_Data(snip,tsnip=True):

    ######## First read headers to get simulation parameters #################
    if not "snap" in sim_name:
        SNIPS = glob.glob(DATDIR+'particledata_snip_0*')+glob.glob(DATDIR+'particledata_snip_1*')+glob.glob(DATDIR+'particledata_snip_2*')+glob.glob(DATDIR+'particledata_snip_3*')+glob.glob(DATDIR+'particledata_snip_4*')
    else:
        SNIPS = glob.glob(DATDIR+'particledata_0*')
    SNIPS.sort(key=os.path.getmtime)

    for SNIP in SNIPS:
        snip_str = str(snip)
        if len(snip_str) == 1:
            snip_str = "00"+snip_str
        elif len(snip_str) ==2:
            snip_str = "0" + snip_str        
        if not "snap" in sim_name:
            if "snip_"+snip_str in SNIP:
                path = SNIP
        else:
            if "data_"+snip_str in SNIP:
                path = SNIP


        # Get redshift for previous snipshot
        if not tsnip:
            snip_str_ps = str(snip_first_ps)
            if len(snip_str_ps) == 1:
                snip_str_ps = "00"+snip_str_ps
            elif len(snip_str_ps) ==2:
                snip_str_ps = "0" + snip_str_ps       
            if not "snap" in sim_name:
                if "snip_"+snip_str_ps in SNIP:
                    path_ps = SNIP
                    redshift_ps = float(path_ps.split('particledata_snip_')[-1][5:].replace('p','.'))
            else:
                if "data_"+snip_str_ps in SNIP:
                    path_ps = SNIP
                    redshift_ps = float(path_ps.split('particledata_')[-1][5:].replace('p','.'))

    if not "snap" in sim_name:
        egfile  = path+"/eagle_subfind_snip_particles"+path.split('particledata_snip')[-1]+'.0.hdf5'
    else:
        egfile  = path+"/eagle_subfind_particles"+path.split('particledata')[-1]+'.0.hdf5'
    sobj        = re.EagleSnapshot(egfile)
    h5py_file = h5py.File(egfile)

    h = h5py_file["Header"].attrs["HubbleParam"]
    omm = h5py_file["Header"].attrs["Omega0"]

    if not "snap" in sim_name:
        redshift = float(path.split('particledata_snip_')[-1][5:].replace('p','.'))
    else:
        redshift = float(path.split('particledata_')[-1][5:].replace('p','.'))
    expansion_factor = 1. / (1.+redshift)
    boxsize = sobj.boxsize # Mpc h^-1

    ######### Now read pre-dumped particle data ################
    path = output_path + "Snip_"+str(snip)+"/part_subvol_"+sim_name+"_snip_"+str(snip)+"_"+str(subvol)+".hdf5"
    subvol_file = h5py.File(path)

    if tsnip:
        gas_group = subvol_file["gas_ts"]; dm_group = subvol_file["dm_ts"]; star_group = subvol_file["star_ts"]; bh_group = subvol_file["bh_ts"]
    else:
        gas_group = subvol_file["gas_ps"]; dm_group = subvol_file["dm_ps"]; star_group = subvol_file["star_ps"]; bh_group = subvol_file["bh_ps"]

    # 0 => gas particles (PartType0)
    group_number_gas_list = gas_group['group_number_gas'][:]
    subgroup_number_gas_list = gas_group['subgroup_number_gas'][:]
    mass_gas_list = gas_group['mass_gas'][:]
    density_gas_list = gas_group["density_gas"][:]
    metallicity_gas_list = gas_group["metallicity_gas"][:]
    id_gas_list = gas_group["id_gas"][:]
    coordinates_gas_list = gas_group['coordinates_gas'][:]
    velocity_gas_list = gas_group["velocity_gas"][:]
    temperature_gas_list = gas_group["temperature_gas"][:]
    internal_energy_gas_list = gas_group["internal_energy_gas"][:]

    # 1 => dm particles (PartType1)
    group_number_dm_list = dm_group['group_number_dm'][:]
    subgroup_number_dm_list = dm_group['subgroup_number_dm'][:]
    mass_dm = h5py_file["Header"].attrs["MassTable"][1]
    coordinates_dm_list = dm_group['coordinates_dm'][:]

    # 4 => star particles (PartType4)
    group_number_star_list = star_group['group_number_star'][:]
    subgroup_number_star_list = star_group['subgroup_number_star'][:]
    mass_star_list = star_group['mass_star'][:]
    mass_star_init_list = star_group['mass_star_init'][:] 
    aform_star_list = star_group["aform_star"][:]
    id_star_list = star_group['id_star'][:]
    coordinates_star_list = star_group['coordinates_star'][:]

    # 5 => bh particles (PartType5)
    group_number_bh_list = bh_group['group_number_bh'][:]
    subgroup_number_bh_list = bh_group['subgroup_number_bh'][:]
    mass_bh_list = bh_group['mass_bh'][:] # Note I think this is the mass of the particle hosting the black hole, not the black hole itself
    coordinates_bh_list = bh_group['coordinates_bh'][:]

    subvol_file.close()
    h5py_file.close()

    gas_data = [group_number_gas_list, subgroup_number_gas_list, mass_gas_list, density_gas_list, metallicity_gas_list, id_gas_list, coordinates_gas_list, velocity_gas_list, temperature_gas_list, internal_energy_gas_list]
    dm_data = [group_number_dm_list, subgroup_number_dm_list, coordinates_dm_list]
    star_data = [group_number_star_list, subgroup_number_star_list, mass_star_list, mass_star_init_list, aform_star_list, id_star_list, coordinates_star_list]
    bh_data = [group_number_bh_list, subgroup_number_bh_list, mass_bh_list, coordinates_bh_list]
    sim_props = [h, redshift, expansion_factor, boxsize, mass_dm, omm]

    if tsnip:
        return gas_data, dm_data, star_data, bh_data, sim_props
    else:
        return gas_data, dm_data, star_data, bh_data, sim_props, redshift_ps

for i_snip, snip_ts in enumerate(snip_list[:-1]):

    snip_ns = snip_list[i_snip+1]
    if i_snip>0:
        snip_ps = snip_list[i_snip-1]
    else:
        snip_ps = snip_first_ps

    print "measuring for snip", snip_ts

    # Read subvol file to load halo catalogue
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    File = h5py.File(output_path_ts+filename_ts)

    print output_path_ts+filename_ts

    # Read halo catalogue
    subhalo_group = File["subhalo_data"]

    try:
        mchalo_list = subhalo_group["mchalo"][:]
    except:
        print "Error: first run copy_mchalo.py, then rerun select_subvolumes.py"
        quit()

    order = np.argsort(mchalo_list)[::-1]

    mchalo_list = mchalo_list[order]
    mhhalo_list = subhalo_group["mhhalo"][:][order]
    subgroup_number_list = subhalo_group["subgroup_number"][:][order]
    group_number_list = subhalo_group["group_number"][:][order]
    node_index_list = subhalo_group["node_index"][:][order]
    descendant_index_list = subhalo_group["descendant_index"][:][order]

    halo_grn = np.min(group_number_list)

    halo_x_list = subhalo_group["x_halo"][:][order] # cMpc h^-1
    halo_y_list = subhalo_group["y_halo"][:][order]
    halo_z_list = subhalo_group["z_halo"][:][order]
    halo_vx_list = subhalo_group["vx_halo"][:][order] # kms^-1
    halo_vy_list = subhalo_group["vy_halo"][:][order]
    halo_vz_list = subhalo_group["vz_halo"][:][order]
    halo_vmax_list = subhalo_group["vmax"][:][order]
    halo_r200_host_list = subhalo_group["r200_host"][:][order] # cMpc h^-1
    
    progenitor_group = File["progenitor_data"]

    ############## Read Eagle particle data #######################
    

    gas_data_ts, dm_data_ts, star_data_ts, bh_data_ts, sim_props_ts = Read_Particle_Data(snip_ts)
    
    # Unpack data
    group_number_gas_list_all_ts = gas_data_ts[0]
    subgroup_number_gas_list_all_ts = gas_data_ts[1]
    mass_gas_list_all_ts = gas_data_ts[2]
    density_gas_list_all_ts = gas_data_ts[3]
    metallicity_gas_list_all_ts = gas_data_ts[4]
    id_gas_list_all_ts = gas_data_ts[5]
    coordinates_gas_list_all_ts = gas_data_ts[6]
    velocity_gas_list_all_ts = gas_data_ts[7]
    temperature_gas_list_all_ts = gas_data_ts[8]
    internal_energy_gas_list_all_ts = gas_data_ts[9]

    print np.unique(group_number_gas_list_all_ts)
    print np.sort(group_number_gas_list_all_ts[group_number_gas_list_all_ts>0])

    halo_grn = np.min(group_number_gas_list_all_ts[group_number_gas_list_all_ts>0])

    halo_x = halo_x_list[0]
    halo_y = halo_y_list[0]
    halo_z = halo_z_list[0]

    boxsize = sim_props_ts[3]

    coordinates_gas_list_all_ts = Centre_Halo(coordinates_gas_list_all_ts, [halo_x,halo_y,halo_z], boxsize)

    r_gas = np.sqrt(np.sum(np.square(coordinates_gas_list_all_ts),axis=1))

    close = r_gas < halo_r200_host_list[0]
    print ""
    print np.unique(group_number_gas_list_all_ts[close])

    print len(group_number_gas_list_all_ts[close][group_number_gas_list_all_ts[close]==-1])

    print np.sort(r_gas), halo_r200_host_list[0]

    print len(close[close]), len(close)

    test = halo_grn == group_number_gas_list_all_ts
    print ""
    print "why hello there"
    for val in np.unique(subgroup_number_gas_list_all_ts[test]):
        print val
    print ""
    print np.bincount(subgroup_number_gas_list_all_ts[test])[np.bincount(subgroup_number_gas_list_all_ts[test])>0]

    print ""
    test2 = -halo_grn == group_number_gas_list_all_ts
    print np.unique(subgroup_number_gas_list_all_ts[test2])
    print np.bincount(subgroup_number_gas_list_all_ts[test2])

    test3 = (group_number_gas_list_all_ts == -halo_grn)
    print ""
    print np.unique(subgroup_number_gas_list_all_ts[test3])

    ok = (group_number_gas_list_all_ts == halo_grn) | (group_number_gas_list_all_ts == -halo_grn)
    grn = group_number_gas_list_all_ts[ok]
    sgrn = subgroup_number_gas_list_all_ts[ok]

    # From Rob
    # These are in the FoF group and bound to a subfind subhalo
    a = len(grn[(grn>0)&(sgrn<1e8)])
    # These are in the FoF group and are not bound to a subfind halo
    b = len(grn[(grn>0)&(sgrn>1e8)])
    # These are not in the FoF group but are bound to a subfind halo - this can never happen because of how subfind works
    c = len(grn[(grn<0)&(sgrn<1e8)])
    # These are not in the FoF group (and so also not in a subfind subhalo), but are within a Spherical Overdensity group which just so happens to be r200!
    d = len(grn[(grn<0)&(sgrn>1e8)])

    print a,b,c,d

    quit()

    # Identify gas particles that are bound to subhaloes
    bound_parts = subgroup_number_gas_list_all_ts >= 0
    group_number_gas_list_ts = group_number_gas_list_all_ts[bound_parts] 
    subgroup_number_gas_list_ts = subgroup_number_gas_list_all_ts[bound_parts] 
    mass_gas_list_ts = mass_gas_list_all_ts[bound_parts] 
    density_gas_list_ts = density_gas_list_all_ts[bound_parts]
    metallicity_gas_list_ts = metallicity_gas_list_all_ts[bound_parts]
    id_gas_list_ts = id_gas_list_all_ts[bound_parts]
    coordinates_gas_list_ts = coordinates_gas_list_all_ts[bound_parts]
    velocity_gas_list_ts = velocity_gas_list_all_ts[bound_parts]
    temperature_gas_list_ts = temperature_gas_list_all_ts[bound_parts]
    internal_energy_gas_list_ts = internal_energy_gas_list_all_ts[bound_parts]

    # Prune particle lists to contain only particles bound to subhalos we are interested in (this is a huge optimization step for nsub_1d > 1)
    
    ptr = ms.match(group_number_gas_list_ts, group_number_list)
    ok_match = ptr >= 0

    group_number_gas_list_ts = group_number_gas_list_ts[ok_match]
    subgroup_number_gas_list_ts = subgroup_number_gas_list_ts[ok_match]
    mass_gas_list_ts = mass_gas_list_ts[ok_match]
    density_gas_list_ts = density_gas_list_ts[ok_match]
    metallicity_gas_list_ts = metallicity_gas_list_ts[ok_match]
    id_gas_list_ts = id_gas_list_ts[ok_match]
       
    coordinates_gas_list_ts = coordinates_gas_list_ts[ok_match]
    velocity_gas_list_ts = velocity_gas_list_ts[ok_match]
    temperature_gas_list_ts = temperature_gas_list_ts[ok_match]
    internal_energy_gas_list_ts = internal_energy_gas_list_ts[ok_match]
    
    h = sim_props_ts[0]
    expansion_factor_ts = sim_props_ts[2]
    mass_dm = sim_props_ts[4]
    redshift_ts = sim_props_ts[1]
    omm = sim_props_ts[5]
    boxsize = sim_props_ts[3]
