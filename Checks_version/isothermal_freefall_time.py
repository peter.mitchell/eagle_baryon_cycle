import numpy as np
import scipy.integrate as si

# This routine calculates the freefall time, starting from rest from within in an isothermal sphere halo of mass, mhalo, and outer radius, rhalo
# Starting radius is r_init, final radius is r_final

G = 6.67*10**-11 # m^3 s^-2 kg^-1
kpc2m = 3.09*10**19 #m
msun = 2e30 # kg
s2Myr = 1./(24*365.25*60**2*10**6)

mhalo = 1e12 # Msun
rhalo = 200. # kpc
r_init = 30. # kpc
r_final = 0. # kpc

a = mhalo / (rhalo*4*np.pi) # Msun kpc^-1 -> normalisation constant for the density profile

integrand = lambda r, r_init: (np.log(r_init)-np.log(r))**-0.5

# See https://physics.stackexchange.com/questions/94746/gravitational-collapse-and-free-fall-time-spherical-pressure-free for a similar derivation to this equation (but for a collapsing sphere)
t = (8*np.pi*a *msun/kpc2m *G)**-0.5 * si.quad(integrand,r_init,r_final,args=(r_init,))[0] * kpc2m
t *= s2Myr

print t
