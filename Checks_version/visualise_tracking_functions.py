import h5py
import numpy as np
import sys
sys.path.append('../')
import measure_particles_functions as mpf
import os

def Read_Pre_Dumped_Part_Data(snip,output_path,sim_name,subvol):
    path = output_path + "Snip_"+str(snip)+"/part_subvol_"+sim_name+"_snip_"+str(snip)+"_"+str(subvol)+".hdf5"

    if not os.path.isfile(path):
        print "Error, no pre-dumped data at", path
        quit()

    subvol_file = h5py.File(path)

    gas_group = subvol_file["gas_ts"]; dm_group = subvol_file["dm_ts"]; star_group = subvol_file["star_ts"]; bh_group = subvol_file["bh_ts"]

    # 0 => gas particles (PartType0)                                                                                                                                                                                                                                                                                     
    group_number_gas_list = gas_group['group_number_gas'][:]
    subgroup_number_gas_list = gas_group['subgroup_number_gas'][:]
    mass_gas_list = gas_group['mass_gas'][:]
    density_gas_list = gas_group["density_gas"][:]
    metallicity_gas_list = gas_group["metallicity_gas"][:]
    id_gas_list = gas_group["id_gas"][:]
    coordinates_gas_list = gas_group['coordinates_gas'][:]
    velocity_gas_list = gas_group["velocity_gas"][:]
    temperature_gas_list = gas_group["temperature_gas"][:]
    internal_energy_gas_list = gas_group["internal_energy_gas"][:]

    if np.sort(group_number_gas_list)[0] > -2:
        print "Doesn't contain all FoF and SO particles - rerun write_part"
        quit()

    # 1 => dm particles (PartType1)                                                                                                                                                                                                                                                                                      
    group_number_dm_list = dm_group['group_number_dm'][:]
    subgroup_number_dm_list = dm_group['subgroup_number_dm'][:]
    coordinates_dm_list = dm_group['coordinates_dm'][:]

    # 4 => star particles (PartType4)                                                                                                                                                                                                                                                                                    
    group_number_star_list = star_group['group_number_star'][:]
    subgroup_number_star_list = star_group['subgroup_number_star'][:]
    mass_star_list = star_group['mass_star'][:]
    mass_star_init_list = star_group['mass_star_init'][:]
    aform_star_list = star_group["aform_star"][:]
    id_star_list = star_group['id_star'][:]
    coordinates_star_list = star_group['coordinates_star'][:]

    # 5 => bh particles (PartType5)                                                                                                                                                                                                                                                                                      
    group_number_bh_list = bh_group['group_number_bh'][:]
    subgroup_number_bh_list = bh_group['subgroup_number_bh'][:]
    mass_bh_list = bh_group['mass_bh'][:] # Note I think this is the mass of the particle hosting the black hole, not the black hole itself                                                                                                                                                                             \
                                                                                                                                                                                                                                                                                                                         
    coordinates_bh_list = bh_group['coordinates_bh'][:]

    extra_data = subvol_file["extra_data"]
    mass_dm = extra_data["mass_dm"][()]
    h = extra_data["h"][()]
    redshift = extra_data["redshift"][()]
    expansion_factor = extra_data["expansion_factor"][()]
    boxsize = extra_data["boxsize"][()]
    omm = extra_data["omm"][()]

    subvol_file.close()

    gas_data = [group_number_gas_list, subgroup_number_gas_list, mass_gas_list, density_gas_list, metallicity_gas_list, id_gas_list, coordinates_gas_list, velocity_gas_list, temperature_gas_list, internal_energy_gas_list]
    dm_data = [group_number_dm_list, subgroup_number_dm_list, coordinates_dm_list]
    star_data = [group_number_star_list, subgroup_number_star_list, mass_star_list, mass_star_init_list, aform_star_list, id_star_list, coordinates_star_list]
    bh_data = [group_number_bh_list, subgroup_number_bh_list, mass_bh_list, coordinates_bh_list]
    sim_props = [h, redshift, expansion_factor, boxsize, mass_dm, omm]

    return gas_data, dm_data, star_data, bh_data, sim_props

def Rebind_Subhalos(grn_in, sgrn_in, coord, halo_grn, halo_sgrn, halo_coord, r200_host, boxsize, verbose=False):


    # Set SO particles that don't belong to a FoF group to the central subhalo of the FoF group
    grn = np.abs(grn_in)

    # Set FoF particles that are not bound to a subhalo to the central subhalo 
    sgrn = np.copy(sgrn_in)
    sgrn[sgrn > 1e8] = 0

    nsep = max(len(str(int(sgrn.max()))) +1 ,6)
    subhalo_index = grn* 10**nsep + sgrn

    order = np.argsort(subhalo_index)
    grn_sorted = grn[order] # Note that indexing in this way does create a new array (not so for [blah:blah])
    sgrn_sorted = sgrn[order]
    coord_sorted = coord[order]
    subhalo_index = subhalo_index[order]

    subhalo_index_unique, particle_index = np.unique(subhalo_index, return_index=True)
    particle_index = np.append(particle_index,len(grn))

    halo_subhalo_index = halo_grn * 10**nsep + halo_sgrn

    if len(np.unique(halo_subhalo_index)) != len(halo_subhalo_index):
        print "Error: duplicates"
        quit()

    if verbose:
        print ""
        print "Nbound part before", len(grn_in[(grn_in>0)&(sgrn_in<1e7)])
        print "Nbound part after", len(grn_sorted[(grn_sorted>0)&(sgrn_sorted<1e7)])

    # Loop over central subhalos (we could remove satellite particles outside r200 of the host at a later point)
    for i_subhalo in range(len(halo_grn)):
        if halo_sgrn[i_subhalo] > 0:
            continue

        # Find particles in FoF group, and associated to this central subhalo  
        match = np.where( halo_subhalo_index[i_subhalo] == subhalo_index_unique)[0]

        if len(match)==0:
            continue
        elif len(match) > 1:
            print "Error: duplicate values"
            quit()

        index1 = particle_index[match[0]]
        index2 = particle_index[match[0]+1]
        coord_i = coord_sorted[index1:index2]

        # Centre on halo                                                       
        halo_coord_i = [halo_coord[0][i_subhalo], halo_coord[1][i_subhalo], halo_coord[2][i_subhalo]]
        coord_i = mpf.Centre_Halo(coord_i, halo_coord_i, boxsize)

        # Flag particles that are outside r200                                 
        r_part = np.sqrt(np.sum(np.square(coord_i),axis=1))

        r200_i = r200_host[i_subhalo]
        outside = np.where(r_part > r200_i)

        # Set grn and sgrn of these particles to NaN                           
        grn_sorted[index1:index2][outside] = -1e8
        sgrn_sorted[index1:index2][outside] = -1e8

    reverse_order = np.argsort(order)
    grn = grn_sorted[reverse_order]
    sgrn = sgrn_sorted[reverse_order]

    if verbose:
        print "Nbound part final", len(grn[(grn>0)&(sgrn<1e7)])
        print ""

    # Return unbound particles to clear <0 groun number state (to reduce chance of making an error later)
    grn[grn > 1e8] = -1e9

    return grn, sgrn
