import numpy as np; from scipy.interpolate import RectBivariateSpline; from scipy.optimize import leastsq, curve_fit

def Weighted_Percentile(data_in,weight_in,perc):
    if perc > 1 or perc < 0:
        print 'Error: The requested percentile needs to lie between 0 and 1'
        exit()
    
    data = np.copy(data_in); weight = np.copy(weight_in)

    #Deal with "not a number" entries
    nans = np.isnan(data)
    data = data[nans==False]; weight = weight[nans==False]
    if len(data) == 0:
        return np.nan

    order = np.argsort(data)
    cum_weight = np.cumsum(weight[order])

    select = cum_weight < np.sum(weight) * perc
    index = len(cum_weight[select])
    
    output = np.sort(data)[index]

    if float(np.sum(weight)) * perc  == float(cum_weight[index]):
        output = 0.5 * (np.sort(data)[index] + np.sort(data)[index+1])

    return output

def TwoD_Histogram(x,y,binx,biny,z=None,weights=None,statistic=None):
    if weights is None:
        weights = np.ones_like(x)
    data, xedges, yedges = np.histogram2d(x,y,bins=[binx,biny],weights=weights)
    extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
    if statistic is None:
        return data,extent
    print "Routine isn't finished yet!"
    exit()

def Calculate_Logarithmic_Point_Density(binx,biny,x,y,weights=None):
    mid_binx = 0.5 * (binx[:-1] + binx[1:]); mid_biny = 0.5*(biny[:-1]+biny[1:])
    if weights is not None:
        data, extent = TwoD_Histogram(x, y, binx, biny, weights = weights)
    else:
        data, extent = TwoD_Histogram(x, y, binx, biny)
    spline = RectBivariateSpline(mid_binx, mid_biny, data)
    density = spline.ev(x,y)

    # This makes the density logarithmic
    density[density<density.max()/1000.0] = density.max()/1000.0
    density *= 1.0 / density.max()
    density = np.log10(density)

    # This option will normalise the density to add up to unity for each x bin
    #for n in range(len(mass)):
    #    density[n] *= len(mass[(mass>mass[n]/1.5)&(mass<mass[n]*1.5)])/float(len(mass))

    return density

def Calculate_Percentiles(bin,x,y,weights=None,min_count=10,lohi=(0.1,0.9),complete=None):
    mid_bin = 0.5 * (bin[:-1] + bin[1:])
    
    med = np.zeros_like(mid_bin); lo_perc = np.zeros_like(mid_bin); hi_perc = np.zeros_like(mid_bin)

    if complete is not None:
        complete_bin = np.zeros_like(med)

    if weights is None:
        weights = np.ones_like(x)

    for n in range(len(mid_bin)):
        index_bin = (x<bin[n+1]) & (x>bin[n])

        if len(x[index_bin]) > min_count:
            med[n] = Weighted_Percentile(y[index_bin],weights[index_bin],0.5)
            lo_perc[n] = Weighted_Percentile(y[index_bin],weights[index_bin],lohi[0])
            hi_perc[n] = Weighted_Percentile(y[index_bin],weights[index_bin],lohi[1])

            if complete is not None:
                ok = (np.isnan(y[index_bin])==False) & (y[index_bin]!=np.inf)
                if len(y[index_bin][ok]) >= complete * len(y[index_bin]):
                    complete_bin[n] = 1

        else:
            med[n] = np.nan; lo_perc[n] = np.nan; hi_perc[n] = np.nan

    if complete is not None:
        return med, lo_perc, hi_perc, mid_bin, complete_bin
    else:
        return med, lo_perc, hi_perc, mid_bin

def Mean_Median_Spread(bin,x,y,weights=None,min_count=10,ymin=None):
    mid_bin = 0.5 * (bin[:-1] + bin[1:])

    sigma = np.zeros_like(mid_bin); med = np.zeros_like(mid_bin)

    if weights is None:
        weights = np.ones_like(x)

    for n in range(len(mid_bin)):
        index_bin = (x<bin[n+1]) & (x>bin[n])

        if len(x[index_bin]) > min_count:
            med[n] = Weighted_Percentile(y[index_bin],weights[index_bin],0.5)
            sigma[n] =  0.5*abs(Weighted_Percentile(y[index_bin],weights[index_bin],0.84)-Weighted_Percentile(y[index_bin],weights[index_bin],0.16))
            
            # Don't include x bins where y distribution is incomplete in the average.
            if ymin is not None:
                lo_perc = Weighted_Percentile(y[index_bin],weights[index_bin],0.1)
                if lo_perc - 0.05 < ymin:
                    med[n] = np.nan; sigma[n] = np.nan

        else:
            med[n] = np.nan; sigma[n] = np.nan

    med_masked = np.ma.masked_array(med,mask=np.isnan(med))
    mean_median = med_masked.mean()
    mean_sigma = np.mean(sigma[sigma>0.])

    return mean_median, mean_sigma

def Fit_Line(xin,yin,mguess,cguess,weights=None):

    f_fit = lambda guess,x: guess[0] * x + guess[1]
    
    if weights == None:
        f_res = lambda guess,x,y: f_fit(guess,x) - y
    else:
        f_res = lambda guess,x,y: (f_fit(guess,x) - y) * weights

    guess = [mguess,cguess]

    fit, ier = leastsq(f_res,guess[:],args=(xin,yin))
    
    #func = lambda x,m,c: m*x+c
    #fit, cov = curve_fit(func,xin,yin,guess)
    
    #A = np.vstack([ xin, np.ones_like(xin) ]).T
    #fit = np.linalg.lstsq(A, yin)[0]

    #from scipy import stats
    #m,c,r,p,e = stats.linregress(xin,yin)
    #fit = np.zeros(2)
    #fit[0] = m; fit[1] = c

    #m,c = np.polyfit(xin,yin,1)
    #fit = np.zeros(2); fit[0] = m; fit[1] = c
    
    return fit[0], fit[1]

def Fit_Line_1_Sigma(x,y,sigma,m_grid_in,c_grid):
    
    fitting_function = lambda m,c,x: m*x +c

    # Reshape for broadcasting purposes
    m_grid = np.reshape(m_grid_in,(len(m_grid_in),1))

    # Calculate y model grid, it has dimenstions of (x,m,c)
    y_grid = fitting_function(m_grid,c_grid,np.reshape(x,(len(x),1,1)))

    # Calculate chi squared for each point on the grid
    chi_squared = np.sum(np.square(np.reshape(y,(len(x),1,1))-y_grid)/np.square(sigma),axis=0)

    # Calculate best fit parameters
    ind = np.unravel_index(np.argmin(chi_squared),chi_squared.shape)
    m_best = m_grid[ind[0]][0]; c_best = c_grid[ind[1]]

    # Compute likelihoods for each point on the grid
    L = np.exp(-0.5*chi_squared)

    # Normalise
    Ltot = np.sum(L); L *= 1./Ltot

    # Calculate m,c values inside 1 sigma contour
    Lravel = np.ravel(L)
    cv,mv = np.meshgrid(c_grid,m_grid[:,0])
    mvravel = np.ravel(mv); cvravel = np.ravel(cv)

    order = np.argsort(Lravel)

    Lcumsum = np.cumsum(Lravel[order])
    ok = Lcumsum > 1.0-0.6827

    m1sig = mvravel[order][ok]; c1sig = cvravel[order][ok]
    m1sig = np.reshape(m1sig,(len(m1sig),1))
    c1sig = np.reshape(c1sig,(len(c1sig),1))

    #print c1sig.min(), c1sig.max()

    y_1sig_grid = fitting_function(m1sig,c1sig,x)

    y_min = np.amin(y_1sig_grid,axis=0)
    y_max = np.amax(y_1sig_grid,axis=0)

    return y_min, y_max, m_best,c_best

if __name__=='__main__':

    import pylab as py
    # Parameter grid parameters
    mlo = 0.0; mhi = 4.0; nm = 200.
    clo = -5.0; chi = 5.0; nc = 160.

    m_true = 2.2; c_true = -3.4
    print m_true, c_true

    sigma = 1.3

    x = np.arange(0.0,10.0,0.05); nx = len(x); print nx, ' data points'
    y = m_true * x + c_true + np.random.randn(len(x))*sigma

    m_grid = np.arange(mlo,mhi,(mhi-mlo)/nm)

    c_grid = np.arange(clo,chi,(chi-clo)/nc)

    y_min, y_max, m_best, c_best = Fit_Line_1_Sigma(x,y,sigma,m_grid,c_grid)

    py.fill_between(x,y_min,y_max,alpha=1.0,facecolor='r')

    fitting_function = lambda m,c,x: m*x +c

    py.plot(x,fitting_function(m_best, c_best,x),c='k')
    py.scatter(x,y)
    py.show()
