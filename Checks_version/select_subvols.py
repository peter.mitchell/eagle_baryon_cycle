import numpy as np
import h5py
import os
import warnings
import match_searchsorted as ms

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
h=0.6777
box_size = 25 * h
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1'''


# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"                                                                                                                                                                                               
h=0.6777
box_size = 25 * h
snap_final = 100
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
                    73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
                        145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
                        217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
                        289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
                        361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"                                                                                                                                                                                               
h=0.6777
box_size = 25 * h
snap_final = 200
snapshots = np.arange(1,snap_final+1)[::-1]
snipshots = [  1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
               37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
               73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
               109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
               145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
               181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
               217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
               253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
               289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
               325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
               361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
               397, 399][::-1]
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1'''

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
h=0.6777
box_size = 25 * h
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1'''

# 25 Mpc ref 500 snapshots
sim_name = "L0025N0376_REF_500_snap"
h=0.6777
box_size = 25 * h
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
nsub_1d = 1

if len(snapshots)>len(snipshots):
    snapshots = snapshots[0:-1]
if len(snapshots)>len(snipshots):
    print "Error, len(snapshots) != len(snipshots)"
    quit()

# Mass scale for which to separate massive haloes (and their subhaloes) from the overthise equal volume subvolumes at z=0
mhhalo_sep = 10**14

# Choose which catalogue file to read
input_path =       "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path_base = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols/"

test_mode = True # Write a single subvolume with a non-default nsub_1d
if test_mode:
    print "running in test mode"
    nsub_1d = 1

    # Halo 1
    #mchalo_choose = 10**12 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**11.9
    
    # Halo 2
    #mchalo_choose = 10**11 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**10.9
    
    # Halo 3
    #mchalo_choose = 5*10**10 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 4.9*10**10

    # Halo 4
    #mchalo_choose = 10**11.9 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**11.8

    # Test halo 5 
    #mchalo_choose = 6.9*10**10 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 5*10**10
    
    # Test halo 6
    #mchalo_choose = 0.8*10**12 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10*10**11.5

    # Test halo 7
    #mchalo_choose = 10**13.5 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**13

    # New tests for looking at convergence at z=2
    # Test halo 8, selected to be mhalo=1e11 at z=2
    mchalo_choose = 10**11
    mhhalo_sep = 10**10.8
    if "500_snap" in sim_name:
        ok = snapshots <= 272
        snapshots = snapshots[ok]
        snipshots = snipshots[ok]
    else:
        print "Test halo 8 is selected at z=2, only implemented for 500 snap trees at present"
        quit()

    #nsub_1d = 4
    output_path_base = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_test_mode/"

# Choose r200 fractions to compute subvol sizes
r200_frac = 1.5



# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name
print "not using test mode subhalo catalogue for now"
#if test_mode:
#    filename += "_test_mode"
filename += ".hdf5"

print "Reading", input_path+filename

File = h5py.File(input_path+filename)
warnings.filterwarnings('ignore')

#### Get tree time/redshift info ########
tree_snapshot_info = File["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
z_snapshots = tree_snapshot_info["redshifts_tree"][:]
t_snapshots = tree_snapshot_info["cosmic_times_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]

############### Select subvolumes for z0 subhaloes #####################################

# First, read the z_final snapshot information
# (by default this is z=0 unless you edit snapshots/snipshots arrays earlier on
snapnum_group = File["Snap_"+str(snapshots.max())]
subhalo_group = snapnum_group["subhalo_properties"]
node_index = subhalo_group["nodeIndex"][:]
subgroup_number = subhalo_group["SubGroupNumber_hydro"][:]
group_number = subhalo_group["GroupNumber_hydro"][:]
halo_x_subhalo = subhalo_group["CentreOfPotential_x"][:] # Mpc h^-1
halo_y_subhalo = subhalo_group["CentreOfPotential_y"][:]
halo_z_subhalo = subhalo_group["CentreOfPotential_z"][:]

subhalo_noprogen_group = snapnum_group["subhalo_no_progenitor"]
node_index_noprogen = subhalo_noprogen_group["nodeIndex"][:]
halo_x_noprogen = subhalo_noprogen_group["CentreOfPotential_x"][:] # Mpc h^-1
halo_y_noprogen = subhalo_noprogen_group["CentreOfPotential_y"][:]
halo_z_noprogen = subhalo_noprogen_group["CentreOfPotential_z"][:]
subgroup_number_noprogen = subhalo_noprogen_group["SubGroupNumber_hydro"][:]
group_number_noprogen = subhalo_noprogen_group["GroupNumber_hydro"][:]

g2Msun = 1./(1.989e33)
try:
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h
    mhhalo_subhalo = subhalo_group["mhhalo"][:] * g2Msun * 1.989e43 /h # Msun
except:
    print "Error: need to run copy_mchalo.py first"
    quit()


# Separate out haloes (and their subhaloes) which are above a given mass point at z=0 (these will get their own separate subvolumes)
sep = mhhalo_subhalo > mhhalo_sep


# For the remaining haloes, subdivide them into equal volume subvolumes at z=0 (otherwise you need a space-filling curve in python)

# Compute subvolume vertices
xyz_subvol = np.zeros((nsub_1d**3,3))
ind_sub = 0
for i in range(nsub_1d):
    x_sub = i / float(nsub_1d)
    for j in range(nsub_1d):
        y_sub = j / float(nsub_1d)
        for k in range(nsub_1d):
            z_sub = k / float(nsub_1d)
            xyz_subvol[ind_sub] = np.array([x_sub, y_sub, z_sub])
            ind_sub += 1
            
# Build node index list
node_index_list = []
node_index_noprogen_list = []
for n in range(nsub_1d**3):

    xmin,ymin,zmin = xyz_subvol[n]

    dx = 1/float(nsub_1d)
    xmax = xmin + dx; ymax = ymin + dx; zmax = zmin + dx

    ok = (halo_x_subhalo > xmin*box_size) & (halo_x_subhalo < xmax*box_size) & (halo_y_subhalo > ymin*box_size) & (halo_y_subhalo < ymax*box_size) & (halo_z_subhalo > zmin*box_size) & (halo_z_subhalo < zmax*box_size) & (sep == False) & (subgroup_number==0)

    ptr = ms.match( group_number[subgroup_number>0], group_number[ok])
    ok[subgroup_number>0] = ptr >= 0

    node_index_list.append(node_index[ok])

    ok2 = (halo_x_noprogen > xmin*box_size) & (halo_x_noprogen < xmax*box_size) & (halo_y_noprogen > ymin*box_size) & (halo_y_noprogen < ymax*box_size) & (halo_z_noprogen > zmin*box_size) & (halo_z_noprogen < zmax*box_size) & (subgroup_number_noprogen==0)

    ptr = ms.match( group_number_noprogen[subgroup_number_noprogen>0], group_number_noprogen[ok2])
    ok2[subgroup_number_noprogen>0] = ptr >= 0

    node_index_noprogen_list.append(node_index_noprogen[ok2]) 

sep_cen = sep & (subgroup_number == 0)
nsep = len(subgroup_number[sep_cen])
for n in range(nsep):
    ok = group_number[sep] == group_number[sep_cen][n]
    node_index_list.append(node_index[sep][ok])

    node_index_noprogen_list.append([])

if test_mode:
    ind_choose = np.argmin(abs(mchalo_subhalo[sep_cen]-mchalo_choose))

# Total number of subvolumes
nsubvol = nsub_1d**3 + nsep

################# Now loop through snipshots, read in the full catalogue at each point and then loop through the subvolumes

def z2snip(z):
    snip = snipnum_list[ np.argmin(abs(ref_z-z)) ]
    return snip

min_grn_prev = 1e9
junk = 0
for snap, snip in zip(snapshots,snipshots):

    junk+=1
    print "building subvolumes for tree snapshot, snipshot", snap, snip
    
    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue

    #if snap < 49:
    #    print "quitting after first snap"
    #    quit()

    # Read the data
    snapnum_group = File["Snap_"+str(snap)]
       
    subhalo_group = snapnum_group["subhalo_properties"]
    subhalo_noprogen_group = snapnum_group["subhalo_no_progenitor"]
    progenitor_group = snapnum_group["main_progenitors"]
    all_subhalo_ns_group = snapnum_group["all_subhalo_ns"]

    group_number_subhalo = subhalo_group["GroupNumber_hydro"][:]
    subgroup_number_subhalo = subhalo_group["SubGroupNumber_hydro"][:]

    redshift_ts = np.array(subhalo_group["redshift"])
    redshift_ps = np.array(progenitor_group["redshift"])

    try:
        nodeIndex_subhalo = subhalo_group["nodeIndex"][:]
    except:
        output_path = output_path_base + "Snip_"+str(snip)+"/"
        try:
            os.makedirs(output_path)
        except OSError:
            pass
        continue

    descendantIndex_subhalo = subhalo_group["descendantIndex"][:]
    halo_x_subhalo = subhalo_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_subhalo = subhalo_group["CentreOfPotential_y"][:]
    halo_z_subhalo = subhalo_group["CentreOfPotential_z"][:]
    halo_vx_subhalo = subhalo_group["Velocity_x"][:] # kms^-1
    halo_vy_subhalo = subhalo_group["Velocity_y"][:]
    halo_vz_subhalo = subhalo_group["Velocity_z"][:]
    vmax_subhalo = subhalo_group["Vmax"][:]
    halo_r200_host_subhalo = subhalo_group["Group_R_Crit200_host"][:]

    # rvir will be zero for satellites, but this should have no adverse effects (will also be zero for 25Mpc ref, but again no problem provided nsub_1d = 1)
    rvir_subhalo = subhalo_group["r200"][:] # kpc
    rvir_subhalo *= h/1e3*(1+redshift_ts) # cMpc h^-1

    g2Msun = 1./(1.989e33)
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h # Msun
    mhhalo_subhalo = subhalo_group["mhhalo"][:] * g2Msun * 1.989e43 /h # Msun

    group_number_noprogen = subhalo_noprogen_group["GroupNumber_hydro"][:]
    subgroup_number_noprogen = subhalo_noprogen_group["SubGroupNumber_hydro"][:]
    nodeIndex_noprogen = subhalo_noprogen_group["nodeIndex"][:]
    descendantIndex_noprogen = subhalo_noprogen_group["descendantIndex"][:]
    halo_x_noprogen = subhalo_noprogen_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_noprogen = subhalo_noprogen_group["CentreOfPotential_y"][:]
    halo_z_noprogen = subhalo_noprogen_group["CentreOfPotential_z"][:]
    halo_vx_noprogen = subhalo_noprogen_group["Velocity_x"][:] # kms^-1
    halo_vy_noprogen = subhalo_noprogen_group["Velocity_y"][:] # kms^-1
    halo_vz_noprogen = subhalo_noprogen_group["Velocity_z"][:]
    vmax_noprogen = subhalo_noprogen_group["Vmax"][:]
    rvir_noprogen = subhalo_noprogen_group["r200"][:] # kpc
    rvir_noprogen *= h/1e3*(1+redshift_ts) # cMpc h^-1

    group_number_progenitor = progenitor_group["GroupNumber_hydro"][:]
    subgroup_number_progenitor = progenitor_group["SubGroupNumber_hydro"][:]
    halo_x_progenitor = progenitor_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_progenitor = progenitor_group["CentreOfPotential_y"][:]
    halo_z_progenitor = progenitor_group["CentreOfPotential_z"][:]
    halo_vx_progenitor = progenitor_group["Velocity_x"][:] # kms^-1
    halo_vy_progenitor = progenitor_group["Velocity_y"][:]
    halo_vz_progenitor = progenitor_group["Velocity_z"][:]
    vmax_progenitor = progenitor_group["Vmax"][:]
    # rvir will be zero for satellites, but this should have no adverse effects
    rvir_progenitor = progenitor_group["r200"][:] # kpc
    rvir_progenitor *= h/1e3*(1+redshift_ps) # cMpc h^-1
    node_index_progenitor = progenitor_group["nodeIndex"][:]
    halo_r200_host_progenitor = progenitor_group["Group_R_Crit200_host"][:]

    group_number_all_progenitor = all_subhalo_ns_group["GroupNumber"][:]
    subgroup_number_all_progenitor = all_subhalo_ns_group["SubGroupNumber"][:]
    halo_x_all_progenitor = all_subhalo_ns_group["CentreOfPotential_x"][:] # Mpc h^-1
    halo_y_all_progenitor = all_subhalo_ns_group["CentreOfPotential_y"][:]
    halo_z_all_progenitor = all_subhalo_ns_group["CentreOfPotential_z"][:]
    descendant_index_all_progenitor = all_subhalo_ns_group["descendantIndex"][:]
    node_index_all_progenitor = all_subhalo_ns_group["nodeIndex"][:]
    halo_r200_host_all_progenitor = all_subhalo_ns_group["Group_R_Crit200_host"][:]

    # Choose where to output subvol files and write directory if needed
    output_path = output_path_base + "Snip_"+str(snip)+"/"
    try:
        os.makedirs(output_path)
    except OSError:
        pass



    if nsub_1d > 1:

        # For all progenitor list, just take crude step of cloning the sample if they are within a given distance to the box boundary
        clone_dist = 200.0 /1e3 *h # 200 kpc in units of Mpc h^-1

        near_large_x = halo_x_all_progenitor > box_size - clone_dist
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_large_x]-box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_large_x]-box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_large_x]-box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_large_x]-box_size)
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_large_x]-box_size)
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_large_x]-box_size)
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_large_x]-box_size)

        near_small_x = halo_x_all_progenitor < clone_dist
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_small_x]+box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_small_x]+box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_small_x]+box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_small_x]+box_size)
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_small_x]+box_size)
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_small_x]+box_size)
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_small_x]+box_size)

        near_large_y = halo_y_all_progenitor > box_size - clone_dist
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_large_y]-box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_large_y]-box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_large_y]-box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_large_y]-box_size)
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_large_y]-box_size)
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_large_y]-box_size)
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_large_y]-box_size)
        
        near_small_y = halo_y_all_progenitor < clone_dist
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_small_y]+box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_small_y]+box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_small_y]+box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_small_y]+box_size)
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_small_y]+box_size)
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_small_y]+box_size)
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_small_y]+box_size)
        
        near_large_z = halo_z_all_progenitor > box_size - clone_dist
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_large_z]-box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_large_z]-box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_large_z]-box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_large_z]-box_size)
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_large_z]-box_size)
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_large_z]-box_size)
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_large_z]-box_size)

        near_small_z = halo_z_all_progenitor < clone_dist
        halo_x_all_progenitor = np.append(halo_x_all_progenitor, halo_x_all_progenitor[near_small_z]+box_size)
        halo_y_all_progenitor = np.append(halo_y_all_progenitor, halo_y_all_progenitor[near_small_z]+box_size)
        halo_z_all_progenitor = np.append(halo_z_all_progenitor, halo_z_all_progenitor[near_small_z]+box_size)
        group_number_all_progenitor = np.append(group_number_all_progenitor, group_number_all_progenitor[near_small_z]+box_size)
        subgroup_number_all_progenitor = np.append(subgroup_number_all_progenitor, subgroup_number_all_progenitor[near_small_z]+box_size)
        descendant_index_all_progenitor = np.append(descendant_index_all_progenitor, descendant_index_all_progenitor[near_small_z]+box_size)
        node_index_all_progenitor = np.append(node_index_all_progenitor, node_index_all_progenitor[near_small_z]+box_size)

    # Select subhalo samples within each subvolume, compute required comoving subvolume boundaries and write to subvolume files
    xyz_subvol_part_min = np.zeros((nsubvol,3))
    xyz_subvol_part_max = np.zeros((nsubvol,3))

    node_index_list_ps = list(node_index_list)

    # Work out which subhaloes are in which subvolume on this snapshot
    if snap != snapshots.max():
        node_index_list = []

        for n in range(nsubvol):

            if len(descendantIndex_subhalo) > 0 and len(node_index_list_ps[n]) >0:
                ptr = ms.match(descendantIndex_subhalo, node_index_list_ps[n])
                ok = ptr >= 0

                node_index_list.append(nodeIndex_subhalo[ok])
            else:
                node_index_list.append([])

    for n in range(nsubvol):

        if test_mode:
            if n != ind_choose + nsub_1d**3:
                continue

        #if test_mode:
        #    if n < nsub_1d**3 and n != 6:
        #        continue

        if snap == snapshots.max():
            ptr = ms.match(nodeIndex_subhalo, node_index_list[n])
            #print "first round, 2nd layer node_index", node_index_list[n]
            ok = (ptr >= 0) & (subgroup_number_subhalo==0)
        else:
            if len(descendantIndex_subhalo) > 0 and len(node_index_list_ps[n]) > 0:
                ptr = ms.match(descendantIndex_subhalo, node_index_list_ps[n])
            else:
                continue
                
            ok = (ptr >= 0) & (subgroup_number_subhalo==0)

            print "min grn is", np.sort(group_number_subhalo[ok])[0]

            if n >= nsub_1d**3 and min_grn_prev *2 <  np.sort(group_number_subhalo[ok])[0]:
                print "problem, main halo has dissapeared, looking for it in satellites"
                ok = ok | ((subgroup_number_subhalo == 0) & (group_number_subhalo == np.sort(group_number_subhalo[ptr>=0])[0]))
                print "and now", group_number_subhalo[ok]
                print group_number_subhalo[ptr>=0]
                 
            min_grn_prev = np.sort(group_number_subhalo[ok])[0]
            
            # Debugging block
            candidate_problem_sats = (ptr>=0) & (subgroup_number_subhalo>0)
            if len(group_number_subhalo[candidate_problem_sats])>0 and len(group_number_subhalo[ok])>0:
                ptr_check = ms.match(group_number_subhalo[candidate_problem_sats], group_number_subhalo[ok])
                if len(nodeIndex_subhalo[candidate_problem_sats][ptr_check<0])>0:
                    ptr_check2 = ms.match(nodeIndex_subhalo, nodeIndex_subhalo[candidate_problem_sats][ptr_check<0])
                    problem_sats = ptr_check2 >= 0
            
                    if len(nodeIndex_subhalo[problem_sats])>0:
                        print ""
                        print "Warning, you have", len(nodeIndex_subhalo[problem_sats]), "satellite subhalos from this snapshot who have a descendant from the subvolume of the previous snapshot, but whose central is not in the subvolume for this snapshot"

                        ptr_check3 = ms.match(group_number_subhalo[problem_sats], group_number_subhalo[subgroup_number_subhalo==0])
                        print len(ptr_check3[ptr_check3>=0])/float(len(ptr_check3)), "of the problem satellites do actually have a central somewhere in the simulation"

                        if len(ptr_check3[ptr_check3>=0])/float(len(ptr_check3)) > 0:
                            ptr_check4 = ms.match( group_number_subhalo[subgroup_number_subhalo==0], group_number_subhalo[problem_sats])
                            print "Group numbers and mchalos of those centrals are ", np.unique(group_number_subhalo[subgroup_number_subhalo==0][ptr_check4>=0]), np.sort(mchalo_subhalo[subgroup_number_subhalo==0][ptr_check4>=0])
                            
                            if np.max(mchalo_subhalo[subgroup_number_subhalo==0][ptr_check4>=0]) > 1e11:
                                ptr_check5 = ms.match( nodeIndex_subhalo_ps, descendantIndex_subhalo[subgroup_number_subhalo==0][ptr_check4>=0])
                                print "Descendant group numbers and subgroup numbers are ", group_number_subhalo_ps[ptr_check5>=0], subgroup_number_subhalo_ps[ptr_check5>=0]
                                print ""
                                #quit()

        # Pick most massive one
        if n < nsub_1d**3:
            xmin,ymin,zmin = xyz_subvol[n]

            dx = 1/float(2*nsub_1d)
            xmid = box_size * (xmin + dx); ymid = box_size * (ymin + dx); zmid = box_size * (zmin + dx)

            if nsub_1d == 0:
                disp = box_size * 2
            else:
                disp = box_size * 0.5
            #else:
            #    disp = box_size * 1/3.0

        else:
            try:
                main = np.argmax(mchalo_subhalo[ok][subgroup_number_subhalo[ok]==0])
            except:
                continue
            xmid = halo_x_subhalo[ok][subgroup_number_subhalo[ok]==0][main]
            ymid = halo_y_subhalo[ok][subgroup_number_subhalo[ok]==0][main]
            zmid = halo_z_subhalo[ok][subgroup_number_subhalo[ok]==0][main]

            disp = box_size * 0.5

        # First rearrange central positions that cross the box boundaries
        halo_x_cen = halo_x_subhalo[ok][subgroup_number_subhalo[ok]==0]
        halo_y_cen = halo_y_subhalo[ok][subgroup_number_subhalo[ok]==0]
        halo_z_cen = halo_z_subhalo[ok][subgroup_number_subhalo[ok]==0]

        too_large_x = halo_x_cen - xmid > disp
        halo_x_cen[too_large_x] -= box_size
        too_small_x = xmid - halo_x_cen > disp
        halo_x_cen[too_small_x] += box_size
        
        too_large_y = halo_y_cen - ymid > disp
        halo_y_cen[too_large_y] -= box_size
        too_small_y = ymid - halo_y_cen > disp
        halo_y_cen[too_small_y] += box_size
        
        too_large_z = halo_z_cen - zmid > disp
        halo_z_cen[too_large_z] -= box_size
        too_small_z = zmid - halo_z_cen > disp
        halo_z_cen[too_small_z] += box_size

        temp = halo_x_subhalo[ok]; temp[subgroup_number_subhalo[ok]==0] = halo_x_cen; halo_x_subhalo[ok] = temp
        temp = halo_y_subhalo[ok]; temp[subgroup_number_subhalo[ok]==0] = halo_y_cen; halo_y_subhalo[ok] = temp
        temp = halo_z_subhalo[ok]; temp[subgroup_number_subhalo[ok]==0] = halo_z_cen; halo_z_subhalo[ok] = temp
 
        # Now rearrange satellites that cross the box boundary
        sat = subgroup_number_subhalo>0
        if len(group_number_subhalo[subgroup_number_subhalo>0])>0 and len(group_number_subhalo[ok])>0: 
            ptr = ms.match( group_number_subhalo[subgroup_number_subhalo>0], group_number_subhalo[ok])

            halo_x_host = halo_x_subhalo[ok][ptr][ptr>=0]
            halo_y_host = halo_y_subhalo[ok][ptr][ptr>=0]
            halo_z_host = halo_z_subhalo[ok][ptr][ptr>=0]       

            ok[subgroup_number_subhalo>0] = ptr >= 0

            '''if n == 36:
                print ""
                print "snap, snip", snap, snip
                print "Second ptr", np.sort(group_number_subhalo[ok])
                if snap == 70 or snap == 140:
                    quit()'''

            halo_x_sat = halo_x_subhalo[sat][ptr>=0]
            halo_y_sat = halo_y_subhalo[sat][ptr>=0]
            halo_z_sat = halo_z_subhalo[sat][ptr>=0]

            too_large_x = halo_x_sat - halo_x_host > 0.25 * box_size
            halo_x_sat[too_large_x] -= box_size
            too_small_x = halo_x_host - halo_x_sat > 0.25 * box_size
            halo_x_sat[too_small_x] += box_size
        
            too_large_y = halo_y_sat - halo_y_host > 0.25 * box_size
            halo_y_sat[too_large_y] -= box_size
            too_small_y = halo_y_host - halo_y_sat > 0.25 * box_size
            halo_y_sat[too_small_y] += box_size
        
            too_large_z = halo_z_sat - halo_z_host > 0.25 * box_size
            halo_z_sat[too_large_z] -= box_size
            too_small_z = halo_z_host - halo_z_sat > 0.25 * box_size
            halo_z_sat[too_small_z] += box_size

            temp = halo_x_subhalo[sat]; temp[ptr>=0] = halo_x_sat; halo_x_subhalo[sat] = temp
            temp = halo_y_subhalo[sat]; temp[ptr>=0] = halo_y_sat; halo_y_subhalo[sat] = temp
            temp = halo_z_subhalo[sat]; temp[ptr>=0] = halo_z_sat; halo_z_subhalo[sat] = temp
            

        if len(node_index_noprogen_list[n]) > 0:
            if snap != snapshots.max():
                ptr2 = ms.match(descendantIndex_noprogen, node_index_list_ps[n])
            else:
                ptr2 = ms.match(nodeIndex_noprogen, node_index_noprogen_list[n])

            ok2 = ptr2 >= 0

            # First rearrange central positions that cross the box boundaries 
            halo_x_cen = halo_x_noprogen[ok2][subgroup_number_noprogen[ok2]==0]
            halo_y_cen = halo_y_noprogen[ok2][subgroup_number_noprogen[ok2]==0]
            halo_z_cen = halo_z_noprogen[ok2][subgroup_number_noprogen[ok2]==0]

            too_large_x = halo_x_cen - xmid > disp
            halo_x_cen[too_large_x] -= box_size
            too_small_x = xmid - halo_x_cen > disp
            halo_x_cen[too_small_x] += box_size
        
            too_large_y = halo_y_cen - ymid > disp
            halo_y_cen[too_large_y] -= box_size
            too_small_y = ymid - halo_y_cen > disp
            halo_y_cen[too_small_y] += box_size
        
            too_large_z = halo_z_cen - zmid > disp
            halo_z_cen[too_large_z] -= box_size
            too_small_z = zmid - halo_z_cen > disp
            halo_z_cen[too_small_z] += box_size

            temp = halo_x_noprogen[ok2]; temp[subgroup_number_noprogen[ok2]==0] = halo_x_cen; halo_x_noprogen[ok2] = temp
            temp = halo_y_noprogen[ok2]; temp[subgroup_number_noprogen[ok2]==0] = halo_y_cen; halo_y_noprogen[ok2] = temp
            temp = halo_z_noprogen[ok2]; temp[subgroup_number_noprogen[ok2]==0] = halo_z_cen; halo_z_noprogen[ok2] = temp

            # Now rearrange satellite positions that cross the box boundaries
            sat = subgroup_number_noprogen>0
            if len(group_number_noprogen[sat])>0 and len(group_number_noprogen[ok2&(sat==False)])>0:
                ptr = ms.match(group_number_noprogen[sat], group_number_noprogen[ok2&(sat==False)])

                halo_x_host = halo_x_noprogen[ok2&(sat==False)][ptr][ptr>=0]
                halo_y_host = halo_y_noprogen[ok2&(sat==False)][ptr][ptr>=0]
                halo_z_host = halo_z_noprogen[ok2&(sat==False)][ptr][ptr>=0]
            
                ok2[subgroup_number_noprogen>0] = ptr >= 0

                halo_x_sat = halo_x_noprogen[sat][ptr>=0]
                halo_y_sat = halo_y_noprogen[sat][ptr>=0]
                halo_z_sat = halo_z_noprogen[sat][ptr>=0]

                too_large_x = halo_x_sat - halo_x_host > 0.25 * box_size
                halo_x_sat[too_large_x] -= box_size
                too_small_x = halo_x_host - halo_x_sat > 0.25 * box_size
                halo_x_sat[too_small_x] += box_size

                too_large_y = halo_y_sat - halo_y_host > 0.25 * box_size
                halo_y_sat[too_large_y] -= box_size
                too_small_y = halo_y_host - halo_y_sat > 0.25 * box_size
                halo_y_sat[too_small_y] += box_size

                too_large_z = halo_z_sat - halo_z_host > 0.25 * box_size
                halo_z_sat[too_large_z] -= box_size
                too_small_z = halo_z_host - halo_z_sat > 0.25 * box_size
                halo_z_sat[too_small_z] += box_size

                temp = halo_x_noprogen[sat]; temp[ptr>=0] = halo_x_sat; halo_x_noprogen[sat] = temp
                temp = halo_y_noprogen[sat]; temp[ptr>=0] = halo_y_sat; halo_y_noprogen[sat] = temp
                temp = halo_z_noprogen[sat]; temp[ptr>=0] = halo_z_sat; halo_z_noprogen[sat] = temp
            
        else:
            ok2 = np.zeros_like(halo_x_noprogen) > 0
    
        halo_xmax_subhalo = halo_x_subhalo + rvir_subhalo * r200_frac
        halo_xmin_subhalo = halo_x_subhalo - rvir_subhalo * r200_frac
        halo_ymax_subhalo = halo_y_subhalo + rvir_subhalo * r200_frac
        halo_ymin_subhalo = halo_y_subhalo - rvir_subhalo * r200_frac
        halo_zmax_subhalo = halo_z_subhalo + rvir_subhalo * r200_frac
        halo_zmin_subhalo = halo_z_subhalo - rvir_subhalo * r200_frac

        halo_xmax_noprogen = halo_x_noprogen + rvir_noprogen * r200_frac
        halo_xmin_noprogen = halo_x_noprogen - rvir_noprogen * r200_frac
        halo_ymax_noprogen = halo_y_noprogen + rvir_noprogen * r200_frac
        halo_ymin_noprogen = halo_y_noprogen - rvir_noprogen * r200_frac
        halo_zmax_noprogen = halo_z_noprogen + rvir_noprogen * r200_frac
        halo_zmin_noprogen = halo_z_noprogen - rvir_noprogen * r200_frac

        # For progenitor haloes, we need to reposition those that cross the simulation boundary w.r.t their descendants in order to compute the required particle data vertices
        too_large_x = halo_x_progenitor - halo_x_subhalo > 0.25 * box_size
        halo_x_progenitor[too_large_x] -= box_size
        too_small_x = halo_x_subhalo - halo_x_progenitor > 0.25 * box_size
        halo_x_progenitor[too_small_x] += box_size

        too_large_y = halo_y_progenitor - halo_y_subhalo > 0.25 * box_size
        halo_y_progenitor[too_large_y] -= box_size
        too_small_y = halo_y_subhalo - halo_y_progenitor > 0.25 * box_size
        halo_y_progenitor[too_small_y] += box_size

        too_large_z = halo_z_progenitor - halo_z_subhalo > 0.25 * box_size
        halo_z_progenitor[too_large_z] -= box_size
        too_small_z = halo_z_subhalo - halo_z_progenitor > 0.25 * box_size
        halo_z_progenitor[too_small_z] += box_size
        
        halo_xmax_progenitor = halo_x_progenitor + rvir_progenitor * r200_frac
        halo_xmin_progenitor = halo_x_progenitor - rvir_progenitor * r200_frac
        halo_ymax_progenitor = halo_y_progenitor + rvir_progenitor * r200_frac
        halo_ymin_progenitor = halo_y_progenitor - rvir_progenitor * r200_frac
        halo_zmax_progenitor = halo_z_progenitor + rvir_progenitor * r200_frac
        halo_zmin_progenitor = halo_z_progenitor - rvir_progenitor * r200_frac

        if len(descendant_index_all_progenitor)>0 and len(node_index_list[n])>0:
            ptr3 = ms.match(descendant_index_all_progenitor, node_index_list[n])
            ok3 = ptr3 >= 0
        else:
            ok3 = np.ones_like(descendant_index_all_progenitor)<0


        # Work out cube size to read particles for in this subvolume
        if len(halo_x_subhalo[ok]) > 0:
                
            if len(halo_xmax_noprogen[ok2]) == 0:
                halo_xmax_noprogen_ok2 = [0]
                halo_xmin_noprogen_ok2 = [1e9]
                halo_ymax_noprogen_ok2 = [0]
                halo_ymin_noprogen_ok2 = [1e9]
                halo_zmax_noprogen_ok2 = [0]
                halo_zmin_noprogen_ok2 = [1e9]
            else:
                halo_xmax_noprogen_ok2 = halo_xmax_noprogen[ok2]
                halo_xmin_noprogen_ok2 = halo_xmin_noprogen[ok2]
                halo_ymax_noprogen_ok2 = halo_ymax_noprogen[ok2]
                halo_ymin_noprogen_ok2 = halo_ymin_noprogen[ok2]
                halo_zmax_noprogen_ok2 = halo_zmax_noprogen[ok2]
                halo_zmin_noprogen_ok2 = halo_zmin_noprogen[ok2]

            # vertices of the cube for which we are going to read particle data for later
            xmax_part = max(np.max(halo_xmax_subhalo[ok]),np.max(halo_xmax_progenitor[ok]),np.max(halo_xmax_noprogen_ok2))
            ymax_part = max(np.max(halo_ymax_subhalo[ok]),np.max(halo_ymax_progenitor[ok]),np.max(halo_ymax_noprogen_ok2))
            zmax_part = max(np.max(halo_zmax_subhalo[ok]),np.max(halo_zmax_progenitor[ok]),np.max(halo_zmax_noprogen_ok2))
    
            xmin_part = min(np.min(halo_xmin_subhalo[ok]),np.min(halo_xmin_progenitor[ok]),np.min(halo_xmin_noprogen_ok2))
            ymin_part = min(np.min(halo_ymin_subhalo[ok]),np.min(halo_ymin_progenitor[ok]),np.min(halo_ymin_noprogen_ok2))
            zmin_part = min(np.min(halo_zmin_subhalo[ok]),np.min(halo_zmin_progenitor[ok]),np.min(halo_zmin_noprogen_ok2))

            # snapshot 35, 39, 42, no progenitor (again!)

            # Check selection boundary has a reasonable size
            if xmax_part - xmin_part > disp or ymax_part -ymin_part > disp or zmax_part -zmin_part > disp:
                print "Warning: selection boundary is too large, maybe to debug"

                print ""
                print xmin_part, xmax_part, ymin_part, ymax_part, zmin_part, zmax_part
                print "xmax", np.max(halo_xmax_subhalo[ok]), np.max(halo_xmax_progenitor[ok]), np.max(halo_xmax_noprogen_ok2)
                print "ymax", np.max(halo_ymax_subhalo[ok]), np.max(halo_ymax_progenitor[ok]), np.max(halo_ymax_noprogen_ok2)
                print "zmax", np.max(halo_zmax_subhalo[ok]), np.max(halo_zmax_progenitor[ok]), np.max(halo_zmax_noprogen_ok2)
                print ""

            
        else:
                
            xmax_part = np.nan
            ymax_part = np.nan
            zmax_part = np.nan
                
            xmin_part = np.nan
            ymin_part = np.nan
            zmin_part = np.nan

        # Write to disk
        filename = "subhalo_catalogue_subvol_"+sim_name+"_snip_"+str(snip)+"_"+str(n)+".hdf5"
        print filename
        print ""

        # Delete existing hdf5 file if one exists before writing a new one
        if os.path.isfile(output_path+filename):
            os.remove(output_path+filename)
            #print "deleted previous hdf5 file at ", output_path+filename

        File_subvol = h5py.File(output_path+filename)

        # Write tree snapshot info
        tree_snapshot_info = File_subvol.create_group("tree_snapshot_info")
        tree_snapshot_info.create_dataset("snapshots_tree", data= snapshot_numbers)
        tree_snapshot_info.create_dataset("redshifts_tree", data = z_snapshots)
        tree_snapshot_info.create_dataset("cosmic_times_tree", data = t_snapshots)
        tree_snapshot_info.create_dataset("this_tree_snapshot", data = snap)
        tree_snapshot_info.create_dataset("sn_i_a_pshots_simulation", data = sn_i_a_pshots_simulation)

        select_group = File_subvol.create_group("select_region")
        select_group.create_dataset("xyz_min",data=np.array([xmin_part,ymin_part,zmin_part]))
        select_group.create_dataset("xyz_max",data=np.array([xmax_part,ymax_part,zmax_part]))

        halo_data_group = File_subvol.create_group("subhalo_data")
        halo_data_group.create_dataset("group_number",data=group_number_subhalo[ok])
        halo_data_group.create_dataset("subgroup_number",data=subgroup_number_subhalo[ok])
        halo_data_group.create_dataset("rc200",data=rvir_subhalo[ok])
        halo_data_group.create_dataset("x_halo",data=halo_x_subhalo[ok])
        halo_data_group.create_dataset("y_halo",data=halo_y_subhalo[ok])
        halo_data_group.create_dataset("z_halo",data=halo_z_subhalo[ok])
        halo_data_group.create_dataset("vx_halo",data=halo_vx_subhalo[ok])
        halo_data_group.create_dataset("vy_halo",data=halo_vy_subhalo[ok])
        halo_data_group.create_dataset("vz_halo",data=halo_vz_subhalo[ok])
        halo_data_group.create_dataset("vmax",data=vmax_subhalo[ok])
        halo_data_group.create_dataset("node_index",data=nodeIndex_subhalo[ok])
        halo_data_group.create_dataset("descendant_index",data=descendantIndex_subhalo[ok])
        halo_data_group.create_dataset("mchalo",data=mchalo_subhalo[ok])
        halo_data_group.create_dataset("mhhalo",data=mhhalo_subhalo[ok])
        halo_data_group.create_dataset("r200_host",data=halo_r200_host_subhalo[ok])

        no_progen_data_group = File_subvol.create_group("subhalo_noprogen")
        no_progen_data_group.create_dataset("group_number",data=group_number_noprogen[ok2])
        no_progen_data_group.create_dataset("subgroup_number",data=subgroup_number_noprogen[ok2])
        no_progen_data_group.create_dataset("rc200",data=rvir_noprogen[ok2])
        no_progen_data_group.create_dataset("x_halo",data=halo_x_noprogen[ok2])
        no_progen_data_group.create_dataset("y_halo",data=halo_y_noprogen[ok2])
        no_progen_data_group.create_dataset("z_halo",data=halo_z_noprogen[ok2])
        no_progen_data_group.create_dataset("node_index",data=nodeIndex_noprogen[ok2])
        no_progen_data_group.create_dataset("descendant_index",data=descendantIndex_noprogen[ok2])


        progenitor_data_group = File_subvol.create_group("progenitor_data")
        progenitor_data_group.create_dataset("group_number",data=group_number_progenitor[ok])
        progenitor_data_group.create_dataset("subgroup_number",data=subgroup_number_progenitor[ok])
        progenitor_data_group.create_dataset("rc200",data=rvir_progenitor[ok])
        progenitor_data_group.create_dataset("x_halo",data=halo_x_progenitor[ok])
        progenitor_data_group.create_dataset("y_halo",data=halo_y_progenitor[ok])
        progenitor_data_group.create_dataset("z_halo",data=halo_z_progenitor[ok])
        progenitor_data_group.create_dataset("vx_halo",data=halo_vx_progenitor[ok])
        progenitor_data_group.create_dataset("vy_halo",data=halo_vy_progenitor[ok])
        progenitor_data_group.create_dataset("vz_halo",data=halo_vz_progenitor[ok])
        progenitor_data_group.create_dataset("vmax",data=vmax_progenitor[ok])
        progenitor_data_group.create_dataset("node_index",data=node_index_progenitor[ok])
        progenitor_data_group.create_dataset("r200_host",data=halo_r200_host_progenitor[ok])

        all_progenitor_group = File_subvol.create_group("all_progenitor_data")
        all_progenitor_group.create_dataset("group_number",data=group_number_all_progenitor[ok3])
        all_progenitor_group.create_dataset("subgroup_number",data=subgroup_number_all_progenitor[ok3])
        all_progenitor_group.create_dataset("descendant_index",data=descendant_index_all_progenitor[ok3])
        all_progenitor_group.create_dataset("x_halo",data=halo_x_all_progenitor[ok3])
        all_progenitor_group.create_dataset("y_halo",data=halo_y_all_progenitor[ok3])
        all_progenitor_group.create_dataset("z_halo",data=halo_z_all_progenitor[ok3])
        all_progenitor_group.create_dataset("r200_host",data=halo_r200_host_all_progenitor[ok3])

        File_subvol.close()

        if snip == 8:
            print "got to a"

    # Note this is only for debugging
    nodeIndex_subhalo_ps = np.copy(nodeIndex_subhalo)
    descendantIndex_subhalo_ps = np.copy(descendantIndex_subhalo)
    group_number_subhalo_ps = np.copy(group_number_subhalo)
    subgroup_number_subhalo_ps = np.copy(subgroup_number_subhalo)
    halo_x_subhalo_ps = np.copy(halo_x_subhalo)
    halo_y_subhalo_ps = np.copy(halo_y_subhalo)
    halo_z_subhalo_ps = np.copy(halo_z_subhalo)

    # Also delete any unwanted subvolume files with higher indicies
    finished = False
    while not finished:
        n += 1
        filename = "subhalo_catalogue_subvol_"+sim_name+"_snip_"+str(snip)+"_"+str(n)+".hdf5"
        if os.path.isfile(output_path+filename):
            os.remove(output_path+filename)
        else:
            finished = True

File.close()
