import numpy as np
import h5py
import os
import warnings
import match_searchsorted as ms
import utilities_cosmology as uc
from utilities_plotting import *

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
h=0.6777
box_size = 25 * h
snap_final = 50
snapshots = np.arange(0,snap_final+1)[::-1]
snipshots = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1
tree_file = "/gpfs/data/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"'''

# 25 Mpc REF with 300 snas
sim_name = "L0025N0376_REF_300_snap"
h=0.6777
box_size = 25 * h
snap_final = 300
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snipshots = np.rint(snipshots[::-1]).astype("int")
snapshots = np.rint(snapshots[::-1]).astype("int")
# Choose number of subvolumes (in 1 dimension such that n_subvolumes = nsub_1d**3)
print "running for 25mpc box"
nsub_1d = 1
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/tree_299.0.hdf5"

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()
t, tlb = uc.t_Universe(a, omm, h)
dt = t[:-1] - t[1:]


# Mass scale for which to separate massive haloes (and their subhaloes) from the overthise equal volume subvolumes at z=0
mhhalo_sep = 10**14

# Choose which catalogue file to read
input_path =       "/gpfs/data/d72fqv/Baryon_Cycle/"
output_path_base = "/gpfs/data/d72fqv/Baryon_Cycle/"+sim_name+"_subvols/"

test_mode = True # Write a single subvolume with a non-default nsub_1d
if test_mode:
    print "running in test mode"
    nsub_1d = 1
    
    # Test halo 1
    mchalo_choose = 10**12 # used to only select a single halo for a separate subvolume
    mhhalo_sep = 10**11.9
    
    # Test halo 2
    #mchalo_choose = 10**11 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**10.9

    # Test halo 3
    #mchalo_choose = 5*10**10 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 4.9*10**10
    
    # Test halo 4
    #mchalo_choose = 10**11.9 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**11.8
    
    # Test halo 5
    #mchalo_choose = 6.9*10**10 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 5*10**10
    
    # Test halo 6
    #mchalo_choose = 0.8*10**12 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10*10**11.5
    
    # Test halo 7
    #mchalo_choose = 10**13.5 # used to only select a single halo for a separate subvolume
    #mhhalo_sep = 10**13

    #nsub_1d = 4
    output_path_base = "/gpfs/data/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_test_mode/"

# Choose r200 fractions to compute subvol sizes
r200_frac = 1.5



# Read catalogue data to select a galaxy/halo
filename = "subhalo_catalogue_" + sim_name + ".hdf5"
File = h5py.File(input_path+filename)
warnings.filterwarnings('ignore')


############### Select subvolumes for z0 subhaloes #####################################

# First, read the z0 snapshot information
snapnum_group = File["Snap_"+str(snapshots.max())]
subhalo_group = snapnum_group["subhalo_properties"]
node_index = subhalo_group["nodeIndex"][:]
subgroup_number = subhalo_group["SubGroupNumber_hydro"][:]
group_number = subhalo_group["GroupNumber_hydro"][:]
halo_x_subhalo = subhalo_group["CentreOfPotential_x"][:] # Mpc h^-1
halo_y_subhalo = subhalo_group["CentreOfPotential_y"][:]
halo_z_subhalo = subhalo_group["CentreOfPotential_z"][:]

g2Msun = 1./(1.989e33)
try:
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h
    mhhalo_subhalo = subhalo_group["mhhalo"][:] * g2Msun * 1.989e43 /h # Msun
except:
    print "Error: need to run copy_mchalo.py first"
    quit()


# Separate out haloes (and their subhaloes) which are above a given mass point at z=0 (these will get their own separate subvolumes)
sep = mhhalo_subhalo > mhhalo_sep

# For the remaining haloes, subdivide them into equal volume subvolumes at z=0 (otherwise you need a space-filling curve in python)

# Compute subvolume vertices
xyz_subvol = np.zeros((nsub_1d**3,3))
ind_sub = 0
for i in range(nsub_1d):
    x_sub = i / float(nsub_1d)
    for j in range(nsub_1d):
        y_sub = j / float(nsub_1d)
        for k in range(nsub_1d):
            z_sub = k / float(nsub_1d)
            xyz_subvol[ind_sub] = np.array([x_sub, y_sub, z_sub])
            ind_sub += 1
            
# Build node index list
node_index_list = []
for n in range(nsub_1d**3):

    xmin,ymin,zmin = xyz_subvol[n]

    dx = 1/float(nsub_1d)
    xmax = xmin + dx; ymax = ymin + dx; zmax = zmin + dx

    ok = (halo_x_subhalo > xmin*box_size) & (halo_x_subhalo < xmax*box_size) & (halo_y_subhalo > ymin*box_size) & (halo_y_subhalo < ymax*box_size) & (halo_z_subhalo > zmin*box_size) & (halo_z_subhalo < zmax*box_size) & (sep == False) & (subgroup_number==0)

    ptr = ms.match( group_number[subgroup_number>0], group_number[ok])
    ok[subgroup_number>0] = ptr >= 0

    node_index_list.append(node_index[ok])

sep_cen = sep & (subgroup_number == 0)
nsep = len(subgroup_number[sep_cen])
for n in range(nsep):
    ok = group_number[sep] == group_number[sep_cen][n]
    node_index_list.append(node_index[sep][ok])

if test_mode:
    ind_choose = np.argmin(abs(mchalo_subhalo[sep_cen]-mchalo_choose))
    
# Total number of subvolumes
nsubvol = nsub_1d**3 + nsep

################# Now loop through snipshots, read in the full catalogue at each point and then loop through the subvolumes

def z2snip(z):
    snip = snipnum_list[ np.argmin(abs(ref_z-z)) ]
    return snip

sfr = np.zeros(len(snapshots))
bfr = np.zeros(len(snapshots))
mdot_reheated = np.zeros_like(sfr)
mdot_ejected = np.zeros_like(sfr)
i_snap = 0

t_snip = []


for snap, snip in zip(snapshots,snipshots):

    print "looking at snap", snap
   
    # No progenitors for first snapshot
    if snap == snapshots.min():
        continue

    # Read the data
    snapnum_group = File["Snap_"+str(snap)]
       
    subhalo_group = snapnum_group["subhalo_properties"]
    progenitor_group = snapnum_group["main_progenitors"]

    group_number_subhalo = subhalo_group["GroupNumber_hydro"][:]
    subgroup_number_subhalo = subhalo_group["SubGroupNumber_hydro"][:]

    mass_new_stars = subhalo_group["mass_new_stars_init"][:]
    sfr_subhalo = mass_new_stars * 1e-9/dt[i_snap]

    mass_acc_bh = subhalo_group["mass_acc_bh"][:]
    bfr_subhalo = mass_acc_bh * 1e-9/dt[i_snap]

    mdot_ejected_subhalo = subhalo_group["mass_ejected"][:] * 1e-9/dt[i_snap]
    mdot_reheated_subhalo = subhalo_group["mass_reheated"][:]* 1e-9/dt[i_snap]

    try:
        nodeIndex_subhalo = subhalo_group["nodeIndex"][:]
    except:
        continue

    descendantIndex_subhalo = subhalo_group["descendantIndex"][:]

    g2Msun = 1./(1.989e33)
    mchalo_subhalo = subhalo_group["mchalo"][:] * g2Msun * 1.989e43 /h # Msun
    mhhalo_subhalo = subhalo_group["mhhalo"][:] * g2Msun * 1.989e43 /h # Msun

    group_number_progenitor = progenitor_group["GroupNumber_hydro"][:]
    subgroup_number_progenitor = progenitor_group["SubGroupNumber_hydro"][:]

    node_index_list_ps = list(node_index_list)

    # Work out which subhaloes are in which subvolume on this snapshot
    if snap != snapshots.max():
        node_index_list = []

        for n in range(nsubvol):

            try:
                ptr = ms.match(descendantIndex_subhalo, node_index_list_ps[n])
                ok = ptr >= 0
                node_index_list.append(nodeIndex_subhalo[ok])
            except:
                continue

    for n in range(nsubvol):
        if test_mode:
            if n != ind_choose + nsub_1d**3:
                continue

        if snap == snapshots.max():
            ptr = ms.match(nodeIndex_subhalo, node_index_list[n])
            ok = (ptr >= 0) & (subgroup_number_subhalo==0)

            sfr[i_snap] = sfr_subhalo[ok]
            bfr[i_snap] = bfr_subhalo[ok]
            mdot_reheated[i_snap] = mdot_reheated_subhalo[ok]
            mdot_ejected[i_snap] = mdot_ejected_subhalo[ok]
     
        else:
            try:
                ptr = ms.match(descendantIndex_subhalo, node_index_list_ps[n])
                ok = (ptr >= 0) & (subgroup_number_subhalo==0)
                
                # Pick most massive halo as main progenitor
                mchalo_progens = mchalo_subhalo[ok]
                ind_main = np.argmax(mchalo_progens)

                if snip == 337:
                    print ""
                    print "here"
                print np.log10(mchalo_progens[ind_main]), sfr_subhalo[ok][ind_main], group_number_subhalo[ok][ind_main], subgroup_number_subhalo[ok][ind_main]
                
                sfr[i_snap] = sfr_subhalo[ok][ind_main]
                bfr[i_snap] = bfr_subhalo[ok][ind_main]
                mdot_reheated[i_snap] = mdot_reheated_subhalo[ok][ind_main]
                mdot_ejected[i_snap] = mdot_ejected_subhalo[ok][ind_main]
            except:
                continue

    #if snip == 281:
    #    t_snip = t[i_snap]

    if mchalo_choose == 10**11 and snip == 225:
        t_snip.append(t[i_snap])
    if mchalo_choose == 10**11 and snip == 73:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 10**12 and snip == 281:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 10**12 and snip == 97:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 5*10**10 and snip == 89:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 10**11.9 and snip == 329:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 6.9*10**10 and snip == 281:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 0.8*10**12 and snip == 129:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 0.8*10**12 and snip == 337:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 0.8*10**12 and snip == 185:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 0.8*10**12 and snip == 193:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 10**13.5 and snip == 313:
        t_snip.append(t[i_snap])
    elif mchalo_choose == 10**13.5 and snip == 121:
        t_snip.append(t[i_snap])


    i_snap += 1
File.close()
try:
    for t_i in t_snip:
        py.axvline(t_i)
except:
    print "no t_snip"
py.plot(t, sfr,c="b",label="SFR")
py.plot(t, 4e3*bfr,c="c",label="BFR x4000")
py.plot(t, mdot_reheated,c="r",label="SF ISM outflow")
py.plot(t, mdot_ejected,c="k",label="Halo outflow")

py.scatter(t, sfr,c="b")
py.scatter(t, 4e3*bfr,c="c")
py.scatter(t, mdot_reheated,c="r")
py.scatter(t, mdot_ejected,c="k")

py.legend(loc="upper right")
py.show()
