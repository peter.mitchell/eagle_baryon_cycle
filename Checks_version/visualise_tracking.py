import numpy as np
visualize_winds =True # If true look at ISM ejecta, if false visualize halo ejecta

central_r200 = True # If true, rebind particles to follow r200 strictly - recommended set to True

ns_tdyn = True # Set to true to use a fixed fraction of the halo dynamical time to use for _ns
# tdyn_frac_ns is a parameter that controls the fraction of a halo dynamical time used for finding _ns for wind slection
tdyn_frac_ns = 0.25

catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"

# Reference z=0 1e12 halo (50 snip)
#subvol = 36
#snap_list = [35,36,37,38,39,40,41]
#snip_list = [281, 289, 297, 305, 313, 321,329]
#snip_first_ps = 273
#snip_ts_start = 281
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

# Reference z=0 1e12 halo (100 snip)
#subvol = 36
#snap_list = [69,70,71,72,73,74,75,76,77,78,79,80]
#snip_list = [277, 281, 285, 289, 293, 297, 301, 305, 309, 313, 317, 321]
#snip_first_ps = 273
#snip_ts_start = 277
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_100_snip"

# Reference z=0 1e12 halo (200 snip)
#subvol = 36
#snip_list = [275, 277, 279, 281, 283, 285, 287, 289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321]
#snap_list = np.arange(138,len(snip_list)+138)
#snip_first_ps = 273
#snip_ts_start = 275
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_200_snip"

# Reference(b) z=0 1e12 halo (300 snap)
#subvol = 3
#snap_choose = 207
#snap_width = 4
#snap_width = 35
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_300_snap"

# Reference(b) z=0 1e12 halo (500 snap)
#subvol = 13
#snap_choose = 207*2
##snap_width = 4
#snap_width = 35*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

#########################################################################

# z=0 1e11 halo
#subvol = 124
#snap_list = [28,29,30,31,32,33]
#snip_list = [225, 233, 241, 249, 257,265]
#snip_first_ps = 217
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

# Reference(b) z=0 1e11 halo (500 snap)
#subvol = 90
#snap_choose = 280
#snap_width = 35*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

#################################################################################

# z=0 5e10 halo
#subvol = 377
#snap_list = [11,12,13,14,15,16]
#snip_list = [89, 97, 105, 113, 121, 129]
#snip_first_ps =81
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

###### this one didn't work - there is a mixup somewhere in selecting which halo to track parts in
# z=0 5e10 halo
#subvol = 390
#snap_choose = 230
#snap_width = 35*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##############################################################################

# z=0 1e11.9 halo
#subvol = 38
#snap_list = [41, 42, 43, 44, 45, 46]
#snip_list = [329, 337, 345, 353, 361, 369]
#snip_first_ps =321
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

# z=0 1e11.9 500 snap version (note the tree for this one is not very stable)
#subvol = 54
#snap_choose = 410
#snap_width = 35*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"


########################################################################


# z=0 6.9e10 halo
#subvol = 273
#snap_list = [35,36,37,38,39,40]
#snip_list = [281, 289, 297, 305, 313, 321]
#snip_first_ps = 273
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 392
#snap_choose = 350
#snap_width = 35*2
##snap_width = 6
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##############################################################

# z=0 5e12 halo, high-z
#subvol = 4
#snap_list = [16, 17, 18, 19, 20, 21]
#snip_list = [129, 137, 145, 153, 161, 169]
#snip_first_ps =121
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 6
#snap_choose = 170
#snap_width = 35*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

#############################################################

# z=0 5e12 halo, low-z
#subvol = 4
#snap_list = [42, 43, 44, 45, 46, 47]
#snip_list = [337, 345, 353, 361, 369, 377]
#snip_first_ps =329
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 6
#snap_choose = 420
#snap_width = 25*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##################################################

# z=0 13.2, high-z
#subvol = 1
#snap_list = [15, 16, 17, 18, 19, 20]
#snip_list = [121, 129, 137, 145, 153, 161]
#snip_first_ps =113
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 1
#snap_choose = 150
#snap_width = 25*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"


#############################################################

# z=0 13.2, low-z, low-z
#subvol = 1
#snap_list = [39, 40, 41, 42, 43, 44]
#snip_list = [313, 321, 329, 337, 345, 353]
#snip_first_ps = 305
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 1
#snap_choose = 390
#snap_width = 25*2
#snap_list = np.arange(snap_choose-snap_width,snap_choose+snap_width)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"


############################################################

## Reference z=0 1e12 halo (50 snip) - high redshift
#subvol = 36
#snap_list = [12, 13, 14, 15, 16, 17]
#snip_list = [97, 105, 113, 121, 129, 137]
#snip_first_ps =89
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

# z=0 1e11 halo (high-z)
#subvol = 124
#snap_list = [9, 10, 11, 12, 13, 14]
#snip_list = [73, 81,  89,  97, 105, 113]
#snip_first_ps =65
#snip_ts_start = snip_list[0]
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

##########################################################
# Test halo 8 (selected 1e11 at z=2)
subvol = 131
snap_choose = 215
snap_list = np.arange(snap_choose-25*2,snap_choose+25*2)
DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
sim_name = "L0025N0376_REF_500_snap"

output_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"+sim_name+"_subvols_test_mode/"

import numpy as np
np.seterr(all='ignore')
import os
import h5py
import match_searchsorted as ms
import glob
import read_eagle as re
from utilities_plotting import *
import utilities_cosmology as uc
import utilities_statistics as us
import plotting_functions_visualize as up
import sys
import visualise_tracking_functions as vpf
import sys
sys.path.append('../')
import measure_particles_functions as mpf
#warnings.filterwarnings('ignore')






# First let's work out the relationship between tree snapshots and simulation snapshots/snipshots                        
print "reading", catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5"    
cat_file = h5py.File(catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5")
tree_snapshot_info = cat_file["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
cosmic_time = tree_snapshot_info["cosmic_times_tree"][:]
cat_file.close()

snip_list = np.zeros_like(snap_list)
cosmic_t_list = np.zeros(len(snap_list))
for n in range(len(snip_list)):
    snip_list[n] = sn_i_a_pshots_simulation[np.argmin(abs(snapshot_numbers-snap_list[n]))]
    cosmic_t_list[n] = cosmic_time[np.argmin(abs(snapshot_numbers-snap_list[n]))]

# Let's read the list of particles to be tracked
id_track_file = output_path + "id_track_check_version_" + sim_name + "_snip_"+str(snap_choose)+"_"+str(subvol)+".hdf5"
if not os.path.isfile(id_track_file):
    print "couldn't find id track file at ", id_track_file
    quit()

File_track = h5py.File(id_track_file)
id_track_full = File_track["id_track"][:]
File_track.close()

#print "temp hack"
#id_track_full = np.array([5540512963913])

radius_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
radius_phys_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
temp_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
cv_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
v_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
nh_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
wind_state_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
ejecta_state_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
ism_reheated_state_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
halo_reheated_state_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
ism_flag_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
sf_flag_track_full = np.zeros((len(snap_list), len(id_track_full))) + np.nan
in_sat_flag_track_full = np.zeros((len(snap_list),len(id_track_full)))+np.nan
vmax_track_full = np.zeros(len(snap_list))
group_number_track_full = np.zeros((len(snap_list),len(id_track_full)))+np.nan
use_ns_track_full = np.zeros(len(snap_list))

# First identify the tree branch we want to be on

snip_choose = snip_list[np.argmin(abs(snap_list-snap_choose))]
for snip_ts in snip_list[1:-1][::-1]:
    if snip_ts > snip_choose:
        continue
    
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"
    File = h5py.File(output_path_ts+filename_ts)

    subhalo_group = File["subhalo_data"]
    mchalo = subhalo_group["mchalo"][:]
    order = np.argsort(mchalo)[::-1]
    halo_x = subhalo_group["x_halo"][:][order]
    subgroup_number_list = subhalo_group["subgroup_number"][:][order]
    group_number_list = subhalo_group["group_number"][:][order]
    
    node_index_list = subhalo_group["node_index"][:][order]
    progenitor_group = File["progenitor_data"]
    node_index_list_progen = progenitor_group["node_index"][:][order]

    if snip_ts == snip_choose:
        ih_choose = np.where(subgroup_number_list==0)[0][0]
    else:
        ih_choose = np.argsort(abs(node_index_choose-node_index_list))[0]

    node_index_progen_choose = node_index_list_progen[ih_choose]
    node_index_choose = node_index_progen_choose

init_tracking = True   
for i_snip_temp, snip_ts in enumerate(snip_list[1:-1]):
    i_snip = i_snip_temp +1

    snip_ps = snip_list[i_snip-1]
    
    # Read subvol file to load halo catalogue
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    File = h5py.File(output_path_ts+filename_ts)

    ######## Work out which snapshot to read as the _ns for wind selection ###############
    tree_snapshot_info = File["tree_snapshot_info"]
    snapshot_numbers = tree_snapshot_info["snapshots_tree"][:][::-1]
    t_snapshots = tree_snapshot_info["cosmic_times_tree"][:][::-1]
    snap_ts = tree_snapshot_info["this_tree_snapshot"][()]
    tdyn_snapshots = t_snapshots*0.1 # halo dynamical time in Gyr - approxiated here as 10% age of the Universe
    print ""
    print "measuring for snip/snap", snip_ts,snap_ts,"which is",i_snip,"of",len(snip_list)-1

    ok = np.where(snapshot_numbers == snap_ts)[0][0]
    tdyn_ts = tdyn_snapshots[ok]

    t_remain = t_snapshots[ok:]
    dt = t_remain[1:] - t_remain[0]

    # tdyn_frac_ns is a parameter set at the top
    d_snap_ns = np.argmin( abs(dt - tdyn_ts * tdyn_frac_ns)) +1 # Number of snapshots into the future that should be read for _ns

    # Special for visuale_tracking version
    d_snap_ns = min(d_snap_ns, len(snip_list)-i_snip-1)

    if not ns_tdyn:
        d_snap_ns = 1

    if d_snap_ns > 1:
        print "_ns skip is", d_snap_ns
 
    snip_ns = snip_list[i_snip + d_snap_ns]
    snap_ns = snap_ts + d_snap_ns


    # Read halo catalogue
    subhalo_group = File["subhalo_data"]

    try:
        mchalo_list = subhalo_group["mchalo"][:]
    except:
        print "Error: first run copy_mchalo.py, then rerun select_subvolumes.py"
        quit()

    order = np.argsort(mchalo_list)[::-1]

    mchalo_list = mchalo_list[order]
    mhhalo_list = subhalo_group["mhhalo"][:][order]
    subgroup_number_list = subhalo_group["subgroup_number"][:][order]
    group_number_list = subhalo_group["group_number"][:][order]
    node_index_list = subhalo_group["node_index"][:][order]
    descendant_index_list = subhalo_group["descendant_index"][:][order]

    halo_x_list = subhalo_group["x_halo"][:][order] # cMpc h^-1
    halo_y_list = subhalo_group["y_halo"][:][order]
    halo_z_list = subhalo_group["z_halo"][:][order]
    halo_vx_list = subhalo_group["vx_halo"][:][order] # kms^-1
    halo_vy_list = subhalo_group["vy_halo"][:][order]
    halo_vz_list = subhalo_group["vz_halo"][:][order]
    halo_vmax_list = subhalo_group["vmax"][:][order]
    halo_r200_host_list = subhalo_group["r200_host"][:][order] # cMpc h^-1

    progenitor_group = File["progenitor_data"]

    group_number_list_progen = progenitor_group["group_number"][:][order]
    subgroup_number_list_progen = progenitor_group["subgroup_number"][:][order]

    halo_x_list_progen = progenitor_group["x_halo"][:][order] # Mpc h^-1
    halo_y_list_progen = progenitor_group["y_halo"][:][order]
    halo_z_list_progen = progenitor_group["z_halo"][:][order]
    halo_vx_list_progen = progenitor_group["vx_halo"][:][order] # kms^-1
    halo_vy_list_progen = progenitor_group["vy_halo"][:][order]
    halo_vz_list_progen = progenitor_group["vz_halo"][:][order]
    halo_vmax_list_progen = progenitor_group["vmax"][:][order]
    halo_r200_host_list_progen = progenitor_group["r200_host"][:][order]

    all_progenitor_group = File["all_progenitor_data"]
    group_number_list_all_progen = all_progenitor_group["group_number"][:]
    subgroup_number_list_all_progen = all_progenitor_group["subgroup_number"][:]
    descendant_index_list_all_progen = all_progenitor_group["descendant_index"][:]
    halo_r200_host_list_all_progen = all_progenitor_group["r200_host"][:]
    halo_x_list_all_progen = all_progenitor_group["x_halo"][:] # Mpc h^-1
    halo_y_list_all_progen = all_progenitor_group["y_halo"][:]
    halo_z_list_all_progen = all_progenitor_group["z_halo"][:]

    # Read halo properties for _ns output (in order to identify the positions of the descendants of haloes from this output) - if _ns is not the next output in the tree we need to walk down the tree to find the correct descendant
    n_tree_walk = snap_ns - snap_ts
    for i_walk in range(n_tree_walk):

        snip_i_walk = snip_list[i_snip+i_walk+1]

        filename_i_walk = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_i_walk)+"_"+str(subvol)+".hdf5"
        output_path_i_walk = output_path + "Snip_"+str(snip_i_walk)+"/"
        File_ns = h5py.File(output_path_i_walk+filename_i_walk)
        subhalo_group_ns = File_ns["subhalo_data"]
        node_index_list_ns = subhalo_group_ns["node_index"][:]

        if i_walk == 0:
            ptr = ms.match(descendant_index_list, node_index_list_ns)
        else:
            ptr = ms.match(descendant_index_list_ns, node_index_list_ns) # Note descendant left over from prev iteration through the loop
        ok_match = ptr>=0
            
        descendant_index_list_ns = np.zeros_like(group_number_list)-1e7 # Note it's important this is an ints array - not float
        descendant_index_list_ns[ok_match] = subhalo_group_ns["descendant_index"][:][ptr][ok_match]

        if i_walk != n_tree_walk-1:
            File_ns.close()

    # This stuff can all sit outside of the above loop
    group_number_list_ns = np.zeros_like(halo_x_list)-1e7
    subgroup_number_list_ns = np.zeros_like(halo_x_list)-1e7
    halo_x_list_ns = np.zeros_like(halo_x_list) + np.nan
    halo_y_list_ns = np.zeros_like(halo_x_list) + np.nan
    halo_z_list_ns = np.zeros_like(halo_x_list) + np.nan
    halo_r200_host_list_ns = np.zeros_like(halo_x_list) + np.nan
    
    group_number_list_ns[ok_match] = subhalo_group_ns["group_number"][:][ptr][ok_match]
    subgroup_number_list_ns[ok_match] = subhalo_group_ns["subgroup_number"][:][ptr][ok_match]
    halo_x_list_ns[ok_match] = subhalo_group_ns["x_halo"][:][ptr][ok_match] # Mpc h^-1
    halo_y_list_ns[ok_match] = subhalo_group_ns["y_halo"][:][ptr][ok_match]
    halo_z_list_ns[ok_match] = subhalo_group_ns["z_halo"][:][ptr][ok_match]
    halo_r200_host_list_ns[ok_match] = subhalo_group_ns["r200_host"][:][ptr][ok_match]
    File_ns.close()

    # Initiate wind particle lists
    if i_snip == 1:
        id_gas_ejecta_list_ps = []
        id_gas_wind_list_ps = []
        descendant_index_list_ps = node_index_list
        id_gas_ism_reheat_list_ps = []
        id_gas_halo_reheat_list_ps = []

        for i_halo in range(len(descendant_index_list_ps)):
            id_gas_ejecta_list_ps.append([])
            id_gas_wind_list_ps.append([])
            id_gas_ism_reheat_list_ps.append([])
            id_gas_halo_reheat_list_ps.append([])
    
    ############## Read Eagle particle data #######################

    gas_data_ps, dm_data_ps, star_data_ps, bh_data_ps, sim_props_ps = vpf.Read_Pre_Dumped_Part_Data(snip_ps,output_path,sim_name,subvol)
    gas_data_ts, dm_data_ts, star_data_ts, bh_data_ts, sim_props_ts = vpf.Read_Pre_Dumped_Part_Data(snip_ts,output_path,sim_name,subvol)
    gas_data_ns, dm_data_ns, star_data_ns, bh_data_ns, sim_props_ns = vpf.Read_Pre_Dumped_Part_Data(snip_ns,output_path,sim_name,subvol)

    boxsize = sim_props_ts[3]

    # Unpack data
    group_number_gas_list_all_ts = gas_data_ts[0]
    subgroup_number_gas_list_all_ts = gas_data_ts[1]
    mass_gas_list_all_ts = gas_data_ts[2]
    density_gas_list_all_ts = gas_data_ts[3]
    metallicity_gas_list_all_ts = gas_data_ts[4]
    id_gas_list_all_ts = gas_data_ts[5]
    coordinates_gas_list_all_ts = gas_data_ts[6]
    velocity_gas_list_all_ts = gas_data_ts[7]
    temperature_gas_list_all_ts = gas_data_ts[8]
    internal_energy_gas_list_all_ts = gas_data_ts[9]

    # Switch central subhalo definition to r200
    if central_r200:
        halo_coordinates_ts = [halo_x_list,halo_y_list,halo_z_list]
        group_number_gas_list_all_ts, subgroup_number_gas_list_all_ts = vpf.Rebind_Subhalos(group_number_gas_list_all_ts, subgroup_number_gas_list_all_ts, coordinates_gas_list_all_ts, group_number_list, subgroup_number_list, halo_coordinates_ts,halo_r200_host_list, boxsize, verbose=False)

    # Identify gas particles that are bound to subhaloes
    bound_parts = (subgroup_number_gas_list_all_ts >= 0) & (subgroup_number_gas_list_all_ts < 1e8) & (group_number_gas_list_all_ts >= 0)
    group_number_gas_list_ts = group_number_gas_list_all_ts[bound_parts] 
    subgroup_number_gas_list_ts = subgroup_number_gas_list_all_ts[bound_parts] 
    mass_gas_list_ts = mass_gas_list_all_ts[bound_parts] 
    density_gas_list_ts = density_gas_list_all_ts[bound_parts]
    metallicity_gas_list_ts = metallicity_gas_list_all_ts[bound_parts]
    id_gas_list_ts = id_gas_list_all_ts[bound_parts]
    coordinates_gas_list_ts = coordinates_gas_list_all_ts[bound_parts]
    velocity_gas_list_ts = velocity_gas_list_all_ts[bound_parts]
    temperature_gas_list_ts = temperature_gas_list_all_ts[bound_parts]
    internal_energy_gas_list_ts = internal_energy_gas_list_all_ts[bound_parts]

    group_number_dm_list_all_ts = dm_data_ts[0]
    subgroup_number_dm_list_all_ts = dm_data_ts[1]
    coordinates_dm_list_all_ts = dm_data_ts[2]

    # Switch central subhalo definition to r200
    if central_r200:
        group_number_dm_list_all_ts, subgroup_number_dm_list_all_ts = vpf.Rebind_Subhalos(group_number_dm_list_all_ts, subgroup_number_dm_list_all_ts, coordinates_dm_list_all_ts, group_number_list, subgroup_number_list, halo_coordinates_ts,halo_r200_host_list, boxsize, verbose=False)

    # Identify dm particles that are bound to subhaloes
    # Note this does nothing at present because I forgot to add the same functionality to write_hdf5_subvol.py (oops, just slightly affects r200 calc though)
    bound_parts = (subgroup_number_dm_list_all_ts >= 0) & (subgroup_number_dm_list_all_ts < 1e8) & (group_number_dm_list_all_ts >= 0)
    group_number_dm_list_ts = group_number_dm_list_all_ts[bound_parts]
    subgroup_number_dm_list_ts = subgroup_number_dm_list_all_ts[bound_parts]
    coordinates_dm_list_ts = coordinates_dm_list_all_ts[bound_parts]

    group_number_star_list_all_ts = star_data_ts[0]
    subgroup_number_star_list_all_ts = star_data_ts[1]
    mass_star_list_all_ts = star_data_ts[2]
    mass_star_init_list_all_ts = star_data_ts[3]
    aform_star_list_all_ts = star_data_ts[4]
    id_star_list_all_ts = star_data_ts[5]
    coordinates_star_list_all_ts = star_data_ts[6]

    # Switch central subhalo definition to r200
    if central_r200:
        group_number_star_list_all_ts, subgroup_number_star_list_all_ts = vpf.Rebind_Subhalos(group_number_star_list_all_ts, subgroup_number_star_list_all_ts, coordinates_star_list_all_ts, group_number_list, subgroup_number_list, halo_coordinates_ts,halo_r200_host_list, boxsize, verbose=False)

    # Identify star particles that are bound to subhaloes
    bound_parts = (subgroup_number_star_list_all_ts >= 0) & (subgroup_number_star_list_all_ts < 1e8) & (group_number_star_list_all_ts >= 0)
    group_number_star_list_ts = group_number_star_list_all_ts[bound_parts]
    subgroup_number_star_list_ts =subgroup_number_star_list_all_ts[bound_parts]
    mass_star_list_ts =mass_star_list_all_ts[bound_parts]
    mass_star_init_list_ts =mass_star_init_list_all_ts[bound_parts]
    aform_star_list_ts =aform_star_list_all_ts[bound_parts]
    id_star_list_ts =id_star_list_all_ts[bound_parts]
    coordinates_star_list_ts =coordinates_star_list_all_ts[bound_parts]

    group_number_bh_list_ts = bh_data_ts[0]
    subgroup_number_bh_list_ts = bh_data_ts[1]
    mass_bh_list_ts = bh_data_ts[2]
    coordinates_bh_list_ts = bh_data_ts[3]

    group_number_gas_list_all_ps = gas_data_ps[0]
    subgroup_number_gas_list_all_ps = gas_data_ps[1]
    mass_gas_list_all_ps = gas_data_ps[2]
    density_gas_list_all_ps = gas_data_ps[3]
    metallicity_gas_list_all_ps = gas_data_ps[4]
    id_gas_list_all_ps = gas_data_ps[5]
    coordinates_gas_list_all_ps = gas_data_ps[6]
    velocity_gas_list_all_ps = gas_data_ps[7]
    temperature_gas_list_all_ps = gas_data_ps[8]
    internal_energy_gas_list_all_ps = gas_data_ps[9]

    # Switch central subhalo definition to r200
    if central_r200:       
        nsep = max(len(str(int(subgroup_number_list_all_progen.max()))) +1 ,6)
        sub_ind_progen = group_number_list_progen * 10**nsep + subgroup_number_list_progen
        sub_ind_all_progen = group_number_list_all_progen * 10**nsep + subgroup_number_list_all_progen
        junk, ind_unique = np.unique(np.append(sub_ind_progen,sub_ind_all_progen),return_index = True)
        
        halo_coordinates_ps = [np.append(halo_x_list_progen,halo_x_list_all_progen)[ind_unique],\
                                   np.append(halo_y_list_progen,halo_y_list_all_progen)[ind_unique],\
                                   np.append(halo_z_list_progen,halo_z_list_all_progen)[ind_unique]]
        grn_temp = np.append(group_number_list_progen, group_number_list_all_progen)[ind_unique]
        sgrn_temp = np.append(subgroup_number_list_progen, subgroup_number_list_all_progen)[ind_unique]
        r200_temp = np.append(halo_r200_host_list_progen, halo_r200_host_list_all_progen)[ind_unique]
        group_number_gas_list_all_ps, subgroup_number_gas_list_all_ps = vpf.Rebind_Subhalos(group_number_gas_list_all_ps, subgroup_number_gas_list_all_ps, coordinates_gas_list_all_ps, grn_temp , sgrn_temp, halo_coordinates_ps, r200_temp, boxsize,verbose=False)

    # Identify gas particles that are bound to subhaloes
    bound_parts = (subgroup_number_gas_list_all_ps >= 0) & (subgroup_number_gas_list_all_ps < 1e8) & (group_number_gas_list_all_ps >= 0)
    group_number_gas_list_ps = group_number_gas_list_all_ps[bound_parts] 
    subgroup_number_gas_list_ps = subgroup_number_gas_list_all_ps[bound_parts] 
    mass_gas_list_ps = mass_gas_list_all_ps[bound_parts] 
    density_gas_list_ps = density_gas_list_all_ps[bound_parts]
    metallicity_gas_list_ps = metallicity_gas_list_all_ps[bound_parts]
    id_gas_list_ps = id_gas_list_all_ps[bound_parts]
    coordinates_gas_list_ps = coordinates_gas_list_all_ps[bound_parts]
    velocity_gas_list_ps = velocity_gas_list_all_ps[bound_parts]
    temperature_gas_list_ps = temperature_gas_list_all_ps[bound_parts]
    internal_energy_gas_list_ps = internal_energy_gas_list_all_ps[bound_parts]

    group_number_dm_list_ps = dm_data_ps[0]
    subgroup_number_dm_list_ps = dm_data_ps[1]
    coordinates_dm_list_ps = dm_data_ps[2]

    bound_parts = (subgroup_number_dm_list_ps >= 0) & (subgroup_number_dm_list_ps < 1e8) & (group_number_dm_list_ps >=0)
    group_number_dm_list_ps = group_number_dm_list_ps[bound_parts]
    subgroup_number_dm_list_ps = subgroup_number_dm_list_ps[bound_parts]
    coordinates_dm_list_ps = coordinates_dm_list_ps[bound_parts]

    group_number_star_list_ps = star_data_ps[0]
    subgroup_number_star_list_ps = star_data_ps[1]
    mass_star_list_ps = star_data_ps[2]
    mass_star_init_list_ps = star_data_ps[3]
    aform_star_list_ps = star_data_ps[4]
    id_star_list_ps = star_data_ps[5]
    coordinates_star_list_ps = star_data_ps[6]

    bound_parts = (subgroup_number_star_list_ps >= 0) & (subgroup_number_star_list_ps < 1e8) & (group_number_star_list_ps >= 0)

    group_number_star_list_ps = group_number_star_list_ps[bound_parts]
    subgroup_number_star_list_ps = subgroup_number_star_list_ps[bound_parts]
    mass_star_list_ps = mass_star_list_ps[bound_parts]
    mass_star_init_list_ps = mass_star_init_list_ps[bound_parts]
    aform_star_list_ps = aform_star_list_ps[bound_parts]
    id_star_list_ps = id_star_list_ps[bound_parts]
    coordinates_star_list_ps = coordinates_star_list_ps[bound_parts]
    
    group_number_bh_list_ps = bh_data_ps[0]
    subgroup_number_bh_list_ps = bh_data_ps[1]
    mass_bh_list_ps = bh_data_ps[2]
    coordinates_bh_list_ps = bh_data_ps[3]

    ## Unpack needed data from next step
    group_number_gas_list_all_ns = gas_data_ns[0]
    subgroup_number_gas_list_all_ns = gas_data_ns[1]
    density_gas_list_all_ns = gas_data_ns[3]
    metallicity_gas_list_all_ns = gas_data_ns[4]
    id_gas_list_all_ns = gas_data_ns[5]
    coordinates_gas_list_all_ns = gas_data_ns[6]
    temperature_gas_list_all_ns = gas_data_ns[8]

    # Switch central subhalo definition to r200
    if central_r200:

        nsep = max(len(str(int(subgroup_number_list_ns.max()))) +1 ,6)
        sub_ind_ns = group_number_list_ns* 10**nsep + subgroup_number_list_ns
        junk, ind_unique = np.unique(sub_ind_ns, return_index=True)
        group_number_temp = group_number_list_ns[ind_unique]
        subgroup_number_temp = subgroup_number_list_ns[ind_unique]
        halo_r200_host_temp = halo_r200_host_list_ns[ind_unique]

        halo_coordinates_ns = [halo_x_list_ns[ind_unique],halo_y_list_ns[ind_unique],halo_z_list_ns[ind_unique]]

        group_number_gas_list_all_ns, subgroup_number_gas_list_all_ns = vpf.Rebind_Subhalos(group_number_gas_list_all_ns, subgroup_number_gas_list_all_ns, coordinates_gas_list_all_ns, group_number_temp, subgroup_number_temp, halo_coordinates_ns,halo_r200_host_temp, boxsize, verbose=False)

    # Identify gas particles that are bound to subhaloes
    bound_parts = (subgroup_number_gas_list_all_ns >= 0) & (subgroup_number_gas_list_all_ns < 1e8) & (group_number_gas_list_all_ns >= 0)
    group_number_gas_list_ns = group_number_gas_list_all_ns[bound_parts] 
    subgroup_number_gas_list_ns = subgroup_number_gas_list_all_ns[bound_parts] 
    density_gas_list_ns = density_gas_list_all_ns[bound_parts]
    metallicity_gas_list_ns = metallicity_gas_list_all_ns[bound_parts]
    id_gas_list_ns = id_gas_list_all_ns[bound_parts]
    coordinates_gas_list_ns = coordinates_gas_list_all_ns[bound_parts]
    temperature_gas_list_ns = temperature_gas_list_all_ns[bound_parts]

    group_number_star_list_all_ns = star_data_ns[0]
    subgroup_number_star_list_all_ns = star_data_ns[1]
    id_star_list_all_ns = star_data_ns[5]
    coordinates_star_list_all_ns = star_data_ns[6]

    # Switch central subhalo definition to r200
    if central_r200:
        group_number_star_list_all_ns, subgroup_number_star_list_all_ns = vpf.Rebind_Subhalos(group_number_star_list_all_ns, subgroup_number_star_list_all_ns, coordinates_star_list_all_ns, group_number_temp, subgroup_number_temp, halo_coordinates_ns,halo_r200_host_temp, boxsize)

    # Identify star particles that are bound to subhaloes
    bound_parts = (subgroup_number_star_list_all_ns >= 0) & (subgroup_number_star_list_all_ns < 1e8) & (group_number_star_list_all_ns >= 0)
    group_number_star_list_ns = group_number_star_list_all_ns[bound_parts] 
    id_star_list_ns = id_star_list_all_ns[bound_parts]

    # Prune particle lists to contain only particles bound to subhalos we are interested in (this is a huge optimization step for nsub_1d > 1)
    
    if len(group_number_gas_list_ts) > 0 and len(group_number_list)>0:
        ptr = ms.match(group_number_gas_list_ts, group_number_list)
        ok_match = ptr >= 0

        group_number_gas_list_ts = group_number_gas_list_ts[ok_match]
        subgroup_number_gas_list_ts = subgroup_number_gas_list_ts[ok_match]
        mass_gas_list_ts = mass_gas_list_ts[ok_match]
        density_gas_list_ts = density_gas_list_ts[ok_match]
        metallicity_gas_list_ts = metallicity_gas_list_ts[ok_match]
        id_gas_list_ts = id_gas_list_ts[ok_match]
       
        coordinates_gas_list_ts = coordinates_gas_list_ts[ok_match]
        velocity_gas_list_ts = velocity_gas_list_ts[ok_match]
        temperature_gas_list_ts = temperature_gas_list_ts[ok_match]
        internal_energy_gas_list_ts = internal_energy_gas_list_ts[ok_match]

    if len(group_number_dm_list_ts) > 0 and len(group_number_list)>0:
        ptr = ms.match(group_number_dm_list_ts, group_number_list)
        ok_match = ptr >= 0

        group_number_dm_list_ts = group_number_dm_list_ts[ok_match]
        subgroup_number_dm_list_ts = subgroup_number_dm_list_ts[ok_match]
        coordinates_dm_list_ts = coordinates_dm_list_ts[ok_match]
        
    if len(group_number_star_list_ts)>0 and len(group_number_list)>0:
        ptr = ms.match(group_number_star_list_ts, group_number_list)
        ok_match = ptr >= 0

        group_number_star_list_ts = group_number_star_list_ts[ok_match]
        subgroup_number_star_list_ts = subgroup_number_star_list_ts[ok_match]
        mass_star_list_ts = mass_star_list_ts[ok_match]
        mass_star_init_list_ts = mass_star_init_list_ts[ok_match]
        aform_star_list_ts = aform_star_list_ts[ok_match]
        id_star_list_ts = id_star_list_ts[ok_match]
        coordinates_star_list_ts = coordinates_star_list_ts[ok_match]

    if len(group_number_bh_list_ts) > 0 and len(group_number_list)>0:
        ptr = ms.match(group_number_bh_list_ts, group_number_list)
        ok_match = ptr >= 0

        group_number_bh_list_ts = group_number_bh_list_ts[ok_match]
        subgroup_number_bh_list_ts = subgroup_number_bh_list_ts[ok_match]
        mass_bh_list_ts = mass_bh_list_ts[ok_match]
        coordinates_bh_list_ts = coordinates_bh_list_ts[ok_match]

    # Prune _ns lists
    if len(group_number_gas_list_ns) > 0 and len(group_number_list_ns)>0:
        ptr = ms.match(group_number_gas_list_ns, group_number_list_ns)
        ok_match = ptr >= 0

        group_number_gas_list_ns = group_number_gas_list_ns[ok_match]
        subgroup_number_gas_list_ns = subgroup_number_gas_list_ns[ok_match]
        density_gas_list_ns = density_gas_list_ns[ok_match]
        metallicity_gas_list_ns = metallicity_gas_list_ns[ok_match]
        id_gas_list_ns = id_gas_list_ns[ok_match]       
        coordinates_gas_list_ns = coordinates_gas_list_ns[ok_match]
        temperature_gas_list_ns = temperature_gas_list_ns[ok_match]

    # For _ps, we need to be careful to select particles that are only in subhalo progenitors (and not complete FoF groups which can include unwanted massive halos)
    # Create a unique id for each subhalo out of group_number and subgroup_number (this is because we don't have node_index for particles)
    nsep = max(len(str(int(subgroup_number_list_all_progen.max()))) +1 ,6)

    cat_list_all_progen = group_number_list_all_progen.astype("int64") * 10**nsep + subgroup_number_list_all_progen.astype("int64")
    cat_list_progen = group_number_list_progen.astype("int64") * 10**nsep + subgroup_number_list_progen.astype("int64")

    cat_ps_master_list = np.concatenate((cat_list_all_progen, cat_list_progen))
    cat_ps_master_list = np.unique(cat_ps_master_list)

    cat_gas_list_ps = group_number_gas_list_ps.astype("int64") * 10**nsep + subgroup_number_gas_list_ps.astype("int64")

    ptr = ms.match(cat_gas_list_ps, cat_ps_master_list)
    ok_match = ptr >= 0

    group_number_gas_list_ps = group_number_gas_list_ps[ok_match]
    subgroup_number_gas_list_ps = subgroup_number_gas_list_ps[ok_match]
    mass_gas_list_ps = mass_gas_list_ps[ok_match]
    density_gas_list_ps = density_gas_list_ps[ok_match]
    metallicity_gas_list_ps = metallicity_gas_list_ps[ok_match]
    temperature_gas_list_ps = temperature_gas_list_ps[ok_match]
    id_gas_list_ps = id_gas_list_ps[ok_match]
    coordinates_gas_list_ps = coordinates_gas_list_ps[ok_match]
    velocity_gas_list_ps = velocity_gas_list_ps[ok_match]
    internal_energy_gas_list_ps = internal_energy_gas_list_ps[ok_match]

    cat_dm_list_ps = group_number_dm_list_ps.astype("int64") * 10**nsep + subgroup_number_dm_list_ps.astype("int64")

    ptr = ms.match(cat_dm_list_ps, cat_ps_master_list)
    ok_match = ptr >= 0

    group_number_dm_list_ps = group_number_dm_list_ps[ok_match]
    subgroup_number_dm_list_ps = subgroup_number_dm_list_ps[ok_match]
    coordinates_dm_list_ps = coordinates_dm_list_ps[ok_match]

    cat_star_list_ps = group_number_star_list_ps.astype("int64") * 10**nsep + subgroup_number_star_list_ps.astype("int64")

    ptr = ms.match(cat_star_list_ps, cat_ps_master_list)
    ok_match = ptr >= 0

    group_number_star_list_ps = group_number_star_list_ps[ok_match]
    subgroup_number_star_list_ps = subgroup_number_star_list_ps[ok_match]
    mass_star_list_ps = mass_star_list_ps[ok_match]
    mass_star_init_list_ps = mass_star_init_list_ps[ok_match]
    aform_star_list_ps = aform_star_list_ps[ok_match]
    id_star_list_ps = id_star_list_ps[ok_match]
    coordinates_star_list_ps = coordinates_star_list_ps[ok_match]

    cat_bh_list_ps = group_number_bh_list_ps.astype("int64") * 10**nsep + subgroup_number_bh_list_ps.astype("int64")

    ptr = ms.match(cat_bh_list_ps, cat_ps_master_list)
    ok_match = ptr >= 0

    group_number_bh_list_ps = group_number_bh_list_ps[ok_match]
    subgroup_number_bh_list_ps = subgroup_number_bh_list_ps[ok_match]
    mass_bh_list_ps = mass_bh_list_ps[ok_match]
    coordinates_bh_list_ps = coordinates_bh_list_ps[ok_match]
    
    h = sim_props_ts[0]
    omm = sim_props_ts[5]
    expansion_factor_ts = sim_props_ts[2]
    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    mass_dm = sim_props_ts[4]
    redshift_ts = sim_props_ts[1]

    expansion_factor_ns = sim_props_ns[2]
    redshift_ns = 1./expansion_factor_ns -1
    t_ns, tlb_ns = uc.t_Universe(expansion_factor_ns, omm, h)

    expansion_factor_ps = sim_props_ps[2]
    t_ps, tlb_ps = uc.t_Universe(expansion_factor_ps, omm, h)
    redshift_ps = sim_props_ps[1]

    #print "this snapshot is at redshift", redshift_ts

    id_star_gas_as_fof = []
    subgroup_number_star_gas_as_fof = []
    id_star_gas_all_progen_fof = []
    subgroup_number_star_gas_all_progen_fof = []
    group_number_star_gas_all_progen_fof = []
    fam_star_gas_all_progen_fof = []
    
    # Copy particle lists so we can prune them as we go through
    group_number_gas_list_ts_copy = np.copy(group_number_gas_list_ts)
    subgroup_number_gas_list_ts_copy = np.copy(subgroup_number_gas_list_ts)
    id_gas_list_ts_copy = np.copy(id_gas_list_ts)
    
    group_number_star_list_ts_copy = np.copy(group_number_star_list_ts)
    subgroup_number_star_list_ts_copy = np.copy(subgroup_number_star_list_ts)
    id_star_list_ts_copy = np.copy(id_star_list_ts)
    
    group_number_gas_list_ps_copy = np.copy(group_number_gas_list_ps)
    subgroup_number_gas_list_ps_copy = np.copy(subgroup_number_gas_list_ps)
    id_gas_list_ps_copy = np.copy(id_gas_list_ps)
    metallicity_gas_list_ps_copy = np.copy(metallicity_gas_list_ps) # Note all the extra fields are there so I can use arbitrary ISM defn
    temperature_gas_list_ps_copy = np.copy(temperature_gas_list_ps) # Note all the extra fields are there so I can use arbitrary ISM defn
    density_gas_list_ps_copy = np.copy(density_gas_list_ps)
    mass_gas_list_ps_copy = np.copy(mass_gas_list_ps)
    coordinates_gas_list_ps_copy = np.copy(coordinates_gas_list_ps)
    velocity_gas_list_ps_copy = np.copy(velocity_gas_list_ps)
    internal_energy_gas_list_ps_copy = np.copy(internal_energy_gas_list_ps)

    group_number_dm_list_ps_copy = np.copy(group_number_dm_list_ps)
    subgroup_number_dm_list_ps_copy = np.copy(subgroup_number_dm_list_ps)
    coordinates_dm_list_ps_copy = np.copy(coordinates_dm_list_ps)

    group_number_star_list_ps_copy = np.copy(group_number_star_list_ps)
    subgroup_number_star_list_ps_copy = np.copy(subgroup_number_star_list_ps)
    id_star_list_ps_copy = np.copy(id_star_list_ps)
    coordinates_star_list_ps_copy = np.copy(coordinates_star_list_ps)
    mass_star_list_ps_copy = np.copy(mass_star_list_ps)

    group_number_bh_list_ps_copy = np.copy(group_number_bh_list_ps)
    subgroup_number_bh_list_ps_copy = np.copy(subgroup_number_bh_list_ps)
    coordinates_bh_list_ps_copy = np.copy(coordinates_bh_list_ps)
    mass_bh_list_ps_copy = np.copy(mass_bh_list_ps)

    # Loop over FoF groups and find progenitor particle lists for each
    for i_halo in range(len(group_number_list)):

        halo_group_number = group_number_list[i_halo]
        halo_subgroup_number = subgroup_number_list[i_halo]
        halo_descendant_index = descendant_index_list[i_halo]

        if halo_subgroup_number > 0:
            continue

        halo_group_number_progen = group_number_list_progen[i_halo]
        halo_subgroup_number_progen = subgroup_number_list_progen[i_halo]

        # List of all star and gas particles in this FoF group
        select_gas_as = (halo_group_number == group_number_gas_list_ts_copy)
        id_gas_as = id_gas_list_ts_copy[select_gas_as]
        subgroup_number_gas_as = subgroup_number_gas_list_ts_copy[select_gas_as]

        select_star_as = (halo_group_number == group_number_star_list_ts_copy)
        id_star_as = id_star_list_ts_copy[select_star_as]
        subgroup_number_star_as = subgroup_number_star_list_ts_copy[select_star_as]

        id_star_gas_as = np.append(id_gas_as, id_star_as)
        subgroup_number_star_gas_as = np.append(subgroup_number_gas_as, subgroup_number_star_as)

        id_star_gas_as_fof.append(id_star_gas_as)
        subgroup_number_star_gas_as_fof.append(subgroup_number_star_gas_as)

        # List of all star and gas particles that are progenitors of this FoF group
        # First select prognenitor particles that aren't progenitors of this subhalo but are progenitors of other subhalos in this FoF group
        select_progenitors = np.zeros_like(descendant_index_list_all_progen) < 0 # init to false
        select_sub = group_number_list == halo_group_number
        for i_sub in range(len(group_number_list[select_sub])):
            select_progenitors = select_progenitors | (node_index_list[select_sub][i_sub] == descendant_index_list_all_progen)

        select_gas_all_progens = np.zeros_like(group_number_gas_list_ps_copy) < 0
        select_dm_all_progens = np.zeros_like(group_number_dm_list_ps_copy) < 0
        select_star_all_progens = np.zeros_like(group_number_star_list_ps_copy) < 0
        select_bh_all_progens = np.zeros_like(group_number_bh_list_ps_copy) < 0

        for group_number_all_progen, subgroup_number_all_progen in zip(group_number_list_all_progen[select_progenitors], subgroup_number_list_all_progen[select_progenitors]):
            
            select_gas_progens = (group_number_gas_list_ps_copy == group_number_all_progen) & (subgroup_number_gas_list_ps_copy == subgroup_number_all_progen)
            select_dm_progens = (group_number_dm_list_ps_copy == group_number_all_progen) & (subgroup_number_dm_list_ps_copy == subgroup_number_all_progen)
            select_star_progens = (group_number_star_list_ps_copy == group_number_all_progen) & (subgroup_number_star_list_ps_copy == subgroup_number_all_progen)
            select_bh_progens = (group_number_bh_list_ps_copy == group_number_all_progen) & (subgroup_number_bh_list_ps_copy == subgroup_number_all_progen)

            select_gas_all_progens[select_gas_progens] = True
            select_dm_all_progens[select_dm_progens] = True
            select_star_all_progens[select_star_progens] = True
            select_bh_all_progens[select_bh_progens] = True


        id_star_gas_all_progen = id_gas_list_ps_copy[select_gas_all_progens]
        subgroup_number_star_gas_all_progen = subgroup_number_gas_list_ps_copy[select_gas_all_progens]
        group_number_star_gas_all_progen = group_number_gas_list_ps_copy[select_gas_all_progens]

        # Work out star-forming/non-star forming progenitor particles
        metallicity_gas = metallicity_gas_list_ps_copy[select_gas_all_progens]
        temperature_gas = temperature_gas_list_ps_copy[select_gas_all_progens]
        xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
        nh = density_gas_list_ps_copy[select_gas_all_progens] * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
        nh_thresh = np.min( (np.zeros_like(nh)+10.0 , 0.1 * (metallicity_gas/0.002)**-0.64), axis=0)
        # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
        T_eos = 8e3 * (nh/0.1)**(4/3.)
        SF = (nh > nh_thresh) & (np.log10(temperature_gas) < np.log10(T_eos) +0.5)
        fam_star_gas_all_progen = np.ones_like(metallicity_gas) # fam == 1 = nsf, 2 = sf
        fam_star_gas_all_progen[SF] = 2

        id_star_gas_all_progen = np.append(id_star_gas_all_progen, id_star_list_ps_copy[select_star_all_progens])
        subgroup_number_star_gas_all_progen = np.append(subgroup_number_star_gas_all_progen, subgroup_number_star_list_ps_copy[select_star_all_progens])
        group_number_star_gas_all_progen = np.append(group_number_star_gas_all_progen, group_number_star_list_ps_copy[select_star_all_progens])
        fam_star_gas_all_progen = np.append(fam_star_gas_all_progen, np.zeros_like(id_star_list_ps_copy[select_star_all_progens]))

        id_star_gas_all_progen_fof.append(id_star_gas_all_progen)
        subgroup_number_star_gas_all_progen_fof.append(subgroup_number_star_gas_all_progen)
        group_number_star_gas_all_progen_fof.append(group_number_star_gas_all_progen)
        fam_star_gas_all_progen_fof.append(fam_star_gas_all_progen)

        # Prune (copied particle lists) to speed up later iterations of this loop
        id_gas_list_ps_copy = id_gas_list_ps_copy[select_gas_all_progens==False]
        group_number_gas_list_ps_copy = group_number_gas_list_ps_copy[select_gas_all_progens==False]
        subgroup_number_gas_list_ps_copy = subgroup_number_gas_list_ps_copy[select_gas_all_progens==False]
        metallicity_gas_list_ps_copy = metallicity_gas_list_ps_copy[select_gas_all_progens==False]
        density_gas_list_ps_copy = density_gas_list_ps_copy[select_gas_all_progens==False]
        mass_gas_list_ps_copy = mass_gas_list_ps_copy[select_gas_all_progens==False]
        coordinates_gas_list_ps_copy = coordinates_gas_list_ps_copy[select_gas_all_progens==False]
        velocity_gas_list_ps_copy = velocity_gas_list_ps_copy[select_gas_all_progens==False]
        temperature_gas_list_ps_copy = temperature_gas_list_ps_copy[select_gas_all_progens==False]
        internal_energy_gas_list_ps_copy = internal_energy_gas_list_ps_copy[select_gas_all_progens==False]

        group_number_dm_list_ps_copy = group_number_dm_list_ps_copy[select_dm_all_progens==False]
        subgroup_number_dm_list_ps_copy = subgroup_number_dm_list_ps_copy[select_dm_all_progens==False]
        coordinates_dm_list_ps_copy = coordinates_dm_list_ps_copy[select_dm_all_progens==False]

        id_star_list_ps_copy = id_star_list_ps_copy[select_star_all_progens==False]
        group_number_star_list_ps_copy = group_number_star_list_ps_copy[select_star_all_progens==False]
        subgroup_number_star_list_ps_copy = subgroup_number_star_list_ps_copy[select_star_all_progens==False]
        mass_star_list_ps_copy = mass_star_list_ps_copy[select_star_all_progens==False]
        coordinates_star_list_ps_copy = coordinates_star_list_ps_copy[select_star_all_progens==False]

        group_number_bh_list_ps_copy = group_number_bh_list_ps_copy[select_bh_all_progens==False]
        subgroup_number_bh_list_ps_copy = subgroup_number_bh_list_ps_copy[select_bh_all_progens==False]
        mass_bh_list_ps_copy = mass_bh_list_ps_copy[select_bh_all_progens==False]
        coordinates_bh_list_ps_copy = coordinates_bh_list_ps_copy[select_bh_all_progens==False]

        id_star_list_ts_copy = id_star_list_ts_copy[select_star_as==False]
        group_number_star_list_ts_copy = group_number_star_list_ts_copy[select_star_as==False]
        subgroup_number_star_list_ts_copy = subgroup_number_star_list_ts_copy[select_star_as==False]

        id_gas_list_ts_copy = id_gas_list_ts_copy[select_gas_as==False]
        group_number_gas_list_ts_copy = group_number_gas_list_ts_copy[select_gas_as==False]
        subgroup_number_gas_list_ts_copy = subgroup_number_gas_list_ts_copy[select_gas_as==False]


    # Loop over haloes list and init the ejected particles lists for this snipshot
    id_gas_ejecta_list_ts = []
    id_gas_wind_list_ts = []

    id_gas_halo_reheat_list_ts = []
    id_gas_ism_reheat_list_ts = []

    # Create a master ejecta list to check for FoF exchange accretion
    id_gas_ejecta_master_list_ts = []
    id_gas_wind_master_list_ts = []

    if i_snip == 1:
        ih_choose = ih_choose # Now set before main loop
    else:
        ih_choose = np.argsort(abs(node_index_list-di_choose))[0]
        if np.sort(abs(node_index_list-di_choose))[0] > 0:
            print "yikes, possible tree problems"
            ih_choose = np.where(subgroup_number_list==0)[0][0]
    ih_choose_prev = ih_choose
    

    for i_halo in range(len(group_number_list)):
        id_gas_ejecta_list_ts.append([])
        id_gas_wind_list_ts.append([])
        id_gas_ism_reheat_list_ts.append([])
        id_gas_halo_reheat_list_ts.append([])

        halo_node_index = node_index_list[i_halo]
        
        # Add ejected particles from each progenitor of this subhalo
        select_progenitors = np.where(halo_node_index == descendant_index_list_ps)[0]
        for select_progenitor_i in select_progenitors:
            
            id_gas_ejecta_list_ts[i_halo] = np.append(np.array(id_gas_ejecta_list_ts[i_halo]), np.array(id_gas_ejecta_list_ps[select_progenitor_i]))
            id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), np.array(id_gas_wind_list_ps[select_progenitor_i]))

            id_gas_ism_reheat_list_ts[i_halo] = np.append(np.array(id_gas_ism_reheat_list_ts[i_halo]), np.array(id_gas_ism_reheat_list_ps[select_progenitor_i]))
            id_gas_halo_reheat_list_ts[i_halo] = np.append(np.array(id_gas_halo_reheat_list_ts[i_halo]), np.array(id_gas_halo_reheat_list_ps[select_progenitor_i]))

            id_gas_ejecta_master_list_ts = np.append(np.array(id_gas_ejecta_master_list_ts), np.array(id_gas_ejecta_list_ps[select_progenitor_i]))
            id_gas_wind_master_list_ts = np.append(np.array(id_gas_wind_master_list_ts), np.array(id_gas_wind_list_ps[select_progenitor_i]))

        # Check uniqueness of the ejected particle lists
        if len(id_gas_ejecta_list_ts[i_halo]) != len(np.unique(id_gas_ejecta_list_ts[i_halo])):
            id_gas_ejecta_list_ts[i_halo], index = np.unique(id_gas_ejecta_list_ts[i_halo], return_index=True)

        if len(id_gas_wind_list_ts[i_halo]) != len(np.unique(id_gas_wind_list_ts[i_halo])):
            id_gas_wind_list_ts[i_halo], index = np.unique(id_gas_wind_list_ts[i_halo], return_index=True)

        # Check uniqueness of the ejected particle lists
        if len(id_gas_ism_reheat_list_ts[i_halo]) != len(np.unique(id_gas_ism_reheat_list_ts[i_halo])):
            id_gas_ism_reheat_list_ts[i_halo], index = np.unique(id_gas_ism_reheat_list_ts[i_halo], return_index=True)

        if len(id_gas_halo_reheat_list_ts[i_halo]) != len(np.unique(id_gas_halo_reheat_list_ts[i_halo])):
            id_gas_halo_reheat_list_ts[i_halo], index = np.unique(id_gas_halo_reheat_list_ts[i_halo], return_index=True)

    id_gas_ejecta_master_list_ts = np.unique(id_gas_ejecta_master_list_ts)
    id_gas_wind_master_list_ts = np.unique(id_gas_wind_master_list_ts)

    # Discard ejecta particles that are not bound to subhalos from the master list to free up memory (note these are retained in the subhalo ejecta lists so that they are passed on for the next step)
    if len(id_gas_ejecta_master_list_ts)>0 and len(id_gas_list_ts)>0:
        ptr = ms.match(id_gas_ejecta_master_list_ts, id_gas_list_ts)
        id_gas_ejecta_master_list = id_gas_ejecta_master_list_ts[ptr>=0]
    if len(id_gas_wind_master_list_ts)>0 and len(id_gas_list_ts)>0:
        ptr = ms.match(id_gas_wind_master_list_ts, id_gas_list_ts)
        id_gas_wind_master_list = id_gas_wind_master_list_ts[ptr>=0]


    # Loop over haloes list and do measurements
    for i_halo in range(len(group_number_list)):

        if not i_halo == ih_choose:
            continue

        halo_group_number = group_number_list[i_halo]
        halo_subgroup_number = subgroup_number_list[i_halo]

        if i_halo == ih_choose:
            print "looking at ", halo_group_number, halo_subgroup_number, ", progen is", group_number_list_progen[i_halo], subgroup_number_list_progen[i_halo]

        halo_mchalo = mchalo_list[i_halo]
        halo_mhhalo = mhhalo_list[i_halo]

        halo_x = halo_x_list[i_halo]
        halo_y = halo_y_list[i_halo]
        halo_z = halo_z_list[i_halo]
        halo_vx = halo_vx_list[i_halo]
        halo_vy = halo_vy_list[i_halo]
        halo_vz = halo_vz_list[i_halo]
        halo_vmax = halo_vmax_list[i_halo]
        halo_r200_host = halo_r200_host_list[i_halo]

        vmax_track_full[i_snip] = halo_vmax
        use_ns_track_full[i_snip] = d_snap_ns

        halo_x_progen = halo_x_list_progen[i_halo]
        halo_y_progen = halo_y_list_progen[i_halo]
        halo_z_progen = halo_z_list_progen[i_halo]
        halo_vx_progen = halo_vx_list_progen[i_halo]
        halo_vy_progen = halo_vy_list_progen[i_halo]
        halo_vz_progen = halo_vz_list_progen[i_halo]
        halo_vmax_progen = halo_vmax_list_progen[i_halo]
        halo_r200_host_progen = halo_r200_host_list_progen[i_halo]

        halo_x_ns = halo_x_list_ns[i_halo]
        halo_y_ns = halo_y_list_ns[i_halo]
        halo_z_ns = halo_z_list_ns[i_halo]
        halo_group_number_ns = group_number_list_ns[i_halo]
        halo_subgroup_number_ns = subgroup_number_list_ns[i_halo]

        halo_group_number_progen = group_number_list_progen[i_halo]
        halo_subgroup_number_progen = subgroup_number_list_progen[i_halo]

        ############### Select particles and perform unit conversions ############################

        ###### Select central gas particles
        select_gas = (halo_group_number == group_number_gas_list_ts) & (halo_subgroup_number == subgroup_number_gas_list_ts)

        # Convert to Msun
        g2Msun = 1./(1.989e33)
        mass_gas = mass_gas_list_ts[select_gas]
        mass_gas *= 1.989e43 * g2Msun / h

        metallicity_gas = metallicity_gas_list_ts[select_gas]
        density_gas = density_gas_list_ts[select_gas]
        temperature_gas = temperature_gas_list_ts[select_gas]
        internal_energy_gas = internal_energy_gas_list_ts[select_gas]        

        # compute sf threshold
        xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
        nh_gas = density_gas * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
        nh_thresh = np.min( (np.zeros_like(nh_gas)+10.0 , 0.1 * (metallicity_gas/0.002)**-0.64), axis=0)
        # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
        T_eos = 8e3 * (nh_gas/0.1)**(4/3.)
        SF = (nh_gas > nh_thresh) & (np.log10(temperature_gas) < np.log10(T_eos) +0.5)

        id_gas = id_gas_list_ts[select_gas]
        coordinates_gas = coordinates_gas_list_ts[select_gas]

        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_halo = [halo_x, halo_y, halo_z]
        coordinates_gas = mpf.Centre_Halo(coordinates_gas, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
                
        # Put velocity into proper coordinates (kms)
        velocity_gas = velocity_gas_list_ts[select_gas] * expansion_factor_ts**0.5

        # change velocities to halo frame
        velocity_gas += - np.array([halo_vx, halo_vy, halo_vz]) # note halo velocities were already in proper peculiar velocity units

        radius_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
        v_gas = np.sqrt(np.square(velocity_gas[:,0])+np.square(velocity_gas[:,1])+np.square(velocity_gas[:,2]))

        r_gas_norm = np.swapaxes(np.array([coordinates_gas[:,0],coordinates_gas[:,1],coordinates_gas[:,2]]) / radius_gas,0,1)
        cv_gas = np.sum(r_gas_norm * velocity_gas,axis=1)
        radius_gas *= 1./ (halo_r200_host * 1e3 /h / (1+redshift_ts)) # dimensionless

        # Stuff to be stored in "full" tracking lists
        ptr = ms.match(id_track_full, id_gas_list_all_ts)
        ok_match = ptr >= 0

        in_sat = (group_number_gas_list_all_ts[ptr][ok_match] >= 0) & ((group_number_gas_list_all_ts[ptr][ok_match] != halo_group_number)|(subgroup_number_gas_list_all_ts[ptr][ok_match] != halo_subgroup_number))
        group_number_track_full[i_snip, ok_match] = group_number_gas_list_all_ts[ptr][ok_match]

        coord_temp = coordinates_gas_list_all_ts[ptr][ok_match]
        coord_temp = mpf.Centre_Halo(coord_temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

        velocity_temp = velocity_gas_list_all_ts[ptr][ok_match] * expansion_factor_ts**0.5
        velocity_temp += - np.array([halo_vx, halo_vy, halo_vz])

        radius_temp = np.sqrt(np.square(coord_temp[:,0])+np.square(coord_temp[:,1])+np.square(coord_temp[:,2]))

        v_temp = np.sqrt(np.square(velocity_temp[:,0])+np.square(velocity_temp[:,1])+np.square(velocity_temp[:,2]))
        r_temp_norm = np.swapaxes(np.array([coord_temp[:,0],coord_temp[:,1],coord_temp[:,2]]) / radius_temp,0,1)
        cv_temp = np.sum(r_temp_norm * velocity_temp,axis=1)
        radius_temp *= 1./ (halo_r200_host * 1e3 /h / (1+redshift_ts)) # dimensionless

        temperature_temp = temperature_gas_list_all_ts[ptr][ok_match]
        nh_temp = density_gas_list_all_ts[ptr][ok_match] * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))

        radius_track_full[i_snip, ok_match] = radius_temp
        radius_phys_track_full[i_snip, ok_match] = radius_temp * (halo_r200_host * 1e3 /h / (1+redshift_ts)) # pkpc

        if np.max(radius_temp) > 20:
            print ""
            print "Looks like we have a tree problem (or possibly periodic box problem), max normaalised radius of tracked particles is", np.max(radius_temp)
            print ""

        temp_track_full[i_snip, ok_match] = temperature_temp
        nh_track_full[i_snip,ok_match] = nh_temp
        cv_track_full[i_snip,ok_match] = cv_temp
        v_track_full[i_snip,ok_match] = v_temp
        in_sat_flag_track_full[i_snip,ok_match] = in_sat

        if len(id_gas)>0:
            ptr = ms.match(id_track_full, id_gas)
            ok_match = ptr>=0
            temp = np.zeros(len(SF[ptr][ok_match]))
            temp[SF[ptr][ok_match]] = 1
            sf_flag_track_full[i_snip,ok_match] = temp
        
        ##### Select star particles that belong to desired group, subgroup
        select_star = (halo_group_number == group_number_star_list_ts) & (halo_subgroup_number == subgroup_number_star_list_ts)

        # Convert to Msun
        mass_star = mass_star_list_ts[select_star]
        mass_star *= 1.989e43 * g2Msun / h

        mass_star_init = mass_star_init_list_ts[select_star]
        mass_star_init *= 1.989e43 * g2Msun / h

        coordinates_star = coordinates_star_list_ts[select_star]
        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_star = mpf.Centre_Halo(coordinates_star, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

        id_star = id_star_list_ts[select_star]

        radius_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2])) / (halo_r200_host * 1e3 /h / (1+redshift_ts))
        if len(id_star)>0:
            ptr = ms.match(id_track_full, id_star)
            ok_match = ptr >= 0
            radius_track_full[i_snip, ok_match] = radius_star[ptr][ok_match]
            radius_phys_track_full[i_snip, ok_match] = radius_star[ptr][ok_match] * (halo_r200_host * 1e3 /h / (1+redshift_ts))

        # Work out which star particles formed between now and the previous timestep
        aform_star = aform_star_list_ts[select_star]
        fresh_star = aform_star > expansion_factor_ps

        ##### Select dm/bh particles that belong to desired group, subgroup (note only used for some ISM defns)
        select_dm = (halo_group_number == group_number_dm_list_ts) & (halo_subgroup_number == subgroup_number_dm_list_ts)
        select_bh = (halo_group_number == group_number_bh_list_ts) & (halo_subgroup_number == subgroup_number_bh_list_ts)

        coordinates_dm = coordinates_dm_list_ts[select_dm]
        coordinates_bh = coordinates_bh_list_ts[select_bh]

        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_dm = mpf.Centre_Halo(coordinates_dm, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
        coordinates_bh = mpf.Centre_Halo(coordinates_bh, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

        mass_bh = mass_bh_list_ts[select_bh]
        mass_bh *= 1.989e43 * g2Msun / h

        # Select particles that belong to the ISM (following at present Mitchell et al. (2018) selection criteria)
        # First compute relevant quantities to be passed in
        coordinates_parts = [coordinates_gas, coordinates_dm, coordinates_star, coordinates_bh]        
        gas_props = [internal_energy_gas, temperature_gas, nh_gas, velocity_gas, SF]
        mass_part = [mass_gas, mass_dm * 1.989e43 * g2Msun / h, mass_star, mass_bh]
        # Note at some point - you may correct the r200 correctly into comoving units in select_subvols.py - in which case you should also correct units here
        halo_props = [halo_r200_host, halo_group_number, halo_subgroup_number,halo_vmax]
        sim_props = [redshift_ts, h, boxsize, omm]

        ISM =  mpf.Select_ISM(coordinates_parts, gas_props, mass_part, halo_props , sim_props)

        if len(id_gas)>0:
            ptr = ms.match(id_track_full, id_gas)
            ok_match = ptr >= 0

            temp = np.zeros(len(ISM[ptr][ok_match]))
            temp[ISM[ptr][ok_match]] = 1
            #temp[np.where(SF[ptr][ok_match])[0]] += 1 # Comment this out to keep track of SF separately
            ism_flag_track_full[i_snip,ok_match] = temp

        ##### Select progenitor star particles that belong to desired group, subgroup
        select_star_progen = (halo_group_number_progen == group_number_star_list_ps) & (halo_subgroup_number_progen == subgroup_number_star_list_ps)

        id_star_progen = id_star_list_ps[select_star_progen]
        mass_star_progen = mass_star_list_ps[select_star_progen]
        mass_star_progen *= 1.989e43 * g2Msun / h
        mass_star_init_progen = mass_star_init_list_ps[select_star_progen]
        mass_star_init_progen *= 1.989e43 * g2Msun / h

        coordinates_star_progen = coordinates_star_list_ps[select_star_progen]
        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_halo_progen = [halo_x_progen, halo_y_progen, halo_z_progen]
        coordinates_star_progen = mpf.Centre_Halo(coordinates_star_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc

        ###### Select central progenitor gas particles #########
        select_gas_progen = (halo_group_number_progen == group_number_gas_list_ps) & (halo_subgroup_number_progen == subgroup_number_gas_list_ps)
         
        mass_gas_progen = mass_gas_list_ps[select_gas_progen]
        mass_gas_progen *= 1.989e43 * g2Msun / h

        metallicity_gas_progen = metallicity_gas_list_ps[select_gas_progen]
        coordinates_gas_progen = coordinates_gas_list_ps[select_gas_progen]

        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_gas_progen = mpf.Centre_Halo(coordinates_gas_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc

        # Put velocity into proper coordinates (kms)
        velocity_gas_progen = velocity_gas_list_ps[select_gas_progen] * expansion_factor_ps**0.5

        # change velocities to halo frame
        velocity_gas_progen += - np.array([halo_vx_progen, halo_vy_progen, halo_vz_progen]) # note halo velocities were already in proper peculiar velocity units

        temperature_gas_progen = temperature_gas_list_ps[select_gas_progen]
        internal_energy_gas_progen = internal_energy_gas_list_ps[select_gas_progen]
        density_gas_progen = density_gas_list_ps[select_gas_progen]

        # compute sf threshold
        nh_gas_progen = density_gas_progen * xh_sf_thresh * h**2 * expansion_factor_ps**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
        nh_thresh = np.min( (np.zeros_like(nh_gas_progen)+10.0 , 0.1 * (metallicity_gas_progen/0.002)**-0.64), axis=0)
        # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
        T_eos = 8e3 * (nh_gas_progen/0.1)**(4/3.)
        SF_progen = (nh_gas_progen > nh_thresh) & (np.log10(temperature_gas_progen) < np.log10(T_eos) +0.5)

        id_gas_progen = id_gas_list_ps[select_gas_progen]

        ##### Select progenitor dm/bh particles that belong to desired group, subgroup (note only used for some ISM defns)
        select_dm_progen = (halo_group_number_progen == group_number_dm_list_ps) & (halo_subgroup_number_progen == subgroup_number_dm_list_ps)
        select_bh_progen = (halo_group_number_progen == group_number_bh_list_ps) & (halo_subgroup_number_progen == subgroup_number_bh_list_ps)

        coordinates_dm_progen = coordinates_dm_list_ps[select_dm_progen]
        coordinates_bh_progen = coordinates_bh_list_ps[select_bh_progen]

        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_dm_progen = mpf.Centre_Halo(coordinates_dm_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
        coordinates_bh_progen = mpf.Centre_Halo(coordinates_bh_progen, coordinates_halo_progen, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc

        mass_bh_progen = mass_bh_list_ps[select_bh_progen]
        mass_bh_progen *= 1.989e43 * g2Msun / h

        # Select particles that belong to the ISM (following at present Mitchell et al. (2018) selection criteria)
        # First compute relevant quantities to be passed in
        coordinates_parts = [coordinates_gas_progen, coordinates_dm_progen, coordinates_star_progen, coordinates_bh_progen]
        gas_props = [internal_energy_gas_progen, temperature_gas_progen, nh_gas_progen, velocity_gas_progen, SF_progen]
        mass_part = [mass_gas_progen, mass_dm * 1.989e43 * g2Msun / h, mass_star_progen, mass_bh_progen]

        # Note at some point - you may correct the r200 correctly into comoving units in select_subvols.py - in which case you should also correct units here
        halo_props = [halo_r200_host_progen, halo_group_number_progen, halo_subgroup_number_progen,halo_vmax_progen]
        sim_props = [redshift_ps, h, boxsize, omm]
        
        ISM_progen =  mpf.Select_ISM(coordinates_parts, gas_props, mass_part, halo_props , sim_props)

        # Append star and gas progen lists together (for calculating accretion of stars onto satellites)
        id_star_gas_progen = np.append(id_star_progen, id_gas_progen)
        id_star_gas = np.append(id_star, id_gas)
        fam_star_gas = np.append(np.ones_like(id_star),np.ones_like(id_gas)+1)

        ###### Select particles from the descendant subhalo on the next output
        select_gas_fof_ns = (halo_group_number_ns == group_number_gas_list_ns)

        id_gas_fof_ns = id_gas_list_ns[select_gas_fof_ns]

        metallicity_gas_fof_ns = metallicity_gas_list_ns[select_gas_fof_ns]
        density_gas_fof_ns = density_gas_list_ns[select_gas_fof_ns]
        coordinates_gas_fof_ns = coordinates_gas_list_ns[select_gas_fof_ns]
        temperature_gas_fof_ns = temperature_gas_list_ns[select_gas_fof_ns]

        # compute sf threshold
        xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
        nh_gas_ns = density_gas_fof_ns * xh_sf_thresh * h**2 * expansion_factor_ns**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
        nh_thresh = np.min( (np.zeros_like(nh_gas_ns)+10.0 , 0.1 * (metallicity_gas_fof_ns/0.002)**-0.64), axis=0)
        # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
        T_eos = 8e3 * (nh_gas_ns/0.1)**(4/3.)
        SF_fof_ns = (nh_gas_ns > nh_thresh) & (np.log10(temperature_gas_fof_ns) < np.log10(T_eos) +0.5)

        # Put spatial position into proper, halo-centered units # pkpc
        coordinates_halo_ns = [halo_x_ns, halo_y_ns, halo_z_ns]
        coordinates_gas_fof_ns = mpf.Centre_Halo(coordinates_gas_fof_ns, coordinates_halo_ns, boxsize) * 1e3 /h / (1+redshift_ns) # pkpc

        select_star_fof_ns = (halo_group_number_ns == group_number_star_list_ns)
        id_star_fof_ns = id_star_list_ns[select_star_fof_ns]


        ####### Select and concatenate all ejected particles from all progenitors of this fof group
        select_progenitors = np.zeros_like(descendant_index_list_ps) < 0 # init to false
        select_sub = group_number_list == halo_group_number
        for i_sub in range(len(group_number_list[select_sub])):
            select_progenitors = select_progenitors | (node_index_list[select_sub][i_sub] == descendant_index_list_ps)
        select_progenitors = np.where(select_progenitors)[0]

        id_gas_ejecta_fof_progen_list = []
        for select_progenitor_i in select_progenitors:
            id_gas_ejecta_fof_progen_list = np.append(np.array(id_gas_ejecta_fof_progen_list),np.array(id_gas_ejecta_list_ps[select_progenitor_i]))

        # Find which FoF group this subhalo belongs to and retrieve appropriate particle lists
        ind_fof = np.argmin(abs(halo_group_number - group_number_list[subgroup_number_list == 0]))
        id_star_gas_all_progen = id_star_gas_all_progen_fof[ind_fof]
        subgroup_number_star_gas_all_progen = subgroup_number_star_gas_all_progen_fof[ind_fof]
        group_number_star_gas_all_progen = group_number_star_gas_all_progen_fof[ind_fof]
        fam_star_gas_all_progen = fam_star_gas_all_progen_fof[ind_fof]

        id_star_gas_as = id_star_gas_as_fof[ind_fof]
        subgroup_number_star_gas_as = subgroup_number_star_gas_as_fof[ind_fof]

        # Find new positions of particles that are being tracked
        if not init_tracking and i_halo == ih_choose:
            ptr = ms.match(id_part_track, id_gas_list_all_ts)
            ok_match = ptr>=0

            coordinates_match = np.zeros((len(id_part_track),3))+np.nan
            coordinates_match[ok_match] = coordinates_gas_list_all_ts[ptr][ok_match]

            # Also match to star particles
            ptr2 = ms.match(id_part_track, id_star_list_all_ts)
            ok_match2 = ptr2>=0

            coordinates_match[ok_match2] = coordinates_star_list_all_ts[ptr2][ok_match2]

            # Put into halo frame
            coordinates_match = mpf.Centre_Halo(coordinates_match, [halo_x, halo_y, halo_z], boxsize) *1e3 /h / (1+redshift_ps)

            velocity_match = np.zeros((len(id_part_track),3))+np.nan
            velocity_match[ok_match] = velocity_gas_list_all_ts[ptr][ok_match]

            velocity_match *= expansion_factor_ts**0.5
            velocity_match += - np.array([halo_vx, halo_vy, halo_vz])

            temperature_match = np.zeros(len(id_part_track))+np.nan
            temperature_match[ok_match] = temperature_gas_list_all_ts[ptr][ok_match]
            internal_energy_match = np.zeros(len(id_part_track))+np.nan
            internal_energy_match[ok_match] = internal_energy_gas_list_all_ts[ptr][ok_match]

            coordinates_track.append(coordinates_match)
            velocity_track.append(velocity_match)
            temperature_track.append(temperature_match)
            internal_energy_track.append(internal_energy_match)

            redshift_list.append(redshift_ts)
            vmax_list.append(halo_vmax)

            if visualize_winds:

                # Work out if tracked particles are star-forming or not
                metallicity_match = metallicity_gas_list_all_ts[ptr][ok_match]
                density_match = density_gas_list_all_ts[ptr][ok_match]
                temperature_match = temperature_gas_list_all_ts[ptr][ok_match]
                xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
                nh_match = density_match * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
                nh_thresh_match = np.min( (np.zeros_like(nh_match)+10.0 , 0.1 * (metallicity_match/0.002)**-0.64), axis=0)
                # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
                T_eos = 8e3 * (nh_match/0.1)**(4/3.)
                SF_match = (nh_match > nh_thresh) & (np.log10(temperature_match) < np.log10(T_eos) +0.5)

                flag1 = np.zeros_like(id_part_track)-1
                # Flag non-SF ISM particles
                if len(id_part_track)>0 and len(id_gas[ISM])>0:
                    ptr = ms.match(id_part_track, id_gas[ISM])
                    ISM_match = ptr>=0

                    flag1[ISM_match] = 3

                flag1_match = flag1[ok_match]
                flag1_match[SF_match] = 0
                flag1_match[(SF_match==False)&(flag1_match!=3)] = 1
                flag1[ok_match] = flag1_match

                flag1[ok_match2] = 2

                flag1_track.append(flag1)

            else:
                # Halo ejecta tracking
                # 0 = bound gas, 1 = not bound, 2 = star (bound or unbound)
                
                group_number_part_track_ts = np.zeros_like(id_part_track) -1
                group_number_part_track_ts[ok_match] = group_number_gas_list_all_ts[ptr][ok_match]
                subgroup_number_part_track_ts = np.zeros_like(id_part_track) -1
                subgroup_number_part_track_ts[ok_match] = subgroup_number_gas_list_all_ts[ptr][ok_match]

                flag1_ts = np.ones_like(id_part_track)
                flag1_ts[(group_number_part_track_ts==halo_group_number)&(subgroup_number_part_track_ts==halo_subgroup_number)] = 0
                flag1_ts[ok_match2] = 2

                flag1_track.append(flag1_ts)

        if i_halo == ih_choose and init_tracking and snap_ts == snap_choose:
            coordinates_track = []
            velocity_track = []
            temperature_track = []
            internal_energy_track = []
            flag1_track = []
            select_wind_track = []

        ###### Prune the particle lists ############ (this is just to speed up the selection of particles next time through the loop)
        group_number_gas_list_ts = group_number_gas_list_ts[select_gas==False]
        subgroup_number_gas_list_ts = subgroup_number_gas_list_ts[select_gas==False]
        id_gas_list_ts = id_gas_list_ts[select_gas==False]
        mass_gas_list_ts = mass_gas_list_ts[select_gas==False]
        metallicity_gas_list_ts = metallicity_gas_list_ts[select_gas==False]
        density_gas_list_ts = density_gas_list_ts[select_gas==False]
        coordinates_gas_list_ts = coordinates_gas_list_ts[select_gas==False]
        velocity_gas_list_ts = velocity_gas_list_ts[select_gas==False]
        temperature_gas_list_ts = temperature_gas_list_ts[select_gas==False]
        internal_energy_gas_list_ts = internal_energy_gas_list_ts[select_gas==False]

        group_number_dm_list_ts = group_number_dm_list_ts[select_dm==False]
        subgroup_number_dm_list_ts = subgroup_number_dm_list_ts[select_dm==False]
        coordinates_dm_list_ts = coordinates_dm_list_ts[select_dm==False]

        group_number_star_list_ts = group_number_star_list_ts[select_star==False]
        subgroup_number_star_list_ts = subgroup_number_star_list_ts[select_star==False]
        id_star_list_ts = id_star_list_ts[select_star==False]
        mass_star_list_ts = mass_star_list_ts[select_star==False]
        mass_star_init_list_ts = mass_star_init_list_ts[select_star==False]
        aform_star_list_ts = aform_star_list_ts[select_star==False]
        coordinates_star_list_ts = coordinates_star_list_ts[select_star==False]

        group_number_bh_list_ts = group_number_bh_list_ts[select_bh==False]
        subgroup_number_bh_list_ts = subgroup_number_bh_list_ts[select_bh==False]
        mass_bh_list_ts = mass_bh_list_ts[select_bh==False]
        coordinates_bh_list_ts = coordinates_bh_list_ts[select_bh==False]
         
        group_number_gas_list_ps = group_number_gas_list_ps[select_gas_progen==False]
        subgroup_number_gas_list_ps = subgroup_number_gas_list_ps[select_gas_progen==False]
        id_gas_list_ps = id_gas_list_ps[select_gas_progen==False]
        mass_gas_list_ps = mass_gas_list_ps[select_gas_progen==False]
        metallicity_gas_list_ps = metallicity_gas_list_ps[select_gas_progen==False]
        temperature_gas_list_ps = temperature_gas_list_ps[select_gas_progen==False]
        density_gas_list_ps = density_gas_list_ps[select_gas_progen==False]
        coordinates_gas_list_ps = coordinates_gas_list_ps[select_gas_progen==False]
        velocity_gas_list_ps = velocity_gas_list_ps[select_gas_progen==False]
        temperature_gas_list_ps = temperature_gas_list_ps[select_gas_progen==False]
        internal_energy_gas_list_ps = internal_energy_gas_list_ps[select_gas_progen==False]

        group_number_dm_list_ps = group_number_dm_list_ps[select_dm_progen==False]
        subgroup_number_dm_list_ps = subgroup_number_dm_list_ps[select_dm_progen==False]
        coordinates_dm_list_ps = coordinates_dm_list_ps[select_dm_progen==False]

        group_number_star_list_ps = group_number_star_list_ps[select_star_progen==False]
        subgroup_number_star_list_ps = subgroup_number_star_list_ps[select_star_progen==False]
        id_star_list_ps = id_star_list_ps[select_star_progen==False]
        mass_star_list_ps = mass_star_list_ps[select_star_progen==False]
        mass_star_init_list_ps = mass_star_init_list_ps[select_star_progen==False]

        group_number_bh_list_ps = group_number_bh_list_ps[select_bh_progen==False]
        subgroup_number_bh_list_ps = subgroup_number_bh_list_ps[select_bh_progen==False]
        mass_bh_list_ps = mass_bh_list_ps[select_bh_progen==False]
        coordinates_bh_list_ps = coordinates_bh_list_ps[select_bh_progen==False]


        ########################## Do the rate measurements #####################################

        ###### Identify baryonic (re)accretion (also include contribution to cooling, SF & stellar recycling) #################
        if len(id_star_gas_all_progen) > 0 and len(id_star_gas) > 0:
            ptr_accrete = ms.match(id_star_gas, id_star_gas_all_progen)
            ok_match = ptr_accrete >= 0
            
            accreted = ok_match == False

            # Cross-match accreted gas against ejected particles to compute gas return rate
            if len(id_star_gas[accreted]) > 0 and len(id_gas_ejecta_fof_progen_list)>0:
                ptr = ms.match(id_star_gas[accreted], id_gas_ejecta_fof_progen_list)
                reaccreted = np.copy(accreted)
                reaccreted[accreted] = ptr >= 0

            else:
                reaccreted = np.zeros_like(accreted) < 0

            # Remove returned particles from ejected particle lists of subhaloes inside this FoF group
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_ejecta_list_ts[i_sub]) > 0 and len(id_star_gas[reaccreted])>0:
                    ptr = ms.match(np.array(id_gas_ejecta_list_ts[i_sub]), id_star_gas[reaccreted])
                    id_gas_ejecta_list_ts[i_sub] = np.array(id_gas_ejecta_list_ts[i_sub])[ptr<0]



            # Contribution of accreted stars that formed between now and the previous timestep to (mass) accretion, cooling, SF and stellar mass loss
            accreted_star_SF = accreted[fam_star_gas==1] & fresh_star

            # Compute contribution from previously ejected particles (from ISM) to cooling rate
            # Note, the total cooling, n_cooled, includes this part - so don't double count in analysis
            if len(id_star[accreted_star_SF]) > 0 and len(id_gas_wind_list_ts[i_halo])>0:
                ptr = ms.match(id_star[accreted_star_SF], id_gas_wind_list_ts[i_halo])
                recooled = ptr >= 0
            else:
                recooled = np.zeros_like(id_star[accreted_star_SF])< 0
            
            # Remove returned particles from ejected particle lists
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_wind_list_ts[i_sub]) > 0 and len(id_star[accreted_star_SF][recooled])>0:
                    ptr = ms.match(np.array(id_gas_wind_list_ts[i_sub]), id_star[accreted_star_SF][recooled])
                    id_gas_wind_list_ts[i_sub] = np.array(id_gas_wind_list_ts[i_sub])[ptr<0]


            # Contribution of accreted gas to (mass) accretion, reaccretion & cooling
            accreted_gas = accreted[fam_star_gas==2]
            reaccreted_gas = accreted_gas & reaccreted[fam_star_gas==2]

            if len(id_gas[accreted_gas&SF]) > 0 and len(id_gas_wind_list_ts[i_halo])>0:
                ptr = ms.match(id_gas[accreted_gas&SF], id_gas_wind_list_ts[i_halo])
                recooled = ptr >= 0
            else:
                recooled = np.zeros_like(id_gas[accreted_gas&SF])< 0
                            
            # Remove returned particles from wind particle lists
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_wind_list_ts[i_sub]) > 0 and len(id_gas[accreted_gas&SF][recooled])>0:
                    ptr = ms.match(np.array(id_gas_wind_list_ts[i_sub]), id_gas[accreted_gas&SF][recooled])
                    id_gas_wind_list_ts[i_sub] = np.array(id_gas_wind_list_ts[i_sub])[ptr<0]

        else:
            ptr_accrete = np.array([])

        ###### Identify particles that have joined the star-forming reservoir from the diffuse gas halo reservoir
        if len(id_gas[SF]) > 0 and len(id_gas_progen[(SF_progen==False)&(ISM_progen==False)]) > 0:
            ptr = ms.match(id_gas[SF], id_gas_progen[(SF_progen==False)&(ISM_progen==False)])
            ok_match = ptr >= 0

            cooled = ok_match

            if len(id_gas[SF][cooled]) > 0 and len(id_gas_wind_list_ts[i_halo])>0:
                ptr = ms.match(id_gas[SF][cooled], id_gas_wind_list_ts[i_halo])
                recooled = ptr >= 0
            else:
                recooled = np.zeros_like(id_gas[SF][cooled])< 0
                
            
            # Remove returned particles from ejected particle lists
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_wind_list_ts[i_sub]) > 0 and len(id_gas[SF][cooled][recooled])>0:
                    ptr = ms.match(np.array(id_gas_wind_list_ts[i_sub]), id_gas[SF][cooled][recooled])
                    id_gas_wind_list_ts[i_sub] = np.array(id_gas_wind_list_ts[i_sub])[ptr<0]

                if len(id_gas_ism_reheat_list_ts[i_sub]) > 0 and len(id_gas[SF][cooled][recooled])>0:
                    ptr = ms.match(np.array(id_gas_ism_reheat_list_ts[i_sub]), id_gas[SF][cooled][recooled])
                    id_gas_ism_reheat_list_ts[i_sub] = np.array(id_gas_ism_reheat_list_ts[i_sub])[ptr<0]

        ###### Identify particles that have joined the non-star-forming ISM
        if len(id_gas[(SF==False)&ISM]) > 0 and len(id_gas_progen[(ISM_progen==False)&(SF_progen==False)]) > 0:
            ptr = ms.match(id_gas[(SF==False)&ISM], id_gas_progen[(ISM_progen==False)&(SF_progen==False)])
            ok_match = ptr >= 0

            cooled = ok_match

            if len(id_gas[(SF==False)&ISM][cooled]) > 0 and len(id_gas_wind_list_ts[i_halo])>0:
                ptr = ms.match(id_gas[(SF==False)&ISM][cooled], id_gas_wind_list_ts[i_halo])
                recooled = ptr >= 0
            else:
                recooled = np.zeros_like(id_gas[(SF==False)&ISM][cooled])< 0
                
            
            # Remove returned particles from ejected particle lists
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_wind_list_ts[i_sub]) > 0 and len(id_gas[(SF==False)&ISM][cooled][recooled])>0:
                    ptr = ms.match(np.array(id_gas_wind_list_ts[i_sub]), id_gas[(SF==False)&ISM][cooled][recooled])
                    id_gas_wind_list_ts[i_sub] = np.array(id_gas_wind_list_ts[i_sub])[ptr<0]

                if len(id_gas_ism_reheat_list_ts[i_sub]) > 0 and len(id_gas[(SF==False)&ISM][cooled][recooled])>0:
                    ptr = ms.match(np.array(id_gas_ism_reheat_list_ts[i_sub]), id_gas[(SF==False)&ISM][cooled][recooled])
                    id_gas_ism_reheat_list_ts[i_sub] = np.array(id_gas_ism_reheat_list_ts[i_sub])[ptr<0]


        ##### Identify particles which have left the star-forming reservoir and joined the diffuse gas halo reservoir
        if len(id_gas[(SF==False)&(ISM==False)]) > 0 and len(id_gas_progen[SF_progen]) > 0:
            ptr = ms.match(id_gas[(SF==False)&(ISM==False)], id_gas_progen[SF_progen])
            ok_match = ptr >= 0

            reheated = ok_match

            # Decide which reheated particles join the wind
            t_list = [t_ps, t_ts, t_ns]
            use_ns = 2

            flag_ps = np.zeros_like(id_gas[(SF==False)&(ISM==False)][ok_match]) # 0 = SF, 1 = NSF, 2 = star
            flag_ts = np.ones_like(flag_ps)

            coordinates_gas_reheat_ns = np.zeros_like(coordinates_gas[(SF==False)&(ISM==False)][reheated])
            temperature_gas_reheat_ns = np.zeros_like(mass_gas[(SF==False)&(ISM==False)][reheated])

            
            flag_ns = np.ones_like(flag_ps)
            if len(id_gas[(SF==False)&(ISM==False)][ok_match]) > 0 and len(id_gas_fof_ns)> 0:
                ptr_ns = ms.match(id_gas[(SF==False)&(ISM==False)][ok_match], id_gas_fof_ns)
                ok_match_ns = ptr_ns >=0

                temp = flag_ns[ok_match_ns]
                temp[SF_fof_ns[ptr_ns][ok_match_ns]] = 0
                flag_ns[ok_match_ns] = temp

                coordinates_gas_reheat_ns[ok_match_ns] = coordinates_gas_fof_ns[ptr_ns][ok_match_ns]
                temperature_gas_reheat_ns[ok_match_ns] = temperature_gas_fof_ns[ptr_ns][ok_match_ns]

            elif len(id_gas[(SF==False)&(ISM==False)][ok_match]) == 0:
                ptr_ns = np.zeros(0).astype("int")
                ok_match_ns = np.zeros(0)<0
            else:
                ptr_ns = np.zeros(len(id_gas[(SF==False)&(ISM==False)][ok_match])).astype("int")-1
                ok_match_ns = np.zeros_like(ptr_ns)<0

            if len(id_gas[(SF==False)&(ISM==False)][ok_match]) > 0 and len(id_star_fof_ns) > 0:
                ptr_ns = ms.match(id_gas[(SF==False)&(ISM==False)][ok_match], id_star_fof_ns)
                ok_match_ns2 = ptr_ns >=0
                flag_ns[ok_match_ns2] = 2
            else:
                ok_match_ns2 = np.zeros_like(id_gas[(SF==False)&(ISM==False)][ok_match])<0

            # Set coordinates of particles that can't be found in bound particles of descendants FoF group to a very large number (i.e. they are definitely in the galaxy wind)
            coordinates_gas_reheat_ns[(ok_match_ns==False) & (ok_match_ns2==False)] += 2e9
                
            flag_list = np.array([flag_ps, flag_ts, flag_ns])
            coordinates_gas_reheat_list = np.array([coordinates_gas_progen[SF_progen][ptr][ok_match], coordinates_gas[(SF==False)&(ISM==False)][ok_match], coordinates_gas_reheat_ns])
            temperature_gas_reheat_list = np.array([temperature_gas_progen[SF_progen][ptr][ok_match], temperature_gas[(SF==False)&(ISM==False)][ok_match], temperature_gas_reheat_ns])

            select_wind, delta_t_thr, vmax_cut, vmax_cut2 = up.Vmax_wind_selection(coordinates_gas_reheat_list, cv_gas[(SF==False)&(ISM==False)][ok_match], temperature_gas_reheat_list, t_list, halo_vmax, halo_r200_host* 1e3 /h / (1+redshift_ts), True, use_ns, flag_list)

            # Add ejected particles that pass wind test (including their mass) to the ejected particle lists
            id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), id_gas[(SF==False)&(ISM==False)][reheated][select_wind])

            id_gas_ism_reheat_list_ts[i_halo] = np.append(np.array(id_gas_ism_reheat_list_ts[i_halo]), id_gas[(SF==False)&(ISM==False)][reheated][select_wind==False])
            

            if i_halo == ih_choose:
                if init_tracking and visualize_winds and snap_ts == snap_choose:

                    id_part_track = id_gas_progen[SF_progen][ptr][ok_match]
                    use_ns_track = d_snap_ns+1

                    velocity_track_ps = np.copy(velocity_gas_progen[SF_progen][ptr][ok_match])
                    velocity_track_ts = np.copy(velocity_gas[(SF==False)&(ISM==False)][reheated])
                    
                    temperature_track_ps = np.copy(temperature_gas_progen[SF_progen][ptr][ok_match])
                    temperature_track_ts = np.copy(temperature_gas[(SF==False)&(ISM==False)][reheated])
                    internal_energy_track_ps = np.copy(internal_energy_gas_progen[SF_progen][ptr][ok_match])
                    internal_energy_track_ts = np.copy(internal_energy_gas[(SF==False)&(ISM==False)][reheated])

                    coordinates_track_ps = np.copy(coordinates_gas_progen[SF_progen][ptr][ok_match])
                    coordinates_track_ts = np.copy(coordinates_gas[(SF==False)&(ISM==False)][reheated])

                    select_wind_track = np.copy(select_wind)

                    coordinates_track.append(coordinates_track_ps)
                    coordinates_track.append(coordinates_track_ts)
                    
                    velocity_track.append(velocity_track_ps)
                    velocity_track.append(velocity_track_ts)
                    temperature_track.append(temperature_track_ps)
                    temperature_track.append(temperature_track_ts)
                    internal_energy_track.append(internal_energy_track_ps)
                    internal_energy_track.append(internal_energy_track_ts)

                    redshift_list = [redshift_ps, redshift_ts]
                    # Note I'm working off assumption that hdf5 metadata is correct in saying vmax is comoving - the sqrt(a) converts into proper units
                    vmax_list = [halo_vmax_progen, halo_vmax]

                    # Flag to keep track of fate of particle
                    flag1_track.append(np.zeros_like(id_part_track)) # 0 = SF, 1 = NSF, 2 = star, 3 = non-SF ISM
                    flag1_ts = np.ones_like(id_part_track)

                    ptr = ms.match(id_part_track, id_gas[ISM])
                    flag1_ts[ptr>=0] = 3

                    flag1_track.append(flag1_ts)

                    dummy1 = len(flag1_ts)

        elif i_halo == ih_choose and init_tracking and visualize_winds and snap_ts == snap_choose:

            id_part_track = np.array([])
            coordinates_track.append(np.zeros((0,3)))
            coordinates_track.append(np.zeros((0,3)))
            velocity_track.append(np.zeros((0,3)))
            velocity_track.append(np.zeros((0,3)))
            temperature_track.append( np.array([]))
            select_wind_track.append(np.array([]))
            internal_energy_track.append( np.array([]))
            redshift_list = [redshift_ps, redshift_ts]
            vmax_list = [halo_vmax_progen, halo_vmax]
            
            flag1_track.append(np.zeros_like(id_part_track)) # 0 = SF, 1 = NSF, 2 = star, 3 = Out
            flag1_track.append(np.ones_like(id_part_track))

        ##### Identify particles which have left the non-star-forming ISM and joined the diffuse gas halo reservoir
        if len(id_gas[(SF==False)&(ISM==False)]) > 0 and len(id_gas_progen[(SF_progen==False)&ISM_progen]) > 0:
            ptr = ms.match(id_gas[(SF==False)&(ISM==False)], id_gas_progen[(SF_progen==False)&ISM_progen])
            ok_match = ptr >= 0

            x_part = coordinates_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match][:,0]
            y_part = coordinates_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match][:,1]
            z_part = coordinates_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match][:,2]
            
            # Check that these particles are not already in the wind for this subhalo
            if len(id_gas_wind_list_ts[i_halo]) > 0:
                ptr_check = ms.match(id_gas[(SF==False)&(ISM==False)], id_gas_wind_list_ts[i_halo])
                ok_match = ok_match & (ptr_check<0)

            reheated_nsf = ok_match

            # Decide which reheated particles join the wind
            t_list = [t_ps, t_ts, t_ns]
            use_ns = 2

            flag_ps = np.zeros_like(id_gas[(SF==False)&(ISM==False)][ok_match]) # 0 = SF, 1 = NSF, 2 = star
            flag_ts = np.ones_like(flag_ps)
            
            flag_ns = np.ones_like(flag_ps)
            if len( id_gas_fof_ns)>0:
                ptr_ns = ms.match(id_gas[(SF==False)&(ISM==False)][ok_match], id_gas_fof_ns)
                ok_match_ns = ptr_ns >=0
                temp = flag_ns[ok_match_ns]
                temp[SF_fof_ns[ptr_ns][ok_match_ns]] = 0
                flag_ns[ok_match_ns] = temp

                coordinates_gas_reheat_ns = np.zeros_like(coordinates_gas[(SF==False)&(ISM==False)][reheated_nsf])
                coordinates_gas_reheat_ns[ok_match_ns] = coordinates_gas_fof_ns[ptr_ns][ok_match_ns]

                temperature_gas_reheat_ns = np.zeros_like(mass_gas[(SF==False)&(ISM==False)][reheated_nsf])
                temperature_gas_reheat_ns[ok_match_ns] = temperature_gas_fof_ns[ptr_ns][ok_match_ns]

                ptr_ns = ms.match(id_gas[(SF==False)&(ISM==False)][ok_match], id_star_fof_ns)
                ok_match_ns2 = ptr_ns >=0
                flag_ns[ok_match_ns2] = 2
                

                # Set coordinates of particles that can't be found in bound particles of descendants FoF group to a very large number (i.e. they are definitely in the galaxy wind)
                coordinates_gas_reheat_ns[(ok_match_ns==False) & (ok_match_ns2==False)] += 2e9
                
                flag_list = np.array([flag_ps, flag_ts, flag_ns])
                coordinates_gas_reheat_list = np.array([coordinates_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match], coordinates_gas[(SF==False)&(ISM==False)][ok_match], coordinates_gas_reheat_ns])
                temperature_gas_reheat_list = np.array([temperature_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match], temperature_gas[(SF==False)&(ISM==False)][ok_match], temperature_gas_reheat_ns])

                select_wind, delta_t_thr, vmax_cut, vmax_cut2 = up.Vmax_wind_selection(coordinates_gas_reheat_list, cv_gas[(SF==False)&(ISM==False)][ok_match], temperature_gas_reheat_list, t_list, halo_vmax, halo_r200_host* 1e3 /h / (1+redshift_ts), True, use_ns, flag_list)

                jimbo = True
                if i_halo == ih_choose and jimbo:

                    if init_tracking and visualize_winds and snap_ts == snap_choose:
                        id_part_track2 = id_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match]

                        coordinates_track_ps2 = np.copy(coordinates_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match])
                        velocity_track_ps2 = np.copy(velocity_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match])
                        temperature_track_ps2 = np.copy(temperature_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match])
                        internal_energy_track_ps2 = np.copy(internal_energy_gas_progen[(SF_progen==False)&ISM_progen][ptr][ok_match])

                        coordinates_track_ts2 = np.copy(coordinates_gas[(SF==False)&(ISM==False)][reheated_nsf])
                        velocity_track_ts2 = np.copy(velocity_gas[(SF==False)&(ISM==False)][reheated_nsf])
                        temperature_track_ts2 = np.copy(temperature_gas[(SF==False)&(ISM==False)][reheated_nsf])
                        internal_energy_track_ts2 = np.copy(internal_energy_gas[(SF==False)&(ISM==False)][reheated_nsf])

                        coordinates_track[0] = np.append(coordinates_track[0],coordinates_track_ps2,axis=0)
                        coordinates_track[1] = np.append(coordinates_track[1],coordinates_track_ts2,axis=0)

                        velocity_track[0] = np.append(velocity_track[0],velocity_track_ps2,axis=0)
                        velocity_track[1] = np.append(velocity_track[1],velocity_track_ts2,axis=0)

                        temperature_track[0] = np.append(temperature_track[0],temperature_track_ps2)
                        temperature_track[1] = np.append(temperature_track[1],temperature_track_ts2)

                        select_wind_track = np.append(select_wind_track, select_wind)

                        internal_energy_track[0] = np.append(internal_energy_track[0],internal_energy_track_ps2)
                        internal_energy_track[1] = np.append(internal_energy_track[1],internal_energy_track_ts2)
                    
                        # Flag to keep track of fate of particle
                        flag1_track[0] = np.append(flag1_track[0], np.zeros_like(id_part_track2)+3) # 0 = SF, 1 = NSF NISM, 2 = star, 3 = NSF ISM
                        flag1_track[1] = np.append(flag1_track[1], np.zeros_like(id_part_track2)+1) # 0 = SF, 1 = NSF NISM, 2 = star, 3 = NSF ISM

                        dummy2 = len(id_part_track2)

                        id_part_track = np.append( id_part_track, id_part_track2)
                        coordinates_track_ps = np.append(coordinates_track_ps,coordinates_track_ps2,axis=0)
                        velocity_track_ps = np.append(velocity_track_ps, velocity_track_ps2,axis=0)

                    # Add ejected particles (including their mass) to the ejected particle lists
                    id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), id_gas[(SF==False)&(ISM==False)][reheated_nsf][select_wind])

                    id_gas_ism_reheat_list_ts[i_halo] = np.append(np.array(id_gas_ism_reheat_list_ts[i_halo]), id_gas[(SF==False)&(ISM==False)][reheated_nsf][select_wind==False])






        ##### Identify particles which have left the reheated ISM list and rejoined the ISM or the SF gas, or become a star
        # Note this block is basically a safety double-check, we've been removing reheated particles that are found in the ISM/stars as we go through already
        if len(np.append(id_gas[SF|ISM],id_star)) > 0 and len(id_gas_ism_reheat_list_ts[i_halo]) > 0:
            ptr = ms.match(np.append(id_gas[SF|ISM],id_star), id_gas_ism_reheat_list_ts[i_halo])
            ok_match = ptr >= 0

            # Remove returned particles from reheated ism lists (note in principle this introduces a bit of order dependence within the loop but the sorting means it shouldn't be significant)
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_ism_reheat_list_ts[i_sub]) > 0 and len(np.append(id_gas[SF|ISM],id_star)[ok_match])>0:
                    ptr = ms.match(np.array(id_gas_ism_reheat_list_ts[i_sub]), np.append(id_gas[SF|ISM],id_star)[ok_match])
                    id_gas_ism_reheat_list_ts[i_sub] = np.array(id_gas_ism_reheat_list_ts[i_sub])[ptr<0]



        ##### Identify particles which have left the reheated ISM list and joined the wind (those that are still bound on this step)
        if len(id_gas[(SF==False)&(ISM==False)]) > 0 and len(id_gas_ism_reheat_list_ts[i_halo]) > 0:
            ptr = ms.match(id_gas_ism_reheat_list_ts[i_halo],id_gas[(SF==False)&(ISM==False)])
            ok_match = ptr >= 0

            # Decide which reheated particles join the wind
            t_list = [t_ps, t_ts, t_ns]

            # Find positions of the reheated particles on the previous step
            coordinates_gas_ism_reheat_ps = np.zeros((len(id_gas_ism_reheat_list_ts[i_halo][ok_match]),3))+1e9 #Assume those that can't be found are at infinity                                                                  
            temperature_gas_ism_reheat_ps = np.zeros_like(id_gas_ism_reheat_list_ts[i_halo][ok_match])
            if len(id_gas_list_all_ps)>0 and len(id_gas_ism_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ps = ms.match(id_gas_ism_reheat_list_ts[i_halo][ok_match],id_gas_list_all_ps)
                ok_match_ps = ptr_ps >= 0

                temp = coordinates_gas_list_all_ps[ptr_ps][ok_match_ps]
                coordinates_halo = [halo_x_progen, halo_y_progen, halo_z_progen]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
                coordinates_gas_ism_reheat_ps[ok_match_ps] = temp

                temperature_gas_ism_reheat_ps[ok_match_ps] = temperature_gas_list_all_ps[ptr_ps][ok_match_ps]

            # Find positions of the reheated particles on the current step 
            coordinates_gas_ism_reheat_ts = np.zeros((len(id_gas_ism_reheat_list_ts[i_halo][ok_match]),3))+2e9 #Assume those that can't be found are at infinity
            temperature_gas_ism_reheat_ts = np.zeros(len(id_gas_ism_reheat_list_ts[i_halo][ok_match]))
            cv_gas_ism_reheat_ts = np.zeros(len(id_gas_ism_reheat_list_ts[i_halo][ok_match]))

            if len(id_gas_list_all_ts)>0 and len(id_gas_ism_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ts = ms.match(id_gas_ism_reheat_list_ts[i_halo][ok_match],id_gas_list_all_ts)
                ok_match_ts = ptr_ts >= 0
                
                temp = coordinates_gas_list_all_ts[ptr_ts][ok_match_ts]
                coordinates_halo = [halo_x, halo_y, halo_z]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
                coordinates_gas_ism_reheat_ts[ok_match_ts] = temp

                temp2 = velocity_gas_list_all_ts[ptr_ts][ok_match_ts]
                temp2 *= expansion_factor_ts**0.5 # pkms
                temp2 += - np.array([halo_vx, halo_vy, halo_vz])
                radius_temp = np.sqrt(np.square(temp[:,0])+np.square(temp[:,1])+np.square(temp[:,2]))
                coordinates_gas_ism_reheat_ts[ok_match_ts]
                r_norm_temp = np.swapaxes(np.array([temp[:,0],temp[:,1],temp[:,2]]) / radius_temp,0,1)
                cv_temp = np.sum(r_norm_temp * temp2,axis=1)
                cv_gas_ism_reheat_ts[ok_match_ts] = cv_temp

                temperature_gas_ism_reheat_ts[ok_match_ts] = temperature_gas_list_all_ts[ptr_ts][ok_match_ts]

            # Find positions of the reheated particles on the next step
            coordinates_gas_ism_reheat_ns = np.zeros((len(id_gas_ism_reheat_list_ts[i_halo][ok_match]),3))+3e9 #Assume those that can't be found are at infinity                                                                  
            temperature_gas_ism_reheat_ns = np.zeros_like(id_gas_ism_reheat_list_ts[i_halo][ok_match])
            if len(id_gas_list_all_ns)>0 and len(id_gas_ism_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ns = ms.match(id_gas_ism_reheat_list_ts[i_halo][ok_match],id_gas_list_all_ns)
                ok_match_ns = ptr_ns >= 0

                coordinates_gas_ism_reheat_ns[ok_match_ns] = coordinates_gas_list_all_ns[ptr_ns][ok_match_ns]

                temp = coordinates_gas_list_all_ns[ptr_ns][ok_match_ns]
                coordinates_halo = [halo_x_ns, halo_y_ns, halo_z_ns]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ns) # pkpc
                coordinates_gas_ism_reheat_ns[ok_match_ns] = temp

                temperature_gas_ism_reheat_ns[ok_match_ns] = temperature_gas_list_all_ns[ptr_ns][ok_match_ns]
            if len(id_star_list_all_ns)>0 and len(id_gas_ism_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ns_star = ms.match(id_gas_ism_reheat_list_ts[i_halo][ok_match],id_star_list_all_ns)
                ok_match_ns_star = ptr_ns_star >= 0
                
                coordinates_gas_ism_reheat_ns[ok_match_ns_star] *= 0

            # These are just dummy values that shouldn't do anything in this case
            flag_ps = np.zeros_like(id_gas_ism_reheat_list_ts[i_halo][ok_match])
            flag_ts = np.ones_like(flag_ps)
            flag_ns = np.ones_like(flag_ps)
            flag_list = np.array([flag_ps, flag_ts, flag_ns])

            coordinates_gas_reheat_list = np.array([coordinates_gas_ism_reheat_ps, coordinates_gas_ism_reheat_ts, coordinates_gas_ism_reheat_ns])

            temperature_gas_reheat_list = np.array([temperature_gas_ism_reheat_ps, temperature_gas_ism_reheat_ts, temperature_gas_ism_reheat_ns])

            select_wind, delta_t_thr, vmax_cut, vmax_cut2 = up.Vmax_wind_selection(coordinates_gas_reheat_list, cv_gas_ism_reheat_ts, temperature_gas_reheat_list, t_list, halo_vmax, halo_r200_host* 1e3 /h / (1+redshift_ts), True, use_ns, flag_list)

            # Add ejected particles (including their mass) to the ejected particle lists 
            id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), id_gas_ism_reheat_list_ts[i_halo][ok_match][select_wind])

            # This doesn't work well because Rmax in plotting scipt can't handle the late wind inclusions
            '''# Tracking block - has to go before below block
            if i_halo == ih_choose:
                if visualize_winds and snap_ts > snap_choose:
                    if len(id_part_track)>0 and len(id_gas_ism_reheat_list_ts[i_halo][ok_match][select_wind])>0:
                        ptr = ms.match(id_part_track, id_gas_ism_reheat_list_ts[i_halo][ok_match][select_wind])
                        ok_match_track = ptr>=0
                        select_wind_track[ok_match_track] = True'''
        
            # Remove these particles from reheated ism lists
            id_temp = np.copy(id_gas_ism_reheat_list_ts[i_halo][ok_match][select_wind])
            select_sub = np.where(group_number_list == halo_group_number)[0]

            for i_sub in select_sub:
                if len(id_gas_ism_reheat_list_ts[i_sub]) > 0 and len(id_temp)>0:
                    ptr = ms.match(np.array(id_gas_ism_reheat_list_ts[i_sub]),id_temp)
                    id_gas_ism_reheat_list_ts[i_sub] = np.array(id_gas_ism_reheat_list_ts[i_sub])[ptr<0]


        ##### Identify particles which have left the reheated halo list and rejoined the FoF group 
        if len(id_star_gas_as) > 0 and len(id_gas_halo_reheat_list_ts[i_halo]) > 0:
            ptr = ms.match(id_star_gas_as, id_gas_halo_reheat_list_ts[i_halo])
            ok_match = ptr >= 0
 
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_halo_reheat_list_ts[i_sub]) > 0 and len(id_star_gas_as[ok_match])>0:
                    ptr = ms.match(np.array(id_gas_halo_reheat_list_ts[i_sub]), id_star_gas_as[ok_match])
                    id_gas_halo_reheat_list_ts[i_sub] = np.array(id_gas_halo_reheat_list_ts[i_sub])[ptr<0]


        ##### Identify unbound particles which have left the reheated halo list and joined the halo wind
        if len(id_star_gas_as) > 0 and len(id_gas_halo_reheat_list_ts[i_halo]) > 0:
            ptr = ms.match(id_gas_halo_reheat_list_ts[i_halo],id_star_gas_as) # This is just to check the particles didn't sneak back into the halo somehow
            ok_match = ptr < 0

            # Decide which reheated particles join the wind
            t_list = [t_ps, t_ts, t_ns]

            # Find positions of the reheated particles on the previous step
            coordinates_gas_halo_reheat_ps = np.zeros((len(id_gas_halo_reheat_list_ts[i_halo][ok_match]),3))+1e9 #Assume those that can't be found are at infinity
            temperature_gas_halo_reheat_ps = np.zeros_like(id_gas_halo_reheat_list_ts[i_halo][ok_match])
            if len(id_gas_list_all_ps)>0 and len(id_gas_halo_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ps = ms.match(id_gas_halo_reheat_list_ts[i_halo][ok_match],id_gas_list_all_ps)
                ok_match_ps = ptr_ps >= 0

                temp = coordinates_gas_list_all_ps[ptr_ps][ok_match_ps]
                coordinates_halo = [halo_x_progen, halo_y_progen, halo_z_progen]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ps) # pkpc
                coordinates_gas_halo_reheat_ps[ok_match_ps] = temp

                temperature_gas_halo_reheat_ps[ok_match_ps] = temperature_gas_list_all_ps[ptr_ps][ok_match_ps]

            # Find positions of the reheated particles on the current step 
            coordinates_gas_halo_reheat_ts = np.zeros((len(id_gas_halo_reheat_list_ts[i_halo][ok_match]),3))+2e9 #Assume those that can't be found are at infinity
            temperature_gas_halo_reheat_ts = np.zeros(len(id_gas_halo_reheat_list_ts[i_halo][ok_match]))
            cv_gas_halo_reheat_ts = np.zeros(len(id_gas_halo_reheat_list_ts[i_halo][ok_match]))

            if len(id_gas_list_all_ts)>0 and len(id_gas_halo_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ts = ms.match(id_gas_halo_reheat_list_ts[i_halo][ok_match],id_gas_list_all_ts)
                ok_match_ts = ptr_ts >= 0
                
                temp = coordinates_gas_list_all_ts[ptr_ts][ok_match_ts]
                coordinates_halo = [halo_x, halo_y, halo_z]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
                coordinates_gas_halo_reheat_ts[ok_match_ts] = temp

                temp2 = velocity_gas_list_all_ts[ptr_ts][ok_match_ts]
                temp2 *= expansion_factor_ts**0.5 # pkms
                temp2 += - np.array([halo_vx, halo_vy, halo_vz])
                radius_temp = np.sqrt(np.square(temp[:,0])+np.square(temp[:,1])+np.square(temp[:,2]))
                coordinates_gas_halo_reheat_ts[ok_match_ts]
                r_norm_temp = np.swapaxes(np.array([temp[:,0],temp[:,1],temp[:,2]]) / radius_temp,0,1)
                cv_temp = np.sum(r_norm_temp * temp2,axis=1)
                cv_gas_halo_reheat_ts[ok_match_ts] = cv_temp

                temperature_gas_halo_reheat_ts[ok_match_ts] = temperature_gas_list_all_ts[ptr_ts][ok_match_ts]

            # Find positions of the reheated particles on the next step
            coordinates_gas_halo_reheat_ns = np.zeros((len(id_gas_halo_reheat_list_ts[i_halo][ok_match]),3))+3e9 #Assume those that can't be found are at infinity                                                                  
            temperature_gas_halo_reheat_ns = np.zeros_like(id_gas_halo_reheat_list_ts[i_halo][ok_match])
            if len(id_gas_list_all_ns)>0 and len(id_gas_halo_reheat_list_ts[i_halo][ok_match])>0:
                ptr_ns = ms.match(id_gas_halo_reheat_list_ts[i_halo][ok_match],id_gas_list_all_ns)
                ok_match_ns = ptr_ns >= 0

                coordinates_gas_halo_reheat_ns[ok_match_ns] = coordinates_gas_list_all_ns[ptr_ns][ok_match_ns]

                temp = coordinates_gas_list_all_ns[ptr_ns][ok_match_ns]
                coordinates_halo = [halo_x_ns, halo_y_ns, halo_z_ns]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ns) # pkpc
                coordinates_gas_halo_reheat_ns[ok_match_ns] = temp

                temperature_gas_halo_reheat_ns[ok_match_ns] = temperature_gas_list_all_ns[ptr_ns][ok_match_ns]
            
            # These are just dummy values that shouldn't do anything in this case
            flag_ps = np.zeros_like(id_gas_halo_reheat_list_ts[i_halo][ok_match])
            flag_ts = np.ones_like(flag_ps)
            flag_ns = np.ones_like(flag_ps)
            flag_list = np.array([flag_ps, flag_ts, flag_ns])

            coordinates_gas_halo_reheat_list = np.array([coordinates_gas_halo_reheat_ps, coordinates_gas_halo_reheat_ts, coordinates_gas_halo_reheat_ns])

            temperature_gas_halo_reheat_list = np.array([temperature_gas_halo_reheat_ps, temperature_gas_halo_reheat_ts, temperature_gas_halo_reheat_ns])

            select_wind, delta_t_thr, vmax_cut, vmax_cut2 = up.Vmax_wind_selection(coordinates_gas_halo_reheat_list, cv_gas_halo_reheat_ts, temperature_gas_halo_reheat_list, t_list, halo_vmax, halo_r200_host* 1e3 /h / (1+redshift_ts), False, use_ns, flag_list)

            # Add ejected particles to the ejected particle lists
            id_gas_ejecta_list_ts[i_halo] = np.append(np.array(id_gas_ejecta_list_ts[i_halo]), id_gas_halo_reheat_list_ts[i_halo][ok_match][select_wind])

            # Tracking block - has to go before below block
            if i_halo == ih_choose:
                if not visualize_winds and snap_ts > snap_choose:
                    if len(id_part_track)>0 and len(id_gas_halo_reheat_list_ts[i_halo][ok_match][select_wind])>0:
                        ptr = ms.match(id_part_track, id_gas_halo_reheat_list_ts[i_halo][ok_match][select_wind])
                        ok_match_track = ptr>=0
                        select_wind_track[ok_match_track] = True


            # Remove selected particles from halo reheated list
            id_temp = np.copy(id_gas_halo_reheat_list_ts[i_halo][ok_match][select_wind])
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_halo_reheat_list_ts[i_sub]) > 0 and len(id_temp)>0:
                    ptr = ms.match(np.array(id_gas_halo_reheat_list_ts[i_sub]),id_temp)
                    id_gas_halo_reheat_list_ts[i_sub] = np.array(id_gas_halo_reheat_list_ts[i_sub])[ptr<0]


        ###### Identify gas progenitor particles which have been ejected entirely from the halo
        if len(id_gas_progen) > 0 and len(id_star_gas_as) > 0:
            ptr_eject_gas = ms.match(id_gas_progen, id_star_gas_as)
            ok_match = ptr_eject_gas >= 0

            ejected = ok_match == False

            # Decide which ejected particles join the halo wind
            t_list = [t_ps, t_ts, t_ns]

            # Find positions of the ejected particles on the current step 
            coordinates_gas_ejected_ts = np.zeros((len(id_gas_progen[ejected]),3))+2e9 #Assume those that can't be found are at infinity
            temperature_gas_ejected_ts = np.zeros(len(id_gas_progen[ejected]))
            cv_gas_ejected_ts = np.zeros(len(id_gas_progen[ejected]))

            if len(id_gas_list_all_ts)>0 and len(id_gas_progen[ejected])>0:
                ptr_ts = ms.match(id_gas_progen[ejected],id_gas_list_all_ts)
                ok_match_ts = ptr_ts >= 0
                
                temp = coordinates_gas_list_all_ts[ptr_ts][ok_match_ts]
                coordinates_halo = [halo_x, halo_y, halo_z]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
                coordinates_gas_ejected_ts[ok_match_ts] = temp

                temp2 = velocity_gas_list_all_ts[ptr_ts][ok_match_ts]
                temp2 *= expansion_factor_ts**0.5 # pkms
                temp2 += - np.array([halo_vx, halo_vy, halo_vz])
                radius_temp = np.sqrt(np.square(temp[:,0])+np.square(temp[:,1])+np.square(temp[:,2]))
                coordinates_gas_ejected_ts[ok_match_ts]
                r_norm_temp = np.swapaxes(np.array([temp[:,0],temp[:,1],temp[:,2]]) / radius_temp,0,1)
                cv_temp = np.sum(r_norm_temp * temp2,axis=1)
                cv_gas_ejected_ts[ok_match_ts] = cv_temp

                temperature_gas_ejected_ts[ok_match_ts] = temperature_gas_list_all_ts[ptr_ts][ok_match_ts]

            # Find positions of the reheated particles on the next step
            coordinates_gas_ejected_ns = np.zeros((len(id_gas_progen[ejected]),3))+3e9 #Assume those that can't be found are at infinity                                                                  
            temperature_gas_ejected_ns = np.zeros_like(id_gas_progen[ejected])
            if len(id_gas_list_all_ns)>0 and len(id_gas_progen[ejected])>0:
                ptr_ns = ms.match(id_gas_progen[ejected],id_gas_list_all_ns)
                ok_match_ns = ptr_ns >= 0

                coordinates_gas_ejected_ns[ok_match_ns] = coordinates_gas_list_all_ns[ptr_ns][ok_match_ns]

                temp = coordinates_gas_list_all_ns[ptr_ns][ok_match_ns]
                coordinates_halo = [halo_x_ns, halo_y_ns, halo_z_ns]
                temp = mpf.Centre_Halo(temp, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ns) # pkpc
                coordinates_gas_ejected_ns[ok_match_ns] = temp

                temperature_gas_ejected_ns[ok_match_ns] = temperature_gas_list_all_ns[ptr_ns][ok_match_ns]
            
            # These are just dummy values that shouldn't do anything in this case
            flag_ps = np.zeros_like(id_gas_progen[ejected])
            flag_ts = np.ones_like(flag_ps)
            flag_ns = np.ones_like(flag_ps)
            flag_list = np.array([flag_ps, flag_ts, flag_ns])

            coordinates_gas_eject_list = np.array([coordinates_gas_progen[ejected], coordinates_gas_ejected_ts, coordinates_gas_ejected_ns])

            temperature_gas_eject_list = np.array([temperature_gas_progen[ejected], temperature_gas_ejected_ts, temperature_gas_ejected_ns])
                
            use_ns = 2 # Should always be set to 2 - other values are a legacy of older code
            select_wind, delta_t_thr, vmax_cut, vmax_cut2 = up.Vmax_wind_selection(coordinates_gas_eject_list, cv_gas_ejected_ts, temperature_gas_eject_list, t_list, halo_vmax, halo_r200_host* 1e3 /h / (1+redshift_ts), False, use_ns, flag_list)

            # Add ejected particles to the ejected particle lists
            id_gas_ejecta_list_ts[i_halo] = np.append(np.array(id_gas_ejecta_list_ts[i_halo]), id_gas_progen[ejected][select_wind])

            # Add any particles that left ISM to wind particle list also (irrespective of whether in halo wind)
            id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), id_gas_progen[ejected][(SF_progen|ISM_progen)[ejected]])

            # Add those that fail wind test to halo_reheated list
            id_gas_halo_reheat_list_ts[i_halo] = np.append(np.array(id_gas_halo_reheat_list_ts[i_halo]), id_gas_progen[ejected][select_wind==False])

            # Add the other subset of these that were in the ISM reheated list to the ism wind particle lists
            if len(id_gas_ism_reheat_list_ts[i_halo])>0 and len(id_gas_progen[ejected])>0:
                ptr = ms.match(id_gas_ism_reheat_list_ts[i_halo], id_gas_progen[ejected])
                ok_match = (ptr>=0)

                # Double check we aren't double-counting w.r.t SF/NSF ISM reheated particles
                if len(id_gas_ism_reheat_list_ts[i_halo]) > 0 and len(id_gas_progen[ejected&(SF_progen|ISM_progen)]) > 0:
                    ptr2 = ms.match(id_gas_ism_reheat_list_ts[i_halo], id_gas_progen[ejected&(SF_progen|ISM_progen)])
                    ok_match = (ptr>=0) & (ptr2<0)

                id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), id_gas_progen[ejected][ptr][ok_match])

            # Remove ejected particles from ISM reheat lists
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_ism_reheat_list_ts[i_sub]) > 0 and len(id_gas_progen[SF_progen&ejected])>0:
                    ptr = ms.match(np.array(id_gas_ism_reheat_list_ts[i_sub]), id_gas_progen[SF_progen&ejected])
                    id_gas_ism_reheat_list_ts[i_sub] = np.array(id_gas_ism_reheat_list_ts[i_sub])[ptr<0]

            if i_halo == ih_choose:

                if init_tracking and not visualize_winds and snap_ts == snap_choose:

                    id_part_track = id_gas_progen[ejected]
                    use_ns_track = d_snap_ns+1


                    coordinates_track_ps = np.copy(coordinates_gas_progen[ejected])
                    velocity_track_ps = np.copy(velocity_gas_progen[ejected])
                    temperature_track_ps = np.copy(temperature_gas_progen[ejected])
                    internal_energy_track_ps = np.copy(internal_energy_gas_progen[ejected])
                                        
                    # Find location of ejected particles on _ts
                    ptr = ms.match(id_part_track, id_gas_list_all_ts)
                    ok_match = ptr>=0
                    
                    coordinates_track_ts = np.zeros((len(id_part_track),3))+np.nan
                    coordinates_track_ts[ok_match] = coordinates_gas_list_all_ts[ptr][ok_match]

                    velocity_track_ts = np.zeros((len(id_part_track),3))+np.nan
                    velocity_track_ts[ok_match] = velocity_gas_list_all_ts[ptr][ok_match]

                    velocity_track_ts *= expansion_factor_ts**0.5
                    velocity_track_ts[ok_match] += - np.array([halo_vx, halo_vy, halo_vz])


                    temperature_track_ts = np.zeros(len(id_part_track))+np.nan
                    temperature_track_ts[ok_match] = temperature_gas_list_all_ts[ptr][ok_match]
                    internal_energy_track_ts = np.zeros(len(id_part_track))+np.nan
                    internal_energy_track_ts[ok_match] = internal_energy_gas_list_all_ts[ptr][ok_match]


                    # Also match to star particles 
                    ptr2 = ms.match(id_part_track, id_star_list_all_ts)
                    ok_match2 = ptr2>=0

                    coordinates_track_ts[ok_match2] = coordinates_star_list_all_ts[ptr2][ok_match2]

                    coordinates_track_ts = mpf.Centre_Halo(coordinates_track_ts, [halo_x, halo_y, halo_z], boxsize) *1e3 /h / (1+redshift_ps)
                    
                    # Halo centering for this array is now done earlier
                    #coordinates_track_ps = mpf.Centre_Halo(coordinates_track_ps, [halo_x_progen, halo_y_progen, halo_z_progen], boxsize)


                    coordinates_track.append(coordinates_track_ps)
                    coordinates_track.append(coordinates_track_ts)
                    
                    velocity_track.append(velocity_track_ps)
                    velocity_track.append(velocity_track_ts)
                    temperature_track.append(temperature_track_ps)
                    temperature_track.append(temperature_track_ts)
                    internal_energy_track.append(internal_energy_track_ps)
                    internal_energy_track.append(internal_energy_track_ts)

                    select_wind_track = select_wind

                    redshift_list = [redshift_ps, redshift_ts]
                    vmax_list = [halo_vmax_progen, halo_vmax]

                    # Flag to keep track of fate of particle
                    flag1_track.append(np.zeros_like(id_part_track)) # 0 = bound gas, 1 = not bound, 2 = star (bound or unbound)

                    subgroup_number_part_track_ts = np.zeros_like(id_part_track) -1
                    subgroup_number_part_track_ts[ok_match] = subgroup_number_gas_list_all_ts[ptr][ok_match]
                    
                    flag1_ts = np.ones_like(id_part_track)
                    #flag1_ts[(subgroup_number_part_track_ts>=0)&(subgroup_number_part_track_ts<1e8)] = 0
                    flag1_ts[ok_match2] = 2

                    flag1_track.append(flag1_ts)

                elif init_tracking and snap_ts == snap_choose:

                    id_part_track2 = id_gas_progen[ejected&(SF_progen|ISM_progen)]

                    coordinates_track_ps2 = np.copy(coordinates_gas_progen[ejected&(SF_progen|ISM_progen)])
                    velocity_track_ps2 = np.copy(velocity_gas_progen[ejected&(SF_progen|ISM_progen)])
                    temperature_track_ps2 = np.copy(temperature_gas_progen[ejected&(SF_progen|ISM_progen)])
                    internal_energy_track_ps2 = np.copy(internal_energy_gas_progen[ejected&(SF_progen|ISM_progen)])
                                        
                    # Find location of ejected particles on _ts
                    ptr = ms.match(id_part_track2, id_gas_list_all_ts)
                    ok_match = ptr>=0

                    coordinates_track_ts2 = np.zeros((len(id_part_track2),3))+np.nan
                    coordinates_track_ts2[ok_match] = coordinates_gas_list_all_ts[ptr][ok_match]

                    velocity_track_ts2 = np.zeros((len(id_part_track2),3))+np.nan
                    velocity_track_ts2[ok_match] = velocity_gas_list_all_ts[ptr][ok_match]
                    
                    velocity_track_ts2 *= expansion_factor_ts**0.5
                    velocity_track_ts2[ok_match] += - np.array([halo_vx, halo_vy, halo_vz])


                    temperature_track_ts2 = np.zeros(len(id_part_track2))+np.nan
                    temperature_track_ts2[ok_match] = temperature_gas_list_all_ts[ptr][ok_match]
                    internal_energy_track_ts2 = np.zeros(len(id_part_track2))+np.nan
                    internal_energy_track_ts2[ok_match] = internal_energy_gas_list_all_ts[ptr][ok_match]


                    # Also match to star particles 
                    ptr2 = ms.match(id_part_track2, id_star_list_all_ts)
                    ok_match2 = ptr2>=0

                    coordinates_track_ts2[ok_match2] = coordinates_star_list_all_ts[ptr2][ok_match2]
                    
                    coordinates_track_ts2 = mpf.Centre_Halo(coordinates_track_ts2, [halo_x, halo_y, halo_z], boxsize) *1e3 /h / (1+redshift_ps)
                    
                    # Halo centering now done earlier
                    #coordinates_track_ps2 = mpf.Centre_Halo(coordinates_track_ps2, [halo_x_progen, halo_y_progen, halo_z_progen], boxsize)

                    if len(temperature_track_ps2)>0:

                        coordinates_track[0] = np.append(coordinates_track[0],coordinates_track_ps2,axis=0)
                        coordinates_track[1] = np.append(coordinates_track[1],coordinates_track_ts2,axis=0)

                        velocity_track[0] = np.append(velocity_track[0],velocity_track_ps2,axis=0)
                        velocity_track[1] = np.append(velocity_track[1],velocity_track_ts2,axis=0)
                    
                        temperature_track[0] = np.append(temperature_track[0],temperature_track_ps2)
                        temperature_track[1] = np.append(temperature_track[1],temperature_track_ts2)
                        internal_energy_track[0] = np.append(internal_energy_track[0],internal_energy_track_ps2)
                        internal_energy_track[1] = np.append(internal_energy_track[1],internal_energy_track_ts2)
  
                        select_wind_track = np.append(select_wind_track, select_wind[(SF_progen|ISM_progen)[ejected]])


                        # Flag to keep track of fate of particle
                        flag1_track[0] = np.append(flag1_track[0], np.zeros_like(id_part_track2)) # 0 = SF gas, 1 = NSF NISM, 2 = star

                        subgroup_number_part_track_ts2 = np.zeros_like(id_part_track2) -1
                        subgroup_number_part_track_ts2[ok_match] = subgroup_number_gas_list_all_ts[ptr][ok_match]

                        flag1_ts2 = np.ones_like(id_part_track2)
                        flag1_track[1] = np.append(flag1_track[1], flag1_ts2)

                        dummy3 = len(flag1_ts2)

                        id_part_track = np.append( id_part_track, id_part_track2)
                        coordinates_track_ps = np.append(coordinates_track_ps,coordinates_track_ps2,axis=0)
                        velocity_track_ps = np.append(velocity_track_ps, velocity_track_ps2,axis=0)


        elif len(id_star_gas_as) == 0:
            mass_reheated_list[i_halo] += np.sum(mass_gas_progen[SF_progen])
            n_reheated_list[i_halo] += len(mass_gas_progen[SF_progen])
            mass_ejected_list[i_halo] += np.sum(mass_gas_progen)
            n_ejected_list[i_halo] += len(mass_gas_progen)
            
            # Add ejected particles (including their mass and metallicity at the time of ejection) to the ejected particle lists
            id_gas_ejecta_list_ts[i_halo] = np.append(np.array(id_gas_ejecta_list_ts[i_halo]), id_gas_progen)
            # Add the subset of these that were in the star forming reservoir previously to the wind particle lists
            id_gas_wind_list_ts[i_halo] = np.append(np.array(id_gas_wind_list_ts[i_halo]), id_gas_progen[SF_progen])

            ptr_eject_gas = np.zeros_like(id_gas_progen) == 0.0
        else:
            ptr_eject_gas = np.array([])
        
        ###### Identify newly formed star forming star particles that came directly from the non-star forming reservoir
        # add these to sfr, recycling and cooling rates
        if len(id_star) > 0 and len(id_gas_progen[SF_progen==False]) >0:
            ptr = ms.match(id_star, id_gas_progen[SF_progen==False])
            ok_match = ptr >= 0

            new_stars = ok_match

            if len(id_star[new_stars]) > 0 and len(id_gas_wind_list_ts[i_halo])>0:
                ptr = ms.match(id_star[new_stars], id_gas_wind_list_ts[i_halo])
                recooled = ptr >= 0
            else:
                recooled = np.zeros_like(id_star[new_stars])< 0
            
            # Remove returned particles from ejected/reheated particle lists
            select_sub = np.where(group_number_list == halo_group_number)[0]
            for i_sub in select_sub:
                if len(id_gas_wind_list_ts[i_sub]) > 0 and len(id_star[new_stars][recooled])>0:
                    ptr = ms.match(np.array(id_gas_wind_list_ts[i_sub]), id_star[new_stars][recooled])
                    id_gas_wind_list_ts[i_sub] = np.array(id_gas_wind_list_ts[i_sub])[ptr<0]

                if len(id_gas_ism_reheat_list_ts[i_sub]) > 0 and len(id_star[new_stars][recooled])>0:
                    ptr = ms.match(np.array(id_gas_ism_reheat_list_ts[i_sub]), id_star[new_stars][recooled])
                    id_gas_ism_reheat_list_ts[i_sub] = np.array(id_gas_ism_reheat_list_ts[i_sub])[ptr<0]

        ###### Identify progenitor star particles which have been ejected entirely from the halo                  
        if len(id_star_progen) > 0 and len(id_star_gas_as) > 0:
            ptr_eject_star = ms.match(id_star_progen, id_star_gas_as)
        elif len(id_star_gas_as) == 0:
            ptr_eject_star = np.array([])
        else:
            ptr_eject_star = np.array([])

        
        if i_halo == ih_choose and init_tracking and snap_ts == snap_choose:
            # Compute half and 90% mass radii for stars
            radius_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))
            radius_SF = np.sqrt(np.square(coordinates_gas[SF][:,0])+np.square(coordinates_gas[SF][:,1])+np.square(coordinates_gas[SF][:,2]))

            radius_star_SF = np.append(radius_star,radius_SF)
            mass_star_SF = np.append(mass_star,mass_gas[SF])
            
            order_star = np.argsort(radius_star)
            order_star_SF = np.argsort(radius_star_SF)
            
            if len(mass_star_SF[order_star_SF]) > 0:
                ind_50 = np.argmin(abs(np.cumsum(mass_star_SF[order_star_SF]) - np.sum(mass_star_SF)*0.5))
                ind_90 = np.argmin(abs(np.cumsum(mass_star_SF[order_star_SF]) - np.sum(mass_star_SF)*0.9))
                
                r50_star_SF = radius_star_SF[order_star_SF][ind_50]
                r90_star_SF = radius_star_SF[order_star_SF][ind_90]
            else:
                r50_star_SF = np.nan; r90_star_SF = np.nan

            if len(mass_star[order_star]) > 0:
                ind_50 = np.argmin(abs(np.cumsum(mass_star[order_star]) - np.sum(mass_star)*0.5))
                ind_90 = np.argmin(abs(np.cumsum(mass_star[order_star]) - np.sum(mass_star)*0.9))

                r50_star = radius_star[order_star][ind_50]
                r90_star = radius_star[order_star][ind_90]
            else:
                r50_star = np.nan; r90_star = np.nan

            # Compute virial radius (r200)
            coordinates_all_part = np.append(np.append(coordinates_star_list_all_ts,coordinates_gas_list_all_ts,axis=0), coordinates_dm_list_all_ts,axis=0)
            mass_all_part = np.append(np.append(mass_star_list_all_ts, mass_gas_list_all_ts), np.zeros_like(group_number_dm_list_all_ts)+mass_dm) * 1.989e43 * g2Msun / h

            radius_all_part = np.sqrt(np.square(coordinates_all_part[:,0]-halo_x)+np.square(coordinates_all_part[:,1]-halo_y)+np.square(coordinates_all_part[:,2]-halo_z)) * 1e3 /h / (1+redshift_ts)
            radial_order = np.argsort(radius_all_part)
            mass_enclosed = np.cumsum(mass_all_part[radial_order])
            volume = 4*np.pi/3.0 * (radius_all_part[radial_order])**3 # kpc^3
            density_enclosed = mass_enclosed / volume # Msun / kpc^-3
            rho_crit = uc.Critical_Density(redshift_ts,h,omm,1-omm) # Msun pMpc^-3
            rho_crit *= 1e-9
            ind_r200 = np.argmin(abs(density_enclosed - rho_crit * 200))
            r200 = radius_all_part[radial_order][ind_r200]

            radius_store = [r50_star, r90_star, r50_star_SF, r90_star_SF, r200]

 
        ################# Prune particle lists ####################
        # (This is a major optimization step which prevents satellites from needing to cross match against all particles from the FoF group)
        
        # Prune particles that were positively id'd between the main progenitor of this subhalo and this subhalo's FoF group
        # These will then be excluded for subsequent subhaloes in this FoF group when computing ejection and stripping from main_progenitor to a different subhalo in the FoF group
        ptr_eject_gas = ptr_eject_gas[ptr_eject_gas > 0]
        ptr_eject_star = ptr_eject_star[ptr_eject_star > 0]
        ptr_eject_star_gas = np.append(ptr_eject_gas, ptr_eject_star)

        id_star_gas_as_fof[ind_fof] = np.delete( id_star_gas_as , ptr_eject_star_gas)
        subgroup_number_star_gas_as_fof[ind_fof] = np.delete(subgroup_number_star_gas_as, ptr_eject_star_gas)

        # Prune particles that were positively id'd between any progenitor of this FoF group and this subhalo
        # These will then be excluded for subsequent subhaloes in this FoF group when computing accretion rates and stripping from the host of the main progenitor onto the subhalo
        ptr_accrete = ptr_accrete[ptr_accrete > 0]

        id_star_gas_all_progen_fof[ind_fof] = np.delete( id_star_gas_all_progen, ptr_accrete)
        group_number_star_gas_all_progen_fof[ind_fof] = np.delete( group_number_star_gas_all_progen, ptr_accrete)
        subgroup_number_star_gas_all_progen_fof[ind_fof] = np.delete( subgroup_number_star_gas_all_progen, ptr_accrete)

        if i_halo == ih_choose:
            halo_descendant_index_tmp = descendant_index_list[i_halo]
            di_choose = halo_descendant_index_tmp

            if len(id_gas_wind_list_ts[i_halo])>0:
                ptr = ms.match(id_track_full, id_gas_wind_list_ts[i_halo])
                ok_match = ptr >= 0
                wind_state_track_full[i_snip,ok_match] = 1
            
            if len(id_gas_ejecta_list_ts[i_halo])>0:
                ptr = ms.match(id_track_full, id_gas_ejecta_list_ts[i_halo])
                ok_match = ptr >= 0
                ejecta_state_track_full[i_snip,ok_match] = 1
            
            if len(id_gas_ism_reheat_list_ts[i_halo])>0:
                ptr = ms.match(id_track_full, id_gas_ism_reheat_list_ts[i_halo])
                ok_match = ptr >= 0
                ism_reheated_state_track_full[i_snip,ok_match] = 1

            if len(id_gas_halo_reheat_list_ts[i_halo])>0:
                ptr = ms.match(id_track_full, id_gas_halo_reheat_list_ts[i_halo])
                ok_match = ptr >= 0
                halo_reheated_state_track_full[i_snip,ok_match] = 1

    # Loop over subhaloes again and compute mass, metals etc in ejected particles
    for i_halo in range(len(group_number_list)):

        # Check uniqueness of the ejected particle lists
        if len(id_gas_ejecta_list_ts[i_halo]) != len(np.unique(id_gas_ejecta_list_ts[i_halo])):
            id_gas_ejecta_list_ts[i_halo], index = np.unique(id_gas_ejecta_list_ts[i_halo], return_index=True)

        if len(id_gas_wind_list_ts[i_halo]) != len(np.unique(id_gas_wind_list_ts[i_halo])):
            id_gas_wind_list_ts[i_halo], index = np.unique(id_gas_wind_list_ts[i_halo], return_index=True)

        if len(id_gas_halo_reheat_list_ts[i_halo]) != len(np.unique(id_gas_halo_reheat_list_ts[i_halo])):
            id_gas_halo_reheat_list_ts[i_halo], index = np.unique(id_gas_halo_reheat_list_ts[i_halo], return_index=True)
        if len(id_gas_ism_reheat_list_ts[i_halo]) != len(np.unique(id_gas_ism_reheat_list_ts[i_halo])):
            id_gas_ism_reheat_list_ts[i_halo], index = np.unique(id_gas_ism_reheat_list_ts[i_halo], return_index=True)

    id_gas_ejecta_list_ps = list(id_gas_ejecta_list_ts)
    id_gas_wind_list_ps = list(id_gas_wind_list_ts)
    id_gas_ism_reheat_list_ps = list(id_gas_ism_reheat_list_ts)
    id_gas_halo_reheat_list_ps = list(id_gas_halo_reheat_list_ts)
    descendant_index_list_ps = descendant_index_list
    
    File.close()
    
    if snap_ts == snap_choose:
        init_tracking = False

data = [id_track_full, radius_track_full, radius_phys_track_full, temp_track_full, nh_track_full, cv_track_full, v_track_full, ism_flag_track_full, sf_flag_track_full, ejecta_state_track_full, wind_state_track_full, ism_reheated_state_track_full, halo_reheated_state_track_full, vmax_track_full,in_sat_flag_track_full,group_number_track_full, use_ns_track_full]
up.Plot_Track_Full( data , snap_list, cosmic_t_list)
#up.Plot_Virgo_Ex( data , snap_list, cosmic_t_list)
#up.Plot_Dist_Track_Full( data , snap_list, cosmic_t_list,snap_choose)

print "got to the end"
quit()

flag1_track = np.array(flag1_track)
plot_particles = False
if plot_particles:
    for n, ax in enumerate(subplots):
        py.axes(ax)
                
        if n == 0:
            dim = "xy"
        else:
            dim = "xz"

        up.Plot_Tracked_Particles(coordinates_track, flag1_track, "c", np.array(redshift_list),h,dim=dim)
    py.show()


vmax_list = np.array(vmax_list)
redshift_list = np.array(redshift_list)    

if visualize_winds:
    monotonic = True
else:
    monotonic = False
    print "mono false for ejecta winds"

up.Plot_Rmax_Dist(id_part_track, coordinates_track, redshift_list, vmax_list, flag1_track,velocity_track, temperature_track, internal_energy_track, select_wind_track, h,omm, radius_store, subvol, visualize_winds,monotonic=monotonic,use_ns=use_ns_track,cum=False)

quit()

#up.Plot_v_rmax(coordinates_track, np.array(redshift_list), flag1_track, velocity_track, temperature_track, internal_energy_track, h, monotonic=True)
#up.Plot_delta_r_rmax(coordinates_track, np.array(redshift_list), flag1_track, velocity_track, temperature_track, internal_energy_track, h, monotonic=True)
#up.Plot_dEth_r_Rmax(coordinates_track, np.array(redshift_list), flag1_track,velocity_track, temperature_track, internal_energy_track, h,visualize_winds,monotonic=True)

#if temporal_convergence_mode:
#    up.Plot_dT_Rmax(coordinates_track, np.array(redshift_list), vmax_list, flag1_track,velocity_track, temperature_track, internal_energy_track, h,omm, visualize_winds,monotonic=True,use_ns=2+len(snip_skip))
#else:
#    up.Plot_dT_Rmax(coordinates_track, np.array(redshift_list), vmax_list, flag1_track,velocity_track, temperature_track, internal_energy_track, h,omm, visualize_winds,monotonic=True)

#up.Plot_Tmax_Rmax(coordinates_track, np.array(redshift_list), vmax_list, flag1_track,velocity_track, temperature_track, internal_energy_track, h,omm, visualize_winds,monotonic=True,use_ns=use_ns)
#up.Plot_tmax_Rmax(coordinates_track, np.array(redshift_list), vmax_list, flag1_track,velocity_track, temperature_track, internal_energy_track, h,omm, visualize_winds,monotonic=True,use_ns=use_ns)
#up.Plot_Vrad_max_Rmax(coordinates_track, np.array(redshift_list), vmax_list, flag1_track,velocity_track, temperature_track, internal_energy_track, h,omm, visualize_winds,monotonic=True,use_ns=use_ns)
