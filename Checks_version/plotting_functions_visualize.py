import numpy as np
from utilities_plotting import *
import utilities_cosmology as uc
import utilities_statistics as us

kpc2km = 3.0857e16
Gyr2s = 3.15576e16

def Vmax_wind_selection(coordinates_list_in, cv_ts, temperature_list, t_list, vmax_ts, r200_ts, visualize_winds, use_ns, flag_list):

    hack = True

    select_wind = np.zeros_like(temperature_list[0])>0

    coordinates_list = np.array(coordinates_list_in)
    x_part = coordinates_list[:,:,0]
    y_part = coordinates_list[:,:,1]
    z_part = coordinates_list[:,:,2]

    r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part) )

    r_part[1:][(flag_list[1:] == 0) | (flag_list[1:]==2)] = 0.0

    if not use_ns >= 2:
        print "Error, use_ns must be >=2"
        quit()

    # This is excluding particles that suddenly increase in thermal energy between _ts and _ns
    # Note they would then have the chance to be selected as wind particles on the next timestep
    if visualize_winds:
        late_t_jump = (temperature_list[use_ns] - temperature_list[1] > 10**4.75) & (temperature_list[1] - temperature_list[0] < 10**3)
        temp = r_part[use_ns:]
        temp[:,late_t_jump] = 0.0
        r_part[use_ns:] = temp

    vrad_ta_part_ts = (r_part[1] - r_part[0]) * kpc2km / (t_list[1]-t_list[0]) / Gyr2s
    vrad_ta_part_ns = (r_part[use_ns] - r_part[1]) * kpc2km / (t_list[use_ns]-t_list[1]) / Gyr2s
    vrad_ta_part_tns = (r_part[use_ns] - r_part[0]) * kpc2km / Gyr2s /  (t_list[use_ns]-t_list[1])

    if visualize_winds:
        vmax_cut = 0.25
        vmax_cut2 = 0.25
        delta_t_thr = 0.2 # Gyr

        if t_list[1] - t_list[0] > delta_t_thr:
            select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) | (vrad_ta_part_ts / vmax_ts > vmax_cut2)
        else:
            select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) & (vrad_ta_part_ts / vmax_ts > vmax_cut2)
            select_wind = select_wind | ((vrad_ta_part_ns / vmax_ts > vmax_cut*2) | (vrad_ta_part_ts / vmax_ts > vmax_cut2*2))

        if hack:
            # Idea here is that you don't want to select particles that haven't joined the wind yet (hence require
            vmax_cut = 0.5
            select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) & (cv_ts / vmax_ts > 0.25 * vmax_cut)
            select_wind = select_wind | (cv_ts / vmax_ts > 1) # Experiment to help out completeness

    else:
        vmax_cut = 0.125
        #vmax_cut = 0.25

        vmax_cut2 = vmax_cut*2
        delta_t_thr = 0.2 # Gyr
        if t_list[1] - t_list[0] > delta_t_thr:
            # As I remember - you have problem that particle that become unbound accelerate after being unbound
            # The second cut (_tns) is to ensure that particles that move inwards between _ps and _ts - have enough velocity to get out beyond r_init in the future
            select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) & (vrad_ta_part_tns / vmax_ts > vmax_cut)
            select_wind = select_wind | ((vrad_ta_part_ts / vmax_ts > vmax_cut2) & ((flag_list[use_ns] != 0)&(flag_list[use_ns] != 2)))
                
        else:
            select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) & (vrad_ta_part_ts / vmax_ts > vmax_cut)
            select_wind = select_wind | (((vrad_ta_part_ns / vmax_ts > vmax_cut2) | (vrad_ta_part_ts / vmax_ts > vmax_cut2)) & ((flag_list[use_ns] != 0)&(flag_list[use_ns] != 2)))

        if hack:
            select_wind = (vrad_ta_part_ns / vmax_ts > vmax_cut) & (cv_ts / vmax_ts > vmax_cut) & (r_part[1] > r200_ts)
    
    return select_wind, delta_t_thr, vmax_cut, vmax_cut2




def Vmax_wind_selection_single(r_part_sel_dr, t_list, vmax_ts, visualize_winds, use_ns, i_flag, flag1):
    # I think this routine is for single particles, where as the above function is for sets of particles

    select_wind = False
    weird = False # Something to tag particles for which I would like to look at their trajectory

    hack = False
    if hack:
        print "Using hack"


    vrad_ta_part_ts = (r_part_sel_dr[1] - r_part_sel_dr[0]) * kpc2km / (t_list[1]-t_list[0]) / Gyr2s
    vrad_ta_part_ns = (r_part_sel_dr[use_ns] - r_part_sel_dr[1]) * kpc2km / (t_list[use_ns]-t_list[1]) / Gyr2s
    vrad_ta_part_tns = (r_part_sel_dr[use_ns] - r_part_sel_dr[0])* kpc2km / Gyr2s /  (t_list[use_ns]-t_list[1])
    vrad_ta_part_nps = (r_part_sel_dr[use_ns] - r_part_sel_dr[0])* kpc2km / Gyr2s /  (t_list[use_ns]-t_list[0])

    if visualize_winds:
        vmax_cut = 0.25
        vmax_cut2 = 0.25
        delta_t_thr = 0.2 # Gyr

        # Use double size cut for NSF ISM particles
        #if flag1[0] == 3:
        #    vmax_cut *= 2
        #    vmax_cut2 *= 2
        
        if t_list[1] - t_list[0] > delta_t_thr:
            # Note the "or" here means the lack of fixed _ps>_ts temporal spacing shouldn't be an issue
            if vrad_ta_part_ns / vmax_ts > vmax_cut or vrad_ta_part_ts / vmax_ts > vmax_cut2:
                select_wind = True
        else:
            if vrad_ta_part_ns / vmax_ts > vmax_cut and vrad_ta_part_ts / vmax_ts > vmax_cut2:
                select_wind = True
            elif vrad_ta_part_ns / vmax_ts > vmax_cut*2 or vrad_ta_part_ts / vmax_ts > vmax_cut2*2:
                select_wind = True

        if hack:
            jim = False
            if select_wind:
                jim = True
            
            if vrad_ta_part_ns / vmax_ts > vmax_cut and vrad_ta_part_ts / vmax_ts > vmax_cut:
                select_wind = True
            else:
                select_wind = False

            if select_wind ==False and jim:
                weird = True
            if select_wind and jim==False:
                weird = True

    else:
        vmax_cut = 0.125
        #vmax_cut = 0.25                                                                                                                                                                                                          
        vmax_cut2 = vmax_cut*2

        delta_t_thr = 0.2 # Gyr                                                                                                                                                                                                   
        if t_list[1] - t_list[0] > delta_t_thr:
            # As I remember - you have problem that particle that become unbound accelerate after being unbound                                                                                                                   
            # The second cut (_tns) is to ensure that particles that move inwards between _ps and _ts - have enough velocity to get out beyond r_init in the future                                                               
            if vrad_ta_part_ns / vmax_ts > vmax_cut and vrad_ta_part_tns / vmax_ts > vmax_cut:
                select_wind = True
            elif vrad_ta_part_ts / vmax_ts > vmax_cut2 and i_flag > 2:
                select_wind = True
                
        else:
            if vrad_ta_part_ns / vmax_ts > vmax_cut and vrad_ta_part_ts / vmax_ts > vmax_cut:
                select_wind = True
            elif vrad_ta_part_ns / vmax_ts > vmax_cut2 or vrad_ta_part_ts / vmax_ts > vmax_cut2:
                if i_flag > 2:
                    select_wind = True

    return select_wind, delta_t_thr, vmax_cut, vmax_cut2, weird

def Plot_Track_Full( data, snap_list, t_list ):
    id_track_full, radius_track_full, radius_phys_track_full, temp_track_full, nh_track_full, cv_track_full, v_track_full, ism_flag_track_full, sf_flag_track_full, ejecta_state_track_full, wind_state_track_full, ism_reheated_state_track_full, halo_reheated_state_track_full, vmax, in_sat_flag_track_full,group_number_track_full,use_ns_track_full = data

    # Delta r / delta t - using ns skip as used for wind selection
    drdt_full_ns_skip = np.zeros((len(vmax)-1, len(radius_phys_track_full[0])))
    for i_n in range(len(use_ns_track_full)-1):
        i_use = int(i_n + min(use_ns_track_full[i_n], len(use_ns_track_full)-1))
        drdt_full_ns_skip[i_n] = (radius_phys_track_full[i_use] - radius_phys_track_full[i_n])*kpc2km/ ((t_list[i_use] -t_list[i_n])*Gyr2s)

    # Delta r / delta t - between adjacent snaps
    drdt_full =  np.swapaxes(np.swapaxes(radius_phys_track_full[1:]-radius_phys_track_full[0:-1],0,1) * kpc2km / ( (t_list[1:]-t_list[0:-1])*Gyr2s),0,1) # kms

    # Choose particle selection
    
    # Was in ISM at some point selection
    #ok = np.nanmax(ism_flag_track_full,axis=0) > 0

    # Was in halo at middle snapshot (this is the original selection)
    #ok = id_track_full > 0 # Dummy - no selection

    # Was in wind at some point
    ok  = np.nanmax(wind_state_track_full,axis=0) > 0

    # Left r200 at some point
    #ok = (np.nanmax(halo_reheated_state_track_full,axis=0)>0)| (np.nanmax(ejecta_state_track_full,axis=0)>0)

    #ok = id_track_full == 5541517851433
    #ok = id_track_full == 5539625076299
    #ok = id_track_full == 5573235045761
    #ok = id_track_full == 554051296391

    #ok = id_track_full == 5540911715017

    #np.random.seed(23231)
    np.random.seed(12316)
    order = np.arange(0,len(id_track_full[ok]))
    np.random.shuffle(order)

    # Choose how many particles to show
    n_show = min(50,len(id_track_full[ok]))

    py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                        'figure.figsize':[6.64*2,4.98*2],
                        'figure.subplot.left':0.05,
                        'figure.subplot.bottom':0.09,
                        'figure.subplot.top':0.98,
                        'font.size':12,
                        'legend.fontsize':4})

    def vert_lines():
        ds = (snap_list[-1]-snap_list[0])
        line_range = np.arange(0,1.1*ds,0.1*ds) + snap_list[0]
        for line in line_range:
            py.axvline(line,alpha=0.1)


    for i_part in range(n_show):

        print "making figure for i_part", i_part,"id of plotted particle is", id_track_full[ok][order][i_part]

        radius = radius_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        temp = temp_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        cv = cv_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        v = v_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        ism_flag = ism_flag_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        sf_flag = sf_flag_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        ejecta_state = ejecta_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        wind_state = wind_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        ism_reheated_state = ism_reheated_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        halo_reheated_state = halo_reheated_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        nh = nh_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        drdt = drdt_full[:,np.where(ok)[0]][:,order][:,i_part]
        drdt_ns_skip = drdt_full_ns_skip[:,np.where(ok)[0]][:,order][:,i_part]
        in_sat = in_sat_flag_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        group_number = group_number_track_full[:,np.where(ok)[0]][:,order][:,i_part]

        # Tangential velocity
        tv = np.sqrt(v**2 - cv**2)

        py.figure()
        py.subplot(221)

        py.scatter(snap_list,radius,c="k")
        py.scatter(snap_list[in_sat==1],radius[in_sat==1],c="r")
        py.plot(snap_list, radius,c="k",label="radius")

        py.scatter(snap_list,ejecta_state*0.1, c="b")
        py.plot(snap_list, ejecta_state*0.1,c="b",label="ejecta")

        py.scatter(snap_list,wind_state*0.09,c="r")
        py.plot(snap_list, wind_state*0.09,c="r",label="wind",linestyle='--')

        py.scatter(snap_list,ism_flag*0.06,c="g")
        py.plot(snap_list, ism_flag*0.06,c="g",label="ism",linestyle='--')

        py.scatter(snap_list,sf_flag*0.05,c="m")
        py.plot(snap_list, sf_flag*0.05,c="m",label="sf",linestyle='--')

        py.scatter(snap_list,ism_reheated_state*0.07,c="y",s=3)
        py.plot(snap_list, ism_reheated_state*0.07,c="y",label="ism-reheated")

        py.scatter(snap_list,halo_reheated_state*0.065,c="c",s=3)
        py.plot(snap_list, halo_reheated_state*0.065,c="c",label="halo-reheated")

        py.axhline(1.0,c="k",alpha=0.2)

        py.ylim((0,2.0))
        py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
        py.legend(loc="center left")
        vert_lines()
        py.xlim((snap_list.min(),snap_list.max()))

        py.subplot(222)

        py.plot(snap_list, cv,label="radial velocity",c="b")
        py.scatter(snap_list, cv,c="b")
        py.scatter(snap_list[wind_state>0], cv[wind_state>0],c="r")
        snap_list_med = 0.5*(snap_list[0:-1]+snap_list[1:])
        py.plot(snap_list_med, drdt,label="time-averaged radial velocity",c="r")
        py.plot(snap_list_med, drdt_ns_skip, label="time-averaged radial velocity (ns skip)",c="k")

        py.plot(snap_list, v,label="speed",c="g")
        py.plot(snap_list, tv,label="tangential speed",c="m")
        py.ylabel(r"$v$")
        py.axhline(0.0,alpha=0.1,c="k")

        py.plot(snap_list, vmax,c="k",label="Vmax",alpha=0.5)
        py.plot(snap_list, vmax*0.25,c="k",label="1/4 Vmax",linestyle='--',alpha=0.5)
        py.plot(snap_list, vmax*0.5,c="k",label="1/2 Vmax",linestyle=':',alpha=0.5)
        
        py.legend(loc="lower right")

        vert_lines()
        py.xlim((snap_list.min(),snap_list.max()))
        py.ylim((-1.5*np.median(vmax[(np.isnan(vmax)==False)&(vmax<2000)]), 1.5*np.median(vmax[(np.isnan(vmax)==False)&(vmax<2000)])))

        py.subplot(223)

        py.plot(snap_list, np.log10(temp))
        py.ylabel(r"$\log(T)$")
        vert_lines()
        py.xlim((snap_list.min(),snap_list.max()))
        
        py.subplot(224)

        py.plot(snap_list, np.log10(nh))
        py.ylabel(r"$\log(n_{\mathrm{H}})$")
        vert_lines()
        py.xlim((snap_list.min(),snap_list.max()))
        
        py.savefig("Temp/temp_"+str(i_part)+".pdf")
        if n_show == 1:
            py.show()

def Plot_Virgo_Ex( data, snap_list, t_list ):
    id_track_full, radius_track_full, radius_phys_track_full, temp_track_full, nh_track_full, cv_track_full, v_track_full, ism_flag_track_full, sf_flag_track_full, ejecta_state_track_full, wind_state_track_full, ism_reheated_state_track_full, halo_reheated_state_track_full, vmax, in_sat_flag_track_full,group_number_track_full,use_ns_track_full = data

    # Choose particle selection
    
    # Was in ISM at some point selection
    #ok = np.nanmax(ism_flag_track_full,axis=0) > 0

    # Was in halo at middle snapshot (this is the original selection)
    #ok = id_track_full > 0 # Dummy - no selection

    # Was in wind at some point
    ok  = np.nanmax(wind_state_track_full,axis=0) > 0

    # Left r200 at some point
    #ok = (np.nanmax(halo_reheated_state_track_full,axis=0)>0)| (np.nanmax(ejecta_state_track_full,axis=0)>0)

    #ok = id_track_full == 5541517851433
    #ok = id_track_full == 5539625076299
    #ok = id_track_full == 5573235045761
    #ok = id_track_full == 554051296391

    #ok = id_track_full == 5540911715017

    #np.random.seed(23231)
    np.random.seed(12316)
    order = np.arange(0,len(id_track_full[ok]))
    np.random.shuffle(order)

    # Choose how many particles to show
    n_show = min(50,len(id_track_full[ok]))

    py.rcParams.update(latexParams)
    py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                        'figure.figsize':[3.32,2.49],
                        'figure.subplot.left':0.17,
                        'figure.subplot.bottom':0.19,
                        'figure.subplot.top':0.97,
                        'font.size':12,
                        'legend.fontsize':6})
    from matplotlib.ticker import FormatStrFormatter

    for i_part in range(n_show):

        print "making figure for i_part", i_part,"id of plotted particle is", id_track_full[ok][order][i_part]

        radius = radius_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        ism_flag = ism_flag_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        sf_flag = sf_flag_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        ejecta_state = ejecta_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        wind_state = wind_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        ism_reheated_state = ism_reheated_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        halo_reheated_state = halo_reheated_state_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        in_sat = in_sat_flag_track_full[:,np.where(ok)[0]][:,order][:,i_part]
        group_number = group_number_track_full[:,np.where(ok)[0]][:,order][:,i_part]

        fig, ax = py.subplots()
        ax.ticklabel_format(useOffset=False)
        #py.xaxis.set_major_formatter(StrMethodFormatter('{x:,.2f}'))

        py.plot(t_list, radius,c="k",linewidth=1.2)
        py.scatter(t_list,radius,c="k",s=3)

        def plot_select(selection, rad, t, c, lw = 1.2,label=""):
            rad_t = np.copy(rad)
            rad_t[selection==False] = np.nan

            if label != "":
                py.plot(t, rad_t, label=label, linewidth=lw,c=c)
            else:
                py.plot(t, rad_t, label=label, linewidth=lw,c=c)

        ism = ism_flag>0
        SF = sf_flag>0
        reheat = ism_reheated_state>0
        wind = wind_state>0
        ej = ejecta_state>0

        plot_select(ism, radius, t_list, "g", 1.2, label="ISM")
        plot_select(SF, radius, t_list, "m", 1.2, label="SF-ISM")
        plot_select(reheat, radius, t_list, "y", 1.2)
        plot_select(wind, radius, t_list, "r", 1.2, label="Wind")
        plot_select(ej, radius, t_list, "b", 1.2, label="Ejected")


        py.axhline(1.0,c="k",alpha=0.2)

        py.ylim((0,2.0))
        py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
        py.legend(loc="lower right")
        py.xlim((t_list.min(),t_list.max()))
        py.xlabel(r"$t \, / \mathrm{Gyr}$")
        
        py.savefig("Temp/virgo_temp_"+str(i_part)+".pdf")
        if n_show == 1:
            py.show()

def Plot_Dist_Track_Full( data, snap_list, t_list, snap_choose ):
    id_track_full, radius_track_full, radius_phys_track_full, temp_track_full, nh_track_full, cv_track_full, v_track_full, ism_flag_track_full, sf_flag_track_full, ejecta_state_track_full, wind_state_track_full, ism_reheated_state_track_full, halo_reheated_state_track_full, vmax, in_sat_flag_track_full,group_number_track_full,use_ns_track_full = data

    # Choose particle selection
    # Note initial selection is that particle must be in halo at snap_choose

    t_choose = t_list[np.argmin(abs(snap_choose-snap_list))]

    n_window = 2
    n_snap = len(snap_list)
    n_snap_tot = n_window + n_snap
    n_part = len(id_track_full)

    radius = np.zeros((n_snap_tot, n_part)) + np.nan
    nh = np.zeros_like(radius)
    cv = np.zeros_like(radius)
    temp = np.zeros_like(radius)

    radius[0:n_snap] = radius_track_full
    nh[0:n_snap] = nh_track_full
    temp[0:n_snap] = temp_track_full
    cv[0:n_snap] = cv_track_full

    ok = np.zeros(n_part)<0
    isnap_select = np.zeros(n_part)+np.nan

    isnap_choose = np.argmin(abs(snap_choose - snap_list))

    # Particles that joined halo within 10 snaps of snap choose
    for i_part in range(n_part):

        if radius[isnap_choose-n_window,i_part] > 1 and radius[isnap_choose,i_part] < 1:
            ok[i_part] = True
            isnap_select[i_part] = np.argmax(np.arange(0,isnap_choose)[(radius[0:isnap_choose,i_part]>=1)|np.isnan(radius[0:isnap_choose,i_part])])
            shift = int(isnap_choose - isnap_select[i_part])

            if shift > n_window:
                print "yikes"
                ok[i_part] = False
                continue

            radius[shift:n_snap+shift,i_part] = radius[0:n_snap,i_part]
            radius[0:shift,i_part] += np.nan
            cv[shift:n_snap+shift,i_part] = cv[0:n_snap,i_part]
            cv[0:shift,i_part] += np.nan
            temp[shift:n_snap+shift,i_part] = temp[0:n_snap,i_part]
            temp[0:shift,i_part] += np.nan
            nh[shift:n_snap+shift,i_part] = nh[0:n_snap,i_part]
            nh[0:shift,i_part] += np.nan

    radius_shifted = radius[n_window+1:-n_window-1,ok]
    nh_shifted = nh[n_window+1:-n_window-1,ok]
    temp_shifted = temp[n_window+1:-n_window-1,ok]
    cv_shifted = cv[n_window+1:-n_window-1,ok]
    time_shifted = t_list[n_window+1:-1] - t_list[isnap_choose+1]

    n_snap_show = len(radius_shifted[:,0])

    radius_med = np.zeros(n_snap_show)
    radius_lo = np.zeros(n_snap_show)
    radius_hi = np.zeros(n_snap_show)

    cv_med = np.zeros(n_snap_show)
    cv_lo = np.zeros(n_snap_show)
    cv_hi = np.zeros(n_snap_show)

    temp_med = np.zeros(n_snap_show)
    temp_lo = np.zeros(n_snap_show)
    temp_hi = np.zeros(n_snap_show)

    nh_med = np.zeros(n_snap_show)
    nh_lo = np.zeros(n_snap_show)
    nh_hi = np.zeros(n_snap_show)

    for i_snap in range(n_snap_show):
        ok = np.isnan(radius_shifted[i_snap])==False
        radius_med[i_snap] = np.median(radius_shifted[i_snap][ok])
        radius_lo[i_snap] = us.Weighted_Percentile(radius_shifted[i_snap][ok],np.ones_like(radius_shifted[i_snap,ok]),0.16)
        radius_hi[i_snap] = us.Weighted_Percentile(radius_shifted[i_snap][ok],np.ones_like(radius_shifted[i_snap,ok]),0.84)
        nh_med[i_snap] = np.median(nh_shifted[i_snap][ok])
        nh_lo[i_snap] = us.Weighted_Percentile(nh_shifted[i_snap][ok],np.ones_like(nh_shifted[i_snap,ok]),0.16)
        nh_hi[i_snap] = us.Weighted_Percentile(nh_shifted[i_snap][ok],np.ones_like(nh_shifted[i_snap,ok]),0.84)
        cv_med[i_snap] = np.median(cv_shifted[i_snap][ok])
        cv_lo[i_snap] = us.Weighted_Percentile(cv_shifted[i_snap][ok],np.ones_like(cv_shifted[i_snap,ok]),0.16)
        cv_hi[i_snap] = us.Weighted_Percentile(cv_shifted[i_snap][ok],np.ones_like(cv_shifted[i_snap,ok]),0.84)
        temp_med[i_snap] = np.median(temp_shifted[i_snap][ok])
        temp_lo[i_snap] = us.Weighted_Percentile(temp_shifted[i_snap][ok],np.ones_like(temp_shifted[i_snap,ok]),0.16)
        temp_hi[i_snap] = us.Weighted_Percentile(temp_shifted[i_snap][ok],np.ones_like(temp_shifted[i_snap,ok]),0.84)

    py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                        'figure.figsize':[6.64*2,4.98*2],
                        'figure.subplot.left':0.05,
                        'figure.subplot.bottom':0.09,
                        'figure.subplot.top':0.98,
                        'font.size':12,
                        'legend.fontsize':4})

    def vert_lines():
        ds = (snap_list[-1]-snap_list[0])
        line_range = np.arange(0,1.1*ds,0.1*ds) + snap_list[0]
        for line in line_range:
            py.axvline(line,alpha=0.1)

            
    py.figure()
    py.subplot(221)

    py.scatter(time_shifted,radius_med,c="k")
    py.plot(time_shifted, radius_med,c="k",label="radius")
    py.plot(time_shifted, radius_hi,c="k")
    py.plot(time_shifted, radius_lo,c="k")
    
    py.axhline(1.0,c="k",alpha=0.2)
    py.axvline(0.0,c="k",alpha=0.2)
    py.ylim((0,2.5))
    py.xlim((time_shifted.min(),time_shifted.max()))
    vert_lines()
    #py.legend(loc="center left")
    py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")

    py.subplot(222)
    py.plot(time_shifted, cv_med,label="radial velocity",c="b")
    py.plot(time_shifted, cv_lo,c="b")
    py.plot(time_shifted, cv_hi,c="b")
    py.ylabel(r"$v$")
    py.axhline(0.0,alpha=0.1,c="k")
        
    py.plot(t_list-t_choose, vmax,c="k",label="Vmax",alpha=0.5)
    py.legend(loc="lower right")
    vert_lines()
    py.xlim((time_shifted.min(),time_shifted.max()))
    py.ylim((-1.5*np.median(vmax[(np.isnan(vmax)==False)&(vmax<2000)]), 1.5*np.median(vmax[(np.isnan(vmax)==False)&(vmax<2000)])))
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")

    py.subplot(223)

    py.plot(time_shifted, np.log10(temp_med))
    py.plot(time_shifted, np.log10(temp_lo))
    py.plot(time_shifted, np.log10(temp_hi))
    py.ylabel(r"$\log(T)$")
    vert_lines()
    py.xlim((time_shifted.min(),time_shifted.max()))
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")
    
    py.subplot(224)
    py.plot(time_shifted, np.log10(nh_med))
    py.plot(time_shifted, np.log10(nh_lo))
    py.plot(time_shifted, np.log10(nh_hi))
    py.ylabel(r"$\log(n_{\mathrm{H}})$")
    vert_lines()
    py.xlim((time_shifted.min(),time_shifted.max()))
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")

    py.savefig("Temp/rdist_acc_halo.pdf")
    py.show()

def Plot_Dist_Track_Full( data, snap_list, t_list, snap_choose ):
    id_track_full, radius_track_full, radius_phys_track_full, temp_track_full, nh_track_full, cv_track_full, v_track_full, ism_flag_track_full, sf_flag_track_full, ejecta_state_track_full, wind_state_track_full, ism_reheated_state_track_full, halo_reheated_state_track_full, vmax, in_sat_flag_track_full,group_number_track_full,use_ns_track_full = data

    # Choose particle selection
    # Note initial selection is that particle must be in halo at snap_choose

    t_choose = t_list[np.argmin(abs(snap_choose-snap_list))]

    n_window = 2
    n_snap = len(snap_list)
    n_snap_tot = n_window + n_snap
    n_part = len(id_track_full)

    radius = np.zeros((n_snap_tot, n_part)) + np.nan
    nh = np.zeros_like(radius)
    cv = np.zeros_like(radius)
    temp = np.zeros_like(radius)

    radius[0:n_snap] = radius_track_full
    nh[0:n_snap] = nh_track_full
    temp[0:n_snap] = temp_track_full
    cv[0:n_snap] = cv_track_full

    ok = np.zeros(n_part)<0
    isnap_select = np.zeros(n_part)+np.nan

    isnap_choose = np.argmin(abs(snap_choose - snap_list))

    # Particles that joined halo within 10 snaps of snap choose
    for i_part in range(n_part):

        if radius[isnap_choose-n_window,i_part] > 1 and radius[isnap_choose,i_part] < 1:
            ok[i_part] = True
            isnap_select[i_part] = np.argmax(np.arange(0,isnap_choose)[(radius[0:isnap_choose,i_part]>=1)|np.isnan(radius[0:isnap_choose,i_part])])
            shift = int(isnap_choose - isnap_select[i_part])

            if shift > n_window:
                print "yikes"
                ok[i_part] = False
                continue

            radius[shift:n_snap+shift,i_part] = radius[0:n_snap,i_part]
            radius[0:shift,i_part] += np.nan
            cv[shift:n_snap+shift,i_part] = cv[0:n_snap,i_part]
            cv[0:shift,i_part] += np.nan
            temp[shift:n_snap+shift,i_part] = temp[0:n_snap,i_part]
            temp[0:shift,i_part] += np.nan
            nh[shift:n_snap+shift,i_part] = nh[0:n_snap,i_part]
            nh[0:shift,i_part] += np.nan

    radius_shifted = radius[n_window+1:-n_window-1,ok]
    nh_shifted = nh[n_window+1:-n_window-1,ok]
    temp_shifted = temp[n_window+1:-n_window-1,ok]
    cv_shifted = cv[n_window+1:-n_window-1,ok]
    time_shifted = t_list[n_window+1:-1] - t_list[isnap_choose+1]

    n_snap_show = len(radius_shifted[:,0])

    radius_med = np.zeros(n_snap_show)
    radius_lo = np.zeros(n_snap_show)
    radius_hi = np.zeros(n_snap_show)

    cv_med = np.zeros(n_snap_show)
    cv_lo = np.zeros(n_snap_show)
    cv_hi = np.zeros(n_snap_show)

    temp_med = np.zeros(n_snap_show)
    temp_lo = np.zeros(n_snap_show)
    temp_hi = np.zeros(n_snap_show)

    nh_med = np.zeros(n_snap_show)
    nh_lo = np.zeros(n_snap_show)
    nh_hi = np.zeros(n_snap_show)

    for i_snap in range(n_snap_show):
        ok = np.isnan(radius_shifted[i_snap])==False
        radius_med[i_snap] = np.median(radius_shifted[i_snap][ok])
        radius_lo[i_snap] = us.Weighted_Percentile(radius_shifted[i_snap][ok],np.ones_like(radius_shifted[i_snap,ok]),0.16)
        radius_hi[i_snap] = us.Weighted_Percentile(radius_shifted[i_snap][ok],np.ones_like(radius_shifted[i_snap,ok]),0.84)
        nh_med[i_snap] = np.median(nh_shifted[i_snap][ok])
        nh_lo[i_snap] = us.Weighted_Percentile(nh_shifted[i_snap][ok],np.ones_like(nh_shifted[i_snap,ok]),0.16)
        nh_hi[i_snap] = us.Weighted_Percentile(nh_shifted[i_snap][ok],np.ones_like(nh_shifted[i_snap,ok]),0.84)
        cv_med[i_snap] = np.median(cv_shifted[i_snap][ok])
        cv_lo[i_snap] = us.Weighted_Percentile(cv_shifted[i_snap][ok],np.ones_like(cv_shifted[i_snap,ok]),0.16)
        cv_hi[i_snap] = us.Weighted_Percentile(cv_shifted[i_snap][ok],np.ones_like(cv_shifted[i_snap,ok]),0.84)
        temp_med[i_snap] = np.median(temp_shifted[i_snap][ok])
        temp_lo[i_snap] = us.Weighted_Percentile(temp_shifted[i_snap][ok],np.ones_like(temp_shifted[i_snap,ok]),0.16)
        temp_hi[i_snap] = us.Weighted_Percentile(temp_shifted[i_snap][ok],np.ones_like(temp_shifted[i_snap,ok]),0.84)

    py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                        'figure.figsize':[6.64*2,4.98*2],
                        'figure.subplot.left':0.05,
                        'figure.subplot.bottom':0.09,
                        'figure.subplot.top':0.98,
                        'font.size':12,
                        'legend.fontsize':4})

    def vert_lines():
        ds = (snap_list[-1]-snap_list[0])
        line_range = np.arange(0,1.1*ds,0.1*ds) + snap_list[0]
        for line in line_range:
            py.axvline(line,alpha=0.1)

            
    py.figure()
    py.subplot(221)

    py.scatter(time_shifted,radius_med,c="k")
    py.plot(time_shifted, radius_med,c="k",label="radius")
    py.plot(time_shifted, radius_hi,c="k")
    py.plot(time_shifted, radius_lo,c="k")
    
    py.axhline(1.0,c="k",alpha=0.2)
    py.axvline(0.0,c="k",alpha=0.2)
    py.ylim((0,2.5))
    py.xlim((time_shifted.min(),time_shifted.max()))
    vert_lines()
    #py.legend(loc="center left")
    py.ylabel(r"$r \, / R_{\mathrm{vir}}$")
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")

    py.subplot(222)
    py.plot(time_shifted, cv_med,label="radial velocity",c="b")
    py.plot(time_shifted, cv_lo,c="b")
    py.plot(time_shifted, cv_hi,c="b")
    py.ylabel(r"$v$")
    py.axhline(0.0,alpha=0.1,c="k")
        
    py.plot(t_list-t_choose, vmax,c="k",label="Vmax",alpha=0.5)
    py.legend(loc="lower right")
    vert_lines()
    py.xlim((time_shifted.min(),time_shifted.max()))
    py.ylim((-1.5*np.median(vmax[(np.isnan(vmax)==False)&(vmax<2000)]), 1.5*np.median(vmax[(np.isnan(vmax)==False)&(vmax<2000)])))
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")

    py.subplot(223)

    py.plot(time_shifted, np.log10(temp_med))
    py.plot(time_shifted, np.log10(temp_lo))
    py.plot(time_shifted, np.log10(temp_hi))
    py.ylabel(r"$\log(T)$")
    vert_lines()
    py.xlim((time_shifted.min(),time_shifted.max()))
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")
    
    py.subplot(224)
    py.plot(time_shifted, np.log10(nh_med))
    py.plot(time_shifted, np.log10(nh_lo))
    py.plot(time_shifted, np.log10(nh_hi))
    py.ylabel(r"$\log(n_{\mathrm{H}})$")
    vert_lines()
    py.xlim((time_shifted.min(),time_shifted.max()))
    py.xlabel("$t - t_{\mathrm{acc}} \, / \mathrm{Gyr}$")

    py.savefig("Temp/ind_example_virgo_talk.pdf")
    py.show()
    
def Plot_Gas_Particles(coordinates, c, ps, dim="xy"):

    x_part = coordinates[:,0]
    y_part = coordinates[:,1]
    z_part = coordinates[:,2]

    if dim == "xy":
        ok = (z_part > -30) & (z_part < 30)
        py.xlabel(r"$x \, / \mathrm{kpc}$")
        py.ylabel(r"$y \, / \mathrm{kpc}$")
        py.scatter( x_part[ok], y_part[ok] , edgecolors="none",s=ps,c=c,rasterized=True)

    if dim == "xz":
        ok = (y_part > -30) & (y_part < 30)
        py.xlabel(r"$x \, / \mathrm{kpc}$")
        py.ylabel(r"$y \, / \mathrm{kpc}$")
        py.scatter( x_part[ok], z_part[ok] , edgecolors="none",s=ps,c=c,rasterized=True)

def Plot_Tracked_Particles(coordinates_list, flag1_list, c, redshift_list, h,sample_rate=0.05,dim="xy"):

    # Only show particles that are always inside the z selection range (unless they are lost from subvolume)                                                                                                                          
    ok_part = np.zeros(len(coordinates_list[0]))==0.0

    coordinates_list = np.array(coordinates_list)

    n_show = 0
    for i_part in range(len(coordinates_list[0,:,0])):

        if not ok_part[i_part] or np.random.random(1)[0]>sample_rate:
            continue

        n_show += 1

        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        if dim == "xy":
            x_plot = x_part; y_plot = y_part
        elif dim == "xz":
            x_plot = x_part; y_plot = z_part
        else:
            print "Error :)"
            quit()

        py.plot( x_plot, y_plot ,c=c)

        print "particle",i_part,"flag1=",flag1_list[:,i_part]

        # Change track colour depending on state of the gas particle                                                                                                                                                                  
        for i_snap in range(len(x_plot)-1):
            if flag1_list[i_snap][i_part] == 1 and flag1_list[i_snap+1][i_part] == 1:
                py.plot([x_plot[i_snap],x_plot[i_snap+1]], [y_plot[i_snap],y_plot[i_snap+1]] ,c="g")

            if flag1_list[i_snap][i_part] == 2 and flag1_list[i_snap+1][i_part] == 2:
                py.plot([x_plot[i_snap],x_plot[i_snap+1]], [y_plot[i_snap],y_plot[i_snap+1]] ,c="k")

            x_mid = 0.5*(x_plot[i_snap] + x_plot[i_snap+1])
            y_mid = 0.5*(y_plot[i_snap] + y_plot[i_snap+1])
            z_mid = 0.5*(z_part[i_snap] + z_part[i_snap+1])

            if flag1_list[i_snap][i_part] != 1 and flag1_list[i_snap+1][i_part] == 1:
                py.plot( [x_mid, x_plot[i_snap+1]], [y_mid, y_plot[i_snap+1]], c="g")

            if flag1_list[i_snap][i_part] == 1 and flag1_list[i_snap+1][i_part] != 1:
                py.plot( [x_plot[i_snap], x_mid], [y_plot[i_snap], y_mid], c="g")

            if flag1_list[i_snap][i_part] != 2 and flag1_list[i_snap+1][i_part] == 2:
                py.plot( [x_mid, x_plot[i_snap+1]], [y_mid, y_plot[i_snap+1]], c="k")

            if flag1_list[i_snap][i_part] == 2 and flag1_list[i_snap+1][i_part] != 2:
                py.plot( [x_plot[i_snap], x_mid], [y_plot[i_snap], y_mid], c="k")

        # Mark start position and end position where particle disappears                                                                                                                                                              
        dis = np.isnan(x_plot)==False
        if len(dis[dis==False])>0:
            py.scatter(x_plot[dis][-1], y_plot[dis][-1], c="r", s=15,edgecolors="none")
        else:
            py.scatter(x_plot[-1], y_plot[-1], c="g", s=15,edgecolors="none")

        py.scatter(x_plot[0], y_plot[0], c="y",s=15,edgecolors="none")

    print "showing tracks for", n_show,"particles of", len(coordinates_list[0,:,0])

def Plot_Rmax_Dist(id_list, coordinates_list, redshift_list, vmax_list, flag1_list, velocity_list, temperature_list, internal_energy_list, select_wind, h,omm, radius_store, subvol, visualize_winds,monotonic,use_ns=2,cum=False):
    
    kpc2km = 3.0857e16        
    Gyr2s = 3.15576e16
    
    a_list = 1./(1.+redshift_list)
    t_list, junk = uc.t_Universe(a_list, omm, h)

    print "t_list", t_list

    coordinates_list = np.array(coordinates_list)
    velocity_list = np.array(velocity_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)

    x_gas_ps = np.copy(coordinates_list[0,:,0])
    y_gas_ps = np.copy(coordinates_list[0,:,1])
    z_gas_ps = np.copy(coordinates_list[0,:,2])
    r_gas = np.sqrt(np.square(x_gas_ps)+(np.square(y_gas_ps))+np.square(z_gas_ps))
    r_gas_norm = np.swapaxes(np.array([x_gas_ps,y_gas_ps,z_gas_ps]) / r_gas,0,1)
    vrad_ps = np.sum(r_gas_norm * velocity_list[0],axis=1)

    x_gas_ts = np.copy(coordinates_list[1,:,0])
    y_gas_ts = np.copy(coordinates_list[1,:,1])
    z_gas_ts = np.copy(coordinates_list[1,:,2])
    r_gas = np.sqrt(np.square(x_gas_ts)+(np.square(y_gas_ts))+np.square(z_gas_ts))
    r_gas_norm = np.swapaxes(np.array([x_gas_ts,y_gas_ts,z_gas_ts]) / r_gas,0,1)
    vrad_ts = np.sum(r_gas_norm * velocity_list[1],axis=1)

    delta_v = vrad_ts - vrad_ps

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    T_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    # Deal with particles that exit the halo, but pass through another halo on their way out (becomes a small issue with particle rebinding within r200)
    if not visualize_winds:
        flythrough = (flag1_list[1] == 0) & (flag1_list[2] == 1)
        flag1_list[1][flythrough] = 1
        
    ncounter = 0

    for i_part in range(len(coordinates_list[0,:,0])):


        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        if i_flag == 1:
            print "Warning, particle", i_part, "has i_flag = 1, which means it shouldn't have been selected as a reheated/ejected particle"
            continue

        # Joop suggestion to show r_max - r_init
        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        r_part_copy = np.copy(r_part)

        r_part_norm = np.swapaxes(np.array([x_part,y_part,z_part]) / r_part,0,1)
        vrad_part = np.sum(r_part_norm * velocity_list[:,i_part],axis=1)

        # For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        # Note this option has now been expanded to included a series of different criteria

        r_part[i_flag:] = 0.0

        if monotonic and visualize_winds:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]

            if len(non_mono)>0:

                non_mono[0:use_ns-1] = 0
                r_part[non_mono] = 0.0

            # Bit unconvinced by this - check if it still needs to be updated
            # I started updating it to use use_ns correctly, but gave up part way through
            '''# The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            v_ta_21 = (r_part[use_ns] - r_part[1])* kpc2km / (t_list[use_ns]-t_list[1]) / Gyr2s
            v_ta_10 = (r_part[1] - r_part[0])* kpc2km / (t_list[1]-t_list[0]) / Gyr2s
            for i_t in range(len(r_part[use_ns+1:])):

                v_ta_n = (r_part[i_t+2] - r_part[i_t+1])* kpc2km / (t_list[i_t+2]-t_list[i_t+1]) / Gyr2s
                v_ta_np1 = (r_part[i_t+3] - r_part[i_t+2])* kpc2km / (t_list[i_t+3]-t_list[i_t+2]) / Gyr2s

                if v_ta_np1 > v_ta_n and v_ta_21 < 0.19*vmax_list[1] and v_ta_10 < 0.19*vmax_list[1] and v_ta_np1 > 0:
                    r_part[i_t+3:] = 0.0
                    break'''
            
            '''# Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**3:
                    r_part[i_t+2:] = 0.0
                    break
            if use_ns > 2:
                if T_part[use_ns] - T_part[1] > 10**4.75 and T_part[1] - T_part[0] < 10**3:
                    r_part[use_ns:] = 0.0

                for i_t in range(len(r_part[use_ns+1:])):
                    if T_part[i_t+use_ns+1] - T_part[i_t+use_ns] > 10**4.75 and T_part[1] - T_part[0] < 10**3:
                        r_part[i_t+use_ns+1:] = 0.0
                        break
            if use_ns < 2:
                print "use_ns should be >=2"
                quit()'''

        if monotonic and not visualize_winds:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]

            if len(non_mono)>0:
                r_part[non_mono[0]+1:] = 0.0

        if visualize_winds:
            r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)
            ind_max = np.argmax(r_part[1:i_flag]-r_part0)
        else: # Now that I use r200 as hard cut with rebinding - makes sense to use this instead
            r_max[i_part] = np.max(r_part[1:i_flag]-radius_store[4])
            ind_max = np.argmax(r_part[1:i_flag]-radius_store[4])

        hmm = select_wind[i_part] & (r_max[i_part] <10)
        if hmm:
            print ""
            print id_list[i_part], r_max[i_part], "vmax 1,0.25,0.125", vmax_list[1], vmax_list[1]*0.25, vmax_list[1]*0.125, "r200", radius_store[4]
            print "r_part (used for Rmax)", r_part
            print "r_part full", r_part_copy
            print "t_part", temperature_list[:,i_part]
            print "vrad_part", vrad_part
            print "flag1", flag1_list[:,i_part]
            ncounter += 1

    print "ncounter", ncounter

    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    py.figure()
    bins = np.append(np.arange(-50,50,2),np.arange(50, r_max_max+10, 10))

    if cum:
        weight_part = np.ones_like(r_max)
    else:
        weight_part = np.ones_like(r_max); weight_part[r_max > 50] *= 0.2

    nsf =flag1_list[0]==3
    sf = flag1_list[0]==0

    py.subplot(211)
    py.hist(r_max[sf], bins=bins, weights = weight_part[sf],color="k",histtype="step",cumulative=cum)
    py.hist(r_max[select_wind&sf], bins=bins, weights = weight_part[select_wind&sf], color="b",alpha=0.5, label="fraction vmax",cumulative=cum)

    for i_rad, radius in enumerate(radius_store):
        if not visualize_winds and i_rad <4:
            continue
        if i_rad < 2:
            py.axvline(radius,c="g")
        elif i_rad<4:
            py.axvline(radius,c="c")
        else:
            py.axvline(radius,c="k")
        print "radius", radius
    
    py.legend(frameon=False,loc="upper right")

    exp_fac_l = 1./(redshift_list+1)

    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    if cum:
        py.ylabel(r"$N(r_{\mathrm{max}}-r{\mathrm{init}} < r)$")
    else:
        py.ylabel(r"$N / \Delta r \, / \mathrm{kpc^{-1}}$")

    py.subplot(212)
    py.hist(r_max[nsf], bins=bins, weights = weight_part[nsf],color="k",histtype="step",cumulative=cum)
    py.hist(r_max[select_wind&nsf], bins=bins, weights = weight_part[select_wind&nsf], color="b",alpha=0.5, label="fraction vmax",cumulative=cum)

    py.show()

def Plot_R_t(coordinates_list, redshift_list,flag1_list):

    exp_fac_l = 1./(redshift_list+1)

    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    coordinates_list = np.array(coordinates_list)

    r = np.zeros(len(coordinates_list[0,:,0]))

    py.figure()
    '''    py.subplot(211)                                                                                                                                                                                                            
                                                                                                                                                                                                                                      
    for i_part in range(len(coordinates_list[0,:,0])):                                                                                                                                                                                
                                                                                                                                                                                                                                      
        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)                                                                                                                                                                                                   
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]                                                                                                                                               
        if len(rejoin>0):                                                                                                                                                                                                             
            i_flag = rejoin[0]                                                                                                                                                                                                        
                                                                                                                                                                                                                                      
        x_part = np.copy(coordinates_list[:,i_part,0])                                                                                                                                                                                
        y_part = np.copy(coordinates_list[:,i_part,1])                                                                                                                                                                                
        z_part = np.copy(coordinates_list[:,i_part,2])                                                                                                                                                                                
                                                                                                                                                                                                                                      
        x_part *= 1e3 /h / (1+redshift_list)                                                                                                                                                                                          
        y_part *= 1e3 /h / (1+redshift_list)                                                                                                                                                                                          
        z_part *= 1e3 /h / (1+redshift_list)                                                                                                                                                                                          
                                                                                                                                                                                                                                      
        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))                                                                                                                                                  
        r_part0 = r_part[0]                                                                                                                                                                                                           
                                                                                                                                                                                                                                      
        py.plot(t[0:i_flag]*1e3, r_part[0:i_flag]-r_part0)                                                                                                                                                                            
                                                                                                                                                                                                                                      
    # First panel shows zoomed picture                                                                                                                                                                                                
    py.xlim((t[0]*1e3, +t[3]*1e3))                                                                                                                                                                                                    
    py.ylim((0, 30))                                                                                                                                                                                                                  
    py.xlabel(r"$t \, / \mathrm{Myr}$")                                                                                                                                                                                               
    py.ylabel(r"$r - r_{\mathrm{0}} \, / \mathrm{pkpc}$")'''

    py.subplot(212)

    r_part_dist = []

    for i_part in range(len(coordinates_list[0,:,0])):

        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        x_part = np.copy(coordinates_list[:,i_part,0])
        y_part = np.copy(coordinates_list[:,i_part,1])
        z_part = np.copy(coordinates_list[:,i_part,2])

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        if not i_flag == 5:
            continue
        #if not i_flag == 3:                                                                                                                                                                                                          
        #    continue                                                                                                                                                                                                                 

        # Second panel shows full picture                                                                                                                                                                                             
        py.plot(t[0:i_flag]*1e3, r_part[0:i_flag]-r_part0,alpha=0.5)

        # Save distn                                                                                                                                                                                                                  
        r_part_dist.append(r_part-r_part0)
        r_part_dist[-1][i_flag:] = -999

    r_part_dist = np.array(r_part_dist)

    for i_ts in range(len(redshift_list)):
        ok = r_part_dist[:,i_ts] >= -990
        if len(ok[ok])> 0:
            lo = us.Weighted_Percentile(r_part_dist[:,i_ts][ok],np.ones_like(r_part_dist[:,i_ts][ok]),0.1)
            med = us.Weighted_Percentile(r_part_dist[:,i_ts][ok],np.ones_like(r_part_dist[:,i_ts][ok]),0.5)
            hi = us.Weighted_Percentile(r_part_dist[:,i_ts][ok],np.ones_like(r_part_dist[:,i_ts][ok]),0.9)
            py.errorbar([t[i_ts]*1e3], [med], [[abs(med-lo)],[abs(med-hi)]],c="k",marker="x")

            print "time",t[i_ts]*1e3,"fraction that made it past 10kpc, 20kpc is", len(r_part_dist[:,i_ts][ok][r_part_dist[:,i_ts][ok]>10])/ float(len(r_part_dist[:,i_ts][ok])), len(r_part_dist[:,i_ts][ok][r_part_dist[:,i_ts][ok]>20])/ float(len(r_part_dist[:,i_ts][ok]))

    py.xlabel(r"$t \, / \mathrm{Myr}$")
    py.ylabel(r"$r - r_{\mathrm{0}} \, / \mathrm{pkpc}$")

    py.show()






def Plot_v_rmax(coordinates_list, redshift_list, flag1_list, velocity_list, temperature_list, internal_energy_list, h, monotonic):

    # Maximum radius to go out to (vanished particles also appear at this radius)
    r_max_max = 500

    coordinates_list = np.array(coordinates_list)
    velocity_list = np.array(velocity_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)

    x_gas_ps = np.copy(coordinates_list[0,:,0])
    y_gas_ps = np.copy(coordinates_list[0,:,1])
    z_gas_ps = np.copy(coordinates_list[0,:,2])
    r_gas = np.sqrt(np.square(x_gas_ps)+(np.square(y_gas_ps))+np.square(z_gas_ps))
    r_gas_norm = np.swapaxes(np.array([x_gas_ps,y_gas_ps,z_gas_ps]) / r_gas,0,1)
    vrad_ps = np.sum(r_gas_norm * velocity_list[0],axis=1)

    x_gas_ts = np.copy(coordinates_list[1,:,0])
    y_gas_ts = np.copy(coordinates_list[1,:,1])
    z_gas_ts = np.copy(coordinates_list[1,:,2])
    r_gas = np.sqrt(np.square(x_gas_ts)+(np.square(y_gas_ts))+np.square(z_gas_ts))
    r_gas_norm = np.swapaxes(np.array([x_gas_ts,y_gas_ts,z_gas_ts]) / r_gas,0,1)
    vrad_ts = np.sum(r_gas_norm * velocity_list[1],axis=1)

    delta_v = vrad_ts - vrad_ps

    exp_fac_l = 1./(redshift_list+1)

    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    r_max = np.zeros_like(vrad_ts)
    delta_r = np.zeros_like(vrad_ts)

    for i_part in range(len(coordinates_list[0,:,0])):
 
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        x_part = np.copy(coordinates_list[:,i_part,0])
        y_part = np.copy(coordinates_list[:,i_part,1])
        z_part = np.copy(coordinates_list[:,i_part,2])

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]
        delta_r[i_part] = r_part[1] - r_part[0]

        r_part_norm = np.swapaxes(np.array([x_part,y_part,z_part]) / r_part,0,1)
        vrad_part = np.sum(r_part_norm * velocity_list[:,i_part],axis=1)

        # For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        # Note this option has now been expanded to included a series of different criteria
        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                if non_mono[0] == 0:
                    i_mono = 2
                elif non_mono[0] ==1:
                    i_mono = 1
                else:
                    i_mono = 0
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            for i_t in range(len(r_part[3:])):
                if r_part[i_t+3] - r_part[i_t+2] > 2 * r_part[i_t+2] - r_part[i_t+1] and r_part[2] - r_part[1] < 20 and r_part[2] - r_part[0] < 30:
                    # Note to be done properly, this should be done on velocities - not distances! (should probably be based on a fraction of halo circular velocity also)
                    '''if r_part[i_t+2:].max() > 0:
                        jim = 1
                        print ""
                        print i_t
                        print r_part[i_t+3] - r_part[i_t+2], 2 * r_part[i_t+2] - r_part[i_t+1]
                        print r_part
                    else:
                        jim = 0'''
                    r_part[i_t+3:] = 0.0
                    '''if jim == 1:
                        print r_part'''
                    break

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break

            # Finally redux - look for particles that don't increase their energy between _ps and _ts
            Eth_part = internal_energy_list[:,i_part] # km2 s^-2
            Ek_rad_part = 0.5 * np.square(vrad_part) # km2 s^-2
            Etot_part = Eth_part + Ek_rad_part

            if Etot_part[1] < Etot_part[0]:
                r_part[2:] = 0.0

        r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)


    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    py.figure()

    py.subplot(211)
    py.scatter(vrad_ts, r_max, edgecolors="none", s=10, c=delta_r)

    nbins = 10
    xmin = vrad_ts[np.isnan(vrad_ts)==False].min()
    xmax = vrad_ts[np.isnan(vrad_ts)==False].max()
    dx = (xmax - xmin) / nbins

    bins = np.arange(xmin, xmax+dx, dx)
    med, lo_perc, hi_perc, mid_bin = us.Calculate_Percentiles(bins, vrad_ts, r_max, min_count=1, lohi=(0.16,0.84))

    py.plot(mid_bin,med,c="k",linewidth=1)
    py.plot(mid_bin,lo_perc,c="k",linewidth=0.7,linestyle='--')
    py.plot(mid_bin,hi_perc,c="k",linewidth=0.7,linestyle='--')

    py.xlabel(r"$v_{\mathrm{rad}} \, / \mathrm{kms^{-1}} \, \mathrm{[After 1 step]}$")
    py.ylabel(r"$r_{\mathrm{max}} - r_{\mathrm{0}} \, / \mathrm{pkpc}$")

    py.subplot(212)
    py.scatter(delta_v, r_max, edgecolors="none", s=10, c=delta_r)

    nbins = 10
    xmin = vrad_ts[np.isnan(delta_v)==False].min()
    xmax = vrad_ts[np.isnan(delta_v)==False].max()
    dx = (xmax - xmin) / nbins

    bins = np.arange(xmin, xmax+dx, dx)
    med, lo_perc, hi_perc, mid_bin = us.Calculate_Percentiles(bins, delta_v, r_max, min_count=1, lohi=(0.16,0.84))

    py.plot(mid_bin,med,c="k",linewidth=1)
    py.plot(mid_bin,lo_perc,c="k",linewidth=0.7,linestyle='--')
    py.plot(mid_bin,hi_perc,c="k",linewidth=0.7,linestyle='--')

    py.xlabel(r"$\Delta v_{\mathrm{rad}} \, / \mathrm{kms^{-1}}$")
    py.ylabel(r"$r_{\mathrm{max}} - r_{\mathrm{0}} \, / \mathrm{pkpc}$")


    py.show()



def Plot_Tmax_Rmax(coordinates_list, redshift_list, vmax_list, flag1_list, velocity_list, temperature_list,internal_energy_list, h,omm,visualize_winds,monotonic, use_ns=2):

    a_list = 1./(1.+redshift_list)
    t_list, junk = uc.t_Universe(a_list, omm, h)
    
    vmax_list = np.array(vmax_list)
    coordinates_list = np.array(coordinates_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)
    velocity_list = np.array(velocity_list)

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    T_max = np.zeros(len(coordinates_list[0,:,0]))
    dT_max = np.zeros(len(coordinates_list[0,:,0]))
    Tfrac_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    T_max = np.zeros(len(coordinates_list[0,:,0]))
    dT_max = np.zeros(len(coordinates_list[0,:,0]))
    Tfrac_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    for i_part in range(len(coordinates_list[0,:,0])):


        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        # Joop suggestion to show r_max - r_init                                                                                                                                                                                      
        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        # For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                i_mono = 0
                if non_mono[i_mono] == 0:
                    i_mono += 1
                
                #if i_mono == 0 or len(non_mono) > 1:
                #    if non_mono[i_mono] ==1:
                #        i_mono += 1
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            v_ta_21 = (r_part[2] - r_part[1])* kpc2km / (t_list[2]-t_list[1]) / Gyr2s
            v_ta_10 = (r_part[1] - r_part[0])* kpc2km / (t_list[1]-t_list[0]) / Gyr2s
            #print_cut = False
            for i_t in range(len(r_part[3:])):

                v_ta_n = (r_part[i_t+2] - r_part[i_t+1])* kpc2km / (t_list[i_t+2]-t_list[i_t+1]) / Gyr2s
                v_ta_np1 = (r_part[i_t+3] - r_part[i_t+2])* kpc2km / (t_list[i_t+3]-t_list[i_t+2]) / Gyr2s

                if v_ta_np1 > v_ta_n and v_ta_21 < 0.19*vmax_list[1] and v_ta_10 < 0.19*vmax_list[1] and v_ta_np1 > 0:

                    #print "I'm applying a", i_t+4, i_t+3, "cut"
                    #print v_ta_np1, v_ta_n, v_ta_21, 0.25*vmax_list[1], v_ta_10, 0.38*vmax_list[1]
                    #print "r_part before", r_part
                    r_part[i_t+3:] = 0.0
                    #print "r_part after", r_part
                    #print_cut = True
                    break
            
            #if not print_cut:
            #    print "yo I didn't bother", v_ta_21, v_ta_10, (r_part[1:] - r_part[0:-1]) / (t_list[1:]-t_list[0:-1])

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break
            if use_ns > 2:
                if T_part[use_ns] - T_part[1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[use_ns:] = 0.0
                    
                for i_t in range(len(r_part[use_ns+1:])):
                    if T_part[i_t+use_ns+1] - T_part[i_t+use_ns] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                        r_part[i_t+use_ns+1:] = 0.0
                        break
            if use_ns < 2:
                print "use_ns should be >=2"
                quit()


        r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)
        ind_max = np.argmax(r_part[1:i_flag]-r_part0)
        inds_use = np.arange(0,ind_max+1)
        T_max[i_part] = np.max(temperature_list[:,i_part][inds_use])

        if len(inds_use)>1:
            dT_temp = temperature_list[:,i_part][inds_use][1:] - temperature_list[:,i_part][inds_use][0:-1]
            dT_max[i_part] = np.max(dT_temp)

            Tfrac_temp = temperature_list[:,i_part][inds_use][1:] / temperature_list[:,i_part][inds_use][0:-1]
            Tfrac_max[i_part] = np.max(Tfrac_temp)

        else:
            dT_max[i_part] = 1e3 # Just so that they show on the plot
            Tfrac_max[i_part] = 1.0

    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    exp_fac_l = 1./(redshift_list+1)
    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    '''py.figure()

    py.subplot(221)
    py.scatter(r_max, np.log10(T_max), edgecolors="none", s=10)



    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(T_{\mathrm{max}} \, / \mathrm{K})$")

    py.subplot(222)
    py.scatter(r_max, np.log10(dT_max), edgecolors="none", s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(\Delta T_{\mathrm{max}} \, / \mathrm{K})$")

    py.subplot(223)
    py.scatter(r_max, np.log10(Tfrac_max), edgecolors="none", s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}((T_{n+1}/T{n})_{\mathrm{max}} )$")

    py.show()'''

    py.figure()
    py.scatter(np.log10(T_max),r_max,edgecolors="none",s=10)
    py.ylabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.xlabel(r"$\log_{10}(T_{\mathrm{max}} \, / \mathrm{K})$")

    nbins = 20
    xmin = np.log10(T_max)[np.isnan(T_max)==False].min()
    xmax = np.log10(T_max)[np.isnan(T_max)==False].max()
    dx = (xmax - xmin) / nbins

    bins = np.arange(xmin, xmax+dx, dx)
    med, lo_perc, hi_perc, mid_bin = us.Calculate_Percentiles(bins, np.log10(T_max), r_max, min_count=1, lohi=(0.16,0.84))

    py.plot(mid_bin,med,c="k",linewidth=1)
    py.plot(mid_bin,lo_perc,c="k",linewidth=0.7,linestyle='--')
    py.plot(mid_bin,hi_perc,c="k",linewidth=0.7,linestyle='--')

    py.show()

def Plot_dEth_r_Rmax(coordinates_list, redshift_list, flag1_list, velocity_list, temperature_list,internal_energy_list, h,visualize_winds,monotonic):

    coordinates_list = np.array(coordinates_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)
    velocity_list = np.array(velocity_list)

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    T_max = np.zeros(len(coordinates_list[0,:,0]))
    dT_max = np.zeros(len(coordinates_list[0,:,0]))
    Tfrac_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    Etot_ts = np.zeros_like(r_max)
    Etot_ps = np.zeros_like(r_max)

    for i_part in range(len(coordinates_list[0,:,0])):


        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        # Joop suggestion to show r_max - r_init                                                                                                                                                                                      
        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        r_part_norm = np.swapaxes(np.array([x_part,y_part,z_part]) / r_part,0,1)
        vrad_part = np.sum(r_part_norm * velocity_list[:,i_part],axis=1)

        # For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                if non_mono[0] == 0:
                    i_mono = 2
                elif non_mono[0] ==1:
                    i_mono = 1
                else:
                    i_mono = 0
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            for i_t in range(len(r_part[3:])):
                if r_part[i_t+3] - r_part[i_t+2] > 2 * r_part[i_t+2] - r_part[i_t+1] and r_part[2] - r_part[1] < 20 and r_part[2] - r_part[0] < 30:
                    # Note to be done properly, this should be done on velocities - not distances! (should probably be based on a fraction of halo circular velocity also)
                    '''if r_part[i_t+2:].max() > 0:
                        jim = 1
                        print ""
                        print i_t
                        print r_part[i_t+3] - r_part[i_t+2], 2 * r_part[i_t+2] - r_part[i_t+1]
                        print r_part
                    else:
                        jim = 0'''
                    r_part[i_t+3:] = 0.0
                    '''if jim == 1:
                        print r_part'''
                    break

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break

            # Finally redux - look for particles that don't increase their energy between _ps and _ts
            Eth_part = internal_energy_list[:,i_part] # km2 s^-2
            Ek_rad_part = 0.5 * np.square(vrad_part) # km2 s^-2
            Etot_part = Eth_part + Ek_rad_part

            if Etot_part[1] < Etot_part[0]:
                r_part[2:] = 0.0

        r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)
        Etot_ts[i_part] = Etot_part[1]
        Etot_ps[i_part] = Etot_part[0]


    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    py.figure()

    py.subplot(221)
    py.scatter(r_max, np.log10(Etot_ts), edgecolors="none", s=10)

    exp_fac_l = 1./(redshift_list+1)

    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(E_{th_r} \, / \mathrm{km^2 s^{-2}})$")

    py.subplot(222)
    py.scatter(r_max, np.log10(Etot_ts-Etot_ps), edgecolors="none", s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(\Delta E_{\mathrm{th_r}} \, / \mathrm{km^2 s^{-2}})$")

    py.subplot(223)
    py.scatter(r_max, np.log10(Etot_ts/Etot_ps), edgecolors="none", s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(E_{\mathrm{1}} / E_{\mathrm{0}})$")

    py.show()

def Plot_dT_Rmax(coordinates_list, redshift_list, vmax_list, flag1_list, velocity_list, temperature_list,internal_energy_list, h,omm,visualize_winds,monotonic, use_ns=2):

    a_list = 1./(1.+redshift_list)
    t_list, junk = uc.t_Universe(a_list, omm, h)
    
    vmax_list = np.array(vmax_list)
    coordinates_list = np.array(coordinates_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)
    velocity_list = np.array(velocity_list)

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    T_max = np.zeros(len(coordinates_list[0,:,0]))
    dT_max = np.zeros(len(coordinates_list[0,:,0]))
    Tfrac_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    T_ts = np.zeros_like(r_max)
    T_ps = np.zeros_like(r_max)

    for i_part in range(len(coordinates_list[0,:,0])):


        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        # Joop suggestion to show r_max - r_init                                                                                                                                                                                      
        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        '''# For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                if non_mono[0] == 0:
                    i_mono = 2
                elif non_mono[0] ==1:
                    i_mono = 1
                else:
                    i_mono = 0
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            for i_t in range(len(r_part[3:])):
                if r_part[i_t+3] - r_part[i_t+2] > 2 * r_part[i_t+2] - r_part[i_t+1] and r_part[2] - r_part[1] < 20 and r_part[2] - r_part[0] < 30:
                    # Note to be done properly, this should be done on velocities - not distances! (should probably be based on a fraction of halo circular velocity also)
                    r_part[i_t+3:] = 0.0
                    break

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break

            # Finally redux - look for particles that don't increase their energy between _ps and _ts
            Eth_part = internal_energy_list[:,i_part] # km2 s^-2
            Ek_rad_part = 0.5 * np.square(vrad_part) # km2 s^-2
            Etot_part = Eth_part + Ek_rad_part

            if Etot_part[1] < Etot_part[0]:
                r_part[2:] = 0.0'''

        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                i_mono = 0
                if non_mono[i_mono] == 0:
                    i_mono += 1
                
                #if i_mono == 0 or len(non_mono) > 1:
                #    if non_mono[i_mono] ==1:
                #        i_mono += 1
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            v_ta_21 = (r_part[2] - r_part[1])* kpc2km / (t_list[2]-t_list[1]) / Gyr2s
            v_ta_10 = (r_part[1] - r_part[0])* kpc2km / (t_list[1]-t_list[0]) / Gyr2s
            #print_cut = False
            for i_t in range(len(r_part[3:])):

                v_ta_n = (r_part[i_t+2] - r_part[i_t+1])* kpc2km / (t_list[i_t+2]-t_list[i_t+1]) / Gyr2s
                v_ta_np1 = (r_part[i_t+3] - r_part[i_t+2])* kpc2km / (t_list[i_t+3]-t_list[i_t+2]) / Gyr2s

                if v_ta_np1 > v_ta_n and v_ta_21 < 0.19*vmax_list[1] and v_ta_10 < 0.19*vmax_list[1] and v_ta_np1 > 0:

                    #print "I'm applying a", i_t+4, i_t+3, "cut"
                    #print v_ta_np1, v_ta_n, v_ta_21, 0.25*vmax_list[1], v_ta_10, 0.38*vmax_list[1]
                    #print "r_part before", r_part
                    r_part[i_t+3:] = 0.0
                    #print "r_part after", r_part
                    #print_cut = True
                    break
            
            #if not print_cut:
            #    print "yo I didn't bother", v_ta_21, v_ta_10, (r_part[1:] - r_part[0:-1]) / (t_list[1:]-t_list[0:-1])

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break
            if use_ns > 2:
                if T_part[use_ns] - T_part[1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[use_ns:] = 0.0
                    
                for i_t in range(len(r_part[use_ns+1:])):
                    if T_part[i_t+use_ns+1] - T_part[i_t+use_ns] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                        r_part[i_t+use_ns+1:] = 0.0
                        break
            if use_ns < 2:
                print "use_ns should be >=2"
                quit()


        r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)
        T_ts[i_part] = T_part[use_ns]
        T_ps[i_part] = T_part[0]
        

    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    py.figure()

    py.subplot(221)
    py.scatter(r_max, np.log10(T_ts), edgecolors="none", s=10)

    exp_fac_l = 1./(redshift_list+1)

    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(T \, / \mathrm{K})$")

    py.subplot(222)
    py.scatter(r_max, np.log10(T_ts-T_ps), edgecolors="none", s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(\Delta T \, / \mathrm{K})$")

    py.subplot(223)
    py.scatter(r_max, np.log10(T_ts/T_ps), edgecolors="none", s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\log_{10}(T_{\mathrm{1}} / T_{\mathrm{0}})$")

    py.show()




def Plot_tmax_Rmax(coordinates_list, redshift_list, vmax_list, flag1_list, velocity_list, temperature_list,internal_energy_list, h,omm,visualize_winds,monotonic, use_ns=2):

    a_list = 1./(1.+redshift_list)
    t_list, junk = uc.t_Universe(a_list, omm, h)
    
    vmax_list = np.array(vmax_list)
    coordinates_list = np.array(coordinates_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)
    velocity_list = np.array(velocity_list)

    r_max_max = 500

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    t_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    for i_part in range(len(coordinates_list[0,:,0])):


        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        # Joop suggestion to show r_max - r_init                                                                                                                                                                                      
        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        r_part_copy = np.copy(r_part)

        # For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                i_mono = 0
                if non_mono[i_mono] == 0:
                    i_mono += 1
                
                #if i_mono == 0 or len(non_mono) > 1:
                #    if non_mono[i_mono] ==1:
                #        i_mono += 1
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            v_ta_21 = (r_part[2] - r_part[1])* kpc2km / (t_list[2]-t_list[1]) / Gyr2s
            v_ta_10 = (r_part[1] - r_part[0])* kpc2km / (t_list[1]-t_list[0]) / Gyr2s
            #print_cut = False
            for i_t in range(len(r_part[3:])):

                v_ta_n = (r_part[i_t+2] - r_part[i_t+1])* kpc2km / (t_list[i_t+2]-t_list[i_t+1]) / Gyr2s
                v_ta_np1 = (r_part[i_t+3] - r_part[i_t+2])* kpc2km / (t_list[i_t+3]-t_list[i_t+2]) / Gyr2s

                if v_ta_np1 > v_ta_n and v_ta_21 < 0.19*vmax_list[1] and v_ta_10 < 0.19*vmax_list[1] and v_ta_np1 > 0:

                    #print "I'm applying a", i_t+4, i_t+3, "cut"
                    #print v_ta_np1, v_ta_n, v_ta_21, 0.25*vmax_list[1], v_ta_10, 0.38*vmax_list[1]
                    #print "r_part before", r_part
                    r_part[i_t+3:] = 0.0
                    #print "r_part after", r_part
                    #print_cut = True
                    break
            
            #if not print_cut:
            #    print "yo I didn't bother", v_ta_21, v_ta_10, (r_part[1:] - r_part[0:-1]) / (t_list[1:]-t_list[0:-1])

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break
            if use_ns > 2:
                if T_part[use_ns] - T_part[1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[use_ns:] = 0.0
                    
                for i_t in range(len(r_part[use_ns+1:])):
                    if T_part[i_t+use_ns+1] - T_part[i_t+use_ns] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                        r_part[i_t+use_ns+1:] = 0.0
                        break
            if use_ns < 2:
                print "use_ns should be >=2"
                quit()


        r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)
        ind_max = np.argmax(r_part[1:i_flag]-r_part0)
        t_max[i_part] = (np.max(t_list[ind_max+1]) - t_list[1]) * 1e3

    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    exp_fac_l = 1./(redshift_list+1)
    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    py.figure()
    py.scatter(r_max,t_max,edgecolors="none",s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$t_{\mathrm{max}} \, / \mathrm{Myr}$")

    nbins = 20
    xmin = r_max[np.isnan(r_max)==False].min()
    xmax = r_max[np.isnan(r_max)==False].max()
    dx = (xmax - xmin) / nbins

    bins = np.arange(xmin, xmax+dx, dx)
    med, lo_perc, hi_perc, mid_bin = us.Calculate_Percentiles(bins, r_max, t_max, min_count=1, lohi=(0.16,0.84))

    py.plot(mid_bin,med,c="k",linewidth=1)
    py.plot(mid_bin,lo_perc,c="k",linewidth=0.7,linestyle='--')
    py.plot(mid_bin,hi_perc,c="k",linewidth=0.7,linestyle='--')

    py.show()



def Plot_Vrad_max_Rmax(coordinates_list, redshift_list, vmax_list, flag1_list, velocity_list, temperature_list,internal_energy_list, h,omm,visualize_winds,monotonic, use_ns=2):

    a_list = 1./(1.+redshift_list)
    t_list, junk = uc.t_Universe(a_list, omm, h)
    
    vmax_list = np.array(vmax_list)
    coordinates_list = np.array(coordinates_list)
    temperature_list = np.array(temperature_list)
    internal_energy_list = np.array(internal_energy_list)
    velocity_list = np.array(velocity_list)

    r_max_max = 500

    r_max = np.zeros(len(coordinates_list[0,:,0]))
    vrad_ta_max = np.zeros(len(coordinates_list[0,:,0]))
    r_max_max = 500

    for i_part in range(len(coordinates_list[0,:,0])):


        # Identify if this particle rejoined the SF ISM or became a star - discard outputs after this happens for calculating r_max                                                                                                   
        # Note to self - this makes quite an impact - reduces number that get to infinity (as they do this after a later fb event) - and increases a lot at delta r_max = 500kpc                                                      
        i_flag = len(redshift_list)
        rejoin = np.where((flag1_list[:,i_part]==0) | (flag1_list[:,i_part]==2))[0][1:]
        if len(rejoin>0):
            i_flag = rejoin[0]

        # Joop suggestion to show r_max - r_init                                                                                                                                                                                      
        x_part = coordinates_list[:,i_part,0]
        y_part = coordinates_list[:,i_part,1]
        z_part = coordinates_list[:,i_part,2]

        #x_part *= 1e3 /h / (1+redshift_list)
        #y_part *= 1e3 /h / (1+redshift_list)
        #z_part *= 1e3 /h / (1+redshift_list)

        r_part = np.sqrt( np.square(x_part) + np.square(y_part) + np.square(z_part))
        r_part0 = r_part[0]

        # For this option, only measure r_max out to where r stops increasing monotonically - the idea is that this will catch most of the particles which join the wind at a later time than _ts
        if monotonic:
            dr = r_part[1:] - r_part[0:-1]
            non_mono = np.where(dr < 0)[0]


            if len(non_mono)>0:
                
                # Only enforce criteria between step 2 and step 3 (and onwards)
                i_mono = 0
                if non_mono[i_mono] == 0:
                    i_mono += 1
                
                #if i_mono == 0 or len(non_mono) > 1:
                #    if non_mono[i_mono] ==1:
                #        i_mono += 1
                    
                if len(non_mono) > i_mono:
                    r_part[non_mono[i_mono]+1:] = 0.0

            # The monotonic criteria will work well for wind particles that get heated in the future, but it will miss ~1/2 those that got heated just after _ts (which may be a significant number)
            # Catch these by looking for suspicious acceleration that occurs after the particle was supposed to have entered the wind
            v_ta_21 = (r_part[2] - r_part[1])* kpc2km / (t_list[2]-t_list[1]) / Gyr2s
            v_ta_10 = (r_part[1] - r_part[0])* kpc2km / (t_list[1]-t_list[0]) / Gyr2s
            #print_cut = False
            for i_t in range(len(r_part[3:])):

                v_ta_n = (r_part[i_t+2] - r_part[i_t+1])* kpc2km / (t_list[i_t+2]-t_list[i_t+1]) / Gyr2s
                v_ta_np1 = (r_part[i_t+3] - r_part[i_t+2])* kpc2km / (t_list[i_t+3]-t_list[i_t+2]) / Gyr2s

                if v_ta_np1 > v_ta_n and v_ta_21 < 0.19*vmax_list[1] and v_ta_10 < 0.19*vmax_list[1] and v_ta_np1 > 0:

                    #print "I'm applying a", i_t+4, i_t+3, "cut"
                    #print v_ta_np1, v_ta_n, v_ta_21, 0.25*vmax_list[1], v_ta_10, 0.38*vmax_list[1]
                    #print "r_part before", r_part
                    r_part[i_t+3:] = 0.0
                    #print "r_part after", r_part
                    #print_cut = True
                    break
            
            #if not print_cut:
            #    print "yo I didn't bother", v_ta_21, v_ta_10, (r_part[1:] - r_part[0:-1]) / (t_list[1:]-t_list[0:-1])

            # Finally, the above criteria will not catch all of the particles that entered the wind between _ts and _ns, i.e. for those that enter the wind shortly after _ts 
            # Catch these by looking for suspicious temperature jumps that occur after _ts (only for particles that have not already been significantly heated)
            T_part = temperature_list[:,i_part]
            for i_t in range(len(r_part[2:])):
                if T_part[i_t+2] - T_part[i_t+1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[i_t+2:] = 0.0
                    break
            if use_ns > 2:
                if T_part[use_ns] - T_part[1] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                    r_part[use_ns:] = 0.0
                    
                for i_t in range(len(r_part[use_ns+1:])):
                    if T_part[i_t+use_ns+1] - T_part[i_t+use_ns] > 10**4.75 and T_part[1] - T_part[0] < 10**4:
                        r_part[i_t+use_ns+1:] = 0.0
                        break
            if use_ns < 2:
                print "use_ns should be >=2"
                quit()


        r_max[i_part] = np.max(r_part[1:i_flag]-r_part0)
        ind_max = np.argmax(r_part[1:i_flag]-r_part0)
        inds_use = np.arange(0,ind_max+2)
       
        if len(inds_use) > 1:
            vrad_ta_max[i_part] = np.max( (r_part[inds_use][1:] - r_part[inds_use][0:-1])* kpc2km / (t_list[inds_use][1:]-t_list[inds_use][0:-1]) / Gyr2s)
        else:
            print "hyh"
            quit()

    r_max[np.isnan(r_max)] = r_max_max
    r_max[r_max>r_max_max] = r_max_max

    exp_fac_l = 1./(redshift_list+1)
    print "assuming Planck cosmo"; omm = 0.307
    t, tlb = uc.t_Universe(exp_fac_l, omm, h)

    py.figure()
    py.scatter(r_max,vrad_ta_max,edgecolors="none",s=10)
    py.xlabel(r"$r_{\mathrm{max}} - r_{\mathrm{init}} \, / \mathrm{pkpc}$ (after $t=%3.1f$"%(1e3*(t.max()-t.min())) + " Myr)")
    py.ylabel(r"$\left<v_{\mathrm{rad}}\right>_{\mathrm{max}} \, / \mathrm{kms^{-1}}$")

    nbins = 20
    xmin = r_max[np.isnan(r_max)==False].min()
    xmax = r_max[np.isnan(r_max)==False].max()
    dx = (xmax - xmin) / nbins

    bins = np.arange(xmin, xmax+dx, dx)

    med, lo_perc, hi_perc, mid_bin = us.Calculate_Percentiles(bins, r_max, vrad_ta_max, min_count=1, lohi=(0.16,0.84))

    py.plot(mid_bin,med,c="k",linewidth=1)
    py.plot(mid_bin,lo_perc,c="k",linewidth=0.7,linestyle='--')
    py.plot(mid_bin,hi_perc,c="k",linewidth=0.7,linestyle='--')

    py.show()
