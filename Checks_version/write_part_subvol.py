# This routine dumps the desired particle data to disk for a range of snapshots
import numpy as np


catalogue_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"

# Reference z=0 1e12 halo (50 snip)
#subvol = 36
#snap_list = [30,31,32,33,34,35,36,37,38,39,40,41]
#snip_list = [241, 249, 257, 265, 273,  281, 289, 297, 305, 313, 321,329]
#snip_first_ps = 233
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

# Reference z=0 1e12 halo (100 snip)
#subvol = 36
#snap_list = [69,70,71,72,73,74,75,76,77,78,79,80]
#snip_list = [277, 281, 285, 289, 293, 297, 301, 305, 309, 313, 317, 321]
#snip_first_ps = 273
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_100_snip"

# Reference z=0 1e12 halo (200 snip)
#subvol = 36
#snip_list = [275, 277, 279, 281, 283, 285, 287, 289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321]
#snap_list = np.arange(138,len(snip_list)+138)
#snip_first_ps = 273
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_200_snip"

# Reference(b) z=0 1e12 halo (300 snap)
#subvol = 3
#snap_select = 207
#snap_list = np.arange(snap_select-36,snap_select+36)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_300_snap"

# Reference(b) z=0 1e12 halo (500 snap)
#subvol = 13
#snap_select = 207*2
#snap_list = np.arange(snap_select-36*2,snap_select+36*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##########################################################

## z=0 1e11 halo (50 snip version)
#subvol = 124
#snap_list = [28,29,30,31,32,33]
#snip_list = [225, 233, 241, 249, 257,265]
#snip_first_ps =217
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

## z= 0 1e11 halo (500 snap version)
#subvol = 90
#snap_select = 280
#snap_list = np.arange(snap_select-36*2,snap_select+36*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

#############################################################

# z=0 5e10 halo
#subvol = 377
#snap_list = [11,12,13,14,15,16]
#snap_select = 13
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

## z= 0 5e10 halo (500 snap version) Note had to use later redshift because the 500 snap halo forms later
# This halo didn't get matched correctly somewhere for full tracking
#subvol = 390
#snap_select = 230
#snap_list = np.arange(snap_select-36*2,snap_select+36*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"


###############################################################

# z=0 1e11.9 halo
#subvol = 38
#snap_list = [41, 42, 43, 44, 45, 46]
#snip_list = [329, 337, 345, 353, 361, 369]
#snip_first_ps =321
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

# z=0 1e11.9 halo
#subvol = 54
#snap_select = 410
#snap_list = np.arange(snap_select-36*2,snap_select+36*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

#########################################################

# z=0 6.9e10 halo
#subvol = 273
#snap_list = [35,36,37,38,39,40]
#snip_list = [281, 289, 297, 305, 313, 321]
#snip_first_ps =273
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 392
#snap_select = 350
#snap_list = np.arange(snap_select-36*2,snap_select+36*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

######################################################

# z=0 5e12 halo, high-z
#subvol = 4
#snap_list = [16, 17, 18, 19, 20, 21]
#snip_list = [129, 137, 145, 153, 161, 169]
#snip_first_ps =121
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 6
#snap_select = 170
#snap_list = np.arange(snap_select-36*2,snap_select+36*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##################################################

# z=0 5e12 halo, low-z
#subvol = 4
#snap_list = [42, 43, 44, 45, 46, 47]
#snip_list = [337, 345, 353, 361, 369, 377]
#snip_first_ps = 329
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 6
#snap_select = 420
#snap_list = np.arange(snap_select-26*2,snap_select+26*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"


#####################################################

# z=0 13.2, high-z
#subvol = 1
#snap_list = [15, 16, 17, 18, 19, 20]
#snip_list = [121, 129, 137, 145, 153, 161]
#snip_first_ps = 113
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 1
#snap_select = 150
#snap_list = np.arange(snap_select-26*2,snap_select+26*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##############################################

# z=0 13.2, low-z, low-z
#subvol = 1
#snap_list = [39, 40, 41, 42, 43, 44]
#snip_list = [313, 321, 329, 337, 345, 353]
#snip_first_ps = 305
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

#subvol = 1
#snap_select = 390
#snap_list = np.arange(snap_select-26*2,snap_select+26*2)
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
#sim_name = "L0025N0376_REF_500_snap"

##########################################################

# Reference z=0 1e12 halo (50 snip) - high redshift
#subvol = 36
#snap_list = [12, 13, 14, 15, 16, 17]
#snip_list = [97, 105, 113, 121, 129, 137]
#snip_first_ps = 89
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

## z=0 1e11 halo (high-z)
#subvol = 124
#snap_list = [9, 10, 11, 12, 13, 14]
#snip_list = [73, 81,  89,  97, 105, 113]
#snip_first_ps =65
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

##########################################################
# Test halo 8 (selected 1e11 at z=2)
subvol = 131
snap_select = 215
snap_list = np.arange(snap_select-25*2,snap_select+25*2)
DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_ApogeeRun/data/"
sim_name = "L0025N0376_REF_500_snap"

#########################################################
# Reference z=0 1e12 halo (50 snip) - high redshift
#subvol = 36
#snap_list = [12, 13, 14, 15, 16, 17]
#snip_list = [97, 105, 113, 121, 129, 137]
#snip_first_ps = 89
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

## z=0 1e11 halo (high-z)
#subvol = 124
#snap_list = [9, 10, 11, 12, 13, 14]
#snip_list = [73, 81,  89,  97, 105, 113]
#snip_first_ps =65
#DATDIR = "/cosma5/data/Eagle/ScienceRuns/Planck1/L0025N0376/PE/REFERENCE_Snipshots/data/"
#sim_name = "L0025N0376_REF_50_snip"

output_path = catalogue_path + sim_name+"_subvols_test_mode/"


import read_eagle as re
import warnings
np.seterr(all='ignore')
import glob
import os
import h5py
from os import listdir
import match_searchsorted as ms
import sys
sys.path.append('../')
import measure_particles_functions as mpf
import measure_particles_io as mpio

# First let's work out the relationship between tree snapshots and simulation snapshots/snipshots
cat_file = h5py.File(catalogue_path + "subhalo_catalogue_"+sim_name+".hdf5")
tree_snapshot_info = cat_file["tree_snapshot_info"]
snapshot_numbers = tree_snapshot_info["snapshots_tree"][:]
sn_i_a_pshots_simulation = tree_snapshot_info["sn_i_a_pshots_simulation"][:]
cat_file.close()

snip_list = np.zeros_like(snap_list)
for n in range(len(snip_list)):
    snip_list[n] = sn_i_a_pshots_simulation[np.argmin(abs(snapshot_numbers-snap_list[n]))]

print "dumping particle data for snips", snip_list, "corresponding to snaps", snap_list

for i_snip, snip_ts in enumerate(snip_list):
    
    snap_ts = snap_list[i_snip]

    #if snap_ts != snap_select:
    #    print "skipping hack"
    #    continue

    print "working on snip", snip_ts

    # Read subvol file to load halo catalogue and selection boundaries
    filename_ts = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"
    output_path_ts = output_path + "Snip_"+str(snip_ts)+"/"

    print "reading", output_path_ts+filename_ts
    File = h5py.File(output_path_ts+filename_ts)

    # Read selection boundary
    try:
        xyz_min_ts = File["select_region/xyz_min"][:]
        xyz_max_ts = File["select_region/xyz_max"][:]
    except:
        print output_path_ts+filename_ts, "doesn't exist, moving to next snipshot"
        continue

    # Quit if there are no subhaloes in this subvolume (this assumes that future subvols will also not have subhaloes)
    if np.isnan(xyz_min_ts[0]):
        print "No haloes inside this subvolume, moving to next snipshot"
        continue
    try:
        # Read in selection boundary for next snipshot
        snip_ns = snip_list[i_snip+1]
        output_path_ns = output_path + "Snip_"+str(snip_ns)+"/"
        filename_ns = "subhalo_catalogue_subvol_" + sim_name + "_snip_"+str(snip_ns)+"_"+str(subvol)+".hdf5"
        File_ns = h5py.File(output_path_ns+filename_ns)
        
        xyz_max_ns = File_ns["select_region/xyz_max"][:]
        xyz_min_ns = File_ns["select_region/xyz_min"][:]
    except:
        if not i_snip+1 == len(snip_list): # There is no next snipshot once we reach z=0
            print "Error: failed to read selection boundary for the next snipshot, quitting"
            exit()

    # Want to read in a big enough volume to accomodate both the subhalo & progenitor samples for this snipshot and the subhalo/progenitor samples for the next snipshot
    boxsize = mpio.Get_Box_Size(sim_name,DATDIR,snip_list[0])

    for i_dim in range(3):
        if xyz_max_ts[i_dim] > xyz_max_ns[i_dim] + 0.5*boxsize:
            xyz_max_ns[i_dim]+= boxsize; xyz_min_ns[i_dim] += boxsize
        if xyz_max_ns[i_dim] > xyz_max_ts[i_dim] + 0.5*boxsize:
            xyz_max_ts[i_dim] += boxsize; xyz_min_ts[i_dim] += boxsize

    for i in range(3):
        xyz_max_ts[i] = max(xyz_max_ts[i], xyz_max_ns[i])
        xyz_min_ts[i] = min(xyz_min_ts[i], xyz_min_ns[i])

    ############## Read Eagle particle data ####################### 

    gas_names_in = ['Mass', "Density", "Metallicity", "Temperature", "InternalEnergy", 'Coordinates', "Velocity"]
    gas_names_out = ['mass', "density", "metallicity", "temperature", "internal_energy", 'coordinates', "velocity"]        

    dm_names_in = ['Coordinates']
    dm_names_out = ['coordinates']

    star_names_in = ['Mass', "InitialMass", 'Coordinates',"StellarFormationTime"]
    star_names_out = ['mass', "mass_init",'coordinates',"aform"]

    bh_names_in = ['BH_Mass','Coordinates']
    bh_names_out = ['mass', 'coordinates']

    data_names_in = [gas_names_in, dm_names_in, star_names_in, bh_names_in]
    data_names_out = [gas_names_out, dm_names_out, star_names_out, bh_names_out]

    print "reading particle data for current snipshot", snip_ts
    gas_data_ts, dm_data_ts, star_data_ts, bh_data_ts, sim_props_ts = mpio.Read_All_Particle_Data(snip_ts, xyz_max_ts, xyz_min_ts, sim_name, DATDIR,data_names_in,data_names_out,read_all="g",verbose=False)

    gas_names_in = ['Mass', "Density", "Metallicity", "Temperature", "InternalEnergy", 'Coordinates', "Velocity"]
    gas_names_out = ['mass', "density", "metallicity", "temperature", "internal_energy", 'coordinates', "velocity"]        

    # Unpack data
    group_number_gas_list_ts = gas_data_ts.data["group_number"]
    subgroup_number_gas_list_ts = gas_data_ts.data["subgroup_number"]
    mass_gas_list_ts = gas_data_ts.data["mass"]
    density_gas_list_ts = gas_data_ts.data["density"]
    metallicity_gas_list_ts = gas_data_ts.data["metallicity"]
    id_gas_list_ts = gas_data_ts.data["id"]
    coordinates_gas_list_ts = gas_data_ts.data["coordinates"]
    velocity_gas_list_ts = gas_data_ts.data["velocity"]
    temperature_gas_list_ts = gas_data_ts.data["temperature"]
    internal_energy_gas_list_ts = gas_data_ts.data["internal_energy"]

    group_number_dm_list_ts = dm_data_ts.data["group_number"]
    subgroup_number_dm_list_ts = dm_data_ts.data["subgroup_number"]
    coordinates_dm_list_ts = dm_data_ts.data["coordinates"]

    group_number_star_list_ts = star_data_ts.data["group_number"]
    subgroup_number_star_list_ts = star_data_ts.data["subgroup_number"]
    mass_star_list_ts = star_data_ts.data["mass"]
    mass_star_init_list_ts = star_data_ts.data["mass_init"]
    aform_star_list_ts = star_data_ts.data["aform"]
    id_star_list_ts = star_data_ts.data["id"]
    coordinates_star_list_ts = star_data_ts.data["coordinates"]

    group_number_bh_list_ts = bh_data_ts.data["group_number"]
    subgroup_number_bh_list_ts = bh_data_ts.data["subgroup_number"]
    mass_bh_list_ts = bh_data_ts.data["mass"]
    coordinates_bh_list_ts = bh_data_ts.data["coordinates"]
 
    # Write data to disk
    
    part_subvol_file = output_path + "Snip_"+str(snip_ts)+"/" + "part_subvol_" + sim_name + "_snip_"+str(snip_ts)+"_"+str(subvol)+".hdf5"

    print "deleting previous subvolume file"
    if os.path.isfile(part_subvol_file):
        os.remove(part_subvol_file)

    print "writing particle data"
    File_subvol = h5py.File(part_subvol_file)

    gas_group = File_subvol.create_group("gas_ts")
    dm_group = File_subvol.create_group("dm_ts")
    star_group = File_subvol.create_group("star_ts")
    bh_group = File_subvol.create_group("bh_ts")
    extra_data_group = File_subvol.create_group("extra_data")

    gas_group.create_dataset("group_number_gas", data=group_number_gas_list_ts)
    gas_group.create_dataset("subgroup_number_gas", data=subgroup_number_gas_list_ts)
    gas_group.create_dataset("mass_gas", data=mass_gas_list_ts)
    gas_group.create_dataset("density_gas", data=density_gas_list_ts)
    gas_group.create_dataset("metallicity_gas", data=metallicity_gas_list_ts)
    gas_group.create_dataset("id_gas", data=id_gas_list_ts)
    gas_group.create_dataset("coordinates_gas", data=coordinates_gas_list_ts)
    gas_group.create_dataset("velocity_gas", data=velocity_gas_list_ts)
    gas_group.create_dataset("temperature_gas", data=temperature_gas_list_ts)
    gas_group.create_dataset("internal_energy_gas", data=internal_energy_gas_list_ts)

    dm_group.create_dataset("group_number_dm", data=group_number_dm_list_ts)
    dm_group.create_dataset("subgroup_number_dm", data=subgroup_number_dm_list_ts)
    dm_group.create_dataset("coordinates_dm", data=coordinates_dm_list_ts)

    star_group.create_dataset("group_number_star", data=group_number_star_list_ts)
    star_group.create_dataset("subgroup_number_star", data=subgroup_number_star_list_ts)
    star_group.create_dataset("mass_star", data=mass_star_list_ts)
    star_group.create_dataset("mass_star_init", data=mass_star_init_list_ts)
    star_group.create_dataset("aform_star", data=aform_star_list_ts)
    star_group.create_dataset("id_star", data=id_star_list_ts)
    star_group.create_dataset("coordinates_star", data=coordinates_star_list_ts)

    bh_group.create_dataset("group_number_bh", data=group_number_bh_list_ts)
    bh_group.create_dataset("subgroup_number_bh", data=subgroup_number_bh_list_ts)
    bh_group.create_dataset("coordinates_bh", data=coordinates_bh_list_ts)
    bh_group.create_dataset("mass_bh", data=mass_bh_list_ts)

    extra_data_group.create_dataset("mass_dm", data=sim_props_ts["mass_dm"])
    extra_data_group.create_dataset("boxsize", data=sim_props_ts["boxsize"])
    extra_data_group.create_dataset("h", data=sim_props_ts["h"])
    extra_data_group.create_dataset("omm", data=sim_props_ts["omm"])
    extra_data_group.create_dataset("redshift", data=sim_props_ts["redshift"])
    extra_data_group.create_dataset("expansion_factor", data=sim_props_ts["expansion_factor"])

    if snap_ts == snap_select:
        subhalo_group = File["subhalo_data"]
        subgroup_number_list = subhalo_group["subgroup_number"][:]
        ok = subgroup_number_list == 0

        mchalo_list = subhalo_group["mchalo"][:][ok]
        order = np.argsort(mchalo_list)[::-1]
        halo_x_list = subhalo_group["x_halo"][:][ok][order] # cMpc h^-1                                                   
        halo_y_list = subhalo_group["y_halo"][:][ok][order]
        halo_z_list = subhalo_group["z_halo"][:][ok][order]
        halo_r200_host_list = subhalo_group["r200_host"][:][ok][order]

        ih_choose = 0
        halo_x = halo_x_list[ih_choose]
        halo_y = halo_y_list[ih_choose]
        halo_z = halo_z_list[ih_choose]
        r200 = halo_r200_host_list[ih_choose]

        grn = subhalo_group["group_number"][:][ok][order]

        x_stargas = np.concatenate(( coordinates_gas_list_ts[:,0], coordinates_star_list_ts[:,0] ))
        y_stargas = np.concatenate(( coordinates_gas_list_ts[:,1], coordinates_star_list_ts[:,1] ))
        z_stargas = np.concatenate(( coordinates_gas_list_ts[:,2], coordinates_star_list_ts[:,2] ))
        id_stargas = np.concatenate(( id_gas_list_ts, id_star_list_ts ))

        r_stargas = np.sqrt(np.square(x_stargas-halo_x)+np.square(y_stargas-halo_y)+np.square(z_stargas-halo_z))
        ok = r_stargas < r200

        print len(ok[ok]), "of", len(ok),"particles selected to be tracked"

        id_track_file = output_path + "id_track_check_version_" + sim_name + "_snip_"+str(snap_ts)+"_"+str(subvol)+".hdf5"

        print "deleting previous subvolume file"
        if os.path.isfile(id_track_file):
            os.remove(id_track_file)

        File_track = h5py.File(id_track_file)
        File_track.create_dataset("id_track", data=id_stargas[ok])
        File_track.close()
        

File.close()
