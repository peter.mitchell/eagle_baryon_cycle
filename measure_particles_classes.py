import match_searchsorted as ms
import numpy as np

class DataSet:
    def __init__(self):
        self.data = {}
        self.names = []

    def add_data(self,data,dname):
        self.data[dname] = data
        self.names.append(dname)

    def add_append_data(self,data,dname):
        if dname in self.names:
            if len(self.data[dname].shape) == 1:
                self.data[dname] = np.append(self.data[dname], data)
            elif len(self.data[dname].shape) == 2:
                self.data[dname] = np.append(self.data[dname], data,axis=0)
            else:
                print "Error Dataset.merge_dataset(): ", dname, " has the wrong shape", self.data[dname].shape
                quit()
        else:
            self.add_data(data,dname)

    def is_bound(self):
        is_bound = (self.data["group_number"] >= 0) & (self.data["subgroup_number"] >= 0) & (self.data["subgroup_number"] < 1e8)
        return is_bound

    def merge_dataset(self,dataset):

        unique_check = False
        if "id" in self.names:
            junk, id_unique = np.unique( np.append(self.data["id"], dataset.data["id"]) , return_index=True)
            unique_check = True

        for dname in dataset.names:
            # First try to append data to existing arrays
            if dname in self.names:
                if len(self.data[dname].shape) == 1:
                    self.data[dname] = np.append(self.data[dname], dataset.data[dname])
                elif len(self.data[dname].shape) == 2:
                    self.data[dname] = np.append(self.data[dname], dataset.data[dname],axis=0)
                else:
                    print "Error Dataset.merge_dataset(): ", dname, " has the wrong shape", self.data[dname].shape
                    quit()
                if unique_check:
                    self.data[dname] = self.data[dname][id_unique]
            # Otherwise init the arrays
            else:
                self.add_data(dataset.data[dname], dname)

    def select_bool(self,bool_list):
        for name in self.names:
            self.data[name] = self.data[name][bool_list]

    def ptr_prune(self,id_match,name_crossmatch,abs_mode=False):

        try:
            if len(id_match) > 0 and len(self.data[name_crossmatch]) > 0:
                if abs_mode: # This is for matching on group number before we have moved the spherical overdensity particles outside the FoF group to +ve group number
                    ptr = ms.match(abs(self.data[name_crossmatch]), id_match)
                else:
                    ptr = ms.match(self.data[name_crossmatch], id_match)
                ok_match = ptr >= 0

                self.select_bool(ok_match)

        except:
            print "Error, name_crossmatch=",name_crossmatch," is not a valid entry for .ptr_prune()"
            print "Valid options include 'cat', and ", self.names
            quit()

    def sort(self,name_sort):
        order = np.argsort(self.data[name_sort])
        for name in self.names:
            self.data[name] = self.data[name][order]
            
    def set_index(self,nsep,extra_string="",return_subhalo_index=False):
        
        if "subgroup_number"+extra_string not in self.names:
            print "set_index() Error: cannot sort by group_number"+extra_string+" or subgroup_number"+extra_string+" if these data are not present in the DataSet"
            print "DataSet currently contains ", self.names
            quit()

        if len(self.data["subgroup_number"+extra_string]) > 0:
            nsep_check = max(len(str(int(self.data["subgroup_number"+extra_string].max()))) +1 ,6)
            if nsep_check > nsep:
                print "Error: nsep passed into DataSet.set_index(nsep) was too small"
                quit()

            # Unique index for each subhalo
            subhalo_index = self.data["group_number"+extra_string].astype("int64") * 10**nsep + self.data["subgroup_number"+extra_string].astype("int64")

            order = np.argsort(subhalo_index)
        
            subhalo_index = subhalo_index[order]
            for name in self.names:
                self.data[name] = self.data[name][order]

            self.subhalo_index_unique, self.particle_subhalo_index = np.unique(subhalo_index, return_index=True)
            self.particle_subhalo_index = np.append(self.particle_subhalo_index,len(self.data["group_number"+extra_string]))
            if return_subhalo_index:
                return subhalo_index
        else:
            self.subhalo_index_unique = np.array([])            
            self.particle_subhalo_index = np.array([])

            if return_subhalo_index:
                return np.array([])
