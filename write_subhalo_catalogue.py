import numpy as np
import os
from os import listdir
import h5py
import match_searchsorted as ms
import utilities_cosmology as uc

# 100 Mpc reference run, 50 snipshots
#tree_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/quarter_snipshots/trees/treedir_050/"
#subfind_path = tree_path
#sim_name = "L0100N1504_REF_50_snip"
#m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
#snap_snip_path = "quarter_100"
#nsnap = "050"

# 100 Mpc reference run, 100 snipshots
#tree_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0100N1504/half_snipshots/trees/treedir_100/"
#subfind_path = tree_path
#sim_name = "L0100N1504_REF_100_snip"
#m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
#snap_snip_path = "half_100"
#nsnap = "100"

# 100 Mpc reference run, 200 snipshots
'''tree_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/"
subfind_path = tree_path
sim_name = "L0100N1504_REF_200_snip"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "full_100"
nsnap = "200"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 50 Mpc reference run, 28 snapshots
'''tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/"
subfind_path = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/subfind/"
sim_name = "L0050N0752_REF_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "snap"
nsnap = "028"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 28 snapshots
'''tree_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/"
subfind_path = tree_path
sim_name = "L0025N0376_REF_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "snap"
nsnap = "028"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 50 snipshots
'''tree_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/"
subfind_path = tree_path
sim_name = "L0025N0376_REF_50_snip"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "quarter_25"
nsnap = "050"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 100 snipshots
'''tree_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/"
subfind_path = tree_path
sim_name = "L0025N0376_REF_100_snip"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "half_25"
nsnap = "100"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 200 snipshots
'''tree_path = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/"
subfind_path = tree_path
sim_name = "L0025N0376_REF_200_snip"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "full_25"
nsnap = "200"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 300 snapshots
'''tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/"
subfind_path = tree_path
sim_name = tree_path
sim_name = "L0025N0376_REF_300_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "300_snap"
nsnap = "299"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 400 snapshots
'''tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/"
subfind_path = tree_path
sim_name = tree_path
sim_name = "L0025N0376_REF_400_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "400_snap"
nsnap = "399"'''

# 25 Mpc reference run, 500 snapshots
'''tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/"
subfind_path = tree_path
sim_name = tree_path
sim_name = "L0025N0376_REF_500_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "500_snap"
nsnap = "499"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc reference run, 1000 snapshots
tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/"
subfind_path = tree_path
sim_name = tree_path
sim_name = "L0025N0376_REF_1000_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "1000_snap"
nsnap = "999"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb

# 50 Mpc - const fb run, 28 snapshots
#tree_path = "/gpfs/data/jch/Eagle/Merger_Trees/L0050N0752/EagleVariation_fE1p00_ALPHA1p0e3/trees/treedir_028/"
#subfind_path = tree_path
#sim_name = "L0050N0752_CFB_snap"
#m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
#snap_snip_path = "snap"
#nsnap = "028"

# 50 Mpc - no AGN variant, 28 snapshots
'''tree_path = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/"
subfind_path = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/subfind/"
sim_name = "L0050N0752_NOAGN_snap"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "snap"
nsnap = "028"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# 25 Mpc - recal (high-res), 202 snipshots
'''tree_path = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/"
subfind_path = tree_path
sim_name = "L0025N0752_Recal"
m200_path = "/cosma5/data/Eagle/matthieu/BaryonFractions/Python/L100N1504/"
snap_snip_path = "202_snip"
nsnap = "202"
h=0.6777; omm = 0.307; fb = 0.0482519 / 0.307; fdm = 1-fb'''

# Read available snipshots
subhalo_quantity_list = ["GroupNumber",       "SubGroupNumber", "Mass",        "ApertureMeasurements/Mass/030kpc"]
# 0 Gas, 1 DM, 4 Stars, 5 Black hole
part_index =            [None,               None             , None,          4] 
name_list =             ["GroupNumber_hydro","SubGroupNumber_hydro", "Mtotal", "Mstars_30pkpc"]
type_list =             ["int",              "int",             "float",       "float"]

subhalo_quantity_list += ["MassType", "MassType", "MassType", "BlackHoleMass", "StellarInitialMass"]
part_index +=            [4,           0,         1         ,  None        ,  None]
name_list +=             ["Mstars",   "Mgas",     "Mdm"     ,  "Mbh"    ,  "Mstars_initial"]
type_list +=             ["float",    "float",    "float",     "float",    "float"]

subhalo_quantity_list += ["CentreOfPotential", "CentreOfPotential", "CentreOfPotential", "Velocity", "Velocity", "Velocity",           "Vmax"]
part_index            += [0,                   1,                   2                  , 0           , 1           , 2                 ,None]
name_list             += ["CentreOfPotential_x", "CentreOfPotential_y", "CentreOfPotential_z", "Velocity_x", "Velocity_y", "Velocity_z","Vmax"]
type_list             += ["float",               "float",               "float",               "float",      "float",      "float",     "float"]

subhalo_output_list = []

for subhalo_quantity, type_i in zip(subhalo_quantity_list,type_list):
    if type_i == "float":
        subhalo_output_list.append([])
    elif type_i == "int":
        subhalo_output_list.append(np.rint(np.zeros(0)).astype("int"))
    else:
        print type_i, "not understood"
        quit()

GroupNumber_list = np.rint(np.zeros(0)).astype("int")
SubGroupNumber_list = np.rint(np.zeros(0)).astype("int")
msbh_list = []
mdm_list = []

descendantIndex_list = np.rint(np.zeros(0)).astype("int")
nodeIndex_list = np.rint(np.zeros(0)).astype("int")
isMainProgenitor_list = np.rint(np.zeros(0)).astype("int")
redshift_list = []
snapshot_number_list =  np.rint(np.zeros(0)).astype("int")
isInterpolated_list = np.rint(np.zeros(0)).astype("int")


########### Open output hdf5 #################################
#output_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/"
output_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"
filename = "subhalo_catalogue_" + sim_name
filename += ".hdf5"
File = h5py.File(output_path+filename,"a")

# Count number of subvolumes
nsubvol = 0
for file_name in listdir(subfind_path):
    if not "subfind" in file_name:
        continue
    nsubvol += 1

# Loop over subvolumes to read in complete subhalo catalogue
for i_sub in range(nsubvol):
    print i_sub, "of", nsubvol

    #if i_sub > 1:
    #    print "skipping"
    #    continue

    subvol_path = subfind_path+"subfind_"+nsnap+"."+str(i_sub)+".hdf5"
    if not os.path.isfile(subvol_path):
        subvol_path = subfind_path+"subfind."+str(i_sub)+".hdf5"
    if not os.path.isfile(subvol_path):
        print "Error, couldn't read the subfind file containing subhalo data, check paths"
        quit()
    subvol_file = h5py.File(subvol_path,"r")

    GroupNumber_list = np.concatenate((GroupNumber_list, subvol_file["Subhalo/GroupNumber"][:]))
    SubGroupNumber_list = np.concatenate((SubGroupNumber_list, subvol_file["Subhalo/SubGroupNumber"][:]))
    mdm_list = np.concatenate((mdm_list,subvol_file["Subhalo/MassType"][:][:,1]))
    msbh_list = np.concatenate((msbh_list,subvol_file["Subhalo/BlackHoleMass"][:]+subvol_file["Subhalo/MassType"][:][:,4]))

    for i, subhalo_quantity in enumerate(subhalo_quantity_list):          

        if part_index[i] is None:
            subhalo_output_list[i] = np.concatenate((subhalo_output_list[i],subvol_file["Subhalo/"+subhalo_quantity][:]))
        else:
            subhalo_output_list[i] = np.concatenate((subhalo_output_list[i],subvol_file["Subhalo/"+subhalo_quantity][:][:,part_index[i]]))

    subvol_file.close()

    # Read tree files
    tree_file = h5py.File(tree_path+"tree_"+nsnap+"."+str(i_sub)+".hdf5","r")
        
    descendantIndex_list = np.concatenate((descendantIndex_list, tree_file["haloTrees/descendantIndex"][:]))
    nodeIndex_list = np.concatenate((nodeIndex_list, tree_file["haloTrees/nodeIndex"][:]))
    isMainProgenitor_list = np.concatenate((isMainProgenitor_list, tree_file["haloTrees/isMainProgenitor"][:]))
    redshift_list = np.concatenate((redshift_list, tree_file["haloTrees/redshift"][:]))
    snapshot_number_list = np.concatenate((snapshot_number_list, tree_file["haloTrees/snapshotNumber"][:]))
    isInterpolated_list = np.concatenate((isInterpolated_list, tree_file["haloTrees/isInterpolated"][:]))
        
    tree_file.close()

snapshot_numbers = np.sort(np.unique(snapshot_number_list))[::-1]
z_snapshots = np.sort(np.unique(redshift_list))
a_snapshots = 1/(1.+z_snapshots)
t_snapshots, junk = uc.t_Universe(a_snapshots, omm, h)

if snap_snip_path == "/cosma/home/jch/Codes/Gadget/Eagle/find_redshifts/dm_vs_hydro_snipshots.txt":
    snapshots_tree, snip_nos_dm, snip_nos_hydro, z_dm, z_hydro, z_diff = np.loadtxt(snap_snip_path, unpack=True)
elif snap_snip_path == "300_snap":
    snip_nos_hydro, snapshots_tree = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
elif snap_snip_path == "400_snap":
    snip_nos_hydro, snapshots_tree = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
elif snap_snip_path == "500_snap":
    snip_nos_hydro, snapshots_tree = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
elif snap_snip_path == "1000_snap":
    snip_nos_hydro, snapshots_tree = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
elif snap_snip_path == "quarter_100":
    snip_nos_hydro = [   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,
                         98, 106, 114, 122, 130, 138, 146, 154, 164, 172, 180, 188,
                         196, 204, 212, 220, 228, 236, 244, 252, 260, 268, 276, 284,
                         292, 300, 308, 318, 326, 334, 342, 350, 358, 366, 374, 382,
                         390, 398, 405]
    snapshots_tree = np.arange(0,51)
elif snap_snip_path == "quarter_25":
    snip_nos_hydro = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
                        145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
                        289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399]
    snapshots_tree = np.arange(0,51)
elif snap_snip_path == "half_25":
    snip_nos_hydro = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69, 
                        73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
                        145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
                        217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
                        289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
                        361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399]
    snapshots_tree = np.arange(0,101)
elif snap_snip_path == "full_25":
    snip_nos_hydro = [  1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
                        37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
                        73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
                        109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
                        145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
                        181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
                        217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
                        253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
                        289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
                        325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
                        361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
                        397, 399]
    snapshots_tree = np.arange(1,201)
elif snap_snip_path == "full_100":
    snip_nos_hydro, snapshots_tree = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
elif snap_snip_path == "snap":
    snip_nos_hydro = np.arange(0,29)
    snapshots_tree = np.arange(0,29)
elif snap_snip_path == "202_snip":
    snip_nos_hydro, snapshots_tree = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
else:
    print "Error, snap_snip_path not recognised"
    quit()

def Snap2Snip(snap_num):
    ok = np.argmin(abs(snapshots_tree - snap_num))
    return int(snip_nos_hydro[ok])

def Read_M200(snap_num):
    snip_num = Snap2Snip(snap_num)

    m200_full_path = m200_path+"BaryonFraction_"+str(snip_num)+"/BaryonFraction_"+str(snip_num)+"_0.dat"

    try:
        group_number_m200, m200, r200, m200_dm, m200_gas, m200_star, m200_bh = np.loadtxt(m200_full_path,unpack=True)
    except:
        #print "Warning: couldn't read m200 at snipshot", snip_num, "setting to 0 instead"
        group_number_m200 = np.zeros(1); m200 = np.zeros(1); r200 = np.zeros(1); m200_dm = np.zeros(1); m200_gas = np.zeros(1)
        m200_star = np.zeros(1); m200_bh = np.zeros(1)

    return group_number_m200, m200, r200, m200_dm, m200_gas, m200_star, m200_bh

# Write the tree snapshots, redshifts, and time to disk
if "tree_snapshot_info" in File:
    del File["tree_snapshot_info"]
tree_snapshot_info = File.create_group("tree_snapshot_info")
tree_snapshot_info.create_dataset("snapshots_tree", data= snapshot_numbers)
tree_snapshot_info.create_dataset("redshifts_tree", data = z_snapshots)
tree_snapshot_info.create_dataset("cosmic_times_tree", data = t_snapshots)

snipshot_numbers = np.zeros_like(snapshot_numbers)
for n in range(len(snipshot_numbers)):
    snipshot_numbers[n] = Snap2Snip(snapshot_numbers[n])

tree_snapshot_info.create_dataset("sn_i_a_pshots_simulation", data = snipshot_numbers)

clumps = (isMainProgenitor_list ==1) & (mdm_list/fdm*fb < (msbh_list)*0.5) & (isInterpolated_list==0)

# Loop over snipshots, starting from z=0
for i_snap, snap_num in enumerate(snapshot_numbers):

    print "i_snap", i_snap, "snap num", snap_num

    #if i_snap > 2:
    #    print "quitting here"
    #    break

    snip_num = Snap2Snip(snap_num)

    snapnum_group_str = "Snap_"+str(int(snap_num))


    if "Snap_"+str(snap_num) in File:
        snapnum_group = File["Snap_"+str(snap_num)]
    else:
        snapnum_group = File.create_group("Snap_"+str(snap_num))
        
    if "subhalo_properties" in snapnum_group:
        del snapnum_group["subhalo_properties"]

    subhalo_group = snapnum_group.create_group("subhalo_properties")

    if "main_progenitors" in snapnum_group:
        del snapnum_group["main_progenitors"]

    progenitor_group = snapnum_group.create_group("main_progenitors")

    if "subhalo_no_progenitor" in snapnum_group:
        del snapnum_group["subhalo_no_progenitor"]

    subhalo_noprogen_group = snapnum_group.create_group("subhalo_no_progenitor")

    if "all_subhalo_ns" in snapnum_group:
        del snapnum_group["all_subhalo_ns"]

    subhalo_ns_group = snapnum_group.create_group("all_subhalo_ns")

    ok_ts = (snapshot_number_list == snapshot_numbers[i_snap])

    redshift_ts = redshift_list[ok_ts][0]

    # Write main progenitor list
    if snap_num == snapshot_numbers[-1]:
        continue

    ok_ns = (snapshot_number_list == snapshot_numbers[i_snap+1])
    redshift_ns = redshift_list[ok_ns][0]

    # Find main progenitors which are star/bh clumps and reassign the main progenitor flag 
    clumps_ns = ok_ns & clumps
    n_clumps = len(mdm_list[clumps_ns])
    isMainProgenitor_clean = np.copy(isMainProgenitor_list)

    for i_clump in range(n_clumps):
        progenitors = (descendantIndex_list == descendantIndex_list[clumps_ns][i_clump]) & ok_ns
        if len(mdm_list[progenitors])>1:
            dmmax = np.argmax(mdm_list[progenitors])
            temp = isMainProgenitor_clean[clumps_ns] # python indexing workaround   
            temp[i_clump] = 0
            isMainProgenitor_clean[clumps_ns] = temp

            temp = isMainProgenitor_clean[progenitors]
            temp[dmmax] = 1
            isMainProgenitor_clean[progenitors] = temp

    ok_ns_full_progen = np.copy(ok_ns)
    ok_ns = ok_ns & (isMainProgenitor_clean == 1) # & (mdm_list/fdm*fb > (msbh_list)*0.5)

    descendantIndex_list_ns = descendantIndex_list[ok_ns]
    nodeIndex_list_ts = nodeIndex_list[ok_ts]

    if len(descendantIndex_list_ns) > 0:

        # Find progenitors for this snapshot
        ptr = ms.match(nodeIndex_list_ts, descendantIndex_list_ns)
        ok_match = ptr >= 0 # Galaxies that have a main progenitor at the next snapshot

        # Write to disk
        # only keep subhalos with progenitors
        dset = subhalo_group.create_dataset("redshift", data=redshift_ts)
        dset = subhalo_group.create_dataset("nodeIndex", data=nodeIndex_list[ok_ts][ok_match])
        dset = subhalo_group.create_dataset("descendantIndex", data=descendantIndex_list[ok_ts][ok_match])
        dset = subhalo_group.create_dataset("isInterpolated", data=isInterpolated_list[ok_ts][ok_match])
        for subhalo_output, name in zip(subhalo_output_list, name_list):
            dset = subhalo_group.create_dataset(name, data=subhalo_output[ok_ts][ok_match])

        dset = progenitor_group.create_dataset("redshift", data=redshift_ns)
        for subhalo_output, name in zip(subhalo_output_list, name_list):
            dset = progenitor_group.create_dataset(name, data=subhalo_output[ok_ns][ptr][ok_match])

        dset = progenitor_group.create_dataset("nodeIndex", data=nodeIndex_list[ok_ns][ptr][ok_match])
        dset = progenitor_group.create_dataset("isInterpolated", data=isInterpolated_list[ok_ns][ptr][ok_match])
            

        # Write a separate group for subhalos without progenitors (so we can calculate the average baryon content these halos have when they appear for the first time)
        dset = subhalo_noprogen_group.create_dataset("nodeIndex",data=nodeIndex_list[ok_ts][ok_match==False])
        dset = subhalo_noprogen_group.create_dataset("descendantIndex",data=descendantIndex_list[ok_ts][ok_match==False])
        dset = subhalo_noprogen_group.create_dataset("isInterpolated",data=isInterpolated_list[ok_ts][ok_match==False])
        for subhalo_output, name in zip(subhalo_output_list, name_list):
            dset = subhalo_noprogen_group.create_dataset(name, data=subhalo_output[ok_ts][ok_match==False])

        # Write positions, group/subgroup numbers + descendent of IDS for all subhalos on next step, used to efficiently compute accretion rates
        dset = subhalo_ns_group.create_dataset("GroupNumber", data=GroupNumber_list[ok_ns_full_progen])
        dset = subhalo_ns_group.create_dataset("SubGroupNumber", data=SubGroupNumber_list[ok_ns_full_progen])
        dset = subhalo_ns_group.create_dataset("descendantIndex", data=descendantIndex_list[ok_ns_full_progen])
        dset = subhalo_ns_group.create_dataset("nodeIndex", data=nodeIndex_list[ok_ns_full_progen])
        dset = subhalo_ns_group.create_dataset("isInterpolated", data=isInterpolated_list[ok_ns_full_progen])


        for subhalo_output, name in zip(subhalo_output_list, name_list):
            if "CentreOfPotential_" in name or "Mtotal" in name:
                dset = subhalo_ns_group.create_dataset(name, data=subhalo_output[ok_ns_full_progen])
                
        # Get r200 values, match to subhalo and progenitor groups and write to disk
        group_number_M200, m200, r200, m200_dm, m200_gas, m200_star, m200_bh = Read_M200(snip_num)
        m200_output_list = [m200, r200, m200_dm, m200_gas, m200_star, m200_bh]
        m200_name_list = ["m200", "r200", "m200_dm", "m200_gas", "m200_star", "m200_bh"]

        ptr2 = ms.match(GroupNumber_list[ok_ts][ok_match], group_number_M200)
        ok_match2 = ptr2 >= 0

        for subhalo_output, name in zip(m200_output_list, m200_name_list):
            array = np.zeros_like(subhalo_output[ptr2]) -1
            array[ok_match2] = subhalo_output[ptr2][ok_match2]
            
            dset = subhalo_group.create_dataset(name, data=array)

        ptr2 = ms.match(GroupNumber_list[ok_ns][ptr][ok_match], group_number_M200)
        ok_match2 = ptr2 >= 0

        for subhalo_output, name in zip(m200_output_list, m200_name_list):
            array = np.zeros_like(subhalo_output[ptr2]) -1
            array[ok_match2] = subhalo_output[ptr2][ok_match2]

            dset = progenitor_group.create_dataset(name, data=array)            

        ptr2 = ms.match(GroupNumber_list[ok_ts][ok_match==False], group_number_M200)
        ok_match2 = ptr2 >= 0

        for subhalo_output, name in zip(m200_output_list, m200_name_list):
            array = np.zeros_like(subhalo_output[ptr2]) -1
            array[ok_match2] = subhalo_output[ptr2][ok_match2]
            
            dset = subhalo_noprogen_group.create_dataset(name, data=array)


    else:
        for subhalo_output, name in zip(subhalo_output_list, name_list):
            dset = subhalo_group.create_dataset(name, data=np.array([]))
            dset = progenitor_group.create_dataset(name, data=np.array([]))
        for subhalo_output, name in zip(m200_output_list, m200_name_list):    
            dset = progenitor_group.create_dataset(name, data=np.array([]))
            dset = subhalo_group.create_dataset(name, data=np.array([]))

File.close()
