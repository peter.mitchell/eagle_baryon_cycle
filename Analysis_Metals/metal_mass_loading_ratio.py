import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

show_incomplete = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

fVmax_cut = 0.25

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 tree snipshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!



########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_Zml"+fV_str,"halo_wind_tot_Zml"+fV_str, "ism_wind_tot_Zratio"+fV_str,"halo_wind_tot_Zratio"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

if show_incomplete:
    file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins.hdf5","r")
    f_name_list2 = ["ism_wind_cum_ml_pp_complete"+fV_str,"ism_wind_cum_ml_pp_complete2"+fV_str]
    for f_name in f_name_list2:
        f_med_dict[f_name] = file_grid2[f_name][:]

    file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    show = [0,1,2,3,4,5,6]

    if i == 0:

        for n in range(len(a_grid)):

            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str][n]/f_med_dict["ism_wind_tot_ml"+fV_str][n])
            if show_incomplete:
                py.plot(bin_mh_mid,quant,c=c_list[n],linestyle='--')

                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                quant[ok==False] = np.nan

            py.plot(bin_mh_mid,quant,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
            py.scatter(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str][n]/f_med_dict["ism_wind_tot_ml"+fV_str][n]),c=c_list[n],edgecolors="none",s=5)

        py.legend(loc='lower right',frameon=False,numpoints=1)

        py.ylabel(r"$\log_{10}(\eta_{\mathrm{ISM}}^Z \, / \eta_{\mathrm{ISM}})$")
        ylo = -0.6; yhi = 0.2

        py.axhline(0.0,c="k",alpha=0.5,linestyle='--')


    elif i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            quant = np.log10(f_med_dict["halo_wind_tot_Zml"+fV_str][n]/f_med_dict["halo_wind_tot_ml"+fV_str][n])
            if show_incomplete:
                py.plot(bin_mh_mid,quant,c=c_list[n],linestyle='--')

                ok = (f_med_dict["ism_wind_cum_ml_pp_complete"+fV_str][n] == 1) | (f_med_dict["ism_wind_cum_ml_pp_complete2"+fV_str][n] == 0)
                quant[ok==False] = np.nan

            py.plot(bin_mh_mid,quant,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n])
            py.scatter(bin_mh_mid,np.log10(f_med_dict["halo_wind_tot_Zml"+fV_str][n]/f_med_dict["halo_wind_tot_ml"+fV_str][n]),c=c_list[n],edgecolors="none",s=5)

        ylo = -1.2; yhi = 0.2

        #py.legend(loc='upper center',frameon=False,numpoints=1,bbox_to_anchor=(0.35,1.0))

        py.ylabel(r"$\log_{10}(\eta_{\mathrm{halo}}^Z \, / \eta_{\mathrm{halo}})$")
        py.xlabel(r"$\log_{10}(M_{200} \, / \mathrm{M_\odot})$")

        py.axhline(0.0,c="k",alpha=0.5,linestyle='--')

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "metal_mass_loading_ratio_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "metal_mass_loading_ratio_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Metal_Flows/'+fig_name)
py.show()
