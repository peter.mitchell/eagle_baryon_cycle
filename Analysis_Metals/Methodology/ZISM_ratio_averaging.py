import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

fVmax_cut = 0.25

# 100 Mpc ref with 200 tree snapshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 tree snipshots
sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!



########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_Zratio"+fV_str,"ism_wind_tot_Zratio_ind_av"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

file_grid2 = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_cumul_zbins_metal_flows.hdf5","r")

f_name_list2 = ["ism_wind_Zratio_cum_pp_med"+fV_str, "ism_wind_Zratio_cum_pp_lo"+fV_str, "ism_wind_Zratio_cum_pp_hi"+fV_str, "ism_wind_Zratio_cum_pp_complete"+fV_str, "ism_wind_Zratio_cum_pp_complete2"+fV_str]
for f_name in f_name_list2:
    f_med_dict[f_name] = file_grid2[f_name][:]
file_grid2.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32,2.49*2],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.97,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 1
subplots = panelplot(nrow,ncol)
lw = 1
xlo = 9.5; xhi = 15.0


for i,ax in enumerate(subplots):
    py.axes(ax)

    if i == 0:
        show = [0]
        show2 = [3]
        for n in range(len(a_grid)):

            quant = np.log10(f_med_dict["ism_wind_tot_Zratio"+fV_str][n])
            quant2 = np.log10(f_med_dict["ism_wind_tot_Zratio_ind_av"+fV_str][n])

            quant3 = np.log10(f_med_dict["ism_wind_Zratio_cum_pp_med"+fV_str][n])
            ok3 = (f_med_dict["ism_wind_Zratio_cum_pp_complete"+fV_str][n]==1)|(f_med_dict["ism_wind_Zratio_cum_pp_complete2"+fV_str][n]==0)

            if n not in show and n not in show2:
                continue
            elif n not in show:
                quant -= 99
                quant2 -= 99
                quant3 -= 99

            py.plot(bin_mh_mid,quant,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n],linewidth=lw)
            py.plot(bin_mh_mid,quant2,c=c_list[n],linewidth=lw,linestyle='--')
            py.plot(bin_mh_mid[ok3],quant3[ok3],linewidth=lw,linestyle=':',alpha=0.7,c=c_list[n])

            temp = quant3
            temp[ok3==False] = np.nan
            py.plot(bin_mh_mid, temp, linewidth=lw,linestyle=':',c=c_list[n])

        py.legend(loc='lower right',frameon=False,numpoints=1)

        py.ylabel(r"$\log_{10}(Z_{\mathrm{wind,ISM}} / Z_{\mathrm{ISM}} )$")
        ylo = -0.4; yhi = 0.2

        py.axhline(0.0,c="k",alpha=0.5,linestyle='--')

    elif i == 1:
        show = [3]

        for n in range(len(a_grid)):

            if n not in show:
                continue

            quant = np.log10(f_med_dict["ism_wind_tot_Zratio"+fV_str][n])
            quant2 = np.log10(f_med_dict["ism_wind_tot_Zratio_ind_av"+fV_str][n])

            quant3 = np.log10(f_med_dict["ism_wind_Zratio_cum_pp_med"+fV_str][n])
            ok3 = (f_med_dict["ism_wind_Zratio_cum_pp_complete"+fV_str][n]==1)|(f_med_dict["ism_wind_Zratio_cum_pp_complete2"+fV_str][n]==0)

            py.plot(bin_mh_mid,quant,label=(r"$\langle M_{\mathrm{out}}^{Z} \, M_{\mathrm{ISM}} \rangle \, / \langle M_{\mathrm{ISM}}^Z \, M_{\mathrm{out}} \rangle$"),c=c_list[n],linewidth=lw)
            py.plot(bin_mh_mid,quant2,label=(r"$\langle M_{\mathrm{out}}^{Z} \rangle \langle M_{\mathrm{ISM}} \rangle \, / \langle M_{\mathrm{ISM}}^Z \rangle \, / \langle M_{\mathrm{out}} \rangle$"),c=c_list[n],linewidth=lw,linestyle='--')
            py.plot(bin_mh_mid[ok3],quant3[ok3],linewidth=lw,linestyle=':',alpha=0.7,c=c_list[n])

            temp = quant3
            temp[ok3==False] = np.nan
            py.plot(bin_mh_mid, temp, linewidth=lw,linestyle=':',c=c_list[n],label="Median")

        py.legend(loc='lower right',frameon=False,numpoints=1)

        py.ylabel(r"$\log_{10}(Z_{\mathrm{wind,ISM}} / Z_{\mathrm{ISM}} )$")
        ylo = -0.4; yhi = 0.2

        py.axhline(0.0,c="k",alpha=0.5,linestyle='--')

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "ZISM_ratio_averaging_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Metal_Flows/'+fig_name)
py.show()
