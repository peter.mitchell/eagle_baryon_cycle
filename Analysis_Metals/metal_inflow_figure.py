import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

test_mode = False
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 snipshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc ref with 200 snipshots
sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc ref with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

part_mass = "particle_mass"
fVmax_cut = 0.25

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb)

########### Load tabulated efficiencies #########################
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows_test_mode.hdf5","r")
else:
    print "Reading", input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5"
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["fZcool_pristine_subnorm","fZcool_galtran_subnorm","fZcool_recooled_subnorm"+fV_str]
f_name_list += ["fZacc_pristine_subnorm","fZacc_fof_subnorm","fZacc_reaccreted_subnorm"+fV_str]

f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

from utilities_plotting import *

py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.25,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*2,2.49*2],
                    'figure.subplot.left':0.09,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'legend.fontsize':7,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

xlo = 9.5; xhi = 15.

c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
nrow = 2; ncol = 2
subplots = panelplot(nrow,ncol)
lw = 1

facc_med = f_med_dict["fZacc_pristine_subnorm"]
freacc_med = f_med_dict["fZacc_reaccreted_subnorm"+fV_str]
ffof_med = f_med_dict["fZacc_fof_subnorm"]
fcool_pristine_med = f_med_dict["fZcool_pristine_subnorm"]
fcool_recooled_med = f_med_dict["fZcool_recooled_subnorm"+fV_str]
fcool_galtran_med = f_med_dict["fZcool_galtran_subnorm"]

for i,ax in enumerate(subplots):
    py.axes(ax)

    show_z = [0,1,2,3,4,5,6]
    if i == 0:
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                py.plot(bin_mh_mid,np.log10(facc_med[n]+freacc_med[n]+ffof_med[n]),c=c_list[n],linewidth=lw,label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)))
                py.scatter(bin_mh_mid,np.log10(facc_med[n]+freacc_med[n]+ffof_med[n]),c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}}^Z / f_{\mathrm{B}} M_{\mathrm{H}} \, / \mathrm{Gyr^{-1}})$")

        py.legend(loc='upper right',frameon=False,numpoints=1)

        #py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,label="Gas")
        #py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=0.8,label="Dark matter",linestyle='--')
        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -2.49; yhi = 1.0

    show_z = [0,3]
    if i == 1:
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                py.plot(bin_mh_mid,np.log10(facc_med[n]),c=c_list[n],linewidth=lw,label="'Pristine'")
                py.plot(bin_mh_mid,np.log10(freacc_med[n]),c=c_list[n],linewidth=lw,linestyle='--',label="Recycled")
                py.plot(bin_mh_mid,np.log10(ffof_med[n]),c=c_list[n],linewidth=lw,linestyle=":",label="Transfer")

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,halo}}^Z / f_{\mathrm{B}} M_{\mathrm{H}} \, / \mathrm{Gyr^{-1}})$")

        py.legend(loc='upper right',frameon=False,numpoints=1)

        #label=(r"$%3.1f < z < %3.1f$" % (1./a_max[0]-1,1./a_min[0]-1))
        #py.annotate(label,(10,-4))

        #py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,label="Gas")
        #py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=0.8,label="Dark matter",linestyle='--')
        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -2.49; yhi = 1.0
        

    show_z = [0,1,2,3,4,5,6]
    if i == 2:
        for n in range(len(freacc_med[:,0])):
            if n in show_z:
                py.plot(bin_mh_mid,np.log10(fcool_pristine_med[n]+fcool_galtran_med[n]+fcool_recooled_med[n]),c=c_list[n],linewidth=lw)
                py.scatter(bin_mh_mid,np.log10(fcool_pristine_med[n]+fcool_recooled_med[n]+fcool_galtran_med[n]),c=c_list[n],edgecolors="none",s=5)

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^Z / f_{\mathrm{B}} M_{\mathrm{H}} \, / \mathrm{Gyr^{-1}})$")

      
        ylo = -3.0; yhi = 0.5

    show_z = [0,3]
    if i == 3:
        for n in range(len(facc_med[:,0])):
            if n in show_z:
                py.plot(bin_mh_mid,np.log10(fcool_pristine_med[n]),c=c_list[n],linewidth=lw,label="'Pristine'")
                py.plot(bin_mh_mid,np.log10(fcool_recooled_med[n]),c=c_list[n],linewidth=lw,linestyle='--',label="Recycled")
                py.plot(bin_mh_mid,np.log10(fcool_galtran_med[n]),c=c_list[n],linewidth=lw,linestyle=":",label="Transfer")

        py.ylabel(r"$\log(\dot{M}_{\mathrm{in,ISM}}^Z / f_{\mathrm{B}} M_{\mathrm{H}} \, / \mathrm{Gyr^{-1}})$")

        py.legend(loc='upper right',frameon=False,numpoints=1)

        
        #label=(r"$%3.1f < z < %3.1f$" % (1./a_max[3]-1,1./a_min[3]-1))
        #py.annotate(label,(10,-4))

        #py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=lw,label="Gas")
        #py.plot(bin_mh_mid,bin_mh_mid-99,c="k",linewidth=0.8,label="Dark matter",linestyle='--')
        #py.legend(loc='upper right',frameon=False,numpoints=1)

        ylo = -2.49; yhi = 1.0

        
    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))
    py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

fig_name = "metal_accretion_rates_subnorm.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Metal_Flows/'+fig_name)
py.show()
