import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

# 100 Mpc ref with 200 tree snapshots
sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

# Read C18 data
name = "C18_Zml_data.dat"
logmstar_c18, logZml_c18, lo_c18, hi_c18, logZratio_c18, lo_c18_2, hi_c18_2 = np.loadtxt(name,unpack=True)

########### Load tabulated efficiencies #########################

# Neistein uses dark matter subhalo mass as "halo mass"
if test_mode:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows_test_mode.hdf5","r")
else:
    file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5","r")

bin_mh_mid = file_grid["log_msub_grid"][:]
bin_mstar_mid = file_grid["log_mstar_grid"][:]

t_grid = file_grid["t_grid"][:]
a_grid = file_grid["a_grid"][:]
z_grid = 1./a_grid -1.0
t_min = file_grid["t_min"][:]
t_max = file_grid["t_max"][:]
a_min = file_grid["a_min"][:]
a_max = file_grid["a_max"][:]

fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
f_name_list = ["ism_wind_tot_Zml"+fV_str+"_mstar","ism_wind_tot_Zratio"+fV_str+"_mstar"]


f_med_dict = {}
for f_name in f_name_list:
    f_med_dict[f_name] = file_grid[f_name][:]

file_grid.close()

#print f_med_dict["ism_wind_tot_Zml"+fV_str+"_mstar"]
#quit()

from utilities_plotting import *

nrow = 2; ncol = 1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.19,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':8})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
subplots = panelplot(nrow,ncol)

xlo = 6.8; xhi = 12.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    show = [0]

    if i == 0:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            py.plot(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_Zratio"+fV_str+"_mstar"][n]),c=c_list[n])
            py.scatter(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_Zratio"+fV_str+"_mstar"][n]),c=c_list[n],edgecolors="none",s=5)

            #print np.log10(f_med_dict["ism_wind_tot_Zratio"+fV_str+"_mstar"][n])
            #quit()

        py.errorbar(logmstar_c18,logZratio_c18, yerr=[-lo_c18_2,hi_c18_2],fmt="kD")

        py.ylabel(r"$\log_{10}(Z_{\mathrm{wind,ism}} \, / Z_{\mathrm{ISM}})$")

    elif i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            py.plot(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str+"_mstar"][n]),c=c_list[n])
            py.scatter(bin_mstar_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str+"_mstar"][n]),c=c_list[n],edgecolors="none",s=5)

        py.errorbar(logmstar_c18,logZml_c18, yerr=[-lo_c18,hi_c18],fmt="kD")

        py.ylabel(r"$\log_{10}( \dot{M}_{\mathrm{wind,ism}}^{Z} \, / \dot{M}_\star^Z )$")

    py.xlabel(r"$\log(M_\star \, / \mathrm{M_\odot})$")

    #py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


if test_mode:
    fig_name = "metal_loading_c18_comp_"+sim_name+"_test_mode.pdf"
else:
    fig_name = "metal_loading_c18_comp_"+sim_name+".pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Metal_Flows/'+fig_name)
py.show()
