import numpy as np
import scipy.integrate as sci






case = 2

########### Case 1 - no outflows ###########
if case == 1:
    t_grid = np.arange(0.0,15.0,0.1)
    dt = 0.1

    Z_grid = np.zeros_like(t_grid)

    p = 0.02
    tau = 1.0 # Gyr

    def Zdot_larson72(Z):
        return (p-Z)/tau

    for i_t in range(len(t_grid[1:])):

        Zdot = Zdot_larson72(Z_grid[i_t])
        Z_grid[i_t+1] = Z_grid[i_t] + Zdot*dt


    Z_l72 = p * (1-np.exp(-t_grid/tau))

    from utilities_plotting import *
    py.plot(t_grid,Z_grid,c="b")
    py.plot(t_grid,Z_l72,c="r",linestyle='--')

    py.show()

############## Case 2 - outflows and stellar recycling #########
elif case == 2:
    
    t_grid = np.arange(0.0,15.0,0.1)
    dt = 0.1

    Z_grid = np.zeros_like(t_grid)

    p = 0.02
    tau = 3.0 # Gyr
    R = 0.4
    eta = 5

    def Zdot_larson72(Z):
        return (p-Z*(1-R+eta))/tau

    for i_t in range(len(t_grid[1:])):

        Zdot = Zdot_larson72(Z_grid[i_t])
        Z_grid[i_t+1] = Z_grid[i_t] + Zdot*dt


    Z_analytic = p / (1-R+eta) * (1-np.exp(-t_grid*(1-R+eta)/tau))

    from utilities_plotting import *
    py.plot(t_grid,Z_grid,c="b")
    py.plot(t_grid,Z_analytic,c="r",linestyle='--')
    
    py.axhline(p/(1-R+eta),c="k",linestyle='--')

    py.show()
