import numpy as np

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

############ Constant accretion rate model ######################

# Age of the Universe
t_universe = 10 # Gyr

# Accretion rate
def macc(mhalo):
    macc = 5 * (mhalo / 10**12) # Msun yr^-1
    macc *= 1e9 # Msun Gyr^-1
    return macc

# Star formation timescale
t_sf = 1 # Gyr

# Mass loading factor
def eta(mhalo):
    eta = 1 * (mhalo/10**12)**(-0.5)
    return eta

# Metal yield
p = 0.02

# numerical parameters
tolerance = 1e-6

nsnapshots = 200
dt = t_universe / float(nsnapshots)
tsnapshots = np.arange(0,t_universe+dt,dt)

# Halo masses
mhalo = 10**(np.arange(9.5, 15, 0.2))
nhalo = len(mhalo)

# Initialise baryon arrays
mism = np.zeros((nsnapshots,nhalo))
mstars = np.zeros_like(mism)

mzism = np.zeros_like(mism)
mzstars = np.zeros_like(mism)

mz_formed = np.zeros_like(mism)

for snapshot in range(nsnapshots-1):

    t_ts = tsnapshots[snapshot]
    t_ns = tsnapshots[snapshot+1]

    dt = t_ns - t_ts

    mism_ts = mism[snapshot]
    mstars_ts = mstars[snapshot]
    
    mzism_ts = mzism[snapshot]
    mzstars_ts = mzstars[snapshot]

    maccretion = np.ones_like(mism_ts) * macc(mhalo) * dt
    msf = mism_ts / t_sf * dt
    mej = msf * eta(mhalo)

    mzaccretion = np.zeros_like(mism_ts)
    mzinj = msf * p

    mzsf = np.zeros_like(mism_ts)
    mzej = np.zeros_like(mism_ts)
    ok = mism_ts > 0
    mzsf[ok] = mzism_ts[ok] / mism_ts[ok] * msf[ok]
    mzej[ok] = mzism_ts[ok] / mism_ts[ok] * mej[ok]

    ########## Apply limiters (using iterative approach) #################

    need_limiters = True
    n_iter = 0
    while need_limiters:
       
        problem_ism = msf + mej > mism_ts + maccretion
        rescale_sf = (msf[problem_ism] + mej[problem_ism]) / (mism_ts[problem_ism] + maccretion[problem_ism])
        msf[problem_ism] *= 1./(rescale_sf * (1+tolerance))
        mej[problem_ism] *= 1./(rescale_sf * (1+tolerance))

        problem_zism = mzsf + mzej > mzism_ts + mzaccretion + mzinj
        rescale_sf = (mzsf[problem_zism] + mzej[problem_zism]) / (mzism_ts[problem_zism] + mzaccretion[problem_zism] +mzinj[problem_zism])
        mzsf[problem_zism] *= 1./(rescale_sf * (1+tolerance))
        mzej[problem_zism] *= 1./(rescale_sf * (1+tolerance))
        
        if len(mism_ts[problem_ism]) == 0 and len(mism_ts[problem_zism]) == 0:
            need_limiters = False

        n_iter += 1
        if n_iter == 50:
            print "Warning: limiters are slow to converge"
        if n_iter > 1000:
            print "Error: limiters didn't converge after 1000 iterations"
            quit()
        
    ######### Exchange the masses ##############

    mism_ts += maccretion - msf - mej
    mstars_ts += msf

    mzism_ts += mzaccretion + mzinj - mzsf - mzej
    mzstars_ts += mzsf

    mz_formed[snapshot+1] += mzinj

    mism[snapshot+1] = mism_ts
    mzism[snapshot+1] = mzism_ts
    mstars[snapshot+1] = mstars_ts
    mzstars[snapshot+1] = mzstars_ts

    #Zism = mzism_ts / mism_ts
    #Zstar = mzstars_ts / mstars_ts

Zism = mzism / mism
fgas = mism / mstars

fzretain = (mzism + mzstars) / mz_formed

from utilities_plotting import *

py.figure()

py.subplot(221)
py.plot(np.log10(mhalo), np.log10(Zism[-1]))
py.ylabel(r"$\log(Z_{\mathrm{ISM}})$")
py.axhline(np.log10(0.02),linestyle='--')

py.plot([9,15],[-3, -3+6*0.5],linestyle='--') 

py.subplot(222)
py.plot(np.log10(mhalo), np.log10(fgas[-1]))
py.ylabel(r"$\log(f_{\mathrm{gas}})$")

py.subplot(223)
py.plot(np.log10(mhalo), np.log10(fzretain[-1]))
py.ylabel(r"$\log(f_{\mathrm{retain}})$")

py.show()
