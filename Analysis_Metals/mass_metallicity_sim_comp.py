import numpy as np
import h5py
import utilities_statistics as us

z = 0.1

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"






# 100 Mpc ref with 200 tree snapshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"
#snap_list, z_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/redshift_list.txt",unpack=True)

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"
#snap_list, z_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/redshift_list.txt",unpack=True)


sim_name_list = ["L0025N0376_REF_snap", "L0025N0752_Recal"]

def get_Z(sim_name):

    if sim_name == "L0025N0376_REF_snap":
        snap_list, z_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/redshift_list.txt",unpack=True)         
        snap = int(snap_list[np.argmin(abs(z_list-z))])
    elif sim_name == "L0025N0752_Recal":
        snip_list, z_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/redshift_list.txt",unpack=True)
        snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
        snip = np.argmin(abs(z_list-z))
        snap = int(snapshots[np.argmin(abs(snipshots-snip))])
    else:
        print "implement", sim_name
        quit()

    filename = "processed_subhalo_catalogue_" + sim_name + ".hdf5"
    File = h5py.File(input_path+filename,"r")

    subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]

    mZ_ISM = subhalo_group["mZ_gas_NSF_ISM"][:] + subhalo_group["mZ_gas_SF"][:]
    m_ISM = subhalo_group["mass_gas_NSF_ISM"][:] + subhalo_group["mass_gas_SF"][:]
    mZ_ISM_SF = subhalo_group["mZ_gas_SF"][:]
    m_ISM_SF = subhalo_group["mass_gas_SF"][:]
    mstars_30 = subhalo_group["mass_stars_30kpc"][:]

    Z_ISM = np.zeros_like(m_ISM)+np.nan
    ok = m_ISM > 0
    Z_ISM[ok] = mZ_ISM[ok] / m_ISM[ok]

    Z_ISM_SF = np.zeros_like(m_ISM)+np.nan
    ok = m_ISM_SF > 0
    Z_ISM_SF[ok] = mZ_ISM_SF[ok] / m_ISM_SF[ok]

    bins_mstar = np.arange(6,12.2,0.2)
    mstar_mid = 0.5*(bins_mstar[1:]+bins_mstar[0:-1])

    Z_ISM_med = np.zeros_like(mstar_mid)
    Z_ISM_lo = np.zeros_like(mstar_mid)
    Z_ISM_hi = np.zeros_like(mstar_mid)
    Z_ISM_complete = np.zeros_like(mstar_mid)

    complete = 0.8 # Only compute an average in bins where more than "complete" of the galaxies have finite gas mass
    Z_ISM_med, Z_ISM_lo, Z_ISM_hi, junk, Z_ISM_complete = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
    ok = Z_ISM_complete ==1

    Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, junk, Z_ISM_complete_SF = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM_SF,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
    ok_SF = Z_ISM_complete_SF ==1

    Z_ISM_av = [Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok]
    Z_ISM_SF_av = [Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, Z_ISM_complete_SF, ok_SF]

    return mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av

from utilities_plotting import *

nrow = 2; ncol = 1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.19/ncol,
                    'figure.subplot.bottom':0.19/nrow,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

py.figure()
subplots = panelplot(nrow,ncol)
c_list = ["k", "r"]

for i,ax in enumerate(subplots):
    py.axes(ax)
    
    if i == 0:
    
        for i_sim, sim_name in enumerate(sim_name_list):
            mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av = get_Z(sim_name)
            c=c_list[i_sim]
            Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok = Z_ISM_av
            
            py.scatter(np.log10(mstars_30), np.log10(Z_ISM),edgecolors="none",s=1,c=c)
            py.errorbar(mstar_mid,np.log10(Z_ISM_med),yerr=[abs(np.log10(Z_ISM_med)-np.log10(Z_ISM_lo)),abs(np.log10(Z_ISM_med)-np.log10(Z_ISM_hi))],fmt=c+"+",linewidth=1,mew=1)

    elif i == 1:

        for i_sim, sim_name in enumerate(sim_name_list):
            mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av = get_Z(sim_name)
            c=c_list[i_sim]
            Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok = Z_ISM_av
            Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, Z_ISM_complete_SF, ok_SF = Z_ISM_SF_av
        
            py.plot(mstar_mid[ok], np.log10(Z_ISM_med[ok]),c=c,linewidth=1.2,label="ISM")
            py.plot(mstar_mid, np.log10(Z_ISM_med),c=c,linewidth=1.2,alpha=0.7)

            py.plot(mstar_mid[ok_SF], np.log10(Z_ISM_med_SF[ok_SF]),c=c,linewidth=1.2,label="SF",linestyle='--')
            py.plot(mstar_mid, np.log10(Z_ISM_med_SF),c=c,linewidth=1.2,linestyle='--',alpha=0.7)
    
        py.legend(frameon=False,numpoints=1,loc="upper left")

    py.xlim((6.0,12.))
    py.ylim((-4.0,-1.0))

fig_name = "mass_metallicity_sim_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Metal_Flows/'+fig_name)
py.show()
