import numpy as np
from os import listdir
import h5py
import utilities_cosmology as uc
import match_searchsorted as ms
from scipy.interpolate import interp1d
import os
import time

fVmax_cut = 0.25

test_mode = False

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
output_path = input_path

#sim_name = "L0100N1504_REF_200_snip"
#sim_name = "L0050N0752_REF_snap"
#sim_name = "L0050N0752_NOAGN_snap"
#sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_50_snip"
#sim_name = "L0025N0376_REF_100_snip"
#sim_name = "L0025N0376_REF_200_snip"
#sim_name = "L0025N0376_recal"

part_mass = "particle_mass" # choose if you want use particle numbers (no stellar recycling) or particle masses (include stellar recycling)

# cosmological parameters
omm = 0.307
oml = 0.693
h=0.6777
omb = 0.0482519
fb = omb / omm
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses - note: don't use this when using dm-only trees!

#sim_name_list = ["L0025N0376_REF_200_snip", "L0025N0752_Recal"]
#label_list = ["Reference (25)", "Recal (25)"]

sim_name_list = ["L0100N1504_REF_200_snip", "L0025N0752_Recal"]
label_list = ["Reference (100)", "Recalibrated (25)"]

########### Load tabulated efficiencies #########################

output = []

for i_sim, sim_name in enumerate(sim_name_list):
    # Neistein uses dark matter subhalo mass as "halo mass"
    if test_mode:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows_test_mode.hdf5","r")
    else:
        file_grid = h5py.File(input_path+"efficiencies_grid_"+sim_name+"_"+part_mass+"_metal_flows.hdf5","r")

    bin_mh_mid = file_grid["log_msub_grid"][:]
    t_grid = file_grid["t_grid"][:]
    a_grid = file_grid["a_grid"][:]
    z_grid = 1./a_grid -1.0
    t_min = file_grid["t_min"][:]
    t_max = file_grid["t_max"][:]
    a_min = file_grid["a_min"][:]
    a_max = file_grid["a_max"][:]

    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list = ["ism_wind_tot_ml"+fV_str,"ism_wind_tot_Zml"+fV_str]

    f_med_dict = {}
    for f_name in f_name_list:
        f_med_dict[f_name] = file_grid[f_name][:]

    file_grid.close()

    output_i = [f_med_dict, a_min, a_max, bin_mh_mid]
    output.append(output_i)

from utilities_plotting import *

nrow = 2; ncol = 1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.18,
                    'figure.subplot.bottom':0.09,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

ylo = 6.0; yhi = 12.0

#c_list = ["b","g","r","c","m","y","k","tab:orange"]
c_list = ["k","m","b","c","g","y","tab:orange","r"]

py.figure()
np.seterr(all='ignore')
subplots = panelplot(nrow,ncol)

xlo = 9.5; xhi = 15.0
ls_list = ["-","--"]
lw = 1.0

for i,ax in enumerate(subplots):
    py.axes(ax)

    #show = [0,1,2,3,4,5,6]
    show = [0,2,4]

    if i == 0:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            for i_sim, sim_name in enumerate(sim_name_list):
                f_med_dict, a_min, a_max, bin_mh_mid = output[i_sim]

                if i_sim == 0:
                    py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]),label=(r"$%3.1f < z < %3.1f$" % (1./a_max[n]-1,1./a_min[n]-1)),c=c_list[n],linestyle=ls_list[i_sim],linewidth=lw)
                else:
                    py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_ml"+fV_str][n]),c=c_list[n],linestyle=ls_list[i_sim],linewidth=lw)

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\eta_{\mathrm{ISM}} = \langle \dot{M}_{\mathrm{wind,ism}} \rangle \, / \langle \dot{M}_\star \rangle)$")
        ylo = -0.75; yhi = 2.0

    if i == 1:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            for i_sim, sim_name in enumerate(sim_name_list):
                f_med_dict, a_min, a_max, bin_mh_mid = output[i_sim]

                if n == show[0]:
                    py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str][n]),label=label_list[i_sim],c=c_list[n],linestyle=ls_list[i_sim],linewidth=lw)
                else:
                    py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str][n]),c=c_list[n],linestyle=ls_list[i_sim],linewidth=lw)

        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\eta^Z_{\mathrm{ISM}} = \langle \dot{M}_{\mathrm{wind,ism}}^{Z} \rangle \, / \langle \dot{M}_\star^Z \rangle)$")
        ylo = -0.75; yhi = 2.0

    # No longer plotted here
    if i == 2:
        for n in range(len(a_grid)):

            if n not in show:
                continue

            c_index = float(n)/(len(a_grid)-1)

            for i_sim, sim_name in enumerate(sim_name_list):
                f_med_dict, a_min, a_max, bin_mh_mid = output[i_sim]

                #if n == show[0]:
                #    py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str][n]/f_med_dict["ism_wind_tot_ml"+fV_str][n]),label=label_list[i_sim],c=c_list[n],linestyle=ls_list[i_sim],linewidth=lw)
                #else:
                py.plot(bin_mh_mid,np.log10(f_med_dict["ism_wind_tot_Zml"+fV_str][n]/f_med_dict["ism_wind_tot_ml"+fV_str][n]),c=c_list[n],linestyle=ls_list[i_sim],linewidth=lw)

        py.axhline(0.0,linestyle=':', alpha=0.5,c="k")
        py.legend(loc='upper right',frameon=False,numpoints=1)
        py.ylabel(r"$\log(\eta^Z \, / \eta)$")
        ylo = -1.0; yhi = 0.5

        py.xlabel(r"$\log(M_{200} \, / \mathrm{M_\odot})$")

    py.ylim((ylo, yhi))
    py.xlim((xlo, xhi))


fig_name = "metal_mass_loading_sim_comp.pdf"
py.rcParams.update({'savefig.dpi':150})
py.savefig('/cosma/home/dphlss/d72fqv/Figures_Metal_Flows/'+fig_name)
py.show()
