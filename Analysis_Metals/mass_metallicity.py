import numpy as np
import h5py
import utilities_statistics as us

z = 0.1

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"


# 100 Mpc ref with 200 tree snapshots
#sim_name = "L0100N1504_REF_200_snip"

# 25 Mpc REF with 28 snapshots
#sim_name = "L0025N0376_REF_snap"
#snap_list, z_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/redshift_list.txt",unpack=True)

# 25 Mpc ref with 200 tree snapshots
#sim_name = "L0025N0376_REF_200_snip"

# 25 Mpc REF with 28 snapshots
sim_name = "L0025N0376_REF_snap"
snap_list, z_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/redshift_list.txt",unpack=True)


sim_name_list = ["L0025N0376_REF_snap"]

def get_Z(sim_name):

    if sim_name == "L0025N0376_REF_snap":
        snap_list, z_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/redshift_list.txt",unpack=True)         
        snap = int(snap_list[np.argmin(abs(z_list-z))])
    elif sim_name == "L0025N0752_Recal":
        snip_list, z_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/redshift_list.txt",unpack=True)
        snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
        snip = np.argmin(abs(z_list-z))
        snap = int(snapshots[np.argmin(abs(snipshots-snip))])
    else:
        print "implement", sim_name
        quit()

    if "snap" in sim_name:
        filename = "processed_subhalo_catalogue_" + sim_name + "_single_snap.hdf5"
    else:
        filename = "processed_subhalo_catalogue_" + sim_name + ".hdf5"
    File = h5py.File(input_path+filename,"r")

    subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]

    mZ_ISM = subhalo_group["mZ_gas_NSF_ISM"][:] + subhalo_group["mZ_gas_SF"][:]
    m_ISM = subhalo_group["mass_gas_NSF_ISM"][:] + subhalo_group["mass_gas_SF"][:]
    mZ_ISM_SF = subhalo_group["mZ_gas_SF"][:]
    m_ISM_SF = subhalo_group["mass_gas_SF"][:]
    mstars_30 = subhalo_group["mass_stars_30kpc"][:]

    Z_ISM_SFW = subhalo_group["Z_gas_NSF_ISM_SFR_weighted"][:] + subhalo_group["Z_gas_SF_SFR_weighted"][:]

    if "snap" in sim_name:
        mZ_ISM_30 = subhalo_group["mZ_gas_SF_30kpc"][:] + subhalo_group["mZ_gas_NSF_ISM_30kpc"][:]
        m_ISM_30 = subhalo_group["mass_gas_SF_30kpc"][:] + subhalo_group["mass_gas_NSF_ISM_30kpc"][:]

        mZ_SF_30 = subhalo_group["mZ_gas_SF_30kpc"][:]
        m_SF_30 = subhalo_group["mass_gas_SF_30kpc"][:]

        mO_ISM = subhalo_group["mO_gas_NSF_ISM"][:] + subhalo_group["mO_gas_SF"][:]
        mO_SF = subhalo_group["mO_gas_SF"][:]        

        mH_ISM = subhalo_group["mH_gas_NSF_ISM"][:] + subhalo_group["mH_gas_SF"][:]
        mH_SF = subhalo_group["mH_gas_SF"][:]        

    Z_ISM = np.zeros_like(m_ISM)+np.nan
    ok = m_ISM > 0
    Z_ISM[ok] = mZ_ISM[ok] / m_ISM[ok]

    Z_ISM_SF = np.zeros_like(m_ISM)+np.nan
    ok = m_ISM_SF > 0
    Z_ISM_SF[ok] = mZ_ISM_SF[ok] / m_ISM_SF[ok]

    if "snap" in sim_name:
        Z_ISM_30 = np.zeros_like(m_ISM)+np.nan
        ok = m_ISM_30 > 0
        Z_ISM_30[ok] = mZ_ISM_30[ok] / m_ISM_30[ok]

        Z_ISM_SF_30 = np.zeros_like(m_ISM_SF)+np.nan
        ok = m_SF_30 > 0
        Z_ISM_SF_30[ok] = mZ_SF_30[ok] / m_SF_30[ok]

        ZO_ISM = np.zeros_like(m_ISM)+np.nan
        ok = m_ISM > 0
        ZO_ISM[ok] = mO_ISM[ok] / mH_ISM[ok]

        ZO_SF = np.zeros_like(m_ISM)+np.nan
        ok = m_ISM_SF > 0
        ZO_SF[ok] = mO_SF[ok] / mH_SF[ok]

    bins_mstar = np.arange(6,12.2,0.2)
    mstar_mid = 0.5*(bins_mstar[1:]+bins_mstar[0:-1])

    complete = 0.8 # Only compute an average in bins where more than "complete" of the galaxies have finite gas mass
    Z_ISM_med, Z_ISM_lo, Z_ISM_hi, junk, Z_ISM_complete = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
    ok = Z_ISM_complete ==1

    Z_ISM_SFW_med, Z_ISM_SFW_lo, Z_ISM_SFW_hi, junk, Z_ISM_SFW_complete = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM_SFW,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
    ok_SFW = Z_ISM_SFW_complete ==1

    Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, junk, Z_ISM_complete_SF = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM_SF,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
    ok_SF = Z_ISM_complete_SF ==1

    if "snap" in sim_name:
        Z_ISM_30_med, Z_ISM_30_lo, Z_ISM_30_hi, junk, Z_ISM_30_complete = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM_30,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
        ok_30 = Z_ISM_30_complete ==1

        Z_ISM_med_SF_30, Z_ISM_lo_SF_30, Z_ISM_hi_SF_30, junk, Z_ISM_complete_SF_30 = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),Z_ISM_SF_30,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
        ok_SF_30 = Z_ISM_complete_SF_30 ==1

        ZO_ISM_med, ZO_ISM_lo, ZO_ISM_hi, junk, ZO_ISM_complete = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),ZO_ISM,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
        ok_O = ZO_ISM_complete ==1

        ZO_ISM_med_SF, ZO_ISM_lo_SF, ZO_ISM_hi_SF, junk, ZO_ISM_complete_SF = us.Calculate_Percentiles(bins_mstar,np.log10(mstars_30),ZO_SF,np.ones_like(mstars_30),1,lohi=(0.16,0.84),complete=0.8)
        ok_OSF = ZO_ISM_complete_SF ==1

    Z_ISM_av = [Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok]
    Z_ISM_SF_av = [Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, Z_ISM_complete_SF, ok_SF]
    Z_ISM_SFW_av = [Z_ISM_SFW_med, Z_ISM_SFW_lo, Z_ISM_SFW_hi, Z_ISM_SFW_complete, ok_SFW]

    print Z_ISM_av
    quit()

    if "snap" in sim_name:

        Z_ISM_30_av = [Z_ISM_30_med, Z_ISM_30_lo, Z_ISM_30_hi, Z_ISM_30_complete, ok_30]
        Z_ISM_SF_30_av = [Z_ISM_med_SF_30, Z_ISM_lo_SF_30, Z_ISM_hi_SF_30, Z_ISM_complete_SF_30, ok_SF_30]
        ZO_ISM_av = [ZO_ISM_med, ZO_ISM_lo, ZO_ISM_hi, ZO_ISM_complete, ok_O]
        ZO_ISM_SF_30_av = [ZO_ISM_med_SF, ZO_ISM_lo_SF, ZO_ISM_hi_SF, ZO_ISM_complete_SF, ok_OSF]
        
        return mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av, Z_ISM_SFW_av, Z_ISM_30_av, Z_ISM_SF_30_av, ZO_ISM_av, ZO_ISM_SF_30_av
       
    else:
        return mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av, Z_ISM_SFW_av

from utilities_plotting import *

nrow = 2; ncol = 1
py.rcParams.update(latexParams)
py.rcParams.update({'figure.subplot.wspace':0.11,'figure.subplot.hspace':0.0,
                    'figure.figsize':[3.32*ncol,2.49*nrow],
                    'figure.subplot.left':0.19/ncol,
                    'figure.subplot.bottom':0.19/nrow,
                    'figure.subplot.top':0.98,
                    'font.size':9,
                    'axes.labelsize':9,
                    'legend.fontsize':7})

py.figure()
subplots = panelplot(nrow,ncol)
c_list = ["k", "r"]

for i,ax in enumerate(subplots):
    py.axes(ax)
    
    if i == 0:
    
        for i_sim, sim_name in enumerate(sim_name_list):
            c=c_list[i_sim]
            if "snap" in sim_name:
                mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av, Z_ISM_SFW_av, Z_ISM_30_av, Z_ISM_SF_30_av, ZO_ISM_av, ZO_ISM_SF_30_av = get_Z(sim_name)
                Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok = Z_ISM_av

            else:
                mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av, Z_ISM_SFW_av = get_Z(sim_name)
                Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok = Z_ISM_av

            py.scatter(np.log10(mstars_30), np.log10(Z_ISM),edgecolors="none",s=1,c=c)
            py.errorbar(mstar_mid,np.log10(Z_ISM_med),yerr=[abs(np.log10(Z_ISM_med)-np.log10(Z_ISM_lo)),abs(np.log10(Z_ISM_med)-np.log10(Z_ISM_hi))],fmt=c+"+",linewidth=1,mew=1)

    elif i == 1:

        for i_sim, sim_name in enumerate(sim_name_list):

            if "snap" in sim_name:
                mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av, Z_ISM_SFW_av, Z_ISM_30_av, Z_ISM_SF_30_av, ZO_ISM_av, ZO_ISM_SF_30_av = get_Z(sim_name)
                Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok = Z_ISM_av
                Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, Z_ISM_complete_SF, ok_SF = Z_ISM_SF_av
                Z_ISM_SFW_med, Z_ISM_SFW_lo, Z_ISM_SFW_hi, Z_ISM_SFW_complete, ok_SFW = Z_ISM_SFW_av

                Z_ISM_30_med, Z_ISM_30_lo, Z_ISM_30_hi, Z_ISM_30_complete, ok_30 = Z_ISM_30_av
                Z_ISM_med_SF_30, Z_ISM_lo_SF_30, Z_ISM_hi_SF_30, Z_ISM_complete_SF_30, ok_SF_30 = Z_ISM_SF_30_av
                ZO_ISM_med, ZO_ISM_lo, ZO_ISM_hi, ZO_ISM_complete, ok_O = ZO_ISM_av
                ZO_ISM_med_SF, ZO_ISM_lo_SF, ZO_ISM_hi_SF, ZO_ISM_complete_SF, ok_OSF = ZO_ISM_SF_30_av
            else:
                mstars_30, mstar_mid, Z_ISM, Z_ISM_av, Z_ISM_SF_av, Z_ISM_SFW_av = get_Z(sim_name)
                Z_ISM_med, Z_ISM_lo, Z_ISM_hi, Z_ISM_complete, ok = Z_ISM_av
                Z_ISM_med_SF, Z_ISM_lo_SF, Z_ISM_hi_SF, Z_ISM_complete_SF, ok_SF = Z_ISM_SF_av
                Z_ISM_SFW_med, Z_ISM_SFW_lo, Z_ISM_SFW_hi, Z_ISM_SFW_complete, ok_SFW =Z_ISM_SFW_av
 
            py.plot(mstar_mid[ok], np.log10(Z_ISM_med[ok]),c="k",linewidth=1.0,label="ISM")
            py.plot(mstar_mid, np.log10(Z_ISM_med),c="k",linewidth=1.0,alpha=0.7)

            py.plot(mstar_mid[ok_SF], np.log10(Z_ISM_med_SF[ok_SF]),c=c,linewidth=1.0,label="SF",linestyle='--')
            py.plot(mstar_mid, np.log10(Z_ISM_med_SF),c="k",linewidth=1.0,linestyle='--',alpha=0.7)

            py.plot(mstar_mid[ok_SFW], np.log10(Z_ISM_SFW_med[ok_SFW]),c="g",linewidth=1.0,label="ISM (SFR-weighted)")
            py.plot(mstar_mid, np.log10(Z_ISM_SFW_med),c="g",linewidth=1.0,alpha=0.7)

            if "snap" in sim_name:
                py.plot(mstar_mid[ok_30], np.log10(Z_ISM_30_med[ok_30]),c=c,linewidth=1.5,label="ISM (r<30kpc)",linestyle='-.')
                py.plot(mstar_mid, np.log10(Z_ISM_30_med),c="k",linewidth=1.5,alpha=0.7,linestyle='-.')

                py.plot(mstar_mid[ok_SF_30], np.log10(Z_ISM_med_SF_30[ok_SF_30]),c=c,linewidth=1.5,label="SF (r<30kpc)",linestyle=':')
                py.plot(mstar_mid, np.log10(Z_ISM_med_SF_30),c="k",linewidth=1.5,alpha=0.7,linestyle=':')

                py.plot(mstar_mid[ok_O], np.log10(ZO_ISM_med[ok_O]),c="r",linewidth=1.0,label="ISM (Oxygen)")
                py.plot(mstar_mid, np.log10(ZO_ISM_med),c="r",linewidth=1.0,alpha=0.7)

                py.plot(mstar_mid[ok_OSF], np.log10(ZO_ISM_med_SF[ok_OSF]),c="r",linewidth=1.0,label="SF (Oxygen)",linestyle=":")
                py.plot(mstar_mid, np.log10(ZO_ISM_med_SF),c="r",linewidth=1.0,alpha=0.7,linestyle=":")
    
        py.legend(frameon=False,numpoints=1,loc="upper left")

    py.xlim((6.0,12.))
    py.ylim((-4.0+12,-1.0+12))

py.show()
