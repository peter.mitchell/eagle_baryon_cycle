import numpy as np
import h5py
import os
import glob
import match_searchsorted as ms

test_mode = False
subsample = False
n_per_bin = 1000

input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/"

if test_mode:
    print ""
    print ""
    print "Running in test mode"
    print ""
    print ""

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")
nsub1d = 2'''

# 25 Mpc Ref with 28 snaps
sim_name = "L0025N0376_REF_snap"
#sim_name = "L0025N0376_REF_snap_par_test"
#sim_name = "L0025N0376_REF_snap_par_test2"
snip_list = np.arange(0,29)[::-1]
snap_list = np.arange(0,29)[::-1]
nsub1d = 1

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_list = np.arange(1,201)[::-1] # starts from 1 for these trees
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]
nsub1d = 1 '''


# 50 Mpc model with no AGN, 28 snapshots
#sim_name = "L0050N0752_NOAGN_snap"
#snap_final = 28
#snap_list = np.arange(0,snap_final+1)[::-1]
#snip_list = np.copy(snap_list)
#nsub1d = 1

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched (likely because tree snapshots don't start at 0) for your selected trees"
    quit()

# Test mode only
if test_mode:
    if sim_name == "L0025N0376_REF_snap" or sim_name == "L0025N0376_REF_50_snip":
        subvol_choose = 36
    elif sim_name == "L0100N1504_REF_50_snip":
        subvol_choose = 2000
    else:
        print "err"
        quit()


output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"/"
if subsample:
    output_base = input_path + sim_name +"_subvols_nsub1d_"+str(nsub1d)+"_subsample_"+str(n_per_bin)+"/"
#output_base = input_path + sim_name+"_subvols/"
if test_mode:
    output_base = input_path + sim_name + "_subvols_nsub1d_"+str(nsub1d)+"_test_mode/"

input_path += "Processed_Catalogues/"

subhalo_prop_names = ["mchalo","mhhalo","group_number","subgroup_number","node_index","descendant_index", "m200_host"]
subhalo_prop_types =  ["float", "float", "int",         "int",            "int",       "int",             "float"]

subhalo_prop_names += ["x_halo","y_halo","z_halo","vx_halo","vy_halo","vz_halo","vmax","r200_host"]
subhalo_prop_types +=  ["float", "float", "float", "float", "float", "float", "float", "float"]

name_list = ["mass_gas_SF", "mass_gas_NSF_ISM", "mass_gas_NSF","mass_star", "mass_star_init", "mass_new_stars","mass_new_stars_init"]
name_list += ["mass_bh", "aform_bh","mass_acc_bh"]
name_list += ["mass_new_stars_init_100_30kpc"]
name_list += ["mZ_new_stars_100_30kpc", "mZ_stars_30kpc", "mZ_gas_SF", "mZ_gas_NSF", "mZ_gas_NSF_ISM", "mZ_star", "mZ_new_stars", "mZ_outside","mZ_outside_galaxy","mZ_injected"]
name_list += ["mZ_new_stars_init"]

name_list += ["mass_gas_SF_30kpc", "mZ_gas_SF_30kpc", "mass_stars_30kpc", "mass_gas_NSF_ISM_30kpc", "mZ_gas_NSF_ISM_30kpc"]
name_list += ["Z_gas_SF_SFR_weighted", "Z_gas_SF_30kpc_SFR_weighted", "Z_gas_NSF_ISM_SFR_weighted", "Z_gas_NSF_ISM_30kpc_SFR_weighted"]

if "snap" in sim_name: # Oxygen masses are available only for snapshots
    name_list += ["mO_gas_SF", "mO_gas_SF_30kpc", "mO_gas_NSF_ISM", "mO_gas_NSF_ISM_30kpc"]
    name_list += ["ZO_gas_SF_SFR_weighted", "ZO_gas_SF_30kpc_SFR_weighted", "ZO_gas_NSF_ISM_SFR_weighted", "ZO_gas_NSF_ISM_30kpc_SFR_weighted"]
    name_list += ["mH_gas_SF", "mH_gas_SF_30kpc", "mH_gas_NSF_ISM", "mH_gas_NSF_ISM_30kpc"]
    name_list += ["ZH_gas_SF_SFR_weighted", "ZH_gas_SF_30kpc_SFR_weighted", "ZH_gas_NSF_ISM_SFR_weighted", "ZH_gas_NSF_ISM_30kpc_SFR_weighted"]

# File to consolidate all processed galaxies within subvols into
filename = "processed_subhalo_catalogue_" + sim_name+"_single_snap"
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

File = h5py.File(input_path+filename,"a")

for snip, snap in zip(snip_list,snap_list):

    # Choose where to output subvol files and write directory if needed
    output_path = output_base + "Snip_"+str(snip)+"/"

    snap_group_str = "Snap_"+str(snap)
    if snap_group_str not in File:
        File.create_group(snap_group_str)
    snap_group = File[snap_group_str]

    subhalo_group_str = "subhalo_properties"
    if subhalo_group_str not in snap_group:
        snap_group.create_group(subhalo_group_str)
    subhalo_group = snap_group[subhalo_group_str]

    sub_input_list = []
    input_list = []
    for name in name_list:
        input_list.append([])
    for name in subhalo_prop_names:
        sub_input_list.append([])

    # Find all the subvolume files (they won't be sorted in order)
    subvols = glob.glob(output_path+'subhalo_catalogue_subvol_'+sim_name+'_snip_'+str(snip)+'_*')

    if len(subvols) == 0:
        continue

    # Gather together measurements
    for n, subvol in enumerate(subvols):

        if test_mode and "_"+str(subvol_choose)+".hdf5" not in subvol:
            continue

        try:
            File_subvol = h5py.File(subvol,"r")
        except:
            continue

        halo_group = File_subvol["subhalo_data"]

        if name_list[-1] in halo_group and name_list[0] in halo_group:
            if n == 0:
                print "Consolidating", snap, snip
            for i_name, name in enumerate(name_list):

                # Deal with 2d arrays
                if input_list[i_name] == [] and len(halo_group[name][:].shape) ==2:
                    input_list[i_name] = np.zeros((0,halo_group[name][:].shape[1]))

                input_list[i_name] = np.append(input_list[i_name], halo_group[name][:],axis=0)

            for i_name, name in enumerate(subhalo_prop_names):

                prop_type = subhalo_prop_types[i_name]
                if prop_type == "int":
                    if n == 0:
                        sub_input_list[i_name] = np.array([]).astype("int")
                    temp = halo_group[name][:]

                elif prop_type == "float":
                    temp = halo_group[name][:]
                else:
                    "Error: prop type",prop_type,"not understood"
                    quit()

                sub_input_list[i_name] = np.append(sub_input_list[i_name], temp)

            # Only keep processed subhalos
            for i_name, name in enumerate(name_list):
                if name == "mass_gas_SF":
                    i1 = i_name
                if name == "mass_gas_NSF":
                    i2 = i_name
                if name == "mass_star":
                    i3 = i_name

            ok = (np.isnan(input_list[i1])==False) & (input_list[i1] + input_list[i2] + input_list[i3] > 0)
            for i_name, name in enumerate(name_list):
                input_list[i_name] = input_list[i_name][ok]
            for i_name, name in enumerate(subhalo_prop_names):
                sub_input_list[i_name] = sub_input_list[i_name][ok]

        File_subvol.close()

    if len(input_list[0]) == 0:
        continue
    
    # Delete previous in the processed catalogue file
    for name in name_list:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]

    for name in subhalo_prop_names:
        for thing in subhalo_group:
            if thing == name:
                del subhalo_group[name]

    # Write measurements to catalogue file
    for i_name, name in enumerate(name_list):
        subhalo_group.create_dataset(name, data=input_list[i_name])

    for i_name, name in enumerate(subhalo_prop_names):
        subhalo_group.create_dataset(name, data=sub_input_list[i_name])

File.close()
