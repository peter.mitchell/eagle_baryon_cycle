import sys
sys.path.append("../.")
import time
import numpy as np
import measure_particles_functions as mpf
import match_searchsorted as ms
import measure_particles_classes as mpc
import utilities_cosmology as uc
import utilities_statistics as us

def Subhalo_Loop(inputs):

    time_subloop = time.time()

    i_halo, i_snip_glob, subhalo_props, part_data, sim_props, output_names = inputs

    time_track_select = 0.0
    time_track_ptr = 0.0


    # First init output containers for this subhalo (I do this for joblib implementation)
    halo_outflow_dict = {}; halo_mass_dict = {}

    output_mass_names, output_outflow_names = output_names
    
    for name in output_mass_names:
        halo_mass_dict[name] = 0.0
    for name in output_outflow_names:
        halo_outflow_dict[name] = 0.0

    # Unpack simulation properties
    sim_props_ts, boxsize, sim_name = sim_props
    h = sim_props_ts["h"]
    omm = sim_props_ts["omm"]
    expansion_factor_ts = sim_props_ts["expansion_factor"]
    redshift_ts = sim_props_ts["redshift"]
    mass_dm = np.copy(sim_props_ts["mass_dm"])

    t_ts, tlb_ts = uc.t_Universe(expansion_factor_ts, omm, h)
    
    # Unpack particle data
    gas_ts, star_ts = part_data

    # Set subhalo properties
    halo_group_number = subhalo_props["group_number"][i_halo]
    halo_subgroup_number = subhalo_props["subgroup_number"][i_halo]

    halo_mchalo = subhalo_props["mchalo"][i_halo]
    halo_mhhalo = subhalo_props["mhhalo"][i_halo]
        
    halo_x = subhalo_props["x_halo"][i_halo]
    halo_y = subhalo_props["y_halo"][i_halo]
    halo_z = subhalo_props["z_halo"][i_halo]
    halo_vx = subhalo_props["vx_halo"][i_halo]
    halo_vy = subhalo_props["vy_halo"][i_halo]
    halo_vz = subhalo_props["vz_halo"][i_halo]
    halo_vmax = subhalo_props["vmax"][i_halo]
    halo_r200_host = np.copy(subhalo_props["r200_host"][i_halo])*1e3/(1+redshift_ts)/h # pkpc 
    halo_subhalo_index = subhalo_props["subhalo_index"][i_halo]
    

    ############### Select particles and perform unit conversions ############################

    time_select = time.time()
    
    ###### Select central gas particles
    select_gas = mpf.Match_Index(halo_subhalo_index, gas_ts.subhalo_index_unique, gas_ts.particle_subhalo_index)

    # Convert to Msun
    g2Msun = 1./(1.989e33)
    mass_gas = np.copy(mpf.Index_Array(gas_ts.data["mass"],select_gas,None))
    mass_gas *= 1.989e43 * g2Msun / h

    coordinates_gas = mpf.Index_Array(gas_ts.data["coordinates"],select_gas,None)

    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_halo = [halo_x, halo_y, halo_z]
    coordinates_gas = mpf.Centre_Halo(coordinates_gas, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc
    # Put velocity into proper coordinates (kms)
    velocity_gas = np.copy(mpf.Index_Array(gas_ts.data["velocity"],select_gas,None)) * expansion_factor_ts**0.5
    # change velocities to halo frame
    velocity_gas += - np.array([halo_vx, halo_vy, halo_vz]) # note halo velocities were already in proper peculiar velocity units

    r_gas = np.sqrt(np.square(coordinates_gas[:,0])+np.square(coordinates_gas[:,1])+np.square(coordinates_gas[:,2]))
    r_gas_norm = np.swapaxes(np.array([coordinates_gas[:,0],coordinates_gas[:,1],coordinates_gas[:,2]]) / r_gas,0,1)
    cv_gas = np.sum(r_gas_norm * velocity_gas,axis=1)

    metallicity_gas = mpf.Index_Array(gas_ts.data["metallicity"],select_gas,None)
    mZ_gas = metallicity_gas*mass_gas
    if "snap" in sim_name:
        oxygen_gas = mpf.Index_Array(gas_ts.data["oxygen"],select_gas,None)
        mO_gas = oxygen_gas * mass_gas
        hydrogen_gas = mpf.Index_Array(gas_ts.data["hydrogen"],select_gas,None)
        mH_gas = hydrogen_gas * mass_gas

    order = np.argsort(metallicity_gas)
    print metallicity_gas[order]
    print oxygen_gas[order]
    print hydrogen_gas[order]
    for n in range(len(order)):
        print metallicity_gas[order][n], oxygen_gas[order][n], oxygen_gas[order][n]/metallicity_gas[order][n]
    print np.sum(mO_gas)/np.sum(mass_gas)
    print np.sum(mZ_gas)/np.sum(mass_gas)
    quit()

    internal_energy_gas = mpf.Index_Array(gas_ts.data["internal_energy"],select_gas,None)
    temperature_gas = mpf.Index_Array(gas_ts.data["temperature"],select_gas,None)
    density_gas = mpf.Index_Array(gas_ts.data["density"],select_gas,None)

    xh_sf_thresh = 0.752 # Hydrogen fraction used to compute star formation threshold - Schaye15
    nh_gas = density_gas * xh_sf_thresh * h**2 * expansion_factor_ts**(-3) * 6.769911178294543E-31 / (1.6726 * 10**(-24))
    temp = np.copy(metallicity_gas) # To avoid raising zero metallicity particles to a negative power
    ok = temp > 0
    temp[ok] = np.power(temp[ok]/0.002,-0.64)
    temp[ok==False] = 100
    nh_thresh = np.min( (np.zeros_like(nh_gas)+10.0 , 0.1 * temp), axis=0)

    # Particles can only form stars if they are within 0.5 dex of the Pressure floor (EoS)
    T_eos = 8e3 * np.power(nh_gas/0.1, 4/3.)
    SF = (nh_gas > nh_thresh) & (np.log10(temperature_gas) < np.log10(T_eos) +0.5)

    gas_props = [coordinates_gas, temperature_gas, nh_gas, SF]
    halo_props = [halo_r200_host, halo_subgroup_number, halo_vmax]
    tdyn_ts = t_ts * 0.1 # Approximation of the halo dynamical time, in Gyr
    sim_props = [redshift_ts, tdyn_ts, h]
    ISM =  mpf.Select_ISM_simple(gas_props, halo_props , sim_props)

    # Compute sfr (note for snapshots could instead use the recoreded SFR)
    m_h = 1.6726 * 10**(-27)
    kms = 1e3
    gamma = 5/3.0 # Ratio of specific heats
    fg = 1.0 # Gas fraction (assumed equal to 0) - see Schaye15
    cm = 1e2
    G = 6.67408e-11
    pc = 3.0857e16
    Msun = 1.989e30
    yr = 365.25*24*60**2
    kpc = pc * 1e3
    # See http://www.mpia.de/homes/dullemon/lectures/hydrodynamicsII/chap_1.pdf, pg 7
    P = (gamma-1) * nh_gas / xh_sf_thresh* cm**3 * m_h * internal_energy_gas * kms**2 # kg m s^-2 = Pa
    # Pressure has units of force/area = kg m s^-2 m^-2 = kg m s^-2
    A = np.ones_like(mass_gas) * 1.515e-4 # Msun yr^-1 kpc^-2
    n = np.zeros_like(mass_gas) + 1.4
    n[nh_gas > 1e3] = 2.0
    # Renormalize in the n=2 regime - this is done empericaly at present
    A[nh_gas > 1e3] *= 1./(10**2.26)
    sfr_gas = (mass_gas*Msun) * (A*Msun/yr/kpc**2) * (1*Msun/pc**2)**(-n) * (gamma/G *fg *P)**(0.5*(n-1)) # kg s^-1
    sfr_gas *= yr / Msun # Msun yr^-1


    ##### Select star particles that belong to desired group, subgroup
    select_star = mpf.Match_Index(halo_subhalo_index, star_ts.subhalo_index_unique, star_ts.particle_subhalo_index)

    # Convert to Msun
    mass_star = np.copy(mpf.Index_Array(star_ts.data["mass"],select_star,None))
    mass_star *= 1.989e43 * g2Msun / h

    mass_star_init = np.copy(mpf.Index_Array(star_ts.data["mass_init"],select_star,None))
    mass_star_init *= 1.989e43 * g2Msun / h

    coordinates_star = mpf.Index_Array(star_ts.data["coordinates"],select_star,None)
    # Put spatial position into proper, halo-centered units # pkpc
    coordinates_star = mpf.Centre_Halo(coordinates_star, coordinates_halo, boxsize) * 1e3 /h / (1+redshift_ts) # pkpc

    r_star = np.sqrt(np.square(coordinates_star[:,0])+np.square(coordinates_star[:,1])+np.square(coordinates_star[:,2]))

    # Work out which star particles formed between now and the previous timestep
    aform_star = mpf.Index_Array(star_ts.data["aform"],select_star,None)
    
    t_star, junk = uc.t_Universe(aform_star, omm, h)
    age_star = (t_ts - t_star)*1e3 # Myr

    time_track_select += time.time() - time_select
    time_ptr = time.time()
    
    ################ Do measurements ####################

    halo_mass_dict["mass_stars_30kpc"] = np.sum(mass_star[r_star<30])

    halo_mass_dict["mass_gas_SF_30kpc"] = np.sum(mass_gas[SF&(r_gas<30)])
    halo_mass_dict["mZ_gas_SF_30kpc"] = np.sum(mZ_gas[SF&(r_gas<30)])
    halo_mass_dict["mass_gas_NSF_ISM_30kpc"] = np.sum(mass_gas[(SF==False)&ISM&(r_gas<30)])
    halo_mass_dict["mZ_gas_NSF_ISM_30kpc"] = np.sum(mZ_gas[(SF==False)&ISM&(r_gas<30)])

    Z_gas = mZ_gas / mass_gas

    halo_mass_dict["Z_gas_SF_SFR_weighted"] = np.sum((sfr_gas*Z_gas)[SF]) / np.sum((sfr_gas)[SF])
    halo_mass_dict["Z_gas_SF_30kpc_SFR_weighted"] = np.sum((sfr_gas*Z_gas)[SF&(r_gas<30)]) / np.sum((sfr_gas)[SF&(r_gas<30)])
    halo_mass_dict["Z_gas_NSF_ISM_SFR_weighted"] = np.sum((sfr_gas*Z_gas)[(SF==False)&ISM]) / np.sum((sfr_gas)[(SF==False)&ISM])
    halo_mass_dict["Z_gas_NSF_ISM_30kpc_SFR_weighted"] = np.sum((sfr_gas*Z_gas)[(SF==False)&ISM&(r_gas<30)]) / np.sum((sfr_gas)[(SF==False)&ISM&(r_gas<30)])

    if "snap" in sim_name:
        halo_mass_dict["mO_gas_SF"] = np.sum(mO_gas[SF])
        halo_mass_dict["mO_gas_SF_30kpc"] = np.sum(mO_gas[SF&(r_gas<30)])
        halo_mass_dict["mO_gas_NSF_ISM"] = np.sum(mO_gas[(SF==False)&ISM])
        halo_mass_dict["mO_gas_NSF_ISM_30kpc"] = np.sum(mO_gas[(SF==False)&ISM&(r_gas<30)])
        
        ZO_gas = mO_gas / mass_gas
        halo_mass_dict["ZO_gas_SF_SFR_weighted"] = np.sum((sfr_gas*ZO_gas)[SF]) / np.sum((sfr_gas)[SF])
        halo_mass_dict["ZO_gas_SF_30kpc_SFR_weighted"] = np.sum((sfr_gas*ZO_gas)[SF&(r_gas<30)]) / np.sum((sfr_gas)[SF&(r_gas<30)])
        halo_mass_dict["ZO_gas_NSF_ISM_SFR_weighted"] = np.sum((sfr_gas*ZO_gas)[(SF==False)&ISM]) / np.sum((sfr_gas)[(SF==False)&ISM])
        halo_mass_dict["ZO_gas_NSF_ISM_30kpc_SFR_weighted"] = np.sum((sfr_gas*ZO_gas)[(SF==False)&ISM&(r_gas<30)]) / np.sum((sfr_gas)[(SF==False)&ISM&(r_gas<30)])

        halo_mass_dict["mH_gas_SF"] = np.sum(mH_gas[SF])
        halo_mass_dict["mH_gas_SF_30kpc"] = np.sum(mH_gas[SF&(r_gas<30)])
        halo_mass_dict["mH_gas_NSF_ISM"] = np.sum(mH_gas[(SF==False)&ISM])
        halo_mass_dict["mH_gas_NSF_ISM_30kpc"] = np.sum(mH_gas[(SF==False)&ISM&(r_gas<30)])
        
        ZH_gas = mH_gas / mass_gas
        halo_mass_dict["ZH_gas_SF_SFR_weighted"] = np.sum((sfr_gas*ZH_gas)[SF]) / np.sum((sfr_gas)[SF])
        halo_mass_dict["ZH_gas_SF_30kpc_SFR_weighted"] = np.sum((sfr_gas*ZH_gas)[SF&(r_gas<30)]) / np.sum((sfr_gas)[SF&(r_gas<30)])
        halo_mass_dict["ZH_gas_NSF_ISM_SFR_weighted"] = np.sum((sfr_gas*ZH_gas)[(SF==False)&ISM]) / np.sum((sfr_gas)[(SF==False)&ISM])
        halo_mass_dict["ZH_gas_NSF_ISM_30kpc_SFR_weighted"] = np.sum((sfr_gas*ZH_gas)[(SF==False)&ISM&(r_gas<30)]) / np.sum((sfr_gas)[(SF==False)&ISM&(r_gas<30)])

    time_track_subloop = time.time() - time_subloop

    # Pack wallclock time tracking information
    timing_info = [time_track_select, time_track_ptr, time_track_subloop]

    return halo_mass_dict, halo_outflow_dict, timing_info
