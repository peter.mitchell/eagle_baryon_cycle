import numpy as np
import h5py
import os
import glob
import utilities_cosmology as uc
import utilities_statistics as us
from scipy.interpolate import interp2d, interp1d

#input_path = "/cosma6/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"
input_path = "/cosma7/data/dp004/d72fqv/Baryon_Cycle/Processed_Catalogues/"

test_mode = False
cumulative_pp = False # Note this is not the measurement I used to assess completeness in the wind paper - that gets its own efficiency_grid file.
use_aperture = True
extra_wind_measurements = True

v50 = False

# Only include particles with a stellar mass above some value (if use apertue this stellar mass will be the 30kpc aperture mass)
# If you want no selection on stellar mass then set to a negative number
mstar_cut = -1e9
#mstar_cut = 0.0
#mstar_cut = 1.81e7 # 10 particles at standard Eagle resolution (assuming stars have starting gas mass, which they won't)

#star_mass = "n_part" # in this mode, we just measure particle numbers, not particle masses
star_mass = "particle_mass" # in this mode, we measure the particle masses and hence include stellar recycling terms

if star_mass == "n_part":
    print "In this case I need to remove hard-coded particle mass (for recal)"
    quit()

halo_mass = "m200"

# 100 Mpc REF with 200 snips
'''sim_name = "L0100N1504_REF_200_snip"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/trees/treedir_200/tree_200.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0100N1504/snipshots_v3/snipnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 50 Mpc REF with 28 snaps
'''sim_name = "L0050N0752_REF_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0050N0752/PE/REFERENCE/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 28 snaps
'''sim_name = "L0025N0376_REF_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 28 snaps par test
'''sim_name = "L0025N0376_REF_snap_par_test"
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/snapshots/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.arange(0,snap_final+1)[::-1]'''

# 25 Mpc REF with 50 snips
'''sim_name = "L0025N0376_REF_50_snip"
snap_final = 50
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/quarter_snipshots/trees/treedir_050/tree_050.0.hdf5"
snip_list = [  1,   9,  17,  25,  33,  41,  49,  57,  65,  73,  81,  89,  97, 105, 113, 121, 129, 137,
               145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225, 233, 241, 249, 257, 265, 273, 281,
               289, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369, 377, 385, 393, 399][::-1]'''

# 25 Mpc REF with 100 snips
'''sim_name = "L0025N0376_REF_100_snip"
snap_final = 100
snap_list = np.arange(0,snap_final+1)[::-1]
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/half_snipshots/trees/treedir_100/tree_100.0.hdf5"
snip_list = [  1,  5,  9, 13, 17, 21, 25, 29, 33, 37, 41, 45, 49, 53, 57, 61, 65, 69,
               73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121, 125, 129, 133, 137, 141,
               145, 149, 153, 157, 161, 165, 169, 173, 177, 181, 185, 189, 193, 197, 201, 205, 209, 213,
               217, 221, 225, 229, 233, 237, 241, 245, 249, 253, 257, 261, 265, 269, 273, 277, 281, 285,
               289, 293, 297, 301, 305, 309, 313, 317, 321, 325, 329, 333, 337, 341, 345, 349, 353, 357,
               361, 365, 369, 373, 377, 381, 385, 389, 393, 397, 399][::-1]'''

# 25 Mpc REF with 200 snips
'''sim_name = "L0025N0376_REF_200_snip"
snap_final = 200
snap_list = np.arange(1,snap_final+1)[::-1] # These trees start at 1
tree_file = "/cosma7/data/dp004/jch/Eagle/Merger_Trees/L0025N0376/all_snipshots/trees/treedir_200/tree_200.0.hdf5"
snip_list = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35,
             37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71,
             73, 75, 77, 79, 81, 83, 85, 87, 89, 91, 93, 95, 97, 99, 101, 103, 105, 107,
             109, 111, 113, 115, 117, 119, 121, 123, 125, 127, 129, 131, 133, 135, 137, 139, 141, 143,
             145, 147, 149, 151, 153, 155, 157, 159, 161, 163, 165, 167, 169, 171, 173, 175, 177, 179,
             181, 183, 185, 187, 189, 191, 193, 195, 197, 199, 201, 203, 205, 207, 209, 211, 213, 215,
             217, 219, 221, 223, 225, 227, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251,
             253, 255, 257, 259, 261, 263, 265, 267, 269, 271, 273, 275, 277, 279, 281, 283, 285, 287,
             289, 291, 293, 295, 297, 299, 301, 303, 305, 307, 309, 311, 313, 315, 317, 319, 321, 323,
             325, 327, 329, 331, 333, 335, 337, 339, 341, 343, 345, 347, 349, 351, 353, 355, 357, 359,
             361, 363, 365, 367, 369, 371, 373, 375, 377, 379, 381, 383, 385, 387, 389, 391, 393, 395,
             397, 399][::-1]'''

# 25 Mpc ref 300 snapshots
'''sim_name = "L0025N0376_REF_300_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/trees/treedir_299/tree_299.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/300_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 400 snapshots
'''sim_name = "L0025N0376_REF_400_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/trees/treedir_399/tree_399.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/400_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 500 snapshots
#sim_name = "L0025N0376_REF_500_snap"
#sim_name = "L0025N0376_REF_500_snap_bound_only_version"
'''sim_name = "L0025N0376_REF_500_snap_bound_only_version_fixedfSNe"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/trees/treedir_499/tree_499.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/500_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 25 Mpc ref 1000 snapshots
'''sim_name = "L0025N0376_REF_1000_snap"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/trees/treedir_999/tree_999.0.hdf5"
snip_list, snap_list = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0376/PE/REFERENCE_ApogeeRun/1000_snapshots/snapnums.txt",unpack=True)
snip_list = np.rint(snip_list[::-1]).astype("int")
snap_list = np.rint(snap_list[::-1]).astype("int")'''

# 50 Mpc no-AGN 28 snaps
'''sim_name = "L0050N0752_NOAGN_snap"
tree_file = "/cosma7/data/dp004/jch/Eagle/Database/TreeData/L0050N0752_EagleVariation_NOAGN/trees/treedir_028/tree_028.0.hdf5"
snap_final = 28
snap_list = np.arange(0,snap_final+1)[::-1]
snip_list = np.copy(snap_list)'''

# 25 Mpc - recal (high-res), 202 snipshots
sim_name = "L0025N0752_Recal"
#tree_file = "/oldcosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/tree_202.0.hdf5"
tree_file = "/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/trees/treedir_202/tree_202.0.hdf5"
snap_final = 202
#snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
# Temp solution until JCH relocates his old files (Otherwise I have copied over to my allocation cosma6)
snipshots, snapshots = np.loadtxt("/cosma5/data/jch/Eagle/MergerTrees/L0025N0752/RECALIBRATED/snapnums.txt",unpack=True)
snip_list = np.rint(snipshots[::-1]).astype("int")
snap_list = np.rint(snapshots[::-1]).astype("int")

if len(snip_list) != len(snap_list):
    print "Error: snap and snip lists mismatched - (probably because tree snapshots don't start at 0)"
    quit()

# SN feedback parameters
e_sn = 8.73e15 # ergs per gram - energy injected per unit mass
g2Msun = 1.989e33
e_sn *= g2Msun # erg per Msun

# Work out conversion factor from energy injected to mass heated
# This assumes mu = 0.6 (molecular weight)
# For derivation see DallVecchia 12 (eqn3) and Schaye 15
dT_SN = 10.0**7.5 # K, SNe temperature jump
dT_AGN = 10.0**8.5 # K, AGN temperature jump
gamma = 5/3. # Ideal monatomic gas
mh = 1.6726219e-27 # kg
kb = 1.38064852e-23 # m2 kg s-2 K-1
Msun = g2Msun *1e-3
erg = 1e-7 # J
mu = 0.6 # Assumption
mheated_perE_injected_sn = (gamma-1) * (mu * mh)/kb / dT_SN / Msun *erg # Msun per erg # Note this is assuming the temmperature of the gas before heating is negligible.
mheated_perE_injected_AGN = (gamma-1) * (mu * mh)/kb / dT_AGN / Msun *erg # Msun per erg

# Convert from Msun km^2 s^-2
to_erg = Msun * 1e6 / erg


# AGN feedback parameters
eps_r = 0.1 # Radiative efficiency (fraction of accreted rest-mass energy which is radiated)
eps_f = 0.15 # Coupling efficiency between radiated energy and the ISM
c = 299792458 # ms-1
c2 = c**2 * 1e7 * g2Msun *1e-3 # erg per Msun
e_bh = eps_r *eps_f / (1-eps_r)*c2 # energy injected per unit mass growth of black holes (erg per Msun)

if "NOAGN" in sim_name:
    e_bh *= 0

omm = 0.307
oml = 1 - omm
omb = 0.0482519
fb = omb / omm
h=0.6777
mgas_part = 1.2252497*10**-4 # initial gas particle mass in code units (AT FIDUCIAL EAGLE RESOLUTION ONLY!) - this is only used if we are using particles numbers (ala Neistein) instead of masses to circumvent stellar recycling
dm2tot_factor = omm / (omm-omb) # rescale dm masses to total masses

# Spacing of bins
log_dmh = 0.2
bin_mh = np.arange(8.0, 15.1,0.2)
bin_mh_mid = 0.5*(bin_mh[1:] + bin_mh[0:-1])

n_bins = len(bin_mh_mid)

bin_mstar = np.arange(7.0, 12.5,(12.5-7)/(n_bins+1))
bin_mstar_mid = 0.5*(bin_mstar[1:]+bin_mstar[0:-1])
bin_vmax = np.arange(1.4, 3.0,(3.0-1.4)/(n_bins+1))
bin_vmax_mid = 0.5*(bin_vmax[1:]+bin_vmax[0:-1])
bin_vcirc = np.arange(1.4, 2.8,(2.8-1.4)/(n_bins+1))
bin_vcirc_mid = 0.5*(bin_vcirc[1:]+bin_vcirc[0:-1])
bin_sfr = np.arange(-3.0, 2.0,(5.0)/(n_bins+1))
bin_sfr_mid = 0.5*(bin_sfr[1:]+bin_sfr[0:-1])
bin_ssfr = np.arange(-3.0, 2.0,(5.0)/(n_bins+1))
bin_ssfr_mid = 0.5*(bin_ssfr[1:]+bin_ssfr[0:-1])

# Set values of ln(expansion factor) that will define the centre of each redshift bin
lna_mid_choose = np.array([-0.05920349, -0.42317188, -0.7114454,  -1.0918915,  -1.3680345,  -1.610239, -1.9068555,  -2.3610115])

File_tree = h5py.File(tree_file)
a = File_tree["outputTimes/expansion"][:][::-1]
File_tree.close()

t, tlb = uc.t_Universe(a, omm, h)

if len(a) != len(snap_list):
    print "problem"
    quit()

lna_temp = np.log(a)
bin_mid_snap = np.zeros(len(lna_mid_choose))
bin_mid_t = np.zeros(len(lna_mid_choose))
bin_mid_a = np.zeros(len(lna_mid_choose))
for i, lna_i in enumerate(lna_mid_choose):
    index = np.argmin(abs(lna_i - np.log(a)))
    bin_mid_snap[i] = snap_list[index]
    bin_mid_a[i] = a[index]
    bin_mid_t[i] = t[index]

# Compute the median expansion factor for each bin
jimbo = [[]]
counter = 0
for i_snap, snap in enumerate(snap_list[:-1]):

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    if ind_bin_snap > len(jimbo)-1:
        jimbo.append([])
        
    jimbo[ind_bin_snap].append(a[i_snap])

bin_a_median = np.zeros(len(lna_mid_choose))
for n in range(len(lna_mid_choose)):
    bin_a_median[n] = np.median(jimbo[n])
print "median expansion factor for each redshift bin", bin_a_median

t_min = np.zeros_like(lna_mid_choose)+1e9
t_max = np.zeros_like(lna_mid_choose)
a_min = np.zeros_like(lna_mid_choose)+1e9
a_max = np.zeros_like(lna_mid_choose)

name_list = ["mchalo","subgroup_number","vmax","m200_host", "r200_host"]
name_list += ["mass_gas_SF", "mass_gas_NSF", "mass_star", "mass_star_init", "mass_new_stars","mass_new_stars_init"]
name_list += ["mass_bh", "aform_bh","mass_acc_bh"]
name_list += ["mass_new_stars_init_100_30kpc", "mass_stars_30kpc"]

name_list += ["mass_join_halo_wind_from_ism"]
name_list += ["mass_ism_wind_cumul", "mass_ism_to_halo_wind_cumul"]
#name_list += ["time_join_halo_wind_from_ism"]

if "snap" in sim_name:
    name_list += ["fb_e_times_mass_new_star","mass_dir_heat_in_ism","mass_dir_heat_in_nism"]

name_list += ["mass_t_ejecta","mass_t_wind","mass_t_ejecta_ret","mass_t_wind_ret"]

if extra_wind_measurements:
    if "snap" in sim_name:
        name_list += ["dEdt_SNe","dEdt_AGN","dEdt_SNe_sat", "dEdt_AGN_sat"]

    #name_list += ["P50mw","Pmean","a_grav","m_shell"]

    vmin =[0.0, 0.125, 0.25, 0.5, 1.0]
    for vmin_i in vmin:

        #name_list += ["P50mw_out_"+str(vmin_i).replace(".","p")+"vmax","P50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

        name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]

        name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]

        if v50:
            name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]

fVmax_cuts = [0.125, 0.25, 0.5]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    name_list += ["mass_sf_ism_join_wind"+fV_str,"mass_nsf_ism_join_wind"+fV_str,"mass_join_halo_wind"+fV_str]
    name_list += ["mass_ism_reheated_join_wind"+fV_str,"mass_halo_reheated_join_wind"+fV_str]
    name_list += ["mass_nsf_ism_join_wind_from_SF"+fV_str, "mass_ism_reheated_join_wind_from_SF"+fV_str]
    
    if extra_wind_measurements:
        for vmin_i in vmin:

            name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

            if v50:
                name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

    if "snap" in sim_name:
        name_list += ["mass_join_ism_wind_dir_heat"+fV_str, "mass_join_halo_wind_dir_heat"+fV_str]

# Need to ensure a normal dataset goes as the last entry
name_list += ["mass_gas_NSF_ISM"]


# 3 plots
# 1) fraction of direct heating that took place inside ISM
# 2) fraction of ISM wind that was directly heated
# 3) fraction of halo wind that was directly heated

data_list = []
dt_list = []

for lna in lna_mid_choose:
    dt_list.append([])
    data_list.append({})
    for name in name_list:
        data_list[-1][name] = []

f_name_list = ["aform_bh", "sfr_subnorm", "ism_wind_cum_ml","f_ism_wind_leaves_halo","f_halo_wind_from_ism"]
#f_name_list += ["time_join_halo_wind_from_ism"]

'''if extra_wind_measurements:
    f_name_list += ["dmdr_shell", "P50grad_shell", "Ggrad_shell"]

    for fVmax_cut in fVmax_cuts:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
        f_name_list += ["P50fwgrad"+fV_str+"_shell"]'''

if "snap" in sim_name:
    f_name_list += ["f_injected_energy_SNe"]
    f_name_list += ["f_heated_mass_SNe","f_dir_heated_mass_in_ISM"]
    f_name_list += ["SNe_energy_injection_rate", "SNe_energy_injection_rate_vcnorm"]
    f_name_list += ["SNe_momentum_injection_rate", "SNe_momentum_injection_rate_vcnorm"]
    f_name_list += ["AGN_energy_injection_rate", "AGN_energy_injection_rate_vcnorm"]
    f_name_list += ["AGN_momentum_injection_rate", "AGN_momentum_injection_rate_vcnorm"]

    if extra_wind_measurements:
        f_name_list += ["dEdt_SNe_shell","dEdt_AGN_shell","dEdt_SNe_sat_shell", "dEdt_AGN_sat_shell"]


for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list += ["sf_wind_ml"+fV_str,"nsfism_wind_ml"+fV_str,"ism_reheat_wind_ml"+fV_str,"halo_wind_ml"+fV_str,"halo_reheat_wind_ml"+fV_str,"ism_wind_tot_ml"+fV_str,"halo_wind_tot_ml"+fV_str,"ism_wind_tot_subnorm"+fV_str,"halo_wind_tot_subnorm"+fV_str,"ism_wind_tot"+fV_str]
    f_name_list += ["ism_wind_tot_from_SF_ml"+fV_str]

    if "snap" in sim_name:
        f_name_list += ["f_mass_ism_wind_dir_heated"+fV_str,"f_mass_halo_wind_dir_heated"+fV_str]

f_name_list2 = ["vmax_med","mstar_med","R200_med"]
for fVmax_cut in fVmax_cuts:
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
    f_name_list2 += ["halo_wind_tot_ml_med"+fV_str,"halo_wind_tot_ml_lo"+fV_str,"halo_wind_tot_ml_hi"+fV_str,"ism_wind_tot_ml_med"+fV_str,"ism_wind_tot_ml_lo"+fV_str,"ism_wind_tot_ml_hi"+fV_str, "halo_wind_complete"+fV_str, "ism_wind_complete"+fV_str]

    if extra_wind_measurements:
        for vmin_i in vmin:

            f_name_list2 += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
            f_name_list2 += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
            f_name_list2 += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
            f_name_list2 += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]

            f_name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
            f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

            f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
            f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]
            f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_vcnorm"]

            if v50:
                f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
                f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
                f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
                f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]

                f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
                f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_med"]
                f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]
                f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind_complete"]

                f_name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                f_name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                f_name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]
                f_name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"]

if extra_wind_measurements:
    for vmin_i in vmin:

        f_name_list2 += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
        f_name_list2 += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
        f_name_list2 += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
        f_name_list2 += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_med"]

        f_name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax"]
        f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax"]

        f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"]
        f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"]
        f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_vcnorm"]

        f_name_list += ["dmdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]
        f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM"]

        f_name_list += ["dpdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"]
        f_name_list += ["dEkdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"]
        f_name_list += ["dEtdt_out_"+str(vmin_i).replace(".","p")+"vmax_ISM_vcnorm"]

        if v50:

            f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
            f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
            f_name_list2 += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]
            f_name_list2 += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]

            f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
            f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax_med"]
            f_name_list2 += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]
            f_name_list2 += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax_complete"]

            f_name_list += ["v50mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            f_name_list += ["v90mw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            f_name_list += ["v50fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
            f_name_list += ["v90fw_out_"+str(vmin_i).replace(".","p")+"vmax"]
 
f_med_dict = {}
f_sat_med_dict = {}
f_med_dict_mstar = {}
f_sat_med_dict_mstar = {}
f_med_dict_vmax = {}
f_sat_med_dict_vmax = {}
f_med_dict_vcirc = {}
f_sat_med_dict_vcirc = {}
f_med_dict_sfr = {}
f_med_dict_ssfr = {}

f_med_dict2 = {}
f_sat_med_dict2 = {}

f_med_dict2_vcirc = {}
f_med_dict2_mstar = {}
f_med_dict2_sfr = {}

for f_name in f_name_list:
    
    if "_out" in f_name or "shell" in f_name:
        n_rbins = 10
        if "grad" in f_name:
            n_rbins = 9
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
    else:
        f_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_sat_med_dict[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vmax[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vmax[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_sat_med_dict_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

    f_med_dict_sfr[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
    f_med_dict_ssfr[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

n_rbins = 10

for f_name in f_name_list2:
    if "_out" in f_name or "shell" in f_name:
        n_rbins = 10
        f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
        f_sat_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1, n_rbins))
        f_med_dict2_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
        f_med_dict2_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
        f_med_dict2_sfr[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1,n_rbins))
    else:
        f_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
        f_sat_med_dict2[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
        f_med_dict2_mstar[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
        f_med_dict2_vcirc[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))
        f_med_dict2_sfr[f_name] = np.zeros((len(lna_mid_choose),len(bin_mh)-1))

filename = "processed_subhalo_catalogue_" + sim_name 
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"
File = h5py.File(input_path+filename)

print "reading data"

for i_snap, snap in enumerate(snap_list[:-1]):

    '''if i_snap > 3:
        if i_snap ==4:
            print ""
            print "temp hack"
            print ""
        continue'''

    if i_snap == 0:
        continue

    ind_bin_snap = np.argmin(abs(np.log(a)[i_snap]-lna_mid_choose))

    print i_snap,"of",len(snap_list[:-1]), "snap", snap

    try:
        subhalo_group = File["Snap_"+str(snap)+"/subhalo_properties"]
    except:
        print "no output file for this snap"
        continue

    #for thing in subhalo_group:
    #    print thing
    #quit()

    if name_list[-1] in subhalo_group:

        data = subhalo_group[name_list[-1]][:]

        if use_aperture:
            mstars = subhalo_group["mass_stars_30kpc"] # Msun
        else:
            mstars = subhalo_group["mass_star"] # Msun

        ok = (np.isnan(data)==False) & (mstars>mstar_cut)

        for i_name, name in enumerate(name_list):
            data = subhalo_group[name][:]
            if name == "Group_R_Crit200_host" or name == "r200_host":
                data *= a[i_snap] # pMpc h^-1
            ngal_tot = len(data)

            if len(data.shape) == 1:
                data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok])


            elif len(data.shape) == 2:
                if data_list[ind_bin_snap][name] == []:
                    data_list[ind_bin_snap][name] = np.zeros((0, data.shape[1] ))
                data_list[ind_bin_snap][name] = np.append(data_list[ind_bin_snap][name], data[ok],axis=0)
            else:
                print "yikeso"
                quit()
            
        ngal = len(data[ok])
        print ngal, ngal_tot

        dt = t[i_snap] - t[i_snap+1]
        dt_list[ind_bin_snap] = np.append(dt_list[ind_bin_snap], np.zeros(ngal)+dt)

        t_min[ind_bin_snap] = min(t_min[ind_bin_snap], t[i_snap])
        t_max[ind_bin_snap] = max(t_max[ind_bin_snap], t[i_snap])
        a_min[ind_bin_snap] = min(a_min[ind_bin_snap], a[i_snap])
        a_max[ind_bin_snap] = max(a_max[ind_bin_snap], a[i_snap])
    else:
        print "no data for this snap"

for i_bin in range(len(lna_mid_choose)):

    data = data_list[i_bin]

    print "computing statistics for time bin", i_bin+1,"of",len(lna_mid_choose)

    dt = dt_list[i_bin]

    if len(data[name_list[-1]]) == 0:
        continue
               

    # Only keep subhalos where measurements have been successfully performed
    ok_measure = (data["mass_gas_NSF"]+data["mass_gas_SF"]+data["mass_star"]) >= 0
    dt = dt[ok_measure]
    for name in name_list:
        data[name] = data[name][ok_measure]

    # Peform unit conversions
    g2Msun = 1./(1.989e33)
    vmax = data["vmax"]
    R200 = data["r200_host"] /h * 1e3 # pkpc - comoving was already converted earlier
    G = 6.674e-11 # m^3 kg^-1 s-2
    Msun = 1.989e30 # kg
    Mpc = 3.0857e22
    vcirc = np.sqrt(G * data["m200_host"]*Msun / (data[ "r200_host"]*Mpc/h)) / 1e3 # kms^-1 - note comoving conversion was done earlier

    if use_aperture:
        sfr = data["mass_new_stars_init_100_30kpc"] / 100 / 1e6 # Msun yr^-1
        mstars = data["mass_stars_30kpc"] # Msun
    else:
        sfr = data["mass_new_stars_init"] / dt * 1e-9 # Msun yr^-1
        mstars = data["mass_star"]

    ssfr = sfr / mstars * 1e9 # Gyr^-1

    mchalo = data["mchalo"]
    if halo_mass == "m200":
        print "Using m200 instead of subhalo mass - this will only work for centrals"
        mchalo = data["m200_host"]
        dm2tot_factor = 1.0 # In this case there is no need to correct dm mass to total mass

    # It's probably a better solution to output mass rather than particle numbers 
    if star_mass == "n_part":
        for name in name_list:
            if "n_" in name:
                data[name] *= mgas_part * 1.989e43 * g2Msun / h

    subgroup_number = data["subgroup_number"]

    #### Compute some percentile efficiencies ########
    cent = subgroup_number == 0
    weight_cent = np.zeros_like(dt[cent])
    dt_u = np.unique(dt)
    for dt_u_i in dt_u:
        ok2_cent = dt[cent] == dt_u_i
        weight_cent[ok2_cent] = dt[cent][ok2_cent] / len(dt[cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point

    # Loop over radial velocity cuts
    for fVmax_cut in fVmax_cuts:
        fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

        mass_out = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
        f_med_dict2["halo_wind_tot_ml_med"+fV_str][i_bin], f_med_dict2["halo_wind_tot_ml_lo"+fV_str][i_bin], f_med_dict2["halo_wind_tot_ml_hi"+fV_str][i_bin],junk,f_med_dict2["halo_wind_complete"+fV_str][i_bin] = \
            us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/data["mass_new_stars_init"][cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
 
        mass_out = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
        f_med_dict2["ism_wind_tot_ml_med"+fV_str][i_bin], f_med_dict2["ism_wind_tot_ml_lo"+fV_str][i_bin], f_med_dict2["ism_wind_tot_ml_hi"+fV_str][i_bin],junk,f_med_dict2["ism_wind_complete"+fV_str][i_bin] = \
            us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mass_out[cent]/data["mass_new_stars_init"][cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

        f_med_dict2_mstar["ism_wind_tot_ml_med"+fV_str][i_bin], f_med_dict2_mstar["ism_wind_tot_ml_lo"+fV_str][i_bin], f_med_dict2_mstar["ism_wind_tot_ml_hi"+fV_str][i_bin],junk,f_med_dict2_mstar["ism_wind_complete"+fV_str][i_bin] = \
            us.Calculate_Percentiles(bin_mstar,np.log10(mstars[cent]),mass_out[cent]/data["mass_new_stars_init"][cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

        f_med_dict2_sfr["ism_wind_tot_ml_med"+fV_str][i_bin], f_med_dict2_sfr["ism_wind_tot_ml_lo"+fV_str][i_bin], f_med_dict2_sfr["ism_wind_tot_ml_hi"+fV_str][i_bin],junk,f_med_dict2_sfr["ism_wind_complete"+fV_str][i_bin] = \
            us.Calculate_Percentiles(bin_sfr,np.log10(sfr[cent]),mass_out[cent]/data["mass_new_stars_init"][cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

        f_med_dict2_vcirc["ism_wind_tot_ml_med"+fV_str][i_bin], f_med_dict2_vcirc["ism_wind_tot_ml_lo"+fV_str][i_bin], f_med_dict2_vcirc["ism_wind_tot_ml_hi"+fV_str][i_bin],junk,f_med_dict2_vcirc["ism_wind_complete"+fV_str][i_bin] = \
            us.Calculate_Percentiles(bin_vcirc,np.log10(vcirc[cent]),mass_out[cent]/data["mass_new_stars_init"][cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

        if extra_wind_measurements:
            for vmin_i in vmin:

                v_str = "_out_"+str(vmin_i).replace(".","p")+"vmax"+fV_str+"_wind"

                if v50:
                    # Bins with no particles in the shell shouldn't have 0 for the median velocity
                    issue = data["v50mw"+v_str] == 0
                    data["v50mw"+v_str][issue] = np.nan
                    issue = data["v50fw"+v_str] == 0
                    data["v50fw"+v_str][issue] = np.nan

                    issue = data["v90mw"+v_str] == 0
                    data["v90mw"+v_str][issue] = np.nan
                    issue = data["v90fw"+v_str] == 0
                    data["v90fw"+v_str][issue] = np.nan

                for i_r in range(n_rbins):

                    f_med_dict2["dmdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dmdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                    f_med_dict2["dpdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dpdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                    f_med_dict2["dEkdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dEkdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                    f_med_dict2["dEtdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dEtdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

                    if v50:
                        f_med_dict2["v50mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                        f_med_dict2["v90mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

                        f_med_dict2["v50fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                        f_med_dict2["v90fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)


    f_med_dict2["vmax_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),vmax[cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
    f_med_dict2["R200_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),R200[cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
    f_med_dict2["mstar_med"][i_bin], junk, junk, junk, junk = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]),mstars[cent],np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)


    if extra_wind_measurements:
        for vmin_i in vmin:

            for i_r in range(n_rbins):

                v_str = "_out_"+str(vmin_i).replace(".","p")+"vmax"

                if v50:
                    # Bins with no particles in the shell shouldn't have 0 for the median velocity
                    issue = data["v50mw"+v_str] == 0
                    data["v50mw"+v_str][issue] = np.nan
                    issue = data["v50fw"+v_str] == 0
                    data["v50fw"+v_str][issue] = np.nan

                    issue = data["v90mw"+v_str] == 0
                    data["v90mw"+v_str][issue] = np.nan
                    issue = data["v90fw"+v_str] == 0
                    data["v90fw"+v_str][issue] = np.nan

                f_med_dict2["dmdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dmdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                f_med_dict2["dpdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dpdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                f_med_dict2["dEkdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dEkdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                f_med_dict2["dEtdt"+v_str+"_med"][i_bin,:,i_r],j,j,j,j = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["dEtdt"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)

                if v50:
                    f_med_dict2["v50mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                    f_med_dict2["v90mw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90mw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90mw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                    f_med_dict2["v50fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v50fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v50fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
                    f_med_dict2["v90fw"+v_str+"_med"][i_bin,:,i_r],j,j,j,f_med_dict2["v90fw"+v_str+"_complete"][i_bin,:,i_r] = us.Calculate_Percentiles(bin_mh,np.log10(mchalo[cent]), data["v90fw"+v_str][:,i_r][cent], np.ones_like(weight_cent),1,lohi=(0.16,0.84),complete=0.8)
            

    #### Compute mean efficiencies ########
    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])
        ok_mstar = (np.log10(mstars) > bin_mstar[j_bin]) & (np.log10(mstars) < bin_mstar[j_bin+1])
        ok_vmax = (np.log10(vmax) > bin_vmax[j_bin]) & (np.log10(vmax) < bin_vmax[j_bin+1])
        ok_vcirc = (np.log10(vcirc) > bin_vcirc[j_bin]) & (np.log10(vcirc) < bin_vcirc[j_bin+1])
        ok_sfr = (np.log10(sfr) > bin_sfr[j_bin]) & (np.log10(sfr) < bin_sfr[j_bin+1])
        ok_ssfr = (np.log10(ssfr) > bin_ssfr[j_bin]) & (np.log10(ssfr) < bin_ssfr[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)
        ok_sat = ok & (subgroup_number > 0)

        ok_cent_mstar = ok_mstar & (subgroup_number == 0)
        ok_sat_mstar = ok_mstar & (subgroup_number >0)
        ok_cent_vmax = ok_vmax & (subgroup_number == 0)
        ok_sat_vmax = ok_vmax & (subgroup_number >0)
        ok_cent_vcirc = ok_vcirc & (subgroup_number == 0)
        ok_sat_vcirc = ok_vcirc & (subgroup_number >0)
        ok_cent_sfr = ok_sfr & (subgroup_number == 0)
        ok_cent_ssfr = ok_ssfr & (subgroup_number == 0)

        weight_cent = np.zeros_like(dt[ok_cent])
        weight_sat = np.zeros_like(dt[ok_sat])
        weight_cent_mstar = np.zeros_like(dt[ok_cent_mstar])
        weight_sat_mstar = np.zeros_like(dt[ok_sat_mstar])
        weight_cent_vmax = np.zeros_like(dt[ok_cent_vmax])
        weight_sat_vmax = np.zeros_like(dt[ok_sat_vmax])
        weight_cent_vcirc = np.zeros_like(dt[ok_cent_vcirc])
        weight_sat_vcirc = np.zeros_like(dt[ok_sat_vcirc])
        weight_cent_sfr = np.zeros_like(dt[ok_cent_sfr])
        weight_cent_ssfr = np.zeros_like(dt[ok_cent_ssfr])
        
        dt_u = np.unique(dt[ok])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent] == dt_u_i
            ok2_sat = dt[ok_sat] == dt_u_i
            weight_cent[ok2_cent] = dt[ok_cent][ok2_cent] / len(dt[ok_cent][ok2_cent]) # Weight galaxies by number per snapshot per time interval - see save_sam_rates.m for reference point
            weight_sat[ok2_sat] = dt[ok_sat][ok2_sat] / len(dt[ok_sat][ok2_sat])

        dt_u = np.unique(dt[ok_mstar])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_mstar] == dt_u_i
            ok2_sat = dt[ok_sat_mstar] == dt_u_i
            weight_cent_mstar[ok2_cent] = dt[ok_cent_mstar][ok2_cent] / len(dt[ok_cent_mstar][ok2_cent])
            weight_sat_mstar[ok2_sat] = dt[ok_sat_mstar][ok2_sat] / len(dt[ok_sat_mstar][ok2_sat])

        dt_u = np.unique(dt[ok_vmax])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_vmax] == dt_u_i
            ok2_sat = dt[ok_sat_vmax] == dt_u_i
            weight_cent_vmax[ok2_cent] = dt[ok_cent_vmax][ok2_cent] / len(dt[ok_cent_vmax][ok2_cent])
            weight_sat_vmax[ok2_sat] = dt[ok_sat_vmax][ok2_sat] / len(dt[ok_sat_vmax][ok2_sat])

        dt_u = np.unique(dt[ok_vcirc])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_vcirc] == dt_u_i
            ok2_sat = dt[ok_sat_vcirc] == dt_u_i
            weight_cent_vcirc[ok2_cent] = dt[ok_cent_vcirc][ok2_cent] / len(dt[ok_cent_vcirc][ok2_cent])
            weight_sat_vcirc[ok2_sat] = dt[ok_sat_vcirc][ok2_sat] / len(dt[ok_sat_vcirc][ok2_sat])

        dt_u = np.unique(dt[ok_sfr])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_sfr] == dt_u_i
            weight_cent_sfr[ok2_cent] = dt[ok_cent_sfr][ok2_cent] / len(dt[ok_cent_sfr][ok2_cent])

        dt_u = np.unique(dt[ok_ssfr])
        for dt_u_i in dt_u:
            ok2_cent = dt[ok_cent_ssfr] == dt_u_i
            weight_cent_ssfr[ok2_cent] = dt[ok_cent_ssfr][ok2_cent] / len(dt[ok_cent_ssfr][ok2_cent])
        

        mass_new_stars = data["mass_new_stars_init"]
        mass_acc_bh = data["mass_acc_bh"]

        if "snap" in sim_name:
            fb_e_times_mass_new_star = data["fb_e_times_mass_new_star"]

        for f_name in f_name_list:
            
            for fVmax_cut in fVmax_cuts:
                fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"
                if fV_str in f_name:
                    break

            if f_name == "sf_reheat_ml":
                mass_out = data["mass_sf_ism_reheated"]
            elif f_name == "nsfism_reheat_ml":
                mass_out = data["mass_nsf_ism_reheated"]
            elif f_name == "ism_reheat_tot_ml":
                mass_out = data["mass_sf_ism_reheated"] + data["mass_nsf_ism_reheated"]
            elif f_name == "halo_reheat_ml":
                mass_out = data["mass_halo_reheated"]
            elif f_name == "f_injected_energy_SNe" or f_name == "f_heated_mass_SNe":
                mass_out = fb_e_times_mass_new_star # Dummy
            elif f_name == "f_ism_wind_leaves_halo":
                mass_out = data["mass_ism_to_halo_wind_cumul"]
            elif f_name == "sf_wind_ml"+fV_str:
                mass_out = data["mass_sf_ism_join_wind"+fV_str]
            elif f_name == "nsfism_wind_ml"+fV_str:
                mass_out = data["mass_nsf_ism_join_wind"+fV_str]
            elif f_name == "ism_reheat_wind_ml"+fV_str:
                mass_out = data["mass_ism_reheated_join_wind"+fV_str]
            elif f_name == "ism_wind_tot_ml"+fV_str or f_name == "ism_wind_tot"+fV_str or f_name == "ism_wind_tot_subnorm"+fV_str:
                mass_out = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
            elif f_name == "halo_wind_ml"+fV_str:
                mass_out = data["mass_join_halo_wind"+fV_str]
            elif f_name == "halo_reheat_wind_ml"+fV_str:
                mass_out = data["mass_halo_reheated_join_wind"+fV_str]
            elif f_name == "halo_wind_tot_ml"+fV_str or f_name == "halo_wind_tot_subnorm"+fV_str:
                mass_out = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
            elif f_name == "ism_tot_eml"+fV_str or f_name == "ism_tot_mlmh"+fV_str or f_name == "f_mass_ism_wind_dir_heated"+fV_str:
                mass_out = data["mass_sf_ism_join_wind"+fV_str] + data["mass_nsf_ism_join_wind"+fV_str] + data["mass_ism_reheated_join_wind"+fV_str]
            elif f_name == "halo_tot_eml"+fV_str or f_name == "halo_tot_mlmh"+fV_str or f_name == "f_mass_halo_wind_dir_heated"+fV_str:
                mass_out = data["mass_join_halo_wind"+fV_str] + data["mass_halo_reheated_join_wind"+fV_str]
            elif f_name == "f_halo_wind_from_ism":
                mass_out = data["mass_join_halo_wind_from_ism"]
            elif f_name == "ism_wind_tot_el"+fV_str:
                mass_out = data["internal_energy_join_ism_wind"+fV_str] + data["kinetic_energy_join_ism_wind"+fV_str]
            elif f_name == "halo_wind_tot_el"+fV_str:
                mass_out = data["internal_energy_join_halo_wind"+fV_str] + data["kinetic_energy_join_halo_wind"+fV_str]
            elif f_name == "sfr_subnorm":
                mass_out = mass_new_stars
            elif f_name == "ism_wind_tot_from_SF_ml"+fV_str:
                mass_out = data["mass_nsf_ism_join_wind_from_SF"+fV_str] + data["mass_ism_reheated_join_wind_from_SF"+fV_str]+data["mass_sf_ism_join_wind"+fV_str]
            else:
                if f_name != "aform_bh" and f_name != "ism_wind_cum_ml" and "_injection_rate" not in f_name and f_name != "f_dir_heated_mass_in_ISM" and "_out" not in f_name and "shell" not in f_name and "time_join_halo_wind" not in f_name:
                    print "No", f_name
                    quit()

            if "subnorm" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor*fb*mchalo[ok_cent]*weight_cent) # Gyr^-1
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat]/dt[ok_sat]*weight_sat) / np.sum(dm2tot_factor*fb*mchalo[ok_sat]*weight_sat)

                f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) / np.sum(dm2tot_factor*fb*mchalo[ok_cent_mstar]*weight_cent_mstar) # Gyr^-1
                f_sat_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_mstar]/dt[ok_sat_mstar]*weight_sat_mstar) / np.sum(dm2tot_factor*fb*mchalo[ok_sat_mstar]*weight_sat_mstar)

                f_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vmax]/dt[ok_cent_vmax]*weight_cent_vmax) / np.sum(dm2tot_factor*fb*mchalo[ok_cent_vmax]*weight_cent_vmax) # Gyr^-1
                f_sat_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vmax]/dt[ok_sat_vmax]*weight_sat_vmax) / np.sum(dm2tot_factor*fb*mchalo[ok_sat_vmax]*weight_sat_vmax)

            elif f_name == "f_injected_energy_SNe":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(fb_e_times_mass_new_star[ok_cent]*e_sn*weight_cent) / np.sum((fb_e_times_mass_new_star[ok_cent]*e_sn+mass_acc_bh[ok_cent]*e_bh)*weight_cent) # Dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(fb_e_times_mass_new_star[ok_sat]*e_sn*weight_sat) / np.sum((fb_e_times_mass_new_star[ok_sat]*e_sn+mass_acc_bh[ok_sat]*e_bh)*weight_sat) # Dimensionless
            elif f_name == "f_heated_mass_SNe":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(fb_e_times_mass_new_star[ok_cent]*e_sn*weight_cent*mheated_perE_injected_sn) / np.sum((fb_e_times_mass_new_star[ok_cent]*e_sn*mheated_perE_injected_sn+mass_acc_bh[ok_cent]*e_bh*mheated_perE_injected_AGN)*weight_cent) # Dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(fb_e_times_mass_new_star[ok_sat]*e_sn*weight_sat*mheated_perE_injected_sn) / np.sum((fb_e_times_mass_new_star[ok_sat]*e_sn*mheated_perE_injected_sn+mass_acc_bh[ok_sat]*e_bh*mheated_perE_injected_AGN)*weight_sat) # Dimensionless

            elif f_name == "f_dir_heated_mass_in_ISM":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(data["mass_dir_heat_in_ism"][ok_cent]*weight_cent) / np.sum((data["mass_dir_heat_in_ism"]+data["mass_dir_heat_in_nism"])[ok_cent]*weight_cent) # Dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(data["mass_dir_heat_in_ism"][ok_sat]*weight_sat) / np.sum((data["mass_dir_heat_in_ism"]+data["mass_dir_heat_in_nism"])[ok_sat]*weight_sat) # Dimensionless

            elif f_name == "f_mass_ism_wind_dir_heated"+fV_str:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(data["mass_join_ism_wind_dir_heat"+fV_str][ok_cent]*weight_cent) / np.sum(mass_out[ok_cent]*weight_cent)
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(data["mass_join_ism_wind_dir_heat"+fV_str][ok_sat]*weight_sat) / np.sum(mass_out[ok_sat]*weight_sat)

            elif f_name == "f_mass_halo_wind_dir_heated"+fV_str:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(data["mass_join_halo_wind_dir_heat"+fV_str][ok_cent]*weight_cent) / np.sum(mass_out[ok_cent]*weight_cent)
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(data["mass_join_halo_wind_dir_heat"+fV_str][ok_sat]*weight_sat) / np.sum(mass_out[ok_sat]*weight_sat)

            elif "f_ism_wind_leaves_halo" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum((data["mass_ism_wind_cumul"][ok_cent])*weight_cent) # dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat]*weight_sat) / np.sum((data["mass_ism_wind_cumul"][ok_sat])*weight_sat)

            elif "f_halo_wind_from_ism" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum((data["mass_join_halo_wind"+fV_str][ok_cent]+data["mass_halo_reheated_join_wind"+fV_str][ok_cent])*weight_cent) # dimensionless

            elif "aform_bh" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(data["aform_bh"][ok_cent&(data["aform_bh"]>0)]*weight_cent[data["aform_bh"][ok_cent]>0]) / np.sum(weight_cent[data["aform_bh"][ok_cent]>0]) # dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(data["aform_bh"][ok_sat&(data["aform_bh"]>0)]*weight_sat[data["aform_bh"][ok_sat]>0]) / np.sum(weight_sat[data["aform_bh"][ok_sat]>0])               

            elif f_name == "ism_wind_cum_ml":
                mass_out = data["mass_ism_wind_cumul"]
                sfr_cumul = data["mass_star_init"]
                
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]*weight_cent) / np.sum(sfr_cumul[ok_cent]*weight_cent) # Dimensionless
                f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]*weight_cent_mstar) / np.sum(sfr_cumul[ok_cent_mstar]*weight_cent_mstar) # Dimensionless

            elif f_name == "ism_wind_tot"+fV_str:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(weight_cent) *1e-9# Msun yr ^-1
                f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) / np.sum(weight_cent_mstar) *1e-9
                f_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vmax]/dt[ok_cent_vmax]*weight_cent_vmax) / np.sum(weight_cent_vmax) *1e-9
                f_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vcirc]/dt[ok_cent_vcirc]*weight_cent_vcirc) / np.sum(weight_cent_vcirc) *1e-9
                f_med_dict_sfr[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_sfr]/dt[ok_cent_sfr]*weight_cent_sfr) / np.sum(weight_cent_sfr) *1e-9
                f_med_dict_ssfr[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_ssfr]/dt[ok_cent_ssfr]*weight_cent_ssfr) / np.sum(weight_cent_ssfr) *1e-9

            elif f_name == "AGN_energy_injection_rate":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_acc_bh[ok_cent]*e_bh/dt[ok_cent]*weight_cent) / np.sum(weight_cent) *1e-9 # erg yr^-1

            elif f_name == "AGN_energy_injection_rate_vcnorm": # Energy injected / halo circular velocity**2 / (fb x halo mass)
                # (i.e. divided by energy needed to accelerate all gas particles from rest to Vcirc, assuming unity baryon fraction within the halo)
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_acc_bh[ok_cent]*e_bh/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor * fb * mchalo[ok_cent] * 0.5 * np.square(vcirc[ok_cent])*weight_cent) /to_erg # Gyr^-1

            elif f_name == "AGN_momentum_injection_rate": # This is the momentum that would be injected if all of the injected thermal energy were to be in kinetic form 
                E = mass_acc_bh[ok_cent]*e_bh # erg
                m = mass_acc_bh[ok_cent]*e_bh*mheated_perE_injected_AGN # Msun
                v = np.zeros_like(m); ok = m > 0
                v[ok] = np.sqrt(2 * E[ok]*erg/(m[ok]*Msun)) * 1e-3 # kms^-1
                p = m * v
                f_med_dict[f_name][i_bin,j_bin] = np.sum(p/dt[ok_cent]*weight_cent) / np.sum(weight_cent) *1e-9 # Msun kms^-1 yr^-1

            elif f_name == "AGN_momentum_injection_rate_vcnorm": # Momentum injected over halo mass x halo circular velocity
                # (i.e. divided by momentum needed to accelerate all gas particles from rest to Vcirc, assuming unity baryon fraction within the halo)
                E = mass_acc_bh[ok_cent]*e_bh # erg
                m = mass_acc_bh[ok_cent]*e_bh*mheated_perE_injected_AGN # Msun
                v = np.zeros_like(m); ok = m > 0
                v[ok] = np.sqrt(2 * E[ok]*erg/(m[ok]*Msun)) * 1e-3 # kms^-1
                p = m * v
                f_med_dict[f_name][i_bin,j_bin] = np.sum(p/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor*fb*mchalo[ok_cent] * vcirc[ok_cent] * weight_cent) # Gyr^-1

            elif f_name == "SNe_energy_injection_rate":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(fb_e_times_mass_new_star[ok_cent]*e_sn/dt[ok_cent]*weight_cent) / np.sum(weight_cent) *1e-9 # erg yr^-1
            elif f_name == "SNe_energy_injection_rate_vcnorm": # Energy injected / 0.5 halo circular velocity**2 / (fb x halo mass) 
                f_med_dict[f_name][i_bin,j_bin] = np.sum(fb_e_times_mass_new_star[ok_cent]*e_sn/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor * fb * mchalo[ok_cent] * 0.5 * np.square(vcirc[ok_cent])*weight_cent) /to_erg # Gyr^-1
            elif f_name == "SNe_momentum_injection_rate": # This is the momentum that would be injected if all of the injected thermal energy were to be in kinetic form 
                E = fb_e_times_mass_new_star[ok_cent]*e_sn
                m = fb_e_times_mass_new_star[ok_cent]*e_sn*mheated_perE_injected_sn # Msun
                v = np.zeros_like(m); ok = m > 0
                v[ok] = np.sqrt(2 * E[ok]*erg/(m[ok]*Msun)) * 1e-3 # kms^-1
                p = m * v
                f_med_dict[f_name][i_bin,j_bin] = np.sum(p/dt[ok_cent]*weight_cent) / np.sum(weight_cent) *1e-9 # Msun kms^-1 yr^-1

            elif f_name == "SNe_momentum_injection_rate_vcnorm": # Momentum injected over halo mass x halo circular velocity
                # (i.e. divided by momentum needed to accelerate all gas particles from rest to Vcirc, assuming unity baryon fraction within the halo)
                E = fb_e_times_mass_new_star[ok_cent]*e_sn
                m = fb_e_times_mass_new_star[ok_cent]*e_sn*mheated_perE_injected_sn # Msun
                v = np.zeros_like(m); ok = m > 0
                v[ok] = np.sqrt(2 * E[ok]*erg/(m[ok]*Msun)) * 1e-3 # kms^-1
                p = m * v # Msun kms^-1
                f_med_dict[f_name][i_bin,j_bin] = np.sum(p/dt[ok_cent]*weight_cent) / np.sum(dm2tot_factor*fb*mchalo[ok_cent] * vcirc[ok_cent] * weight_cent)              # Gyr^-1

            elif "_ml" in f_name:
                f_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent]/dt[ok_cent]*weight_cent) / np.sum(mass_new_stars[ok_cent]/dt[ok_cent]*weight_cent) # Dimensionless
                f_sat_med_dict[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat]/dt[ok_sat]*weight_sat) / np.sum(mass_new_stars[ok_sat]/dt[ok_sat]*weight_sat)

                f_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) / np.sum(mass_new_stars[ok_cent_mstar]/dt[ok_cent_mstar]*weight_cent_mstar) # Dimensionless
                f_sat_med_dict_mstar[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_mstar]/dt[ok_sat_mstar]*weight_sat_mstar) / np.sum(mass_new_stars[ok_sat_mstar]/dt[ok_sat_mstar]*weight_sat_mstar)

                f_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vmax]/dt[ok_cent_vmax]*weight_cent_vmax) / np.sum(mass_new_stars[ok_cent_vmax]/dt[ok_cent_vmax]*weight_cent_vmax) # Dimensionless
                f_sat_med_dict_vmax[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vmax]/dt[ok_sat_vmax]*weight_sat_vmax) / np.sum(mass_new_stars[ok_sat_vmax]/dt[ok_sat_vmax]*weight_sat_vmax)

                f_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_vcirc]/dt[ok_cent_vcirc]*weight_cent_vcirc) / np.sum(mass_new_stars[ok_cent_vcirc]/dt[ok_cent_vcirc]*weight_cent_vcirc) # Dimensionless
                f_sat_med_dict_vcirc[f_name][i_bin,j_bin] = np.sum(mass_out[ok_sat_vcirc]/dt[ok_sat_vcirc]*weight_sat_vcirc) / np.sum(mass_new_stars[ok_sat_vcirc]/dt[ok_sat_vcirc]*weight_sat_vcirc)

                f_med_dict_sfr[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_sfr]/dt[ok_cent_sfr]*weight_cent_sfr) / np.sum(mass_new_stars[ok_cent_sfr]/dt[ok_cent_sfr]*weight_cent_sfr) # Dimensionless
                f_med_dict_ssfr[f_name][i_bin,j_bin] = np.sum(mass_out[ok_cent_ssfr]/dt[ok_cent_ssfr]*weight_cent_ssfr) / np.sum(mass_new_stars[ok_cent_ssfr]/dt[ok_cent_ssfr]*weight_cent_ssfr) # Dimensionless
                

            elif "_out" in f_name and "_vcnorm" in f_name:
                for i_r in range(n_rbins):
                    temp = len("_vcnorm")
                    if "dE" in f_name:
                        f_med_dict[f_name][i_bin,j_bin,i_r] = np.sum(data[f_name[0:-temp]][:,i_r][ok_cent]*weight_cent) / np.sum(dm2tot_factor * fb * mchalo[ok_cent] * 0.5 * np.square(vcirc[ok_cent])*weight_cent) *1e9 # Gyr^-1
                    elif "dp" in f_name:
                        f_med_dict[f_name][i_bin,j_bin,i_r] = np.sum(data[f_name[0:-temp]][:,i_r][ok_cent]*weight_cent) / np.sum(dm2tot_factor * fb * mchalo[ok_cent] * vcirc[ok_cent]*weight_cent) *1e9 # Gyr^-1
                    else:
                        print "Yikes"
                        quit()

            elif "_out" in f_name or "shell" in f_name: # Mean shell flux

                for i_r in range(n_rbins):
                    if "grad" in f_name and i_r == 9:
                        continue
                    f_name_temp = f_name
                    if "shell" in f_name:
                        if "dmdr" in f_name:
                            f_name_temp = "m_shell"
                            y = data[f_name_temp][:,i_r][ok_cent] * n_rbins
                        elif f_name == "Ggrad_shell":
                            f_name_temp = "a_grav"
                            y = data[f_name_temp][:,i_r][ok_cent]                            
                        elif f_name == "P50fwgrad"+fV_str+"_shell":
                            f_name_temp = "P50fw_out"+fV_str
                            y2 = data[f_name_temp][:,i_r+1][ok_cent]
                            y1 = data[f_name_temp][:,i_r][ok_cent]
                            dy = y2-y1
                            kpc =  3.0857e19 # m
                            Msun = 1.989e30
                            dr = 0.1
                            rbins = np.arange(0, 1.1, 0.1)
                            rmid = 0.5*(rbins[1:]+rbins[0:-1])
                            rmid_mid = 0.5*(rmid[1:]+rmid[0:-1])
                            dydr = dy / (dr*R200[ok_cent]*kpc) # kg m^-2 s^-2
                            mshell = data["m_shell"][:,i_r][ok_cent]
                            a_pressure = -(dydr * (dr*R200[ok_cent]*kpc) * 4*np.pi*(rmid_mid[i_r]*R200[ok_cent]*kpc)**2)/( mshell*Msun) # m s^-2 
                            y = a_pressure

                        elif f_name == "P50grad_shell":
                            f_name_temp = "P50mw"
                            y2 = data[f_name_temp][:,i_r+1][ok_cent]
                            y1 = data[f_name_temp][:,i_r][ok_cent]
                            dy = (y2-y1)
                            kpc =  3.0857e19 # m
                            Msun = 1.989e30
                            dr = 0.1
                            rbins = np.arange(0, 1.1, 0.1)
                            rmid = 0.5*(rbins[1:]+rbins[0:-1])
                            rmid_mid = 0.5*(rmid[1:]+rmid[0:-1])
                            dydr = dy / (dr*R200[ok_cent]*kpc) # kg m^-2 s^-2
                            mshell = data["m_shell"][:,i_r][ok_cent]
                            a_pressure = -(dydr * (dr*R200[ok_cent]*kpc) * 4*np.pi*(rmid_mid[i_r]*R200[ok_cent]*kpc)**2)/( mshell*Msun) # m s^-2 
                            y = a_pressure

                        else:
                            f_name_temp = f_name_temp[0:-6]
                            y = data[f_name_temp][:,i_r][ok_cent]
                    else:
                        y = data[f_name][:,i_r][ok_cent]

                    ok_nan = np.isnan(y) == False # Don't include radial bins that don't contain selected gas in the average

                    f_med_dict[f_name][i_bin,j_bin,i_r] = np.sum(y[ok_nan]*weight_cent[ok_nan]) / np.sum(weight_cent[ok_nan])

            elif f_name == "time_join_halo_wind_from_ism":
                f_med_dict[f_name][i_bin,j_bin] = np.sum(data[f_name][ok_cent]*weight_cent) / np.sum(weight_cent)

            else:

                print "nope", f_name
                quit()

if cumulative_pp:
    fVmax_cut = 0.25
    fV_str = "_"+str(fVmax_cut).replace(".","p")+"vmax"

    f_name = "ism_wind_cum_ml_pp"+fV_str
    f_name_list += [f_name]
    f_med_dict[f_name] = np.zeros(len(bin_mh)-1)
    f_sat_med_dict[f_name] = np.zeros(len(bin_mh)-1)
    f_med_dict_mstar[f_name] = np.zeros(len(bin_mh)-1)
    f_sat_med_dict_mstar[f_name] = np.zeros(len(bin_mh)-1)
    f_med_dict_vmax[f_name] = np.zeros(len(bin_mh)-1)
    f_sat_med_dict_vmax[f_name] = np.zeros(len(bin_mh)-1)
    f_med_dict_vcirc[f_name] = np.zeros(len(bin_mh)-1)
    f_sat_med_dict_vcirc[f_name] = np.zeros(len(bin_mh)-1)
    f_med_dict_sfr[f_name] = np.zeros(len(bin_mh)-1)
    f_med_dict_ssfr[f_name] = np.zeros(len(bin_mh)-1)
    
    subhalo_group = File["Snap_"+str(snap_list[1:].max())+"/subhalo_properties"] # _pp data is written for the last snapshot where there are measurements (which due to _ns is not the final snapshot)
                
    subgroup_number = subhalo_group["subgroup_number"][:]
    mass_out = subhalo_group["mass_ism_wind_cumul_pp"+fV_str][:]
    sfr_cumul = subhalo_group["mass_stars_init_cumul_pp"][:]
    mstars = subhalo_group["mass_star"][:]
    if use_aperture:
        mstars = subhalo_group["mass_stars_30kpc"][:]
    mchalo = subhalo_group["m200_host"][:]

    for j_bin in range(len(bin_mh_mid)):
        ok = (np.log10(mchalo) > bin_mh[j_bin]) & (np.log10(mchalo) < bin_mh[j_bin+1])
        ok_mstar = (np.log10(mstars) > bin_mstar[j_bin]) & (np.log10(mstars) < bin_mstar[j_bin+1])

        ok_cent = ok & (subgroup_number == 0)
        ok_cent_mstar = ok_mstar & (subgroup_number == 0)

        f_med_dict[f_name][j_bin] = np.sum(mass_out[ok_cent]) / np.sum(sfr_cumul[ok_cent]) # Dimensionless
        f_med_dict_mstar[f_name][j_bin] = np.sum(mass_out[ok_cent_mstar]) / np.sum(sfr_cumul[ok_cent_mstar]) # Dimensionless

# save the results to disk
filename = input_path+"efficiencies_grid_"+sim_name+"_"+star_mass
if test_mode:
    filename += "_test_mode"
filename += ".hdf5"

if os.path.isfile(filename):
    os.remove(filename)

print "Writing", filename
File_grid = h5py.File(filename)

File_grid.create_dataset("log_msub_grid", data=bin_mh_mid)
File_grid.create_dataset("log_mstar_grid", data=bin_mstar_mid)
File_grid.create_dataset("log_vmax_grid", data=bin_vmax_mid)
File_grid.create_dataset("log_vcirc_grid", data=bin_vcirc_mid)
File_grid.create_dataset("log_sfr_grid", data=bin_sfr_mid)
File_grid.create_dataset("log_ssfr_grid", data=bin_ssfr_mid)
File_grid.create_dataset("t_grid", data=bin_mid_t)
File_grid.create_dataset("a_grid", data=bin_mid_a)
File_grid.create_dataset("a_median", data=bin_a_median)
File_grid.create_dataset("t_min", data=t_min)
File_grid.create_dataset("t_max", data=t_max)
File_grid.create_dataset("a_min", data=a_min)
File_grid.create_dataset("a_max", data=a_max)

for f_name in f_name_list:

    File_grid.create_dataset(f_name, data=f_med_dict[f_name])
    File_grid.create_dataset(f_name+"_sat", data=f_sat_med_dict[f_name])

    File_grid.create_dataset(f_name+"_mstar", data=f_med_dict_mstar[f_name])
    File_grid.create_dataset(f_name+"_sat_mstar", data=f_sat_med_dict_mstar[f_name])

    File_grid.create_dataset(f_name+"_vmax", data=f_med_dict_vmax[f_name])
    File_grid.create_dataset(f_name+"_sat_vmax", data=f_sat_med_dict_vmax[f_name])

    File_grid.create_dataset(f_name+"_vcirc", data=f_med_dict_vcirc[f_name])
    File_grid.create_dataset(f_name+"_sat_vcirc", data=f_sat_med_dict_vcirc[f_name])

    File_grid.create_dataset(f_name+"_sfr", data=f_med_dict_sfr[f_name])
    File_grid.create_dataset(f_name+"_ssfr", data=f_med_dict_ssfr[f_name])

for f_name in f_name_list2:
    File_grid.create_dataset(f_name, data=f_med_dict2[f_name])
    File_grid.create_dataset(f_name+"_sat", data=f_sat_med_dict2[f_name])

    File_grid.create_dataset(f_name+"_mstar", data=f_med_dict2_mstar[f_name])
    File_grid.create_dataset(f_name+"_vcirc", data=f_med_dict2_vcirc[f_name])
    File_grid.create_dataset(f_name+"_sfr", data=f_med_dict2_sfr[f_name])

File_grid.close()
